delete from err_list_s where languageid = 7

Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (200,'Errore di sistema.Prego contattare l''amministratore di VRM e fornire questo codice di errore.','Errore di sistema.Prego contattare l''amministratore di VRM e fornire questo codice di errore.','S',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (201,'UserID mancante.Operazione interrotta o UserID non fornito.','UserID mancante.Operazione interrotta o UserID non fornito.','S',7,'S')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (202,'Inserisci un nome conferenza.','Inserisci un nome conferenza.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (203,'Inserisci una data e ora valida.','Inserisci una data e ora valida.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (205,'L''utente non � registrato in VRM.','L''utente non � registrato in VRM.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (206,'Si prega di inserire un nome univoco di accesso.','Si prega di inserire un nome univoco di accesso.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (207,'Accesso o password non validi.Per favore riprova.','Accesso o password non validi.Per favore riprova.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (208,'Tuo account � bloccato a causa di molteplici tentativi di password non valida.Si prega di contattare l''amministratore di VRM.','Tuo account � bloccato a causa di molteplici tentativi di password non valida.Si prega di contattare l''amministratore di VRM.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (209,'Si prega d''inserire�un indirizzo di posta elettronica valido.','Si prega d''inserire�un indirizzo di posta elettronica valido.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (210,'Non abbastanza porte IP o T1.','Non abbastanza porte IP o T1.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (211,'Non abbastanza porte T1 Mux.','Non abbastanza porte T1 Mux.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (212,'Inserisci un nome conferenza univoco.','Inserisci un nome conferenza univoco.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (213,'Non abbastanza porte T1.','Non abbastanza porte T1.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (214,'Non abbastanza schede audio.','Non abbastanza schede audio.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (215,'Non abbastanza schede video.','Non abbastanza schede video.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (216,'Complicato errore nel calcolo della risorsa.','Complicato errore nel calcolo della risorsa.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (217,'Si prega di inserire un nome univoco di gruppo.','Si prega di inserire un nome univoco di gruppo.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (218,'Il saldo del tuo conto non � sufficiente per questo cambiamento di conferenza. Prego contattare l''amministratore di VRM e fornire questo codice di errore.','Il saldo del tuo conto non � sufficiente per questo cambiamento di conferenza. Prego contattare l''amministratore di VRM e fornire questo codice di errore.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (219,'Inserisci il tuo Nome, Cognome o Indirizzo e-mail.','Inserisci il tuo Nome, Cognome o Indirizzo e-mail.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (220,'Nome utente, Login, Indirizzo e-mail duplicati. Si prega di controllare e inserire nuovamente.','Nome utente, Login, Indirizzo e-mail duplicati. Si prega di controllare e inserire nuovamente.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (221,'La conferenza � stata creata con successo. "Nuovo utente" con indirizzo e-mail partecipante duplicato <indirizzo e-mail> non invitato - utente gi� presente nel database VRM � stato invitato.','La conferenza � stata creata con successo. "Nuovo utente" con indirizzo e-mail partecipante duplicato <indirizzo e-mail> non invitato - utente gi� presente nel database VRM � stato invitato.','I',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (222,'Si prega di inserire un nome univoco di modello.','Si prega di inserire un nome univoco di modello.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (223,'Si prega di inserire una data/ora di inizio della conferenza valida.','Si prega di inserire una data/ora di inizio della conferenza valida.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (224,'Solo funzione super amministratore. Si prega di contattare l''amministratore VRM per ulteriori informazioni.','Solo funzione super amministratore. Si prega di contattare l''amministratore VRM per ulteriori informazioni.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (225,'Accetta il tuo invito conferenza prima dell''accesso','Accetta il tuo invito conferenza prima dell''accesso','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (226,'Risorse di sistema insufficienti per questa particolare conferenza. Si prega di provare a ridurre il numero dei partecipanti o stanze comprese nella conferenza. Per maggiore assistenza prego contattare l''amministratore di VRM e fornire questo codice di errore.','Risorse di sistema insufficienti per questa particolare conferenza. Si prega di provare a ridurre il numero dei partecipanti o stanze comprese nella conferenza. Per maggiore assistenza prego contattare l''amministratore di VRM e fornire questo codice di errore.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (227,'Inserisci un login o password validi.','Inserisci un login o password validi.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (228,'Si prega di controllare le impostazioni conferenza ricorrenti e rientrare.','Si prega di controllare le impostazioni conferenza ricorrenti e rientrare.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (229,'Non hai il permesso per questa funzione. Prego contattare l''amministratore di VRM e fornire questo codice di errore per assistenza.','Non hai il permesso per questa funzione. Prego contattare l''amministratore di VRM e fornire questo codice di errore per assistenza.','S',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (230,'Il saldo del tuo conto non � sufficiente per configurare questa conferenza. Prego contattare l''amministratore di VRM e fornire questo codice di errore.','Il saldo del tuo conto non � sufficiente per configurare questa conferenza. Prego contattare l''amministratore di VRM e fornire questo codice di errore.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (231,'Saldo del conto insufficiente per configurare una conferenza. Prego contattare l''amministratore di VRM e fornire questo codice di errore per ottenere un account di utilizzo.','Saldo del conto insufficiente per configurare una conferenza. Prego contattare l''amministratore di VRM e fornire questo codice di errore per ottenere un account di utilizzo.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (233,'Errore di sistema. Si prega di chiudere e riavviare il browser e accedere al VRM di nuovo per ripristinare l''applicazione.','Errore di sistema. Si prega di chiudere e riavviare il browser e accedere al VRM di nuovo per ripristinare l''applicazione.','S',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (234,'Non c''� gruppo che soddisfa i criteri di ricerca.','Non c''� gruppo che soddisfa i criteri di ricerca.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (237,'Si prega di inserire un nome univoco di ponte.','Si prega di inserire un nome univoco di ponte.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (238,'Errore di fatturazione. Prego contattare l''amministratore di VRM e fornire questo codice di errore.','Errore di fatturazione. Prego contattare l''amministratore di VRM e fornire questo codice di errore.','S',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (240,'Risorse di sistema insufficienti per questa particolare conferenza. Nessun numero di telefono ISDN utilizzabile � stato trovato per il ponte. Per maggiore assistenza prego contattare l''amministratore di VRM e fornire questo codice di errore.','Risorse di sistema insufficienti per questa particolare conferenza. Nessun numero di telefono ISDN utilizzabile � stato trovato per il ponte. Per maggiore assistenza prego contattare l''amministratore di VRM e fornire questo codice di errore.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (241,'Una o pi� stanze selezionate sono gi� state prenotate.','Una o pi� stanze selezionate sono gi� state prenotate.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (242,'La conferenza richiesta � stata superata o non esiste','La conferenza richiesta � stata superata o non esiste','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (243,'La conferenza richiesta � stata eliminata da parte dell''ospitante o amministratore.','La conferenza richiesta � stata eliminata da parte dell''ospitante o amministratore.','I',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (244,'Conferenze punto-a-punto sono state disabilitate dall''amministratore. Si prega di scegliere una delle altre sezioni per iniziare una conferenza.','Conferenze punto-a-punto sono state disabilitate dall''amministratore. Si prega di scegliere una delle altre sezioni per iniziare una conferenza.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (250,'Chiave di sicurezza non valida o Ora scaduta.Si prega di contattare l''amministratore del VRM per ulteriore assistenza.','Chiave di sicurezza non valida o Ora scaduta.Si prega di contattare l''amministratore del VRM per ulteriore assistenza.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (252,'La tua Licenza VRM non consente questa operazione','La tua Licenza VRM non consente questa operazione','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (253,'Sistema VRM non � disponibile durante la data e l''ora prescelta. Si prega di verificare la disponibilit� del sistema VRM e riprovare.','Sistema VRM non � disponibile durante la data e l''ora prescelta. Si prega di verificare la disponibilit� del sistema VRM e riprovare.','C',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (254,'Il tuo account � stato bloccato. Prego contattare l''amministratore di VRM per assistenza.','Il tuo account � stato bloccato. Prego contattare l''amministratore di VRM per assistenza.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (255,'La conferenza richiesta � in attesa di approvazione. Si prega di contattare l''amministratore VRM per ulteriore assistenza','La conferenza richiesta � in attesa di approvazione. Si prega di contattare l''amministratore VRM per ulteriore assistenza','S',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (256,'Il nome richiesto � gi� stato preso. Si prega di scegliere un altro nome.','Il nome richiesto � gi� stato preso. Si prega di scegliere un altro nome.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (257,'Il nome dell''edificio richiesto per questa citt� � gi� stato preso. Si prega di scegliere un altro nome per questo edificio','Il nome dell''edificio richiesto per questa citt� � gi� stato preso. Si prega di scegliere un altro nome per questo edificio','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (258,'Il nome della citt� richiesto � gi� stato preso. Si prega di scegliere un altro nome.','Il nome della citt� richiesto � gi� stato preso. Si prega di scegliere un altro nome.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (259,'La parte richiesta � gi� presente alla conferenza','La parte richiesta � gi� presente alla conferenza','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (260,'Il nome di ponte richiesto � gi� stato assegnato a un altro ponte','Il nome di ponte richiesto � gi� stato assegnato a un altro ponte','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (261,'Il nome utente / e-mail richiesto � gi� in uso','Il nome utente / e-mail richiesto � gi� in uso','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (262,'Una o pi� camere selezionate non hanno punti di fine associati.','Una o pi� camere selezionate non hanno punti di fine associati.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (263,'Il tuo account VRM � scaduto ed � stato bloccato. Si prega di contattare l''amministratore VRM locale per ulteriore assistenza','Il tuo account VRM � scaduto ed � stato bloccato. Si prega di contattare l''amministratore VRM locale per ulteriore assistenza','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (264,'Tuo account VRM sta per scadere il %s. Si prega di contattare l''amministratore VRM per ulteriore assistenza. Il tuo account scade fra {0} giorni.','Tuo account VRM sta per scadere il %s. Si prega di contattare l''amministratore VRM per ulteriore assistenza. Il tuo account scade fra {0} giorni.','M',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (302,'Chiave di sicurezza non valida o Tempo Scaduto.','Chiave di sicurezza non valida o Tempo Scaduto.','S',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (305,'Impossibile eseguire sql. Errore nel testo.','Impossibile eseguire sql. Errore nel testo.','S',7,'S')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (306,'Impossibile leggere DB.','Impossibile leggere DB.','S',7,'S')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (307,'Data di scadenza dell''account non valida. Si prega di controllare la licenza. ','Data di scadenza dell''account non valida. Si prega di controllare la licenza. ','S',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (308,'Conferenza non pu� essere cancellata. O � scaduta o � stata terminata.','Conferenza non pu� essere cancellata. O � scaduta o � stata terminata.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (309,'Info account utente mancanti o danneggiate nel database.','Info account utente mancanti o danneggiate nel database.','S',7,'S')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (310,'Errore nell''operazione dell''utente, utente in uso. Impossibile eliminare!','Errore nell''operazione dell''utente, utente in uso. Impossibile eliminare!','S',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (311,'Si � verificato un errore interno - codice di errore 311 (Errore, errore id stanza in utenteconf per l''utente <nome> )','Si � verificato un errore interno - codice di errore 311 (Errore, errore id stanza in utenteconf per l''utente <nome> )','S',7,'S')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (312,'Errore1 dati fuso orario di una certa conferenza.','Errore1 dati fuso orario di una certa conferenza.','S',7,'S')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (313,'Errore di tabella fuso orario','Errore di tabella fuso orario','S',7,'S')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (314,'Errore di query < query di database >','Errore di query < query di database >','S',7,'S')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (315,'Sostituire errore utente','Sostituire errore utente','S',7,'S')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (316,'Impossibile interrogare [utente].','Impossibile interrogare [utente].','S',7,'S')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (317,'Impossibile interrogare Operatore Bounded.','Impossibile interrogare Operatore Bounded.','S',7,'S')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (318,'seleziona id alimento, nome alimento dal men� alimenti','seleziona id alimento, nome alimento dal men� alimenti','S',7,'S')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (319,'RecuperaListaGruppi','RecuperaListaGruppi','S',7,'S')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (320,'numero di query da PartecipanteGruppo','numero di query da PartecipanteGruppo','S',7,'S')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (321,'query da PartecipanteGruppo, [utente]','query da PartecipanteGruppo, [utente]','S',7,'S')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (322,'inserisci gruppoconf <stmt>','inserisci gruppoconf <stmt>','S',7,'S')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (323,'Impossibile inserire gruppoconf','Impossibile inserire gruppoconf','S',7,'S')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (325,'Elimina errore.','Elimina errore.','S',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (326,'Non � possibile inserire Stanzaconf.','Non � possibile inserire Stanzaconf.','S',7,'S')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (327,'Non � possibile inserire altre posizioni in Stanzaconf.','Non � possibile inserire altre posizioni in Stanzaconf.','S',7,'S')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (328,'Impossibile interrogare [StanzaConferenza].','Impossibile interrogare [StanzaConferenza].','S',7,'S')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (330,'RecuperaPosizioni','RecuperaPosizioni','S',7,'S')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (331,'Si � verificato un errore interno � codice di errore 331 (selezionare conto(*) da utenteconf dove idconf.)','Si � verificato un errore interno � codice di errore 331 (selezionare conto(*) da utenteconf dove idconf.)','S',7,'S')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (334,'selezionare un errore dell''operatore','selezionare un errore dell''operatore','S',7,'S')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (400,'Errore di convalida XML','Errore di convalida XML','S',7,'S')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (402,'Massimo gruppi superato.','Massimo gruppi superato.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (403,'Massimo modelli superato.','Massimo modelli superato.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (404,'Errore durante la lettura delle licenze.','Errore durante la lettura delle licenze.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (406,'Reparto � in uso. Impossibile eliminare!','Reparto � in uso. Impossibile eliminare!','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (407,'Verifica della licenza non riuscita. Si prega di contattare l''amministratore locale o VRM per ulteriore assistenza','Verifica della licenza non riuscita. Si prega di contattare l''amministratore locale o VRM per ulteriore assistenza','S',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (408,'Intervallo di data selezionato non valido','Intervallo di data selezionato non valido','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (410,'Errore di creazione di conferenza. Separa stanza non pu� che essere NON VIDEO!','Errore di creazione di conferenza. Separa stanza non pu� che essere NON VIDEO!','S',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (411,'Impossibile eliminare MCU. Rimuovere tutte le risorse collegate e riprovare.','Impossibile eliminare MCU. Rimuovere tutte le risorse collegate e riprovare.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (412,'Duplica nome del reparto. Non � possibile creare / modificare!','Duplica nome del reparto. Non � possibile creare / modificare!','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (413,'Nome del punto di fine esiste gi�. Si prega di scegliere un altro nome.','Nome del punto di fine esiste gi�. Si prega di scegliere un altro nome.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (414,'Collegamento non riuscito. Transazione ritirata! ','Collegamento non riuscito. Transazione ritirata! ','S',7,'S')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (415,'Duplica Nome gruppo.','Duplica Nome gruppo.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (416,'Comando non � supportato.','Comando non � supportato.','S',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (417,'Livello � in uso e non pu� essere cancellato.','Livello � in uso e non pu� essere cancellato.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (418,'Errore Punto di fine deve essere assegnato a un ponte','Errore Punto di fine deve essere assegnato a un ponte','S',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (419,'Conferenza � finita.','Conferenza � finita.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (420,'Tipo di categoria non valido','Tipo di categoria non valido','S',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (422,'Operazione non valida','Operazione non valida','S',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (423,'ID Organizzazione non valido','ID Organizzazione non valido','S',7,'S')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (424,'Immagine non valida o ID mancante','Immagine non valida o ID mancante','S',7,'S')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (425,'Sito non � attualmente attivato. Prego contattare l''amministratore di VRM per assistenza.','Sito non � attualmente attivato. Prego contattare l''amministratore di VRM per assistenza.','S',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (426,'L''indirizzo e-mail richiesto � gi� in uso - Stato inattivo','L''indirizzo e-mail richiesto � gi� in uso - Stato inattivo','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (427,'Nome gi� esiste per livello1.','Nome gi� esiste per livello1.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (428,'Nome gi� esiste per livello2.','Nome gi� esiste per livello2.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (429,'Licenza modello scaduta. Si prega di contattare l''amministratore di VRM.','Licenza modello scaduta. Si prega di contattare l''amministratore di VRM.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (430,'L''utente amministratore � stato eliminato.','L''utente amministratore � stato eliminato.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (431,'Errore nella generazione di rapporti','Errore nella generazione di rapporti','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (432,'Una conferenza ricorrente deve contenere almeno (2) istanze. Si prega di modificare il criterio di ricorrenza prima della presentazione della conferenza.','Una conferenza ricorrente deve contenere almeno (2) istanze. Si prega di modificare il criterio di ricorrenza prima della presentazione della conferenza.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (433,'Seleziona una camera','Seleziona una camera','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (434,'Nome del punto di fine esiste gi�.','Nome del punto di fine esiste gi�.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (435,'ERRORE non si pu� calcolare l''utilizzo di MCU','ERRORE non si pu� calcolare l''utilizzo di MCU','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (436,'Errore Fornitore non trovato in','Errore Fornitore non trovato in','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (437,'Ci sono conferenze in attesa di approvazione da questo utente. Si prega di approvare conferenze in lista> Visualizza attesa di approvazione prima di eliminare questo utente.','Ci sono conferenze in attesa di approvazione da questo utente. Si prega di approvare conferenze in lista> Visualizza attesa di approvazione prima di eliminare questo utente.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (438,'Errore modello � in uso. Impossibile eliminare','Errore modello � in uso. Impossibile eliminare','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (439,'Errore nell''aggiornamento delle impostazioni organizzazione.','Errore nell''aggiornamento delle impostazioni organizzazione.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (440,'Errore nell''aggiornamento del contatore di approvazione degli utenti.','Errore nell''aggiornamento del contatore di approvazione degli utenti.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (441,'Errore nell''aggiornamento approvatori di sistema per l''organizzazione.','Errore nell''aggiornamento approvatori di sistema per l''organizzazione.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (442,'Errore nell''aggiornamento dei dettagli tecnici dell''organizzazione.','Errore nell''aggiornamento dei dettagli tecnici dell''organizzazione.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (443,'Errore nell''aggiornamento dei dettagli dell''organizzazione.','Errore nell''aggiornamento dei dettagli dell''organizzazione.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (444,'Solo gli amministratori del sito possono creare / modificare l''organizzazione.','Solo gli amministratori del sito possono creare / modificare l''organizzazione.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (445,'Limite di organizzazione superato.','Limite di organizzazione superato.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (446,'Titolo organizzazione esiste gi�.','Titolo organizzazione esiste gi�.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (447,'Errore durante la creazione dei dati di predefiniti per l''organizzazione.','Errore durante la creazione dei dati di predefiniti per l''organizzazione.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (448,'Errore durante la creazione dei dati tecnici predefiniti per l''organizzazione.','Errore durante la creazione dei dati tecnici predefiniti per l''organizzazione.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (449,'Errore nella creazione di attributi personalizzati predefiniti per l''organizzazione.','Errore nella creazione di attributi personalizzati predefiniti per l''organizzazione.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (450,'Errore nell''aggiornamento dei limiti delle risorse dell''organizzazione.','Errore nell''aggiornamento dei limiti delle risorse dell''organizzazione.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (451,'Non � possibile abilitare il modulo Struttura, numero di licenza superato.','Non � possibile abilitare il modulo Struttura, numero di licenza superato.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (452,'Impossibile abilitare modulo Catering, conto licenza superato.','Impossibile abilitare modulo Catering, conto licenza superato.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (453,'Impossibile abilitare modulo Pulizia, conto licenza superato.','Impossibile abilitare modulo Pulizia, conto licenza superato.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (454,'Non � possibile abilitare il modulo API, conto licenza superato.','Non � possibile abilitare il modulo API, conto licenza superato.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (455,'Limite stanze video supera licenza VRM.','Limite stanze video supera licenza VRM.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (456,'Limite stanze non-video supera licenza VRM.','Limite stanze non-video supera licenza VRM.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (457,'Limite MCU supera licenza VRM.','Limite MCU supera licenza VRM.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (458,'Limite punto di fine supera licenza VRM.','Limite punto di fine supera licenza VRM.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (459,'Limite utente supera licenza VRM.','Limite utente supera licenza VRM.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (460,'Limite utente di Exchange supera licenza VRM.','Limite utente di Exchange supera licenza VRM.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (461,'Limite utente Domino supera licenza VRM.','Limite utente Domino supera licenza VRM.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (462,'Limite utente dovrebbe essere comprensivo di entrambi gli utenti Domino ed Exchange.','Limite utente dovrebbe essere comprensivo di entrambi gli utenti Domino ed Exchange.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (463,'Si prega di disattivare stanze video per ridurre il numero. Poich� ci sono pi� stanze video attive.','Si prega di disattivare stanze video per ridurre il numero. Poich� ci sono pi� stanze video attive.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (464,'Si prega di disattivare stanze non video per ridurre il numero. Poich� ci sono pi� stanze non-video attive.','Si prega di disattivare stanze non video per ridurre il numero. Poich� ci sono pi� stanze non-video attive.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (465,'Si prega di cancellare MCU per ridurre il numero. Poich� ci sono pi� MCU attivi.','Si prega di cancellare MCU per ridurre il numero. Poich� ci sono pi� MCU attivi.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (466,'Si prega di eliminare i punti finali per ridurre il numero. Poich� ci sono pi� punti di fine attivi.','Si prega di eliminare i punti finali per ridurre il numero. Poich� ci sono pi� punti di fine attivi.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (467,'Si prega di eliminare gli utenti per ridurre il numero. Poich� ci sono pi� utenti attivi.','Si prega di eliminare gli utenti per ridurre il numero. Poich� ci sono pi� utenti attivi.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (468,'L''organizzazione di predefinita non pu� essere eliminata.','L''organizzazione di predefinita non pu� essere eliminata.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (469,'L''organizzazione ha la lista di utenti attivi. Eliminare gli utenti prima di eliminare l''organizzazione.','L''organizzazione ha la lista di utenti attivi. Eliminare gli utenti prima di eliminare l''organizzazione.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (470,'L''organizzazione ha la lista delle stanze. Eliminare stanze prima di eliminare l''organizzazione.','L''organizzazione ha la lista delle stanze. Eliminare stanze prima di eliminare l''organizzazione.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (471,'L''organizzazione contiene la lista MCU. Eliminare MCU prima di eliminare l''organizzazione.','L''organizzazione contiene la lista MCU. Eliminare MCU prima di eliminare l''organizzazione.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (472,'Errore durante l''eliminazione dell''organizzazione','Errore durante l''eliminazione dell''organizzazione','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (473,'Impossibile eliminare rapporto','Impossibile eliminare rapporto','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (474,'Rapporto non esiste!','Rapporto non esiste!','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (475,'Operazione riuscita!','Operazione riuscita!','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (476,'Errore nella conversione LDAP. Nessun record di modello utente','Errore nella conversione LDAP. Nessun record di modello utente','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (477,'Errore nella conversione LDAP. Nessun record di ruolo utente','Errore nella conversione LDAP. Nessun record di ruolo utente','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (478,'Errore nome/e-mail utente esiste. Impossibile aggiungere','Errore nome/e-mail utente esiste. Impossibile aggiungere','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (479,'Si � verificato un errore interno; � necessario definire un utente � codice di errore 479 (Errore; Devi aggiungere un solo UTENTE!)','Si � verificato un errore interno; � necessario definire un utente � codice di errore 479 (Errore; Devi aggiungere un solo UTENTE!)','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (480,'Errore non si pu� convertire dataora','Errore non si pu� convertire dataora','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (481,'Impossibile eliminare! Men� in uso da parte dei seguenti ordini di lavoro:','Impossibile eliminare! Men� in uso da parte dei seguenti ordini di lavoro:','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (482,'Si prega di eliminare gli utenti exchange per ridurre il numero. Poich� ci sono pi� utenti exchange attivi.','Si prega di eliminare gli utenti exchange per ridurre il numero. Poich� ci sono pi� utenti exchange attivi.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (483,'L''opzione selezionata personalizzata � collegata con la conferenza. Non pu� essere modificata / cancellata.','L''opzione selezionata personalizzata � collegata con la conferenza. Non pu� essere modificata / cancellata.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (484,'Errore durante l''eliminazione dell''attributo personalizzato','Errore durante l''eliminazione dell''attributo personalizzato','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (485,'Titolo opzione personalizzata � gi� presente nella lista.','Titolo opzione personalizzata � gi� presente nella lista.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (486,'Limite massimo per le opzioni personalizzate �','Limite massimo per le opzioni personalizzate �','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (487,'Errore utente non autorizzato come amministratore in carica. Inventario NON salvato.','Errore utente non autorizzato come amministratore in carica. Inventario NON salvato.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (488,'Errore: nome esiste gi�','Errore: nome esiste gi�','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (489,'Errore utente non ha l''autorizzazione dipartimentale di amministrare stanze.','Errore utente non ha l''autorizzazione dipartimentale di amministrare stanze.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (490,'ERRORE non si pu� rimuovere la stanza di inventario. Ordine di lavoro in attesa','ERRORE non si pu� rimuovere la stanza di inventario. Ordine di lavoro in attesa','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (491,'Errore utente non autorizzato come amministratore in carica. Ordine di lavoro NON salvato.','Errore utente non autorizzato come amministratore in carica. Ordine di lavoro NON salvato.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (492,'Errore utente non ha l''autorizzazione dipartimentale di amministrare Ordine di lavoro. Ordine di lavoro NON salvato.','Errore utente non ha l''autorizzazione dipartimentale di amministrare Ordine di lavoro. Ordine di lavoro NON salvato.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (493,'Errore Impossibile eliminare Inventario in uso.','Errore Impossibile eliminare Inventario in uso.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (494,'Si prega di eliminare gli utenti dominio per ridurre il numero. Poich� ci sono pi� utenti dominio attivi.','Si prega di eliminare gli utenti dominio per ridurre il numero. Poich� ci sono pi� utenti dominio attivi.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (495,'Ci sono {0} UTENTI allegati a questo MCU,','Ci sono {0} UTENTI allegati a questo MCU,','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (496,'Ci sono {0} PUNTI DI FINE che sono assegnati a questo MCU,','Ci sono {0} PUNTI DI FINE che sono assegnati a questo MCU,','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (497,'Proprietario della conferenza non esiste durante la creazione della Conferenza: File:','Proprietario della conferenza non esiste durante la creazione della Conferenza: File:','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (498,'Indirizzo e-mail non valido','Indirizzo e-mail non valido','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (499,'Si prega di impostare i valori per le opzioni obbligatorie personalizzate.','Si prega di impostare i valori per le opzioni obbligatorie personalizzate.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (500,'Errore nel calcolo dell''inventario in tempo reale','Errore nel calcolo dell''inventario in tempo reale','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (501,'Connessione LDAP fallita.','Connessione LDAP fallita.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (502,'Errore durante il recupero delle impostazioni ldap','Errore durante il recupero delle impostazioni ldap','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (503,'Errore durante l''autenticazione dell''utente Idap.','Errore durante l''autenticazione dell''utente Idap.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (504,'Errore nel confrontare e caricare gli utenti in un''area di attesa ldap.','Errore nel confrontare e caricare gli utenti in un''area di attesa ldap.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (505,'Connessione riuscita. Tuttavia, nessun record corrispondente ldap � stato trovato.','Connessione riuscita. Tuttavia, nessun record corrispondente ldap � stato trovato.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (506,'Autenticazione non riuscita! Nessun record ldap corrispondente � stato trovato.','Autenticazione non riuscita! Nessun record ldap corrispondente � stato trovato.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (507,'Impossibile eliminare il punto finale: punto finale � associato alla stanza/e','Impossibile eliminare il punto finale: punto finale � associato alla stanza/e','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (508,'Punto finale non trovato! Impossibile aggiornare il punto finale:','Punto finale non trovato! Impossibile aggiornare il punto finale:','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (509,'Impossibile aggiornare le tabelle utente o ldap','Impossibile aggiornare le tabelle utente o ldap','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (510,'Conflitti trovati in alcuni casi ricorrenti. Si prega di modificare le singole istanze qui sotto','Conflitti trovati in alcuni casi ricorrenti. Si prega di modificare le singole istanze qui sotto','C',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (511,'Controllo Licenza FALLITO: limite organizzazione superato','Controllo Licenza FALLITO: limite organizzazione superato','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (512,'Verifica della licenza FALLITA. Limite stanze superato','Verifica della licenza FALLITA. Limite stanze superato','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (513,'Verifica della licenza FALLITA. Superato il limite di utenti','Verifica della licenza FALLITA. Superato il limite di utenti','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (514,'Verifica della licenza FALLITA. Superato il limite MCU','Verifica della licenza FALLITA. Superato il limite MCU','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (515,'Verifica della licenza FALLITA: data di scadenza non corretta','Verifica della licenza FALLITA: data di scadenza non corretta','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (516,'Errore nel Aggiornamento di MCU come privato, MCU � stato utilizzato','Errore nel Aggiornamento di MCU come privato, MCU � stato utilizzato','E',7,'U')
INSERT INTO err_list_s (ID,  Message, Description, Level, Languageid,ErrorType) VALUES (518,'Utente � collegato con una conferenza sia come Programmatore / Ospitante / Partecipanti. Impossibile eliminare!','Utente � collegato con una conferenza sia come Programmatore / Ospitante / Partecipanti. Impossibile eliminare!','E',7,'U')
INSERT INTO err_list_s (ID,  Message, Description, Level, Languageid,ErrorType) VALUES (519,'Errore di funzionamento degli utenti, l''utente viene assegnato come approvatore di Sistema / MCU / stanza / Reparto. Impossibile eliminare!','Errore di funzionamento degli utenti, l''utente viene assegnato come approvatore di Sistema / MCU / stanza / Reparto. Impossibile eliminare!','E',7,'U')
INSERT INTO err_list_s (ID,  Message, Description, Level, Languageid,ErrorType) VALUES (520,'Errore di funzionamento degli utenti, l''utente viene assegnato come Sistema / MCU / Stanza / Ordine di lavoro In carica. Impossibile eliminare!','Errore di funzionamento degli utenti, l''utente viene assegnato come Sistema / MCU / Stanza / Ordine di lavoro In carica. Impossibile eliminare!','E',7,'U')
INSERT INTO err_list_s (ID,  Message, Description, Level, Languageid,ErrorType) VALUES (521,'Utente � collegato con un gruppo / reparto. Impossibile eliminare!','Utente � collegato con un gruppo / reparto. Impossibile eliminare!','E',7,'U')
INSERT INTO err_list_s (ID,  Message, Description, Level, Languageid,ErrorType) VALUES (522,'Questa versione dell''addin myVRM non � supportata!','Questa versione dell''addin myVRM non � supportata!','E',7,'U')
INSERT INTO err_list_s (ID,  Message, Description, Level, Languageid,ErrorType) VALUES (523,'L''utente non ha licenza di Exchange.','L''utente non ha licenza di Exchange.','E',7,'U')
INSERT INTO err_list_s (ID,  Message, Description, Level, Languageid,ErrorType) VALUES (524,'L''utente non ha la licenza Domino,','L''utente non ha la licenza Domino,','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (525,'Utente non dispone della licenza Mobile,','Utente non dispone della licenza Mobile,','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (526,'Limite utente mobile supera licenza VRM.','Limite utente mobile supera licenza VRM.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (527,'Si prega di eliminare gli utenti mobili per ridurre il numero. Poich� ci sono pi� utenti mobili attivi.','Si prega di eliminare gli utenti mobili per ridurre il numero. Poich� ci sono pi� utenti mobili attivi.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (528,'Verifica della licenza FALLITA. Ridurre il limite di utenti mobili in qualsiasi Organizzazione.','Verifica della licenza FALLITA. Ridurre il limite di utenti mobili in qualsiasi Organizzazione.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (529,'Inserisci ID e-mail.','Inserisci ID e-mail.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (530,'Inserisci la password.','Inserisci la password.','E',7,'U')
INSERT INTO err_list_s (ID,  Message, Description, Level, Languageid,ErrorType) VALUES (531,'ID Ponte � vuoto!','ID Ponte � vuoto!','E',7,'U')
INSERT INTO err_list_s (ID,  Message, Description, Level, Languageid,ErrorType) VALUES (534,'Non sei un partecipante a questa conferenza e la conferenza selezionata � passata o � stata cancellata.','Non sei un partecipante a questa conferenza e la conferenza selezionata � passata o � stata cancellata.','S',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (536,'La stanza � collegata con la conferenza. Non pu� essere disattivata','La stanza � collegata con la conferenza. Non pu� essere disattivata','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (537,'Conflitto di istanza in date selezionate personalizzate.','Conflitto di istanza in date selezionate personalizzate.','E',7,'U')
INSERT INTO err_list_s (ID,  Message, Description, Level, Languageid,ErrorType) VALUES (538,'Stanza in uso (non pu� essere eliminata essendo utilizzata come stanza preferita da un utente)!','Stanza in uso (non pu� essere eliminata essendo utilizzata come stanza preferita da un utente)!','E',7,'U')
INSERT INTO err_list_s (ID,  Message, Description, Level, Languageid,ErrorType) VALUES (539,'Stanza in uso (non pu� essere eliminata essendo utilizzata come stanza preferita nel modello utente)!','Stanza in uso (non pu� essere eliminata essendo utilizzata come stanza preferita nel modello utente)!','E',7,'U')
INSERT INTO err_list_s (ID,  Message, Description, Level, Languageid,ErrorType) VALUES (540,'Stanza in uso (non pu� essere eliminata essendo utilizzata nell''ordine di lavoro)!','Stanza in uso (non pu� essere eliminata essendo utilizzata nell''ordine di lavoro)!','E',7,'U')
INSERT INTO err_list_s (ID,  Message, Description, Level, Languageid,ErrorType) VALUES (541,'Stanza in uso (non pu� essere eliminata essendo utilizzata nel modello di ricerca)!','Stanza in uso (non pu� essere eliminata essendo utilizzata nel modello di ricerca)!','E',7,'U')
INSERT INTO err_list_s (ID,  Message, Description, Level, Languageid,ErrorType) VALUES (542,'Conferenze audio/video sono state disabilitate dall''amministratore. Si prega di contattare l''amministratore di VRM.','Conferenze audio/video sono state disabilitate dall''amministratore. Si prega di contattare l''amministratore di VRM.','E',7,'U')
INSERT INTO err_list_s (ID,  Message, Description, Level, Languageid,ErrorType) VALUES (543,'Conferenze solo audio sono state disabilitate dall''amministratore. Si prega di contattare l''amministratore di VRM.','Conferenze solo audio sono state disabilitate dall''amministratore. Si prega di contattare l''amministratore di VRM.','E',7,'U')
INSERT INTO err_list_s (ID,  Message, Description, Level, Languageid,ErrorType) VALUES (544,'Conferenze in stanza sono state disabilitate dall''amministratore. Si prega di contattare l''amministratore di VRM.','Conferenze in stanza sono state disabilitate dall''amministratore. Si prega di contattare l''amministratore di VRM.','E',7,'U')
INSERT INTO err_list_s (ID,  Message, Description, Level, Languageid,ErrorType) VALUES (545,'La stanza � collegata con un modello. Non � possibile disattivarla!','La stanza � collegata con un modello. Non � possibile disattivarla!','E',7,'U')
INSERT INTO err_list_s (ID,  Message, Description, Level, Languageid,ErrorType) VALUES (546,'Almeno un elemento dovrebbe avere una quantit� richiesta maggiore di 0.','Almeno un elemento dovrebbe avere una quantit� richiesta maggiore di 0.','E',7,'U')
INSERT INTO err_list_s (ID,  Message, Description, Level, Languageid,ErrorType) VALUES (547,'Errore non � possibile creare e-mail','Errore non � possibile creare e-mail','E',7,'U')
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)VALUES (548,'Nessuna opzione di entit� trovata!','Nessuna opzione di entit� trovata!','E',7,'U')
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)VALUES (549,'L''opzione selezionata personalizzata non � collegata con alcuna conferenza.','L''opzione selezionata personalizzata non � collegata con alcuna conferenza.','E',7,'U')
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)VALUES (605,'Immetti il numero telefonico','Immetti il numero telefonico','E',7,'U')
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)VALUES (606,'Nessuna immissione di e-mail per richiesta d''aiuto trovata.','Nessuna immissione di e-mail per richiesta d''aiuto trovata.','E',7,'U')
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)VALUES (517,'Criterio di ricorrenza non valido','Criterio di ricorrenza non valido','E',7,'U')
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)VALUES (532,'ID utente o ID Conferenza non � valido!','ID utente o ID Conferenza non � valido!','S',7,'U')
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)VALUES (533,'Nessun Reparto Trovato!','Nessun Reparto Trovato!','S',7,'U')
INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)VALUES (535,'La conferenza � associata con il set d''inventariazione in quantit� superiore a quello indicato!','La conferenza � associata con il set d''inventariazione in quantit� superiore a quello indicato!','E',7,'U')

Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (335,'Duplicate Address.Please enter another address','Duplicate Address.Please enter another address','E',7,'U')
insert into Err_list_s values(265,'The requested Room Email Queue has already been taken. Please choose another email.','Duplicate email name for this location','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) values (550,'Room is linked with conference.Cannot deactivated','Room is linked with conference.Cannot deactivated','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) values (604,'Conference already deleted or Expired','Conference already deleted or Expired','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) values (607,'Can not enable PC module, license count exceeded.', 'Can not enable PC module, license count exceeded.','E',7,'U')
Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) values (608,'Currently conferences are linked with PC attendees,please contact administrator for further assistance. ', 'Attualmente conferenze sono collegate con i partecipanti per PC, si prega di contattare l''amministratore per ulteriore assistenza.','E',7,'U')


/* *************************** FB 2390 - start *************************** */

Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) 
values (610, 'Point-To-Point Conferenza deve avere un chiamante e il chiamato assegnare. ',
 'Point-To-Point Conferenza deve avere un chiamante e il chiamato assegnare.','E',7,'U')

/* *************************** FB 2390 - end *************************** */



/************************** * FB 2398(V2.6) - Starts * *****************************/

Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) 
values (611, 'Please enter a valid conference start date / time.This might be due to default setup-time in Organization Options.',
'Please enter a valid conference start date / time.This might be due to default setup-time in Organization Options.','E',7,'U')


Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) 
values (612, 'Maximum Limit is 24 hours including buffer period.This might be due to default buffer in Organization Options.',
'Maximum Limit is 24 hours including buffer period.This might be due to default buffer in Organization Options.','E',7,'U')


/************************** * FB 2398(V2.6) - End * *****************************/


--FB 2261 and Fb 2440

INSERT INTO [dbo].[Err_List_S]
           ([ID]
           ,[Message]
           ,[Description]
           ,[Level]
           ,[Languageid]
           ,[ErrorType])
     VALUES
           (613 ,'Setup time cannot be less than the MCU pre start time.'
           ,'Tempo di preparazione non pu� essere inferiore al tempo di MCU di inizio pre.'
           ,'E'
           ,7
           ,'U')

INSERT INTO [dbo].[Err_List_S]
           ([ID]
           ,[Message]
           ,[Description]
           ,[Level]
           ,[Languageid]
           ,[ErrorType])
     VALUES
           (614 ,'Teardown time cannot be less than the MCU pre end time.'
           ,'Teardown tempo non pu� essere inferiore al tempo di fine pre MCU.'
           ,'E'
           ,7
           ,'U')

/** FB 2426 FOR ITALY*******/


Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) 
values (615, 'Si prega di eliminare Camere per ridurre i count.As ci sono le camere degli ospiti pi� attivi.',
'Si prega di eliminare Camere per ridurre i count.As ci sono le camere degli ospiti pi� attivi.','E',7,'U')


Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) 
values (616, 'Camera non viene creata come limite Camera per licenza utente supera.',
'Camera non viene creata come limite Camera per licenza utente supera.','E',7,'U')


Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) 
values (617, 'Camera non viene creata come Ambiente di licenza Guest limite superato.',
'Camera non viene creata come Ambiente di licenza Guest limite superato.','E',7,'U')



Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) 
values (618, 'Camera degli ospiti supera il limite di licenza VRM.',
'Camera degli ospiti supera il limite di licenza VRM.','E',7,'U')


Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) 
values (619, 'Camera degli ospiti limite per utente supera il limite Camera.',
'Camera degli ospiti limite per utente supera il limite Camera.','E',7,'U')


/*   ********* FB 2448 Starts ************** */
Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) 
values (620, 'Solo pernottamento VMR pu� essere selezionata per questa conferenza.',
'Solo pernottamento VMR pu� essere selezionata per questa conferenza.','E',7,'U')

Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) 
values (621, 'Si prega di selezionare una sola Camera VMR.',
'Si prega di selezionare una sola Camera VMR.','E',7,'U')

Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) 
values (622, 'Si prega di deselezionare Camere VMR.',
'Si prega di deselezionare Camere VMR.','E',7,'U')

/*   ********* FB 2448 Starts ************** */

/*   ********* FB 2426 ************** */

Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) 
values (623, 'Camera Permessi solo per audio e tipologia Conferenza Video.',
'Camera Permessi solo per audio e tipologia Conferenza Video.','E',7,'U')

/* ************************ FB 2486 Starts (09 Aug 2012)************************  */


insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (7, 'Max Allowed Standard MCU', 'Max ammessi standard MCU')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (7, 'Max Allowed Enhanced MCU', 'Max ammessi avanzato MCU')

Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) values (624, 'Avanzato MCU supera il limite di licenza VRM. ','Avanzato MCU supera il limite di licenza VRM.','E',7,'U')

Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) values (625, 'Verifica della licenza non riuscita:avanzato MCU supera il limite Standard MCU.', 'Verifica della licenza non riuscita:avanzato MCU supera il limite Standard MCU.','E',7,'U')

Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) values (626, 'avanzato MCU supera il limite Standard MCU.', 'avanzato MCU supera il limite Standard MCU.','E',7,'U')

Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) 
values (551, 'Errore durante l''inserimento di dettagli di ricorrenza.',
'Error in inserting Recurrence details.','E',7,'U')

Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) 
values (552, 'Errore nell''inserimento paramerters AdvancedAduio.',
'Error in inserting AdvancedAduio paramerters.','E',7,'U')

Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) 
values (553, 'Errore durante l''aggiunta di dettagli utente.',
'Error in adding User details.','E',7,'U')

Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) 
values (554, 'Errore durante l''inserimento della Conferenza attributi personalizzati record.',
'Error in inserting Conference Custom Attributes records.','E',7,'U')

Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) 
values (555, 'Errore durante l''inserimento della Conferenza record sovrapposizione di messaggi.',
'Error in inserting Conference Message Overlay records.','E',7,'U')

/* ************************ FB 2486 Ends (09 Aug 2012)************************  */


/*  **************** FB 2506 Starts (2012-08-29) ****************** */
Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) 
values (556, 'Errore durante la creazione Messaggio Conferenza per l''organizzazione.',
'Error in creating Conference Message for the organization.','E',7,'U')
/*  **************** FB 2506 Ends (2012-08-29) ****************** */


-- ****************************** FB 2501 StartMode - Starts - 13th Sep 2012 ******************************************
Insert into Err_List_S values(557,'Modalit� di avvio non valido.','Modalit� di avvio non valido.','E',7,'U')
-- **************************** FB 2501 StartMode - Ends - 13th Sep 2012*********************************************//

--***************************** FB 2501_ Overwrite of Meeting Requestor name(27th Sep 2012)**************************--
Insert into Err_List_S values(627,'missing.Operation loginUserID abbandonare o loginUserID non in dotazione.','missing.Operation loginUserID abbandonare o loginUserID non in dotazione.','E',7,'U')
--***************************** FB 2501_ Overwrite of Meeting Requestor name(27th Sep 2012)**************************--

--***************************** FB 2537 and FB 2517 - Starts (16th Oct 2012)**************************--
Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (558,'Licenza non riuscito: il limite Endpoint superato.','Licenza non riuscito: il limite Endpoint superato.','E',7,'U')
Insert into Err_List_S values(628,'Si prega di eliminare MCU migliorato per ridurre il numero.Come ci sono pi� attivi MCU Enhanced.','Si prega di eliminare MCU migliorato per ridurre il numero.Come ci sono pi� attivi MCU Enhanced.','E',7,'U')
--***************************** FB 2537 and FB 2517 - Ends (16th Oct 2012)**************************--



/* ************************ FB 2539 ************************  */


Insert into Err_List_S values(629,'User not authorized as Assistant-in-charge.','User not authorized as Assistant-in-charge.','E',7,'U')

Insert into Err_List_S values(630,'User not authorized as VNOC Operator.','User not authorized as VNOC Operator.','E',7,'U')

Insert into Err_List_S values(631,'User not authorized as Guest Room Assistant-in-charge.','User not authorized as Guest Room Assistant-in-charge.','E',7,'U')

Insert into Err_List_S values(632,'User not authorized as Approver.','User not authorized as Approver.','E',7,'U')

--FB 2609
Insert into Err_List_S values(633,'Conference schedule time is less than Meet and Greet buffer.','Conference schedule time is less than Meet and Greet buffer.','E',7,'U')


--FB 2609 - 07thFeb 2013
Update Err_List_S set Message = 'We�re sorry, but the request for Meet and Greet support does not meet the minimum notice requirement. Please choose another time for the conference, or deselect the Meet and Greet request to complete this conference schedule.', 
Description = 'We�re sorry, but the request for Meet and Greet support does not meet the minimum notice requirement. Please choose another time for the conference, or deselect the Meet and Greet request to complete this conference schedule.' where ID = 633 and Languageid = 7

--FB 2632 - 07thFeb 2013
Insert into Err_List_S values(634,'Please select Dedicated VNOC Operator.','Please select Dedicated VNOC Operator.','E',7,'U')

--FB 2626 - 11thFeb 2013


Insert into Err_List_S values(635,'The item is linked with a Inventory/Catering/Housekeeping Work orders. Cannot delete!','The item is linked with a Inventory/Catering/Housekeeping work orders. Cannot delete!','E',7,'U')
Insert into Err_List_S values(636,'Please associate at least one Menu with this Provider.','>Please associate at least one Menu with this Provider.','E',7,'U')


--FB 2599 - Vidyo - 21stFeb 2013
Insert into Err_List_S values(637,'License Check FAILED:Please reduce organizations limit count to 1.','License Check FAILED:Please reduce Max.Organizations count to 1.','E',7,'U')

Insert into Err_List_S values(638,'License Check FAILED:Cloud should be 1 or 0.','License Check FAILED:Cloud should be 1 or 0.','E',7,'U')


--FB 2594 - WhyGo - 21stFeb 2013
Insert into Err_List_S values(639,'License Check FAILED:Public Room Service should be 1 or 0.','License Check FAILED:Public Room Service should be 1 or 0.','E',7,'U')

Insert into Err_List_S values(640,'License Check FAILED:Public Rooms are linked with conference.','License Check FAILED:Public Rooms are linked with conference.','E',7,'U')

Insert into Err_List_S values(641,'License Check FAILED:Public Endpoint is link with Private Room.','License Check FAILED:Public Endpoint is link with Private Room.','E',7,'U')

Insert into Err_List_S values(642,'Public Rooms are linked with conference.Cannot remove!','Public Rooms are linked with conference.Cannot remove!','E',7,'U')

Insert into Err_List_S values(643,'Public Endpoint is link with Private Room.Cannot remove!','Public Endpoint is link with Private Room.Cannot remove!','E',7,'U')

--FB 2586 & FB 2531- VMR room ENHANCEMENT & Issue in setting License- Start 23stFeb 2013

Insert into Err_List_S values(655,'VMR Rooms limit exceeds VRM license.','VMR Rooms limit exceeds VRM license.','E',7,'U')

Insert into Err_List_S values(656,'VMR rooms limit exceeded.','VMR rooms limit exceeded.','E',7,'U')

Insert into Err_List_S values(657,'Please deactivate VMR rooms to reduce the count.As there are more active VMR rooms.','Please deactivate VMR rooms to reduce the count.As there are more active VMR rooms.','E',7,'U')



--- validation for video room limits--
 insert into Err_List_S (ID,  Message, Description, Level,Languageid,Errortype) values
 (653,'License Check FAILED: Increase video room limit','License Check FAILED : Increase video room limit','E',7,'U')
 
--- validation for non-video room limits-- 
 insert into Err_List_S (ID,  Message, Description, Level,Languageid,Errortype) values
 (650,'License Check FAILED: Increase non-video room limit','License Check FAILED : Increase non-video room limit','E',7,'U')

--validation for vmr rooms limit---

insert into Err_List_S (ID,  Message, Description, Level,Languageid,Errortype) values
(658,'License Check FAILED: Increase VMR rooms limit','License check FAILED : Increase VMR rooms limit','E',7,'U')
  
 ---- validation for user limits---
 insert into Err_List_S (ID,  Message, Description, Level,Languageid,Errortype) values
 (651,'License Check FAILED : Increase user limit', 
 'License Check FAILED : Increase user limit','E',7,'U')
 
 ---- validation for MCU limits---
 insert into Err_List_S (ID,  Message, Description, Level,Languageid,Errortype) values
 (652,'License Check FAILED : Increase MCU limit','License Check FAILED : Increase MCU limit','E',7,'U')
  
 
 ---- validation for Guest room limit ----
 insert into Err_List_S (ID,  Message, Description, Level,Languageid,Errortype) values
 (654,'License Check FAILED : Increase guest room limit','License Check FAILED : Increase guest room limit','E',7,'U')
 

 ---- validation for Enhanced MCU Limit ---
 insert into Err_List_S (ID,  Message, Description, Level,Languageid,Errortype) values
 (659,'License Check FAILED : Increase enhanced MCU limit','License Check FAILED : Increase enhanced MCU Limit','E',7,'U')
 

 ---- validation for GuestRoomLimit Per user Exceeds ----
 insert into Err_List_S (ID,  Message, Description, Level,Languageid,Errortype) values
 (660,'License Check FAILED : Increase GuestRoomLimit Per User Limit','License Check FAILED : Increase GuestRoomLimit Per User Limit','E',7,'U')
 

 ---- validation for Endpoint Limit ---
 insert into Err_List_S (ID,  Message, Description, Level,Languageid,Errortype) values
 (661,'License Check FAILED : Increase endpoint limit','License Check FAILED : Increase endpoint limit','E',7,'U')
 
 ---- validation for Outlook User Limit ---
 insert into Err_List_S (ID,  Message, Description, Level,Languageid,Errortype) values
 (662,'License Check FAILED : Increase Outlook User Limit','License Check FAILED : Increase Outlook User Limit','E',7,'U')
 
 ---- validation for Notes User Limit ---
 insert into Err_List_S (ID,  Message, Description, Level,Languageid,Errortype) values
 (663,'License Check FAILED : Increase Notes User Limit','License Check FAILED : Increase Notes User Limit','E',7,'U')
 
 ---- validation for Mobile User Limit ---
 insert into Err_List_S (ID,  Message, Description, Level,Languageid,Errortype) values
 (664,'License Check FAILED : Increase Mobile User Limit','License Check FAILED : Increase Mobile User Limit','E',7,'U')
 
---- validation for Total all User Limit ---
 insert into Err_List_S (ID,  Message, Description, Level,Languageid,Errortype) values
 (644,'License Check FAILED : Increase all user limit','License Check FAILED : Increase all user limit','E',7,'U')
 
 ---- validation for Facility Module Count ---
 insert into Err_List_S (ID,  Message, Description, Level,Languageid,Errortype) values
 (645,'License Check FAILED : Facility Module is in use.Please increase count',
 'License Check FAILED : Facility Module is in use.Please increase count','E',7,'U')
 
 
---- validation for House Keeping Module Count ---
 insert into Err_List_S (ID,  Message, Description, Level,Languageid,Errortype) values
 (646,'License Check FAILED : Facility Services Module is in use.Please increase count',
 'License Check FAILED : Facility Services Module is in use.Please increase count','E',7,'U')
 
 ---- validation for Catering Module Count ---
 insert into Err_List_S (ID,  Message, Description, Level,Languageid,Errortype) values
 (647,'License Check FAILED : Catering Module is in use.Please increase count',
 'License Check FAILED : Catering Module is in use.Please increase count','E',7,'U')
 
  ---- validation for API Module Count ---
 insert into Err_List_S (ID,  Message, Description, Level,Languageid,Errortype) values
 (648,'License Check FAILED : API module is in use.Please increase count',
 'License Check FAILED : API module is in use.Please increase count','E',7,'U')
 
   ---- validation for PC Module Count ---
 insert into Err_List_S (ID,  Message, Description, Level,Languageid,Errortype) values
 (649,'License Check FAILED : PC module is in use.Please increase count',
 'License Check FAILED : PC module is in use.please increase count','E',7,'U')

--FB 2586 & FB 2531- VMR room ENHANCEMENT & Issue in setting License- End23stFeb 2013

--FB 2636
Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) values
(650,N'No E.164 Service found.Please add alteast one E.164 Service.',N'No E164 Service.Please add alteast one E164 Service.','E',7,'U')
namespace NS_CONFIG
{
	#region References
	using System;
	using System.Xml;
	using System.Diagnostics;
	using System.IO;
	#endregion 

	class Config 
	{
		public Config ()
		{
			// constructor	
		}

		public bool Initialize (string configPath,ref NS_MESSENGER.ConfigParams configParams,ref string errMsg, String appConfig)
		{			
			configParams.localConfigPath = configPath;
			bool ret = Read_Local_Config_File(ref configParams,ref errMsg,appConfig);


			if (!ret) return (false);

			return(true);
		}


        private bool Read_Local_Config_File(ref NS_MESSENGER.ConfigParams configParams, ref string errMsg, String appConfig)
		{
			// Read from XML config file 
            String time = "";
            Int32 timeinms = 0;
            string vrmMaintserviceLog = "MaintenanceLogs\\VRMMaintServiceLog";
            string vrmMaintserviceconfig = "VRMConfig.xml";
			try 
			{ 
				// Open an XML file 
				XmlDocument xmlConfig = new XmlDocument();
				xmlConfig.Load(configParams.localConfigPath);

				XmlNode node;

				node = xmlConfig.SelectSingleNode("//VRMMaintServiceConfig/VRM_Config_File_Path");
                if (appConfig != "")
                {
                    node.InnerXml = appConfig + vrmMaintserviceconfig;
                }
				configParams.globalConfigPath = node.InnerXml.Trim();
				
				node = xmlConfig.SelectSingleNode("//VRMMaintServiceConfig/Log_File_Path");

                if (appConfig != "")
                {
                    node.InnerXml = appConfig + vrmMaintserviceLog;
                }

				configParams.logFilePath = node.InnerXml.Trim();				

				node = xmlConfig.SelectSingleNode("//VRMMaintServiceConfig/Debug");
				string debug = node.InnerXml.Trim();
				if (debug.Equals("yes") || debug.Equals("Yes") || debug.Equals("YES"))
				{
					configParams.debugEnabled = true;
				}
				
				node = xmlConfig.SelectSingleNode("//VRMMaintServiceConfig/SiteURL");
				configParams.siteUrl = node.InnerXml.Trim();


                node = xmlConfig.SelectSingleNode("//VRMMaintServiceConfig/ActivationTimer");
                if (node != null)
                {
                    time = node.InnerXml.Trim();
                    if (time != "")
                    {
                        if(Int32.TryParse(time,out timeinms))
                        {
                            if (timeinms < 8 && timeinms > 0)
                            {
                                configParams.activationTimer = Convert.ToDouble(timeinms * 24 * 60 * 60 * 1000);
                            }

                        }
                    }
 
                }

                xmlConfig.Save(configParams.localConfigPath);

				
				return true;
			}
			catch (Exception e)
			{
				errMsg = "Error reading local config file. Exception Message : " + e.Message;
				return false;
			}			
		}

        
		

	}
}	
namespace NS_MESSENGER
{
	#region references
	using System;
	using System.Collections;
	#endregion 

	class ConfigParams
	{
		public string localConfigPath,globalConfigPath,logFilePath,siteUrl,reportFilePath;//FB 2363		
		public string databaseLogin,databaseServer,databasePwd,databaseName;		
		public bool debugEnabled;
        public double activationTimer;
		public ConfigParams()
		{
            activationTimer = Convert.ToDouble(24 * 60 * 60 * 1000);
			debugEnabled = false;
		}
	}
}
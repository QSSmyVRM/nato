using System;
using System.CodeDom;
using System.Xml;

namespace myVRM.ExcelXmlWriter
{
	public interface IWriter
	{
		// Methods
		void WriteXml(XmlWriter writer);
	}

	internal interface IReader
	{
		// Methods
		void ReadXml(XmlElement element);
	}

	public interface ICodeWriter
	{
		// Methods
		void WriteTo(CodeTypeDeclaration type, CodeMemberMethod method, CodeExpression targetObject);
	}
}

using System;
using System.Xml;

namespace myVRM.ExcelXmlWriter.Schemas
{
	public sealed class RowsetData : IWriter
	{
		// Fields
		private RowsetRowCollection _rows;

		// Methods
		void IWriter.WriteXml(XmlWriter writer)
		{
			writer.WriteStartElement("rs", "data", "urn:schemas-microsoft-com:rowset");
			if (this._rows != null)
			{
				((IWriter) this._rows).WriteXml(writer);
			}
			writer.WriteEndElement();
		}

		// Properties
		public RowsetRowCollection Rows
		{
			get
			{
				if (this._rows == null)
				{
					this._rows = new RowsetRowCollection();
				}
				return this._rows;
			}
		}
	}


}

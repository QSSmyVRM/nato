﻿<%@ Page Language="C#" Inherits="ns_ConferenceSetup.ConferenceSetup" validateRequest="false" %>
<%@ Register Assembly="MetaBuilders.WebControls.ComboBox" Namespace="MetaBuilders.WebControls" TagPrefix="mbcbb" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<!-- FB 2426 -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">  <!-- FB 2050 -->
<meta http-equiv="X-UA-Compatible" content="IE=7" /> <!-- FB 2050 -->
<!-- #INCLUDE FILE="inc/maintopNET.aspx" --> 

<style type="text/css">
#TopMenu div 
{
	width:123px; /* FB 2050 */
	height:35px;
}
</style>

<script type="text/javascript">
  var servertoday = new Date();
  
//  Merging Recurrence - start
  var dFormat;
    dFormat = "<%=format %>";
  
  var servertoday = new Date(parseInt("<%=DateTime.Today.Year%>", 10), parseInt("<%=DateTime.Today.Month%>", 10)-1, parseInt("<%=DateTime.Today.Day%>", 10),
        parseInt("<%=DateTime.Today.Hour%>", 10), parseInt("<%=DateTime.Today.Minute%>", 10), parseInt("<%=DateTime.Today.Second%>", 10)); 
 
  var  maxDuration = 24; //Buffer zone
  if( "<%=Application["MaxConferenceDurationInHours"] %>" != "")
     maxDuration = parseInt("<%=Application["MaxConferenceDurationInHours"] %>",10); 
//  Merging Recurrence - end

//FB 2426 Start

	function ValidateflyEndpoints()
	{
	  var ret = true;
	  ValidatorEnable(document.getElementById("reqRoomName"), true);
      ValidatorEnable(document.getElementById("regRoomName"), true);
      ValidatorEnable(document.getElementById("reqcontactName"), true);  
      ValidatorEnable(document.getElementById("cmpIPValPwd1"), true);
      ValidatorEnable(document.getElementById("cmpIPValPwd2"), true);
      ValidatorEnable(document.getElementById("cmpSIPValPwd1"), true);
      ValidatorEnable(document.getElementById("cmpSIPValPwd2"), true);
      ValidatorEnable(document.getElementById("cmpISDNValPwd1"), true);
      ValidatorEnable(document.getElementById("cmpISDNValPwd2"), true);
              
	  var txtIPAdd = document.getElementById("txtIPAddress").value;
      var txtIPPwd = document.getElementById("txtIPPassword").value;
      var txtIPconfirmPwd = document.getElementById("txtIPconfirmPassword").value;
      var dropA1 = document.getElementById("lstIPlinerate");
      var lstIPlinerate = dropA1.options[dropA1.selectedIndex].value;
      var dropA2 = document.getElementById("lstIPConnectionType");
      var lstIPConType = dropA2.options[dropA2.selectedIndex].value;
      var isDefault1 = document.getElementById("radioIsDefault").checked;
	  
      if(isDefault1 == false && txtIPAdd == "" && txtIPPwd == "" && txtIPconfirmPwd == "" && lstIPlinerate == "-1" && lstIPConType == "-1")
      {
        fnDisableValidator1(false);
        ret = true;
      }
      else
      {
        fnDisableValidator1(true);
      }
	  
      var txtSIPAdd = document.getElementById("txtSIPAddress").value;
      var txtSIPPwd = document.getElementById("txtSIPPassword").value;
      var txtSIPconfirmPwd = document.getElementById("txtSIPconfirmPassword").value;
      var dropB1 = document.getElementById("lstSIPlinerate");
      var lstSIPlinerate = dropB1.options[dropB1.selectedIndex].value;
      var dropB2 = document.getElementById("lstSIPConnectionType");
      var lstSIPConType = dropB2.options[dropB2.selectedIndex].value;
	  var isDefault2 = document.getElementById("radioIsDefault2").checked;
      
	  if(isDefault2 == false && txtSIPAdd == "" && txtSIPPwd == "" && txtSIPconfirmPwd == "" && lstSIPlinerate == "-1" && lstSIPConType == "-1")
	  {
        fnDisableValidator2(false);
        ret = true;
      }
      else
      {
        fnDisableValidator2(true);
	  }
	  
      var txtISDNAdd = document.getElementById("txtISDNAddress").value;
      var txtISDNPwd = document.getElementById("txtISDNPassword").value;
      var txtISDNconfirmPwd = document.getElementById("txtISDNconfirmPassword").value;
      var dropC1 = document.getElementById("lstISDNlinerate");
      var lstISDNlinerate = dropC1.options[dropC1.selectedIndex].value;
      var dropC2 = document.getElementById("lstISDNConnectionType");
      var lstISDNConType = dropC2.options[dropC2.selectedIndex].value;
	  var isDefault3 = document.getElementById("radioIsDefault3").checked;
	  
	  if(isDefault3 == false && txtISDNAdd == "" && txtISDNPwd == "" && txtISDNconfirmPwd == "" && lstISDNlinerate == "-1" && lstISDNConType == "-1")
	  {
        fnDisableValidator3(false);
        ret = true;
      }
      else
      {
        fnDisableValidator3(true);
	  }
	  
	   if (!Page_ClientValidate())
               return Page_IsValid;
               
	  return ret;
	}
	
	function deleteAssistant()
    {
        document.getElementById("txtApprover5").value = "";
        document.getElementById("txtEmailId").value = "";
    }
      //FB 2486
    function toggle() 
    {
        //alert("hai");
	    var ele = document.getElementById("toggleText");
	    var text = document.getElementById("displayText");
	    if(ele.style.display == "block")
	    {
    		ele.style.display = "none";
		    text.innerHTML = "より多くの";
  	    }
	    else 
	    {
		    ele.style.display = "block";
		    text.innerHTML = "以下";
	    }
    } 
    function fnClearGuestGrid()
     {
        document.getElementById("txtsiteName").value = "";
        document.getElementById("txtApprover5").value = "";
        document.getElementById("txtEmailId").value = "";
        document.getElementById("txtPhone").value = "";

        document.getElementById("txtAddress").value = "";
        document.getElementById("txtState").value = "";
        document.getElementById("txtCity").value = "";
        document.getElementById("txtZipcode").value = "";
        document.getElementById("lstCountries").selectedIndex = 1;

        document.getElementById("txtIPAddress").value = "";
        document.getElementById("txtIPPassword").value = "";
        document.getElementById("txtIPconfirmPassword").value = "";
        document.getElementById("lstIPlinerate").selectedIndex = 0;
        document.getElementById("lstIPConnectionType").selectedIndex = 0;
        
        document.getElementById("txtSIPAddress").value = "";
        document.getElementById("txtSIPPassword").value = "";
        document.getElementById("txtSIPconfirmPassword").value = "";
        document.getElementById("lstSIPlinerate").selectedIndex = 0;
        document.getElementById("lstSIPConnectionType").selectedIndex = 0;
        
        document.getElementById("txtISDNAddress").value = "";
        document.getElementById("txtISDNPassword").value = "";
        document.getElementById("txtISDNconfirmPassword").value = "";
        document.getElementById("lstISDNlinerate").selectedIndex = 0;
        document.getElementById("lstISDNConnectionType").selectedIndex = 0;
        
        document.getElementById("radioIsDefault").checked = true;
        document.getElementById("radioIsDefault2").checked = false;
        document.getElementById("radioIsDefault3").checked = false;
        
        document.getElementById("btnGuestLocationSubmit").value = "送信";
        document.getElementById("hdnGuestRoom").value = "-1";
    }
    
    function fnValidator()
    {
        fnClearGuestGrid();
        document.getElementById("reqRoomName").enabled = "true";
        document.getElementById("regRoomName").enabled = "true";
        document.getElementById("reqcontactName").enabled = "true";
       
        document.getElementById("reqIPAddress").enabled = "true";
        document.getElementById("regIPAddress").enabled = "true";
        document.getElementById("reqIPlinerate").enabled = "true";
        document.getElementById("reqIPConnectionType").enabled = "true";
        document.getElementById("cmpIPValPwd1").enabled = "true";
        document.getElementById("cmpIPValPwd2").enabled = "true";
        
        document.getElementById("reqSIPAddress").enabled = "true";
        document.getElementById("regSIPAddress").enabled = "true";
        document.getElementById("reqSIPlinerate").enabled = "true";
        document.getElementById("reqSIPConnectionType").enabled = "true";
        document.getElementById("cmpSIPValPwd1").enabled = "true";
        document.getElementById("cmpSIPValPwd2").enabled = "true";
        
        document.getElementById("reqISDNAddress").enabled = "true";
        document.getElementById("regISDNAddress").enabled = "true";
        document.getElementById("reqISDNlinerate").enabled = "true";
        document.getElementById("reqISDNConnectionType").enabled = "true";
        document.getElementById("cmpISDNValPwd1").enabled = "true";
        document.getElementById("cmpISDNValPwd2").enabled = "true";
    }

    function fnDisableValidator()
    {
        
        ValidatorEnable(document.getElementById("reqRoomName"), false);
        ValidatorEnable(document.getElementById("regRoomName"), false);
        ValidatorEnable(document.getElementById("reqcontactName"), false);
        ValidatorEnable(document.getElementById("cmpIPValPwd1"), false);
        ValidatorEnable(document.getElementById("cmpIPValPwd2"), false);
        ValidatorEnable(document.getElementById("cmpSIPValPwd1"), false);
        ValidatorEnable(document.getElementById("cmpSIPValPwd2"), false);
        ValidatorEnable(document.getElementById("cmpISDNValPwd1"), false);
        ValidatorEnable(document.getElementById("cmpISDNValPwd2"), false);
        fnDisableValidator1(false);
        fnDisableValidator2(false);
        fnDisableValidator3(false);
    }

    function fnDisableValidator1(par1)
    {
        ValidatorEnable(document.getElementById("reqIPAddress"), par1);
        ValidatorEnable(document.getElementById("regIPAddress"), par1);
        ValidatorEnable(document.getElementById("reqIPlinerate"), par1);
        ValidatorEnable(document.getElementById("reqIPConnectionType"), par1);
    }

    function fnDisableValidator2(par2)
    {
        ValidatorEnable(document.getElementById("reqSIPAddress"), par2);
        ValidatorEnable(document.getElementById("regSIPAddress"), par2);
        ValidatorEnable(document.getElementById("reqSIPlinerate"), par2);
        ValidatorEnable(document.getElementById("reqSIPConnectionType"), par2);
    }

    function fnDisableValidator3(par3)
    {
        ValidatorEnable(document.getElementById("reqISDNAddress"), par3);
        ValidatorEnable(document.getElementById("regISDNAddress"), par3);
        ValidatorEnable(document.getElementById("reqISDNlinerate"), par3);
        ValidatorEnable(document.getElementById("reqISDNConnectionType"), par3);
    }

    //FB 2426 End


// FB 2359 Start
   //Dan Disney Requirement for audio addon start
	function fnShowHideAVLink()
	{
	    var args = fnShowHideAVLink.arguments;
	    var obj = eval(document.getElementById("LnkAVExpand"));
	    
	    if(obj)
	    {
	        obj.style.display = 'none';
	        if(args[0] == '1')
	        {
	            obj.style.display = '';
	        }
	    }
	}
	
	function fnShowAVParams()
	{
	    var obj = eval(document.getElementById("trAVCommonSettings"));
	    
	    var linkState = eval(document.getElementById("hdnAVParamState"));
	    var expandlink = eval(document.getElementById("LnkAVExpand"));
	    
	    if(linkState)
	    {
	        if(obj)
	        {
	            obj.style.display = 'none';
	            if(linkState.value == '')
	            {
	                obj.style.display = '';
	                linkState.value = '1';
	                
	                if(expandlink)
	                {
	                    expandlink.innerText = 'て収縮';
	                }
	            }
	            else
	            {
	                obj.style.display = 'none';
	                
	                linkState.value = '';
	                if(expandlink)
	                {
	                    expandlink.innerText = '拡大する';
	                }
	            }
	        }
	     }
	     return false;
	}
	//Dan Disney Requirement for audio addon end
	//FB 2359 End
	
	function fnShowHideMeetLink()
	{
	    var args = fnShowHideMeetLink.arguments;
	    var obj = eval(document.getElementById("LnkMeetExpand"));
	    
	    if(obj)
	    {
	        obj.style.display = 'none';
	        if(args[0] == '1')
	        {
	            obj.style.display = '';
	        }
	    }
	}
	
	function fnShowMeetPlanner()
	{
	    var obj = eval(document.getElementById("tdMeetingPlanner"));
	    var linkState = eval(document.getElementById("hdnMeetLinkSt"));
	    var expandlink = eval(document.getElementById("LnkMeetExpand"));
	    
	    if(linkState)
	    {
	        if(obj)
	        {
	            obj.style.display = 'none';
	            if(linkState.value == '')
	            {
	                obj.style.display = '';
	                linkState.value = '1';
	                
	                if(expandlink)
	                {
	                    expandlink.innerText = 'て収縮';
	                }
	            }
	            else
	            {
	                obj.style.display = 'none';
	                linkState.value = '';
	                if(expandlink)
	                {
	                    expandlink.innerText = '拡大する';
	                }
	            }
	        }
	     }
	     return false;
	}
// FB 1985 End
    //FB 2501 Starts
    function deleteApprover(id)
    {
	    eval("document.getElementById('hdnApprover" + (id) + "')").value = "";
	    eval("document.getElementById('txtApprover" + (id) + "')").value = "";
    }
    //FB 2501 Ends
    
</script>
<script runat="server">
    protected void Menu1_MenuItemClick(object sender, MenuEventArgs e)
    {
        int index = Int32.Parse(e.Item.Value);
        Page.Validate();
        //Response.Write(index);
        if (Page.IsValid)
        {
           
            TopMenu.Items[Int32.Parse(e.Item.Value)].Selected = true;
            Wizard1.ActiveViewIndex = index;
        }
        else
        {
            TopMenu.Items[Int32.Parse(e.Item.Value)].Selected = false;
            TopMenu.Items[Wizard1.ActiveViewIndex].Selected = true;
        }

        //Merging Recurrence - start
        if (index > 0)
        {   
            if (hdnRecurValue.Value == "R" && Recur.Value == "")
            {
                errLabel.Text = "繰り返しパターンを選択してください。";
                Wizard1.ActiveViewIndex = 0;
                TopMenu.Items[Wizard1.ActiveViewIndex].Selected = true;
                index = 0;
            }
        }

        hdnValue.Value = index.ToString();
        //Merging Recurrence - end

        /*if (!ValidateSetupTearDownTime())   //buffer zone
        {
            errLabel.Visible = true;
            TopMenu.Items[0].Selected = true;
            Wizard1.ActiveViewIndex = 0;
        }*/
       
        
        if (index.Equals(Wizard1.Views.Count - 1))
        {
            btnPrev.Visible = false;
            btnNext.Visible = false;
        }
        else
        {
            btnNext.Visible = true;
            btnPrev.Visible = true;
        }

        if (index == 0)
            btnPrev.Visible = false; //FB 2516
        
        //Response.Write(index + " : " + Wizard1.Views.Count);
        if (index.Equals(Wizard1.Views.Count - 1))
            LoadPreview();
        if (e.Item.Text.IndexOf("ケータリング") > 0)
        {
            LoadCateringWorkorders();
        }
        //FB 1985
        //if (e.Item.Text.IndexOf("Audio/Video") > 0) 
        if (e.Item.Text.IndexOf("オーディオ") > 0)
        {
            UpdateAdvAVSettings(new object(), new EventArgs());
        }
        // FB Case 718: Saima hiding the conflict datagrid when navigating away from preview screen after it is bbeen populated.
        if (Wizard1.ActiveViewIndex.Equals(Wizard1.Views.Count - 1) && (dgConflict.Items.Count > 0) && (dgConflict.Visible.Equals(true)))
        {
            dgConflict.DataSource = null;
            dgConflict.DataBind();
            dgConflict.Visible = false;
            tblConflict.Visible = false;
            btnConfSubmit.Text = "会議を送信する";
        }
        
        //FB 1865
        if (hdnParty.Value != "" && txtPartysInfo.Text.Trim() == "")
            txtPartysInfo.Text = hdnParty.Value;
       
    }
</script>

<%--Merging Recurrence start--%>
<script type="text/javascript" language="JavaScript" src="script/errorList.js"></script>
<script type="text/javascript" language="javascript1.1" src="extract.js"></script>
<%--Merging Recurrence end--%>
<script language="VBScript" src="script/lotus.vbs"></script>
<script type="text/javascript" src="inc/functions.js"></script>
<%--Merging Recurrence--%>
<%--<script type="text/javascript" src="script/cal.js"></script>--%>
<script type="text/javascript" src="script/mytreeNET.js"></script>
<script type="text/javascript" src="script/settings2.js"></script>
<script type="text/vbscript" src="script/settings2.vbs"></script>
<script type="text/javascript" src="script/mousepos.js"></script>

<%--Merging Recurrence start--%>
<script type="text/javascript" src="script/cal-flat.js"></script>
<script type="text/javascript" src="script/calview.js"></script>
<script type="text/javascript" src="lang/calendar-en.js"></script>
<script type="text/javascript" src="script/calendar-setup.js"></script>
<script language="VBScript" src="script/outlook.vbs"></script>
<script type="text/javascript" src="script/saveingroup.js"></script>
<%--<script type="text/javascript" src="script/group2.js"></script> - Code Commented For FB 1476 --%>
<script type="text/javascript" src="script/calendar-flat-setup.js"></script>
<%--Merging Recurrence end--%>
<script type="text/javascript" src="script/RoomSearch.js"></script><%--Room Search--%>

<script language="javascript">
		
			function callalert(val)
			{
				alert(val);
				return false;
			}
</script>


<script language="javascript">
//Merging Recurrence
function fnEnableBuffer()
 {
 var chkenablebuffer = document.getElementById("chkEnableBuffer");
 
 //FB 1911
 if(document.getElementById("RecurSpec").value  != "")
    return;
 
 /***  SJV BufferZone Fix ****/
    var chkrecurrence = document.getElementById("chkRecurrence");
 
 if(chkrecurrence && chkrecurrence.checked == true)
            {
    
             var confstdate = '';
            confstdate = GetDefaultDate(document.getElementById("confStartDate").value,'<%=format%>');
        
            var endTime = new Date(confstdate + " " + document.getElementById("confStartTime_Text").value);        
            //var apBfr = endTime.toLocaleTimeString().toString().split(" ")[1]; 
            var apBfr = endTime.format("tt"); //FB 2108
            var hrs = document.getElementById("RecurDurationhr").value;
            var mins = document.getElementById("RecurDurationmi").value;
            
            if(hrs == "")
                hrs = "0";
            
            if(mins == "")
                mins = "0"; 
            
            var timeMins = parseInt(hrs) * 60 ;
            
            timeMins = timeMins + parseInt(mins);
             
            endTime.setMinutes(endTime.getMinutes()  + parseInt(timeMins));
            //FB 2614
            var hh = endTime.getHours() ;// parseInt(endTime.toLocaleTimeString().toString().split(" ")[0].toString().split(":")[0],10);            
            if("<%=Session["timeFormat"]%>" == "1") //FB 2108
            {
                if(hh >= 12)
                    hh = hh - 12;
                    
                if(hh == 0)
                    hh = 12
            }
            if (hh < 10)
                hh = "0" + hh;
            //FB 2614
            var mm = endTime.getMinutes();// parseInt(endTime.toLocaleTimeString().toString().split(" ")[0].toString().split(":")[1],10);
            if (mm < 10)
                mm = "0" + mm;
            //var ap = endTime.toLocaleTimeString().toString().split(" ")[1];
            var ap = endTime.format("tt"); //FB 2108
            /*** Code changed for FB 1426 *****/
            //document.getElementById("confEndTime_Text").value = hh + ":" + mm + " " + ap;
            var evntime = parseInt(hh,10);
            
            if(evntime < 12)
                evntime = evntime + 12
                       
            if("<%=Session["timeFormat"]%>" == "0")
            {
                if(ap == "AM")
                {
                    if(hh == "12")
                        document.getElementById("confEndTime_Text").value = "00:" + mm ;
                    else
                        document.getElementById("confEndTime_Text").value = hh + ":" + mm ;
                    
                }
                else
                {
                    if(evntime == "24")
                        document.getElementById("confEndTime_Text").value = "12:" + mm ;
                    else
                        document.getElementById("confEndTime_Text").value = evntime + ":" + mm ;
                }
            }
            else
            {
                document.getElementById("confEndTime_Text").value = hh + ":" + mm + " " + ap;
            }
//Commented for FB 1714 - Starts            
//            document.getElementById("TearDownDate").value = document.getElementById("confEndDate").value;
//            document.getElementById("TeardownTime_Text").value = document.getElementById("confEndTime_Text").value;
//            
//             document.getElementById("SetupDate").value = document.getElementById("confStartDate").value;
//         document.getElementById("SetupTime_Text").value = document.getElementById("confStartTime_Text").value; //Edited For FF
//Commented for FB 1714 - End       
            }
            
            
 /***  SJV BufferZone Fix ****/
 
    if(chkenablebuffer && chkenablebuffer.checked)
    {
        document.getElementById("SDateText").innerHTML = "事前会議開始 <span class='reqfldstarText'>*</span>";
        document.getElementById("EDateText").innerHTML  = "事後会議終了 <span class='reqfldstarText'>*</span>";
        document.getElementById("NONRecurringConferenceDiv6").style.display = ""; // Edited for FF
        document.getElementById("NONRecurringConferenceDiv7").style.display = ""; // Edited for FF
        document.getElementById("SetupTime_Text").style.width = "75px"; //buffer zone
        document.getElementById("TeardownTime_Text").style.width = "75px"; //buffer zone
    }
    else
    {   
        document.getElementById("SDateText").innerHTML = "開始日/時間 <span class='reqfldstarText'>*</span>";     
        document.getElementById("EDateText").innerHTML = "終了日/時間 <span class='reqfldstarText'>*</span>";            
        document.getElementById("NONRecurringConferenceDiv6").style.display = "None";
        document.getElementById("NONRecurringConferenceDiv7").style.display = "None";
        
         document.getElementById("SetupDate").value = document.getElementById("confStartDate").value;
         
         document.getElementById("SetupTime_Text").value = document.getElementById("confStartTime_Text").value;//Edited for FF
        
        if(document.getElementById("Recur").value == "" && document.getElementById("hdnRecurValue").value == "")
        { 
            
                           
            document.getElementById("TearDownDate").value = document.getElementById("confEndDate").value;
            document.getElementById("TeardownTime_Text").value = document.getElementById("confEndTime_Text").value;//Edited for FF
        } 
          
    }
 }
 /***  SJV BufferZone Fix ****/
 function SetRecurBuffer()
 {
    var chkenablebuffer = document.getElementById("chkEnableBuffer");
    var chkrecurrence = document.getElementById("chkRecurrence");
    
            if(chkenablebuffer && chkenablebuffer.checked == false)
            {
    
            if(chkrecurrence && chkrecurrence.checked == true)
            {
    
             var confstdate = '';
            confstdate = GetDefaultDate(document.getElementById("confStartDate").value,'<%=format%>');
        
            var endTime = new Date(confstdate + " " + document.getElementById("confStartTime_Text").value);        
            //var apBfr = endTime.toLocaleTimeString().toString().split(" ")[1]; 
            var apBfr = endTime.format("tt"); //FB 2108
            
            var hrs = document.getElementById("RecurDurationhr").value;
            var mins = document.getElementById("RecurDurationmi").value;
            
            if(hrs == "")
                hrs = "0";
            
            if(mins == "")
                mins = "0"; 
            
            var timeMins = parseInt(hrs) * 60 ;
            
            timeMins = timeMins + parseInt(mins);
             
            endTime.setMinutes(endTime.getMinutes()  + parseInt(timeMins));
            //FB 2614
             var hh = endTime.getHours(); //parseInt(endTime.toLocaleTimeString().toString().split(" ")[0].toString().split(":")[0],10);  
             if("<%=Session["timeFormat"]%>" == "1") //FB 2108
            {
                if(hh >= 12)
                    hh = hh - 12;
                    
                if(hh == 0)
                    hh = 12
            }          
            
            if (hh < 10)
                hh = "0" + hh;
            //FB 2614
            var mm = endTime.getMinutes();// parseInt(endTime.toLocaleTimeString().toString().split(" ")[0].toString().split(":")[1],10);
            if (mm < 10)
                mm = "0" + mm;
            //var ap = endTime.toLocaleTimeString().toString().split(" ")[1];
            var ap = endTime.format("tt"); //FB 2108
            /*** Code changed for FB 1426 *****/
            //document.getElementById("confEndTime_Text").value = hh + ":" + mm + " " + ap;
            var evntime = parseInt(hh,10);
            
            if(evntime < 12)
                evntime = evntime + 12
                       
            if("<%=Session["timeFormat"]%>" == "0")
            {
                if(ap == "AM")
                {
                    if(hh == "12")
                        document.getElementById("confEndTime_Text").value = "00:" + mm ;
                    else
                        document.getElementById("confEndTime_Text").value = hh + ":" + mm ;
                    
                }
                else
                {
                    if(evntime == "24")
                        document.getElementById("confEndTime_Text").value = "12:" + mm ;
                    else
                        document.getElementById("confEndTime_Text").value = evntime + ":" + mm ;
                }
            }
            else
            {
                document.getElementById("confEndTime_Text").value = hh + ":" + mm + " " + ap;
            }
            
            document.getElementById("TearDownDate").value = document.getElementById("confEndDate").value;
            document.getElementById("TeardownTime_Text").value = document.getElementById("confEndTime_Text").value;
            
             document.getElementById("SetupDate").value = document.getElementById("confStartDate").value;
         document.getElementById("SetupTime_Text").value = document.getElementById("confStartTime_Text").value;
            
            }
            }
 }
 /***  SJV BufferZone Fix ****/
     
function randam() { 
   var totalDigits = 4;
   var n = Math.random()*10000;
   n = Math.round(n);
   n = n.toString(); 
      var pd = ''; 
      if (totalDigits > n.length) { 
         for (i=0; i < (totalDigits-n.length); i++) { 
            pd += '0'; 
         } 
      } 
   return pd + n.toString();
}

function num_gen() 
{
   var num = randam()
  //alert(num);
   if (num.indexOf(0) == 0) 
      num = num.replace(0,1)
  
    //FB 2244 - Starts
    if(document.getElementById("cmpValPassword") != null)
        document.getElementById("cmpValPassword").style.display = "none";
    if(document.getElementById("cmpValPassword1") != null)
        document.getElementById("cmpValPassword1").style.display = "none";
    if(document.getElementById("numPassword1") != null)
        document.getElementById("numPassword1").style.display = "none";

    var pass = document.getElementById("ConferencePassword")
    var pass1 = document.getElementById("ConferencePassword2")
    //if(pass != "")
    if(pass != null)
        pass.value = num;
    //if(pass1 != "")
    if(pass1 != null)
        pass1.value = num;
    //FB 2244 - End
     
   SavePassword(); 
} //fb 676 end 



    function UpdateCheckbox(obj)
    {
        if (obj.value == "")
        {
           document.getElementById(obj.id.replace("txtQuantity", "chkSelectedMenu")).checked = false;
           UpdatePrice(obj);
        }
        else
        {
            if (!isNaN(obj.value) && obj.value.indexOf(".") < 0)
            {
                document.getElementById(obj.id.replace("txtQuantity", "chkSelectedMenu")).checked = true;
                UpdatePrice(obj);
            }
            else
            {
                alert("無効な値");
                obj.value = "";
            }
        }
    }
    
    function UpdatePrice(obj)
    {
        var lblPrice = document.getElementById(obj.id.substring(0, obj.id.indexOf("_dgCateringMenus")) + "_lblPrice");
        var price = parseFloat("0.00"); //FB 1686
        var i = 2;
        
        while(document.getElementById(obj.id.substring(0, (obj.id.indexOf("Menus_ctl0") + 10)) + i + "_txtQuantity"))
        {
            if (i<10)
            {
                price += document.getElementById(obj.id.substring(0, (obj.id.indexOf("Menus_ctl0") + 10)) + i + "_txtPrice").value * document.getElementById(obj.id.substring(0, (obj.id.indexOf("Menus_ctl0") + 10)) + i + "_txtQuantity").value;
            }
            else
            {
                price += document.getElementById(obj.id.substring(0, (obj.id.indexOf("Menus_ctl") + 9)) + i + "_txtPrice").value * document.getElementById(obj.id.substring(0, (obj.id.indexOf("Menus_ctl") + 9)) + i + "_txtQuantity").value;
            }
            i++;
        }
        // WO Bug Fix
        lblPrice.innerHTML = ((price * 100.00) / 100.00).toFixed(2);
        //FB 1830
        if("<%=cFormat%>" == "€")
            lblPrice.innerHTML = (((price * 100.00) / 100.00).toFixed(2)).replace(","," ").replace(".",",");
    }

    function ShowItems(obj)
    {
        getMouseXY();
        var str = document.getElementById(obj.id.substring(0, obj.id.lastIndexOf("_")) + "_dgMenuItems").innerHTML;
        str = str.replace("<TBODY>", "<TABLE border='0' cellspacing='0' cellpadding='3' class='tableBody' width='200'><TBODY>");
        str = str.replace("</TBODY>", "</TBODY></TABLE>");
        document.getElementById("tblMenuItems").style.position = 'absolute';
        document.getElementById("tblMenuItems").style.left = mousedownX - 200;
        document.getElementById("tblMenuItems").style.top = mousedownY;
        document.getElementById("tblMenuItems").style.borderWidth = 1;
        document.getElementById("tblMenuItems").style.display="";
        document.getElementById("tblMenuItems").innerHTML = str;
    }
    
    function HideItems()
    {
       document.getElementById("tblMenuItems").style.display="none";
    }

    function ShowImage(obj)
    {
        //alert(obj.src);
        document.getElementById("myPic").src = obj.src;
        getMouseXY();
        //alert(document.body.scrollHeight);
        document.getElementById("divPic").style.position = 'absolute';
        document.getElementById("divPic").style.left = mousedownX + 20;
        document.getElementById("divPic").style.top = mousedownY;
        document.getElementById("divPic").style.display="";
        //alert(obj.style.height + " : " + obj.style.width);
    }

    function HideImage()
    {
        document.getElementById("divPic").style.display="none";
    }


function CheckFiles()
{
    //alert("in checkfiles");
    var objMain = document.getElementById("dgUsers");
    //alert(objMain);
    
    DataLoading(1);
//    alert(typeof(Page_ClientValidate));
    if (typeof(Page_ClientValidate) == 'function') 
        if (!Page_ClientValidate())
        {
            DataLoading(0);
            return false;
        }
        
    if (document.getElementById("ConferencePassword")) //FB Case 763: Saima
        SavePassword();
    if (document.getElementById("lstEndpoints"))
    {
        if ( (document.getElementById("chkLectureMode").checked) && (document.getElementById("lstEndpoints").value == "-1") && (document.getElementById("lstEndpoints").options.length > 1))
            {
                DataLoading(0);
                alert("講演者を選んでください");
                return false;
            }
        else
            document.getElementById("txtLecturer").value = document.getElementById("lstEndpoints").value;
       
    }
    if (document.getElementById("lblTab"))
    {
        if (document.getElementById("lblTab").value == "1")
            if(document.getElementById("btnCancel"))
            {
                DataLoading(0);
                alert("次へをクリックする前に作成/編集/キャンセルをクリックしてください。");  //FB 1246
                return false;
            }
           
        if (document.getElementById("lblTab").value == "2")
        {
            if(document.getElementById("btnCATCancel"))
            {
                DataLoading(0);
                alert("次へをクリックする前に作成/編集/キャンセルをクリックしてください。");
                return false;
            }
            if (!document.getElementById("btnAddNewCAT") && document.getElementById("CATMainGrid"))
            {
                DataLoading(0);
                alert("次へをクリックする前に作成/編集/キャンセルをクリックしてください。");  //FB 1246
                return false;
            }
        }
        if (document.getElementById("lblTab").value == "3")
            if(document.getElementById("btnHKCancel"))
            {
                DataLoading(0);
                alert("次へをクリックする前に作成/編集/キャンセルをクリックしてください。");
                return false;
            }
    }
    if (document.getElementById("ifrmPartylist"))
    {
        if (!ifrmPartylist.bfrRefresh())
        {
            DataLoading(0);
            return false;
        }
    }
    if (document.getElementById("FileUpload1"))
    {
        if(document.getElementById("FileUpload1").value)
        {
            if (confirm("あなたは、ブラウズファイルをアップロードしたいですか?"))
            {
                document.getElementById("__EVENTTARGET").value="btnUploadFiles";
                WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions("btnUploadFiles", "", true, "", "", false, false));
            }
            else
            {
                DataLoading(0);            
                return false;
            }
        }
    }
    if (document.getElementById("FileUpload2"))
    {
        if(document.getElementById("FileUpload2").value)
        {
            if (confirm("あなたは、ブラウズファイルをアップロードしたいですか?"))
            {
                document.getElementById("__EVENTTARGET").value="btnUploadFiles";
                WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions("btnUploadFiles", "", true, "", "", false, false));
            }
            else
                DataLoading(0);            
        }
    }
    if (document.getElementById("FileUpload3"))
    {
        if(document.getElementById("FileUpload3").value)
        {
            if (confirm("あなたは、ブラウズファイルをアップロードしたいですか?"))
            {
                document.getElementById("__EVENTTARGET").value="btnUploadFiles";
                WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions("btnUploadFiles", "", true, "", "", false, false));
            }
            else
                DataLoading(0);            
        }
    }
   
    //Merging Recurrence
    if(CheckFiles.arguments.length > 0 && CheckFiles.arguments[0] == 'N')
    {     
        var chkrecurrence = document.getElementById("chkRecurrence");
    
        if(chkrecurrence != null && chkrecurrence.checked == true)
        {
            if(!SubmitRecurrence())
            {
                DataLoading(0);            
                return false;
            }
        }
    }
    
    if (document.getElementById("confStartDate") != null)
    {
        if (document.getElementById("<%=ConferenceName.ClientID %>").value == "")
        {
            DataLoading(0);            
            return false;
        }
        return ChangeEndDate(2); //MOJ 
        return ChangeStartDate(0);
    }
    // FB Case 712 Saima
    if (document.getElementById("dgUsers"))
    {
       //   debugger;
       //alert("dgusers");
        var objMain = document.getElementById("dgUsers");
        var tVars = objMain.getElementsByTagName("INPUT").length + objMain.getElementsByTagName("SPAN").length + objMain.getElementsByTagName("SELECT").length;        

        var inputs = objMain.getElementsByTagName("INPUT");
        var selects = objMain.getElementsByTagName("SELECT");
        var objResult = document.getElementById("txtdgUsers");
        
        objResult.value = "";
        /*
        for(var i=0;i<inputs.length-1;i++)
        {
            if (inputs[i].id.indexOf("chkIsOutside") > 0)
                if (inputs[i].checked)
                    objResult.value = objResult.value + "1:";
                else
                    objResult.value = objResult.value + "0:";
            if (inputs[i].id.indexOf("txtAddress") > 0)
                objResult.value = objResult.value + inputs[i].value + ":";
            if (inputs[i].id.indexOf("lblUserID") > 0)
                objResult.value = objResult.value + inputs[i].value + ":";
            if (inputs[i].id.indexOf("txtEndpointURL") > 0)
                objResult.value = objResult.value + inputs[i].value + ":";
            if (inputs[i].id.indexOf("txtExchangeID") > 0)
                objResult.value = objResult.value + inputs[i].value + ":";
               //API Port Starts...
                if (inputs[i].id.indexOf("txtApiportno") > 0)
                objResult.value = objResult.value + inputs[i].value + ";";
                //API Port Ends...
        }   
        for (var i=0;i<selects.length;i++)
        {
            if (selects[i].id.indexOf("lstBridges") > 0)
                objResult.value = objResult.value + selects[i].value + ":";
            else if (selects[i].id.indexOf("lstTelnet") > 0)    //FB 1468
                objResult.value = objResult.value + selects[i].value + ":";
                
            if (selects[i].id.indexOf("lstLineRate") > 0)
                objResult.value = objResult.value + selects[i].value + ":";
            if (selects[i].id.indexOf("lstConnectionType") > 0)
                objResult.value = objResult.value + selects[i].value + ":";
            if (selects[i].id.indexOf("lstProtocol") > 0)
                objResult.value = objResult.value + selects[i].value + ":";
            if (selects[i].id.indexOf("lstAddressType") > 0)
                objResult.value = objResult.value + selects[i].value + ":";
            if (selects[i].id.indexOf("lstVideoEquipment") > 0)
                objResult.value = objResult.value + selects[i].value + ":";
            if (selects[i].id.indexOf("lstConnection") > 0 && selects[i].id.indexOf("lstConnectionType") <= 0)
                objResult.value = objResult.value + selects[i].value + ";";
        }
        */
        /* Code Modified during API Ports */
        var hdnextusrs = document.getElementById("hdnextusrcnt");
        var extusrcnt = 0;
        var controlName = 'dgUsers_ctl';
        var ctlid = '';
        
        if(hdnextusrs)
        {
            if(hdnextusrs.value != '')
                extusrcnt = parseInt(hdnextusrs.value);
        }
        var inputval;
        
        if(extusrcnt > 0)
		{
		    if(objResult)
                objResult.value = '';
                
            var conftype = document.getElementById("hdnconftype");
            
            var cnftype = "";
			  
		    if(conftype)
		        cnftype = conftype.value;
			        
            for(var i=1; i<=extusrcnt; i++)
			{
			    ctlid = ""; 
                if(i < 10)
                    ctlid = '0' + (i+1); 
                else 
                    ctlid = i+1;

			    //All input controls
			    var opt1 = document.getElementById(controlName + ctlid + "_" + "chkIsOutside");
			    var txtAdd = document.getElementById((controlName + ctlid + "_" + 'txtAddress'));
				var lblusr = document.getElementById((controlName + ctlid + "_" + 'lblUserID'));
                var txtepturl = document.getElementById((controlName + ctlid + "_" + 'txtEndpointURL'));
                var txtexchangeid = document.getElementById((controlName + ctlid + "_" + 'txtExchangeID'));
                var txtAPIPort = document.getElementById((controlName + ctlid + "_" + 'txtApiportno'));
                var txtconfcode = document.getElementById((controlName + ctlid + "_" + 'txtConfCode'));//Audio Addon
                var txtleader = document.getElementById((controlName + ctlid + "_" + 'txtleaderPin'));//Audio Addon
                
                
                //All select(drp) controls
                var lstBridge = document.getElementById((controlName + ctlid + "_" + 'lstBridges'));
                var lstTelnet = document.getElementById((controlName + ctlid + "_" + 'lstTelnetUsers'));
                var lstLineRate = document.getElementById((controlName + ctlid + "_" + 'lstLineRate'));
                var lstConType = document.getElementById((controlName + ctlid + "_" + 'lstConnectionType'));
                var lstProtocol = document.getElementById((controlName + ctlid + "_" + 'lstProtocol'));
                var lstAddType = document.getElementById((controlName + ctlid + "_" + 'lstAddressType'));
                var lstVideoEquip = document.getElementById((controlName + ctlid + "_" + 'lstVideoEquipment'));
                var lstConn = document.getElementById((controlName + ctlid + "_" + 'lstConnection'));
                                
                inputval = '';
			    if(opt1) //0
				{
					if(opt1.checked)
					{
                        inputval = inputval + "1:";
                    }
                    else
                    {
                        inputval = inputval + "0:";
					}
				}
				if (txtAdd) //1
				{
				    inputval = inputval + txtAdd.value +':';
				}
				else
				    inputval = inputval + ':';
				
				if(lblusr) //2
				{
					inputval = inputval + lblusr.value +':';
				}
				else
				    inputval = inputval + ':';
				    
				if(txtepturl) //3
				{
					inputval = inputval + txtepturl.value +':';
				}
				else
				    inputval = inputval + ':';
				    
				if(txtexchangeid)//4
				{
					inputval = inputval + txtexchangeid.value +':';
				}
				else
				    inputval = inputval + ':';
				    
				if(txtAPIPort) //5
				{
					inputval = inputval + txtAPIPort.value +':';
				}
				else
				    inputval = inputval + ':';
				    
				
				    
				if(lstBridge) // Selects
				{
				    inputval = inputval + lstBridge.value +':';
				}
				else if (lstTelnet)
				{
				    inputval = inputval + lstTelnet.value +':';
				}
				else
				{
				    inputval = inputval + ':';
				}
			    
				if(lstLineRate)
				{
					inputval = inputval + lstLineRate.value +':';
				}
				else
				{
				    inputval = inputval + ':';
				}
				if(lstConType)
				{
					inputval = inputval + lstConType.value +':';
				}
				else
				{
				    inputval = inputval + ':';
				}
				if(lstProtocol)
				{
					inputval = inputval + lstProtocol.value +':';
				}
				else
				{
				    inputval = inputval + ':';
				}
				if(lstAddType)
				{
					inputval = inputval + lstAddType.value +':';
				}
				else
				{
				    inputval = inputval + ':';
				}
				if(lstVideoEquip)
				{
					inputval = inputval + lstVideoEquip.value +':';
				}
				if(lstConn)
				{
					inputval = inputval + lstConn.value +':';
				}
				else
				{
				    inputval = inputval + ':';
				}
				/****** Code addedd for audio addon ***** */
				    
				if(txtconfcode)//13 
				{
					inputval = inputval + txtconfcode.value +':';
				}
				else
				    inputval = inputval + ':';
				    
				if(txtleader)//14
				{
					inputval = inputval + txtleader.value +';';
				}
				else
				    inputval = inputval + ';';
				    
				/****** Code addedd for audio addon ***** */
				objResult.value = objResult.value + inputval;
			}
	    }
        /* **** */
        //alert(objResult.value);
    }
    //false::127.0.0.1::;;false::1.2.3.4::;;1::768::1::1;;1::1::1::3;;1::384::2::2;;1::1::5::3;;
    
    
    
    return true;
}


function CheckPolycom(obj)
{
    if (obj.checked)
    {
        document.getElementById("trPoly1").style.display="";
        document.getElementById("trPoly2").style.display="";
        document.getElementById("trPoly3").style.display="";
    }
    else
    {
        document.getElementById("trPoly1").style.display="none";
        document.getElementById("trPoly2").style.display="none";
        document.getElementById("trPoly3").style.display="none";
    }
	document.getElementById("<%=imgVideoDisplay.ClientID %>").src = document.getElementById("<%=ImagesPath.ClientID %>").value + document.getElementById("<%=txtSelectedImage.ClientID %>").value + ".gif";
	CheckLectureMode();
}

function CheckLectureMode()
{
    if (document.getElementById("chkLectureMode").checked)  
    {
        document.getElementById("trLecture1").style.display="";
        document.getElementById("trLecture2").style.display="";
        //Code Changed by offshore for FB Issue 1121 - Start
        //document.getElementById("lstVideoMode").options[1].selected = true; //FB case 764 Saima
        document.getElementById("lstVideoMode").options[3].selected = true; 
        //Code Changed by offshore for FB Issue 1121 - End
    }
    else
    {
        document.getElementById("trLecture1").style.display="none";
        document.getElementById("trLecture2").style.display="none";
        if(document.getElementById("lstVideoMode").options[3]!=null)  //FB 2516
             document.getElementById("lstVideoMode").options[3].selected = true; //FB case 764 Saima
    }
}

function managelayout (dl, epid, epty)
{
//	change_display_layout_prompt('image/pen.gif', 'Manage Display Layout', epid, epty, dl, 4, "", ""); 
	change_display_layout_prompt('image/pen.gif', 'Manage Display Layout', epid, epty, dl, 5, document.getElementById('<%=ImageFiles.ClientID%>').value + '|' + document.getElementById('<%=ImageFilesBT.ClientID%>').value, document.getElementById('<%=ImagesPath.ClientID%>').value);
}
function displayLayout(s)
{	
	var obj = document.getElementById("default" + s);
	var temp = document.getElementById("display" + s);
	//alert(obj.checked);
	if (obj.checked)
		temp.style.display = "block";
	else
		temp.style.display = "none";
}

function change_display_layout_prompt(promptpicture, prompttitle, epid, epty, dl, rowsize, images, imgpath) 
{
	var title = new Array()
	title[0] = "Default ";
	title[1] = "Custom ";
	promptbox = document.createElement('div'); 
	promptbox.setAttribute ('id' , 'prompt');
	document.getElementsByTagName('body')[0].appendChild(promptbox);
	promptbox = document.getElementById('prompt').style; // FB 2050

	promptbox.position = 'absolute'
	promptbox.top = -155+ mousedownY + 'px'; //FB 1373 start FB 2050
	promptbox.left = mousedownX - 485 + 'px'; // FB 2050
	promptbox.width = rowsize * 125 + 'px'; // FB 2050
	promptbox.border = 'outset 1 #bbbbbb';
	promptbox.height = '400px' ; // FB 2050
	promptbox.overflow ='auto'; //FB 1373 End
   

	m = "<table cellspacing='0' cellpadding='0' border='0' width='100%'><tr valign='middle'><td width='22' height='22' style='text-indent:2;' class='tableHeader'>&nbsp;</td><td class='tableHeader'>" + prompttitle + "</td></tr></table>" 
	m += "<table cellspacing='2' cellpadding='2' border='0' width='100%' class='promptbox'>";
	imagesary = images.split(":");
	rowNum = parseInt( (imagesary.length + rowsize - 2) / rowsize, 10 );
	m += "  <tr><td align='right' colspan='" + (rowsize * 2) + "'>"
	 //Code Changed for Soft Edge Button
	//m += "    <input type='button' class='altShortBlueButtonFormat' value='Submit' onMouseOver='this.style.border=\"1 outset #dddddd\"' onMouseOut='this.style.border=\"1 solid transparent\"' onClick='saveOrder(epid);'>"
	//m += "    <input type='button' class='altShortBlueButtonFormat' value='Cancel' onMouseOver='this.style.border=\"1 outset transparent\"' onMouseOut='this.style.border=\"1 solid transparent\"' onClick='cancelthis();'>"
	m += "    <input type='button' onfocus='this.blur()' class='altShortBlueButtonFormat' value='送信'  onClick='saveOrder(epid);'>"
	m += "    <input type='button' onfocus='this.blur()' class='altShortBlueButtonFormat' value='キャンセル'  onClick='cancelthis();'>"
	m += "  </td></tr>"
	m += "	<tr>";
	//Window Dressing
	m += "    <td colspan='" + (rowsize * 2) + "' align='left' class='blackblodtext'><b>レイアウトを表示します。</b></td>";
	m += "  </tr>"
	m += "  <tr>"
	m += "    <td colspan='" + (rowsize * 2) + "' height='5'></td>";
	m += "  </tr>"

	imgno = 0;
	for (i = 0; i < rowNum; i++) 
	{
		m += "  <tr>";
		for (j = 0; (j < rowsize) && (imgno < imagesary.length-1); j++) {
			
		
			m += "    <td valign='middle'>";
			m += "      <input type='radio' name='layout' id='layout' value='" + imagesary[imgno] + "' onClick='epid=" + imagesary[imgno] + ";'>";
			m += "    </td>";
			m += "    <td valign='middle'>";
			m += "      <img src='" + imgpath + imagesary[imgno] + ".gif' width='57' height='43'>";
			m += "    </td>";
			imgno ++;
		}
		m += "  </tr>";
	}
    
	m += "  <tr>";
	m += "    <td colspan='" + (rowsize * 2) + "' height='5'></td>";
	m += "  </tr>"
	m += "  <tr><td align='right' colspan='" + (rowsize * 2) + "'>"
	//Code Changed for Soft Edge Button
	//m += "    <input type='button' class='altShortBlueButtonFormat' value='Submit' onMouseOver='this.style.border=\"1 outset #dddddd\"' onMouseOut='this.style.border=\"1 solid transparent\"' onClick='saveOrder(epid);'>"
	//m += "    <input type='button' class='altShortBlueButtonFormat' value='Cancel' onMouseOver='this.style.border=\"1 outset transparent\"' onMouseOut='this.style.border=\"1 solid transparent\"' onClick='cancelthis();'>"
	m += "    <input type='button' onfocus='this.blur()' class='altShortBlueButtonFormat' value='送信' onClick='saveOrder(epid);'>"
	m += "    <input type='button' onfocus='this.blur()' class='altShortBlueButtonFormat' value='キャンセル' onClick='cancelthis();'>"
	m += "  </td></tr>"
	m += "</table>" 
	
	document.getElementById('prompt').innerHTML = m;
} 

function saveOrder(id) 
{
    if (id < 10)
        id = "0" + id;
	document.getElementById("<%=txtSelectedImage.ClientID %>").value = id;
	document.getElementById("<%=imgVideoDisplay.ClientID %>").src = document.getElementById("<%=ImagesPath.ClientID %>").value + document.getElementById("<%=txtSelectedImage.ClientID %>").value + ".gif";
	cancelthis();
} 


function cancelthis()
{
	document.getElementsByTagName("body")[0].removeChild(document.getElementById("prompt"));
	//window.resizeTo(750,450); //FB Case 536 Saima
}

function CheckDefault(obj)
{
//    var textToChange = obj.parentNode.parentNode.innerHTML;
//    alert(textToChange);
//    if (obj.checked)
//        textToChange = textToChange.replace(/<TD style="/g, '<TD style="text-decoration:line-through ');

//    obj.parentNode.parentNode.innerHTML = textToChange;
}

function viewLayout()
{
    var obj = document.getElementById("<%=lstRoomLayout.ClientID%>");
    if (obj.disabled == "disabled")
        alert("選択した部屋は部屋の配置が確定していません。");
    else
        if (obj.selectedIndex > 0 && obj[obj.selectedIndex].text != '[None]') // Code added for FB 1176
        {
            // url = "image/room/" + obj[obj.selectedIndex].text + ".jpg";// Code added for FB 1176 // Image Project
            url = "../en/image/room/" + obj[obj.selectedIndex].text;// Image Project //FB 1830
            window.open(url, "RoomLayout", "width=750,height=400,resizable=yes,scrollbars=yes,status=no");
        }
        else
            alert("リストから部屋の配置を選んでくささい。");
        
}
function viewendpoint(val)
{
	
	url = "dispatcher/admindispatcher.asp?eid=" + val + "&cmd=GetEndpoint&ed=1&wintype=pop";

	if (!window.winrtc) {	// has not yet been defined
		winrtc = window.open(url, "", "width=400,height=310,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
		winrtc.focus();
	} else { // has been defined
	    if (!winrtc.closed) {     // still open
	    	winrtc.close();
	        winrtc = window.open(url, "", "width=400,height=310,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
			winrtc.focus();
		} else {
	        winrtc = window.open(url, "", "width=400,height=310,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
	        winrtc.focus();
    	}
	}
}

function viewconflict(id, confTime, confDate, confDuration, confTZ, ConferenceName, confID)
{
    //alert(id + " : " + confTime + " : " + confDate + " : " + confDuration);
    var rms = document.getElementById("selectedloc").value;
	url = "preconferenceroomchart.asp?wintype=ifr&cid=" + confID + "&cno=" + id +
		"&date=" + confDate + "&time=" + confTime + "&timeZone=" + confTZ +
		"&duration=" + confDuration + "&r=" + rms + "&n=" + ConferenceName;
//alert(url);
	wincrm = window.open(url,'confroomchart','status=no,width=1,height=1,top=30,left=0,scrollbars=yes,resizable=yes')
	if (wincrm)
		wincrm.focus();
	else
		alert(EN_132);
}

function formatTime(timeText, regText)
{

   if("<%=Session["timeFormat"]%>" == "1")
   {    
		var tText = document.getElementById(timeText);

	    if(tText.value.length < 8)
        {
            tText.value = tText.value.replace('a','A').replace('p','P').replace('m','M').replace('AM',' AM').replace('PM',' PM');
        }

        if (document.getElementById(timeText).value.search(/^[0|1][0-9]:[0-5][0-9] ?[a|A|p|P][M|m]/) != -1)
        {
            var a_p = "";
            //FB 2614
            //var t = new Date();
            //var t = new Date(GetDefaultDate(document.getElementById("confStartDate").value,'<%=format%>'));
            
            //var d = new Date(t.getDay() + "/" + t.getMonth() + "/" + t.getYear() + " " + tText.value); //document.getElementById(timeText).value);
            var d = new Date(GetDefaultDate(document.getElementById("confStartDate").value,'<%=format%>') + " " + tText.value);
           
            var curr_hour = d.getHours();
            if (curr_hour < 12) a_p = "AM"; else a_p = "PM";

            if (curr_hour == 0) curr_hour = 12;
            if (curr_hour > 12) curr_hour = curr_hour - 12;
            
            curr_hour = curr_hour + "";             
            if (curr_hour.length == 1)
               curr_hour = "0" + curr_hour;
                     
            var curr_min = d.getMinutes();
            curr_min = curr_min + "";
            if (curr_min.length == 1)
               curr_min = "0" + curr_min;

            document.getElementById(timeText).value = curr_hour + ":" + curr_min + " " + a_p;            
            document.getElementById(regText).style.display = "None"; 
            return true;            
       }
       else
       {    
            document.getElementById(regText).style.display = ""; 
            //document.getElementById(timeText).focus();
            return false;
       }
   }
    return true;
}

//FB 1716

function ChangeDuration()
{ 
    if ('<%=isEditMode%>' == "1" )    
    {        
        var duration = document.getElementById("hdnDuration").value;
        var chgVar =  document.getElementById("hdnChange").value;
              
        if(duration != '')
        {        
            var durArr = duration.split("&");
            var changeTime;
            
            var setupTime = new Date(GetDefaultDate(document.getElementById("confStartDate").value,'<%=format%>') + " "
            + document.getElementById("confStartTime_Text").value);
            
            var startTime = new Date(GetDefaultDate(document.getElementById("SetupDate").value,'<%=format%>') + " "
            + document.getElementById("SetupTime_Text").value);
            
            var endTime = new Date(GetDefaultDate(document.getElementById("TearDownDate").value,'<%=format%>') + " "
            + document.getElementById("TeardownTime_Text").value);
           
            switch(chgVar)
            {
                case "ST":  
                    setupTime = setCDuration(setupTime,durArr[1]);                    
                    document.getElementById("SetupDate").value = getCDate(setupTime);                    
                    document.getElementById("SetupTime_Text").value = getCTime(setupTime);
                    var startTime = new Date(GetDefaultDate(document.getElementById("SetupDate").value,'<%=format%>') + " "
                    + document.getElementById("SetupTime_Text").value);
                    
                    startTime = setCDuration(startTime,durArr[0]);                    
                    document.getElementById("TearDownDate").value = getCDate(startTime);                    
                    document.getElementById("TeardownTime_Text").value = getCTime(startTime);
                    var endTime = new Date(GetDefaultDate(document.getElementById("TearDownDate").value,'<%=format%>') + " "
                    + document.getElementById("TeardownTime_Text").value);                
                    
                    endTime = setCDuration(endTime,durArr[2]);
                    document.getElementById("confEndDate").value = getCDate(endTime);                   
                    document.getElementById("confEndTime_Text").value = getCTime(endTime);
                break;
                case "SU":
                
                    startTime = setCDuration(startTime,durArr[0]);                    
                    document.getElementById("TearDownDate").value = getCDate(startTime);                    
                    document.getElementById("TeardownTime_Text").value = getCTime(startTime);
                    var endTime = new Date(GetDefaultDate(document.getElementById("TearDownDate").value,'<%=format%>') + " "
                    + document.getElementById("TeardownTime_Text").value);                
                    
                    endTime = setCDuration(endTime,durArr[2]);
                    document.getElementById("confEndDate").value = getCDate(endTime);                   
                    document.getElementById("confEndTime_Text").value = getCTime(endTime);
                break;
                case "TD":
                   endTime = setCDuration(endTime,durArr[2]);
                   document.getElementById("confEndDate").value = getCDate(endTime);                   
                   document.getElementById("confEndTime_Text").value = getCTime(endTime);
                break;               
            }
        }
    }  
}

//FB 1716
function setCDuration(setupTime, dura)
{
    var chTime = new Date(setupTime);
    var d = 0;
    var min = 0;
    var hh = 0, dur = 0;
    if(dura > 60)
    {   
        hh = dura / 60;
        min = dura % 60;                        
        if(min > 0)
            hh = Math.floor(hh) + 1;                        
     
        for(d = 1; d <= hh ; d++)
        {
            if(min > 0 && d == hh)
                dur = dura % 60;
            else
                dur = 60;
                
             chTime.setMinutes(chTime.getMinutes() + dur);   
        }
    }
    else if(dura > 0)        
        chTime.setTime(chTime.getTime() + (dura * 60 * 1000));
    else
        chTime;
        
   return chTime
}
//FB 1716

function getCDate(changeTime)
{
    var strDate;
    var month,date;
    month = changeTime.getMonth() + 1;
    date = changeTime.getDate();
    
    if(eval(changeTime.getMonth() + 1) <10)
        month = "0" + (changeTime.getMonth() + 1);
    
    if(eval(date) <10)
        date = "0" + changeTime.getDate();
       
    if('<%=format%>' == 'MM/dd/yyyy')    
        strDate = month + "/" + date + "/" + changeTime.getFullYear();
    else
        strDate =  date + "/" + month + "/" + changeTime.getFullYear();
    
    return strDate;
}
//FB 1716

function getCTime(changeTime)
{
    //FB 2614
    var hh = changeTime.getHours(); //parseInt(changeTime.toLocaleTimeString().toString().split(" ")[0].toString().split(":")[0],10);   
    
    if("<%=Session["timeFormat"]%>" == "1") //FB 2108
    {
        if(hh >= 12)
            hh = hh - 12;
            
        if(hh == 0)
            hh = 12
    }
             
    if (hh < 10)
        hh = "0" + hh;
    //FB 2614
    var mm = changeTime.getMinutes();//parseInt(changeTime.toLocaleTimeString().toString().split(" ")[0].toString().split(":")[1],10);
    if (mm < 10)
        mm = "0" + mm;
    //var ap = changeTime.toLocaleTimeString().toString().split(" ")[1];
    var ap = changeTime.format("tt"); //FB 2108
    var evntime = parseInt(hh,10);
    
    if(evntime < 12 && ap == "PM")
       evntime = evntime + 12;
       
    var tiFormat =  "<%=Session["timeFormat"]%>" ;

    if(tiFormat == '0')
    {
        if(ap == "AM")                        
            strTime = (hh == "12") ? "00:" + mm : hh + ":" + mm ;
        else
        {
            if(evntime == "24")
                strTime = (hh == "12") ? "00:" + mm : evntime + ":" + mm;
            else
                strTime =  evntime + ":" + mm;
        }   
    }
    else
        strTime = hh + ":" + mm + " " + ap;
        
   return strTime;
}

function ChangeEndDate(frm)
{
   
//FB 1715
//if (document.getElementById("Recur").value == ""  && document.getElementById("hdnRecurValue").value == "")
//{
    //Code modified for FB 1073 -- Start
    // if (Date.parse("<%=Session["systemDate"]%>" + " " + "<%=Session["systemTime"]%>") > Date.parse(document.getElementById("confStartDate").value + " " + document.getElementById("confStartTime_Text").value) )
    var confstdate = '';
    //FB 1715
        if (document.getElementById("Recur").value == ""  && document.getElementById("hdnRecurValue").value == "")        
            confstdate = GetDefaultDate(document.getElementById("confStartDate").value,'<%=format%>');
        else
            confstdate = GetDefaultDate(document.getElementById("StartDate").value,'<%=format%>');
       
   
    //Code added for FB 1308 - start
    var confenddate = '';
    confenddate = GetDefaultDate(document.getElementById("confEndDate").value,'<%=format%>');
    //Code added for FB 1308 - end
    
    /****** Code chnaged for time zone issue *****/
    /*
    if (Date.parse("<%=Session["systemDate"]%>" + " " + "<%=Session["systemTime"]%>") > Date.parse(confstdate + " " + document.getElementById("confStartTime_Text").value) )
    //Code modified for FB 1073 -- End   
        {
            document.getElementById("confStartDate").value = document.getElementById("confEndDate").value;
            if (frm == "0" || frm == "2")
            {
                alert("Invalid Start Date or Time. It should be greater than time current time in user preferred timezone.");
                DataLoading(0);
                return false;
            }
        }*/
         //Code added for FB 1308
         /****** Code chnaged for time zone issue *****/
         //FB 1716
        if(frm == "0" && '<%=isEditMode%>' == "0" ) //code added fro MOJ
        {
          ChangeEndDateTime(confstdate,confenddate);
        }
            
       EndDateValidation(frm,confstdate,confenddate,'E');
       //FB 1716
       ChangeDuration();       
       
    //}  FB 1715
   return true;
}


//Code added for FB 1308 start 

function DuraionCalculation()
{
    var confenddate = '';
    var confstdate = '';
    var durationMin = '';
    var totalMinutes;
    var totalDays;
    var totalHours;
    var lblconfduration = document.getElementById('<%=lblConfDuration.ClientID%>');
    
    lblconfduration.innerText = '';
    confenddate = GetDefaultDate(document.getElementById("confEndDate").value + " " + document.getElementById("confEndTime_Text").value,'<%=format%>');
    confstdate = GetDefaultDate(document.getElementById("confStartDate").value + " " + document.getElementById("confStartTime_Text").value,'<%=format%>');
    
    var durationMin = parseInt(Date.parse(confenddate) - Date.parse(confstdate)) /  1000  ;
    
    if(durationMin != '')
    {
        totalDays = Math.floor(durationMin / (24 * 60 * 60));
        totalHours = Math.floor((durationMin - (totalDays * 24 * 60 * 60)) / (60 * 60));
        totalMinutes = Math.floor((durationMin - (totalDays * 24 * 60 * 60) -  (totalHours * 60 * 60)) / 60);
    }
    
    if (Math.floor(durationMin) < 0)
        lblconfduration.innerText = "無効な継続時間";
    if (totalDays > 0)
        lblconfduration.innerText = totalDays + " 日 ";
    if (totalHours > 0)
        lblconfduration.innerText += totalHours + " 時間 ";
    if (totalMinutes > 0)
        lblconfduration.innerText += totalMinutes + " 分 ";
}

function EndDateValidation(frm,confstdate,confenddate,msgchk)
{
    var setupdate = Date.parse(document.getElementById("SetupDate").value + " " + document.getElementById("SetupTime_Text").value);
    var sDate = Date.parse(document.getElementById("confStartDate").value + " " + document.getElementById("confStartTime_Text").value);
    var eDate = Date.parse(document.getElementById("confEndDate").value + " " + document.getElementById("confEndTime_Text").value);
    var tDate = Date.parse(document.getElementById("TearDownDate").value + " " + document.getElementById("TeardownTime_Text").value);
    
    //FB 1715
    if ( (sDate >= eDate) && (document.getElementById("confStartTime_Text").value.search(/^[0|1][0-9]:[0-5][0-9] ?[a|A|p|P][M|m]/) != -1)
    && (document.getElementById("confEndTime_Text").value.search(/^[0|1][0-9]:[0-5][0-9] ?[a|A|p|P][M|m]/) != -1))
    {
         if(msgchk == 'S')
        {
            if (Date.parse(confstdate) == Date.parse(confenddate))        
            {
                if (Date.parse(confstdate + " " + document.getElementById("confStartTime_Text").value) > Date.parse(confenddate + " " + document.getElementById("confEndTime_Text").value) )        
                    alert("終了時間は開始時間より後になるべきなので変更されます。");  
               else if (Date.parse(confstdate + " " + document.getElementById("confStartTime_Text").value) == Date.parse(confenddate + " " + document.getElementById("confEndTime_Text").value) )        
                    alert("終了時間は開始時間より後になるべきなので変更されます。");              
            }    
            else
                alert("終了日は開始日と同じか後になるべきなので変更されます。");
        }
            
         ChangeEndDateTime(confstdate,confenddate); //MOJ Fix
      }
     
      if(!document.getElementById("chkStartNow").checked) //buffer zone start
      {
        var chkbuffer = document.getElementById("chkEnableBuffer");
        if (("<%=enableBufferZone%>" == "1") && (chkbuffer.checked))
 /***  SJV BufferZone Fix ****/
        { 
              if ((setupdate < sDate) || (setupdate >= tDate) ||(setupdate > eDate) || (tDate > eDate) || (tDate < setupdate))
              {
                  if( (setupdate < sDate) || (setupdate >= tDate) || (setupdate > eDate) || (tDate < setupdate))
                    {
                        document.getElementById("SetupDate").value = document.getElementById("confStartDate").value;
                        document.getElementById("SetupTime_Text").value = document.getElementById("confStartTime_Text").value;//Edited For FF...
                        document.getElementById("TearDownDate").value = document.getElementById("confEndDate").value;
                        document.getElementById("TeardownTime_Text").value = document.getElementById("confEndTime_Text").value;//Edited For FF...
                    }
              }
              if(tDate < setupdate)
               {
                    document.getElementById("TearDownDate").value = document.getElementById("confEndDate").value;
                    document.getElementById("TeardownTime_Text").value = document.getElementById("confEndTime_Text").value;
               } 
              if(tDate > eDate)
              {
                   document.getElementById("confEndDate").value = document.getElementById("TearDownDate").value;
                   document.getElementById("confEndTime_Text").value = document.getElementById("TeardownTime_Text").value;
              }
             if(setupdate >= tDate)
              {
                   document.getElementById("SetupDate").value = document.getElementById("confStartDate").value;
                   document.getElementById("SetupTime_Text").value = document.getElementById("confStartTime_Text").value;
                   document.getElementById("TearDownDate").value = document.getElementById("confEndDate").value;
                   document.getElementById("TeardownTime_Text").value = document.getElementById("confEndTime_Text").value;
              }
         }
         else
         {
            document.getElementById("SetupDate").value = document.getElementById("confStartDate").value;
            document.getElementById("SetupTime_Text").value = document.getElementById("confStartTime_Text").value; //Edited for FF
                        
            document.getElementById("TearDownDate").value = document.getElementById("confEndDate").value;
            document.getElementById("TeardownTime_Text").value = document.getElementById("confEndTime_Text").value;//Edited for FF
         }   
      }//buffer zone end
      DuraionCalculation();  
}

//Code added for FB 1308 end 

//Method added for MOJ
function ChangeEndDateTime(confstdate,confenddate)
{
      document.getElementById("confEndDate").value = document.getElementById("confStartDate").value;
         
         /*** Code changed for FB 1454 *****/
        //var endTime = new Date(document.getElementById("confStartDate").value + " " + document.getElementById("confStartTime_Text").value);
        var endTime = new Date(confstdate + " " + document.getElementById("confStartTime_Text").value);
         /*** Code changed for FB 1454 *****/
        
        //var apBfr = endTime.toLocaleTimeString().toString().split(" ")[1];          
        var apBfr = endTime.format("tt");      //FB 2108
                
        /*** Code added for FB 1454 *****/
        
        //FB 2501 - Start
        //endTime.setHours(endTime.getHours() + 1);
        if("<%=Session["DefaultConfDuration"]%>" != null)
        {
            var ConfDuration = "<%=Session["DefaultConfDuration"]%>";
            var ConfHours = parseInt(ConfDuration) / 60; 
            var ConfMinutes = parseInt(ConfDuration) % 60;    
            ConfMinutes = ConfMinutes + endTime.getMinutes();
            if(ConfMinutes > 59)
            {
               ConfMinutes = ConfMinutes - 60;
               ConfHours = ConfHours + 1;
            }
            endTime.setHours(endTime.getHours() + ConfHours);
            endTime.setMinutes(ConfMinutes);
        }       
       
        //Added for FB 1425 QA Bug START
            //if('<%=client.ToString().ToUpper()%>' =="MOJ")
            //endTime.setMinutes(endTime.getMinutes() - 45);
        //Added for FB 1425 QA Bug End
        //FB 2501 - End
        
        var month,date;
        month = endTime.getMonth() + 1;
        date = endTime.getDate();
        
        if(eval(endTime.getMonth() + 1) <10)
            month = "0" + (endTime.getMonth() + 1);
        
        if(eval(date) <10)
            date = "0" + endTime.getDate();
        
                   
        document.getElementById("SetupDate").value = document.getElementById("confStartDate").value;
           
        if('<%=format%>' == 'MM/dd/yyyy')
        {
            document.getElementById("confEndDate").value = month + "/" + date + "/" + endTime.getFullYear();
            document.getElementById("TearDownDate").value = month + "/" + date + "/" + endTime.getFullYear();
        }
        else
        {
            document.getElementById("confEndDate").value =  date + "/" + month + "/" + endTime.getFullYear();
            document.getElementById("TearDownDate").value =  date + "/" + month + "/" + endTime.getFullYear();
        }
        
        /*** Code added for FB 1454 *****/
        //FB 2614
        //var hh = parseInt(endTime.toLocaleTimeString().toString().split(" ")[0].toString().split(":")[0],10);            
        var hh = endTime.getHours();
        
        if("<%=Session["timeFormat"]%>" == "1") //FB 2108
        {
            if(hh >= 12)
                hh = hh - 12;
                
            if(hh == 0)
                hh = 12
        }
                
        if (hh < 10)
            hh = "0" + hh;
        //FB 2614
        //var mm = parseInt(endTime.toLocaleTimeString().toString().split(" ")[0].toString().split(":")[1],10);
        var mm = endTime.getMinutes()
        if (mm < 10)
            mm = "0" + mm;
        //var ap = endTime.toLocaleTimeString().toString().split(" ")[1];
        var ap = endTime.format("tt");    //FB 2108  
        
        /*** Code changed for FB 1426 *****/
        //document.getElementById("confEndTime_Text").value = hh + ":" + mm + " " + ap;


        var evntime = parseInt(hh,10);
        
         if(evntime < 12 && ap == "PM")
            evntime = evntime + 12;
            
                   
        if("<%=Session["timeFormat"]%>" == "0")
        {
            if(ap == "AM")
            {
                if(hh == "12")
                    document.getElementById("confEndTime_Text").value = "00:" + mm ;
                else
                    document.getElementById("confEndTime_Text").value = hh + ":" + mm ;
                
            }
            else
            {
                if(evntime == "24")
                    document.getElementById("confEndTime_Text").value = "12:" + mm ;
                else
                    document.getElementById("confEndTime_Text").value = evntime + ":" + mm ;
            }
                
        }
        else
        {
            document.getElementById("confEndTime_Text").value = hh + ":" + mm + " " + ap;
        }
}

function ChangeStartDate(frm)
{
//    //alert("in ChangeStartDate");
//    if (Date.parse(document.getElementById("confEndDate").value + " " + document.getElementById("confEndTime_Text").value) <= Date.parse(document.getElementById("confStartDate").value + " " + document.getElementById("confStartTime_Text").value) )
//    {
//        alert("End Date/Time should be greater than Start Date/Time.");
//        document.getElementById("confEndDate").value = document.getElementById("confStartDate").value;
//        //document.getElementById("confStartTime").value = document.getElementById("confEndTime").value;
//        return false;
//    }
//    else

    if(document.getElementById("Recur").value == "" && document.getElementById("hdnRecurValue").value == "")
    {
        //Code added for FB 1073 -- Start
        var confenddate = '';
        confenddate = GetDefaultDate(document.getElementById("confEndDate").value,'<%=format%>');
        var confstdate = '';
        confstdate = GetDefaultDate(document.getElementById("confStartDate").value,'<%=format%>');
       
        //FB 1728
        //if (Date.parse("<%=Session["systemDate"]%>" + " " + "<%=Session["systemTime"]%>") > Date.parse(document.getElementById("confEndDate").value + " " + document.getElementById("confEndTime_Text").value))-- FB 1073
//         if (Date.parse("<%=Session["systemDate"]%>" + " " + "<%=Session["systemTime"]%>") > Date.parse(confenddate + " " + document.getElementById("confEndTime_Text").value))
//        {
//            document.getElementById("confEndDate").value = document.getElementById("confStartDate").value;
//            if (frm == "0") 
//            {
//                alert("Invalid End Date or Time. It should be greater than time current time in user preferred timezone.");
//                DataLoading(0);
//                return false;
//            }
//        }
        
        //if (Date.parse(document.getElementById("confStartDate").value + " " + document.getElementById("confStartTime_Text").value) > Date.parse(document.getElementById("confEndDate").value + " " + document.getElementById("confEndTime_Text").value) )
        //Code added & commented for FB 1308 start    
        EndDateValidation(frm,confstdate,confenddate,'S');

    }
     //FB 1716
     ChangeDuration(); 
    return true;
}

function CheckDuration()
{
    //alert("here in check duration");
    if (document.getElementById("Recur").value == "")
    if (Date.parse(document.getElementById("confStartDate").value + " " + document.getElementById("confStartTime_Text").value) > Date.parse(document.getElementById("confEndDate").value + " " + document.getElementById("confEndTime_Text").value))
    {
        alert("無効な継続時間");
        return false;
    }
}

function SavePassword()
{
    //alert(document.getElementById("<%=ConferencePassword.ClientID %>").value);
    document.getElementById("<%=confPassword.ClientID %>").value = document.getElementById("<%=ConferencePassword.ClientID %>").value;
}

function ChangePublic()
{
    var t;
	//FB 2274 Strats   
    var dynInvite_Session;
    if(document.getElementById("hdnCrossdynInvite").value != null)
        dynInvite_Session = document.getElementById("hdnCrossdynInvite").value;
    else 
        dynInvite_Session = "<%=Session["dynInvite"] %>";

    //FB 2274 Ends
    if(document.getElementById("chkPublic") != null)
    {
        if ((document.getElementById("chkPublic").checked) && (dynInvite_Session == "1") ) //FB 2274   
        {
            document.getElementById("openRegister").style.display = "";
        }
        else
        {
            document.getElementById("openRegister").style.display ="none";
        }
    }
}
//VMR
function changeVMR()
{
 var iVMR;//FB 2448
 iVMR = "<%=iEnableVMR%>";

var vmrParty = document.getElementById("isVMR")
if(document.getElementById("chkVMR") != null)
{
    if ((document.getElementById("chkVMR").checked) == true)
    {
	if (iVMR  == "1")//FB 2448
	    {
    		    document.getElementById("divbridge").style.display = "block";
        }
        vmrParty.value = "1"
        // FB 2501 Starts
        if(document.getElementById('ConferencePassword') != null )
        {
          if(document.getElementById('ConferencePassword').value == "")
            num_gen();
        }
        // FB 2501 Ends
    }
    else
    {
        document.getElementById("divbridge").style.display ="none";
        vmrParty.value = "0"
        
        // FB 2501 Starts
        if(document.getElementById('ConferencePassword') != null )
        {
            document.getElementById('ConferencePassword').value = "";
            document.getElementById('ConferencePassword2').value = "";
            SavePassword();
        }
        // FB 2501 Ends
     }
}

return true;
}

function fnVMR()
{
 var iVMR;//FB 2448
 iVMR = "<%=iEnableVMR%>";

var vmrParty = document.getElementById("isVMR")
if(document.getElementById("chkVMR") != null)
{
    if ((document.getElementById("chkVMR").checked) == true)
    {
	if (iVMR  == "1")//FB 2448
	    {
            document.getElementById("divbridge").style.display = "block";
        }
        vmrParty.value = "1"
    }
    else
    {
        document.getElementById("divbridge").style.display ="none";
        vmrParty.value = "0"
     }
}

return true;
}
function ChangeImmediate()
{
    var t;
	var t1;
    //alert(document.getElementById("chkStartNow").checked);
    document.getElementById("lstDuration_Text").style.width = "75px";
    document.getElementById("confStartTime_Text").style.width = "75px";
    document.getElementById("confEndTime_Text").style.width = "75px";
    document.getElementById("SetupTime_Text").style.width = "75px"; //buffer zone
    document.getElementById("TeardownTime_Text").style.width = "75px"; //buffer zone
    
    if (document.frmSettings2.Recur.value == "")
    {
		//Merging Recurrence - start
        if( document.getElementById("hdnRecurValue").value == 'R')
        {
             t = "none";
             t1 = "";
            Page_ValidationActive=false;
            document.getElementById("divDuration").style.display = "none";
        }
        else if (document.getElementById("chkStartNow").checked)  //Merging Recurrence - end
        {
            t = "none";
            t1 = "none";
            Page_ValidationActive=false;
            document.getElementById("divDuration").style.display = "";
            document.getElementById("chkEnableBuffer").checked = false;
        }
        else
        {
            t = "";
            t1 = "";
            Page_ValidationActive=true;
            document.getElementById("divDuration").style.display ="none";
        }
        
	    document.getElementById("NONRecurringConferenceDiv1").style.display = t1;
	    document.getElementById("NONRecurringConferenceDiv2").style.display = t;
	    document.getElementById("NONRecurringConferenceDiv3").style.display = t1;
	    //document.getElementById("NONRecurringConferenceDiv5").style.display = t; // Merging Recurrence
	    document.getElementById("NONRecurringConferenceDiv6").style.display = t1; //buffer zone
	    document.getElementById("NONRecurringConferenceDiv7").style.display = t1; //buffer zone
	    
	    //FB 1911
	    var args = ChangeImmediate.arguments;
	    if(args != null)
	    {
	        if(args[0] == "S")
	        {
	            document.getElementById("recurDIV").style.display = "None"; //FB 1981
	            document.frmSettings2.RecurSpec.value = '';
	            document.frmSettings2.RecurringText.value = '';	            
	        }
	    }
	    if ("<%=enableBufferZone%>" == "0")
	        document.getElementById("NONRecurringConferenceDiv9").style.display = "None";
	    else
	        document.getElementById("NONRecurringConferenceDiv9").style.display = t1; //Enable Recurrence
 		
	    //FB 1250
        if ("<%=Session["recurEnable"] %>" == "0")
        {
            //document.getElementById("recurDIV").style.display = "none";
            document.getElementById("NONRecurringConferenceDiv8").style.display = "none";
        }
        else if ("<%=isInstanceEdit%>" == "Y" )
            document.getElementById("NONRecurringConferenceDiv8").style.display = "none";
        else
        {
            //document.getElementById("recurDIV").style.display = t;
            document.getElementById("NONRecurringConferenceDiv8").style.display = t1;
        }
        //FB 1250 
        
        if ("<%=timeZone%>" == "0" ) //FB 1425
            document.getElementById("NONRecurringConferenceDiv3").style.display = "none";    
          
        document.getElementById("EndDateArea").style.display = t;
	    document.getElementById("TeardownArea").style.display = t;
	    
	    fnEnableBuffer();
        
    }
}


function isRecur()
{
    //Merging Recurrence - start
    var t = (document.frmSettings2.Recur.value == "") ? "" : "none";
    
    if(isRecur.arguments.length > 0 || document.getElementById("hdnRecurValue").value == 'R')
    {
        if(isRecur.arguments[0] == 'R' || document.getElementById("hdnRecurValue").value == 'R')
            t = "None";
    }
   //Code Commented for Merging Recurrence
    //FB 2274 Starts
    var EnableImmConf_Session;
    if(document.getElementById("hdnCrossEnableImmConf").value != null)
        EnableImmConf_Session = document.getElementById("hdnCrossEnableImmConf").value;
    else 
        EnableImmConf_Session = "<%=Session["EnableImmConf"] %>";
    //FB 2274 Ends
    
	document.getElementById("NONRecurringConferenceDiv2").style.display = t;
    //Code Modified For MOJ Phase 2 -Start
    if ("<%=client.ToString().ToUpper() %>" == "MOJ")  
    {  
        document.getElementById("NONRecurringConferenceDiv4").style.display = "none";
        document.getElementById("NONRecurringConferenceDiv6").style.display = "none"; //buffer zone
	    document.getElementById("NONRecurringConferenceDiv7").style.display = "none"; //buffer zone
	 }
    else if(EnableImmConf_Session == "0") //FB 2036 2274
       document.getElementById("NONRecurringConferenceDiv4").style.display = "none"; //t;
    else
       document.getElementById("NONRecurringConferenceDiv4").style.display = t;
	
    //Code Modified For MOJ Phase 2  -End
	document.getElementById("lstDuration_Text").style.width = "75px";
    document.getElementById("confStartTime_Text").style.width = "75px";
    document.getElementById("confEndTime_Text").style.width = "75px";
    document.getElementById("SetupTime_Text").style.width = "75px"; //buffer zone 
    document.getElementById("TeardownTime_Text").style.width = "75px"; //buffer zone 
 	//Merging Recurrence - start
    if(t == "")
    {
        document.getElementById("RecurrenceRow").style.display = 'None';
        document.getElementById("DurationRow").style.display = 'None';
        
    }
    else
    {
        document.getElementById("RecurrenceRow").style.display = '';//Edited For FF...
        document.getElementById("DurationRow").style.display = '';//Edited For FF...
    }
    //Merging Recurrence - end
        
	if (t != "")
	{
	    document.getElementById("<%=RecurFlag.ClientID %>").value="1";
	}
	
	if ("<%=timeZone%>" == "0" )//FB 1425
      document.getElementById("NONRecurringConferenceDiv3").style.display = "none"; 
      
	document.getElementById("EndDateArea").style.display = t;
    document.getElementById("StartDateArea").style.display = t;
    document.getElementById("TeardownArea").style.display = t;
      
      return false;
}


function getYourOwnEmailList (i)
{
	//Code Modified by Offshore for FB 412 - Start(changed frm=approverNET To party2NET)
	//url = "dispatcher/conferencedispatcher.asp?frm=approverNET&frmname=Setup&no=" + i + "&cmd=GetEmailList&emailListPage=1&wintype=pop";
	
	if(i == 999)
//	    url = "dispatcher/conferencedispatcher.asp?frm=party2NET&frmname=Setup&no=" + i + "&cmd=GetEmailList&emailListPage=1&wintype=pop"; //Login Management	
       url = "emaillist2main.aspx?t=e&frm=party2NET&fn=Setup&n=" + i; //Login Management
	else
	    //url = "dispatcher/conferencedispatcher.asp?frm=approverNET&frmname=Setup&no=" + i + "&cmd=GetEmailList&emailListPage=1&wintype=pop"; //Login Management
	    url = "emaillist2main.aspx?t=e&frm=approverNET&fn=Setup&n=" + i; //Login Management
	       	
	//Code Modified by Offshore for FB 412 - End
	if (!window.winrtc) {	// has not yet been defined
		winrtc = window.open(url, "", "width=950,height=450,top=0,left=0,resizable=yes,scrollbars=yes,status=no");//FB 2596
		winrtc.focus();
	} else // has been defined
	    if (!winrtc.closed) {     // still open
	    	winrtc.close();
	        winrtc = window.open(url, "", "width=950,height=450,top=0,left=0,resizable=yes,scrollbars=yes,status=no");//FB 2596
			winrtc.focus();
		} else {
	        winrtc = window.open(url, "", "width=950,height=450,top=0,left=0,resizable=yes,scrollbars=yes,status=no");//FB 2596
	        winrtc.focus();
		}
}


function checkClicks()
{
    var o = window.event.srcElement;
    if (o.tagName == "INPUT" && o.type == "checkbox")
    {
        //alert(o.title);
    } 

}
function goToCal()
{
	roomcalendarview();
}

function openRecur()
{
    //FB 1911
    var args = openRecur.arguments;
    var isarg = "0";
    if(args.length > 0)
        if(args[0] == 'S')
            isarg = "1";
    
	var chkrecurrence = document.getElementById("chkRecurrence");
    
    if(isarg == "1" && chkrecurrence.checked)
    {
        removerecur();        
        chkrecurrence.checked = false;
    }
    
    if(isarg =="0" && chkrecurrence.checked)
        document.getElementById("RecurSpec").value = "";
    
    if(chkrecurrence != null && chkrecurrence.checked == false && isarg == "0")
    {
        removerecur();
        ChangeEndDate();
    }
    else
    {
	    if (ChangeEndDate(1) && ChangeStartDate(1))
	    {
	        var st = document.getElementById("<%=lstConferenceTZ.ClientID%>");
	        //Code added by Offshore for FB Issue 1073 -- Start
	        //var sd = document.getElementById("<%=confStartDate.ClientID%>");
	        var sd = GetDefaultDate(document.getElementById("confStartDate").value,'<%=format%>')        
	        //Code added by Offshore for FB Issue 1073 -- End
	        var stime = document.getElementById("<%=confStartTime.ClientID%>");
	        /* *** code added for buffer zone *** --Start */
	        var setupTime = document.getElementById("SetupTime_Text"); //Edited for FF
	        var teardownTime = document.getElementById("TeardownTime_Text");//Edited for FF
	        var sTime = document.getElementById("hdnSetupTime");
	        var tTime = document.getElementById("hdnTeardownTime");
	       
	         sTime.value  = setupTime.value;
	         tTime.value = teardownTime.value;
	        
	        /* *** code added for buffer zone *** --End */
		   //Merging Recurrence
	       document.getElementById("EndDateArea").style.display = 'None';
	       document.getElementById("StartDateArea").style.display = 'None';
	       document.getElementById("TeardownArea").style.display = 'None';
	       document.getElementById("NONRecurringConferenceDiv2").style.display = 'None';
           
           if(isarg == "1")
           {
                showSpecialRecur();	           
	            top.recurWin = window.open('recurNET.aspx?pn=C&frm=frmSettings2&st='+ st.value + "&sd=" + sd + "&stime=" + stime + "&wintype=pop", 'recur1', 'titlebar=yes,width=650,height=563,resizable=yes,scrollbars=yes,status=yes');//buffer zone
	       }
           else
           {        
	           document.getElementById("NONRecurringConferenceDiv1").style.display = '';
	           if(document.getElementById("chkEnableBuffer").checked == true)	
	           {
	               document.getElementById("NONRecurringConferenceDiv6").style.display = '';                
	               document.getElementById("NONRecurringConferenceDiv7").style.display = '';
	           }
	           document.getElementById("recurDIV").style.display = 'None';       
	           document.getElementById("RecurText").value = '';    
	           document.getElementById("hdnRecurValue").value = 'R';
	           isRecur();
	           initial();
	           fnShow();
	       }
	        //code commented for Merging Recurrence	        
		    //top.recurWin = window.open('recurNET.asp?frm=frmSettings2&st='+ st.value + "&sd=" + sd + "&stime=" + stime + "&wintype=pop", 'recur', 'titlebar=yes,width=600,height=450,resizable=yes,scrollbars=yes,status=yes');
		}
	}	
	fnHideStartNow();//FB 1825
}
//FB 1911
function visControls()
{
    if(document.getElementById("RecurSpec").value == "")
    {
	   document.getElementById("StartDateArea").style.display = '';
	   document.getElementById("NONRecurringConferenceDiv2").style.display = '';
	   document.getElementById("TeardownArea").style.display = '';
	   document.getElementById("EndDateArea").style.display = '';
	   document.getElementById("NONRecurringConferenceDiv1").style.display = '';	   
       document.getElementById("recurDIV").style.display = 'None';     
       document.getElementById("RecurText").value = '';    
       if(document.getElementById("chkEnableBuffer").checked == true)	
       {            
           document.getElementById("NONRecurringConferenceDiv6").style.display = '';                
           document.getElementById("NONRecurringConferenceDiv7").style.display = '';
       }
       else
       {
           document.getElementById("NONRecurringConferenceDiv6").style.display = 'None';                
           document.getElementById("NONRecurringConferenceDiv7").style.display = 'None';
       }
	}
}

function showSpecialRecur()
{
    document.getElementById("recurDIV").style.display = '';
    document.getElementById("NONRecurringConferenceDiv1").style.display = 'None';
    document.getElementById("EndDateArea").style.display = 'None';
    document.getElementById("StartDateArea").style.display = 'None';
    document.getElementById("TeardownArea").style.display = 'None';
    document.getElementById("NONRecurringConferenceDiv2").style.display = 'None';
    document.getElementById("NONRecurringConferenceDiv6").style.display = 'None';                
    document.getElementById("NONRecurringConferenceDiv7").style.display = 'None';
    
}

function roomcalendarview()
{   
    var frm = "<%=confStartDate.Text%>";//FB 1073
    rn = document.getElementById("selectedloc").value //FB 1138
    //rn = getListViewChecked(rn);	//FB 1138
    //dispatcher/admindispatcher.asp?cmd=ManageConfRoom
    
    url = "roomcalendar.aspx?f=v&hf=1&m=" + rn + "&d=" + frm;
    window.open(url,"","left=50,top=50,width=860,height=550,resizable=yes,scrollbars=yes,status=no");
    return false;
}
function roomconflict(confDate) //FB 1154
{   
    var frm = confDate;
    rn = document.getElementById("selectedloc").value
    
    //rn = getListViewChecked(rn);	
    
    //url = "dispatcher/admindispatcher.asp?cmd=ManageConfRoom&f=v&hf=1&m=" + rn  + "&d=" + frm;
    url = "roomcalendar.aspx?f=v&hf=1&m=" + rn + "&d=" + confDate;
    window.open(url,"","left=50,top=50,width=860,height=550,resizable=yes,scrollbars=yes,status=no");
    return false;
}
//FB 1048
function CallMeetingPla()
{   

        var confTimeZone = "<%=lstConferenceTZ.SelectedValue%>";
        var sd = "<%=confStartDate.Text %>";
       // alert(confTimeZone);
        var stime = "<%=confStartTime.Text %>";
        //alert(stime);
        var confDateTime = sd + " " + stime;
        var roomId = document.getElementById("selectedloc").value;

        //roomId = getListViewChecked(roomId); //FB 1138,1142
        rn = roomId.split(",").length ;
        rId = roomId.split(",").length ;



        if(roomId == ' ')
        {
            alert("少なくとも１つ部屋を選んでくさい。")
            return false;
            
        }
        
        
        if(rn==0 || (rn==1 && roomId == ', '))
        {
          alert("少なくとも１つ部屋を選んでくさい。")
          return false;
        }
          // FB 2459 starts
        if (roomId.length < 1)
        {
          alert("少なくとも１つ部屋を選んでくさい。")
          return false;
        }
        // FB 2459 end
        
        if(rn >5)
        {
          alert("５部屋のみ会議企画者を見ることができます。")
          return false;
        }
//         if(rn > 0)
//          alert("Make Sure the Selected Room has TimeZone Value")
       
        url = "MeetingPlanner.aspx?";
                url += "ConferenceDateTime=" + confDateTime + "&";
                url += "ConferenceTimeZone=" + confTimeZone + "&";
                url += "RoomID=" + roomId + ",";
    
	  window.open(url,"","left=50,top=50,width=860,height=550,resizable=yes,scrollbars=yes,status=no");
      return false;
          
      }

function SetConference()
{
//FB 2430 start
  document.getElementById("hdnconftype").value = "";
  if (document.getElementById("hdnCrossEnableSmartP2P").value == "1" && document.getElementById("hdnSmartP2PTotalEps").value == 2)
  {
     if (confirm("この会議の種類は、ポイントツーポイントに変更されており、MCUの接続を使用することはありません"))
        document.getElementById("hdnconftype").value = "4";
       
  }
  //FB 2430 end 
 //FB 1830 Email Edit - start
 if ("<%=isEditMode%>" == "1")
 {
      var hdnalert =   document.getElementById("hdnemailalert");
      var alertreq = hdnalert.value.trim();
      hdnalert.value = "";
      if( alertreq == "2") //Alert user for notify on edit
      {
        var alertmessage = '参加者全員に通知しますか？（OKをクリックして全員に通知するかキャンセルをクリックして新規参加者がいれば通知する）';
        if (confirm(alertmessage))
            hdnalert.value = "1"; //send email to all existing participants
        else
            hdnalert.value = "0"; //dont notify old participants
      }
  }  
  //FB 1830 Email Edit - end
  
  //FB 2274 Starts
 var roomModule_Session ;
 var foodModule_Session ;
 var hkModule_Session ;
 		
 if(document.getElementById("hdnCrossroomModule").value != null)
 	roomModule_Session = document.getElementById("hdnCrossroomModule").value;
 else
 	roomModule_Session = "<%=Session["roomModule"] %>";
 	
 if(document.getElementById("hdnCrossfoodModule").value != null)
 	foodModule_Session = document.getElementById("hdnCrossfoodModule").value;
 else
 	foodModule_Session = "<%=Session["foodModule"] %>";
 	
 if(document.getElementById("hdnCrosshkModule").value != null)
 	hkModule_Session = document.getElementById("hdnCrosshkModule").value;
 else
 	hkModule_Session = "<%=Session["hkModule"] %>";
 //FB 2274 Ends
  
  document.getElementById("btnConfSubmit").style.display = "none";
  //btn.style.display = true;
  var flag = "1";
    if ( (document.getElementById("<%=plblAVWorkOrders.ClientID %>").value == "0") && (roomModule_Session == "1") && ("<%=Session["admin"] %>" != "0") && ("<%=client.ToString().ToUpper() %>" == "WASHU") ) //FB 2274
      if (confirm("あなたがどんなオーディオ/ビジュアル作業指示せずに続行してもよろしいですか？"))
          flag = "1";
      else
          flag = "2";
    if ( (document.getElementById("<%=plblCateringWorkOrders.ClientID %>").value == "0") && (foodModule_Session == "1") && ("<%=Session["admin"] %>" != "0") && ("<%=client.ToString().ToUpper() %>" == "WASHU") ) //FB 2274
        if (confirm("あなたはどんなケータリング作業指示せずに続行してもよろしいですか？"))
            flag = "1";
        else
            flag = "2";
    if ( (document.getElementById("<%=plblHouseKeepingWorkOrders.ClientID %>").value == "0") && (hkModule_Session == "1") && ("<%=Session["admin"] %>" != "0") && ("<%=client.ToString().ToUpper() %>" == "WASHU") ) //FB 2274
        if (confirm("あなたはあらゆる清掃作業オーダーせずに続行してもよろしいですか？"))
            flag = "1";
        else
            flag="2";

  if (flag == "1")
  {
    document.getElementById("__EVENTTARGET").value="btnConfSubmit";
  }
  else if (flag == "2")
  {
      document.getElementById("__EVENTTARGET").value="";
      document.getElementById("btnConfSubmit").style.display = "";
  }
  DataLoading(1);
  WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions("btnConfSubmit", "", true, "", "", false, false));
/*
  if (flag=="1")
    document.getElementById("<%=btnConfSubmit.ClientID %>").disabled = true;
  else if (flag=="2")
    document.getElementById("<%=btnConfSubmit.ClientID %>").disabled = false;
*/
}
//Added by Sudhir Mangla For  Bug no 588

/*  Code Added For FB 1476 - Start  */
function getAGroupDetail(frm, cb, gid)
{
	if (cb == null) {
		if (gid != null) {
			id = gid;
		} else {
			alert("申し訳ございませんがシステムにエラーが生じました。管理者にお問い合わせください。")
			return false;
		}
	} else {

		if (cb.selectedIndex != -1) {
			if (gid != null) id = gid; else id = cb.options[cb.selectedIndex].value;
		} else {
			alert(EN_53);
			return false;
		}
	}
	
	//code added for Managegroup2.asp to aspx convertion 
	if(frm == 'g')
	    gm = window.open("memberallstatus.aspx?GroupID=" + id , "groupmember", "status=no,width=420,height=300,scrollbars=yes,resizable=no");
	else
	    //gm = window.open("memberallstatus.asp?f=" + frm + "&n=" + id + "&wintype=pop", "groupmember", "status=no,width=420,height=300,scrollbars=yes,resizable=yes");
	    gm = window.open("memberallstatus.aspx?GroupID=" + id , "groupmember", "status=no,width=420,height=300,scrollbars=yes,resizable=yes");
	if (gm) gm.focus(); else alert(EN_132);
}
/*  Code Added For FB 1476 - End  */


//Audio Addon
function AudioAddon()
{
   
    var args = AudioAddon.arguments;
    
    var drptyp = document.getElementById("CreateBy");
    var controlName = 'dgUsers_ctl';
    var ctlid = '';
    
    if(drptyp)
    {
        if(drptyp.value == "2")
        {
            if(args)
            {
                var tbl1 = document.getElementById(args[0]);
                var tbl2 = document.getElementById(args[1]);
                var drp = document.getElementById(args[2]);
                var drp2 = document.getElementById(args[3]);
                 
                var usrArr=args[2].split("_");
                var len=usrArr.length;

                if(len > 2)
                {
                    ctlid = usrArr[1];
                    ctlid = ctlid.replace(/ctl/i,'');
                }
                
                var lstAddType = document.getElementById((controlName + ctlid +'_lstAddressType'));
                //var lstProtocol = document.getElementById((controlName + ctlid +'_lstProtocol'));
                var lstConType = document.getElementById((controlName + ctlid +'_lstConnectionType'));
                var txtAdd = document.getElementById((controlName + ctlid +'_txtAddress'));

                var txtaddphone = document.getElementById((controlName + ctlid +'_txtaddphone'));
                var txtconfcode = document.getElementById((controlName + ctlid +'_txtConfCode'));
                var txtleader = document.getElementById((controlName + ctlid +'_txtleaderPin'));
                
//                var lstAddType = document.getElementById((controlName + ctlid +'_reqAddressType'));
//                var lstConType = document.getElementById((controlName + ctlid +'_RequiredFieldValidator3'));
//                var txtAdd = document.getElementById((controlName + ctlid +'_reqAddress'));

//                var txtaddphone = document.getElementById((controlName + ctlid +'_RequiredFieldValidator13'));
//                var txtconfcode = document.getElementById((controlName + ctlid +'_RequiredFieldValidator20'));
//                var txtleader = document.getElementById((controlName + ctlid +'_RequiredFieldValidator19'));
            
                if(drp.value == "2" && '<%=Application["EnableAudioAddOn"]%>' == "1")
                {
                    tbl1.style.display = 'none';
                    tbl2.style.display = 'block';
                    
                    if(lstAddType)
                    {
                        lstAddType.value = '1';
                    }
                    if(lstConType)
                    {
                        lstConType.value = '2';
                    }
                    if(txtAdd)
                    {
                        if(Trim(txtAdd.value) == '')
                            txtAdd.value = '127.0.0.1';
                    }
                }
                else
                {
                    if(txtaddphone)
                    {
                        if(Trim(txtaddphone.value) == '')
                        {
                            txtaddphone.value = '127.0.0.1';
                        }
                    }
                    if(txtconfcode)
                    {
                        if(Trim(txtconfcode.value) == '')
                            txtconfcode.value = '0';
                    }
                    if(txtleader)
                    {
                        if(Trim(txtleader.value) == '')
                            txtleader.value = '0';
                    }
                    tbl1.style.display = 'block';
                    tbl2.style.display = 'none';
                }
                drp2.value = drp.value;
            }
                
        }
    }
}

</script>

<%--Merging Recurrence Script Start Here--%>

<script type="text/javascript">

var openerfrm = document.frmSettings2;


function CheckDate(obj)
{    
    var endDate;//FB 2246
    if ('<%=format%>'=='dd/MM/yyyy')
        endDate = GetDefaultDate(obj.value,'dd/MM/yyyy');
    else
        endDate = obj.value;
        
    if (Date.parse(endDate) < Date.parse(new Date()))
        alert("無効な日付");
}

function fnHide()
{    
    document.getElementById("Daily").style.display = 'none';
    document.getElementById("Weekly").style.display = 'none';
    document.getElementById("Monthly").style.display = 'none';
    document.getElementById("Yearly").style.display = 'none';
    document.getElementById("Custom").style.display = 'none';
    document.getElementById("RangeRow").style.display = 'block';    
}

function fnShow()
{    

    var a = null;
    var f = document.forms[1];
    var e = f.elements["RecurType"];

    for (var i=0; i < e.length; i++)
    {
        if (e[i].checked)
        {
        a = e[i].value;
        document.frmSettings2.RecPattern.value = a;
        break;
        }
    }
    
    if(a != null)
    {
        fnHide();
        switch(a)
        {
            case "1":
	            initialdaily(rpstr);
                document.getElementById("Daily").style.display = 'block';                
                break;
            case "2":
	            initialweekly(rpstr);
                document.getElementById("Weekly").style.display = 'block';                            
                break;
            case "3":
	            initialmonthly(rpstr);
                document.getElementById("Monthly").style.display = 'block';                            
                break;
            case "4":
	            initialyearly(rpstr);
                document.getElementById("Yearly").style.display = 'block';                            
                break;
            case "5":
                document.getElementById('flatCalendarDisplay').innerHTML = "";  // clear the div values
                showFlatCalendar(1, dFormat,document.getElementById("StartDate").value);  //for custom calendar display      
	            initialcustomly(rpstr);
                document.getElementById("Custom").style.display = 'block';                            
                document.getElementById("RangeRow").style.display = 'none';
                break;            
        }
    }      
}


function initial()
{

    var setuphr;
    var setupmin;
    var setupap;
    var teardownhr;
    var teardownmin;
    var teardownap;
    
    document.getElementById("hdnSetupTime").value = document.getElementById("SetupTime_Text").value;
    document.getElementById("hdnTeardownTime").value = document.getElementById("TeardownTime_Text").value;
    
	if (document.getElementById("Recur").value=="") {
		timeMin = document.getElementById("confStartTime_Text").value.split(":")[1].split(" ")[0] ; 
		timeDur = getConfDurNET(document.frmSettings2,dFormat);
		
        if (timeDur > (maxDuration * 60))
            alert(EN_314);
         
         /* *** code changed for buffer zone *** --Start */   
		if("<%=Session["timeFormat"].ToString()%>" == '0')
		    tmpstr = document.getElementById("lstConferenceTZ").options[document.getElementById("lstConferenceTZ").selectedIndex].value + "&" + document.getElementById("confStartTime_Text").value.split(":")[0] + "&" + timeMin + "&" + "-1" + "&" + timeDur + "#"
		else
		    tmpstr = document.getElementById("lstConferenceTZ").options[document.getElementById("lstConferenceTZ").selectedIndex].value + "&" + document.getElementById("confStartTime_Text").value.split(":")[0] + "&" + timeMin + "&" + document.getElementById("confStartTime_Text").value.split(" ")[1] + "&" + timeDur + "#"
		
		tmpstr += "1&1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1#"
		tmpstr += document.getElementById("confStartDate").value + "&1&-1&-1";

 		document.frmSettings2.RecurValue.value = tmpstr;
		document.frmSettings2.RecurPattern.value = "1&1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1";
	}
	else
	{
	    setuptime = document.getElementById("hdnBufferStr").value.split("&")[0];
	    teardowntime = document.getElementById("hdnBufferStr").value.split("&")[1];
	    
	    if(document.getElementById("SetupTime_Text"))
	        document.getElementById("SetupTime_Text").value = setuptime;
	    
	    if(document.getElementById("TeardownTime_Text"))
	        document.getElementById("TeardownTime_Text").value = teardowntime;
	}
     
    
	rpstr = AnalyseRecurStr(document.frmSettings2.RecurValue.value);
	/* *** code Changed for buffer zone *** --End */
	
	// 1.
	//FB 1635 starts
	if("<%=Session["timeFormat"].ToString()%>" == '0')
	{
	    if(atint[1] < 12 && atint[3] == "PM")
             atint[1] = atint[1] + 12;
    }
	//FB 1635 Ends
	if(atint[1] < 10)
	    atint[1] = "0" + atint[1];
	
	//FB 1715
	if(atint[2] < 10)
	    atint[2] = "0" + atint[2];
	
	//FB 1799
	if("<%=Session["timeFormat"].ToString()%>" == '0')
	    startstr = (atint[1] == "12" && atint[3] == "AM"? "00" : atint[1]) + ":" + (atint[2]=="0" ? "00" : atint[2]);
	else
	    startstr = atint[1] + ":" + (atint[2]=="0" ? "00" : atint[2]) + " " + atint[3];
	
	if(document.getElementById("confStartTime_Text"))
	    document.getElementById("confStartTime_Text").value = startstr;
	
	if (document.frmSettings2.RecurDurationhr) {
		set_select_field (document.frmSettings2.RecurDurationhr, parseInt(atint[4]/60, 10) , true);
		set_select_field (document.frmSettings2.RecurDurationmi, atint[4]%60, true);
	}

	document.frmSettings2.RecurPattern.value = rpstr;	
	if(document.frmSettings2.RecurType)
	    document.frmSettings2.RecurType[rpint[0]-1].checked = true;

    document.frmSettings2.Occurrence.value ="";
    document.frmSettings2.EndDate.value ="";
   
	if (rpint[0] != 5) {
		document.frmSettings2.StartDate.value = rrint[0];
	
		switch (rrint[1]) 
		{
			case 1:
		        document.frmSettings2.EndType.checked = true;
				break;
			case 2:
		        document.frmSettings2.REndAfter.checked = true;
				document.frmSettings2.Occurrence.value = rrint[2];
				break;
			case 3:
		        document.frmSettings2.REndBy.checked = true;
				document.frmSettings2.EndDate.value = rrint[3];
				break;
			default:
				alert(EN_36);
				break;
		}
	}
}

function recurTimeChg()
{
   

    var aryStart = document.getElementById("confStartTime_Text").value.split(" ");
	
	rshr = aryStart[0].split(":")[0];
	rsmi = aryStart[0].split(":")[1];
	rsap = aryStart[1];

	if (document.frmSettings2.RecurDurationhr) {
		rdhr = parseInt(document.frmSettings2.RecurDurationhr.value, 10);
		rdmi = parseInt(document.frmSettings2.RecurDurationmi.value, 10);
		recurduration = rdhr * 60 + rdmi;
        if (recurduration > (maxDuration *60))
            alert(EN_314);
		document.frmSettings2.EndText.value = calEnd(calStart(rshr, rsmi, rsap), recurduration);
	}
	
	if (document.frmSettings2.RecurEndhr) {
		rehr = document.frmSettings2.RecurEndhr.options[document.frmSettings2.RecurEndhr.selectedIndex].value;
		remi = document.frmSettings2.RecurEndmi.options[document.frmSettings2.RecurEndmi.selectedIndex].value;
		reap = document.frmSettings2.RecurEndap.options[document.frmSettings2.RecurEndap.selectedIndex].value;
		
		document.frmSettings2.DurText.value = calDur(document.getElementById("confStartDate").value, document.getElementById("confEndDate").value, rshr, rsmi, rsap, rehr, remi, reap, 1);
	}
}

</script>

<%--daily Script--%>
<script type="text/javascript">
function summarydaily()
{
	dg  = Trim(document.frmSettings2.DayGap.value);
	rp  = "1&";
	rp += (document.frmSettings2.DEveryDay.checked) ? ("1&" + ((dg == "") ? "-1" : dg)) : "2&-1";
	rp += "&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1";

	document.frmSettings2.RecurPattern.value = rp;
}

function initialdaily(rp)   
{
	var rpary = (rp).split("&");
	if (rpary[0] == 1) {
		if (!isNaN(rpary[2]))
			dg = parseInt(rpary[2], 10);
		if ((dg == -1) || isNaN(dg))
			document.frmSettings2.DayGap.value = "";
		else
			document.frmSettings2.DayGap.value = dg;
		if (!isNaN(rpary[1]))
			dt =  parseInt(rpary[1], 10);
		if ((dt == -1) || isNaN(dt))
			rpary[1] = 1;
			
		if(rpary[1] == 1)
		    document.frmSettings2.DEveryDay.checked = true;
		else if(rpary[1] == 2)
		    document.frmSettings2.DWeekDay.checked = true;		
	} 
	else 
	{
		document.frmSettings2.DayGap.value = "";
	    document.frmSettings2.DEveryDay.checked = true;
	}
}

</script>

<%--Weekly Script--%>
<script type="text/javascript">

function summaryweekly ()
{
	wg  = Trim(document.frmSettings2.WeekGap.value);
	rp  = "2&-1&-1&";
	rp += ((wg == "") ? "" : wg) + "&";
	
	var elementRef = document.getElementById("WeekDay");
    var checkBoxArray = elementRef.getElementsByTagName('input');
    var checkedValues = '';
	for (var i = 0; i < checkBoxArray.length; i++)
        rp += checkBoxArray[i].checked ? ((i+1) + ",") : ""  
	
	if (rp.substr(rp.length-1,1)==",")
		rp = rp.substr(0, rp.length-1);

	rp += "&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1";

	document.frmSettings2.RecurPattern.value = rp;
}

function initialweekly(rp)
{
    var elementRef = document.getElementById("WeekDay");
    var checkBoxArray = elementRef.getElementsByTagName('input');
	rpary = (rp).split("&");
	if (rpary[0] == 2) {
		if (!(isNaN(rpary[3])))
			wg = parseInt(rpary[3], 10);
		if ((wg == -1) || isNaN(wg))
			document.frmSettings2.WeekGap.value = "1";
		else
			document.frmSettings2.WeekGap.value = wg;
		if (rpary[4] != -1) 
		{
			wdary = (rpary[4]).split(",");
	    
		    for (i = 0; i < wdary.length; i++) 
		        checkBoxArray[parseInt(wdary[i]) - 1].checked = true
		}
	} 
	else 
	{
		document.frmSettings2.WeekGap.value = "1";
  
		for (var i = 0; i < checkBoxArray.length; i++)
		{
            if ( checkBoxArray[i].checked == true )    
			     checkBoxArray[i].checked = false;
		}
	}
}
</script>

<%--Monthly Script--%>
<script type="text/javascript">

function summarymonthly()
{
	mdn  = Trim(document.frmSettings2.MonthDayNo.value);
	mg1  = Trim(document.frmSettings2.MonthGap1.value);
	mg2  = Trim(document.frmSettings2.MonthGap2.value);
	rp  = "3&-1&-1&-1&-1&";
	rp += (document.frmSettings2.MEveryMthR1.checked) ? ("1&" + ((mdn == "") ? "-1" : mdn) + "&" + ((mg1 == "") ? "-1" : mg1) + "&-1&-1&-1") 
		: ("2&-1&-1&" + (document.frmSettings2.MonthWeekDayNo.selectedIndex + 1) + "&" + (document.frmSettings2.MonthWeekDay.selectedIndex + 1) + "&" + ((mg2 == "") ? "-1" : mg2));
	rp += "&-1&-1&-1&-1&-1&-1";

//	alert(rp);
	document.frmSettings2.RecurPattern.value = rp;
}

function initialmonthly(rp)
{
	rpary = (rp).split("&");
	if (rpary[0] == 3) {
		for (i=5; i<11; i++) {
			if (!isNaN(rpary[i]))
				rpary[i] = parseInt(rpary[i], 10);
			else
				rpary[i] = -1;
		}
		
		switch (rpary[5]) {
			case 1:
				document.frmSettings2.MEveryMthR1.checked = true;
				document.frmSettings2.MonthDayNo.value = (rpary[6] == -1) ? "" : rpary[6];
				document.frmSettings2.MonthGap1.value = (rpary[7] == -1) ? "" : rpary[7];
				document.frmSettings2.MonthWeekDayNo[0].checked = true;
				document.frmSettings2.MonthWeekDay[0].checked = true;
				document.frmSettings2.MonthGap2.value = "";
				break;
			case 2:
				document.frmSettings2.MEveryMthR2.checked = true;
				document.frmSettings2.MonthDayNo.value = "";
				document.frmSettings2.MonthGap1.value = "";
				if (rpary[8] == -1)
					document.frmSettings2.MonthWeekDayNo[0].selected = true;
				else
					document.frmSettings2.MonthWeekDayNo[rpary[8]-1].selected = true;
				if (rpary[9] == -1)
					document.frmSettings2.MonthWeekDay[0].selected = true;
				else
					document.frmSettings2.MonthWeekDay[rpary[9]-1].selected = true;
				document.frmSettings2.MonthGap2.value = (rpary[10] == -1) ? "" : rpary[10];
				break;
		}
	} 
	else 
	{
		document.frmSettings2.MEveryMthR1.checked = true;
		document.frmSettings2.MonthDayNo.value = "";
		document.frmSettings2.MonthGap1.value = "";
		document.frmSettings2.MonthWeekDayNo[0].checked = true;
		document.frmSettings2.MonthWeekDay[0].checked = true;
		document.frmSettings2.MonthGap2.value = "";
	}
}




</script> 

<%--Yearly Script--%>
<script type="text/javascript">

function summaryyearly()
{
	ymd  = Trim(document.frmSettings2.YearMonthDay.value);
	rp  = "4&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&";
	rp += (document.frmSettings2.YEveryYr1.checked) ? ("1&" + (document.frmSettings2.YearMonth1.selectedIndex + 1) + "&" + ((ymd == "") ? "-1" : ymd) + "&-1&-1&-1") 
		: ("2&-1&-1&" + (document.frmSettings2.YearMonthWeekDayNo.selectedIndex + 1) + "&" + (document.frmSettings2.YearMonthWeekDay.selectedIndex + 1) + "&" + (document.frmSettings2.YearMonth2.selectedIndex + 1));
	
	document.frmSettings2.RecurPattern.value = rp;
}

function initialyearly(rp)
{   
	rpary = (rp).split("&");
	if (rpary[0] == 4) 
	{
		for (i=11; i<17; i++) 
		{
			if (!isNaN(rpary[i]))
				rpary[i] = parseInt(rpary[i], 10);
			else
				rpary[i] = -1;
		}
		switch (rpary[11]) 
		{
			case 1:
				document.frmSettings2.YEveryYr1.checked = true;
				if (rpary[12] == -1)
					document.frmSettings2.YearMonth1[0].selected = true;
				else
					document.frmSettings2.YearMonth1[rpary[12]-1].selected = true;
				document.frmSettings2.YearMonthDay.value = (rpary[13] == -1) ? "" : rpary[13];
				document.frmSettings2.YearMonthWeekDayNo[0].selected = true;
				document.frmSettings2.YearMonthWeekDay[0].selected = true;
				document.frmSettings2.YearMonth2[0].selected = true;
				break;
			case 2:
				document.frmSettings2.YEveryYr2.checked = true;
				document.frmSettings2.YearMonth1[0].selected = true;
				document.frmSettings2.YearMonthDay.value = "";
				if (rpary[14] == -1)
					document.frmSettings2.YearMonthWeekDayNo[0].selected = true;
				else
					document.frmSettings2.YearMonthWeekDayNo[rpary[14]-1].selected = true;
				if (rpary[15] == -1)
					document.frmSettings2.YearMonthWeekDay[0].selected = true;
				else
					document.frmSettings2.YearMonthWeekDay[rpary[15]-1].selected = true;
				if (rpary[16] == -1)
					document.frmSettings2.YearMonth2[0].selected = true;
				else
					document.frmSettings2.YearMonth2[rpary[16]-1].selected = true;
				break;
		}
	} 
	else 
	{
		document.frmSettings2.YEveryYr1.checked = true;
		document.frmSettings2.YearMonth1[0].selected = true;
		document.frmSettings2.YearMonthDay.value = "";
		document.frmSettings2.YearMonthWeekDayNo[0].selected = true;
		document.frmSettings2.YearMonthWeekDay[0].selected = true;
		document.frmSettings2.YearMonth2[0].selected = true;
	}
}
</script>

<%--Custom Script--%>
<script  type="text/javascript" language="JavaScript">

function removedate(cb)
{
	if (cb.selectedIndex != -1) {
	    cb.options[cb.selectedIndex] = null;
	}
	cal.refresh();
}

function initialcustomly(seldates)
{
	if (seldates != "") 
	{	
		seldatesary = seldates.split("&");
	
		cb = document.getElementById("CustomDate");
		//cb = document.getElementById("StartDate");
		if(seldatesary[0].length > 2)
		{
		    for (var i = 0; i < seldatesary.length; i++)
			    chgOption(cb, seldatesary[i], seldatesary[i], false, false)	
    	
		    if (cal)	
			    cal.refresh();
	    }
	}
}

function summarycustomly()
{
	var selecteddate = "";
	dFormat = document.getElementById("HdnDateFormat").value;	
	SortDates();
	cb = document.getElementById("CustomDate");
	for (i=0; i<cb.length; i++)
		selecteddate += cb.options[i].value + "&";

	document.frmSettings2.CutomDates.value = (selecteddate == "") ? "" : selecteddate.substring(0, selecteddate.length-1);
}

function isOverInstanceLimit(cb)
{
	csl = parseInt("<%=CustomSelectedLimit%>");

	if (!isNaN(csl)) {
		if (cb.length >= csl) {
			alert(EN_211)
			return true;
		}
	}
	
	return false;
}

function SortDates()
{
	var temp;

	datecb = document.frmSettings2.CustomDate;
	
	var dateary = new Array();

	for (var i=0; i<datecb.length; i++) {
		dateary[i] = datecb.options[i].value;
		
		dateary[i] = ( (parseInt(dateary[i].split("/")[0], 10) < 10) ? "0" + parseInt(dateary[i].split("/")[0], 10) : parseInt(dateary[i].split("/")[0], 10) ) + "/" + ( (parseInt(dateary[i].split("/")[1], 10) < 10) ? "0" + parseInt(dateary[i].split("/")[1], 10) : parseInt(dateary[i].split("/")[1], 10) ) + "/" + ( parseInt(dateary[i].split("/")[2], 10) );
	}

	for (i=0;i<dateary.length-1;i++)
	 	for(j=i+1;j<dateary.length;j++)
			if (mydatesort(dateary[i], dateary[j]) > 0)
			{
				temp = dateary[i];
				dateary[i] = dateary[j];
				dateary[j] = temp;	
			}


	for (var i=0; i<dateary.length; i++) {
		datecb.options[i].text = dateary[i];
		datecb.options[i].value = dateary[i];		
	} 

return false;
}

//-->
</script>

<%--Submit Recurence --%>
<script type="text/javascript">

function SubmitRecurrence()
{
    var chkrecurrence = document.getElementById("chkRecurrence");
    
    if(chkrecurrence != null && chkrecurrence.checked == true)
    {
        if (validateConfDuration())    //Buffer zone
        {
            if(summary())
                return true;
        }
        return false;        
    }
}

function validateDurationHr()
{
    var obj = document.getElementById("RecurDurationhr");
    
    if (obj.value == "") //FB Case 951
         obj.value = "0";
         
    if (isNaN(obj.value)) 
    {
        alert(EN_314);
        return false;
    }

    var maxdur = '<%= Application["MaxConferenceDurationInHours"] %>';
    
    if(maxdur == "")
        maxdur = "120";
    
    if (obj.value < 0 || eval(obj.value) > eval(maxdur))
    {
        alert(EN_314);
        return false;
    }
    return true;
}

function validateDurationMi()
{
    var obj = document.getElementById("RecurDurationmi");
    if (obj.value == "") // FB Case 951
        obj.value = "0";
    if (isNaN(obj.value))
    {
        alert(EN_314);
        obj.focus();
        return false;
    }
    return true;
}

/* Methods added for Buffer zone - start */
function validateConfDuration()
{
    var obj = document.getElementById("RecurDurationhr");
    var obj1 = document.getElementById("RecurDurationmi");
    
    
    if (obj.value == "") //FB Case 951
         obj.value = "0";
         
    if (obj1.value == "") //FB Case 951
        obj1.value = "0";
   
         
    if (isNaN(obj.value)) 
    {
        alert(EN_314);
        return false;
    }
    if (isNaN(obj1.value)) 
    {
        alert(EN_314);
        return false;
    }

    var maxdur = '<%= Application["MaxConferenceDurationInHours"] %>';
    
    if(maxdur == "")
        maxdur = "24";

    var totDur = parseInt(obj.value) * 60;
    totDur = totDur + parseInt(obj1.value);
    
    if (totDur < 0 || totDur > eval(maxdur*60))
    {
        alert(EN_314);
        return false;
    }
  
    return true;
}


/* *** Methods added for buffer zone *** -- End */

function summary()
{
     SetRecurBuffer();//SJV BufferZone Fix
    
    
    switch(document.frmSettings2.RecPattern.value)
    {
        case "1":
		    summarydaily();
		    break;
		case "2":	
		    summaryweekly();
		    break;
        case "3":
		    summarymonthly();
		    break;
	    case "4":
		    summaryyearly();
		    break;
        case "5":
		    summarycustomly();
		    if(document.frmSettings2.CutomDates.value != "")
		    {
		        var cuDates = document.frmSettings2.CutomDates.value.split("&");
		        if(cuDates.length <=1)
		        {
		            alert("定例会議は少なくとも２回行われないといけません。会議を送信する前に繰り返しパターンを変更してください。");
		            return false;   
		        }
		    }
		    break;
    }
    
     if(document.frmSettings2.RecPattern.value != "5" && document.frmSettings2.REndAfter.checked)
    {
       if(document.frmSettings2.Occurrence.value <= 1)
       {
           alert("定例会議は少なくとも２回行われないといけません。会議を送信する前に繰り返しパターンを変更してください。");
           document.frmSettings2.Occurrence.focus();
           return false;   
       }
    }

	var aryStart = document.getElementById("confStartTime_Text").value.split(" ");
	
	sh = aryStart[0].split(":")[0];
	sm = aryStart[0].split(":")[1];
	ss = aryStart[1];
	tz = document.getElementById("<%=lstConferenceTZ.ClientID%>").value;
	
	if("<%=Session["timeFormat"].ToString()%>" == '0')
	{
	    if(sh != "")
	    { 
	        if(eval(sh) >= 12)
	            ss = "PM";
	        else
	            ss = "AM";
	    }
	}
	
	//code added for buffer zone -Start
	var aryStup = document.getElementById("SetupTime_Text").value.split(" ");
	
	setuphr = aryStup[0].split(":")[0];
	setupmi = aryStup[0].split(":")[1];
	setupap = aryStup[1];

	var aryTear = document.getElementById("TeardownTime_Text").value.split(" ");
	tearhr = aryTear[0].split(":")[0];
	tearmi = aryTear[0].split(":")[1];
	tearap = aryTear[1];
	
	if("<%=Session["timeFormat"].ToString()%>" == '0')
	{ 
	    startTime = sh + ":" + (sm =="0" ? "00" : sm);
	    setupTime = setuphr + ":" + (setupmi =="0" ? "00" : setupmi);
        teardownTime =  tearhr + ":" + (tearmi =="0" ? "00" : tearmi);
    }
	else
	{
	    startTime = sh + ":" + (sm =="0" ? "00" : sm) + " " + ss;
	    setupTime = setuphr + ":" + (setupmi =="0" ? "00" : setupmi) + " " + setupap;
        teardownTime =  tearhr + ":" + (tearmi =="0" ? "00" : tearmi) + " " + tearap;
    }
    
    var insStDate = '';
    if (document.frmSettings2.RecPattern.value == "5")
    {
        var cb = document.getElementById("CustomDate");

	    if(cb.length > 0)
	    {
		    insStDate = cb.options[0].value;
        }
    }
    else
    {
        insStDate = document.frmSettings2.StartDate.value;
    }
    startdate = insStDate + " " + startTime;
   
    var obj = document.getElementById("RecurDurationhr");
    var obj1 = document.getElementById("RecurDurationmi");
    
    var totDur = parseInt(obj.value) * 60;
    totDur = totDur + parseInt(obj1.value);
    
    if(totDur > 1440)
    {
        alert(EN_314);
        return false;
    }
    
    var confEndDt = '';
    var sysdate = dateAddition(startdate,"m",totDur);
    var dateP = sysdate.getDate();
	
	var monthN = sysdate.getMonth() + 1;
	var yearN = sysdate.getFullYear();
	var hourN = sysdate.getHours();
	var minN = sysdate.getMinutes();
	var secN = sysdate.getSeconds();
	var timset = 'AM';
	
	if("<%=Session["timeFormat"].ToString()%>" == '0')
	{
	    timset = '';
	}
	else
	{
	    if(hourN == 12)
	    {
	        timset = 'PM';
	    }
	    else if( hourN > 12)
	    {
	         timset = 'PM';
	         hourN = hourN - 12;
	    }
	    else if(hourN == 0)
	    {
	        timset = "AM";
	        hourN = 12;
	    }
	 }
	
	var confDtAlone = monthN + "/" + dateP + "/" + yearN;
	
	confEndDt = monthN + "/" + dateP + "/" + yearN + " "+ hourN + ":" + minN + ":" + secN +" "+ timset;
	
	if(document.getElementById("chkEnableBuffer").checked == true)	
	{
        setupDate = insStDate + " " + setupTime;
        if(Date.parse(setupDate) < Date.parse(startdate))
        {
            setupDate = confDtAlone + " " + setupTime;
        }
        
        teardownDate = confDtAlone + " " + teardownTime;
        
        if(Date.parse(confEndDt) < Date.parse(teardownDate))
        {
            teardownDate = insStDate + " " + teardownTime;
        }
      
        if(Date.parse(setupDate) < Date.parse(startdate))
        {
            alert(EN_316);
            return false;
        }

        if( (Date.parse(teardownDate) < Date.parse(setupDate)) || (Date.parse(teardownDate) < Date.parse(startdate)) || (Date.parse(teardownDate) > Date.parse(confEndDt)))
        {
             alert(EN_317);
             return false;
        }
      
        var setupDurMin = parseInt(Date.parse(setupDate) - Date.parse(startdate)) / (1000 * 60);
        var tearDurMin = parseInt(Date.parse(confEndDt) - Date.parse(teardownDate)) / (1000 * 60);
      }
    else
    {
        setupDate = startdate;
        teardownDate = confEndDt;
    }  
    
    if(isNaN(setupDurMin))
        setupDurMin = 0;
    
    if(isNaN(tearDurMin))
        tearDurMin = 0;
        
    if(setupDurMin >0 || tearDurMin > 0)
    {
        if( (totDur -(setupDurMin + tearDurMin)) < 15)
        {
            alert(EN_314);
            return false;
        }
    }
    
    document.frmSettings2.hdnBufferStr.value = setupTime + "&" + teardownTime;
	document.frmSettings2.hdnSetupTime.value = setupDurMin;
	document.frmSettings2.hdnTeardownTime.value = tearDurMin;
		
	//code added for buffer zone -End
	
	if (document.frmSettings2.RecurDurationhr)
		dr = parseInt(document.frmSettings2.RecurDurationhr.value, 10) * 60 + parseInt(document.frmSettings2.RecurDurationmi.value, 10);
	if (document.frmSettings2.RecurEndhr) {
		rehr = document.frmSettings2.RecurEndhr.options[document.frmSettings2.RecurEndhr.selectedIndex].value;
		remi = document.frmSettings2.RecurEndmi.options[document.frmSettings2.RecurEndmi.selectedIndex].value;
		reap = document.frmSettings2.RecurEndap.options[document.frmSettings2.RecurEndap.selectedIndex].value;
		
		dr = calDur(sh, sm, ss, rehr, remi, reap, 0);
	}
	if ( dr < 15 ) {
		alert(EN_31);
		
		if (document.frmSettings2.RecurDurationhr)
			document.frmSettings2.RecurDurationhr.focus();
		if (document.frmSettings2.RecurEndhr)
			document.frmSettings2.RecurEndhr.focus();
			
		return (false);		
	}

	
	rv = tz + "&" + sh + "&" + sm + "&" + ss + "&" + dr + "#";
	
	recurrange = document.frmSettings2.StartDate.value + "&" + (document.frmSettings2.EndType.checked ? "1&-1&-1" : ( document.frmSettings2.REndAfter.checked ? ("2&" + document.frmSettings2.Occurrence.value + "&-1") : (document.frmSettings2.REndBy.checked ? ("3&-1&" + document.frmSettings2.EndDate.value) : "-1&-1&-1") ));
	rv += (document.frmSettings2.RecPattern.value == 5) ? ("5#" + document.frmSettings2.CutomDates.value) : (document.frmSettings2.RecurPattern.value + "#" + recurrange);
	if (frmRecur_Validator(document.frmSettings2.RecurPattern.value, recurrange, (document.frmSettings2.RecPattern.value == 5)?true:false)) {
		document.getElementById("Recur").value = rv;
	
		if (document.frmSettings2.EndText)
			endvalue = document.frmSettings2.EndText.value;
		if (document.frmSettings2.RecurEndhr)
			endvalue = document.frmSettings2.RecurEndhr.value + ":" + document.frmSettings2.RecurEndmi.value + " " + document.frmSettings2.RecurEndap.value;

		//merging Conference
		document.getElementById("RecurringText").value = recur_discription(rv, endvalue, document.frmSettings2.TimeZoneText.value, document.frmSettings2.StartDate.value,"<%=Session["timeFormat"].ToString()%>","<%=Session["timezoneDisplay"].ToString()%>");
		isRecur();
		
		BtnCheckAvailDisplay ();
		
		return true;
	}
}


function frmRecur_Validator(rp, rr, isCustomly)
{
	if (parseInt(document.frmSettings2.Occurrence.value) > parseInt("<%=Application["confRecurrence"]%>"))
	{
		alert("会議の最大限度 " + "<%=Application["confRecurrence"]%>" + " 繰り返しシリーズにおける");
		return false;
	}
	
	if (isCustomly) {
		if (document.frmSettings2.CutomDates.value == "") {
			alert(EN_193)
			return false;
		} else
			return true;
	}

	if (chkPattern(rp))
		if (chkRange(rr))
			return true;
		else
			return false;
	else
		return false;
}


function chkPattern(rp)
{
	rpary = rp.split("&");
	
	switch (rpary[0]) {
		case "1":
			for (i=3; i<rpary.length; i++) {
				if  (rpary[i]!= "-1") {
					alert(EN_37);
					return false;
				}
			}

			switch (rpary[1]) {
				case "1":
					rst = isPositiveInt (rpary[2], "interval");
					if (rst == 1)
						return true;
					else
						return false;
					break;
				case "2":
					if (rpary[2] == "-1")
						return true;						
					else {
						alert(EN_37);
						return false;						
					}				
					break;
				default:
					alert(EN_38);
					return false;
					break;
			}
			break;

		case "2":
			for (i=1; i<3; i++) {
				if  (rpary[i]!= "-1") {
					alert(EN_37);
					return false;
				}
			}
			for (i=5; i<rpary.length; i++) {
				if  (rpary[i]!= "-1") {
					alert(EN_37);
					return false;
				}
			}
			rst = isPositiveInt (rpary[3], "weeks interval");
			if (rst == 1) {
				if (rpary[4]!="")
					return true;
				else {
					alert(EN_107);
					return false;
				}
			}
			break;
			
		case "3":
			for (i=1; i<5; i++) {
				if  (rpary[i]!= "-1") {
					alert(EN_37);
					return false;
				}
			}
			for (i=11; i<rpary.length; i++) {
				if  (rpary[i]!= "-1") {
					alert(EN_37);
					return false;
				}
			}
			
			switch (rpary[5]) {
				case "1":
					for (i=8; i<11; i++) {
						if  (rpary[i]!= "-1") {
							alert(EN_37);
							return false;
						}
					}
					if ( (isPositiveInt (rpary[6], "days interval") == 1) && (isPositiveInt (rpary[7], "months interval") == 1) )
						if ( isMonthDayNo(parseInt(rpary[6], 10)) )
							return true;
						else
							return false;
					else
						return false;
					break;
				case "2":
					for (i=6; i<8; i++) {
						if  (rpary[i]!= "-1") {
							alert(EN_37);
							return false;
						}
					}
					if (isPositiveInt(rpary[10], "months interval") == "1")
						return true;						
					else {
						return false;						
					}				
					break;
				default:
					alert(EN_38);
					return false;
					break;
			}
			break;
			
		case "4":
			for (i=1; i<11; i++) {
				if  (rpary[i]!= "-1") {
					alert(EN_37);
					return false;
				}
			}
			switch (rpary[11]) {
				case "1":
					for (i=14; i<rpary.length; i++) {
						if  (rpary[i]!= "-1") {
							alert(EN_37);
							return false;
						}
					}
					if (isPositiveInt (rpary[13], "date of the month") == 1)
						if ( isYearMonthDay(parseInt(rpary[12], 10), parseInt(rpary[13], 10)) )
							return true;
						else
							return false;
					else
						return false;						
					break;
				case "2":
					for (i=12; i<14; i++) {
						if  (rpary[i]!= "-1") {
							alert(EN_37);
							return false;
						}
					}
					return true;
					break;
				default:
					alert(EN_38);
					return false;
					break;
			}
			break;
		default:
			alert(EN_38);
			return false;
			break;
	}
}

var british = false;//FB 1073

function chkRange(rr)
{
	rrary = rr.split("&");
    
    if('<%=Session["FormatDateType"]%>' == 'dd/MM/yyyy')//FB 1073
        british = true;//FB 1073
    
    var dDate = GetDefaultDate(rrary[0],dFormat);
     
	if ( (!isValidDate(rrary[0])) || ( caldatecompare(dDate, servertoday) == -1 ) ) {		// check start time is reasonable future time
		alert(EN_74);
		document.frmSettings2.StartDate.focus();
		return (false);
	}

	switch (rrary[1]) {
		case "1":
			if ( (rrary[2]!= "-1") && (rrary[3]!= "-1") ) {
				alert(EN_38);
				return false;
			} else 
				return true;
			break;
		case "2":
			if (rrary[3]!= "-1") {
				alert(EN_38);
				return false;
			} else {
				if ( isPositiveInt(rrary[2], "times of occurrences")==1 )
					return true;
				else {
					document.frmSettings2.Occurrence.focus();
					return false;
				}
			}
			break;
		case "3":
			if (rrary[2]!= "-1") {
				alert(EN_38);
				return false;
			} else {
				if ( (!isValidDate(rrary[3])) ||  (caldatecompare(GetDefaultDate(rrary[3],dFormat), servertoday)== -1) ) {
					alert(EN_108);
					document.frmSettings2.EndDate.focus();
					return false;
				} else
			    {
			       var fstdate = rrary[0];  //FB 2366
			       var snddate = rrary[3];
			    	if(!british)
				     {
				        fstdate = GetDefaultDate(rrary[0],"dd/MM/yyyy");
				        snddate = GetDefaultDate(rrary[3],"dd/MM/yyyy");				         
				     }
					if ( !dateIsBefore(fstdate,snddate) ) {
						alert(EN_109);
						document.frmSettings2.EndDate.focus();
						return false;
					} else
						return true;
				}
			}
			return false;
			break;
		default:
			alert(EN_38);
			return false;
			break;
	}	
}

function BtnCheckAvailDisplay ()
{		
	if ( (e = document.getElementById("btnCheckAvailDIV")) != null ) {
		e.style.display = (document.getElementById("Recur").value=="") ? "" : "none";
	}
}
</script>

<script>
function removerecur()
{
	document.getElementById("Recur").value=""; 
	//document.getElementById("RecurText").value = "no recurrence";
	document.getElementById("RecurringText").value = "";
	document.getElementById("hdnRecurValue").value = "";
	document.getElementById('RecurValue').value = "";
	isRecur(); 
	initial();
    
    return false;
}

//function goToCallist()
//{
//        if(document.getElementById("lstCalendar") != null)
//        {
//            if (document.getElementById("lstCalendar").value == "1"){
//			    window.location.href = "ConferenceSetup.aspx?t=n&op=1" ; 
//		    }
//		    
//		    if (document.getElementById("lstCalendar").value == "2"){
//			    window.location.href = "ConferenceSetup.aspx?t=n&op=2" ; 
//		    }
//		    
//		    if (document.getElementById("lstCalendar").value == "3"){
//			    window.location.href = "ManageTemplate.aspx" ; 
//		    }
//		}
//        
//		
//}

</script>

<%--Merging Recurrence script End here--%>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>会議のセットアップ</title>
    <%--Merging  Recurrence start--%>
    <link rel="StyleSheet" href="css/divtable.css" type="text/css" />
    <link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/border-table.css"/>
    <link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/main-table.css"/>
    <link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/main.css"/> 
    <link rel="stylesheet" title="Expedite base styles" type="text/css" href="<%=Session["OrgCSSPath"]%>" />   
    <link rel="stylesheet" type="text/css" media="all" href="css/calendar-blue.css"/> <%--FB 1982--%>
     <style type="text/css">
            .blackxxxsboldtext label { font-size: 11px; vertical-align:text-top;FONT-WEIGHT: bold; } 
            .blackxxxstext label { vertical-align:text-top;}  
     </style>
	<%--Merging  Recurrence end--%>
</head>
<body>
    <form id="frmSettings2" name="frmSettings2" defaultbutton="btnNext" runat="server" method="post" enctype="multipart/form-data"> <%--FB 1931--%>
    <div>
    <asp:ScriptManager ID="CalendarScriptManager" runat="server" AsyncPostBackTimeout="600">
                 </asp:ScriptManager>
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <%--<tr>
            <td width="30%" align="right" nowrap  class="blackblodtext">
            <table>
            <tr>
            <td colspan="2">
            <b>Meeting Type:</b>
            <select id="lstCalendar" name="lstCalendar" class="altText" size="1" onChange="goToCallist()" runat="server">
                    <option value="1">Future</option>
                    <option value="2">Immediate</option>
                    <option value="3">From Template</option>
                </select>
            </td>
            
            </tr>
            
            
            </table> 
			    
            </td>
    
        </tr>--%>
        <tr>
            <td>
            <table width="100%">
              <tr>
                <td align="center" style="height: 21px">
                    <asp:Label ID="errLabel" runat="server" CssClass="lblError" Font-Size="Small"></asp:Label>
                    <asp:HiddenField ID="emailClient" runat="server" />
                    <asp:HiddenField ID="hostEmail" runat="server" />
                    <input type="hidden" runat="server" id="CreateBy" value="" />
                    <input type="hidden" id="hdnSetStartNow" runat="server" /><%--FB 1825--%>
                    <input type="hidden" name="ModifyType" value="0"/> <%--Edited for FF--%>
                    <input type="hidden" name="Recur" id="Recur" runat="server" />
                    <input id="confPassword" runat="server" type="hidden" />
                    <input runat="server" id="RecurFlag" type="hidden" />
                    <input runat="server" id="selectedloc" type="hidden" />
                    <input ID="RecurringText" runat="server" type="hidden" />
                    <input type="hidden" name="ConfID" value="">
                    <input type="hidden" name="NeedInitial" value="2">
                    <input type="hidden" name="outlookEmails" value="">
                    <input type="hidden" name="lotusEmails" value="">
                     <input type="hidden" name="hdnSetupTime" id="hdnSetupTime" runat="server" /> 
                    <input type="hidden" name="hdnTeardownTime" id="hdnTeardownTime" runat="server" />
                    <input type="hidden" name="hdnBufferStr" id="hdnBufferStr" runat="server" />
                    <input type="hidden" name="hdnextusrcnt" id="hdnextusrcnt" runat="server" /> <%-- API PortNo --%>
                    <%--<input type="hidden" name="hdnConceirgeSupp" id="hdnConceirgeSupp" runat="server" />--%> <%--FB 2023--%><%--FB 2377--%>
                    <div id="dataLoadingDIV" style="z-index:1"></div>
                    <asp:DropDownList ID="lstServices" Visible="false" runat="server" DataTextField="Name" DataValueField="ID" OnInit="LoadCateringServices" ></asp:DropDownList>
                    
                    <%--Merging Recurrence - Start--%>
                    <input type="hidden" id="TimeZoneText" name="TimeZoneText" value=""/>
                    <input type="hidden" id="TimeZoneValues" name="TimeZoneValues" value=""/>
                    <input type="hidden" id="EndDateText" name="EndDateText" value=""/>
                    <input type="hidden" id="RecurValue" name="RecurValue" value=""/>
                    <input type="hidden" id="RecurPattern" name="RecurPattern" value=""/>
                    <input type="hidden" id="CustomDates" name="CutomDates" value=""/>
                    <input type="hidden" id="RecPattern" name="RecPattern" value=""/>
                    <input type="hidden" id="HdnDateFormat" name="HdnDateFormat" value="<%=format %>" /> 
                    <input type="hidden" id="hdnValue" runat="server"/>
                    <input type="hidden" id="hdnRecurValue" runat="server"/>
                    <%--Merging Recurrence - End--%>
                    <%--FB 1716--%>
                    <input runat="server" id="hdnChange" type="hidden" />
                    <input runat="server" id="hdnDuration" type="hidden" />
                    <input runat="server" name="hdnemailalert" id="hdnemailalert" type="hidden" /> <%--FB 1830 Email Edit--%>
					<input type="hidden" name="Recur" id="hdnParty" runat="server" /> <%--FB 1865--%>
					<%--FB 1911--%>
                    <input runat="server" id="hdnSpecRec" type="hidden" />
                    <input type="hidden" name="Recur" id="RecurSpec" runat="server" />
                    <%--FB 1985--%>
                    <input type="hidden" name="hdnMeetLinkSt" id="hdnMeetLinkSt" runat="server" />
                    <input type="hidden" name="hdnAudioBridges" id="hdnAudioBridges" runat="server" /><%--FB 2359--%>
                    <input type="hidden" name="hdnAVParamState" id="hdnAVParamState" runat="server" /><%--FB 2359--%>
					<%--FB 2274 Starts--%>
                    <input type="hidden" name="hdnCrossrecurEnable" id="hdnCrossrecurEnable" runat="server" />
                    <input type="hidden" name="hdnCrossdynInvite" id="hdnCrossdynInvite" runat="server" />
                    <input type="hidden" name="hdnCrossroomModule" id="hdnCrossroomModule" runat="server" />
                    <input type="hidden" name="hdnCrossfoodModule" id="hdnCrossfoodModule" runat="server" />
                    <input type="hidden" name="hdnCrosshkModule" id="hdnCrosshkModule" runat="server" />
                    <input type="hidden" name="hdnCrossisVIP" id="hdnCrossisVIP" runat="server" />
                    <input type="hidden" name="hdnCrossEnableRoomServiceType" id="hdnCrossEnableRoomServiceType" runat="server" />                   
                    <input type="hidden" name="hdnCrossisSpecialRecur" id="hdnCrossisSpecialRecur" runat="server" />
                    <input type="hidden" name="hdnCrossConferenceCode" id="hdnCrossConferenceCode" runat="server" />
                    <input type="hidden" name="hdnCrossLeaderPin" id="hdnCrossLeaderPin" runat="server" />
                    <input type="hidden" name="hdnCrossAdvAvParams" id="hdnCrossAdvAvParams" runat="server" />
                    <input type="hidden" name="hdnCrossEnableBufferZone" id="hdnCrossEnableBufferZone" runat="server" />
                    <input type="hidden" name="hdnCrossEnableEntity" id="hdnCrossEnableEntity" runat="server" />
                    <input type="hidden" name="hdnCrossAudioParams" id="hdnCrossAudioParams" runat="server" />
                    <input type="hidden" name="hdnCrossdefaultPublic" id="hdnCrossdefaultPublic" runat="server" />
                    <input type="hidden" name="hdnCrossP2PEnable" id="hdnCrossP2PEnable" runat="server" />
                    <input type="hidden" name="hdnCrossEnableRoomConfType" id="hdnCrossEnableRoomConfType" runat="server" />
                    <input type="hidden" name="hdnCrossEnableAudioVideoConfType" id="hdnCrossEnableAudioVideoConfType" runat="server" />
                    <input type="hidden" name="hdnCrossDefaultConferenceType" id="hdnCrossDefaultConferenceType" runat="server" />
                    <input type="hidden" name="hdnCrossEnableAudioOnlyConfType" id="hdnCrossEnableAudioOnlyConfType" runat="server" />
                    <input type="hidden" name="hdnCrossenableAV" id="hdnCrossenableAV" runat="server" />
                    <input type="hidden" name="hdnCrossenableParticipants" id="hdnCrossenableParticipants" runat="server" />
                    <input type="hidden" name="hdnCrossisMultiLingual" id="hdnCrossisMultiLingual" runat="server" />
                    <input type="hidden" name="hdnCrossroomExpandLevel" id="hdnCrossroomExpandLevel" runat="server" />
                    <input type="hidden" name="hdnCrossEnableImmConf" id="hdnCrossEnableImmConf" runat="server" />
                    <input type="hidden" name="hdnCrossEnableAudioBridges" id="hdnCrossEnableAudioBridges" runat="server" />
                    <input type="hidden" name="hdnCrossEnablePublicConf" id="hdnCrossEnablePublicConf" runat="server" />
                    <input type="hidden" name="hdnCrossEnableConfPassword" id="hdnCrossEnableConfPassword" runat="server" />
                    <input type="hidden" name="hdnCrossEnableRoomParam" id="hdnCrossEnableRoomParam" runat="server" />
                    <input type="hidden" name="hdnCrossAddtoGroup" id="hdnCrossAddtoGroup" runat="server" />
                    <input type="hidden" name="hdnCrossEnableVMR" id="hdnCrossEnableVMR" runat="server" /> <%--FB 2376--%>
					<input type="hidden" name="hdnCrossEnablePC" id="hdnCrossEnablePC" runat="server" /><%--FB 2348--%>
					<input type="hidden" name="hdnCrossEnableSurvey" id="hdnCrossEnableSurvey" runat="server" /><%--FB 2348--%>
                    <%--FB 2274 Ends--%>
					<input type="hidden" id="hdnSelectVMRRoom" runat="server" value=""  /> <%--FB 2448--%>
					<input type="hidden" id="isVMR" runat="server" value=""  /> <%--FB 2376--%>
					<input type="hidden" name="hdnCrossSetupTime" id="hdnCrossSetupTime" runat="server" /><%--FB 2398--%>
					<input type="hidden" name="hdnCrossTearDownTime" id="hdnCrossTearDownTime" runat="server" /><%--FB 2398--%>
					<input type="hidden" name="hdnCrossMeetGreetBufferTime" id="hdnCrossMeetGreetBufferTime" runat="server" /><%--FB 2632--%>
					<input type="hidden" runat="server" id="hdnGuestRoom" /><%--FB 2426--%>
					<input type="hidden" runat="server" id="hdnGuestRoomID" /><%--FB 2426--%>
					<input type="hidden" runat="server" id="hdnGuestloc" /><%--FB 2426--%>
					<input type="hidden" runat="server" id="hdnTxtMsg" /><%--FB 2486--%>
					<input type="hidden" runat="server" id="hdnSmartP2PTotalEps" /><%--FB 2430--%>
					<input type="hidden" runat="server" id="hdnCrossEnableSmartP2P" /><%--FB 2430--%>
					<input type="hidden" runat="server" id="hdnconftype" /><%--FB 2430--%>				                    
                </td>
              </tr>
            </table>
            </td>
        </tr>
        <tr valign="top"><td style="vertical-align:top"> 
        <div style="border:1px black">
                   <asp:Menu
                        id="TopMenu"
                        Orientation="Horizontal"
                        StaticMenuItemStyle-CssClass="tab"
                        StaticSelectedStyle-CssClass="selectedTab"
                        CssClass="tabs"
                        ItemWrap="true" 
                        OnMenuItemClick="Menu1_MenuItemClick"
                        Runat="server">
                        <Items>
                        <asp:MenuItem Selected="true" Text="<div align='center' style='width:123'><b>ベーシック</b><br>詳細</b></div>" Value="0" />
                        <asp:MenuItem Text="<div align='center' style='width:123' onclick='javascript:return SubmitRecurrence();'><b>セレクト</b><br><b>参加者</b></div>" Value="1" />
                        <asp:MenuItem Text="<div align='center' style='width:123' onclick='javascript:return SubmitRecurrence();'><b>セレクト</b><br><b>部屋</b></div>" Value="2" />
                        <asp:MenuItem Text="<div align='center' style='width:123' onclick='javascript:return SubmitRecurrence();'><b>オーディオ/ビデオ</b><br><b>設定</b></div>" Value="3" />
                        <asp:MenuItem Text="<div align='center' style='width:123' onclick='javascript:return SubmitRecurrence();'><b>セレクト</b><br><b>A/V</b></div>" Value="4" />
                        <asp:MenuItem Text="<div align='center' style='width:123' onclick='javascript:return SubmitRecurrence();'><b>セレクト</b><br><b>ケータリング</b></div>" Value="5" />
                        <asp:MenuItem Text="<div align='center' style='width:123' onclick='javascript:return SubmitRecurrence();'><b>セレクト</b><br><b>ハウスキーピング</b></div>" Value="6"></asp:MenuItem>
                        <asp:MenuItem Text="<div align='center' style='width:123' onclick='javascript:return SubmitRecurrence();'><b>追加</b><br><b>オプション</b></div>" Value="7"></asp:MenuItem> <%--Custom attribute Fixes --%>
                        <asp:MenuItem Text="<div align='center' style='width:123' onclick='javascript:return SubmitRecurrence();'><b>レビュー &amp;</b><br><b>送信</b></div>" Value="8" />
                        </Items>
                    </asp:Menu>
                    <div class="tabContents" style="width:100%;vertical-align:top" >
                            <asp:MultiView id="Wizard1" Runat="server">
                                <asp:View ID="BasicDetails" runat="server" OnDeactivate="SetSetupTearDownTime" OnActivate="SetBufferTimeOnActiate"> 
                                  <asp:Panel runat="server" Width="100%" Id="pnlNormal">
                                    <input type="hidden" id="helpPage" value="12">
                                    <table width="100%" border="0">
                                        <tr>
                                            <td colspan="2" style="height: 20px; text-align: center">
                                            <h3>
                                                <asp:Label ID="lblConfHeader" runat="server"></asp:Label>
                                                <%-- Code Added for FB 1428--%>
                                                <span id="Field1" runat="server">会議</span>
                                                </h3> <%--Added for FF--%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:50%;" valign="top">
                                                <table width="100%" cellpadding="3" border="0" cellspacing="0">
                                                    <tr>
                                                    <%--FB 1985 - Starts--%>
                                                     <%if ((Application["Client"].ToString().ToUpper().Equals("DISNEY")))
                                                       {%>                                                        
                                                     <td align="right" colspan="2">
                                                         <span class="reqfldstarText">* 必須欄</span>
                                                         <span style="width:10%">&nbsp;</span>
                                                    </td> 
                                                     <%}
                                                       else
                                                       {%>                                                           
                                                                <td style="height:10px;"></td>
                                                     <%}%><%--FB 1985 - End--%>
                                                    </tr>
                                                    <tr>
                                                         <td class="blackblodtext" align="left">
                                                             <asp:Label ID="Label2" runat="server" Visible="False" width="20%"></asp:Label>
                                                             <%-- Code Added for FB 1428--%>                                                
                                                            テンプレートから作成 
                                                            </td><%--Added for FF--%>
                                                         <td style="height: 20px" valign="top">
                                                            <asp:DropDownList ID="lstTemplates" CssClass="altText" runat="server" DataTextField="name" DataValueField="ID"  AutoPostBack="true" OnSelectedIndexChanged="UpdateTemplates" Width="80%">
                                                            </asp:DropDownList>
                                                         </td>                     
                                                     </tr>
                                                     <tr>
                                                         <td class="blackblodtext" align="left" width="35%">
                                                             <asp:Label ID="lblConfID" runat="server" Visible="False" width="20%"></asp:Label>
                                                             <%-- Code Added for FB 1428--%>                                                
                                                            <span ID="Field2" runat="server">会議タイトル</span>
                                                             <span class="reqfldstarText">*</span></td>
                                                         <td style="height: 20px" valign="top">
                                                             <asp:TextBox ID="ConferenceName" runat="server" CssClass="altText" Width="85%"></asp:TextBox>
                                                             <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ConferenceName" ErrorMessage="必須" Font-Bold="True" Display="Dynamic"></asp:RequiredFieldValidator>
                                                             <!--[Vivek: 29th Apr 2008]Changed Regular expression as per issue number 306  -->
                                                             <%-- Code Added for FB 1640--%>                                                
                                                             <asp:RegularExpressionValidator ID="regConfName" ControlToValidate="ConferenceName" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br> & < と >  は無効な記号です." ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator><%--FB 2321--%>
                                                         </td>                     
                                                     </tr>
                                                    <tr id="trConfType"> <%-- Code Modified For MOJ Phase2 --%>                                        
                                                        <%-- Code Added for FB 1428--%>
                                                        <td class="blackblodtext" align="left" id="Field3"  width="35%" runat="server">会議タイプ</td> <%--FB 2599 feb 21--%>
                                                        <td style="height: 24px">
                                                            <asp:DropDownList ID="lstConferenceType" CssClass="altSelectFormat" runat="server" OnSelectedIndexChanged="ShowHidePasswords" AutoPostBack="True">
                                                                <asp:ListItem Value="6">音声のみ</asp:ListItem>
                                                                <asp:ListItem Value="2">オーディオ/ビデオセッティング</asp:ListItem>
                                                                <asp:ListItem Value="4">ポイントツーポイント</asp:ListItem>
                                                                <asp:ListItem Selected="True" Value="7">部屋会議</asp:ListItem>
                                                            </asp:DropDownList>
                                                            &nbsp;&nbsp;
                                                            <asp:ImageButton ID="imgAudioNote" src="image/info.png" runat="server" OnClientClick="javascript:return false;"/>
                                                        </td>
                                                    </tr>
                                                    <%--FB 1985 - Starts--%>
                                                    <%if ((Application["Client"].ToString().ToUpper().Equals("DISNEY")))
                                                      {%> 
                                                    <tr >
                                                        <td colspan="2" style="color:Red">
                                                        <asp:Label runat="server" ID="lblAudioNote" Visible="false"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <%}%><%--FB 1985 - End--%>
                                                    <tr><%-- Custom Attribute Fixes --%>
                                                        <td align="left" style="font-weight:bold" class="blackblodtext">説明</td>
                                                        <td align="left">
                                                              <asp:TextBox ID="ConferenceDescription" runat="server" MaxLength="2000" CssClass="altText" Rows="2" TextMode="MultiLine"
                                                                  Width="100%" Columns="20" Wrap="true"></asp:TextBox>
                                                             <%-- Code Added for FB 1640--%>                                                
                                                              <asp:RegularExpressionValidator ID="RegularExpressionValidator3" ControlToValidate="ConferenceDescription" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br> + % \ | ^ = ! ` { } ~ は無効な記号です." ValidationExpression="^(a-z|A-Z|0-9)*[^\\^+|!`\[\]{}\=%~]*$"></asp:RegularExpressionValidator><%--//FB 2236--%><%--FB 1888--%>
                                                                <%--Removed ? from error message--%>                                                                  
                                                        </td>
                                                   </tr>
                                                   <tr><%--FB 2359--%>
                                                        <td class="blackblodtext" align="left">リクエスター</td>
                                                       <%-- FB 2501 Starts--%>
                                                        <td>
                                                            <asp:TextBox ID="txtApprover7" runat="server" CssClass="altText"></asp:TextBox>
                                                             &nbsp;<img id="Img5" onclick="javascript:getYourOwnEmailList(6)" src="image/edit.gif" />
                                                            <asp:TextBox ID="hdnApprover7" runat="server" BackColor="Transparent" BorderColor="White"
                                                                BorderStyle="None" Width="0px" ForeColor="Black" style="display:none"></asp:TextBox>
                                                            <asp:TextBox ID="hdnRequestorMail" runat="server" Width="0px" style="display:none"></asp:TextBox>
                                                        </td>
                                                        <%-- FB 2501 ends--%>
                                                    </tr>
                                                   <tr>
                                                        <%-- Code Added for FB 1428--%>
                                                        <td class="blackblodtext" align="left" id="Field4" runat="server">会議主催者</td>
                                                        <td>
                                                            <asp:TextBox ID="txtApprover4" runat="server" CssClass="altText" Enabled="true"></asp:TextBox>
                                                            &nbsp;<img id="Img8" onclick="javascript:getYourOwnEmailList(3)" src="image/edit.gif" />
                                                            <asp:TextBox ID="hdnApprover4" runat="server" BackColor="Transparent" BorderColor="White"
                                                                BorderStyle="None" Width="0px" ForeColor="Black" style="display:none"></asp:TextBox><%--Edited for FF--%>
                                                          <%--Code added for FB : 1116--%>
                                                            <asp:TextBox ID="hdnApproverMail" runat="server" Width="0px" style="display:none"></asp:TextBox><%--Edited for FF--%>   
                                                        </td>
                                                    </tr>
                                                    
                                                  <tr>
                                                        <asp:Panel ID="pnlPassword1" runat="server">
                                                        <td class="blackblodtext" align="left">数字パスワード
                                                          </td>
                                                        <td valign="top" nowrap="nowrap"><%--FB 2501--%>
                                                      
                                                            <asp:TextBox ID="ConferencePassword" runat="server" TextMode="SingleLine" CssClass="altText"></asp:TextBox><%--FB 2244--%>
                                                            <%--Code changes for FB : 1232--%>
                                                            <asp:Button runat="server" ID="btnGeneratePassword" width="140px" Text="パスワード生成" autopostback="false" CssClass="altLongblueButtonFormat" OnClientClick="javascript:num_gen(); return false;" /><%-- FB 676--%>
                                                            <br />
                                                            <asp:CompareValidator ID="cmpValPassword1" runat="server" ControlToCompare="ConferencePassword2"
                                                                ControlToValidate="ConferencePassword" Display="Dynamic" ErrorMessage="確認してください / 似たパスワードを入力"></asp:CompareValidator>
                                                            <asp:RegularExpressionValidator ID="numPassword1" runat="server" ErrorMessage="<br>４から１０桁のみ許可されています。 最初の数は０にしないでください。" SetFocusOnError="True" ToolTip="数値だけが許可されます" ControlToValidate="ConferencePassword" ValidationExpression="^([1-9])([0-9]\d{2,8})" Display="Dynamic"></asp:RegularExpressionValidator> <%--Comments: Fogbugz case 107, 522 --%>
                                                        </td>
                                                        </asp:Panel>
                                                   </tr>
                                                    <tr>
                                                        <asp:Panel ID="pnlPassword2" runat="server">
                                                        <td class="blackblodtext" align="left">パスワード確認</td>
                                                        <td style="height: 20px" valign="top">
                                                            <asp:TextBox ID="ConferencePassword2" runat="server" CssClass="altText" TextMode="SingleLine" ></asp:TextBox><%--FB 2244--%>
                                                            <asp:CompareValidator ID="cmpValPassword" runat="server" ControlToCompare="ConferencePassword"
                                                                ControlToValidate="ConferencePassword2" Display="Dynamic" ErrorMessage="パスワードがマッチしません。"></asp:CompareValidator>
                                                        </td>
                                                        </asp:Panel>
                                                    </tr>
                                                    <tr id="trPublic" runat="server"><%--FB 2359--%>
                                                        <td class="blackblodtext" align="left" width="182px">公の</td>
                                                        <td style="height: 24px">
                                                        <table cellpadding="0" cellspacing="0">
                                                            <tr><td>
                                                            <asp:CheckBox ID="chkPublic" runat="server" />
                                                            </td><td>
                                                            <div id="openRegister" style="display:none">
                                                            <asp:CheckBox ID="chkOpenForRegistration" runat="server" TextAlign="Left" /><span 
                                                                    align="right" class="blackblodtext">登録受付中</span>
                                                            </div>
                                                            </td></tr></table>
                                                        </td>
                                                    </tr>
                                                    <tr id="trServType" runat="server"><%--FB 2219--%>
                                                        <td class="blackblodtext" align="left">サービスタイプ</td>
                                                        <td style="height: 24px">
                                                            <asp:DropDownList ID="DrpServiceType" CssClass="altSelectFormat" DataTextField="Name" DataValueField="ID" runat="server">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr id="trVIP" runat="server"> <%-- Code Modified For MOJ Phase2 --%>
                                                        <td class="blackblodtext" align="left">ビップ</td>
                                                        <td style="height: 24px">
                                                        <table cellpadding="0" cellspacing="0">
                                                            <tr><td>
                                                            <asp:CheckBox ID="chkisVIP" runat="server" />
                                                            </td><td>
                                                            <div id="Div1" style="display:none">
                                                            </div>
                                                            </td></tr></table>
                                                        </td>
                                                    </tr>
                                                    <tr id="trVMR" runat="server"> <%-- Code Modified For FB  2376 --%>
                                                        <td class="blackblodtext" align="left" style="vertical-align:top" >VMR</td>
                                                        <td style="height: 24px">
                                                           <asp:CheckBox ID="chkVMR" runat="server"  onclick="javascript:changeVMR();"  OnCheckedChanged="ShowHideAVforVMR" AutoPostBack="True"/><%--FB 2248--%>
                                                            <%--VMR Start--%>
                                                        
								<table id="divbridge" style="display:none">
                                                                        <tr>
                                                                            <td class="blackblodtext" align="left">内部ブリッジ</td>
                                                                            <td style="height: 24px">
                                                                               <asp:TextBox ID="txtintbridge" ReadOnly="true" runat="server" ></asp:TextBox>
										<input type="hidden" name="intbridge" id="hdnintbridge" runat="server" />
										</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="blackblodtext" align="left">外部ブリッジ</td>
                                                                            <td style="height: 24px">
                                                                               <asp:TextBox ID="txtextbridge" ReadOnly="true" runat="server" ></asp:TextBox>
										<input type="hidden" name="extbridge" id="hdnextbridge" runat="server" />
										</td>
                                                                        </tr>
                                                                    </table>
                                                        <%--VMR End--%>
                                                        </td>
                                                    </tr>

                                                    <tr id="NONRecurringConferenceDiv4">
                                                        <td class="blackblodtext" align="left">今始める</td>
                                                        <td style="height: 24px">
                                                            <asp:CheckBox runat="server" ID="chkStartNow" />
                                                        </td>
                                                    </tr>
                                                    <tr id="NONRecurringConferenceDiv9" runat="server" >
                                                        <td class="blackblodtext">バッファーゾーンを有効にする</td>
                                                        <td>
                                                             <asp:CheckBox ID="chkEnableBuffer" runat="server" onclick="javascript:fnEnableBuffer();" />                                                            
                                                        </td>                                                        
                                                    </tr>
                                                     <tr id="NONRecurringConferenceDiv1">
                                         <td class="blackblodtext" align="left" id="SDateText">開始日/時間<span class="reqfldstarText">*</span></td>
                                         <td valign="top">
                                             <table cellpadding="0px" cellspacing="1px" border="0" width="100%"> <%--FB 1722--%>
                                             <tr>
                                             <td valign="top" style="text-align: left; height: 27px; color: black; font-family: arial; width: 403px;" align="center" colspan="2">
                                                 <span id="StartDateArea" >
                                                     <asp:TextBox ID="confStartDate" runat="server" CssClass="altText" Width="25%" onblur="javascript:ChangeEndDate(0)" OnTextChanged="confStartTime_TextChanged" AutoPostBack="false"></asp:TextBox>
                                                     <%--//Code changed by Offshore for FB Issue 1073 -- Start
                                                     <img src="image/calendar.gif" border="0" width="20" height="20" id="cal_triggerd" style="cursor: pointer;" title="日付セレクタ" onblur="javascript:ChangeEndDate(0)" onclick="return showCalendar('<%=confStartDate.ClientID %>', 'cal_triggerd', 1, '%m/%d/%Y');" />@--%>
                                                     <img src="image/calendar.gif" border="0" width="20" height="20" id="cal_triggerd" style="cursor: pointer;vertical-align:middle;" title="日付セレクタ" onblur="javascript:ChangeEndDate(0)" onclick="return showCalendar('<%=confStartDate.ClientID %>', 'cal_triggerd', 1, '<%=format%>');" /> 
                                                     <span class="blackblodtext">@</span> <%--window dressing--%>
                                                 </span>
                                                 <!--//Code changed by Offshore for FB Issue 1073 -- End-->
                                                 <%--window dressing--%>                                                                            <%--FB 1716--%>
                                                 <mbcbb:combobox id="confStartTime" runat="server" CssClass="altSelectFormat" Rows="10" CausesValidation="True" style="width:75px" 
                                                 onblur="javascript:return document.getElementById('hdnChange').value='SU';formatTime('confStartTime_Text');return ChangeEndDate(0);" OnTextChanged="confStartTime_TextChanged" OnSelectedIndexChanged="confStartTime_SelectedIndexChanged" AutoPostBack="false"><%--Edited for FF--%><%--FB 1715 FB 1716--%>
                                                    <asp:ListItem Value="01:00 AM" Selected="True"></asp:ListItem>
                                                    <asp:ListItem Value="02:00 AM"></asp:ListItem>
                                                    <asp:ListItem Value="03:00 AM"></asp:ListItem>
                                                    <asp:ListItem Value="04:00 AM"></asp:ListItem>
                                                    <asp:ListItem Value="05:00 AM"></asp:ListItem>
                                                    <asp:ListItem Value="06:00 AM"></asp:ListItem>
                                                    <asp:ListItem Value="07:00 AM"></asp:ListItem>
                                                    <asp:ListItem Value="08:00 AM"></asp:ListItem>
                                                    <asp:ListItem Value="09:00 AM"></asp:ListItem>
                                                    <asp:ListItem Value="10:00 AM"></asp:ListItem>
                                                    <asp:ListItem Value="11:00 AM"></asp:ListItem>
                                                    <asp:ListItem Value="12:00 PM"></asp:ListItem>
                                                    <asp:ListItem Value="01:00 PM"></asp:ListItem>
                                                    <asp:ListItem Value="02:00 PM"></asp:ListItem>
                                                    <asp:ListItem Value="03:00 PM"></asp:ListItem>
                                                    <asp:ListItem Value="04:00 PM"></asp:ListItem>
                                                    <asp:ListItem Value="05:00 PM"></asp:ListItem>
                                                    <asp:ListItem Value="06:00 PM"></asp:ListItem>
                                                    <asp:ListItem Value="07:00 PM"></asp:ListItem>
                                                    <asp:ListItem Value="08:00 PM"></asp:ListItem>
                                                    <asp:ListItem Value="09:00 PM"></asp:ListItem>
                                                    <asp:ListItem Value="10:00 PM"></asp:ListItem>
                                                    <asp:ListItem Value="11:00 PM"></asp:ListItem>
                                                    <asp:ListItem Value="12:00 AM"></asp:ListItem>
                                            </mbcbb:combobox>
                                            <asp:RequiredFieldValidator ID="reqStartTime" runat="server" ControlToValidate="confStartTime" Display="Dynamic" ErrorMessage="時間は必須です"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="regStartTime" runat="server" ControlToValidate="confStartTime" Display="Dynamic" ErrorMessage="無効な時間 (HH:MM AM/PM)" ValidationExpression="[0|1][0-9]:[0-5][0-9] ?[a|A|p|P][M|m]"></asp:RegularExpressionValidator> <%--FB 1715-%><%-- FB Case 371 Saima --%>
                                            <asp:RequiredFieldValidator ID="reqStartData" runat="server" ControlToValidate="confStartDate" Display="Dynamic" ErrorMessage="日付は必須です"></asp:RequiredFieldValidator>
                                            <%-- <!--//Code changed by Offshore for FB Issue 1073 -- Start
                                            <asp:RegularExpressionValidator ID="regStartDate" runat="server" ControlToValidate="confStartDate" Display="Dynamic" ErrorMessage="Invalid Date (mm/dd/yyyy)" ValidationExpression="\b(0?[1-9]|1[012])[- /.](0?[1-9]|[12][0-9]|3[01])[- /.](19|20)?[0-9]{2}\b"></asp:RegularExpressionValidator>-->--%>
                                            <asp:RegularExpressionValidator ID="regStartDate" runat="server" ControlToValidate="confStartDate" Display="Dynamic" ErrorMessage="無効な日付です  " ValidationExpression="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d|(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d"></asp:RegularExpressionValidator>
                                            <!--//Code changed by Offshore for FB Issue 1073 -- End-->

                                            </td>
                                             </tr>
                                             </table>
                                             </td>
                                         </tr>
                                           <%-- 1715  Code added for Buffer Zone -- start--%>
                                           
                                         <tr id="NONRecurringConferenceDiv6" align="left"> <%--Edited for FF--%>
                                         <td class="blackblodtext" align="left" id="tdConfSt">会議開始 
                                             <span class="reqfldstarText">*</span></td>
                                         <td valign="top" align="left">
                                             <table cellpadding="0px" cellspacing="1px" border="0" align="left" width="100%">
                                             <tr>
                                             <td valign="top" style="text-align: left; height: 27px; color: black; font-family: arial; width: 403px;" align="left" colspan="2">
                                                 <span id="EndDateArea" >
                                                 <asp:TextBox ID="SetupDate" runat="server" CssClass="altText" Width="25%" onblur="javascript:ChangeEndDate(0)" AutoPostBack="false"></asp:TextBox>
                                                 <img alt="conimg" src="image/calendar.gif" border="0" width="20" height="20" id="Img1" style="cursor: pointer;vertical-align:middle;" title="日付セレクタ" onblur="javascript:ChangeEndDate(0)" onclick="return showCalendar('<%=SetupDate.ClientID %>', 'cal_triggerd', 1, '<%=format%>');" /><%--Edited for FF--%>
                                                 <span class="blackblodtext">@</span>
                                                 </span>
                                                 <mbcbb:combobox id="SetupTime" runat="server" CssClass="altSelectFormat" Rows="10" CausesValidation="True" style="width:75px" 
                                                 onblur="javascript:return document.getElementById('hdnChange').value='ST';formatTime('SetupTime_Text');return ChangeEndDate(0);" AutoPostBack="false"><%--Edited for FF--%><%--FB 1715 FB 1716--%>
                                                    <asp:ListItem Value="01:00 AM" Selected="True"></asp:ListItem>
                                                    <asp:ListItem Value="02:00 AM"></asp:ListItem>
                                                    <asp:ListItem Value="03:00 AM"></asp:ListItem>
                                                    <asp:ListItem Value="04:00 AM"></asp:ListItem>
                                                    <asp:ListItem Value="05:00 AM"></asp:ListItem>
                                                    <asp:ListItem Value="06:00 AM"></asp:ListItem>
                                                    <asp:ListItem Value="07:00 AM"></asp:ListItem>
                                                    <asp:ListItem Value="08:00 AM"></asp:ListItem>
                                                    <asp:ListItem Value="09:00 AM"></asp:ListItem>
                                                    <asp:ListItem Value="10:00 AM"></asp:ListItem>
                                                    <asp:ListItem Value="11:00 AM"></asp:ListItem>
                                                    <asp:ListItem Value="12:00 PM"></asp:ListItem>
                                                    <asp:ListItem Value="01:00 PM"></asp:ListItem>
                                                    <asp:ListItem Value="02:00 PM"></asp:ListItem>
                                                    <asp:ListItem Value="03:00 PM"></asp:ListItem>
                                                    <asp:ListItem Value="04:00 PM"></asp:ListItem>
                                                    <asp:ListItem Value="05:00 PM"></asp:ListItem>
                                                    <asp:ListItem Value="06:00 PM"></asp:ListItem>
                                                    <asp:ListItem Value="07:00 PM"></asp:ListItem>
                                                    <asp:ListItem Value="08:00 PM"></asp:ListItem>
                                                    <asp:ListItem Value="09:00 PM"></asp:ListItem>
                                                    <asp:ListItem Value="10:00 PM"></asp:ListItem>
                                                    <asp:ListItem Value="11:00 PM"></asp:ListItem>
                                                    <asp:ListItem Value="12:00 AM"></asp:ListItem>
                                                </mbcbb:combobox>
                                                <asp:RequiredFieldValidator ID="reqSetupStartTime" runat="server" ControlToValidate="SetupTime" Display="Dynamic" ErrorMessage="時間は必須です"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="regSetupStartTime" runat="server" ControlToValidate="SetupTime" Display="Dynamic" ErrorMessage="無効な時間 (HH:MM AM/PM)" ValidationExpression="[0-1][0-9]:[0-5][0-9] ?[A|a|P|p][M|m]"></asp:RegularExpressionValidator><%--FB 1715--%>
                                                <asp:RequiredFieldValidator ID="reqSetupStartDate" runat="server" ControlToValidate="SetupDate" Display="Dynamic" ErrorMessage="日付は必須です"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="regSetupStartDate" runat="server" ControlToValidate="SetupDate" Display="Dynamic" ErrorMessage="無効な日付です <%=format%>" ValidationExpression="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d|(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d"></asp:RegularExpressionValidator>
                                                <asp:TextBox runat="server" ID="SetupDateTime" Width="0px" style="display:none"></asp:TextBox> <%--Edited for FF--%>
                                            </td>
                                            </tr>
                                           </table>
                                          </td>
                                         </tr>
                                         <%-- FB 1715Edited for FF--%>                                         
                                         <tr id="NONRecurringConferenceDiv7">
                                             <td class="blackblodtext" align="left"  id="tdConfEd">会議終了 
                                                 <span class="reqfldstarText">*</span></td>
                                         <td valign="top"  >
                                             <table cellpadding="0px" cellspacing="1px" border="0" align="right" width="100%">
                                             <tr>
                                             <td valign="top" style="text-align: left; height: 27px; color: black; font-family: arial; width: 403px;" align="center" colspan="2">
                                                                 <span id="TeardownArea" >
                                                                 <asp:TextBox ID="TearDownDate" runat="server" CssClass="altText" Width="25%" onblur="javascript:ChangeStartDate(0)"  AutoPostBack="false"></asp:TextBox>
                                                                <img src="image/calendar.gif" border="0" width="20" height="20" id="Img4" style="cursor: pointer;vertical-align:middle;" title="日付セレクタ" onblur="javascript:ChangeStartDate(0)" onclick="return showCalendar('<%=TearDownDate.ClientID %>', 'cal_triggerd', 1, '<%=format%>');" />
                                                                <span class="blackblodtext">@</span>
                                                                </span>  <%--FB 1716--%>
                                                                 <mbcbb:combobox id="TeardownTime" runat="server" CssClass="altSelectFormat" Rows="10" CausesValidation="True" style="width:75px" 
                                                                 onblur="javascript: document.getElementById('hdnChange').value='TD';formatTime('TeardownTime_Text');return ChangeStartDate(0);" AutoPostBack="false"><%--Edited for FF--%><%--FB 1715 FB 1716--%>
                                                                    <asp:ListItem Value="01:00 AM" Selected="True"></asp:ListItem>
                                                                    <asp:ListItem Value="02:00 AM"></asp:ListItem>
                                                                    <asp:ListItem Value="03:00 AM"></asp:ListItem>
                                                                    <asp:ListItem Value="04:00 AM"></asp:ListItem>
                                                                    <asp:ListItem Value="05:00 AM"></asp:ListItem>
                                                                    <asp:ListItem Value="06:00 AM"></asp:ListItem>
                                                                    <asp:ListItem Value="07:00 AM"></asp:ListItem>
                                                                    <asp:ListItem Value="08:00 AM"></asp:ListItem>
                                                                    <asp:ListItem Value="09:00 AM"></asp:ListItem>
                                                                    <asp:ListItem Value="10:00 AM"></asp:ListItem>
                                                                    <asp:ListItem Value="11:00 AM"></asp:ListItem>
                                                                    <asp:ListItem Value="12:00 PM"></asp:ListItem>
                                                                    <asp:ListItem Value="01:00 PM"></asp:ListItem>
                                                                    <asp:ListItem Value="02:00 PM"></asp:ListItem>
                                                                    <asp:ListItem Value="03:00 PM"></asp:ListItem>
                                                                    <asp:ListItem Value="04:00 PM"></asp:ListItem>
                                                                    <asp:ListItem Value="05:00 PM"></asp:ListItem>
                                                                    <asp:ListItem Value="06:00 PM"></asp:ListItem>
                                                                    <asp:ListItem Value="07:00 PM"></asp:ListItem>
                                                                    <asp:ListItem Value="08:00 PM"></asp:ListItem>
                                                                    <asp:ListItem Value="09:00 PM"></asp:ListItem>
                                                                    <asp:ListItem Value="10:00 PM"></asp:ListItem>
                                                                    <asp:ListItem Value="11:00 PM"></asp:ListItem>
                                                                    <asp:ListItem Value="12:00 AM"></asp:ListItem>
                                                                </mbcbb:combobox>
                                                                <asp:RegularExpressionValidator ID="regTearDownStartTime" runat="server" ControlToValidate="TeardownTime" Display="Dynamic" ErrorMessage="無効な時間 (HH:MM AM/PM)" ValidationExpression="[0-1][0-9]:[0-5][0-9] ?[A|a|P|p][M|m]"></asp:RegularExpressionValidator><%--FB 1715--%>
                                                                <asp:RequiredFieldValidator ID="reqTearDownStartDate" runat="server" ControlToValidate="TeardownDate" Display="Dynamic" ErrorMessage="日付は必須です"></asp:RequiredFieldValidator>
                                                                <asp:RegularExpressionValidator ID="regTearDownStartDate" runat="server" ControlToValidate="TeardownDate" Display="Dynamic" ErrorMessage="無効な日付です <%=format%>" ValidationExpression="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d|(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d"></asp:RegularExpressionValidator>
                                                                <asp:TextBox runat="server" ID="TearDownDateTime" Width="0px" style="display:none"></asp:TextBox> <%--Edited for FF--%>
                                                             </td>
                                                            </tr>
                                                           </table>
                                                          </td>
                                                         </tr>
                                         <%-- FB 1715Edited for FF--%>
                                                         <%--  Code added for Buffer Zone -- end--%>
                                                        <tr id="NONRecurringConferenceDiv2">
                                                            <td class="blackblodtext" align="left"  id="EDateText">終了日/時間<span class="reqfldstarText">*</span></td>
                                                            <td valign="top" width="65%">
                                                                <table cellpadding="0px" cellspacing="1px" border="0" width="100%"> <%--FB 1722--%>
                                                                    <tr>
                                                                        <td valign="top" style="text-align: left; height: 27px; color: black; font-family: arial; width: 403px;" align="center" colspan="2">
                                                                         <asp:TextBox ID="confEndDate" runat="server" CssClass="altText" Width="25%" onblur="javascript:ChangeStartDate(0)" OnTextChanged="confEndTime_TextChanged" AutoPostBack="false" ></asp:TextBox>
                                                                          <!--//Code changed by Offshore for FB Issue 1073 -- Start
                                                                          <img src="image/calendar.gif" border="0" width="20" height="20" onblur="javascript:ChangeStartDate(0)" id="cal_trigger1" style="cursor: pointer;" title="日付セレクタ" onclick="return showCalendar('<%=confEndDate.ClientID %>', 'cal_trigger1', 1, '%m/%d/%Y');" />-->
                                                                          <img src="image/calendar.gif" border="0" width="20" height="20" onblur="javascript:ChangeStartDate(0)" id="cal_trigger1" style="cursor: pointer;vertical-align:middle;" title="日付セレクタ" onclick="return showCalendar('<%=confEndDate.ClientID %>', 'cal_trigger1', 1, '<%=format%>');" />
                                                                          <!--Code changed by Offshore for FB Issue 1073 -- End-->
                                                                            <span class="blackblodtext">@
                                                                            </span><mbcbb:combobox id="confEndTime" runat="server" CssClass="altSelectFormat" onblur="javascript:formatTime('confEndTime_Text');return ChangeStartDate(0)" Width="75px"
                                                                            Rows="10"  CausesValidation="True" OnSelectedIndexChanged="confEndTime_SelectedIndexChanged" OnTextChanged="confEndTime_TextChanged" AutoPostBack="false"><%--Edited for FF--%><%--FB 1715 FB 1716--%><%--FB 1722--%>
                                                                                <asp:ListItem Value="01:00 AM"></asp:ListItem>
                                                                                <asp:ListItem Value="02:00 AM"></asp:ListItem>
                                                                                <asp:ListItem Value="03:00 AM"></asp:ListItem>
                                                                                <asp:ListItem Value="04:00 AM"></asp:ListItem>
                                                                                <asp:ListItem Value="05:00 AM"></asp:ListItem>
                                                                                <asp:ListItem Value="06:00 AM"></asp:ListItem>
                                                                                <asp:ListItem Value="07:00 AM"></asp:ListItem>
                                                                                <asp:ListItem Value="08:00 AM"></asp:ListItem>
                                                                                <asp:ListItem Value="09:00 AM"></asp:ListItem>
                                                                                <asp:ListItem Value="10:00 AM"></asp:ListItem>
                                                                                <asp:ListItem Value="11:00 AM"></asp:ListItem>
                                                                                <asp:ListItem Value="12:00 PM"></asp:ListItem>
                                                                                <asp:ListItem Value="01:00 PM"></asp:ListItem>
                                                                                <asp:ListItem Value="02:00 PM"></asp:ListItem>
                                                                                <asp:ListItem Value="03:00 PM"></asp:ListItem>
                                                                                <asp:ListItem Value="04:00 PM"></asp:ListItem>
                                                                                <asp:ListItem Value="05:00 PM"></asp:ListItem>
                                                                                <asp:ListItem Value="06:00 PM"></asp:ListItem>
                                                                                <asp:ListItem Value="07:00 PM"></asp:ListItem>
                                                                                <asp:ListItem Value="08:00 PM"></asp:ListItem>
                                                                                <asp:ListItem Value="09:00 PM"></asp:ListItem>
                                                                                <asp:ListItem Value="10:00 PM"></asp:ListItem>
                                                                                <asp:ListItem Value="11:00 PM"></asp:ListItem>
                                                                                <asp:ListItem Value="12:00 AM"></asp:ListItem>
                                                                            </mbcbb:combobox>
                                                                            <asp:RequiredFieldValidator ID="reqEndTime" runat="server" ControlToValidate="confEndTime" Display="Dynamic" ErrorMessage="時間は必須です"></asp:RequiredFieldValidator>
                                                                            <asp:RegularExpressionValidator ID="regEndTime" runat="server" ControlToValidate="confEndTime" Display="Dynamic" ErrorMessage="無効な時間 (HH:MM AM/PM)" ValidationExpression="[0-1][0-9]:[0-5][0-9] ?[A|a|P|p][M|m]"></asp:RegularExpressionValidator><%--FB 1715--%>
                                                                            <asp:RequiredFieldValidator ID="reqEndDate" runat="server" ControlToValidate="confEndDate" Display="Dynamic" ErrorMessage="日付は必須です"></asp:RequiredFieldValidator>
                                                                            <%--<!--//Code changed by Offshore for FB Issue 1073 -- Start
                                                                            <asp:RegularExpressionValidator ID="regEndDate" runat="server" ControlToValidate="confEndDate" Display="Dynamic" ErrorMessage="Invalid Date (mm/dd/yyyy)" ValidationExpression="\b(0?[1-9]|1[012])[- /.](0?[1-9]|[12][0-9]|3[01])[- /.](19|20)?[0-9]{2}\b"></asp:RegularExpressionValidator>-->--%>
                                                                            <asp:RegularExpressionValidator ID="regEndDate" runat="server" ControlToValidate="confEndDate" Display="Dynamic" ErrorMessage="無効な日付です <%=format%>" ValidationExpression="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d|(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d"></asp:RegularExpressionValidator>
                                                                            <!--//Code changed by Offshore for FB Issue 1073 -- End-->
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr id="NONRecurringConferenceDiv8">
                                                           <td class="blackblodtext" align="left" >定期</td>
                                                           <td> <%--FB 1911--%>
                                                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                    <tr >
                                                                        <td width="5%" align="left">
                                                                           <asp:CheckBox onClick="openRecur()" runat="server" ID="chkRecurrence" />
                                                                        </td>
                                                                        <td class="blackblodtext" align="right" nowrap id="SPCell1"><span class="subtitleblueblodtext">&nbsp;または </span>&nbsp; 特殊な再発の &nbsp;</td>
                                                                        <td id="SPCell2" width="45%">
                                                                            <a onClick="openRecur('S')" style="cursor: hand;">
                                                                              <img src="image/recurring.gif" width="25" height="25" border="0">
                                                                            </a>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>   
                                                        </tr> <%--FB 1911--%>
                                                        <tr id="recurDIV" style="display:none;">
                                                            <td align="left" valign="top">
                                                                 <span class="blackblodtext">特殊な再発のテキスト</span>
                                                            </td>
                                                            <td>                                                               
                                                                <asp:TextBox ID="RecurText" Enabled="false" runat="server" CssClass="altText" Rows="4" TextMode="MultiLine" Width="70%">なし再発</asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr id="NONRecurringConferenceDiv5" style="display:none;">
                                                            <td class="blackblodtext" align="left">継続時間</td>
                                                            <td align="left">    
                                                                <asp:Label ID="lblConfDuration" runat="server"></asp:Label>
                                                                <asp:Button ID="btnRefresh" CssClass="altShortBlueButtonFormat" Text="リフレッシュ" OnClick="CalculateDuration" OnClientClick="javascript:DataLoading(1);" runat="server" ValidationGroup="Update" />
                                                            </td>
                                                        </tr>
                                                        <tr id="NONRecurringConferenceDiv3">
                                                            <td class="blackblodtext" align="left">タイムゾーン<span class="reqfldstarText">*</span></td>
                                                            <td>
                                                                     <%-- <asp:DropDownList ID="lstConferenceTZ"  Width="300px" CssClass="altSelectFormat">--%> <%--Code Modified For FB 1453--%>
                                                                     <asp:DropDownList ID="lstConferenceTZ" runat="server"  DataTextField="timezoneName" DataValueField="timezoneID"  CssClass="altText">                                                    
                                                                     </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                         <%--FB 2501 starts--%>
                                                         <tr id="trStartMode" runat="server"><%--FB 2501--%>
                                                            <td class="blackblodtext" align="left">モードを開始します</td>
                                                            <td >
                                                             <asp:DropDownList ID="lstStartMode" runat="server" Width="30%"  CssClass="alt2SelectFormat">
                                                                      <asp:ListItem Value="0" Selected="True" Text="自動"></asp:ListItem>
                                                                      <asp:ListItem Value="1" Text="マニュアル"></asp:ListItem>
                                                             </asp:DropDownList> 
                                                            </td>
                                                        </tr>
                                                        <%--FB2501 ends--%>
                                                       
                                                        <tr id="divDuration" style="display:none">
                                                            <td class="blackblodtext" align="left">継続時間<span class="reqfldstarText">*</span></td>
                                                            <td><mbcbb:combobox id="lstDuration" runat="server" CssClass="altSelectFormat" Rows="10" CausesValidation="True" style="width:auto"><%--Edited for FF--%>
                                                                    <asp:ListItem Value="01:00" Selected="True">01:00</asp:ListItem>
                                                                    <asp:ListItem Value="02:00">02:00</asp:ListItem>
                                                                    <asp:ListItem Value="03:00">03:00</asp:ListItem>
                                                                    <asp:ListItem Value="04:00">04:00</asp:ListItem>
                                                                    <asp:ListItem Value="05:00">05:00</asp:ListItem>
                                                                    <asp:ListItem Value="06:00">06:00</asp:ListItem>
                                                                    <asp:ListItem Value="07:00">07:00</asp:ListItem>
                                                                    <asp:ListItem Value="08:00">08:00</asp:ListItem>
                                                                    <asp:ListItem Value="09:00">09:00</asp:ListItem>
                                                                    <asp:ListItem Value="10:00">10:00</asp:ListItem>
                                                                    <asp:ListItem Value="11:00">11:00</asp:ListItem>
                                                                    <asp:ListItem Value="12:00">12:00</asp:ListItem>
                                                            </mbcbb:combobox> hh:mm
                                                        </td>
                                                    </tr>
                                                     <%--FB 1722 - Start--%>
                                                     <tr><td colspan="2">
                                                         <table border="0" cellpadding="0" width="100%"> <%--Edited for FF--%>
                                                         <tr id="DurationRow" style="display:none;">                                                       
                                                            <td id="tabCelldur" align="left" class="blackblodtext" valign="top" width="35%"> 継続時間</td><%--Edited For FF --%>
                                                            <td>
                                                            <table border="0" cellpadding="0px" cellspacing="0" width="100%"><tr><td>
                                                     <%--FB 1722 - End--%>
                                                                <asp:TextBox ID="RecurDurationhr" runat="server" CssClass="altText" 
                                                                    onblur="Javascript:  return validateDurationHr();" 
                                                                    onchange="javascript: recurTimeChg();" Width="15%"></asp:TextBox> 時間
                                                                <asp:TextBox ID="RecurDurationmi" runat="server" CssClass="altText" 
                                                                    onblur="Javascript: return validateDurationMi();" 
                                                                    onchange="javascript: recurTimeChg();" Width="15%"></asp:TextBox> 分                
                                                                <br />
                                                                <%if (enableBufferZone == "0")
                                                                  { %> 
                                                                    ( 最大限度です  <%=Application["MaxConferenceDurationInHours"]%> 時間) 
                                                                 <%}
                                                                  else
                                                                  { %>
                                                                    ( 最大の制限は、 バッファの期間を含め <%=Application["MaxConferenceDurationInHours"]%> 時間です。) 
                                                                <%} %>
                                                                <asp:TextBox ID="EndText" runat="server" style="display:none" ></asp:TextBox>
                                                                </td></tr></table>
                                                            </td>
                                                            <%--Window Dressing--%>
                                                        </tr>
                                                    </table></td></tr><%--Edited for FF--%>
                                                    
                                                </table>
                                            </td>                                             
                                            <td valign="top" style="width:50%;">
                                                <table width="100%">
                                                    <%--Merging Recurrence start--%>
                                                    <tr id="RecurrenceRow">
                                                        <td colspan="2" align="left">
                                                            <table width="100%" border="0" >
                                                                <tr>
                                                                    <td align="left" style="width:31%;" valign="top" >
                                                                        <table style="width:100%;" border="0" cellpadding="3" >
                                                                            <tr>
                                                                                <td colspan="2"  valign="top" >
                                                                                    <table style="width:100%;" border="0" cellpadding="3">
                                                                                        <tr>
                                                                                            <td valign="top" align="left" colspan="2" >
                                                                                              <span class="blackblodtext">                                                                                  
                                                                                              繰り返し起こるパターン</span>       
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td valign="top" nowrap style="width:20%;" align="left">                                                                                    
                                                                                                <asp:RadioButtonList ID="RecurType" runat="server" CssClass="blackxxxsboldtext" RepeatDirection="Vertical"  OnClick="javascript:return fnShow();" CellPadding="3" >
                                                                                                </asp:RadioButtonList>                        
                                                                                            </td>
                                                                                            <td align="left" valign="top" class="blackxxxsboldtext">
                                                                                                <%--Daily Recurring Pattern--%>
                                                                                                &nbsp;
                                                                                                <asp:Panel ID="Daily" runat="server" HorizontalAlign="left" >
                                                                                                    <asp:RadioButton ID="DEveryDay" runat="server" GroupName="RDaily" /> すべて 
                                                                                                    <asp:TextBox ID="DayGap" CssClass="altText"  runat="server" Width="8%" onClick="javaScript: DEveryDay.checked = true;" onChange="javaScript: summarydaily();"></asp:TextBox> 日
                                                                                                    <br />
                                                                                                    <asp:RadioButton ID="DWeekDay" CssClass="blackxxxstext" runat="server" GroupName="RDaily" onClick="javaScript: DayGap.value=''; summarydaily();" /> 毎週
                                                                                                </asp:Panel>                
                                                                                                <%--Weekly Recurring Pattern--%>
                                                                                                <asp:Panel ID="Weekly" runat="server">
                                                                                                    すべてを再発する <asp:TextBox ID="WeekGap" runat="server" CssClass="altText" Width="8%"></asp:TextBox> 週（s）の:
                                                                                                    <asp:CheckBoxList ID="WeekDay" runat="server" CssClass="blackxxxsboldtext" RepeatDirection="horizontal" RepeatColumns="4" CellPadding="2" CellSpacing="3" onClick="javaScript: summaryweekly();" ></asp:CheckBoxList>
                                                                                                </asp:Panel>
                                                                                                <%--Monthly Recurring Pattern--%>
                                                                                                <asp:Panel ID="Monthly" runat="server">
                                                                                                    <asp:RadioButton ID="MEveryMthR1" runat="server" GroupName="GMonthly" onClick="javaScript: MonthGap2.value = ''; summarymonthly();" />  
                                                                                                    日 <asp:TextBox ID="MonthDayNo" runat="server" CssClass="altText" Width="8%" onClick="javaScript: MEveryMthR1.checked = true;" onChange="javaScript: summarymonthly();" class="altText"></asp:TextBox> 
                                                                                                    すべての <asp:TextBox ID="MonthGap1" runat="server" CssClass="altText" Width="8%" onClick="javaScript: MEveryMthR1.checked = true;" onChange="javaScript: summarymonthly();" ></asp:TextBox> 月 
                                                                                                    <br /><br />
                                                                                                    <asp:RadioButton ID="MEveryMthR2" runat="server" GroupName="GMonthly" onClick="javaScript: summarymonthly(); MonthDayNo.value = ''; MonthGap1.value = '';"  /> 
                                                                                                    市販 <asp:DropDownList CssClass="altText" runat="server" ID="MonthWeekDayNo" onClick="javaScript: summarymonthly();"></asp:DropDownList>
                                                                                                    <asp:DropDownList CssClass="altText" runat="server" ID="MonthWeekDay" onClick="javaScript: summarymonthly();"></asp:DropDownList> 
                                                                                                    すべての <asp:TextBox ID="MonthGap2" runat="server" CssClass="altText" Width="8%" onClick="javaScript: MEveryMthR2.checked = true;" onChange="javaScript: summarymonthly();"></asp:TextBox> 月
                                                                                                </asp:Panel>
                                                                                                <%--Yearly Recurring Pattern--%>
                                                                                                <asp:Panel ID="Yearly" runat="server">
                                                                                                    <asp:RadioButton ID="YEveryYr1" runat="server" GroupName="GYearly" onClick="javaScript: summaryyearly();" />  
                                                                                                    すべて <asp:DropDownList CssClass="altText" runat="server" ID="YearMonth1" onClick="javaScript: summaryyearly();"></asp:DropDownList> 
                                                                                                    <asp:TextBox ID="YearMonthDay" runat="server" CssClass="altText" Width="8%" onChange="javaScript: summaryyearly();" onClick="YEveryYr1.checked = true;" ></asp:TextBox>
                                                                                                    <br /><br />
                                                                                                    <asp:RadioButton ID="YEveryYr2" runat="server" GroupName="GYearly" onClick="javaScript: summaryyearly(); document.frmSettings2.YearMonthDay.value = '';"/> 
                                                                                                    市販 <asp:DropDownList CssClass="altText" runat="server" ID="YearMonthWeekDayNo" onClick="javaScript: summaryyearly();"></asp:DropDownList>
                                                                                                    <asp:DropDownList CssClass="altText" runat="server" ID="YearMonthWeekDay" onClick="javaScript: summaryyearly();"></asp:DropDownList> 
                                                                                                    of <asp:DropDownList CssClass="altText" runat="server" ID="YearMonth2" onClick="javaScript: summaryyearly();"></asp:DropDownList>
                                                                                                </asp:Panel>
                                                                                                <%--Custom Recurring Pattern--%>
                                                                                                <asp:Panel ID="Custom" runat="server" >
                                                                                                    <table border="0" width="370px">
                                                                                                        <tr>
                                                                                                            <td style="width:210px"><%--FB 2274--%>                            
                                                                                                                <div id="flatCalendarDisplay" style="float: left; clear: both;"></div><br />                                
                                                                                                                <div id="Div2" style="font-size: 80%; text-align: center; padding: 2px"></div>
                                                                                                            </td>
                                                                                                            <td style="width:120px">
                                                                                                                <span > 選択された日付</span><br />
                                                                                                                <asp:ListBox runat="server" id="CustomDate" Rows="8" CssClass="altSmall0SelectFormat" onChange="JavaScript: removedate(this);"></asp:ListBox>
                                                                                                                <br />
                                                                                                                <span> * 日付をクリックしてリストから削除してください</span>
                                                                                                            </td>
                                                                                                            <td style="width:40px">                            
                                                                                                                 <%--code added for Soft Edge button--%>
                                                                                                                <input type='submit' name='SoftEdgeTest1' style='max-height:0px;max-width:0px;height:0px;width:0px;display:none'/><%--edited for FF--%>
                                                                                                                <asp:Button ID="btnsortDates" runat="server" Text="並べ替え" CssClass="altixsButtonFormat" OnClientClick="javascript:return SortDates();" />
                                                                                                            </td>
                                                                                                        </tr>                                                                                            
                                                                                                    </table>
                                                                                                    
                                                                                                </asp:Panel>
                                                                                            </td>
                                                                                        </tr> 
                                                                                       
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr id="RangeRow" runat="server">
                                                                                <td colspan="2"> 
                                                                                    <table border="0" width="100%" cellpadding="5">
                                                                                        <tr>
                                                                                            <td colspan="2"> 
                                                                                              <span class="blackblodtext">                                                                                             
                                                                                              繰り返しの幅 </span>       
                                                                                            </td>            
                                                                                        </tr>
                                                                                        <tr valign="top"  >
                                                                                            <td class="blackblodtext" nowrap style="width:20%;" align="right">
                                                                                                開始 <asp:TextBox ID="StartDate" Width="100px" Font-Size="9" CssClass="altText" runat="server"  ></asp:TextBox>
                                                                                                      <img alt="" src="image/calendar.gif" border="0"  id="cal_triggerd1" style="cursor: pointer;vertical-align:top;" title="日付セレクタ" onclick="return showCalendar('<%=StartDate.ClientID%>', 'cal_triggerd1', 1, '<%=format %>');" />                 
                                                                                            </td>    
                                                                                            <td >
                                                                                                <table width="100%" border="0"> 
                                                                                                    <tr>
                                                                                                        <td class="blackxxxsboldtext" colspan="2">
                                                                                                            <asp:RadioButton ID="EndType" runat="server" GroupName="RangeGroup"  onClick="javascript: document.frmSettings2.Occurrence.value=''; document.frmSettings2.EndDate.value='';"/> 終了日なし
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td  class="blackxxxsboldtext" nowrap style="width:9%;">
                                                                                                            <asp:RadioButton ID="REndAfter" runat="server" GroupName="RangeGroup" onClick="javascript: document.frmSettings2.EndDate.value='';"/> 後に終了 
                                                                                                        </td>
                                                                                                        <td class="blackxxxsboldtext">
                                                                                                            <asp:TextBox ID="Occurrence" CssClass="altText"  Width="100px" runat="server" onClick="javascript: document.frmSettings2.REndAfter.checked=true; document.frmSettings2.EndDate.value='';"></asp:TextBox> 出現                            
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td  class="blackxxxsboldtext" nowrap="nowrap">
                                                                                                            <asp:RadioButton ID="REndBy" runat="server" GroupName="RangeGroup" onClick="javascript: document.frmSettings2.Occurrence.value='';"/> 年末までに 
                                                                                                        </td>
                                                                                                        <td nowrap>
                                                                                                            <asp:TextBox ID="EndDate" Width="100px" onblur="javascript:CheckDate(this)" onchange="javascript:CheckDate(this)" CssClass="altText" runat="server"  onClick="javascript: document.frmSettings2.REndBy.checked=true; document.frmSettings2.Occurrence.value='';" ></asp:TextBox>
                                                                                                            <img alt="" src="image/calendar.gif" border="0"  id="cal_trigger2" style="cursor: pointer;vertical-align:top;" title="日付セレクタ" onclick="return showCalendar('<%=EndDate.ClientID%>', 'cal_trigger2', 1, '<%=format %>');" />  
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>       
                                                                                        </tr>
                                                                                         <tr>
                                                                                            <td colspan="2" >
                                                                                                 注釈: 最大限度は繰り返しのシリーズにおいて<%=Application["confRecurrence"]%>の事例です。
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>  
                                                                        </table> 
                                                                    </td>
                                                                    
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    
                                               <%--Merging Recurrence end--%>
                                                </table>
                                            </td>
                                        </tr>
                                  </table>
                           <script language="javascript">
                            document.getElementById("regStartTime").controltovalidate = "confStartTime_Text";
                            document.getElementById("reqStartTime").controltovalidate = "confStartTime_Text";
                            document.getElementById("regEndTime").controltovalidate = "confEndTime_Text";
                            document.getElementById("reqEndTime").controltovalidate = "confEndTime_Text";
                            document.getElementById("regSetupStartTime").controltovalidate = "SetupTime_Text"; //buffer zone
                            document.getElementById("reqSetupStartTime").controltovalidate = "SetupTime_Text";
                            document.getElementById("regTearDownStartDate").controltovalidate = "TeardownTime_Text";//buffer zone // Edited for FF
                            
                            //alert("here");
                            //setTimeout("document.getElementById('confStartTime_Text').onchange = javascript:ChangeEndDate()", 1);
                            //setTimeout("document.getElementById('confEndTime_Text').onchange = javascript:ChangeStartDate()", 1);
                            //alert(document.getElementById("<%=Recur.ClientID %>").value);
				        if (document.getElementById("<%=Recur.ClientID %>").value != "" ) 
				        {
				            isRecur();
//				            alert(document.getElementById("<%=Recur.ClientID %>").value);
					        AnalyseRecurStr(document.getElementById("<%=Recur.ClientID %>").value);
					        st = calStart(atint[1], atint[2], atint[3]);
					        et = calEnd(st, parseInt(atint[4], 10));
					        //Merging Recurrence
					        document.getElementById("RecurringText").value =  recur_discription(document.getElementById("<%=Recur.ClientID %>").value, et, "Eastern Standard Time", Date(),"<%=Session["timeFormat"].ToString()%>","<%=Session["timezoneDisplay"].ToString()%>");
					    }
                        isRecur();
                        ChangeImmediate();
                        ChangePublic();
                        //Recurrence Fixes - hiding recur icon on single instance edit - start
                        if ("<%=isInstanceEdit%>" == "Y" )
                        {  
                            document.getElementById("NONRecurringConferenceDiv8").style.display = "none";
                        }
                        
                         //Recurrence Fixes - hiding recur icon on single instance edit - end
                        
                        if ("<%=timeZone%>" == "0" ) //FB 1425
                            document.getElementById("NONRecurringConferenceDiv3").style.display = "none";
                                                   
                           //MOJ Phase 2 - Start
                        if ("<%=client.ToString().ToUpper() %>" == "MOJ")
                        {
                            document.getElementById("NONRecurringConferenceDiv4").style.display = "none";
                            //document.getElementById("trPublic").style.display = "none";  //FB 2359
                            document.getElementById("trConfType").style.display = "none"; 
                            document.getElementById("NONRecurringConferenceDiv6").style.display =  "none"; //buffer zone
	                        document.getElementById("NONRecurringConferenceDiv7").style.display =  "none"; //buffer zone
                            
                        }
                        
                        if("<%=EnableVMR%>" == "0") //FB 2376 
                        {
                            if(document.getElementById("trVMR") != null)//FB 2272
                                document.getElementById("trVMR").style.display = "none"; 
                        }
                        else
                        {
							//changeVMR();//FB 2501
                            fnVMR();
                        }
                        //MOJ Phase 2 - End 
                         
                         //Code added for  Fb 1728
                           
        
          
     if(document.getElementById("hdnValue").value == "1")
	  {
	    if (document.frmSettings2.EndText)
		    document.frmSettings2.EndText.disabled = true;
	    if (document.frmSettings2.DurText)
		    document.frmSettings2.DurText.disabled = true;

	    document.frmSettings2.RecurValue.value = document.getElementById("Recur").value;
    	
	    if (document.getElementById("ModifyType").value=="3") {
		    document.getElementById("RemoveRecurDiv").style.display = "none";
	    }
        
         var chkrecurrence = document.getElementById("chkRecurrence");
         
        if(document.getElementById("Recur").value != "" || (chkrecurrence && chkrecurrence.checked == true))
        {
            
            var chkrecurrence = document.getElementById("chkRecurrence");
            if(chkrecurrence)
                chkrecurrence.checked = true;
            document.getElementById("hdnRecurValue").value = 'R'
            initial();
           
            fnShow();
        }
        
         fnEnableBuffer();
        
     }
     //Code added for  Fb 1728
                           
                    </script> 
                  </asp:Panel>
                </asp:View>
                <asp:View ID="SelectParticipants" runat="server">
                    <asp:Panel ID="pnlEndpoint" runat="server" Width="100%">
                            <h3>参加者を選択する</h3>
                            <input type="hidden" id="Hidden1" value="17">
                                 <table border="0" cellpadding="2" cellspacing="0" width="90%" height="95" align="center">
             <!--                     <tr>
                                    <td width="10%" height="15" align="right" valign="top"></td>
                                    <td width="90%" bordercolor="#0000ff" colspan="4" align="left">
                                      <table border="0" cellpadding="2" cellspacing="0" width="100%">
                                        <tr>
                                          <td align="center" width="4%"><span class="tableblueblodtext">削除</span></td>
                                          <td align="center" width="28%"><span class="tableblueblodtext">NAME</span></td>
                                          <td align="center" width="29%"><span class="tableblueblodtext">EMAIL</span></td>
                                          <td align="center" width="8%"><span class="tableblueblodtext"><a>External Attendees</a></span></td>
                                          <td align="center" width="8%"><span class="tableblueblodtext"><a>Room Attendees</a></span></td>
                                          <td align="center" width="4%"><span class="tableblueblodtext">CC</span></td>
                                          <td align="center" width="4%"><span class="tableblueblodtext">NOTIFY</span></td>
                                        </tr>
                                      </table>
                                    </td>
                                  </tr>-->
                                  <tr>
                                    <td height="21" align="left" valign="top" width="100" class="blackblodtext">
                                      <b>参加者</b><br />
                                      <input type="button" name="AddGroup" id="btnAddgrp" value="グループに追加" class="altShort5BlueButtonFormat" title="新しいグループに選択した参加者を追加します。" onclick="saveInGroupNET()" /> 
                                      <%--FB 1985--%>
                                      <span style="color:Red;" ><asp:Label runat="server" ID="lblParNote" Visible="true"></asp:Label></span>
                                      <input type="button" name="sbtgroup" id="sbtgroup" style="display:none;" onclick="saveGroupSucc()" /> <%--Login Management--%>
                                      <input type="button" name="cbtgroup" id="cbtgroup" style="display:none;" onclick="saveGroupFail('<%=Session["GrpErrMsg"]%>')" /> <%--Login Management--%>
                                      
            <%--<iframe src="ifrmsaveingroup.asp?wintype=ifr" name="ifrmSaveingroup" width="0" height="0"> FB 412 --%>
            <iframe src="ifrmsaveingroup.aspx?wintype=ifr" name="ifrmSaveingroup" style="display:none" width="0" height="0"> <%--Edited for FF--%>
              <p>グループページに保存</p>
            </iframe>                          
                                    </td>
                                    <td bordercolor="#0000ff" colspan="5" align="center">
                                      <table border="0" cellpadding="2" cellspacing="0" width="100%">
                                        <tr>
                                          <td width="100%" valign="top" align="left" colspan="6">

                                            <!--Changed a href Start-->
                                            <iframe align="left" height="350" name="ifrmPartylist" id="ifrmPartylist" src="settings2partyNET.aspx?wintype=ifr" valign="top" width="100%"><%--Edited for FF--%>
                                              <p>行く <a href="settings2partyNET.aspx?wintype=ifr">参加者</a></p>
                                            </iframe> 
                                            <!--Changed a href End-->

                                          </td>
                                        </tr>
                                      </table>
                                      <%--the following 2 controls have been added to confirm the validation on next and previous click under CheckFiles function--%>
                                      <asp:TextBox ID="txtTempUser" runat="server" Visible="false" Text="User" ></asp:TextBox>
                                      <asp:RequiredFieldValidator ID="reqUser" ControlToValidate="txtTempUser" runat="server"></asp:RequiredFieldValidator>
                                    </td>
                                  </tr>
                                  <tr> <%--FB 1985 - Starts--%>                                    
                                    <td height="62" align="right" valign="top" class="blackblodtext"> 
                                    <%if (!(Application["Client"].ToString().ToUpper().Equals("DISNEY")))
                                      {%> 
                                      <b>グループ</b>
                                      <% }%> 
                                    </td>
                                    <td id="trGroup" runat="server">
                                        <asp:ListBox ID="Group" runat="server" CssClass="altSelectFormat" SelectionMode="Multiple">
                                        </asp:ListBox>
                                        <asp:TextBox ID="txtUsersStr" runat="server" Width="0px" ForeColor="transparent" BackColor="transparent" BorderStyle="None" BorderColor="Transparent"></asp:TextBox>
                                        <asp:TextBox ID="txtPartysInfo" style="display:none" runat="server" Width="0px" ForeColor="Black" BackColor="transparent" BorderStyle="None" BorderColor="Transparent"></asp:TextBox><%--Edited for FF--%>
                                    </td>
                                    <td align="left" valign="middle" nowrap><font size="1" class="blackblodtext">
                                    <%if (!(Application["Client"].ToString().ToUpper().Equals("DISNEY")))
                                      {%> 
                                    追加するためにグループ名<br />をクリックしてください。<br />参加者詳細を見るにはダブ<br />ルクリックしてください。</font>
                                    <% }%> 
                                    </td> <%--FB 1985 - End--%>
                                    <td width="21%" class="blackblodtext">
                                        <%--Window Dressing--%>
                                        <label class="blackblodtext">iCal招待状を送る</label>
                                        <asp:CheckBox ID="chkICAL" runat="server" class="blackblodtext" Text="" TextAlign="left" />
                                        <br /><font size="1">カスタマイズ定例パターンには利用不可です.</font>
                                    </td>
                                    <td width="31%" align="left" valign="top">
                                      <table border="0" cellpadding="2" cellspacing="0" width="100%">
                                        <tr>
                                          <td width="100%" align="right">
                                            <input id="Button2" type="button" runat="server" name="Settings2Submit" value="すべて削除" class="altShortBlueButtonFormat" onClick="deleteAllPartyNET();" language="JavaScript">
                                            
                                            </td>
                                         
                                          <%--FB 1985 - Starts--%>
                                         <%if ((Application["Client"].ToString().ToUpper().Equals("DISNEY")))
                                           {%>
                                          <td width="100%" align="right">
                                            <input type="button" runat="server" id="VRMLookup1" value="アドレスブック" class="altLongBlueButtonFormat" onclick="getYourOwnEmailListNET();" language="JavaScript" /></td>
                                            <br />
                                            <% }
                                           else
                                           { %>
                                          <td width="100%" align="right">
                                            <input type="button" runat="server" id="VRMLookup" value="myVRMアドレスブック" class="altLongBlueButtonFormat" onclick="getYourOwnEmailListNET();" language="JavaScript" /></td>
                                            <br />
                                            <% } %><%--FB 1985 - End--%>
                                        </tr>
                                        <tr>
                                          <td width="100%" align="right" colspan="2">
                                          
                                            <input type="button" runat="server" id="btnAddNewParty" value="新規参加者追加" class="altLongBlueButtonFormat" onclick="javascript:addNewPartyNET(1);" />
                                            
                                          </td>
                                        </tr>
                                        <tr>
                                        <td></td>
                                        <td>
                                        <input type="button" runat="server" id="btnAudioparticipant" value="オーディオ参加者追加" class="altLongBlueButtonFormat" onClick="getAudioparticipantListNET();" />
                                        </td>
                                        </tr>
                                        <tr><td  colspan="2"><table cellpadding="0px" cellspacing="0px"  border="0" width="50%" align="right"><%--Edited for FF--%>
                                        <tr id="trOutlook" align="right"> <%-- FB Case 526: Saima--%>
                                          <td>
                                            <input type="button" runat="server" id="OutlookLookup" value="Outlookアドレスブック" class="altLongBlueButtonFormat" onClick="javascript:getOutLookEmailList();"></td>
                                        </tr>
                                        </table></td></tr>
                                        <tr id="trLotus" style="display:none">
                                            <td width="100%" align="right" colspan="2">
                                                <asp:Button id="btnLotus" runat="server" CssClass="altLongBlueButtonFormat" Text="Lotusアドレスブック" />
                                            </td>
                                        </tr>
                                        <tr>
                                          <td width="100%" align="right" colspan="2" height="20">
                                          </td>
                                        </tr>
                                      </table>
                                    </td>
                                    <td style="width:3%"><%--FB 2023--%>
                                     <table width="100%" border="0">
                                     <tr>
                                          <td width="100%" align="right" colspan="2" height="60">
                                          </td>
                                      </tr>
                                      <tr style="vertical-align:bottom">
                                       <td>
                                        <asp:ImageButton ID="imgParNote" src="image/info.png" runat="server" OnClientClick="javascript:return false;"/> 
                                       </td>
                                      </tr>
                                      <tr>
                                          <td width="100%" align="right" colspan="2" height="20">
                                          </td>
                                      </tr>
                                     </table>                                       
                                    </td>
                                   </tr>
                                  </table>
                                <script language="javascript">
                                //FB 1985 - Starts
                                  if('<%=Application["Client"]%>'.toUpperCase() == "DISNEY")
                                   {
                                     document.getElementById("trGroup").style.display = "none";
                                   }
                                   //FB 1985 - End
                                    var recurText = document.getElementById("Recur").value;
                                    function dothis()
                                    {
                                        //alert("before dothis");
                                        ifrmPartylist.bfrRefresh();
                                        //alert("after dothis");
                                    }
                                    if (recurText != "")
                                    {
                                        //alert(recurText.split("#")[1].split("&")[0]);
                                        if (recurText.split("#")[1].split("&")[0] == "5")
                                        {
                                            document.getElementById("<%=chkICAL.ClientID %>").disabled = true;
                                            //document.getElementById("<%=chkICAL.ClientID %>").checked = false;
                                        }
                                        else
                                        {
                                            document.getElementById("<%=chkICAL.ClientID %>").disabled = false;
                                            //document.getElementById("<%=chkICAL.ClientID %>").checked = true;
                                        }
                                    }
            /* FB Case 727 Saima
                                    if(document.all)
                                        document.getElementById("ifrmPartylist").attachEvent("onblur",dothis);
                                    else
                                        document.getElementById("ifrmPartylist").contentDocument.addEventListener("blur",dothis,false);
            */                            
                                    if (document.getElementById("trOutlook") != null) // FB Case 526: Saima
                                    {   
                                        /*Code Modified For Enabling Outlook button on  21Mar09 -  FB 412 - Start */
                                        //if ("<%=Session["emailClient"] %>" != "1")
                                            //document.getElementById("trOutlook").style.display = "none";
                                            if ("<%=Session["emailClient"] %>" == "1")
                                            document.getElementById("trOutlook").style.display = "block";
                                        else
                                             document.getElementById("trOutlook").style.display = "none"; 
                                        /*Code Modified For Enabling Outlook button on  21Mar09 -  FB 412 - End */
                                    }
                                    if (document.getElementById("trLotus") != null) // FB Case 576: Saima
                                    {   
                                        if ("<%=Session["emailClient"] %>" != "2")
                                            document.getElementById("trLotus").style.display = "none";
                                    }
                                </script> 
                    </asp:Panel>
                </asp:View>
                <asp:View ID="SelectRooms" runat="server" OnActivate="ExpandTree"  OnDeactivate="SelectTree">
                                    <asp:Panel ID="pnlResource" runat="server" Width="100%">
                <h3>部屋選択</h3>
                <input type="hidden" id="Hidden2" value="18">

<!--                <img src="image/locationlist.jpg" />-->
               <table border="0" cellpadding="3" cellspacing="0" width="100%">
                   <tr>
                       <td align="right" valign="top" width="10%">
                       </td>
                       <td align="left" class="blackblodtext" valign="top" width="78%">
                        <table border="0" style="width: 100%">
                            <tr style="display:none;">
                                <td valign="top" align="left" width="80">
                                <%--Code changed for Search Room Error - start --%>                                
                                <input type="button" value="比較" id="btnCompare" runat="server" onclick="javascript:compareselected();" class="altShortBlueButtonFormat" />
                                <%--Code changed for Search Room Error - end --%>                                
                                </td>
                                <td valign="top" align="left" class="blackblodtext">
                                   <asp:RadioButtonList ID="rdSelView" runat="server" CssClass="blackblodtext" OnSelectedIndexChanged="rdSelView_SelectedIndexChanged"
                                      RepeatDirection="Horizontal" AutoPostBack="True" RepeatLayout="Flow"><%--FB 1481--%>
                                      <asp:ListItem Selected="True" Value="1"><span class="blackblodtext">レベルのビュー</span></asp:ListItem>
                                      <asp:ListItem Value="2"><span class="blackblodtext">リスト一覧</span></asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                        </table>                     
                       </td>
                   </tr>

                  <tr>
                    
                    <td width="100%" colspan="2" align="left" style="font-weight: bold; font-size: small; color: green; font-family: arial" valign="top">
                    <%-- Code added for Room Search --%>
    <iframe id="RoomFrame" runat="server" width="100%" valign="top" height="600px"></iframe>
    <div style="display:none;">
                    <asp:Panel ID="pnlLevelView" runat="server" Height="300px" Width="100%" ScrollBars="Auto" BorderColor="Blue" BorderStyle="Solid" BorderWidth="1px" HorizontalAlign="Left">
                        <asp:TreeView ID="treeRoomSelection" runat="server" BorderColor="White" Height="90%" ShowCheckBoxes="All" onclick="javascript:getRooms(event)"
                            ShowLines="True" Width="95%" OnTreeNodeCheckChanged="treeRoomSelection_TreeNodeCheckChanged" OnSelectedNodeChanged="treeRoomSelection_SelectedNodeChanged" >
                            <NodeStyle CssClass="treeNode" />
                            <RootNodeStyle CssClass="treeRootNode" />
                            <ParentNodeStyle CssClass="treeParentNode" />
                            <LeafNodeStyle CssClass="treeLeafNode" />
                        </asp:TreeView>
                        
                        </asp:Panel>
                        <asp:Panel ID="pnlListView" runat="server" BorderColor="Blue" BorderStyle="Solid"
                            BorderWidth="1px" Height="300px" ScrollBars="Auto" Visible="False" Width="100%" HorizontalAlign="Left" Direction="LeftToRight" Font-Bold="True" Font-Names="arial" Font-Size="Small" ForeColor="Green">
                            <%--code changed for FB 1319 -- start--%>
                            <input type="checkbox" id="selectAllCheckBox" runat="server" onclick="CheckBoxListSelect('lstRoomSelection',this);" /><font size="2"> すべてを選択</font>
                            <br />
                            <asp:CheckBoxList ID="lstRoomSelection" runat="server" Height="95%" Width="95%" Font-Size="Smaller" ForeColor="ForestGreen" onclick="javascript:getValues(event)" Font-Names="arial" RepeatLayout="Flow">
                            </asp:CheckBoxList>
                            <%--code changed for FB 1319 -- end--%>
                        </asp:Panel>
                         <%--Code added for Search Room Error - start --%>                                
                        <asp:Panel ID="pnlNoData" runat="server" BorderColor="Blue" BorderStyle="Solid"
                            BorderWidth="1px" Height="300px" ScrollBars="Auto" Visible="False" Width="100%" HorizontalAlign="Left" Direction="LeftToRight" Font-Size="Small">
                            <table><tr align="center"><td>
                            選択した日付/時刻のために利用可能な部屋はありません. <%--FB 1481 --%>
                            </td></tr></table>                            
                        </asp:Panel>
                         <%--Code added for Search Room Error - start --%>  
                        <asp:TextBox runat="server" ID="txtTemp" Text="test" Visible="false" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Enabled="false" ControlToValidate="txtTemp" runat="server" ></asp:RequiredFieldValidator>
                        
                        </div>
                    </td>
                  </tr>
                  
                  <tr>
                    <td></td>
                    <td align="right">
                      <table border="0" cellspacing="5" cellpadding="0">
                        <tr>
                          <td align="left">
                            <label class="blackblodtext" style="display:none;">空室状況をチェック</label>
                          </td>
                          <td align="right">
                            <asp:Button runat="server" ID="GetAvailableRoom" Text="リフレッシュ" CssClass="altShort2BlueButtonFormat" OnClientClick="javascript:DataLoading(1);" OnClick="RefreshRoom" style="display:none;"/>
                          </td>
                           <!--Meeting Planner FB 1048-->
                           <td id="tdMeetingPlanner" runat="server"> <%--FB 1985--%>
                          <asp:Button Text="ミーティングプランナー" ID="MeetingPlanner" Width="120" CssClass="altShort2BlueButtonFormat" OnClientClick="javascript:return CallMeetingPla(); return false;"  runat="server" />
                          </td>
                          <td align="right" style="font-weight:bold" id="btnCheckAvailDIV" runat="server"> <%--FB 2274--%>
                              <asp:Button ID="openCalendar" runat="server" Text="カレンダー" CssClass="altShort2BlueButtonFormat" OnClientClick="javascript:goToCal(); return false;" />
                          </td>
                        </tr>
                        <%--FB 1985 - Start--%>
                        <%if ((Application["Client"].ToString().ToUpper().Equals("DISNEY")))
                          {%> 
                        <tr>
                            <td align="right" colspan="4" style="padding:0px;">
                                <table width="60px" border="0" align="right" id="tblMeetPlan" runat="server">
                                    <tr> 
                                        <td align="center" onmouseover="javascript:return fnShowHideMeetLink('1');" onmouseout="javascript:return fnShowHideMeetLink('0');" >&nbsp;
                                            <asp:LinkButton ID="LnkMeetExpand" runat="server" Text="拡大する" OnClientClick="javascript:return fnShowMeetPlanner()"></asp:LinkButton>
                                        </td>
                                     </tr>
                                </table>
                            </td>
                        </tr>
                         <%} %>
                         <%--FB 1985 - End--%>
                      </table>
                    </td>
                  </tr>
                 </table>
                 <input type="hidden" runat="server" id="txtHasCalendar" />
                 <script language="javascript">
                   if(document.getElementById("<%=txtHasCalendar.ClientID %>").value.toUpperCase() == "FALSE")
                        document.getElementById("btnCheckAvailDIV").style.display = "none";
                 </script> 
                                    </asp:Panel>
                                </asp:View>
                                <asp:View ID="SelectAudioVisual" runat="server" >
                                    <asp:Panel ID="pnlAudioVisual" runat="server" Width="100%" HorizontalAlign="center">
                                      <%--FB 2359--%>
                                    <table id="tblAVExpand" runat="server" width="8%" border="0" align="left" style="display:none">
                                        <tr> 
                                            <td align="left" onmouseover="javascript:return fnShowHideAVLink('1');" onmouseout="javascript:return fnShowHideAVLink('0');" >&nbsp;
                                                <asp:LinkButton ID="LnkAVExpand" runat="server" Text="拡大する" OnClientClick="javascript:return fnShowAVParams()"></asp:LinkButton>
                                            </td>
                                         </tr>
                                    </table>
                    <%--FB 1985--%>
                <%if ((Application["Client"].ToString().ToUpper().Equals("DISNEY")))
                  {%>
                <h3>オーディオ設定</h3>
                <%}
                  else
                  {%>
                <h3>アドバンスサーチ（入力してください</h3>
                <%} %>
                <table width="100%" cellspacing="2" cellpadding="2">
                    <%-- Code Modified For FB 1422 - Added ID  to <tr> tag --%>             
                    <tr id="trAVCommonSettings" runat="server" align="left" width="100%"> <%--Edited for FF--%>
                        <td>
                            <table border="0" width="900px" align="center">
                                <tr>
                                    <td colspan="4" class="subtitlexxsblueblodtext">共通設定（ベンダーニュートラル）</td>
                                </tr>
                                <tr>
                                    <td align="right" class="blackblodtext">
                                        ～へのネットワークアクセスを規制する
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="lstRestrictNWAccess" runat="server" CssClass="altSelectFormat">
                                            <asp:ListItem Text="IP" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="IP,ISDN" Value="2"></asp:ListItem>
                                            <asp:ListItem Text="IP,ISDN,SIP" Value="3"></asp:ListItem>
                                            <asp:ListItem Text="IP,ISDN,SIP,MPI" Value="4" Selected="True"></asp:ListItem> <%--FB 1721--%><%--FB 2390--%>
                                        </asp:DropDownList>
                                    </td>
                                    <td align="right" class="blackblodtext">
                                        へ使用規制する<span class="reqfldstarText">*</span>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="lstRestrictUsage" runat="server" CssClass="altSelectFormat">
                                            <%--<asp:ListItem Text="None" Value="1"></asp:ListItem>--%><%--FB 1744--%>
                                            <asp:ListItem Text="音声のみ" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="オーディオ/ビデオ" Value="2"></asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="reqUsage" ControlToValidate="lstRestrictUsage" InitialValue="0" ErrorMessage="必須" runat="server" Display="Dynamic"></asp:RequiredFieldValidator>
                                   </td>
                                </tr>
                                <tr>
                                    <td align="right" class="blackblodtext">
                                        最大限ビデオポート
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtMaxVideoPorts" runat="server" CssClass="altText" ></asp:TextBox>
                                    </td>
                                    <td align="right" class="blackblodtext">
                                        最大限オーディオポート
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtMaxAudioPorts" CssClass="altText" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" class="blackblodtext">
                                        ビデオコーデック <span class="reqfldstarText">*</span>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="lstVideoCodecs" CssClass="altSelectFormat" DataTextField="Name" DataValueField="ID" runat="server">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="reqVideoCodecs" ControlToValidate="lstVideoCodecs" InitialValue="-1" ErrorMessage="必須" runat="server" Display="Dynamic"></asp:RequiredFieldValidator>
                                    </td>
                                    <td align="right" class="blackblodtext">
                                        オーディオコーデック <span class="reqfldstarText">*</span>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="lstAudioCodecs" CssClass="altSelectFormat" DataTextField="Name" DataValueField="ID" runat="server">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="reqAudioCodecs" ControlToValidate="lstAudioCodecs" InitialValue="-1" ErrorMessage="必須" runat="server" Display="Dynamic"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" class="blackblodtext">
                                        デゥアルストリームモード 
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="chkDualStreamMode" runat="server" />
                                    </td>
                                     <td align="right" class="blackblodtext">
                                        ビデオ表示
                                    </td>
                                    <td>
                                        <asp:Image ID="imgVideoDisplay" runat="server" />
                                        <input id="Button3" type="button" runat="server" name="ConfLayoutSubmit" value="変更" class="altShortBlueButtonFormat" onclick="javascript: managelayout('', '01', '');" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" class="blackblodtext">
                                        暗号化
                                    </td>
                                    <td align="left">
                                       <asp:CheckBox ID="chkEncryption" runat="server" />
                                    </td>
                                    <td align="right" class="blackblodtext">
                                        シングルダイアルイン番号
                                    </td>
                                    <td align="left">
                                       <asp:CheckBox ID="chkSingleDialin" runat="server" />
                                    </td>
                                </tr>
                                <tr style="display:none;"><%--FB 2341--%>
                                    <td align="right" class="blackblodtext">
                                        リアルタイムのアシスタント
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="chkisLiveAssitant" runat="server" />
                                    </td>
                                    <td align="right" class="blackblodtext">
                                        専用エンジニア
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="chkisDedicatedEngineer" runat="server" />
                                    </td>
                                </tr>
                                <%--Code chaned for FB 1360 --%>
                                <tr>
                                    <td align="right" class="blackblodtext">
                                        最大ラインレート <span class="reqfldstarText">*</span>
                                    </td>
                                    <td>
                                        <asp:DropDownList CssClass="altSelectFormat" ID="lstLineRate" runat="server" DataTextField="LineRateName" DataValueField="LineRateID"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="reqMaxLineRate" ControlToValidate="lstLineRate" InitialValue="-1" ErrorMessage="必須" runat="server" Display="Dynamic"></asp:RequiredFieldValidator>
                                    </td>
                                    <%--FB 2501 Starts--%>
                                     <td id="tdFECC" runat="server" align="right" class="blackblodtext">FECC</td><%--FB 2571--%>
                                    <td><asp:CheckBox ID="chkFECC" runat="server" /></td>
                                 </tr>
                                <%-- FB 2501 Ends --%>
                                <%--Code chaned for FB 1360 --%>
                                 <tr>
                                    <td colspan="4" class="subtitlexxsblueblodtext">Polycom特定設定
                                        <asp:CheckBox ID="chkPolycomSpecific" onclick="javascript:CheckPolycom(this)" runat="server" />
                                    </td>
                                </tr>
                                <tr id="trPoly1">
                                    <td align="right" class="blackblodtext">
                                        ビデオモード
                                    </td>
                                    <td>
                                        <asp:DropDownList CssClass="altSelectFormat" ID="lstVideoMode" runat="server" DataTextField="Name" DataValueField="ID">
                                        </asp:DropDownList>
                                    </td>
                                    <%--Code chaned for FB 1360 --%>
                                    <td colspan="2"></td>
                                    <%--Code chaned for FB 1360 --%>
                                </tr>
                                <tr id="trPoly2">
                                    <td align="right" class="blackblodtext">
                                        講義モード
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="chkLectureMode" runat="server" onclick="javascript:CheckLectureMode()" />
                                    </td>
                                    <td align="right" id="trLecture1" class="blackblodtext">
                                        講師
                                    </td>
                                    <td align="left" id="trLecture2">
                                        <asp:DropDownList ID="lstEndpoints" runat="server" CssClass="altSelectFormat"></asp:DropDownList>
<%--                                        <asp:RequiredFieldValidator ID="reqLecturer" InitialValue="-1" ControlToValidate="lstEndpoints" runat="server" ErrorMessage="必須" Display="Dynamic"></asp:RequiredFieldValidator>
--%>                                    </td>
                                </tr>
                                <tr id="trPoly3">
                                    <td align="right" class="blackblodtext">
                                        ポートでの会議
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="chkConfOnPort" runat="server" />
                                    </td>
                                    <td align="right">
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                 <%-- FB 2426 Starts --%>
                                        <tr id="trGuestLocation" runat="server">
                                            <td colspan="4" class="subtitlexxsblueblodtext">
                                                ゲストの場所
                                            </td>
                                        </tr>
                                        <tr id="trVideoGuestLocation" runat="server">
                                            <td class="blackblodtext" >
                                                ビデオゲストロケーション
                                            </td>
                                            <td id="tdbtnGuestLocation" colspan="3">
                                                <asp:Button ID="btnGuestLocation" runat="server" Text="ビデオゲストの場所を追加"
                                                    class="altLongBlueButtonFormat" OnClientClick="javascript:return fnValidator();" /><br />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">
                                                <ajax:ModalPopupExtender ID="guestLocationPopup" runat="server" TargetControlID="btnGuestLocation"
                                                    PopupControlID="PopupLdapPanel" DropShadow="false" Drag="true" BackgroundCssClass="modalBackground"
                                                    CancelControlID="ClosePUp" BehaviorID="btnGuestLocation">
                                                </ajax:ModalPopupExtender>
                                                <asp:Panel ID="PopupLdapPanel" runat="server" Width="98%" Height="98%" HorizontalAlign="Center"
                                                    CssClass="treeSelectedNode" ScrollBars="Vertical">
                                                    <table align="center" cellpadding="3" cellspacing="0" width="98%" style="border-collapse: collapse;
                                                        height: 100%;">
                                                        <tr>
                                                            <td align="center">
                                                                <table width="100%" border="0" cellpadding="3" style="border-collapse: collapse;
                                                                    height: 100%;">
                                                                    <tr>
                                                                        <td colspan="6">
                                                                            <h3>
                                                                                ビデオゲストロケーション</h3>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 1%;">
                                                                        </td>
                                                                        <td align="left" nowrap="nowrap" class="blackblodtext" style="width: 10%;">
                                                                            <b>クライアントのサイト名 <span class="reqfldstarText">*</span></b>
                                                                        </td>
                                                                        <td align="left" style="width: 20%">
                                                                            <asp:TextBox ID="txtsiteName" runat="server" CssClass="altText" maxlength="20" Width="187px" /><br /><%--FB 2523--%>
                                                                            <asp:RequiredFieldValidator ID="reqRoomName" Enabled="false" runat="server" ControlToValidate="txtsiteName"
                                                                                Display="dynamic" SetFocusOnError="true" ErrorMessage="必須" ValidationGroup="Submit1"></asp:RequiredFieldValidator>
                                                                            <asp:RegularExpressionValidator ID="regRoomName" Enabled="false" ControlToValidate="txtsiteName"
                                                                                Display="dynamic" runat="server" ValidationGroup="Submit1" SetFocusOnError="true"
                                                                                ErrorMessage="<br> & < > + % \ ? | ^ = ! ` [ ] { } $ @ ~ は無効な記号です"
                                                                                ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>^+?|!`\[\]{}\=@$%&~]*$"></asp:RegularExpressionValidator>
                                                                        </td>
                                                                        <td style="width: 5%">
                                                                        </td>
                                                                        <td align="left" class="blackblodtext" style="width: 10%">
                                                                            <b>アドレス</b>
                                                                        </td>
                                                                        <td align="left" style="vertical-align: top; width: 54%">
                                                                            <asp:TextBox ID="txtAddress" runat="server" CssClass="altText" TextMode="MultiLine"
                                                                                Width="187px" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                        </td>
                                                                        <td align="left" class="blackblodtext" nowrap="nowrap">
                                                                            <b>担当者名 <span class="reqfldstarText">*</span></b>
                                                                        </td>
                                                                        <td align="left" nowrap="nowrap">
                                                                            <asp:TextBox ID="txtApprover5" runat="server" CssClass="altText" Width="187px" />
                                                                            <img id="Img10" onclick="javascript:getYourOwnEmailList(4)" alt="" src="image/edit.gif" />
                                                                            <asp:TextBox ID="hdnApprover5" runat="server" BackColor="Transparent" BorderColor="White"
                                                                                BorderStyle="None" Width="0px" ForeColor="Black" Style="display: none"></asp:TextBox>
                                                                            <a href="javascript: deleteAssistant();" onmouseover="window.status='';return true;">
                                                                                <img border="0" src="image/btn_delete.gif" alt="delete" width="16" height="16" /></a><br />
                                                                            <asp:RequiredFieldValidator ID="reqcontactName" Enabled="false" runat="server" ControlToValidate="txtApprover5" 
                                                                                Display="dynamic" SetFocusOnError="true" ErrorMessage="必須" ValidationGroup="Submit1"></asp:RequiredFieldValidator>
                                                                        </td>
                                                                        <td>
                                                                        </td>
                                                                        <td align="left" class="blackblodtext" nowrap="nowrap">
                                                                            <b>州/地域</b>
                                                                        </td>
                                                                        <td align="left">
                                                                            <asp:TextBox ID="txtState" runat="server" CssClass="altText" Width="187px" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                        </td>
                                                                        <td align="left" class="blackblodtext" nowrap="nowrap">
                                                                            <b>連絡先電子メールアドレス</b>
                                                                        </td>
                                                                        <td align="left">
                                                                            <asp:TextBox ID="txtEmailId" runat="server" CssClass="altText" Width="187px" /><br />
                                                                        </td>
                                                                        <td>
                                                                        </td>
                                                                        <td align="left" class="blackblodtext">
                                                                            <b>シティ</b>
                                                                        </td>
                                                                        <td align="left">
                                                                            <asp:TextBox ID="txtCity" runat="server" CssClass="altText" Width="187px" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                        </td>
                                                                        <td align="left" class="blackblodtext" nowrap="nowrap">
                                                                            <b>部屋の電話 #</b>
                                                                        </td>
                                                                        <td align="left">
                                                                            <asp:TextBox ID="txtPhone" runat="server" CssClass="altText" Width="187px" />
                                                                        </td>
                                                                        <td>
                                                                        </td>
                                                                        <td align="left" class="blackblodtext" nowrap="nowrap">
                                                                            <b>郵便番号</b>
                                                                        </td>
                                                                        <td align="left">
                                                                            <asp:TextBox ID="txtZipcode" runat="server" CssClass="altText" Width="187px" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="4">
                                                                        </td>
                                                                        <td align="left" class="blackblodtext" nowrap="nowrap">
                                                                            <b>サイトの国</b>
                                                                        </td>
                                                                        <td align="left">
                                                                            <asp:DropDownList ID="lstCountries" CssClass="altText" runat="server" DataTextField="Name"
                                                                                DataValueField="ID" Width="195px">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="6" align="left">
                                                                            <table border="0" width="85%" cellpadding="1">
                                                                                <tr>
                                                                                    <td colspan="8" class="blackblodtext" align="left" nowrap="nowrap">
                                                                                        <br />
                                                                                        ビデオシステムに接続するための情報をダイヤルする。
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="8"  align="left" nowrap="nowrap">
                                                                                        注：ネットワークの外部から接続するために使用できる形式で以下の情報を提供してください。<br />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="8" align="left" nowrap="nowrap" class="subtitlexxsblueblodtext">
                                                                                        IPアドレス
                                                                                    </td>
                                                                                </tr>
                                                                                <tr align="left" nowrap="nowrap">
                                                                                    <td class="blackblodtext" >
                                                                                        <b>アドレス</b>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="txtIPAddress" runat="server" CssClass="altText" Width="120px" />
                                                                                        <asp:RequiredFieldValidator ID="reqIPAddress" Enabled="false" ControlToValidate="txtIPAddress" ErrorMessage="必須"
                                                                                            runat="server" Display="Dynamic" ValidationGroup="Submit1"></asp:RequiredFieldValidator>
                                                                                        <asp:RegularExpressionValidator ID="regIPAddress" Enabled="false" ControlToValidate="txtIPAddress"
                                                                                            Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="IPアドレスが無効です"
                                                                                            ValidationExpression="^([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}$"></asp:RegularExpressionValidator>
                                                                                    </td>
                                                                                    <td class="blackblodtext">
                                                                                        <b>パスワード</b>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="txtIPPassword" runat="server" CssClass="altText"
                                                                                            Width="120px" />
                                                                                        <asp:CompareValidator ID="cmpIPValPwd1" runat="server" Enabled="false" ControlToCompare="txtIPconfirmPassword"
                                                                                            ControlToValidate="txtIPPassword" Display="Dynamic" ErrorMessage="再入力するパスワード"
                                                                                            ValidationGroup="Submit1"></asp:CompareValidator>
                                                                                    </td>
                                                                                    <td class="blackblodtext" nowrap="nowrap">
                                                                                        <b>パスワードの確認</b>
                                                                                    </td>
                                                                                    <td nowrap="nowrap">
                                                                                        <asp:TextBox ID="txtIPconfirmPassword" runat="server" CssClass="altText"
                                                                                            Width="120px" />
                                                                                        <asp:CompareValidator ID="cmpIPValPwd2" runat="server" Enabled="false" ControlToCompare="txtIPPassword"
                                                                                            ControlToValidate="txtIPconfirmPassword" Display="Dynamic" ErrorMessage="パスワードが一致しません"
                                                                                            ValidationGroup="Submit1"></asp:CompareValidator>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr align="left" >
                                                                                    <td class="blackblodtext" nowrap="nowrap">
                                                                                        ライン速度
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:DropDownList CssClass="altSelectFormat" Width="125px" ID="lstIPlinerate" runat="server"
                                                                                            DataTextField="LineRateName" DataValueField="LineRateID">
                                                                                        </asp:DropDownList>
                                                                                        <asp:RequiredFieldValidator ID="reqIPlinerate" Enabled="false" ControlToValidate="lstIPlinerate"
                                                                                            InitialValue="-1" ErrorMessage="必須" runat="server" Display="Dynamic" ValidationGroup="Submit1"></asp:RequiredFieldValidator>
                                                                                    </td>
                                                                                    <td class="blackblodtext" style="display: none">
                                                                                        <b>機器</b>
                                                                                    </td>
                                                                                    <td style="display: none">
                                                                                        <asp:DropDownList CssClass="altSelectFormat" ID="lstIPVideoEquipment" runat="server"
                                                                                            DataTextField="VideoEquipmentName" DataValueField="VideoEquipmentID">
                                                                                        </asp:DropDownList>
                                                                                    </td>
                                                                                    <td class="blackblodtext" nowrap="nowrap">
                                                                                        <b>接続タイプ</b>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:DropDownList ID="lstIPConnectionType" Width="125px" CssClass="altSelectFormat"
                                                                                            runat="server" DataTextField="Name" DataValueField="ID">
                                                                                        </asp:DropDownList>
                                                                                        <asp:RequiredFieldValidator ID="reqIPConnectionType" Enabled="false" ControlToValidate="lstIPConnectionType"
                                                                                            ValidationGroup="Submit1" InitialValue="-1" ErrorMessage="必須" runat="server"
                                                                                            Display="Dynamic"></asp:RequiredFieldValidator>
                                                                                    </td>
                                                                                    <td class="blackblodtext">
                                                                                        デフォルトを使用
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:RadioButton ID="radioIsDefault" runat="server" GroupName="RDaily" Checked="true" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="left" colspan="8" nowrap="nowrap" class="subtitlexxsblueblodtext">
                                                                                        E164/SIPアドレス
                                                                                    </td>
                                                                                </tr>
                                                                                <tr align="left" nowrap="nowrap">
                                                                                    <td class="blackblodtext" >
                                                                                        <b>アドレス</b>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="txtSIPAddress" runat="server" CssClass="altText" Width="120px" />
                                                                                        <asp:RequiredFieldValidator ID="reqSIPAddress" Enabled="false" ControlToValidate="txtSIPAddress"
                                                                                            ErrorMessage="必須" runat="server" Display="Dynamic" ValidationGroup="Submit1"></asp:RequiredFieldValidator>
                                                                                        <asp:RegularExpressionValidator ID="regSIPAddress" Enabled="false" ControlToValidate="txtSIPAddress"
                                                                                            Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="E164/SIPアドレスが無効です"
                                                                                            ValidationExpression="\d+"></asp:RegularExpressionValidator>
                                                                                    </td>
                                                                                    <td class="blackblodtext">
                                                                                        <b>パスワード</b>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="txtSIPPassword" runat="server" CssClass="altText"
                                                                                            Width="120px" />
                                                                                        <asp:CompareValidator ID="cmpSIPValPwd1" Enabled="false" runat="server" ControlToCompare="txtSIPconfirmPassword"
                                                                                            ControlToValidate="txtSIPPassword" Display="Dynamic" ErrorMessage="再入力するパスワード"
                                                                                            ValidationGroup="Submit1"></asp:CompareValidator>
                                                                                    </td>
                                                                                    <td class="blackblodtext" nowrap="nowrap">
                                                                                        <b>パスワードの確認</b>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="txtSIPconfirmPassword" runat="server" CssClass="altText"
                                                                                            Width="120px" />
                                                                                        <asp:CompareValidator ID="cmpSIPValPwd2" Enabled="false" runat="server" ControlToCompare="txtSIPPassword"
                                                                                            ControlToValidate="txtSIPconfirmPassword" Display="Dynamic" ErrorMessage="パスワードが一致しません"
                                                                                            ValidationGroup="Submit1"></asp:CompareValidator>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr align="left">
                                                                                    <td class="blackblodtext" nowrap="nowrap">
                                                                                        ライン速度
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:DropDownList CssClass="altSelectFormat" Width="125px" ID="lstSIPlinerate" runat="server"
                                                                                            DataTextField="LineRateName" DataValueField="LineRateID">
                                                                                        </asp:DropDownList>
                                                                                        <asp:RequiredFieldValidator ID="reqSIPlinerate" Enabled="false" ControlToValidate="lstSIPlinerate"
                                                                                            InitialValue="-1" ErrorMessage="必須" runat="server" Display="Dynamic" ValidationGroup="Submit1"></asp:RequiredFieldValidator>
                                                                                    </td>
                                                                                    <td class="blackblodtext" style="display: none">
                                                                                        <b>機器</b>
                                                                                    </td>
                                                                                    <td style="display: none">
                                                                                        <asp:DropDownList CssClass="altSelectFormat" ID="lstSIPVideoEquipment" runat="server"
                                                                                            DataTextField="VideoEquipmentName" DataValueField="VideoEquipmentID">
                                                                                        </asp:DropDownList>
                                                                                    </td>
                                                                                    <td class="blackblodtext" nowrap="nowrap">
                                                                                        <b>接続タイプ</b>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:DropDownList ID="lstSIPConnectionType" Width="125px" CssClass="altSelectFormat"
                                                                                            runat="server" DataTextField="Name" DataValueField="ID">
                                                                                        </asp:DropDownList>
                                                                                        <asp:RequiredFieldValidator ID="reqSIPConnectionType" Enabled="false" ControlToValidate="lstSIPConnectionType"
                                                                                            ValidationGroup="Submit1" InitialValue="-1" ErrorMessage="必須" runat="server"
                                                                                            Display="Dynamic"></asp:RequiredFieldValidator>
                                                                                    </td>
                                                                                    <td class="blackblodtext">
                                                                                        デフォルトを使用
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:RadioButton ID="radioIsDefault2" runat="server" GroupName="RDaily" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="left" colspan="8" class="subtitlexxsblueblodtext" >
                                                                                        ISDNアドレス
                                                                                    </td>
                                                                                </tr>
                                                                                <tr align="left" nowrap="nowrap">
                                                                                    <td class="blackblodtext">
                                                                                        <b>の住所</b>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="txtISDNAddress" runat="server" CssClass="altText" Width="120px" />
                                                                                        <asp:RequiredFieldValidator ID="reqISDNAddress" Enabled="false" ControlToValidate="txtISDNAddress"
                                                                                            ErrorMessage="必須" runat="server" Display="Dynamic" ValidationGroup="Submit1"></asp:RequiredFieldValidator>
                                                                                        <asp:RegularExpressionValidator ID="regISDNAddress" ControlToValidate="txtISDNAddress"
                                                                                            Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="ISDNアドレスが無効です"
                                                                                            ValidationExpression="\d+"></asp:RegularExpressionValidator>
                                                                                    </td>
                                                                                    <td class="blackblodtext">
                                                                                        <b>パスワード</b>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="txtISDNPassword" runat="server" CssClass="altText"
                                                                                            Width="120px" />
                                                                                        <asp:CompareValidator ID="cmpISDNValPwd1" runat="server" Enabled="false" ControlToCompare="txtISDNconfirmPassword"
                                                                                            ControlToValidate="txtISDNPassword" Display="Dynamic" ErrorMessage="再入力するパスワード"
                                                                                            ValidationGroup="Submit1"></asp:CompareValidator>
                                                                                    </td>
                                                                                    <td class="blackblodtext" nowrap="nowrap">
                                                                                        <b>パスワードの確認</b>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="txtISDNconfirmPassword" runat="server" CssClass="altText"
                                                                                            Width="120px" />
                                                                                        <asp:CompareValidator ID="cmpISDNValPwd2" runat="server" Enabled="false" ControlToCompare="txtISDNPassword"
                                                                                            ControlToValidate="txtISDNconfirmPassword" Display="Dynamic" ErrorMessage="パスワードが一致しません"
                                                                                            ValidationGroup="Submit1"></asp:CompareValidator>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr align="left" >
                                                                                    <td class="blackblodtext" nowrap="nowrap">
                                                                                        ライン速度
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:DropDownList CssClass="altSelectFormat" Width="125px" ID="lstISDNlinerate" runat="server"
                                                                                            DataTextField="LineRateName" DataValueField="LineRateID">
                                                                                        </asp:DropDownList>
                                                                                        <asp:RequiredFieldValidator ID="reqISDNlinerate" Enabled="false" ControlToValidate="lstISDNlinerate"
                                                                                            InitialValue="-1" ErrorMessage="必須" runat="server" Display="Dynamic" ValidationGroup="Submit1"></asp:RequiredFieldValidator>
                                                                                    </td>
                                                                                    <td class="blackblodtext" style="display: none">
                                                                                        <b>機器</b>
                                                                                    </td>
                                                                                    <td style="display: none">
                                                                                        <asp:DropDownList CssClass="altSelectFormat" ID="lstISDNVideoEquipment" runat="server"
                                                                                            DataTextField="VideoEquipmentName" DataValueField="VideoEquipmentID">
                                                                                        </asp:DropDownList>
                                                                                    </td>
                                                                                    <td class="blackblodtext" nowrap="nowrap">
                                                                                        <b>接続タイプ</b>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:DropDownList ID="lstISDNConnectionType" Width="125px" CssClass="altSelectFormat"
                                                                                            runat="server" DataTextField="Name" DataValueField="ID">
                                                                                        </asp:DropDownList>
                                                                                        <asp:RequiredFieldValidator ID="reqISDNConnectionType" Enabled="false" ControlToValidate="lstISDNConnectionType"
                                                                                            ValidationGroup="Submit1" InitialValue="-1" ErrorMessage="必須" runat="server"
                                                                                            Display="Dynamic"></asp:RequiredFieldValidator>
                                                                                    </td>
                                                                                    <td class="blackblodtext">
                                                                                        デフォルトを使用
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:RadioButton ID="radioIsDefault3" runat="server" GroupName="RDaily" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr align="center" style="height: 50%">
                                                                        <td align="center" colspan="6">
                                                                            <br />
                                                                            <input align="middle" type="button" runat="server" validationgroup="Submit1" id="ClosePUp"
                                                                                value=" 閉じる " class="altShortBlueButtonFormat" onserverclick="fnGuestLocationCancel"
                                                                                onclick="javascript:return fnDisableValidator();" />
                                                                            <asp:Button ID="btnGuestLocationSubmit" runat="server" Text="提出する" ValidationGroup="Submit1"
                                                                                OnClick="fnGuestLocationSubmit" class="altShortBlueButtonFormat" OnClientClick="javascript:return ValidateflyEndpoints();" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                        <tr id="OnFlyRowGuestRoom" runat="server">
                                            <td style="text-align: left" colspan="4">
                                                <asp:DataGrid ID="dgOnflyGuestRoomlist" runat="server" AutoGenerateColumns="False"
                                                    CellPadding="4" BorderColor="blue" BorderStyle="solid" BorderWidth="1" GridLines="None"
                                                    OnEditCommand="dgOnflyGuestRoomlist_Edit" OnDeleteCommand="dgOnflyGuestRoomlist_DeleteCommand"
                                                    OnItemCreated="BindRowsDeleteMessage" Width="100%" AllowSorting="True" Style="border-collapse: separate">
                                                    <FooterStyle CssClass="tableBody" Font-Bold="True" />
                                                    <Columns>
                                                        <asp:BoundColumn DataField="RowUID" Visible="false"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="RoomID" Visible="false"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="RoomName" ItemStyle-CssClass="tableBody" HeaderText="ゲストルームの名前" HeaderStyle-CssClass="tableHeader"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="ContactName" ItemStyle-CssClass="tableBody" HeaderText="充電のゲストルーム" HeaderStyle-CssClass="tableHeader"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="ContactEmail" Visible="false"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="ContactPhoneNo" Visible="false"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="ContactAddress" Visible="false"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="State" Visible="false"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="City" Visible="false"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="ZIP" Visible="false"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="Country" Visible="false"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="IPAddressType" Visible="false"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="IPAddress" Visible="false" ></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="IPPassword" Visible="false"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="IPconfirmPassword" Visible="false"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="IPMaxLineRate" Visible="false"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="IPConnectionType" Visible="false" ></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="IsIPDefault" Visible="false"></asp:BoundColumn>
                                                        
                                                        <asp:BoundColumn DataField="SIPAddressType" Visible="false"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="SIPAddress" Visible="false" ></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="SIPPassword" Visible="false"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="SIPconfirmPassword" Visible="false"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="SIPMaxLineRate" Visible="false"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="SIPConnectionType" Visible="false" ></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="IsSIPDefault" Visible="false"></asp:BoundColumn>
                                                        
                                                        <asp:BoundColumn DataField="ISDNAddressType" Visible="false"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="ISDNAddress" Visible="false" ItemStyle-CssClass="tableBody" HeaderText="アドレス" HeaderStyle-CssClass="tableHeader"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="ISDNPassword" Visible="false"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="ISDNconfirmPassword" Visible="false"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="ISDNMaxLineRate" Visible="false"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="ISDNConnectionType" Visible="false" ItemStyle-CssClass="tableBody" HeaderText="接続タイプ" HeaderStyle-CssClass="tableHeader"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="IsISDNDefault" Visible="false"></asp:BoundColumn>
                                                        
                                                        <asp:BoundColumn DataField="DefaultAddressType" ItemStyle-CssClass="tableBody" HeaderText="アドレスタイプ" HeaderStyle-CssClass="tableHeader"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="DefaultAddress" ItemStyle-CssClass="tableBody" HeaderText="アドレス" HeaderStyle-CssClass="tableHeader"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="DefaultConnetionType" ItemStyle-CssClass="tableBody" HeaderText="接続タイプ" HeaderStyle-CssClass="tableHeader"></asp:BoundColumn>
                                                        <asp:TemplateColumn HeaderText="アクション" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="btnGuestRoomEdit" CommandName="Edit" runat="server" Text="編集"></asp:LinkButton>
                                                                <asp:LinkButton ID="btnGuestRoomDelete" CommandName="Delete" runat="server" Text="削除する"></asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                    </Columns>
                                                    <%--Window Dressing Start--%>
                                                    <SelectedItemStyle CssClass="tableBody" Font-Bold="True" />
                                                    <EditItemStyle CssClass="tableBody" />
                                                    <AlternatingItemStyle CssClass="tableBody" />
                                                    <ItemStyle CssClass="tableBody" />
                                                    <HeaderStyle CssClass="tableBody" Font-Bold="True" />
                                                    <PagerStyle CssClass="tableBody" HorizontalAlign="Center" />
                                                    <%--Window Dressing End--%>
                                                </asp:DataGrid>
                                            </td>
                                        </tr>
                                        <%-- FB 2426 Ends --%>
                           </table>
                        </td>
                    </tr>
                    <tr  id="trp2pLinerate" runat="server" style="display:none;">
                    <td width="100%" align="left" class="blackblodtext">
                            最大ラインレート <span class="reqfldstarText">*</span>
                            <asp:DropDownList CssClass="altSelectFormat" ID="DrpDwnLstRate" runat="server" DataTextField="LineRateName" DataValueField="LineRateID"></asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator21" ControlToValidate="DrpDwnLstRate" InitialValue="-1" ErrorMessage="必須" runat="server" Display="Dynamic"></asp:RequiredFieldValidator>
                                       
                        </td>
                    </tr>
                    <tr id="trRooms" runat="server"><%--FB 2359--%>
                        <td colspan="4" class="subtitlexxsblueblodtext">部屋
                        </td>
                    </tr>
                    <tr id="trRoomsDetails" runat="server"><%--FB 2359--%>
                        <td align="center">
                            <asp:DataGrid runat="server" EnableViewState="true" OnItemDataBound="InitializeLists" ID="dgRooms" AutoGenerateColumns="false"
                              CellSpacing="0" CellPadding="4" GridLines="None" BorderColor="blue" BorderStyle="solid" BorderWidth="1"  Width="98%" style="border-collapse:separate"> <%--Edited for FF--%>
                            <AlternatingItemStyle CssClass="tableBody" HorizontalAlign="left" />
                            <ItemStyle CssClass="tableBody" HorizontalAlign="left" />
                            <HeaderStyle CssClass="tableHeader" HorizontalAlign="left" />
                            <Columns>
                                <asp:BoundColumn DataField="ID" Visible="false"></asp:BoundColumn>
                                <asp:TemplateColumn>
                                    <HeaderStyle CssClass="tableHeader" HorizontalAlign="center" Height="25" />
                                    <HeaderTemplate>
                                        <table width="100%">
                                            <tr>
                                                <td width="25%" class="tableHeader">部屋名</td>
                                                <td width="25%" class="tableHeader">エンドポイント</td>
                                                <td width="25%" class="tableHeader">エンドポイントのプロファイル<span class="reqfldstarText">*</span></td>
                                                 <%-- Code Modified FB 1422- %>
                                               <%-- <td width="25%" class="tableHeader">Select MCU<span class="reqfldstarText">*</span></td>--%>
                                                <td width="25%" class="tableHeader"><asp:Label ID="LblEP" runat="server" Text="選択して MCU" ></asp:Label><span class="reqfldstarText">*</span></td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                    <table width="100%">
                                        <tr>
                                            <td style="width:25%;">
                                                <asp:Label ID="lblRoomName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Name") %>'></asp:Label>
                                            </td>
                                            <%--Window Dressing--%>
                                            <td style="width:25%" class="tableBody">
                                                <asp:Label ID="lblEndpointName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.EndpointName") %>'></asp:Label>
                                                <asp:Label ID="lblEndpointID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.EndpointID") %>' Visible="false" ></asp:Label>
                                                <asp:Label ID="lblDefaultProfileID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ProfileID") %>' Visible="false" ></asp:Label>
                                                <asp:Label ID="lblApiportno" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.APIPortNo") %>' Visible="false" ></asp:Label><%--API Port--%>
                                                <asp:Label ID="lblIsTelepresence" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.isTelePresence") %>' Visible="false" ></asp:Label><%--FB 2400--%>
                                            </td>
                                            <td style="width:25%">
                                                <asp:DropDownList CssClass="altSelectFormat" Enabled="false" ID="lstProfiles" runat="server" DataTextField="ProfileName" DataValueField="ProfileID" OnSelectedIndexChanged="ChangeDefaultBridge" AutoPostBack="true"></asp:DropDownList> <%-- SelectedValue='<%# DataBinder.Eval(Container, "DataItem.DefaultProfileID") %>'--%>                                  
                                                <asp:DropDownList Visible="false" ID="lstProfileType" runat="server" DataTextField="AddressType" DataValueField="ProfileID"></asp:DropDownList> <%-- SelectedValue='<%# DataBinder.Eval(Container, "DataItem.DefaultProfileID") %>'--%>                                  
                                                <asp:DropDownList Visible="false" ID="lstProfileBridge" runat="server" DataTextField="Bridge" DataValueField="ProfileID"></asp:DropDownList> <%-- FB Case 198: Saima --%>                                  
                                                <asp:RequiredFieldValidator ID="reqProfiles" ControlToValidate="lstProfiles" InitialValue="-1" ErrorMessage="必須" runat="server" Display="Dynamic"></asp:RequiredFieldValidator>
                                            </td>
                                            <td style="width:25%">
                                                <asp:DropDownList CssClass="altSelectFormat" Enabled="false" ID="lstBridges" runat="server" DataTextField="BridgeName" DataValueField="BridgeID"></asp:DropDownList>  <%-- SelectedValue='<%# DataBinder.Eval(Container, "DataItem.BridgeID") %>'--%>
                                                 <%--  Code Added for FB 1422 - Start--%>
                                                <asp:DropDownList CssClass="altSelectFormat" Visible="false" Enabled="false" ID="lstTelnet" runat="server">
                                                    <asp:ListItem Value="1" Text="呼び出し"></asp:ListItem>
                                                    <asp:ListItem Value="0" Text="呼び出し先" ></asp:ListItem>                                                
                                                </asp:DropDownList> 
                                                <asp:Label ID="lblRoomTelnet" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Connect2") %>' Visible="false"></asp:Label>
                                                <%--  Code Added for FB 1422 - End--%>     
                                                <asp:Label ID="lblBridgeID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BridgeID") %>' Visible="false"></asp:Label>
                                                <%--<asp:RequiredFieldValidator ID="reqBridges" ControlToValidate="lstBridges" InitialValue="-1" ErrorMessage="必須" runat="server" Display="Dynamic"></asp:RequiredFieldValidator>--%>
                                            </td>
                                        </tr>
                                    </table>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                    </FooterTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="デフォルトを使用します。" ItemStyle-VerticalAlign="Top">
                                <HeaderStyle CssClass="tableHeader" />
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkUseDefault" AutoPostBack="true" OnCheckedChanged="EnableControls" runat="server" Checked='<%# !Request.QueryString["t"].ToString().Trim().Equals("") %>' /> <%-- Saima: fogbugz case 153 --%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                            <SelectedItemStyle BackColor="Beige" />
                         </asp:DataGrid>
                         <%--Window Dressing--%>
                         <asp:Label ID="lblNoRooms" runat="server" Text="部屋が選択されていません" Visible="False" CssClass="lblError"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                         <%--FB 1315--%>
                        <td colspan="4" class="subtitlexxsblueblodtext">外部ユーザー</td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:DataGrid runat="server" OnItemCreated="InitializeUsers" ID="dgUsers" AutoGenerateColumns="false" EnableViewState="true"
                            CellSpacing="0" CellPadding="4" GridLines="None" BorderColor="blue" BorderStyle="solid" BorderWidth="1"  Width="98%" style="border-collapse:separate" OnItemDataBound="BindUsers"> <%--Edited for FF--%>
                            <AlternatingItemStyle CssClass="tableBody" />
                            <ItemStyle CssClass="tableBody" />
                            <Columns>
                                <asp:BoundColumn DataField="ID" Visible="false"></asp:BoundColumn>
                                <asp:TemplateColumn>
                                    <HeaderStyle CssClass="tableHeader" HorizontalAlign="center" Height="25" />
                                    <ItemTemplate>
                                    <table width="100%">
                                        <tr>
                                            <td align="left" colspan="6">
                                                <asp:TextBox ID="lblUserID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ID") %>' Width="0" Height="0" BorderColor="transparent" ForeColor="transparent" />
                                                <asp:Label ID="lblUserName" CssClass="subtitlexxsblueblodtext" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Name") %>'></asp:Label>
                                            </td>
                                            </tr>
                                            <tr>
                                            <td align="right" class="blackblodtext" style="width:15%">アドレス/電話<span class="reqfldstarText">*</span></td>
                                            <td align="left"  style="width:21%">
                                                <asp:TextBox CssClass="altText" ID="txtAddress" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Address") %>'></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="reqAddress" ControlToValidate="txtAddress" ErrorMessage="必須" runat="server" Display="Dynamic"></asp:RequiredFieldValidator>
<%--                                                <asp:RegularExpressionValidator runat="server" ID="regAddress" ControlToValidate="txtAddress" Display="Dynamic" ValidationExpression="^([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}$" ErrorMessage="Invalid IP Address" Enabled='<%# lstAddressType.SelectedValue.Equals("1") %>' ></asp:RegularExpressionValidator>--%>
                                            </td>
                                            <td colspan="4">
                                             <table id="tbMCUandConn" runat="server" width="100%" border="0">
                                              <tr>
                                                  <td align="right" class="blackblodtext"  style="width:17%"><asp:Label ID="LblEPUsers" runat="server" Text="MCU" ></asp:Label><span class="reqfldstarText">*</span></td>                                           
                                                  <td align="left"   style="width:28">
                                                    <asp:DropDownList CssClass="altSelectFormat" ID="lstBridges" runat="server" DataTextField="BridgeName" DataValueField="BridgeID"></asp:DropDownList> <% //FB 1468 %>
                                                     <asp:DropDownList CssClass="altSelectFormat" Visible="false" ID="lstTelnetUsers" runat="server"> 
                                                        <asp:ListItem Value="0"  Text="受信者" ></asp:ListItem> 
                                                        <asp:ListItem Value="1" Text="発信者"></asp:ListItem>
                                                    </asp:DropDownList> 
                                                    <asp:RequiredFieldValidator ID="reqBridge" Enabled="false" ControlToValidate="lstBridges" InitialValue="-1" ErrorMessage="必須" runat="server" Display="Dynamic"></asp:RequiredFieldValidator>
                                                  </td>
                                                  <td align="right" class="blackblodtext"  style="width:20%">
                                                    接続
                                                  </td>
                                                  <td align="left">
                                                    <asp:DropDownList ID="lstConnection" runat="server" CssClass="altSelectFormat" DataTextField="Name" DataValueField="ID" Enabled="false" SelectedValue='<%# DataBinder.Eval(Container, "DataItem.Connection") %>'>
                                                    
                                                    </asp:DropDownList>
                                                  </td>
                                              </tr>
                                             </table>
                                            </td>
                                        </tr>
                                        <tr id="AudioParams1" runat="server">
                                        <td align="right" class="blackblodtext">ラインレート<span class="reqfldstarText">*</span></td>
                                            <td align="left">
                                                <asp:DropDownList CssClass="altSelectFormat" ID="lstLineRate" runat="server" DataTextField="LineRateName" DataValueField="LineRateID" SelectedValue='<%# DataBinder.Eval(Container, "DataItem.Bandwidth") %>'></asp:DropDownList>  <%--SelectedValue='<%# DataBinder.Eval(Container, "DataItem.BridgeID") %>'--%>
                                                <asp:RequiredFieldValidator ID="reqLineRate" ControlToValidate="lstLineRate" InitialValue="-1" ErrorMessage="必須" runat="server" Display="Dynamic"></asp:RequiredFieldValidator><%--FB 2444--%>
                                            </td>
                                            
                                            <td align="right" class="blackblodtext"  style="width:10%">プロトコル<span class="reqfldstarText">*</span></td>
                                            <td align="left">
                                                <asp:DropDownList ID="lstProtocol" runat="server" CssClass="altSelectFormat" DataTextField="Name" DataValueField="ID" SelectedValue='<%# DataBinder.Eval(Container, "DataItem.DefaultProtocol") %>'></asp:DropDownList>
                                            </td>
                                            <td align="right" class="blackblodtext">アドレスタイプ<span class="reqfldstarText">*</span></td>
                                            <td align="left">
                                                <asp:DropDownList CssClass="altSelectFormat" ID="lstAddressType" runat="server" SelectedValue='<%# DataBinder.Eval(Container, "DataItem.AddressType") %>' DataTextField="Name" DataValueField="ID"></asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="reqAddressType" ControlToValidate="lstAddressType" InitialValue="-1" ErrorMessage="必須" runat="server" Display="Dynamic"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr id="AudioParams2" runat="server">
                                            <td align="right" class="blackblodtext">外部N/W</td>
                                            <td align="left">
                                                <asp:CheckBox ID="chkIsOutside" EnableViewState="true" runat="server" Checked='<%# (DataBinder.Eval(Container, "DataItem.IsOutside").ToString().Equals("1")) %>'></asp:CheckBox>  <%--SelectedValue='<%# DataBinder.Eval(Container, "DataItem.BridgeID") %>'--%>
                                                <asp:Label ID="Label1" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container, "DataItem.IsOutside") %>'></asp:Label>
                                            </td>
                                            <td align="right" class="blackblodtext">機器の</td>
                                            <td align="left">
                                                <asp:DropDownList CssClass="altSelectFormat" ID="lstVideoEquipment" runat="server" DataTextField="VideoEquipmentName" DataValueField="VideoEquipmentID" SelectedValue='<%# DataBinder.Eval(Container, "DataItem.VideoEquipment") %>'></asp:DropDownList>  <%--SelectedValue='<%# DataBinder.Eval(Container, "DataItem.BridgeID") %>'--%>
                                                <asp:RequiredFieldValidator ID="reqEquipment" ControlToValidate="lstVideoEquipment" InitialValue="0" ErrorMessage="必須" runat="server" Display="Dynamic"></asp:RequiredFieldValidator>
                                            </td>
                                            <td align="right" class="blackblodtext">接続タイプ<span class="reqfldstarText">*</span></td>
                                            <td align="left">
                                                <asp:DropDownList ID="lstConnectionType" CssClass="altSelectFormat" runat="server" DataTextField="Name" DataValueField="ID" SelectedValue='<%# DataBinder.Eval(Container, "DataItem.ConnectionType") %>'></asp:DropDownList>  <%--Fogbugz case 427--%>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" is="reqConnectionType" runat="server" ControlToValidate="lstConnectionType" Display="dynamic" InitialValue="-1" SetFocusOnError="true" ErrorMessage="必須"></asp:RequiredFieldValidator>
                                            </td>
                                            
                                        </tr>
                                        <tr id="AudioParams3" runat="server">
                                            <td align="right" class="blackblodtext">URL</td>
                                            <td align="left">
                                                 <%--Window Dressing--%>                                                
                                                <asp:TextBox ID="txtEndpointURL" CssClass="altText" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.URL") %>'></asp:TextBox>
                                            </td>
                                            <td align="right" class="blackblodtext">電子メールID</td> <%-- ICAL Cisco Telepresence fix--%>
                                        <td align="left">
                                            <asp:TextBox CssClass="altText"  ID="txtExchangeID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ExchangeID") %>' TextMode="SingleLine"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" ControlToValidate="txtExchangeID" ValidationGroup="Submit" Display="dynamic" runat="server"  SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ ( ) ; ? | ^ = ! ` , [ ] { } # $ @ ~ &#34; は無効な記号です" ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>+;?|!`,\[\]{}\x22;=^#$%&()'~]*$"></asp:RegularExpressionValidator>
                                        </td>
                                        <%--API Port Starts--%>
                                        <td align="right" class="blackblodtext">
                                        APIモジュール
                                        </td>
                                        <td align="left" nowrap>
                                        <asp:TextBox CssClass="altText"  ID="txtApiportno" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.APIPortNo") %>' MaxLength="5" TextMode="SingleLine"></asp:TextBox>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator16" ControlToValidate="txtApiportno" ValidationGroup="Submit" Display="dynamic" runat="server"  SetFocusOnError="true" ErrorMessage="の数値のみ" ValidationExpression="\d+"></asp:RegularExpressionValidator>
                                        </td>
                                        
                                        </tr>
                                        <%--Audio addon--%>
                                        <tr  id="trAudioaddonett" >
                                       
                                                 <td align="right" class="blackblodtext"><asp:Label ID="LblConfCode" CssClass="blackblodtext" runat="server" Text="コンピューターコード"></asp:Label></td>
                                       <td align="left">
                                                                                                
                                                <asp:TextBox ID="txtConfCode" CssClass="altText" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ConfCode") %>'></asp:TextBox>
                                                
                                            </td>
                                           <td align="right" class="blackblodtext"  style="width:11%"><asp:Label ID="LblLeaderpin" CssClass="blackblodtext" runat="server" Text="リーダーピン"></asp:Label></td> 
                                           <td align="left" style="width:20%">
                                                                                                
                                                <asp:TextBox ID="txtleaderPin" CssClass="altText" runat="server"  Text='<%# DataBinder.Eval(Container, "DataItem.LPin") %>'></asp:TextBox>
                                                                                            </td>
                                                                                            <td colspan="2"></td>
                                        </tr>
                                        <%--Audio addon--%>
                                        
                                    </table>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                    </FooterTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="デフォルトを使用します。" ItemStyle-VerticalAlign="Top">
                                <HeaderStyle CssClass="tableHeader" />
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkUseDefault" runat="server" Checked="false" onclick="javascript:CheckDefault(this)" />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                            <SelectedItemStyle BackColor="Beige" />
                         </asp:DataGrid>
                        <%--Code added for FB 1315--%>
                         <asp:Label ID="lblNoUsers" runat="server" Text="ない外部ユーザーが選択されていません" Visible="False" CssClass="lblError"></asp:Label>                        
                        </td>
                    </tr>
                </table>
                <asp:TextBox ID="hasVisited" runat="server" Text="" Visible="false"></asp:TextBox>
                <asp:TextBox ID="ImageFiles" runat="server" Text=""></asp:TextBox>
                <asp:TextBox ID="ImageFilesBT" runat="server" Text=""></asp:TextBox>
                <asp:TextBox ID="ImagesPath" runat="server" Text=""></asp:TextBox>
                <asp:TextBox ID="txtSelectedImage" runat="server" Text="01"></asp:TextBox><%--FB 2524--%>
                <script language="javascript" type="text/javascript">
                    CheckPolycom(document.getElementById("<%=chkPolycomSpecific.ClientID%>"));
                </script>
                    </asp:Panel>
                </asp:View>
                <asp:View ID="SelectAV" runat="server" OnLoad="SetInstructionsAV">
                    <asp:Panel ID="pnlAV" runat="server" Width="100%">
                <center>
                    <h3>オーディオ/ビジュアルワークオーダー</h3>
                </center>
                <input type="hidden" id="Hidden3" value="14">
                <input type="hidden" id="lblTab" value="1" />
                    <table width="100%">
                    <tr>
                        <td width="100%" align="left" style="height: 18px">
                            <asp:Label ID="lblAVWOInstructions" runat="server" Width="90%" CssClass="blackblodtext"></asp:Label>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;<table width="100%">
                                <tr>
                                    <td colspan="3">
                                    
                            <asp:DataGrid ID="AVMainGrid" runat="server" AutoGenerateColumns="False" CellPadding="4" BorderColor="blue" BorderStyle="solid" BorderWidth="1"
                            GridLines="None" OnCancelCommand="AVMainGrid_Cancel" OnUpdateCommand="AVMainGrid_Update" OnEditCommand="AVMainGrid_Edit" OnDeleteCommand="AVMainGrid_DeleteCommand"
                            OnItemCreated="BindRowsDeleteMessage" Width="100%" AllowSorting="True" style="border-collapse:separate"> <%--Edited for FF--%>
                            <%--Window Dressing--%>
                            <FooterStyle CssClass="tableBody" Font-Bold="True" />
                            <Columns>
                                <asp:BoundColumn DataField="ID" Visible="False"></asp:BoundColumn>
                                <asp:BoundColumn DataField="Name" ItemStyle-CssClass="tableBody" HeaderText="名前" HeaderStyle-CssClass="tableHeader"></asp:BoundColumn>
                                <asp:BoundColumn DataField="AssignedToID" Visible="False"></asp:BoundColumn>
                                <asp:BoundColumn DataField="AssignedToName" ItemStyle-CssClass="tableBody" HeaderText="に割り当て" HeaderStyle-CssClass="tableHeader"></asp:BoundColumn>
                                <asp:BoundColumn DataField="StartByDate" ItemStyle-CssClass="tableBody" HeaderText="日<br>付で開始" HeaderStyle-CssClass="tableHeader"></asp:BoundColumn>
                                <asp:BoundColumn DataField="StartByTime" ItemStyle-CssClass="tableBody" HeaderText="時間で開始" HeaderStyle-CssClass="tableHeader"></asp:BoundColumn>
                                <asp:BoundColumn DataField="CompletedByDate" ItemStyle-CssClass="tableBody" HeaderText="日<br>完全" HeaderStyle-CssClass="tableHeader"></asp:BoundColumn>
                                <asp:BoundColumn DataField="CompletedByTime" ItemStyle-CssClass="tableBody" HeaderText="の時間<br>完全" HeaderStyle-CssClass="tableHeader"></asp:BoundColumn>
                                <asp:BoundColumn DataField="Status" ItemStyle-CssClass="tableBody" HeaderText="状況" HeaderStyle-CssClass="tableHeader"></asp:BoundColumn>
                                <asp:BoundColumn DataField="RoomID" Visible="False"></asp:BoundColumn>
                                <asp:BoundColumn DataField="RoomName" ItemStyle-CssClass="tableBody" HeaderText="部屋" HeaderStyle-CssClass="tableHeader"></asp:BoundColumn>
                                <asp:BoundColumn DataField="Comments" ItemStyle-CssClass="tableBody" HeaderText="コメント" HeaderStyle-CssClass="tableHeader"></asp:BoundColumn>
                                <asp:BoundColumn DataField="RoomLayout" Visible="False"></asp:BoundColumn>
                                <asp:BoundColumn DataField="ServiceCharge" ItemStyle-CssClass="tableBody" HeaderText="サービス料金" HeaderStyle-CssClass="tableHeader"></asp:BoundColumn>
                                <asp:BoundColumn DataField="DeliveryCost" ItemStyle-CssClass="tableBody" HeaderText="コスト<br>配信" HeaderStyle-CssClass="tableHeader"></asp:BoundColumn>
                                <asp:BoundColumn DataField="TotalCost" ItemStyle-CssClass="tableBody" HeaderText="トータルコスト" HeaderStyle-CssClass="tableHeader"></asp:BoundColumn>
                                <asp:TemplateColumn HeaderText="アクション" ItemStyle-CssClass="tableBody"  HeaderStyle-CssClass="tableHeader">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="btnEdit" CommandName="Edit" runat="server" Text="編集"  Visible='<%# DataBinder.Eval(Container, "DataItem.Status") != "完了" %>'></asp:LinkButton>
                                        <asp:LinkButton ID="btnDelete" CommandName="Delete" runat="server" Text="削除" ></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:BoundColumn DataField="SetID" ItemStyle-CssClass="tableHeader" Visible="false"></asp:BoundColumn>
                                <asp:BoundColumn DataField="ReqQuantity" ItemStyle-CssClass="tableHeader" Visible="false" HeaderStyle-CssClass="tableHeader"></asp:BoundColumn>
                                <asp:BoundColumn DataField="Notify" ItemStyle-CssClass="tableHeader" Visible="false" HeaderStyle-CssClass="tableHeader"></asp:BoundColumn>
                                <asp:BoundColumn DataField="DeliveryType" ItemStyle-CssClass="tableHeader" HeaderText="の種類<br>配信" Visible="false" HeaderStyle-CssClass="tableHeader"></asp:BoundColumn>
                                <asp:BoundColumn DataField="Timezone" ItemStyle-CssClass="tableHeader" Visible="false"></asp:BoundColumn>
                                </Columns>
                            <%--Window Dressing Start--%>
                            <SelectedItemStyle CssClass="tableBody"  Font-Bold="True"/>
                            <EditItemStyle CssClass="tableBody"   />
                            <AlternatingItemStyle CssClass="tableBody" />
                            <ItemStyle CssClass="tableBody" />
                            <HeaderStyle CssClass="tableBody"  Font-Bold="True" />
                            <PagerStyle CssClass="tableBody" HorizontalAlign="Center" />
                            <%--Window Dressing End--%>
                                        </asp:DataGrid>
                                                </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <asp:Table runat="server" ID="AVItemsTable" Visible="False" Width="100%" CellPadding="0" CellSpacing="0">
                                                                           <asp:TableHeaderRow Height="20">
                                    <asp:TableHeaderCell CssClass="subtitleblueblodtext"><asp:Label ID="lblNewEditAV" runat="server"></asp:Label></asp:TableHeaderCell>
                                            </asp:TableHeaderRow>
                        <asp:TableRow ID="TableRow2" runat="server" HorizontalAlign="Center" VerticalAlign="Middle">
                            <asp:TableCell ID="TableCell1" runat="server">
                 <table border="0"  cellpadding="5" cellspacing="0" width="70%" >
                        <tr>
                            <td align="left" class="blackblodtext" width="15%">部屋</td>
                            <td style="font-weight: bold; " align="left"  width="25%">
                                <asp:DropDownList ID="lstRooms" runat="server" AutoPostBack="True" CssClass="altSelectFormat"
                                    OnSelectedIndexChanged="selRooms_SelectedIndexChanged">
                                </asp:DropDownList></td>
                            <td align="left" class="blackblodtext" width="15%">名前を設定します。</td>
                            <td align="left" width="25%">
                                <asp:DropDownList ID="lstAVSet" runat="server" AutoPostBack="True" CssClass="altSelectFormat"
                                    OnSelectedIndexChanged="selAVSet_SelectedIndexChanged">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="req2" ControlToValidate="lstAVSet" ErrorMessage="必須" runat="server" InitialValue="-1"></asp:RequiredFieldValidator>
                                </td>
                        </tr>
                        <tr>
                            <td align="left" class="blackblodtext">名前</td>
                            <td align="left" >
                                <asp:TextBox ID="txtWorkOrderName" runat="server" CssClass="altText"></asp:TextBox><asp:TextBox ID="txtWorkOrderID" runat="server" CssClass="altText" Visible="False"></asp:TextBox><asp:RequiredFieldValidator ID="ValidateWOName" runat="server" ControlToValidate="txtWorkOrderName" Display="Dynamic" ErrorMessage="必須" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="regWOName" ControlToValidate="txtWorkOrderName" Display="dynamic" runat="server"  SetFocusOnError="true" ErrorMessage="<br>& < と > は無効な記号です." ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator> <%--FB 2321--%>
                            </td>
                            <td align="left" class="blackblodtext">人の電荷</td>
                            <td align="left">
                                <asp:TextBox ID="txtApprover1" runat="server" CssClass="altText" Enabled="true"></asp:TextBox>
                                &nbsp;<img id="ImageButton1" onclick="javascript:getYourOwnEmailList(0)" src="image/edit.gif" /><asp:TextBox
                                    ID="hdnApprover1" runat="server" BackColor="Transparent" BorderColor="White"
                                    BorderStyle="None" Width="0px" ForeColor="Black"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reqFieldApproverAV" runat="server" ControlToValidate="txtApprover1"
                                    Display="Dynamic" ErrorMessage="必須" SetFocusOnError="True"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="blackblodtext">日付で開始</td>
                            <td style="font-weight: bold; " align="left" >
                                <asp:TextBox ID="txtStartByDate" runat="server" CssClass="altText"></asp:TextBox>
                                <!--//Code changed by Offshore for FB Issue 1073 -- Start
                                <img id="Img3" height="20px" onclick="javascript:showCalendar('<%=txtStartByDate.ClientID %>', 'Img3', 1, '<%=format%>');"-->
                                <img id="Img3" height="20px" onclick="javascript:showCalendar('<%=txtStartByDate.ClientID %>', 'Img3', 1, '<%=format%>');"
                                src="image/calendar.gif" width="20px" />
                                <!--//Code changed by Offshore for FB Issue 1073 -- end-->
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtStartByDate"
                                    Display="Dynamic" ErrorMessage="必須" SetFocusOnError="True"></asp:RequiredFieldValidator>
                            </td>
                            <td align="left" class="blackblodtext">時間で開始</td>
                            <td align="left" >
                            <%--Window Dressing--%>
                                 <mbcbb:ComboBox ID="startByTime" CssClass="altSelectFormat" runat="server"  CausesValidation="True"
                                    Rows="10" style="width:auto"> <%--Edited for FF--%>
                                    <asp:ListItem Selected="True" Value="01:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="02:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="03:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="04:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="05:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="06:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="07:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="08:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="09:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="10:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="11:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="12:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="01:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="02:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="03:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="04:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="05:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="06:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="07:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="08:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="09:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="10:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="11:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="12:00 AM"></asp:ListItem>
                                </mbcbb:ComboBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="startByTime"
                                    Display="Dynamic" ErrorMessage="必須"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator14" runat="server" ControlToValidate="startByTime"
                                    Display="Dynamic" ErrorMessage="無効な時間 (HH:MM AM/PM)" ValidationExpression="[0|1][0-9]:[0-5][0-9] [a|A|p|P][M|m]"></asp:RegularExpressionValidator> <%-- FB Case 371 Saima --%>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="blackblodtext">日までに完了</td>
                            <td align="left" >
                                <asp:TextBox ID="txtCompletedBy" runat="server" CssClass="altText"></asp:TextBox>
                                <!--//Code changed by Offshore for FB Issue 1073 -- Start
                                <img id="cal_trigger" height="20px" onclick="javascript:showCalendar('<%=txtCompletedBy.ClientID %>', 'cal_trigger', 1, '%m/%d/%Y');"-->
                                <img id="cal_trigger" height="20px" onclick="javascript:showCalendar('<%=txtCompletedBy.ClientID %>', 'cal_trigger', 1, '<%=format%>');"
                                 src="image/calendar.gif" width="20px" />
                                 <!--//Code changed by Offshore for FB Issue 1073 -- end-->
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="txtCompletedBy"
                                    Display="Dynamic" ErrorMessage="必須" SetFocusOnError="True"></asp:RequiredFieldValidator>
                            </td>
                            <td align="left" class="blackblodtext">の時間までに完了</td>
                            <td align="left" >
                            <%--Window Dressing--%>
                                 <mbcbb:ComboBox ID="completedByTime" CssClass="altSelectFormat" runat="server" CausesValidation="True"
                                    Rows="10" style="width:auto"> <%--Edited for FF--%>
                                    <asp:ListItem Selected="True" Value="01:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="02:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="03:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="04:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="05:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="06:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="07:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="08:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="09:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="10:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="11:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="12:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="01:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="02:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="03:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="04:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="05:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="06:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="07:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="08:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="09:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="10:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="11:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="12:00 AM"></asp:ListItem>
                                </mbcbb:ComboBox>
                                <asp:RequiredFieldValidator ID="reqTime" runat="server" ControlToValidate="completedByTime"
                                    Display="Dynamic" ErrorMessage="必須"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="regTime" runat="server" ControlToValidate="completedByTime"
                                    Display="Dynamic" ErrorMessage="無効な時間 (HH:MM AM/PM)" ValidationExpression="[0|1][0-9]:[0-5][0-9] [a|A|p|P][M|m]"></asp:RegularExpressionValidator> <%-- FB Case 371 Saima --%> 
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="blackblodtext">ホストに通知する</td>
                            <td align="left" >
                                <asp:CheckBox ID="chkNotify" runat="server" /></td>
                            <td align="left" class="blackblodtext">タイムゾーン</td>
                            <td align="left" >
                                <asp:DropDownList CssClass="altSelectFormat" ID="lstTimezones" OnInit="GetTimezones" DataTextField="timezoneName" DataValueField="timezoneID" runat="server"></asp:DropDownList>
                        </tr>
                        <tr>
                            <td align="left" class="blackblodtext">状況</td>
                            <td align="left">
                                <asp:DropDownList ID="lstStatus" runat="server" CssClass="altSelectFormat">
                                    <asp:ListItem Text="承認待ち" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="完了" Value="1"></asp:ListItem>
                                </asp:DropDownList></td>
                            <td align="left" class="blackblodtext">コメント</td>
                            <td align="left" >
                                <asp:TextBox ID="txtComments" runat="server" CssClass="altText" Rows="2" TextMode="MultiLine"></asp:TextBox>
                                <asp:RegularExpressionValidator ValidationGroup="Submit" ID="RegularExpressionValidator15" ControlToValidate="txtComments" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ &#34; は無効な記号です" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:RegularExpressionValidator><%--FB 2011--%>
                            </td>
                        </tr>
                        <tr id="trDelivery" runat="server">
                            <td align="left" class="blackblodtext">デリバリータイプ</td>
                            <td align="left" ><%-- FB 1830 --%>
                                <asp:DropDownList cssclass="altSelectFormat" ID="lstDeliveryType" runat="server" DataTextField="Name" DataValueField="ID" OnInit="LoadDeliveryTypes" OnSelectedIndexChanged="ChangeDeliveryType" AutoPostBack="true" onclick="javascript:DataLoading(1)"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator9" ControlToValidate="lstDeliveryType" runat="server" ValidationGroup="Submit"  ErrorMessage="必須" InitialValue="-1" Display="dynamic"></asp:RequiredFieldValidator>
                            </td>
                            <td align="left" class="blackblodtext">デリバリーコスト (<%=cFormat%>)</td> <%--FB 1830--%>
                            <td align="left">
                                <asp:TextBox ID="txtDeliveryCost" runat="server" CssClass="altText"></asp:TextBox>
                                <asp:CustomValidator ID="cusDCost" runat="server" ControlToValidate="txtDeliveryCost"  SetFocusOnError="True" ValidationGroup="Submit" Display="dynamic" ErrorMessage=" 無効量" ClientValidationFunction="ClientValidate"></asp:CustomValidator>
                                <%--<asp:RegularExpressionValidator ID="regDC" runat="server" ErrorMessage="Numerics only." SetFocusOnError="True" ToolTip="Numerics only." ControlToValidate="txtDeliveryCost" ValidationExpression="^\d+(?:\.\d{0,2})?$" Display="Dynamic"></asp:RegularExpressionValidator>--%>
                            </td>
                        </tr>
                        <tr id="trService" runat="server">
                            <td align="left" class="blackblodtext">サービスチャージ (<%=cFormat%>)</td> <%--FB 1830--%>
                            <td align="left" >
                                <asp:TextBox ID="txtServiceCharges" runat="server" CssClass="altText"></asp:TextBox>
                                <asp:CustomValidator ID="CustomValidator1" runat="server" ControlToValidate="txtServiceCharges"  SetFocusOnError="True" ValidationGroup="Submit" Display="dynamic" ErrorMessage=" 無効量" ClientValidationFunction="ClientValidate"></asp:CustomValidator>
                                <%--<asp:RegularExpressionValidator ID="regSC" runat="server" ErrorMessage="Numerics only." SetFocusOnError="True" ToolTip="Numerics only." ControlToValidate="txtServiceCharges" ValidationExpression="^\d+(?:\.\d{0,2})?$" Display="Dynamic"></asp:RegularExpressionValidator>--%>
                            </td>
                            <td align="right" class="blackblodtext">&nbsp;</td>
                            <td align="left" >&nbsp;</td>
                        </tr>
                    </table>
                    </asp:TableCell>
                                            </asp:TableRow>
                       <asp:TableRow ID="TableRow3" HorizontalAlign="Center" VerticalAlign="Middle" runat="server"><asp:TableCell ID="TableCell2" runat="server">
                       <table width="100%">
                      <tr>
                          <td colspan="2" style="font-weight: bold; " align="center">
                                <asp:DataGrid ID="itemsGrid" runat="server" AutoGenerateColumns="False" ShowFooter="true"
                                    CellPadding="4" Font-Bold="False" Font-Names="arial" Font-Size="Small" OnItemDataBound="SetDeliveryAttributes"
                                    GridLines="None" Width="100%" BorderColor="blue" BorderStyle="Solid" BorderWidth="1" style="border-collapse:separate"> <%--Edited for FF--%>
                                    <FooterStyle Font-Bold="True" ForeColor="Blue" />
                                    <AlternatingItemStyle CssClass="tableBody" />
                                    <ItemStyle CssClass="tableBody" />
                                    <HeaderStyle CssClass="tableHeader" />
                                    <Columns>
                                    <%--Window Dressing--%>
                                        <asp:BoundColumn DataField="ID" Visible="False"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>
                                        <asp:BoundColumn DataField="Name" ItemStyle-CssClass="tableBody" HeaderText="名前"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>
                                        <asp:BoundColumn DataField="SerialNumber" ItemStyle-CssClass="tableBody" HeaderText="シリアル番号"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>
                                        <asp:TemplateColumn HeaderText="Image">
                                        <HeaderStyle CssClass="tableHeader" />
                                        <ItemStyle CssClass="tableBody" />
                                            <ItemTemplate>
                                               <asp:Image runat="server" ID="imgItem"  Height="30" Width="30" onmouseover="javascript:ShowImage(this)" onmouseout="javascript:HideImage()" />
                                            </ItemTemplate>
                                        </asp:TemplateColumn> 
                                        <asp:BoundColumn DataField="Comments" ItemStyle-CssClass="tableBody" HeaderText="コメント"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>
                                        <asp:BoundColumn DataField="Description" ItemStyle-CssClass="tableBody" HeaderText="説明"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>
                                        <asp:BoundColumn DataField="Quantity" ItemStyle-CssClass="tableBody" HeaderText="手の量"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>
                                        <asp:TemplateColumn HeaderText="の種類<br>配信" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody" >
                                            <ItemTemplate>
<%--                                                <asp:Label ID="lblDeliveryType" Text='<%# DataBinder.Eval(Container, "DataItem.DeliveryType") %>' runat="server"></asp:Label>--%>
                                                  <asp:DropDownList AutoPostBack="true" CssClass="altText" OnSelectedIndexChanged="ChangeDeliveryTypeItem" runat="server" ID="lstDeliveryTypeItem" Enabled="false" DataTextField="Name" DataValueField="ID" OnInit="LoadDeliveryTypes" SelectedText='<%# DataBinder.Eval(Container, "DataItem.DeliveryType") %>' ></asp:DropDownList><%----%>
                                             </ItemTemplate>
                                         </asp:TemplateColumn>
                                         <asp:TemplateColumn HeaderText="コスト<br>配信" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader">
                                            <ItemTemplate>
                                                <asp:Label ID="lblDeliveryCost" Text='<%# DataBinder.Eval(Container, "DataItem.DeliveryCost") %>' runat="server"/>
                                                <asp:DropDownList ID="lstDeliveryCost" runat="server" Visible="false" Enabled="true" DataValueField="DeliveryTypeID" DataTextField="DeliveryCost" /> <%--SelecteValue='<%# DataBinder.Eval(Container, "DataItem.DeliveryCost") %>'--%>
                                            </ItemTemplate>
                                         </asp:TemplateColumn>
                                         <asp:TemplateColumn HeaderText="サービス料金" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader">
                                            <ItemTemplate>
                                                <asp:Label ID="lblServiceCharge" Text='<%# DataBinder.Eval(Container, "DataItem.ServiceCharge") %>' runat="server"/>
                                                <asp:DropDownList AutoPostBack="true" runat="server" Visible="false" ID="lstServiceCharge" Enabled="true" DataTextField="ServiceCharge" DataValueField="DeliveryTypeID"></asp:DropDownList><%--SelecteValue='<%# DataBinder.Eval(Container, "DataItem.ServiceCharge") %>'--%>
                                            </ItemTemplate>
                                         </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody"  HeaderText="価格">
                                            <ItemTemplate>
                                                <asp:Label ID="lblPrice" Text='<%# DataBinder.Eval(Container, "DataItem.Price") %>' runat="server"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="発注"><HeaderStyle CssClass="tableHeader" />
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtReqQuantity" CssClass="altText" runat="server" Width="30px" Text='<%# DataBinder.Eval(Container, "DataItem.QuantityRequested") %>'></asp:TextBox>
                                                <asp:RegularExpressionValidator ID="numPassword1" runat="server" ValidationGroup="Submit" ErrorMessage="数値のみ" SetFocusOnError="True" ToolTip="Numerics only." ControlToValidate="txtReqQuantity" ValidationExpression="\d+" Display="Dynamic"></asp:RegularExpressionValidator>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtReqQuantity" ValidationGroup="Submit" ErrorMessage="必須" Display="Dynamic"></asp:RequiredFieldValidator>
                                                <asp:RangeValidator ID="validateQuantityRange" runat="server" ValidationGroup="Submit" ErrorMessage="無効量" ControlToValidate="txtReqQuantity" MinimumValue="0" Display="Dynamic" Type="Integer" MaximumValue='<%# DataBinder.Eval(Container, "DataItem.Quantity") %>'></asp:RangeValidator>
                                            </ItemTemplate>
                                            <FooterStyle Font-Bold="True" ForeColor="Blue" HorizontalAlign="right" />
                                            <FooterTemplate>
                                                <span class="blackblodtext">総 (<%=cFormat%>): </span> <asp:Label runat="server" ID="lblTotalQuantity" Text="0" Width="50" OnLoad="UpdateTotalAV"></asp:Label>&nbsp;
                                                <br /><asp:Button ID="btnUpdateTotal" OnClick="UpdateTotalAV" runat="server" ValidationGroup="Submit" CssClass="altShortBlueButtonFormat" Text="アップデート" />
                                            </FooterTemplate>
                                        </asp:TemplateColumn>
                            <asp:BoundColumn DataField="UID" Visible="false" HeaderText="UID"></asp:BoundColumn>
                                    </Columns>
                           <%--Window Dressing--%>
                                    <PagerStyle CssClass="tableBody" HorizontalAlign="Center" />
                                </asp:DataGrid>
                          </td>
                      </tr>
 
                      <tr>
                          <td align="right">
                             <asp:Button ID="btnCancel" runat="server" CssClass="altShortBlueButtonFormat" ValidationGroup="Cancel" OnClientClick="javascript:DataLoading(1)" OnClick="btnCancel_Click" Text="キャンセル" /></td>
                          <td align="left">
                             <asp:Button ID="btnSubmit" runat="server" CssClass="altShortBlueButtonFormat" ValidationGroup="Submit" OnClientClick="javascript:DataLoading(1)" OnClick="btnSubmit_Click" Text="送信" /></td>
                      </tr>
                    </table>          
                            </asp:TableCell>
                                            </asp:TableRow>

                                        </asp:Table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" style="font-weight:bold"colspan="3">
                             <asp:Button ID="btnAddNewAV" runat="server" CssClass="altLongBlueButtonFormat" Text="新規A/Vワークオーダーを追加する" OnClientClick="javascript:DataLoading(1)" OnClick="A_btnAddNew_Click" />
                                    </td>
                                </tr>
                            </table>

                        </td>
                    </tr>
                 </table>
                <script language="javascript">
                    if (document.getElementById("txtApprover1") != null)
                    {
                        document.getElementById("txtApprover1").Readonly = true;
//                        alert("in if");
                    }
                </script> 
                                    </asp:Panel>
                                </asp:View>
                                <asp:View ID="SelectCatering" runat="server" OnLoad="SetInstructionsCAT">
                                    <asp:Panel ID="pnlCatering" runat="server" Width="100%">
                    <h3>ケータリングワークオーダー</h3>
                    <input type="hidden" id="Hidden4" value="15">
                <input type="hidden" id="lblTab" value="2" />

                    <table width="100%">
                    <tr>
                        <td width="100%" align="left">
                            <asp:Label ID="lblCATWOInstructions" CssClass="blackblodtext" runat="server" Width="90%"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;<table width="100%">
                                <tr>
                                    <td>
                                         <%--Window Dressing start--%>
                                        <asp:DataGrid ID="CATMainGrid" runat="server" AutoGenerateColumns="False" BorderColor="blue" BorderStyle="solid" BorderWidth="1" CellPadding="4" OnItemCreated="BindRowsDeleteMessage" GridLines="None" OnItemDataBound="SetCalendar" 
                                        OnCancelCommand="CancelChanges" OnUpdateCommand="UpdateWorkorder" OnEditCommand="EditWorkorder" OnDeleteCommand="DeleteWorkorder" Width="100%" style="border-collapse:separate"> <%--Edited for FF--%>
                                            <FooterStyle Font-Bold="True" CssClass="tableBody"/>
                                            <AlternatingItemStyle CssClass="tableBody" VerticalAlign="top" HorizontalAlign="left" />
                                            <ItemStyle CssClass="tableBody" VerticalAlign="top" HorizontalAlign="left" />
                                            <HeaderStyle CssClass="tableHeader" />
                                             <%--Window Dressing end--%>
                                             <EditItemStyle CssClass="tableBody" />
                                            <Columns>
                                                <asp:BoundColumn DataField="ID" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="Name" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="RoomID" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="SelectedService" Visible="false"></asp:BoundColumn>
                                             <%--Window Dressing --%>
                                                <asp:TemplateColumn HeaderText="部屋" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblID" Text='<%# DataBinder.Eval(Container, "DataItem.ID") %>' Visible="false"></asp:Label>
                                                        <asp:Label ID="lblRoomName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.RoomName") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:Label runat="server" ID="lblID" Text='<%# DataBinder.Eval(Container, "DataItem.ID") %>' Visible="false"></asp:Label>
                                                        <asp:DropDownList ID="lstRooms" CssClass="altText" runat="server" selectedValue='<%# DataBinder.Eval(Container, "DataItem.RoomId") %>' OnInit="LoadRooms" AutoPostBack="true" onchange="javascript:DataLoading(1)" OnSelectedIndexChanged="UpdateMenus" ></asp:DropDownList>
                                                    </EditItemTemplate>
                                                </asp:TemplateColumn>
                                             <%--Window Dressing --%>
                                                <asp:TemplateColumn HeaderText="サービスの種類" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblServiceName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ServiceName") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:DropDownList ID="lstServices" CssClass="altText" runat="server" DataTextField="Name" DataValueField="ID" OnLoad="LoadCateringServices" OnInit="LoadCateringServices" AutoPostBack="true" onchange="javascript:DataLoading(1)" OnSelectedIndexChanged="UpdateMenus" selectedValue='<%# DataBinder.Eval(Container, "DataItem.SelectedService") %>'></asp:DropDownList>  <%--OnLoad="GetServices"--%>
                                                    </EditItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn ItemStyle-CssClass="tableBody">
                                                    <HeaderTemplate>
                                                        <table width="100%" cellpadding="4" cellspacing="0" border="0">
                                                            <tr>
                                                                <td width="60%" class="tableHeader">名前</td>
                                                                <td class="tableHeader" width="40%">量</td>
                                                            </tr>
                                                        </table>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:DataGrid ID="dgCateringMenus" ShowHeader="false" Width="100%" CellPadding="4" CellSpacing="0" BorderWidth="1px" BorderStyle="Solid" runat="server" AutoGenerateColumns="false">
                                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="top" />
                                                            <AlternatingItemStyle HorizontalAlign="left" VerticalAlign="Top" />
                                                            <Columns>
                                                                <asp:BoundColumn DataField="ID" Visible="false"></asp:BoundColumn>
                                                                <asp:BoundColumn ItemStyle-CssClass="tableBody" DataField="Name" ItemStyle-Width="60%"></asp:BoundColumn>
                                                                <asp:BoundColumn ItemStyle-CssClass="tableBody" DataField="Quantity" ItemStyle-Width="40%"></asp:BoundColumn>
                                                            </Columns>
                                                        </asp:DataGrid>
                                                        <asp:Label ID="lblCateringMenus" visible="false" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.strMenus") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:DataGrid ID="dgCateringMenus" ShowHeader="false" Width="100%" CellPadding="4" CellSpacing="0" BorderWidth="0px" BorderStyle="none" runat="server" AutoGenerateColumns="false" style="border-collapse:separate"> <%--Edited for FF--%>
                                                            <Columns>
                                                                <asp:BoundColumn DataField="ID" Visible="false"></asp:BoundColumn>
                                                                <asp:BoundColumn DataField="Price" Visible="false"></asp:BoundColumn>
                                                                <asp:TemplateColumn ItemStyle-Width="60%">
                                                                    <ItemTemplate>
                                                                        <asp:TextBox ID="txtPrice" runat="server" style="display:none" Text='<%# Double.Parse(DataBinder.Eval(Container, "DataItem.Price").ToString()).ToString("00.00") %>'></asp:TextBox>
                                                                        <asp:CheckBox ID="chkSelectedMenu" runat="server" />
                                                                        <a href="#"><asp:Label ID="txtMenuName" onmouseover="javascript:ShowItems(this)" onmouseout="javascript:HideItems()" Text='<%# DataBinder.Eval(Container, "DataItem.Name") %>' runat="server"></asp:Label></a>
                                                                        <asp:DataGrid ID="dgMenuItems" AutoGenerateColumns="false" runat="server" style="border-collapse:separate"> <%--Edited for FF--%>
                                                                            <Columns>
                                                                                <asp:BoundColumn DataField="ID" Visible="false"></asp:BoundColumn>
                                                                                <asp:BoundColumn DataField="Name" HeaderText="アイテムリスト" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody"></asp:BoundColumn>
                                                                            </Columns>
                                                                        </asp:DataGrid>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn ItemStyle-Width="40%" >
                                                                    <ItemTemplate>
                                                                        <asp:TextBox runat="server" ID="txtQuantity" onkeyup="javascript:UpdateCheckbox(this);" Width="50px" CssClass="altText" Text='<%# DataBinder.Eval(Container, "DataItem.Quantity") %>'></asp:TextBox>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                            </Columns>
                                                        </asp:DataGrid>
                                                        <asp:Label ID="lblNoMenus" runat="server" Visible="false" Text="メニューが見つかりませんでした" CssClass="lblError" ></asp:Label>
                                                        <asp:Label ID="lblCateringMenus" ItemStyle-CssClass="tableBody" Visible="false" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.strMenus") %>'></asp:Label>
                                                    </EditItemTemplate>
                                                </asp:TemplateColumn>
                                                <%--Window Dressing--%>
                                                <asp:TemplateColumn HeaderText="コメント" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" ItemStyle-Wrap="true" ItemStyle-Width="20%">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblComments" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Comments") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtComments" TextMode="MultiLine" Rows="2" MaxLength="2000" CssClass="altText" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Comments") %>'></asp:TextBox>
                                                        <asp:RegularExpressionValidator ID="regComments" ControlToValidate="txtComments" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ &#34; は無効な記号です" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                                    </EditItemTemplate>
                                                </asp:TemplateColumn>
                                                <%--Window Dressing--%>
                                                <asp:TemplateColumn HeaderText="価格(USD)" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader"> 
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPrice" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Price") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate> 
                                                        <asp:Label ID="lblPrice" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Price") %>'></asp:Label>
                                                    </EditItemTemplate>
                                                </asp:TemplateColumn>
                                                <%--Window Dressing--%>
                                                <asp:TemplateColumn HeaderText="日付/時刻によって配達" ItemStyle-CssClass="tableBody"  HeaderStyle-CssClass="tableHeader">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDeliverByDateTime" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DeliverByDate") + " " + DataBinder.Eval(Container, "DataItem.DeliverbyTime") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtDeliverByDate" Width="100" runat="server" CssClass="altText" Text='<% # DataBinder.Eval(Container, "DataItem.DeliverByDate") %>'></asp:TextBox> <%-- FB 2050 --%>
                                                        <asp:Image ID="imgCalendar" runat="server" Height="20" Width="20" ImageUrl="image/calendar.gif" />
                                                        <mbcbb:ComboBox ID="lstDeliverByTime" runat="server" CausesValidation="True"
                                                        CssClass="altSelectFormat" Rows="10" style="width:auto" Text='<%# DataBinder.Eval(Container, "DataItem.DeliverByTime") %>'> <%--Edited for FF--%>
                                                        <asp:ListItem Value="01:00 AM"></asp:ListItem>
                                                        <asp:ListItem Value="02:00 AM"></asp:ListItem>
                                                        <asp:ListItem Value="03:00 AM"></asp:ListItem>
                                                        <asp:ListItem Value="04:00 AM"></asp:ListItem>
                                                        <asp:ListItem Value="05:00 AM"></asp:ListItem>
                                                        <asp:ListItem Value="06:00 AM"></asp:ListItem>
                                                        <asp:ListItem Value="07:00 AM"></asp:ListItem>
                                                        <asp:ListItem Value="08:00 AM"></asp:ListItem>
                                                        <asp:ListItem Value="09:00 AM"></asp:ListItem>
                                                        <asp:ListItem Value="10:00 AM"></asp:ListItem>
                                                        <asp:ListItem Value="11:00 AM"></asp:ListItem>
                                                        <asp:ListItem Value="12:00 PM"></asp:ListItem>
                                                        <asp:ListItem Value="01:00 PM"></asp:ListItem>
                                                        <asp:ListItem Value="02:00 PM"></asp:ListItem>
                                                        <asp:ListItem Value="03:00 PM"></asp:ListItem>
                                                        <asp:ListItem Value="04:00 PM"></asp:ListItem>
                                                        <asp:ListItem Value="05:00 PM"></asp:ListItem>
                                                        <asp:ListItem Value="06:00 PM"></asp:ListItem>
                                                        <asp:ListItem Value="07:00 PM"></asp:ListItem>
                                                        <asp:ListItem Value="08:00 PM"></asp:ListItem>
                                                        <asp:ListItem Value="09:00 PM"></asp:ListItem>
                                                        <asp:ListItem Value="10:00 PM"></asp:ListItem>
                                                        <asp:ListItem Value="11:00 PM"></asp:ListItem>
                                                        <asp:ListItem Value="12:00 AM"></asp:ListItem>
                                                    </mbcbb:ComboBox>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="lstDeliverByTime"
                                                            Display="Dynamic" ErrorMessage="必須"></asp:RequiredFieldValidator>
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="lstDeliverByTime"
                                                            Display="Dynamic" ErrorMessage="無効な時間 (HH:MM AM/PM)" ValidationExpression="[0|1][0-9]:[0-5][0-9] ?[a|A|p|P][M|m]"></asp:RegularExpressionValidator><%--FB 1715--%>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="txtDeliverByDate"
                                                            Display="Dynamic" ErrorMessage="必須" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                        <!--//Code changed by Offshore for FB Issue 1073 -- Start
                                                        <asp:RegularExpressionValidator ID="regDeliverByDate"  ControlToValidate="txtDeliverByDate" Display="Dynamic" ErrorMessage="Invalid Date (mm/dd/yyyy)" ValidationExpression="\b(0?[1-9]|1[012])[- /.](0?[1-9]|[12][0-9]|3[01])[- /.](19|20)?[0-9]{2}\b"></asp:RegularExpressionValidator> -->
                                                        <asp:RegularExpressionValidator ID="regDeliverByDate" runat="server" ControlToValidate="txtDeliverByDate" Display="Dynamic" ErrorMessage="無効な日付です" ValidationExpression="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d|(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d"></asp:RegularExpressionValidator>
                                                        <!--//Code changed by Offshore for FB Issue 1073 -- End-->
                                                    </EditItemTemplate>
                                                </asp:TemplateColumn>
                                                <%--Window Dressing--%>
                                                <asp:TemplateColumn HeaderText="アクション" HeaderStyle-CssClass="tableHeader">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="btnEdit" Text="編集" runat="server" CommandName="Edit" CommandArgument="2"></asp:LinkButton>
                                                        <asp:LinkButton ID="btnDelete" Text="削除" runat="server" CommandName="Delete" CommandArgument="2"></asp:LinkButton>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:LinkButton ID="btnUpdate" Text="作成" runat="server" CommandName="Update" CommandArgument="2"></asp:LinkButton><%--FB 1086 work order fixes --%>
                                                        <asp:LinkButton ID="btnCancel" Text="キャンセル" runat="server" CommandName="Cancel" CommandArgument="2"></asp:LinkButton>
                                                    </EditItemTemplate>
                                                </asp:TemplateColumn>
                                            </Columns>
                                        </asp:DataGrid>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" style="font-weight:bold"colspan="3">
                             <asp:Button ID="btnAddNewCAT" runat="server" CssClass="altLongBlueButtonFormat" Text="新規ワークオーダーを追加する" OnClientClick="javascript:DataLoading(1)" OnClick="C_btnAddNew_Click" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                 </table>
                                    </asp:Panel>
                    <div id="tblMenuItems" style="display:none">
    
                    </div>
                                </asp:View>
                                <asp:View ID="SelectHousekeeping" runat="server" OnLoad="SetInstructionsHK" >
                                    <asp:Panel ID="pnlSelectHousekeeping" runat="server" Width="100%">
                     <h3>ハウスキーピングワークオーダー</h3>
                     <input type="hidden" id="Hidden6" value="16">
                     <input type="hidden" id="lblTab" value="3" />
                     <table width="90%"><!--FB 1086 -->
                    <tr>
                        <td width="90%" align="left" style="height: 18px">
                            <asp:Label ID="lblHKWOInstructions" runat="server" Width="90%" class="blackblodtext"></asp:Label>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;<table width="100%">
                                <tr>
                                    <td colspan="3">
                                    
                            <asp:DataGrid ID="HKMainGrid" runat="server" AutoGenerateColumns="False" CellPadding="4" GridLines="None"
                                OnItemCreated="BindRowsDeleteMessage" OnCancelCommand="HKMainGrid_Cancel" OnUpdateCommand="HKMainGrid_Update" OnEditCommand="HKMainGrid_Edit" OnDeleteCommand="HKMainGrid_DeleteCommand" Width="100%" AllowSorting="True" style="border-collapse:separate"> <%--Edited for FF--%>
                            <FooterStyle CssClass="tableBody" />
                            <AlternatingItemStyle CssClass="tableBody" VerticalAlign="top" HorizontalAlign="left" />
                            <ItemStyle CssClass="tableBody" VerticalAlign="top" HorizontalAlign="left" />
                            <Columns>
                                <asp:BoundColumn DataField="ID" Visible="False"></asp:BoundColumn>
                                <asp:BoundColumn DataField="Name" HeaderText="名前" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader"></asp:BoundColumn>
                                <asp:BoundColumn DataField="AssignedToID" Visible="False"></asp:BoundColumn>
                                <asp:BoundColumn DataField="AssignedToName" ItemStyle-CssClass="tableBody" HeaderText="に割り当て" HeaderStyle-CssClass="tableHeader"></asp:BoundColumn>
                                <asp:BoundColumn DataField="StartByDate" ItemStyle-CssClass="tableBody"  HeaderText="日<br>付で開始" HeaderStyle-CssClass="tableHeader"></asp:BoundColumn>
                                <asp:BoundColumn DataField="StartByTime" ItemStyle-CssClass="tableBody"  HeaderText="時間で開始" HeaderStyle-CssClass="tableHeader"></asp:BoundColumn>
                                <asp:BoundColumn DataField="CompletedByDate" ItemStyle-CssClass="tableBody"  HeaderText="日<br>完全" HeaderStyle-CssClass="tableHeader"></asp:BoundColumn>
                                <asp:BoundColumn DataField="CompletedByTime" ItemStyle-CssClass="tableBody"  HeaderText="の時間<br>完全" HeaderStyle-CssClass="tableHeader"></asp:BoundColumn>
                                <asp:BoundColumn DataField="Status" ItemStyle-CssClass="tableBody"  HeaderText="状況" HeaderStyle-CssClass="tableHeader"></asp:BoundColumn>
                                <asp:BoundColumn DataField="RoomID" Visible="False"></asp:BoundColumn>
                                <asp:BoundColumn DataField="RoomName" ItemStyle-CssClass="tableBody"  HeaderText="部屋" HeaderStyle-CssClass="tableHeader"></asp:BoundColumn>
                                <asp:BoundColumn DataField="Comments" ItemStyle-CssClass="tableBody"  HeaderText="コメント" HeaderStyle-CssClass="tableHeader"></asp:BoundColumn>
                                <asp:BoundColumn DataField="TotalCost" ItemStyle-CssClass="tableBody"  HeaderText="トータルコスト" HeaderStyle-CssClass="tableHeader"></asp:BoundColumn>
                                <asp:TemplateColumn HeaderText="アクション" ItemStyle-CssClass="tableBody"  HeaderStyle-CssClass="tableHeader">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="btnEdit" CommandName="Edit" runat="server" Text="編集" Visible='<%# DataBinder.Eval(Container, "DataItem.Status") != "完了" %>' ></asp:LinkButton>
                                        <asp:LinkButton ID="btnDelete" CommandName="Delete" runat="server" Text="削除" ></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:BoundColumn DataField="SetID" Visible="false"></asp:BoundColumn>
                                <asp:BoundColumn DataField="ReqQuantity" Visible="false" HeaderStyle-CssClass="tableHeader"></asp:BoundColumn>
                                <asp:BoundColumn DataField="Notify" Visible="false" HeaderStyle-CssClass="tableHeader"></asp:BoundColumn>
                                <asp:BoundColumn DataField="RoomLayout" Visible="False"></asp:BoundColumn> <%--FB 2079--%>
                                </Columns>
                                <SelectedItemStyle Font-Bold="True" CssClass="tableBody"/>
                                <EditItemStyle CssClass="tableBody"  />
                                <AlternatingItemStyle CssClass="tableBody" />
                                <ItemStyle CssClass="tableBody"/>
                                <HeaderStyle CssClass="tableHeader" />
                                <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                        </asp:DataGrid>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                   <asp:Table runat="server" ID="HKItemsTable" Visible="False" Width="100%" CellPadding="0" CellSpacing="0">
                                   <asp:TableHeaderRow Height="30">
                                    <asp:TableHeaderCell Height="30" CssClass="subtitleblueblodtext"><asp:Label ID="lblNewEditHK" runat="server"></asp:Label></asp:TableHeaderCell>
                                       </asp:TableHeaderRow>
               <asp:TableRow ID="TableRow7" runat="server" HorizontalAlign="center"><asp:TableCell ID="TableCell7" runat="server">
                        <table border="0" cellpadding="5" cellspacing="0" width="70%">
                        <tr>
                            <td align="left" width="15%" class="blackblodtext">部屋</td>
                            <td align="left" style="width:25%;">
                                <asp:DropDownList ID="lstHKRooms" runat="server" AutoPostBack="True" CssClass="altSelectFormat"
                                    OnSelectedIndexChanged="selHKRooms_SelectedIndexChanged">
                                </asp:DropDownList></td>
                            <td align="left" class="blackblodtext" width="15%">名前を設定します。</td>
                            <td align="left"  width="25%">
                                <asp:DropDownList ID="lstHKSet" runat="server" AutoPostBack="True" CssClass="altSelectFormat"
                                    OnSelectedIndexChanged="selHKSet_SelectedIndexChanged">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="req2HK" ControlToValidate="lstHKSet" ErrorMessage="必須" runat="server" InitialValue="Select one..." ValidationGroup="Submit" Display="dynamic"></asp:RequiredFieldValidator><%--FB 1951--%>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="blackblodtext">名前</td>
                            <td align="left" >
                                <asp:TextBox ID="txtHKWorkOrderName" runat="server" CssClass="altText"></asp:TextBox><asp:TextBox ID="txtHKWorkOrderID" runat="server" CssClass="altText" Visible="False"></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtHKWorkOrderName" Display="Dynamic" ErrorMessage="必須" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="regHKWOName" ControlToValidate="txtHKWorkOrderName" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < と > は無効な記号です" ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator> <%--FB 2321--%>
                            </td>
                            <td align="left" class="blackblodtext">人の電荷</td>
                            <td align="left" >
                                <asp:TextBox ID="txtApprover3" runat="server" CssClass="altText" Enabled="true"></asp:TextBox>
                                &nbsp;<img id="Img2" onclick="javascript:getYourOwnEmailList(2)" src="image/edit.gif" /><asp:TextBox
                                    ID="hdnApprover3" runat="server" BackColor="Transparent" BorderColor="White"
                                    BorderStyle="None" Width="0px" ForeColor="Transparent"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ControlToValidate="txtApprover3"
                                    Display="Dynamic" ErrorMessage="必須" SetFocusOnError="True"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="blackblodtext">日付で開始</td>
                            <td align="left" >
                                <asp:TextBox ID="txtHKStartByDate" runat="server" CssClass="altText"></asp:TextBox>
                                <%--code changed for FB 1090--%>
                                <!--//Code changed by Offshore for FB Issue 1073 -- Start
                                <img id="Img6" height="20px" onclick="javascript:showCalendar('<%=txtHKStartByDate.ClientID %>', 'Img4', 1, '%m/%d/%Y');" change Img4 to Img6-->
                                <img id="Img6" height="20px" onclick="javascript:showCalendar('<%=txtHKStartByDate.ClientID %>', 'Img6', 1, '<%=format%>');" src="image/calendar.gif" width="20px" />
                                <!--//Code changed by Offshore for FB Issue 1073 -- End-->
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ControlToValidate="txtHKStartByDate"
                                    Display="Dynamic" ErrorMessage="必須" SetFocusOnError="True"></asp:RequiredFieldValidator>
                            </td>
                            <td align="left" class="blackblodtext">時間で開始</td>
                            <td align="left" >
                                 <%--Window Dressing--%>
                                 <mbcbb:ComboBox ID="startByTimeHK" CssClass="altSelectFormat" runat="server" CausesValidation="True"
                                   Rows="10" style="width:auto"><%--edited for FF--%>
                                    <asp:ListItem Selected="True" Value="01:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="02:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="03:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="04:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="05:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="06:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="07:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="08:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="09:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="10:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="11:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="12:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="01:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="02:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="03:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="04:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="05:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="06:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="07:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="08:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="09:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="10:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="11:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="12:00 AM"></asp:ListItem>
                                </mbcbb:ComboBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" ControlToValidate="startByTimeHK"
                                    Display="Dynamic" ErrorMessage="必須"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="startByTimeHK"
                                    Display="Dynamic" ErrorMessage="無効な時間 (HH:MM AM/PM)" ValidationExpression="[0|1][0-9]:[0-5][0-9] ?[a|A|p|P][M|m]"></asp:RegularExpressionValidator> <%-- FB Case 371 Saima --%> <%--FB 1715--%>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="blackblodtext">日までに完了</td>
                            <td style="font-weight: bold; " align="left" >
                                <asp:TextBox ID="txtHKCompletedBy" runat="server" CssClass="altText"></asp:TextBox>
                                <%--code changed for FB 1090--%>
                                <!--//Code changed by Offshore for FB Issue 1073 -- Start
                                <img id="Img7" height="20px" onclick="javascript:showCalendar('<%=txtCompletedBy.ClientID %>', 'cal_trigger', 1, '%m/%d/%Y');"
                                    src="image/calendar.gif" width="20px" />-->
                                <img id="Img7" height="20px" onclick="javascript:showCalendar('<%=txtHKCompletedBy.ClientID %>', 'Img7', 1, '<%=format%>');" src="image/calendar.gif" width="20px" />
                                    <!--//Code changed by Offshore for FB Issue 1073 -- End-->
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" ControlToValidate="txtHKCompletedBy"
                                    Display="Dynamic" ErrorMessage="必須" SetFocusOnError="True"></asp:RequiredFieldValidator>
                            </td>
                            <td align="left" class="blackblodtext">時間までに完了</td>
                            <td align="left" >
                                 <%--Window Dressing--%>
                                 <mbcbb:ComboBox ID="completedByTimeHK" CssClass="altSelectFormat" runat="server" CausesValidation="True"
                                    Rows="10" style="width:auto"><%--edited for FF--%>
                                    <asp:ListItem Selected="True" Value="01:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="02:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="03:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="04:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="05:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="06:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="07:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="08:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="09:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="10:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="11:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="12:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="01:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="02:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="03:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="04:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="05:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="06:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="07:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="08:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="09:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="10:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="11:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="12:00 AM"></asp:ListItem>
                                </mbcbb:ComboBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator18" runat="server" ControlToValidate="completedByTimeHK"
                                    Display="Dynamic" ErrorMessage="必須"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator18" runat="server" ControlToValidate="completedByTimeHK"
                                    Display="Dynamic" ErrorMessage="無効な時間 (HH:MM AM/PM)" ValidationExpression="[0|1][0-9]:[0-5][0-9] ?[a|A|p|P][M|m]"></asp:RegularExpressionValidator> <%-- FB Case 371 Saima --%><%--FB 1715--%>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="blackblodtext">ホストに通知する</td>
                            <td align="left" >
                                <asp:CheckBox ID="chkHKNotify" runat="server" /></td>
                            <td align="left" class="blackblodtext">タイムゾーン</td>
                            <td align="left" >
                                <asp:DropDownList ID="lstHKTimezone" OnInit="GetTimezones" CssClass="altSelectFormat" DataTextField="timezoneName" DataValueField="timezoneID" runat="server"></asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="blackblodtext">状況</td>
                            <td align="left" >
                                <asp:DropDownList ID="lstHKStatus" runat="server" CssClass="altSelectFormat">
                                    <asp:ListItem Text="承認待ち" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="完了" Value="1"></asp:ListItem>
                                </asp:DropDownList></td>
                            <td align="left" class="blackblodtext">コメント</td>
                            <td align="left" >
                                <asp:TextBox ID="txtHKComments" runat="server" CssClass="altText" Rows="2" TextMode="MultiLine"></asp:TextBox>
                                <asp:RegularExpressionValidator ValidationGroup="Submit" ID="RegularExpressionValidator19" ControlToValidate="txtHKComments" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ &#34; は無効な記号です" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:RegularExpressionValidator><%--FB 2011--%>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="blackblodtext">部屋のレイアウト</td>
                                <%--Code added for FB 1176--%>
                                 <td align="left" colspan="2"> 
                                   <asp:DropDownList ID="lstRoomLayout" runat="server" CssClass="altSelectFormat"></asp:DropDownList>                                
                                   <input type="Button" ID="btnViewRoomLayout" Class="altShortBlueButtonFormat" value="見る" OnClick="javascript:viewLayout()" />
                                </td>
                            <td align="left">&nbsp;</td>
                           </tr>
                    </table>
                    </asp:TableCell>
                                       </asp:TableRow>
                <asp:TableRow ID="TableRow8" runat="server">
                        <asp:TableCell ID="TableCell28" ColumnSpan="6" runat="server" HorizontalAlign="Center" ><br />
                             <asp:DataGrid runat="server" ID="itemsGridHK" AllowSorting="True" CellPadding="4" Font-Bold="False" Font-Names="arial" Font-Size="Small"
                              ForeColor="#333333" GridLines="None" Width="90%" AutoGenerateColumns="False" ShowFooter="true" OnItemDataBound="SetDeliveryAttributes" style="border-collapse:separate">  <%--WO Bug Fix--%>
                                    <AlternatingItemStyle CssClass="tableBody" />
                                    <ItemStyle CssClass="tableBody" />
                                    <HeaderStyle CssClass="tableHeader" />
                                    <FooterStyle CssClass="tableBody" HorizontalAlign="right" />
                                 <Columns>
                                     <asp:BoundColumn DataField="ID" Visible="False"></asp:BoundColumn>
                                     <asp:BoundColumn DataField="Name" HeaderText="タスク/アイテム" ItemStyle-CssClass="tableBody">
                                        <HeaderStyle CssClass="tableHeader"/>
                                     </asp:BoundColumn>
                                        <asp:BoundColumn DataField="SerialNumber" HeaderText="シリアル番号" ItemStyle-CssClass="tableBody">
                                        <HeaderStyle CssClass="tableHeader" />
                                        </asp:BoundColumn>
                                        <asp:TemplateColumn HeaderText="画像">
                                             <HeaderStyle CssClass="tableHeader" />
                                            <ItemTemplate >  <%--WO Bug Fix - Removed Image URL--%>
                                               <asp:Image Height="30" Width="30" runat="server" ID="imgItem" onmouseover="javascript:ShowImage(this)" onmouseout="javascript:HideImage()" />
                                            </ItemTemplate>
                                        </asp:TemplateColumn> 
                                        <asp:BoundColumn DataField="Comments" HeaderText="コメント">
                                        <%--Window Dressing--%>
                                        <HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>
                                     <asp:BoundColumn DataField="Price" HeaderText="価格 (USD)" ItemStyle-CssClass="tableBody">
                                        <HeaderStyle CssClass="tableHeader"  />
                                        </asp:BoundColumn>
                                        <asp:TemplateColumn HeaderText="発注" ItemStyle-CssClass="tableBody">
                                            <HeaderStyle CssClass="tableHeader" />
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtReqQuantity" runat="server" Width="30px" Text='<%# DataBinder.Eval(Container, "DataItem.QuantityRequested") %>'></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtReqQuantity" ErrorMessage="必須" Display="Dynamic"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="regHKQuantity" runat="server" Display="Dynamic" ErrorMessage="正の数値のみ" SetFocusOnError="True" ControlToValidate="txtReqQuantity" ValidationExpression="\d+"></asp:RegularExpressionValidator>
                                            </ItemTemplate>
                                            <FooterTemplate> <%--FB 1686--%> <%--FB 1830--%>
                                                <span class="blackblodtext" > 総 (<%=cFormat%>):</span><asp:Label runat="server" ID="lblTotalQuantity" Text="0" OnLoad="UpdateTotalHK"></asp:Label>&nbsp;<%--Fb 1086--%><%--FB 1830--%>
                                                <asp:Button ID="btnUpdateTotal" OnClick="UpdateTotalHK" runat="server" CssClass="altShortBlueButtonFormat" Text="アップデート" />
                                            </FooterTemplate>
                                        </asp:TemplateColumn>                                       
                            <asp:BoundColumn DataField="UID" Visible="false" HeaderText="UID"></asp:BoundColumn>
                                  </Columns>
                                </asp:DataGrid>
                        </asp:TableCell>
                                       </asp:TableRow>
                    <asp:TableRow ID="TableRow9" runat="server">
                        <asp:TableCell ID="TableCell29" ColumnSpan="6" runat="server" HorizontalAlign="center"> 
                            <asp:Button runat=server ID="btnHKCancel" OnClick="btnHKCancel_Click" UseSubmitBehavior="False" CssClass="altShortBlueButtonFormat" Text="キャンセル" OnClientClick="javascript:DataLoading(1)" ValidationGroup="HKCancel" />&nbsp;
                            <asp:Button runat=server ID="btnHKSubmit" OnClick="btnHKSubmit_Click" CssClass="altShortBlueButtonFormat" Text="送信" ValidationGroup="Submit"/><%--FB 1951--%>
                        </asp:TableCell>
                                                    </asp:TableRow>
                                        </asp:Table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" style="font-weight:bold"colspan="3">
                             <asp:Button ID="btnAddNewHK" runat="server" CssClass="altLongBlueButtonFormat" Text="新規H/Kワークオーダーを追加する" OnClientClick="javascript:DataLoading(1)" OnClick="H_btnAddNew_Click" />
                                    </td>
                                </tr>
                            </table>

                        </td>
                    </tr>
                 </table>

                                    </asp:Panel>
                                </asp:View>
                                <asp:View ID="AdditionalComments" runat="server">
                                    <asp:Panel ID="pnlAdditionalComments" runat="server" Width="100%">
                        <input type="hidden" id="Hidden8" value="11" />
                        <input type="hidden" id="isAdditionalComments" value="1" />
                        <table width="95%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td align="center">
                                   <h3>カスタムオプション</h3></td>
                            </tr>
                            <tr><td><br /></td></tr> <%--Custom Attribute Fixes--%>
                            <tr>
                                <td align="center">
                                    <asp:Table runat="server" ID="tblCustomAttribute" Width="80%" CellPadding="3" cellspacing="2" Visible="true">
                                    
                                    </asp:Table>
                                </td>
                             </tr>
                             <tr><%--FB 2341--%>
                                    <td align="center">
                                        <table width="100%" border ="0">
                                            <tr>
                                                <td align="left" id="trHdConcSupport" runat="server" width="28%">
                                                    <table width="100%" border="0">
                                                        <tr>
                                                            <td align="left" class="subtitlexxsblueblodtext">コンシェルジュのサポート</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                  <td align="left" id="tdTxtMsg" runat="server" width="42%"> <%--FB 2486--%>
                                                    <table width="100%" border="0">
                                                        <tr>
                                                            <td align="left" class="subtitlexxsblueblodtext"  width="50%">メッセージオーバーレイ</td>
                                                            <td align ="left" valign="baseline" class="blackblodtext"  width="20%">終了前に</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td align="left"  style="width:30%">
                                                    <table width="100%" border="0">
                                                        <tr>
                                                            <td align="left" class="subtitlexxsblueblodtext">アップロードファイル</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <%--FB 2632 Starts--%>
                                                <td align="left" valign="top" id="tdConcSupport" runat="server" >
                                                  <table id="tblConciergeNew" cellspacing="2" cellpadding="3" border="0" style="width:80%;">
                                                        <tr id="trOnSiteAVSupport" runat="server">
                                                            <td align="left" valign="middle" nowrap="nowrap" style="width: 2%;">
                                                                <input id="chkOnSiteAVSupport" type="checkbox" runat="server" />
                                                               <span class='blackblodtext'>On-Site A/V Support</span>
                                                            </td>
                                                        </tr>
                                                        <tr id="trMeetandGreet" runat="server">
                                                            <td align="left" valign="middle" nowrap="nowrap" style="width: 2%;">
                                                                <input id="chkMeetandGreet" type="checkbox" runat="server"  />
                                                                <span class='blackblodtext'>Meet and Greet</span>
                                                            </td>
                                                        </tr>
                                                        <tr id="trConciergeMonitoring" runat="server" >
                                                            <td align="left" valign="middle" nowrap="nowrap" style="width: 2%;">
                                                                <input id="chkConciergeMonitoring" type="checkbox" runat="server"  />
                                                                <span class='blackblodtext'>Concierge Monitoring</span>
                                                            </td>
                                                        </tr>
                                                        <tr id="trDedicatedVNOCOperator" runat="server">
                                                            <td align="left" valign="middle" nowrap="nowrap" style="width: 2%;">
                                                            <input id="chkDedicatedVNOCOperator" type="checkbox" runat="server"  />
                                                            <span class='blackblodtext'>Dedicated VNOC Operator</span><br /><br />
                                                            <asp:TextBox ID="txtApprover6" runat="server" CssClass="altText"></asp:TextBox>
                                                             &nbsp;<img id="img9" onclick="javascript:getYourOwnEmailList(5)" src="image/edit.gif" />
                                                             <a href="javascript: deleteApprover(6);" onmouseover="window.status='';return true;">
                                                             <img border="0" id="img11" src="image/btn_delete.gif" alt="delete" width="16" height="16" runat="server"></a>
                                                            <asp:TextBox ID="hdnApprover6" runat="server" BackColor="Transparent" BorderColor="White"
                                                                BorderStyle="None" Width="0px" ForeColor="Black" style="display:none"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                </table>
                                                </td>
                                                 <%--FB 2632 End--%>
                                                <%--FB 2486 Starts--%>
                                                <td align="left" id="tdTxtMsgDetails" runat="server" valign="top">
                                                    <table width="100%">
                                                        <tr>
                                                            <td valign="top">
                                                                <table cellpadding="3" cellspacing="0" width="100%" border = "0">
                                                                    <tr  id="tr1" runat="server">
                                                                        <td align="left"  width="5%" >
                                                                        <asp:CheckBox ID="chkmsg1" runat="server" TextAlign="Left" onclick="javascript:return fnCheck('drpdownmsgduration1');"  />
                                                                        </td>
                                                                        <td align="left" width="68%">
                                                                           <asp:DropDownList ID="drpdownconfmsg1" CssClass="altSelectFormat" runat="server" DataTextField="ConfMsg" DataValueField="ConfMsgID" Width="100%" >
                                                                           </asp:DropDownList>
                                                                        </td>
                                                                        <td align ="left" width="12%">
                                                                        <asp:DropDownList ID="drpdownmsgduration1" CssClass="altText" runat="server" Width="100%" onChange="javascript:return fnCheck(this.id);" >
                                                                        </asp:DropDownList>
                                                                        </td>
                                                                         <td align ="left" width="10%"></td>
                                                                     </tr>
                                                                    <tr  id="tr2" runat="server">
                                                                        <td align="left" >
                                                                        <asp:CheckBox ID="chkmsg2" runat="server" TextAlign="Left" onclick="javascript:return fnCheck('drpdownmsgduration2');" />
                                                                        </td>
                                                                        <td align="left">
                                                                            <asp:DropDownList ID="drpdownconfmsg2" CssClass="altSelectFormat" runat="server" DataTextField="ConfMsg" DataValueField="ConfMsgID"  Width="100%" >
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                        <td align ="left">
                                                                        <asp:DropDownList ID="drpdownmsgduration2" CssClass="altText" runat="server"   Width="100%" onChange="javascript:return fnCheck(this.id);"> 
                                                                        </asp:DropDownList>
                                                                        </td>
                                                                        <td></td>
                                                                     </tr>
                                                                    <tr  id="tr3" runat="server">
                                                                        <td align="left" >
                                                                        <asp:CheckBox ID="chkmsg3" runat="server" TextAlign="Left" onclick="javascript:return fnCheck('drpdownmsgduration3');" />
                                                                        </td>
                                                                        <td align="left">
                                                                           <asp:DropDownList ID="drpdownconfmsg3" CssClass="altSelectFormat" runat="server" DataTextField="ConfMsg" DataValueField="ConfMsgID"  Width="100%" >
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                        <td align ="left">
                                                                        <asp:DropDownList ID="drpdownmsgduration3" CssClass="altText" runat="server"   Width="100%" onChange="javascript:return fnCheck(this.id);"> 
                                                                        </asp:DropDownList>
                                                                        </td>
                                                                        <td><a id="displayText"  runat="server" href="javascript:toggle();">より多くの</a></td> <%--FB 2506--%>
                                                                     </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table id="toggleText" cellpadding="3" cellspacing="0" width="100%" border="0" runat="server"> <%--FB 2506--%>
                                                                     <tr  id="tr4" runat="server"> 
                                                                        <td align="left"  width="5%" >
                                                                        <asp:CheckBox ID="chkmsg4" runat="server" TextAlign="Left" onclick="javascript:return fnCheck('drpdownmsgduration4');"  />
                                                                        </td>
                                                                        <td align="left"  width="68%">
                                                                           <asp:DropDownList ID="drpdownconfmsg4" CssClass="altSelectFormat" runat="server" DataTextField="ConfMsg" DataValueField="ConfMsgID"  Width="100%" >
                                                                           </asp:DropDownList>
                                                                        </td>
                                                                        <td align ="left"  width="12%">
                                                                        <asp:DropDownList ID="drpdownmsgduration4" CssClass="altText" runat="server"  Width="100%" onChange="javascript:return fnCheck(this.id);" >
                                                                        </asp:DropDownList>                                                            
                                                                        </td>
                                                                         <td align ="left" width="10%"></td>
                                                                     </tr>
                                                                     <tr  id="tr5" runat="server">
                                                                        <td align="left" >
                                                                        <asp:CheckBox ID="chkmsg5" runat="server" TextAlign="Left" onclick="javascript:return fnCheck('drpdownmsgduration5');"  />
                                                                        </td>
                                                                        <td align="left">
                                                                           <asp:DropDownList ID="drpdownconfmsg5" CssClass="altSelectFormat" runat="server" DataTextField="ConfMsg" DataValueField="ConfMsgID"  Width="100%">
                                                                        </asp:DropDownList>
                                                                        </td>
                                                                        <td align ="left">
                                                                        <asp:DropDownList ID="drpdownmsgduration5" CssClass="altText" runat="server"  Width="100%" onChange="javascript:return fnCheck(this.id);" >
                                                                        </asp:DropDownList>
                                                                        </td>
                                                                        <td></td>
                                                                     </tr>
                                                                     <tr  id="tr6" runat="server">
                                                                        <td align="left" >
                                                                        <asp:CheckBox ID="chkmsg6" runat="server" TextAlign="Left" onclick="javascript:return fnCheck('drpdownmsgduration6');" />
                                                                        </td>
                                                                        <td align="left">
                                                                           <asp:DropDownList ID="drpdownconfmsg6" CssClass="altSelectFormat" runat="server" DataTextField="ConfMsg" DataValueField="ConfMsgID"  Width="100%" >
                                                                        </asp:DropDownList>
                                                                        </td>
                                                                        <td align ="left">
                                                                        <asp:DropDownList ID="drpdownmsgduration6" CssClass="altText" runat="server"  Width="100%" onChange="javascript:return fnCheck(this.id);" >
                                                                        </asp:DropDownList>
                                                                        </td>
                                                                        <td></td>
                                                                     </tr>
                                                                     <tr  id="tr7" runat="server"> 
                                                                        <td align="left" >
                                                                        <asp:CheckBox ID="chkmsg7" runat="server" TextAlign="Left" onclick="javascript:return fnCheck('drpdownmsgduration7');" />
                                                                        </td>
                                                                        <td align="left">
                                                                           <asp:DropDownList ID="drpdownconfmsg7" CssClass="altSelectFormat" runat="server" DataTextField="ConfMsg" DataValueField="ConfMsgID"  Width="100%">
                                                                           </asp:DropDownList>
                                                                        </td>
                                                                        <td align ="left">
                                                                        <asp:DropDownList ID="drpdownmsgduration7" CssClass="altText" runat="server" Width="100%" onChange="javascript:return fnCheck(this.id);"> 
                                                                        </asp:DropDownList>
                                                                        </td>
                                                                        <td></td>
                                                                     </tr>
                                                                     <tr  id="tr8" runat="server"> 
                                                                        <td align="left" >
                                                                        <asp:CheckBox ID="chkmsg8" runat="server" TextAlign="Left" onclick="javascript:return fnCheck('drpdownmsgduration8');" />
                                                                        </td>
                                                                        <td align="left">
                                                                           <asp:DropDownList ID="drpdownconfmsg8" CssClass="altSelectFormat" runat="server" DataTextField="ConfMsg" DataValueField="ConfMsgID"  Width="100%">
                                                                        </asp:DropDownList>
                                                                        </td>
                                                                        <td align ="left">
                                                                        <asp:DropDownList ID="drpdownmsgduration8" CssClass="altText" runat="server"  Width="100%" onChange="javascript:return fnCheck(this.id);" >
                                                                        </asp:DropDownList>
                                                                        </td>
                                                                        <td></td>
                                                                     </tr>
                                                                     <tr  id="tr9" runat="server">
                                                                        <td align="left" >
                                                                        <asp:CheckBox ID="chkmsg9" runat="server" TextAlign="Left" onclick="javascript:return fnCheck('drpdownmsgduration9');" />
                                                                        </td>
                                                                        <td align="left">
                                                                           <asp:DropDownList ID="drpdownconfmsg9" CssClass="altSelectFormat" runat="server" DataTextField="ConfMsg" DataValueField="ConfMsgID"  Width="100%" >
                                                                        </asp:DropDownList>
                                                                        </td>
                                                                        <td align ="left">
                                                                        <asp:DropDownList ID="drpdownmsgduration9" CssClass="altText" runat="server"  Width="100%" onChange="javascript:return fnCheck(this.id);"> 
                                                                        </asp:DropDownList>
                                                                        </td>
                                                                        <td></td>
                                                                     </tr>
                                                                </table>
                                                            </td>
                                                        </tr>   
                                                    </table>
                                              </td>
                                              <%--FB 2486 Ends--%>
                                                <td align="left" valign="top" >
                                                <table cellpadding="3" cellspacing="0" width="100%" border = "0" style="margin-left:5%">
                                                    <%-- Custom Attribute Fixes start --%>
                                                        <tr  id="trFile1" runat="server"> <%-- Code Modified For MOJ Phase2 --%>                                
                                                            <td align="left" class="blackblodtext" nowrap="nowrap">ファイル1</td>
                                                            <td align="left">
                                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td width="30%">
                                                                            <input type="file" id="FileUpload1" contenteditable="false" enableviewstate="true" size="40" class="altText" runat="server" />
                                                                            <asp:Label ID="lblUpload1" Text="" Visible="false" runat="server"></asp:Label>
                                                                        </td>
                                                                        <td width="30%" align="left">
                                                                            <asp:Button ID="btnRemove1" CssClass="altShortBlueButtonFormat" Text="除去する" Visible="false" runat="server" OnCommand="RemoveFile" CommandArgument="1" />
                                                                            <asp:Label ID="hdnUpload1" Text="" Visible="false" runat="server"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                         </tr>
                                                        <tr  id="trFile2" runat="server"> <%-- Code Modified For MOJ Phase2 --%>                             
                                                            <td align="left" class="blackblodtext" nowrap="nowrap">ファイル2</td>
                                                            <td align="left">
                                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td width="50%">
                                                                                <input type="file" id="FileUpload2" contenteditable="false" enableviewstate="true" size="50" class="altText" runat="server" />
                                                                                <asp:Label ID="lblUpload2" Text="" Visible="false" runat="server"></asp:Label>
                                                                            </td>
                                                                            <td width="30%">
                                                                                <asp:Button ID="btnRemove2" CssClass="altShortBlueButtonFormat" Text="除去する" Visible="false" runat="server" OnCommand="RemoveFile" CommandArgument="2" />
                                                                                <asp:Label ID="hdnUpload2" Text="" Visible="false" runat="server"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                            </td>
                                                         </tr>
                                                        <tr  id="trFile3" runat="server"> <%-- Code Modified For MOJ Phase2 --%>                             
                                                             
                                                            <td align="left" class="blackblodtext" nowrap="nowrap">ファイル3</td>
                                                            <td align="left">
                                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td width="50%">
                                                                                <input type="file" id="FileUpload3" contenteditable="false" enableviewstate="true" size="50" class="altText" runat="server" />
                                                                                <asp:Label ID="lblUpload3" Text="" Visible="false" runat="server"></asp:Label>
                                                                            </td>
                                                                            <td width="30%">
                                                                                <asp:Button ID="btnRemove3" CssClass="altShortBlueButtonFormat" Text="除去する" Visible="false" runat="server" OnCommand="RemoveFile" CommandArgument="3" />
                                                                                <asp:Label ID="hdnUpload3" Text="" Visible="false" runat="server"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                            </td>
                                                          </tr>
                                                        <tr>
                                                            <td colspan="2" align="center">
                                                                <asp:Button ID="btnUploadFiles" OnClick="UploadFiles" runat="server" Text="アップロードファイル" CssClass="altLongBlueButtonFormat" />
                                                            </td>
                                                          </tr>
                                                    <%--Custom Attribute Fixes End--%>
                                                </table>
                                              </td>   
                                        </tr>
                                        </table>
                                    </td>
                                    
                                </tr>
                             
                             <tr>
                                <td align="left">
                                  <%--code added for Add FB 1470 start--%>
                                <%if (!(client.ToString().ToUpper() == "MOJ"))
                                  {%> <%--Edited For FB 1425--%>
                                    
                                
                                    <asp:Table runat="server" ID="tblEntityCode" Width="100%" CellPadding="0" cellspacing="2" Visible="false">
                                    <asp:TableRow>
                                        <asp:TableCell Width="8%"></asp:TableCell>
                                        <asp:TableCell Width="7%" HorizontalAlign="left" CssClass="blackblodtext">
                                            課金コード
                                        </asp:TableCell>
                                        <asp:TableCell>
                                           <%-- <asp:TextBox ID="txtCANGC1" runat="server" CssClass="altText"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator20" ControlToValidate="txtCANGC1" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ and &#34; are invalid characters." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:RegularExpressionValidator>--%>
                                            &nbsp;
                                            <asp:DropDownList ID="lstEntityCode" CssClass="altLong0SelectFormat" DataTextField="Name" DataValueField="ID" runat="server" AutoPostBack="false">
                                            </asp:DropDownList>
                                        </asp:TableCell>
                                        <asp:TableCell Width="20%" HorizontalAlign="Right" ID="tblEntityDesc" runat="server" Visible="false" Font-Bold>
                                                請求書の説明
                                        </asp:TableCell>
                                        <asp:TableCell>
                                               <asp:Label ID="lblEntityDesc" runat="server" Visible="false" ForeColor="Blue"></asp:Label>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                                <%}%><%--Fb 1425 --%>
                                  <%--code added for Add FB 1470 end--%>
                                
                                <asp:Table runat="server" ID="tblBillingOptions" width="100%" CellSpacing="2" CellPadding="0" Visible="false">
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <table>
                                                <tr>
                                                    <td>
                                    <asp:Label ID="Label1" runat="server" CssClass="subtitleblueblodtext" Text="支払いオプション"></asp:Label>
                                </td>
                            </tr>
                           
                            <tr>
                                <td align="left">
                                    <table border="0" cellspacing="5" cellpadding="0" width="100%">
                                        <tr>
                                            <td align="right" width="20%" class="blackblodtext">人の名前の法案に</td>
                                            <td align="left">
                                                <asp:TextBox ID="txtCA1" runat="server" CssClass="altText"></asp:TextBox>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator4" ControlToValidate="txtCA1" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ &#34; は無効な記号です" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                            </td>
                                            <td align="right" class="blackblodtext">リクエスタメール</td>
                                            <td align="left">
                                                <asp:TextBox ID="txtCA2" runat="server" CssClass="altText"></asp:TextBox>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator21" ControlToValidate="txtCA2" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ &#34; は無効な記号です" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>^+;?|!`,\[\]{}\x22;=:#$%&()']*$"></asp:RegularExpressionValidator>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator5" ControlToValidate="txtCA2" Display="dynamic" runat="server" ErrorMessage="<br>無効なEメールアドレス" ValidationExpression="^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"></asp:RegularExpressionValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="blackblodtext">部門名</td>
                                            <td align="left">
                                                <asp:TextBox ID="txtCA3" runat="server" CssClass="altText"></asp:TextBox>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator6" ControlToValidate="txtCA3" Display="dynamic" runat="server" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ &#34; は無効な記号です" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                            </td>
                                            <td align="right" class="blackblodtext">リクエスタキャンパスボックス</td>
                                            <td align="left">
                                                <asp:TextBox ID="txtCA4" runat="server" CssClass="altText"></asp:TextBox>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator7" ControlToValidate="txtCA4" Display="dynamic" runat="server" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ &#34; は無効な記号です" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                            </td>
                                            <td align="right" class="blackblodtext"></td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="blackblodtext">リクエスタ電話</td>
                                            <td>
                                                <asp:TextBox ID="txtCA5" runat="server" CssClass="altText"></asp:TextBox>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator8" ControlToValidate="txtCA5" Display="dynamic" runat="server" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ &#34; は無効な記号です" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                            </td>
                                            <td align="right" class="blackblodtext">FISの請求番号</td>
                                            <td>
                                                <asp:TextBox ID="txtCA6" runat="server" CssClass="altText"></asp:TextBox>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator9" ControlToValidate="txtCA6" Display="dynamic" runat="server" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ &#34; は無効な記号です" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="blackblodtext">チェックでのお支払い</td>
                                            <td>
                                                <asp:CheckBox ID="chkCA1" runat="server" />
                                            </td>
                                            <td align="right" class="blackblodtext" valign="top" width="20%" rowspan="2">
                                                外部ビル<br />
                                                場所/スポンサー</td>
                                            <td rowspan="2">
                                                <asp:TextBox ID="txtCA7" runat="server" CssClass="altText" Rows="4" TextMode="MultiLine"></asp:TextBox>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator10" ControlToValidate="txtCA7" Display="dynamic" runat="server" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ &#34; は無効な記号です" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="blackblodtext">参加者の数</td>
                                            <td>
                                                <asp:TextBox ID="txtCA10" runat="server" CssClass="altText"></asp:TextBox>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator11" ControlToValidate="txtCA10" Display="dynamic" runat="server" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ &#34; は無効な記号です" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                                <asp:RegularExpressionValidator ID="reg10" runat="server" ControlToValidate="txtCA10" Display=Dynamic ValidationExpression="\d+" ErrorMessage="数値のみ"></asp:RegularExpressionValidator>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td align="right" class="blackblodtext">現場での代表名</td>
                                            <td>
                                                <asp:TextBox ID="txtCA8" runat="server" CssClass="altText"></asp:TextBox>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator12" ControlToValidate="txtCA8" Display="dynamic" runat="server" ErrorMessage="<br> & < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ &#34; は無効な記号です" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                            </td>
                                            <td align="right" class="blackblodtext">現場での代表電話番号</td>
                                            <td>
                                                <asp:TextBox ID="txtCA9" runat="server" CssClass="altText"></asp:TextBox>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator13" ControlToValidate="txtCA9" Display="dynamic" runat="server" ErrorMessage="<br> & < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ &#34; は無効な記号です" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                            </td>
                                        </tr> 
                                    </table>
                                    </td></tr></table>
                                    </asp:TableCell>
                                        
                                    </asp:TableRow>
                                </asp:Table>
                                <asp:Table runat="server" ID="tblLHRICCustomAttributes" Width="100%" Visible="false">
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                <table width="100%" cellspacing="5" cellpadding="2">
                                                    <tr>
                                                        <td colspan="4" align="left">
                                                            <SPAN class=subtitleblueblodtext>カスタム属性</SPAN>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" width="25%"><asp:Label ID="lblCA12" runat="server" class="blackblodtext"></asp:Label></td> <%--Student Class Conference--%>
                                                        <td align="left"><asp:DropDownList ID="ctrlCA12" runat="server" CssClass="altLongSelectFormat" Width="160"></asp:DropDownList></td>
                                                        <td align="right"><asp:Label ID="lblCA13" runat="server" class="blackblodtext"></asp:Label></td> <%--Grade Level--%>
                                                        <td align="left"><asp:DropDownList ID="ctrlCA13" runat="server" CssClass="altLongSelectFormat" Width="160"></asp:DropDownList></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right"><asp:Label ID="lblCA17" runat="server" class="blackblodtext"></asp:Label></td><%--Professional Development--%>
                                                        <td align="left"><asp:DropDownList ID="ctrlCA17" runat="server" CssClass="altLongSelectFormat" Width="160"></asp:DropDownList></td>
                                                        <td align="right"><asp:Label ID="lblCA14" runat="server" class="blackblodtext"></asp:Label></td><%--Content Area --%>
                                                        <td align="left"><asp:DropDownList ID="ctrlCA14" runat="server" CssClass="altLongSelectFormat" Width="160"></asp:DropDownList></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right"><asp:Label ID="lblCA18" runat="server" class="blackblodtext"></asp:Label></td><%--Guidance/College--%>
                                                        <td align="left"><asp:DropDownList ID="ctrlCA18" runat="server" CssClass="altLongSelectFormat" Width="160"></asp:DropDownList></td>
                                                        <td align="right"><asp:Label ID="lblCA15" runat="server" class="blackblodtext"></asp:Label></td><%--Teacher Last Name--%>
                                                        <td align="left"><asp:TextBox ID="ctrlCA15" runat="server" CssClass="altText"></asp:TextBox></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right"><asp:Label ID="lblCA19" runat="server" class="blackblodtext"></asp:Label></td><%--Administrative--%>
                                                        <td align="left"><asp:DropDownList ID="ctrlCA19" runat="server" CssClass="altLongSelectFormat" Width="160"></asp:DropDownList></td>
                                                        <td align="right"><asp:Label ID="lblCA16" runat="server" class="blackblodtext"></asp:Label></td><%--2nd Teacher Last Name--%>
                                                        <td align="left"><asp:TextBox ID="ctrlCA16" runat="server" CssClass="altText"></asp:TextBox></td>
                                                    </tr>
                                                </table>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                     </asp:Table>
                                </td>
                            </tr>
                          </table>
                                    </asp:Panel>
                                </asp:View>
                                <asp:View ID="Preview" runat="server">
                                    <asp:Panel ID="pnlPreview" runat="server">
                <input type="hidden" id="Hidden9" value="13">

                <table width="90%" border="0" bordercolor="blue" cellspacing="2" cellpadding="2">
                    <tr>
                        <td colspan="3" style="height: 20px; text-align: center">
                            <h3> <%-- Code Added for FB 1428--%>
                            <span ID="Field5" runat="server">プレビュー</span>
                            </h3>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:10%;"></td>
                        <td align="left" class="blackblodtext" style="width:22%;">名前</td>
                        <td valign="top">
                            <asp:Label ID="plblConfName" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <%--Code added for FB : 1116 -- Start--%>                    
                    <tr>
                        <td></td>
                        <td align="left" class="blackblodtext">主催者名 & メール</td>
                        <td valign="top">
                            <asp:Label ID="plblHostName" runat="server"></asp:Label>
                        </td>                        
                    </tr>
					<%--FB 2501 starts--%>
                    <tr>
                        <td></td>
                        <td align="left" class="blackblodtext">リクエスタの名前と電子メール</td>
                        <td valign="top">
                            <asp:Label ID="plblRequestorName" runat="server"></asp:Label>
                        </td>   
                    </tr>
					<tr id ="trStartMode1" runat="server">
                        <td></td>
                        <td align="left" class="blackblodtext">開始モード</td>
                        <td valign="top">
                            <asp:Label ID="plblStartMode" runat="server"></asp:Label>
                        </td>                        
                    </tr>
                    <%--FB 2501 Ends--%>
                    <tr id="trPwd" runat="server"> <%--Code Modified For MOJ Phase2 --%>                    
                        <td></td>
                        <td align="left" class="blackblodtext">パスワード</td>
                        <td valign="top">
                            <asp:Label ID="plblPassword" runat="server"></asp:Label>
                        </td>
                        
                    </tr>
                    <tr>
                        <td></td>
                        <td align="left" class="blackblodtext">説明</td>
                        <td valign="top">
                            <asp:Label ID="plblConfDescription" Width="500" runat="server"></asp:Label>
                        </td>
                    </tr>
                     <tr id="trType" runat="server"> <%-- Code Modified For MOJ Phase2 --%>
                        <td></td>
                        <td align="left" class="blackblodtext">タイプ</td>
                        <td valign="top">
                            <asp:Label ID="plblConfType" runat="server"></asp:Label>
                        </td>
                    </tr>
                    
                    <%if (EnableVMR == "1")
                      { %>
                      
                      <tr>
                        <td></td>
                         <td align="left" class="blackblodtext">VRMコール</td>
                        <td valign="top">
                            <asp:Label ID="plblConfVMR" runat="server"></asp:Label>
                        </td>
                    </tr>
                      
                      <%} %>
                    
                    <tr>
                        <td></td>
                        <td class="blackblodtext" align="left" id="PSndDate" runat="server">開始日/時間</td>
                        <td valign="top">
                            <asp:Label ID="plblConfStartDateTime" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <%--Code added for buffer zone -- Start --%>
                    <%if (enableBufferZone == "1")
                      { %>
                    <tr>
                        <td></td>
                        <td align="left" class="blackblodtext">
                          <asp:Label runat="server" ID="lblSetupDateTime"></asp:Label>
                        </td>
                        <td valign="top">
                            <asp:Label ID="plblSetupDTime" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td align="left" class="blackblodtext">
                          <asp:Label runat="server" ID="lblTeardownDateTime"></asp:Label>
                        </td>
                        <td valign="top">
                            <asp:Label ID="plblTeardownDTime" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <%--Code added for buffer zone -- End --%>
                    <%} %>
                    <tr>
                        <td></td>
                        <td align="left" class="blackblodtext" id="PEndDate" runat="server">終了日/時間</td>
                        <td valign="top">
                            <asp:Label ID="plblConfEndDateTime" runat="server"></asp:Label>
                        </td>
                    </tr>
                   
                    <tr id="trRec" runat="server"> <%--Edited for FB 1425 MOJ--%>
                        <td></td>
                        <td align="left" class="blackblodtext">繰り返しパターン</td>
                        <td valign="top">
                            <asp:Label ID="plblConfRecurrance" runat="server" Text="再発セットません"></asp:Label>
                        </td>
                    </tr>
                    <%if (!(client.ToString().ToUpper() == "MOJ"))
                      {%> <%--Edited For FB 1425--%>
                    <tr>
                        <td></td>
                        <td align="left" class="blackblodtext">参加者</td>
                        <td valign="top">
                        <%--FB 2234 Start--%>
                        <div id="Div3" style="width:500px;overflow-y:auto;overflow-x:auto; word-break:break-all;" runat="server">
                            <asp:Label ID="plblPartys" runat="server"></asp:Label>
                            </div>
                             <%--FB 2234 End--%>
                        </td>
                    </tr>
                    <%} %><%--Edited For FB 1425--%>
                    <tr id="traudiobrdige" runat="server"><%--FB 2359--%><%--FB 2443--%>
                        <td></td>
                        <td align="left" class="blackblodtext" valign="top">オーディオブリッジ</td>
                        <td valign="top">
                        <div id="Div4" style="width:500px;overflow-y:auto;overflow-x:auto; word-break:break-all;" runat="server">
                            <asp:Label ID="lblAudioBridge" runat="server"></asp:Label>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td align="left" class="blackblodtext">場所</td>
                        <td valign="top">
                            <asp:Label ID="plblLocation" runat="server"></asp:Label>
                        </td>
                    </tr>
                     <%--FB 2426 Start--%>
                     <tr id="trPreviewGuestLocation" runat="server">
                        <td></td>
                        <td align="left" class="blackblodtext">ゲストレンタル</td>
                        <td valign="top">
                            <asp:Label ID="plblGuestLocation" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <%--FB 2426 End--%>
					<%--FB 2446 - Start--%>
                    <tr id="trPublicOpen" runat="server"> <%-- Code Modified For MOJ Phase2 --%> 
                        <td></td>                   
                        <td id="tdpublic1" runat="server" align="left" class="blackblodtext">公開</td>
                        <td id="tdpubopen" runat="server" align="left" class="blackblodtext">公開/登録受付中</td>
                        <td valign="top">
                            <asp:Label ID="plblPublic" runat="server"></asp:Label>
                            <asp:Label ID="plblSlash" Text="/" runat="server" />
                            <asp:Label ID="plblOpenForRegistration" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <%--FB 2446 - End--%>
                     <%if (!(client.ToString().ToUpper() == "MOJ"))
                       {%> <%--Edited For FB 1425--%>
                    <tr>
                        <td></td>
                        <td align="left" class="blackblodtext">iCal添付ファイルを送る</td>
                        <td valign="top">
                            <asp:Label ID="plblICAL" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <%} %><%--Edited For FB 1425--%>
                    <%--FB 2359--%>
                    <%--FB 2632 Starts--%>
                    <tr>
                       <td></td>
                       <td id="tdConcierge" valign="top" class="blackblodtext" runat="server" align="left">Concierge Support</td>
                       <td valign="top">
                       <label id="Label3" style="font-weight: normal;">On-Site A/V Support ></label>
                       <label id="plblOnsiteAV" runat="server" style="font-weight: normal; " ></label><br />
                       <label id="Label4" style="font-weight: normal; " >Meet and Greet ></label>
                       <label id="plblMeetandGreet" runat="server" style="font-weight: normal; "></label><br />
                       <label id="Label6" style="font-weight: normal; ">Concierge Monitoring ></label>
                       <label id="plblConciergeMonitoring" runat="server" style="font-weight: normal; " ></label><br />
                       <label id="lblDedicatedVNOC" runat="server" style="font-weight: normal; ">Dedicated VNOC Operator ></label>
                       <label id="plblDedicatedVNOC" runat="server" style="font-weight: normal; "></label>
                       </td>
                     </tr>
                     <%--FB 2632 End--%>
                     <%if (enableEntity == "1")
                     { %> 
                     <%--FB 2377--%>
                     <%--Custom Attribute Fixes--%>
                     <tr>
                        <td></td>
                        <td align="left" valign="top" class="blackblodtext">カスタマイズ情報</td> <%-- Changed for FB 1718 --%>
                        <td valign="top">
                            <asp:Label ID="plblCustomOption" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <%} %>
                    <tr id="trPLBLAVWO">
                        <td></td>
                        <td align="left" class="blackblodtext">オーディオ/ビジュアルワークオーダー</td>
                        <td valign="top">
                            <asp:TextBox ID="plblAVWorkOrders" runat="server" ReadOnly="true" BackColor="Transparent" BorderStyle="None" BorderWidth="0px" Width="20px"></asp:TextBox>ワークオーダー
                            <asp:Label ID="plblAVInstructions" EnableViewState="false" runat="server" CssClass="lblError" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr id="trPLBLCATWO">
                        <td></td>
                        <td align="left" class="blackblodtext">ケータリングワークオーダー</td>
                        <td valign="top">
                            <asp:TextBox ID="plblCateringWorkOrders" runat="server" ReadOnly="true" BackColor="Transparent" BorderStyle="None" BorderWidth="0px" Width="20px"></asp:TextBox>ワークオーダー
                            <asp:Label ID="plblCATInstructions" runat="server" CssClass="lblError" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr id="trPLBLHKWO">
                        <td></td>
                        <td align="left" class="blackblodtext">ハウスキーピングワークオーダー</td>
                        <td valign="top">
                            <asp:TextBox ID="plblHouseKeepingWorkOrders" runat="server" ReadOnly="true" BackColor="Transparent" BorderStyle="None" BorderWidth="0px" Width="20px"></asp:TextBox>ワークオーダー
                            <asp:Label ID="plblHKInstructions" runat="server" CssClass="lblError" Text=""></asp:Label>
                        </td>
                    </tr>
                    <%-- Changed for FB 1926 --%>
                    <tr>
                        <td></td>
                        <td align="left" class="blackblodtext">自動化された参加者のリマインダを送信する</td>
                        <td valign="top">
                            <asp:CheckBox ID="chkReminder" runat="server"></asp:CheckBox>
                        </td>
                    </tr>
                    
                    <tr><td colspan="3" align="center">
                    <asp:Table runat="server" Width="100%" CellSpacing="5" cellpadding="2" id="tblConflict" Visible="False">
                    <asp:TableRow ID="TableRow11" runat="server"><asp:TableCell ID="TableCell8" HorizontalAlign="Center" runat="server">
                           <h4>以下の競合を解決する:</h4>
                    </asp:TableCell></asp:TableRow>
                    <asp:TableRow ID="TableRow12" runat="server"><asp:TableCell ID="TableCell9" HorizontalAlign="Center" runat="server">
                    <!-- Code changed for FB 1073
                                    <asp:BoundColumn DataField="startDate" HeaderText="Date">
                                   -->  
                        <asp:DataGrid Width="80%" ID="dgConflict" runat="server" AutoGenerateColumns="False" OnItemDataBound="InitializeConflict" style="border-collapse:separate"> <%--Edited for FF--%>
                                <Columns>
                                    <asp:BoundColumn DataField="formatDate" HeaderText="日付">
                                        <HeaderStyle CssClass="tableHeader" />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="conflict" HeaderText="競合" ItemStyle-Wrap="false">
                                        <HeaderStyle CssClass="tableHeader" />
                                    </asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="時間を開始">
                                        <HeaderStyle CssClass="tableHeader" />
                                        <ItemTemplate><%--window dressing--%>
                                            <mbcbb:combobox CssClass="altSelectFormat" runat="server" id="conflictStartTime" DataValueField='<%# DataBinder.Eval(Container, "DataItem.startHour") %>' DataTextField='<%# DataBinder.Eval(Container, "DataItem.startHour") %>' style="width:auto"><%--Edited for FF--%>
                                                <asp:ListItem Value="01:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="02:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="03:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="04:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="05:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="06:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="07:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="08:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="09:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="10:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="11:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="12:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="01:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="02:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="03:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="04:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="05:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="06:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="07:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="08:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="09:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="10:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="11:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="12:00 AM"></asp:ListItem>
                                            </mbcbb:combobox>
                                        
                                    <%--Code added for FB 1426--%>
                                    <asp:RequiredFieldValidator ID="ReqConflictTime" runat="server" ControlToValidate="conflictStartTime"
                                        Display="Dynamic" ErrorMessage="必須"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegConflictTime" runat="server" ControlToValidate="conflictStartTime"
                                        Display="Dynamic" ErrorMessage="無効な時間 (HH:MM AM/PM)" ValidationExpression="[0|1][0-9]:[0-5][0-9] ?[a|A|p|P][M|m]"></asp:RegularExpressionValidator> <%--FB 1715--%>
                                    <%--Code added for FB 1426--%>
                                    </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <%--code added for buffer zone --Start--%>                                    
                                     <asp:TemplateColumn>
                                     <HeaderTemplate> <%--buffer zone --%>
                                      セットアップ時間
                                     </HeaderTemplate>
                                        <ItemTemplate>
                                            <mbcbb:combobox CssClass="altSelectFormat" runat="server" id="conflictSetupTime" style="width:auto"><%--Edited for FF--%>
                                                <asp:ListItem Value="01:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="02:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="03:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="04:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="05:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="06:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="07:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="08:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="09:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="10:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="11:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="12:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="01:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="02:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="03:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="04:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="05:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="06:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="07:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="08:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="09:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="10:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="11:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="12:00 AM"></asp:ListItem>
                                            </mbcbb:combobox>
                                    <asp:RequiredFieldValidator ID="ReqConflictSetupTime" runat="server" ControlToValidate="conflictSetupTime"
                                        Display="Dynamic" ErrorMessage="必須"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegConflictSetupTime" runat="server" ControlToValidate="conflictSetupTime"
                                        Display="Dynamic" ErrorMessage="無効な時間 (HH:MM AM/PM)" ValidationExpression="[0|1][0-9]:[0-5][0-9] ?[a|A|p|P][M|m]"></asp:RegularExpressionValidator><%--FB 1715--%>
                                    </ItemTemplate>
                                    </asp:TemplateColumn>
                                     <asp:TemplateColumn>
                                        <HeaderTemplate> <%--buffer zone--%>
                                            テアダウン時間  
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <mbcbb:combobox CssClass="altSelectFormat" runat="server" id="conflictTeardownTime" style="width:auto"><%--Edited for FF--%>
                                                <asp:ListItem Value="01:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="02:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="03:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="04:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="05:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="06:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="07:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="08:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="09:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="10:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="11:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="12:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="01:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="02:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="03:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="04:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="05:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="06:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="07:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="08:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="09:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="10:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="11:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="12:00 AM"></asp:ListItem>
                                            </mbcbb:combobox>
                                    <asp:RequiredFieldValidator ID="ReqConflictTeardownTime" runat="server" ControlToValidate="conflictTeardownTime"
                                        Display="Dynamic" ErrorMessage="必須"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegConflictTeardownTime" runat="server" ControlToValidate="conflictTeardownTime"
                                        Display="Dynamic" ErrorMessage="無効な時間 (HH:MM AM/PM)" ValidationExpression="[0|1][0-9]:[0-5][0-9] ?[a|A|p|P][M|m]"></asp:RegularExpressionValidator><%--FB 1715--%>
                                    </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <%--code added for buffer zone --End--%>
                                    <asp:TemplateColumn HeaderText="終了時間">
                                        <ItemTemplate>
                                            <mbcbb:combobox CssClass="altSelectFormat" runat="server" id="conflictEndTime" style="width:auto"><%--Edited for FF--%>
                                                <asp:ListItem Value="01:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="02:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="03:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="04:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="05:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="06:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="07:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="08:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="09:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="10:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="11:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="12:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="01:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="02:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="03:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="04:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="05:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="06:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="07:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="08:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="09:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="10:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="11:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="12:00 AM"></asp:ListItem>
                                            </mbcbb:combobox>
                                    <asp:RequiredFieldValidator ID="ReqConflictEndTime" runat="server" ControlToValidate="conflictEndTime"
                                        Display="Dynamic" ErrorMessage="必須"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegConflictEndTime" runat="server" ControlToValidate="conflictEndTime"
                                        Display="Dynamic" ErrorMessage="無効な時間 (HH:MM AM/PM)" ValidationExpression="[0|1][0-9]:[0-5][0-9] ?[a|A|p|P][M|m]"></asp:RegularExpressionValidator><%--FB 1715--%>
                                    </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="アクション">
                                        <HeaderStyle CssClass="tableHeader" />
                                        <ItemTemplate>
                                            <asp:Button runat="server" ID="btnViewConflict" CssClass="altShortBlueButtonFormat" Text="見る"/>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="startHour" Visible="False"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="startMin" Visible="False"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="startSet" Visible="False"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="durationMin" Visible="False"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="startDate" Visible="False"></asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="削除">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkConflictDelete" runat="server" />
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="tableHeader" />
                                    </asp:TemplateColumn>
                                </Columns>
                            <AlternatingItemStyle  CssClass="tablebody"  />
                            <ItemStyle CssClass="tableBody" />
                            <HeaderStyle CssClass="tableHeader" />
                            </asp:DataGrid>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>
                        </td>
                    </tr>
                    <tr><td colspan="3" align="center">
                        <%-- Code Added for FB 1428--%>
                        <input type='submit' name='SoftEdgeTest1' style='max-height:0px;max-width:0px;height:0px;width:0px;display:none'/><%--edited for FF--%>
                        <asp:Button runat="server" CssClass="altLongBlueButtonFormat" ID="btnConfSubmit" OnClientClick="javascript:DataLoading(1);SetConference()" Text="会議を送信する" />
                    </td></tr>
                      
                </table>
                <script language="javascript"> 
                if(document.getElementById("hdnCrossroomModule").value != null)
 	                roomModule_Session = document.getElementById("hdnCrossroomModule").value;
                 else
 	                roomModule_Session = "<%=Session["roomModule"] %>";
                 	
                 if(document.getElementById("hdnCrossfoodModule").value != null)
 	                foodModule_Session = document.getElementById("hdnCrossfoodModule").value;
                 else
 	                foodModule_Session = "<%=Session["foodModule"] %>";
                 	
                 if(document.getElementById("hdnCrosshkModule").value != null)
 	                hkModule_Session = document.getElementById("hdnCrosshkModule").value;
                 else
 	                hkModule_Session = "<%=Session["hkModule"] %>"; 
 	                
                    if (roomModule_Session != "1" || "<%=Session["admin"] %>" == "0") //FB 2274
                    {
                        document.getElementById("trPLBLAVWO").style.display="none";
                    }
                    if (hkModule_Session != "1" || "<%=Session["admin"] %>" == "0")  //FB 2274
                    {
                        document.getElementById("trPLBLHKWO").style.display="none";
                    }
                    
                    if (foodModule_Session != "1" || "<%=Session["admin"] %>" == "0")  //FB 2274
                        document.getElementById("trPLBLCATWO").style.display="none";
                </script> <%--                <script language="javascript">
                    for (i=0;i<15;i++)
                    {
                        if (document.getElementById("SideBarContainer_SideBarList_ctl0" + i + "_SideBarButton") != null)
                        {
                            document.getElementById("SideBarContainer_SideBarList_ctl0" + i + "_SideBarButton").href = document.getElementById("SideBarContainer_SideBarList_ctl0" + i + "_SideBarButton").href.replace("javascript:", "javascript:CheckFiles();");
                            //alert(document.getElementById("SideBarContainer_SideBarList_ctl0" + i + "_SideBarButton").href);
                        }
                    }
                    if (document.getElementById("FinishNavigationTemplateContainerID_FinishPreviousButton"))
                        document.getElementById("FinishNavigationTemplateContainerID_FinishPreviousButton").style.display="none";
                </script>
--%>
                                    </asp:Panel>
                                </asp:View>
                            </asp:MultiView>
    </div>
<script language="javascript">

                    for (i=0;i<15;i++) //FB 1039
                    {
                        if (document.getElementById("TopMenun" + i) != null)
                        {
                       // alert(document.getElementById("TopMenun"));
                            document.getElementById("TopMenun" + i).innerHTML = document.getElementById("TopMenun" + i).innerHTML.replace("javascript:", "javascript:if (CheckFiles()) ");
                        }
                    }

</script>
    <asp:TextBox ID="HKDefaultRoomID" Text="-1" Visible="false" runat="server"></asp:TextBox>
    <asp:TextBox ID="HKDefaultSetID" Text="0" Visible="false" runat="server"></asp:TextBox>
    <asp:TextBox ID="HKDefaultQuantity" Text="1" Visible="false" runat="server"></asp:TextBox>
    <asp:TextBox ID="txtTimeCheck" Text="" Visible="false" runat="server"></asp:TextBox>
    <asp:TextBox ID="txtModifyType" Text="" Visible="false" runat="server"></asp:TextBox>
    <input type="hidden" id="txtdgUsers" runat="server" />
    <input type="hidden" id="txtLecturer" runat="server" />
                        <div id="divPic" style="display:none;">
                            <img src="" name="myPic" width="200" height="200">
                        </div>
                            
                    </div>
            </td>
        </tr>
        <%--Window Dressing--%>
        <tr class="tabContents">
            <td align="center">
                <table cellpadding="10" cellspacing="10" border="0" width="80%">
                    <tr>
                        <td align="right" width="50%">
                            <asp:Button ID="btnPrev" runat="server" CssClass="altShortBlueButtonFormat" Text="&lt;&lt; 前" OnClientClick="javascript:return CheckFiles();" OnClick="MoveBack" /> 
                        &nbsp;</td>
                        <td align="left" width="50%">
                            <asp:Button ID="btnNext" runat="server" CssClass="altShortBlueButtonFormat" Text="次のページ &gt;&gt;" OnClientClick="javascript:document.getElementById('hdnValue').value ='N';return CheckFiles('N');" OnClick="MoveNext" /> 
                        &nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </div>    
    </form>
    <%--Code added for FB 1308 start--%>
    <script type="text/javascript">
        if(document.getElementById("confStartTime_Text"))
        {
            var confstarttime_text = document.getElementById("confStartTime_Text");
            confstarttime_text.onblur  =  function()
            {
                document.getElementById('hdnChange').value = 'ST'; //FB 1716
                if(formatTime('confStartTime_Text','regStartTime')) //FB 1715
                    ChangeEndDate(0);
            };
        }
        if(document.getElementById("confEndTime_Text"))
        {
            var confendtime_text = document.getElementById("confEndTime_Text");
            confendtime_text.onblur  =  function()
            {
                document.getElementById('hdnChange').value = 'ET'; //FB 1716
                if(formatTime('confEndTime_Text','regEndTime')) //FB 1715
                    ChangeStartDate(0);
            };
        }
        
        if(document.getElementById("SetupTime_Text"))
        {
            var setuptime_text = document.getElementById("SetupTime_Text");
            setuptime_text.onblur  =  function()
            {
                document.getElementById('hdnChange').value = 'SU'; //FB 1716
                if(formatTime('SetupTime_Text', 'regSetupStartTime')) //FB 1715
                    ChangeEndDate(0);
            };
        }
       if(document.getElementById("TeardownTime_Text"))
        {
            var teardowntime_text = document.getElementById("TeardownTime_Text");
            teardowntime_text.onblur  =  function()
            {
                document.getElementById('hdnChange').value = 'TD'; //FB 1716
                if(formatTime('TeardownTime_Text','regTearDownStartTime')) //FB 1715
                    ChangeStartDate(0);
            };
        }
      //<%--FB 1481 Start--%>  
      if(document.getElementById("<%=rdSelView.ClientID%>"))
      {
        if(!document.getElementById("<%=rdSelView.ClientID%>").disabled)
            document.getElementById("<%=rdSelView.ClientID%>").onclick= function()
            { DataLoading(1);};
      }      
      <%--FB 1481 End--%>  
    </script>
    <%--Code added for FB 1308 end--%>
    
    <%--Merging Recurrence start--%>
    <script type="text/javascript"> 
	  
	  if(document.getElementById("hdnValue").value == "" || document.getElementById("hdnValue").value == "0")
	  {
	    if (document.frmSettings2.EndText)
		    document.frmSettings2.EndText.disabled = true;
	    if (document.frmSettings2.DurText)
		    document.frmSettings2.DurText.disabled = true;

	    document.frmSettings2.RecurValue.value = document.getElementById("Recur").value;
    	
	    if (document.getElementById("ModifyType") != null)// Added for FF
	    if (document.getElementById("ModifyType").value=="3") {
		    document.getElementById("RemoveRecurDiv").style.display = "none";
	    }
        
         var chkrecurrence = document.getElementById("chkRecurrence");
         
        if(document.getElementById("Recur").value != "" || (chkrecurrence && chkrecurrence.checked == true))
        {
            var chkrecurrence = document.getElementById("chkRecurrence");
            if(chkrecurrence)
                chkrecurrence.checked = true;
            document.getElementById("hdnRecurValue").value = 'R'
            initial();
           
            fnShow();
        }
        
         fnEnableBuffer();
        
     }
        //Added for FF Fix START
        if(document.getElementById("lstDuration_Container") != null)
        document.getElementById("lstDuration_Container").style.width = "auto";
        
        if(document.getElementById("confStartTime_Container") != null)
        document.getElementById("confStartTime_Container").style.width = "auto";
        
        if(document.getElementById("confEndTime_Container") != null)
        document.getElementById("confEndTime_Container").style.width = "auto";
        
        if(document.getElementById("SetupTime_Container") != null)
        document.getElementById("SetupTime_Container").style.width = "auto";
        
        if(document.getElementById("TeardownTime_Container") != null)
        document.getElementById("TeardownTime_Container").style.width = "auto";
        
        if(document.getElementById("completedByTime_Container") != null)
        document.getElementById("completedByTime_Container").style.width = "auto";
        
        if(document.getElementById("dgConflict_conflictStartTime_Container") != null)
        document.getElementById("dgConflict_conflictStartTime_Container").style.width = "auto";
        
        if(document.getElementById("startByTime_Container") != null)
        document.getElementById("startByTime_Container").style.width = "auto";
        
       //Commented for FB 1722 - Start
//        if(navigator.appName != "Microsoft Internet Explorer")
//	{ 
//	 if(document.getElementById("tabCelldur") != null)
//            document.getElementById("tabCelldur").style.width = "35%";
//	}
//       else
//	{             
//            if(document.getElementById("tabCelldur") != null)
//            document.getElementById("tabCelldur").style.width = "36%";
//	}
       //Commented for FB 1722 - End       
        //Added for FF Fix END
	//FB 1734 - Method getAudioparticipantListNET moved to settings2.js
	
	 //FB 1830
    function ClientValidate(source, arguments)
    {     
        var cFor = '<%=cFormat%>';
        var strValidation = "";

        if(cFor == "€")
            strValidation = /^\d+$|^\d+\,\d{1,2}$/ ;
        else
            strValidation = /^\d+$|^\d+\.\d{1,2}$/ ;

        if (!arguments.Value.match(strValidation ) ) 
            arguments.IsValid = false       
    }
    //FB 2274 Starts
    var isSpecialRecur_Session ;
 		
    if(document.getElementById("hdnCrossisSpecialRecur").value != null)
 	    isSpecialRecur_Session = document.getElementById("hdnCrossisSpecialRecur").value;
    else
 	    isSpecialRecur_Session = "<%=Session["isSpecialRecur"] %>";
    //FB 2274 Ends
    
    //FB 1911 - Start
    if ('<%=isEditMode%>' == "1" || isSpecialRecur_Session == "0") //FB 2052 FB 2274
    {
        if(document.getElementById("SPCell1"))
            document.getElementById("SPCell1").style.display = 'None'
        
        if(document.getElementById("SPCell2"))
            document.getElementById("SPCell2").style.display = 'None'
    }    
    
	if(document.getElementById("hdnValue").value == "" || document.getElementById("hdnValue").value == "0")
	{
        if(document.getElementById("RecurSpec").value != "")
        {
           showSpecialRecur();
           document.getElementById("RecurText").value = document.getElementById("RecurringText").value;
        }
    }
    
    //FB 1911 - End
    
    //Firefox Remember Password Issue
    function clearValues()
    {
        if(document.getElementById('ConferencePassword2') != null)//FB 2164
        {
           if(document.getElementById('ConferencePassword2').value =="")
           {
            document.getElementById('ConferencePassword').value="";
           }
       }
    }

   //function triggerClear() // Covered on FB 2050
   //{
     //setTimeout("clearValues()",500);
   //}
   //window.onload=triggerClear;
    </script>
    <%--Merging Recurrence end--%>
    
     <%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
<!-- FB 2050 Start -->
<script type="text/javascript">
    //FB 2426 Start
    function fnchangetype() {
        document.getElementById('txtIPPassword').type = "password";
        document.getElementById('txtIPconfirmPassword').type = "password";
        document.getElementById('txtISDNPassword').type = "password";
        document.getElementById('txtISDNconfirmPassword').type = "password";
        document.getElementById('txtSIPPassword').type = "password";
        document.getElementById('txtSIPconfirmPassword').type = "password";

    }
    //FB 2426 End

    function refreshImage() {
        fnHideStartNow(); //FB 1825
        fnchangetype(); //FB 2426
  //triggerClear();
  setTimeout("clearValues()",500);
  var obj = document.getElementById("mainTop");
  if(obj != null)
  {
      var src = obj.src;
      var pos = src.indexOf('?');
      if (pos >= 0) {
         src = src.substr(0, pos);
      }
      var date = new Date();
      obj.src = src + '?v=' + date.getTime();
      
      if(obj.width > 804)
      obj.setAttribute('width','804');
  }
  //refreshStyle(); // Commented for Refresh Issue
  setMarqueeWidth();
  return false;
}

function refreshStyle()
{
	var i,a,s;
	a=document.getElementsByTagName('link');
	for(i=0;i<a.length;i++) {
		s=a[i];
		if(s.rel.toLowerCase().indexOf('stylesheet')>=0&&s.href) {
			var h=s.href.replace(/(&|\\?)forceReload=d /,'');
			s.href=h+(h.indexOf('?')>=0?'&':'?')+'forceReload='+(new Date().valueOf());
		}
	}
}

function setMarqueeWidth()
{
    var screenWidth = screen.width - 25;
    if(document.getElementById('martickerDiv')!=null)
        document.getElementById('martickerDiv').style.width = screenWidth + 'px';
        
    if(document.getElementById('marticDiv')!=null)
        document.getElementById('marticDiv').style.width = screenWidth + 'px';
    
    if(document.getElementById('marticker2Div')!=null)
        document.getElementById('marticker2Div').style.width = (screenWidth-15) + 'px';
    
    if(document.getElementById('martic2Div')!=null)
        document.getElementById('martic2Div').style.width = (screenWidth-15) + 'px';
}

window.onload = refreshImage;

function refreshIframe()
{
var iframeId = document.getElementById('ifrmPartylist');
iframeId.src = iframeId.src;
}

//FB 1825 Start
function fnHideStartNow()
{
    var startNow = document.getElementById('hdnSetStartNow').value;
    if (startNow == "hide" && chkrecurrence.checked == false) 
    {

        document.getElementById("NONRecurringConferenceDiv4").style.display = "none";
    }
}
//FB 1825 End
//FB 2274
if(document.getElementById('hdnCrossAddtoGroup').value != null)
{
    if(document.getElementById('hdnCrossAddtoGroup').value != "")
        document.getElementById('btnAddgrp').style.display = 'none'
}
//FB 2486
function fnCheck(arg) {
    var srcID = document.getElementById(arg);
    var ckboxName = "chkmsg";
    var ctrlIDNo = arg.substring(arg.length, arg.length - 1)
    var drpName = srcID.id.replace(ctrlIDNo, "");
    var pVal = srcID.getAttribute("PreValue");
    var ckboxSel = document.getElementById(ckboxName + "" + ctrlIDNo);

    if (ckboxSel && ckboxSel.checked == false)
        return true;

    if (pVal == null)
        pVal = srcID.options[0].text;

    for (var i = 1; i <= 9; i++) {
        var ckbox = document.getElementById(ckboxName + "" + i);
        if (ckbox) {
            if (i == ctrlIDNo)
                continue;

            if (ckbox.checked) {
                var destDrpName = document.getElementById(drpName + i);
                if (destDrpName) {
                    if (destDrpName.value == srcID.value) {
                        srcID.value = pVal;
                        alert("選択された時間はすでに他のメッセージのために定義され");
                        if (ckboxSel)
                            ckboxSel.checked = false;
                        return false;
                    }
                }
            }
        }
    }

    srcID.setAttribute("PreValue", srcID.value);
    return true;
}

    
</script>
<!-- FB 2050 End -->

</body>
</html>


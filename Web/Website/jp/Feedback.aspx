﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="en_Feedback" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
  <title>myVRM</title>
  <meta name="Description" content="myVRM (Videoconferencing Resource Management) is a revolutionary Web-based software application that manages any video conferencing environment." />
  <meta name="Keywords" content="VRM, myVRM, Videoconferencing Resource Management, video conferencing, video bridges, video endpoints" />
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  
  <script type="text/javascript" src="script/errorList.js"></script>
  <link rel="StyleSheet" href="css/headingStyles.css" type="text/css" />
  <link title="Expedite base styles" href="<%=Session["OrgCSSPath"]%>" type="text/css" rel="stylesheet" />
 
<script type="text/javascript">
<!--
function fnValidate()
{
	if ((document.getElementById("TxtName").value) == "") {
		//alert(EN_89);
		document.getElementById("TxtName").value= "";
		document.getElementById("TxtName").focus();
		return (false);		
	}

	if ( !checkemail(document.getElementById("TxtEmail").value) ) {
		 alert("有効なEメールアドレスを入力してください。");
		document.getElementById("TxtEmail").focus();
		return(false);
	}

	if ((document.getElementById("TxtSubject").value) == "") {
		 alert("件名を入力してください。");
		document.getElementById("TxtSubject").value = "";
		document.getElementById("TxtSubject").focus();
		return (false);		
	}

	if ((document.getElementById("TxtComment").value) == "") {
		alert("コメントを入力してください。");
		document.getElementById("TxtComment").value = "";
		document.getElementById("TxtComment").focus();
		return (false);		
	}
	
	return true;
}

//-->
</script>

  <script language="javascript">
  <!--
	
	function errorHandler( e, f, l ){
	    alert("エラーがこのページではJavaScriptで発生しました.\nFile: " + f + "\nLine: " + l + "\nError:" + e);
		return true;
	}
	
  //-->
  </script>

</head>

<body bottommargin="0" leftmargin="5" rightmargin="3" topmargin="8" marginheight="0" marginwidth="0">
<script language="JavaScript" src="inc\functions.js"></script>

<%--CONTENT START HERE --%>
 <asp:Label ID="LblError" runat="server" Text="" CssClass="lblError"></asp:Label> <%--Edited for Login Management--%> 
    <form name="frmFeedback" runat="server" method="POST">
     <input type="hidden" name="cmd" value="Feedback" />
      <input type="hidden" name="parentpage" runat="server" id="parentpage"/>
        <table border="0" cellspacing="2" cellpadding="2" width="95%" >
          <tr>
            <td vAlign="bottom" height="20" colspan="2" align="left">
            <%--Window Dressing--%>
              <h3>&nbsp;フィードバック</h3>
            </td>
          </tr>
          <tr>
            <td vAlign="top" height="8" align="left" colspan="2">
              <hr noShade SIZE="1">
            </td>
          </tr>          
          <tr>
            <td height="21" class="blackblodtext" colspan="2">
              &nbsp;の一部を共有するために数分を割いてください。 <br />
              &nbsp;あなたの貴重な提案やコメント 
              このサイトについて.
            </td>
          </tr>
          <tr>
            <td></td>
            <td align="right">
              <SPAN class="reqfldText">
              * 必須欄
              </SPAN>
            </td>
          </tr>
          <tr>
            <td align="right">
            <%--Window Dressing--%>
              <div class="blackblodtext">へ:</div>
            </td>
            <td class="headingBlue">myVRM サポート</td>
          </tr>
          <tr>
            <td align="right">
            <%--Window Dressing--%>
              <div class="blackblodtext">の名前:</div>
            </td>
            <td vAlign="top">
                <asp:TextBox ID="TxtName" runat="server" CssClass="altText"  size="30" ReadOnly="true"></asp:TextBox>                               
            </td>
          </tr>
          <tr>
            <td align="right">
            <%--Window Dressing--%>
              <div class="blackblodtext">メール:
              <SPAN class="reqfldText">*</SPAN>
              </div>
            </td>
            <td>
                <asp:TextBox ID="TxtEmail" runat="server" CssClass="altText"  size="30" MaxLength="512"></asp:TextBox>                               
            </td>
          </tr>
          <tr>
            <td align="right">
            <%--Window Dressing--%>
              <div class="blackblodtext">件名:
              <SPAN class="reqfldText">*</SPAN>
              </div>
            </td>
            <td>
                <asp:TextBox ID="TxtSubject" runat="server" CssClass="altText"  size="30" MaxLength="2000"></asp:TextBox>                                              
            </td>
          </tr>
          <tr>
            <td align="right" valign="top">
            <%--Window Dressing--%>
              <div class="blackblodtext">コメント:
              <SPAN class="reqfldText">*</SPAN>
              </div>
            </td>
            <td>
            <asp:TextBox ID="TxtComment" CssClass="altText" TextMode="MultiLine" Rows="4" runat="server" MaxLength="4000"></asp:TextBox> 
            </td>
          </tr>
          <tr><td colspan="2"></td></tr>
          <tr>
            <td colspan=2 align="right" valign="bottom" >
                <%--code added for Soft Edge button - start--%>                
                <input type="submit" name="SoftEdgeTest" style='max-height:0px;max-width:0px;height:0px;width:0px;display:none'/><%--Edited for FF--%>
                <input type="button" onfocus="this.blur()" value="キャンセル" name="frmFeedback" class="altShortBlueButtonFormat" onClick="Javascript: window.close();"/>
                <asp:Button ID="BtnSubmit" onfocus="this.blur()" OnClick="SubmitFeedback" runat="server"   Text="送信" CausesValidation="True" CssClass="altShortBlueButtonFormat" />
                <%--code added for Soft Edge button - end--%>                
            </td>
          </tr>
        </table>
    </form>
<script language="javascript">
<!--
//	document.body.style.background = "";
	document.body.style.margin = "3px";
	document.body.style.bgcolor = "ghostwhite";
	document.body.style.overflow = "hidden";
	
	document.getElementById("parentpage").value = opener.window.location.href;
	
	window.resizeTo(450,450); //Edited for FF
	
//-->
</script>
 
</body>
</html>
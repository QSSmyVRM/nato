﻿<%@ Page Language="C#" Inherits="ns_EditInventory.EditInventory" EnableViewStateMac="false" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <!-- FB 2050 -->
<meta http-equiv="X-UA-Compatible" content="IE=7" /> <!-- FB 2050 -->
<!--Window Dressing-->
<!-- #INCLUDE FILE="inc/maintopNET.aspx" --> 
<script language="JavaScript" src="inc\functions.js"></script>
<script language="JavaScript" src="script\mytreeNET.js"></script>
<script type="text/javascript" src="script/mousepos.js"></script>
<script type="text/javascript" src="inc/functions.js"></script>
<script type="text/javascript" src="script/roomsearch.js"></script>
<script language="javascript">

function ValidateSubmit(val)
{
    DataLoading(val);
    if (typeof(Page_ClientValidate) == 'function') 
    if (!Page_ClientValidate())
    {
        DataLoading(0);
        return false;
    }
}

function ShowImage(obj)
{
    //alert(obj.src);
    document.getElementById("myPic").src = obj.src;
    //getMouseXY();
    //alert(document.body.scrollHeight);
    document.getElementById("divPic").style.position = 'absolute';
    document.getElementById("divPic").style.left = mousedownX + 20;
    document.getElementById("divPic").style.top = mousedownY;
    document.getElementById("divPic").style.display="";
    //alert(obj.style.height + " : " + obj.style.width);
}

function HideImage()
{
    document.getElementById("divPic").style.display="none";
}

function getYourOwnEmailList (i)
{
//	url = "dispatcher/conferencedispatcher.asp?frm=approverNET&frmname=frmSubmit&no=" + i + "&cmd=GetEmailList&emailListPage=1&wintype=pop";//Login Management
	url = "emaillist2main.aspx?t=e&frm=approverNET&fn=frmSubmit&n=" + i;//Login Management
	
	if (!window.winrtc) {	// has not yet been defined
		winrtc = window.open(url, "", "width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
		winrtc.focus();
	} else // has been defined
	    if (!winrtc.closed) {     // still open
	    	winrtc.close();
	        winrtc = window.open(url, "", "width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
			winrtc.focus();
		} else {
	        winrtc = window.open(url, "", "width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
	        winrtc.focus();
		}
}

function OpenItemsList(srcID)
{
    url = "ItemsList.aspx?type=" + document.getElementById("<%=txtType.ClientID %>").value + "&srcID=" + srcID.id;
	if (!window.winrtc) {	// has not yet been defined
		winrtc = window.open(url, "", "width=700,height=550,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
		winrtc.focus();
	} else // has been defined
	    if (!winrtc.closed) {     // still open
	    	winrtc.close();
	        winrtc = window.open(url, "", "width=700,height=550,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
			winrtc.focus();
		} else {
	        winrtc = window.open(url, "", "width=700,height=550,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
	        winrtc.focus();
		}
}

function DeleteItem(rownum)
{
    if (confirm("あなたは、AVのセットからこの項目を削除してよろしいです?"))
    {
        //alert(document.getElementById("selItems").value + " : " + document.getElementById("itemName" + rownum).value + ":" + document.getElementById("itemImg" + rownum).value + ",");
        document.getElementById("selItems").value = document.getElementById("selItems").value.replace(document.getElementById("itemName" + rownum).value + ":" + document.getElementById("itemImg" + rownum).value + ",", "");
        document.getElementById("itemName" + rownum).value = "Deleted";
        document.getElementById("itemQuantity" + rownum).value = "2";
        document.getElementById("itemRow" + rownum).style.display = "none";
        document.getElementById("itemDeleted" + rownum).checked = "true";
    }
}

function frmSubmit()
{
    document.frmSubmit.action = document.location.href + "&submit=1";
    document.frmSubmit.submit();
}
//FB 1830
function ValidateNumeric()
 {      
    var args = ValidateNumeric.arguments;
     var cFor = '<%=currencyFormat%>';
     var strValidation = "";
     
     if(cFor == "€")
        strValidation = /^\d+$|^\d+\,\d{1,2}$/ ;
     else
        strValidation = /^\d+$|^\d+\.\d{1,2}$/ ;
     
     if (!args[0].value.match(strValidation) ) 
     {        
        if(document.getElementById(args[1]))
            document.getElementById(args[1]).style.display ="block";
        else
        {
            var ctrlCus = args[0].id.split('_');
            if(ctrlCus.length > 0)            
                args[1] = ctrlCus[0] + '_' + ctrlCus[1] + '_' + args[1];
             
            if(document.getElementById(args[1]))
                document.getElementById(args[1]).style.display ="block";
        }
            
        //alert("Invalid value");
        args[0].focus();
     }

 }
function ValidateInteger(ctrl)
 {     
     if (!ctrl.value.match(/^\d+$/) ) 
     {
//        ctrl.value = "0";
        alert("無効な値");
        ctrl.focus();
     }

 }
 //FB 1830
  function ClientValidate(source, arguments)
  {     
     var cFor = '<%=currencyFormat%>';
     var strValidation = "";
     
     if(cFor == "€")
        strValidation = /^\d+$|^\d+\,\d{1,2}$/ ;
     else
        strValidation = /^\d+$|^\d+\.\d{1,2}$/ ;
     
     if (!arguments.Value.match(strValidation ) ) 
        arguments.IsValid = false       
   }

</script>
<body>
 <form id="frmSubmit" runat="server" method="Post" onsubmit="return true">
     <input name="locstrname" type="hidden" id="locstrname" runat="server"  /> <!--Added room search-->
 <center>
        <h3>
            <asp:Label ID="lblType" runat="server" Text="Label"></asp:Label>
        </h3>
            <%--Window Dressing--%>
            <asp:TextBox ID="txtType" runat="server" BackColor="White" BorderColor="White" BorderStyle="None" ForeColor="White" Width="0px"></asp:TextBox>
        <asp:HiddenField id="AVInventoryID" runat="server"  />
            <asp:DropDownList Visible="false" ID="lstDeliveryTypes" DataTextField="Name" DataValueField="ID" runat="server"></asp:DropDownList>
            <input type="hidden" id="helpPage" value="40">
 </center>
    <div>
        <input type="hidden" id="selectedloc" runat="server" />
        <table width="100%" border="0">
            <tr>
                <td align="center">
                    <asp:Label ID="errLabel" runat="server" CssClass="lblError" Visible="False"></asp:Label>
                    <div id="dataLoadingDIV" style="z-index:1"></div>
               </td>
        </table>
    </div>
    <center>
    <table cellspacing="1" cellpadding="0" width="100%" border="0">
        <tr>
          <td>
            <table width="100%" height="100%" cellpadding="0" cellspacing="2" border="0">
                <tr>
                    <td valign="top" style="width:40%">
                        <table width="100%" cellpadding="5" cellspacing="2">
                            <tr>
                                <td valign="top" align="right" class="blackblodtext" width="50%"><b>の名前</b><span class="reqfldstarText">*</span></td>
                                <td valign="top" align="left" style="width: 30%" colspan="2"> <%--WO Bug List--%>
                                    <asp:TextBox ID="txtInvName" runat="server" CssClass="altText"></asp:TextBox>
                                     <asp:RequiredFieldValidator ID="reqInvName" runat="server" ControlToValidate="txtInvName"
                                         ErrorMessage="必須" Display="Dynamic" ValidationGroup="Submit"></asp:RequiredFieldValidator>
                                     <asp:RegularExpressionValidator ID="regInvName" ValidationGroup="Submit" ControlToValidate="txtInvName" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br> & < > ' + % \ ; ? | ^ = ! ` [ ] { } : # $ @ ~ &#34; は無効な記号です" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()'~]*$"></asp:RegularExpressionValidator>  
                                </td>
                            </tr>
                            <tr>
                                <td align="right" valign="top" class="blackblodtext">
                                    <b>管理責任者</b><span class="reqfldstarText">*</span></td>
                                <td align="left" valign="top" style="width: 60%" colspan="2"> <%--WO Bug List--%>
                                    <asp:TextBox ID="txtApprover1" runat="server" CssClass="altText" EnableViewState="true"></asp:TextBox>&nbsp;
                                    <a href="javascript: getYourOwnEmailList(0);" onmouseover="window.status='';return true;"><img ID="Img1" border="0" src="image/edit.gif" /></a> 
                                    <asp:RequiredFieldValidator ValidationGroup="Submit" ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtApprover1"
                                        ErrorMessage="必須" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                    <asp:TextBox ID="hdnApprover1" runat="server" BackColor="White" BorderColor="White"
                                        BorderStyle="None" Height="0px" Width="0px"></asp:TextBox>
                                    </td>                                
                            </tr>
                            <tr>
                            <%--Window Dressing--%>
                                <td align="right" class="blackblodtext" valign="top">
                                   <b>通知する</b></td>
                                <td align="left" valign="top" style="height: 20px; width: 30%;">
                                    <asp:CheckBox ID="chkNotify" runat="server" /></td>
                                <td align="left" style="height: 20px;" valign="top">
                                </td>
                            </tr>
                            <tr id="trComments1" runat="server">
                            <%--Window Dressing--%>
                                <td align="right" class="blackblodtext" valign="top">
                                    <b>コメント</b></td>
                                <td align="left" valign="top" style="width: 30%">
                                    <asp:TextBox ID="txtInvComments" runat="server" CssClass="altText" Rows="8" TextMode="MultiLine" Width="98%"></asp:TextBox>
                                     <asp:RegularExpressionValidator ValidationGroup="Submit" ID="regInvComments" ControlToValidate="txtInvComments" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br> & < > ' + % \ ; ? | ^ = ! ` [ ] { } : # $ @ ~ &#34; は無効な記号です" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                </td>   
                                <td align="left" valign="top">
                                    &nbsp; &nbsp;
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td align="left" valign="top" style="width: 500px">
                      <table border="0" cellpadding="3" cellspacing="0" width="100%">
                       <tr  style="display:none;">
                           <td align="right" valign="top" width="10%">
                           </td>
                           <td align="left" 
                               valign="top" width="78%">
                            <table border="0" style="width: 100%">
                                <tr>
                                    <td valign="top" align="left" width="80" id="tdCom"  runat="server"> <%--Edited for Loaction issues--%>
                                    <input type="button" value="比較" id="btnCompare" onclick="javascript:compareselected();" class="altShortBlueButtonFormat" style="width:100;height:20" runat="server"/>
                                    </td>
                                         <%--Window Dressing--%>
                                    <td valign="top" align="left" class="blackblodtext">
                                          <asp:RadioButtonList ID="rdSelView" runat="server" OnSelectedIndexChanged="rdSelView_SelectedIndexChanged"
                                              RepeatDirection="Horizontal" AutoPostBack="True" RepeatLayout="Flow">
                                              <asp:ListItem Value="1" Selected="True"><font class="blackblodtext">レベルのビュー</font></asp:ListItem>
                                              <asp:ListItem Value="2"><font class="blackblodtext">リスト一覧</font></asp:ListItem>
                                          </asp:RadioButtonList>
                                    </td>
                                </tr>
                            </table>                     
                           </td>
                       </tr>
                  <tr style="display:none;">
                     <%--Window Dressing--%>
                    <td align="left" style="width:50px" valign="top" class="blackblodtext"><b>部屋に割り当てられた</b><span class="reqfldstarText">*</span></td>
                        <td align="left"  valign="top">
                        <asp:Panel ID="pnlLevelView" runat="server" Height="250px" Width="100%" ScrollBars="Auto" BorderColor="Blue" BorderStyle="Solid" BorderWidth="1px" HorizontalAlign="Left">
                            <asp:TreeView ID="treeRoomSelection" runat="server" Height="90%" ShowCheckBoxes="All" onclick="javascript:getRooms(event)"
                                ShowLines="True" Width="95%" >
                                <NodeStyle CssClass="treeNode" />
                                <RootNodeStyle CssClass="treeRootNode" />
                                <ParentNodeStyle CssClass="treeParentNode" />
                                <LeafNodeStyle CssClass="treeLeafNode" />
                            </asp:TreeView>
                            </asp:Panel>
                            <asp:Panel ID="pnlListView" runat="server" BorderColor="Blue" BorderStyle="Solid"
                                BorderWidth="1px" Height="250px" ScrollBars="Auto" Visible="False" Width="100%" HorizontalAlign="Left" Direction="LeftToRight" Font-Bold="True" Font-Names="Verdana" Font-Size="Small" ForeColor="Green">
                                <asp:CheckBoxList ID="lstRoomSelection" runat="server" Height="95%" Width="95%" Font-Size="Smaller" ForeColor="ForestGreen" Font-Names="Verdana" RepeatLayout="Flow">
                                </asp:CheckBoxList>
                            </asp:Panel>
                            <%--Added for Location Issues  - Start--%>
                                            <asp:Panel ID="pnlNoData" runat="server" BorderColor="Blue" BorderStyle="Solid"
                                                BorderWidth="1px" Height="300px" ScrollBars="Auto" Visible="False" Width="100%" HorizontalAlign="Left" Direction="LeftToRight" Font-Size="Small">
                                                <table><tr align="center"><td>
                                                使用可能な部屋がありません。
                                                </td></tr></table>
                                            </asp:Panel>
                                            <%--Added for Location Issues  - End--%>
                            <br />
<%--                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="selectedloc" ValidationGroup="Submit" runat="server" Display="Dynamic" ErrorMessage="Please select at least one location"></asp:RequiredFieldValidator>
--%>                        </td>
                      </tr>
                   
                   <tr>
                 
                <td  valign="top" align="right">
                    <input name="opnRooms" type="button" id="opnRooms" onclick="javascript:OpenRoomSearch('frmSubmit');" value="部屋を追加" class="altShortBlueButtonFormat" />
                    
                    <input name="addRooms" type="button" id="addRooms" onclick="javascript:AddRooms();" style="display:none;" /><br />
                    <span class="blackblodtext"> <font size="1">リストから削除するには、部屋をダブルクリックします。</font></span>
                </td>
                <td valign="top" align="left" width="60%">
                  <select size="4" name="RoomList" id="RoomList" class="treeSelectedNode"  onDblClick="javascript:Delroms(this.value)"  style="height:350px;width:80%;" runat="server"></select>
                  
                    
                  <iframe style="display:none;" name="ifrmLocation" src=""   width="100%" height="300" align="left" valign="top">
                    <p>行く <a id="aLocation" href="" name="aLocation">場所のリスト</a></p>
                  </iframe> 
                </td>
              </tr>
                  
                 </table>
                    </td>
                </tr>
                <tr id="trComments2" runat="server">
                    <td colspan="5">
                      <asp:DataGrid runat="server" AutoGenerateColumns="False" ID="itemsGrid" BorderColor="Blue" CellPadding="4" Font-Bold="False" Font-Names="Verdana" Font-Size="Small" ForeColor="#333333"
                       GridLines="None" Width="90%" BorderStyle="Solid" BorderWidth="1px" AllowPaging="false" OnItemDataBound="BindItemImage" style="border-collapse:separate"> <%--Edited for FF--%>
                                 <%--Window Dressing - Start --%>
                                <FooterStyle  Font-Bold="True" CssClass="tableBody" />
                                <EditItemStyle CssClass="tableBody" />
                                <SelectedItemStyle CssClass="tableBody" Font-Bold="True" />
                                <PagerStyle CssClass="tableBody" HorizontalAlign="Center" />
                                <AlternatingItemStyle CssClass="tableBody"/>
                                <ItemStyle CssClass="tableBody" />
                                  <%--Window Dressing - End --%><%--UI changes for FB 1830 --%>
                                <HeaderStyle CssClass="tableHeader" Height="30px" />
                                     <Columns>
                                        <asp:TemplateColumn>
                                            <HeaderTemplate>
                                                <asp:Table ID="Table1" Width="100%" cellpadding="0" cellspacing="0" runat="server">
                                                    <asp:TableRow>
                                                        <asp:TableCell width="15%" ID="tdHName" HorizontalAlign="center" runat="server" CssClass="tableHeader"><asp:label ID="lblHName" runat="server" Text="の名前"></asp:label><span class="reqfldstarText">*</span></asp:TableCell>
                                                        <asp:TableCell width="10%" HorizontalAlign="center" cssclass="tableHeader" id="tdHQuantity" runat="server" Text="所有数<span class='reqfldstarText'>*</span>" Visible='<%# txtType.Text.Equals("1") %>'></asp:TableCell>
                                                        <asp:TableCell width="15%" HorizontalAlign="center" cssclass="tableHeader" id="tdHPrice" runat="server" Text="価格 (USD)<span class='reqfldstarText'>*</span>"></asp:TableCell>
                                                        <asp:TableCell width="10%" HorizontalAlign="center" cssclass="tableHeader" id="tdHSerialNumber" runat="server" Text="シリアルナンバー" Visible='<%# txtType.Text.Equals("1") %>'></asp:TableCell>
                                                        <asp:TableCell width="10%" HorizontalAlign="center" cssclass="tableHeader" id="tdHPortable" runat="server" Text="ポータブル" Visible='<%# txtType.Text.Equals("1") %>'></asp:TableCell>
                                                        <asp:TableCell width="10%" HorizontalAlign="center" cssclass="tableHeader" id="tdHImage" runat="server" Text="画像"></asp:TableCell>
                                                        <asp:TableCell width="15%" HorizontalAlign="center" cssclass="tableHeader" id="tdHComments" runat="server" Text="コメント"></asp:TableCell>
                                                        <asp:TableCell width="15%" HorizontalAlign="center" cssclass="tableHeader" id="tdHDescription" runat="server" Text="説明"></asp:TableCell>
                                                        <asp:TableCell width="5%" HorizontalAlign="center" cssclass="tableHeader" Text="削除"></asp:TableCell>
                                                    </asp:TableRow>
                                                </asp:Table>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Table ID="Table2" runat="server" width="100%" cellpadding="0" cellspacing="0">
                                                    <asp:TableRow Height="45px" VerticalAlign="Top">
                                                        <asp:TableCell HorizontalAlign="center" width="15%" id="tdName" runat="server">
                                                             <asp:Label ID="lblItemID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ID") %>' Visible="false"></asp:Label>
                                                             <asp:TextBox ID="txtName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Name") %>' CssClass="altText"></asp:TextBox>
                                                             <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtName" ValidationGroup="Submit"
                                                                 ErrorMessage="必須" Font-Bold="True" Display="Dynamic"></asp:RequiredFieldValidator>
                                                             <asp:RegularExpressionValidator ID="regConfName" ControlToValidate="txtName" Display="dynamic" runat="server" ValidationGroup="Submit" SetFocusOnError="true" ErrorMessage="<br> & < > ' + % \ ; ? | ^ = ! ` [ ] { } : # $ @ ~ &#34; は無効な記号です" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                                        </asp:TableCell>
                                                        <asp:TableCell HorizontalAlign="center" width="10%" id="tdQuantity" runat="server" Visible='<%# txtType.Text.Equals("1") %>' >
                                                              <asp:TextBox ID="txtQuantity" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Quantity") %>' onblur="javascript:ValidateInteger(this)" CssClass="altText" Width="30"></asp:TextBox>
                                                             <asp:RegularExpressionValidator ID="RegularExpressionValidator3" ValidationGroup="Submit" runat="server" Display="Dynamic" ErrorMessage="<br>正の数値のみ" SetFocusOnError="True" ControlToValidate="txtQuantity" ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                                             <asp:RequiredFieldValidator Display="Dynamic" ValidationGroup="Submit" ID="RequiredFieldValidator2" runat="server" ErrorMessage="必須" ControlToValidate="txtQuantity"></asp:RequiredFieldValidator>
                                                        </asp:TableCell>
                                                        <asp:TableCell HorizontalAlign="center" width="15%" id="tdPrice" runat="server">
                                                             <asp:TextBox ID="txtPrice" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Price") %>'  CssClass="altText" Width="50"></asp:TextBox>
                                                             <asp:RequiredFieldValidator Display="dynamic" ValidationGroup="Submit" ID="RequiredFieldValidator3" runat="server" ErrorMessage="必須" ControlToValidate="txtPrice"></asp:RequiredFieldValidator>
                                                            <asp:CustomValidator ID="cusPrice" runat="server" ControlToValidate="txtPrice" Display="dynamic" ErrorMessage="<br>無効量" SetFocusOnError="True" ValidationGroup="Submit" ClientValidationFunction="ClientValidate"></asp:CustomValidator>
                                                             <%--<asp:RegularExpressionValidator ID="regHKQuantity" ValidationGroup="Submit" runat="server" Display="Dynamic" ErrorMessage="<br>Positive Numeric Values Only" SetFocusOnError="True" ControlToValidate="txtPrice" ValidationExpression="^\d+(?:\.\d{0,2})?$"></asp:RegularExpressionValidator>--%>
                                                        </asp:TableCell>
                                                        <asp:TableCell HorizontalAlign="center" width="10%" id="tdSerialNumber" runat="server" Visible='<%# txtType.Text.Equals("1") %>'>
                                                             <asp:TextBox ID="txtSno" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SerialNumber") %>' CssClass="altText" Width="50"></asp:TextBox>
                                                             <asp:RegularExpressionValidator ID="RegularExpressionValidator2" ValidationGroup="Submit" ControlToValidate="txtSno" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br> & < > ' + % \ ; ? | ^ = ! ` [ ] { } : # $ @ ~ &#34; は無効な記号です" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                                        </asp:TableCell>
                                                        <asp:TableCell HorizontalAlign="center" width="10%" id="tdPortable" runat="server" Visible='<%# txtType.Text.Equals("1") %>' >
                                                             <asp:CheckBox ID="chkPortable" runat="server" Checked='<%# "1"==DataBinder.Eval(Container.DataItem, "Portable").ToString() %>'  />
                                                        </asp:TableCell>
                                                        <asp:TableCell HorizontalAlign="center" width="10%" id="tdImage" runat="server">
                                                             <asp:Image runat="server" ID="imgItem" Width="30" Height="30" onmouseover="javascript:ShowImage(this)" onmouseout="javascript:HideImage()" />
                                                             <%--<cc1:ImageControl id="imgItem" Width="30" Height="30" Visible="false" Runat="server" ></cc1:ImageControl>--%>
                                                        </asp:TableCell>
                                                        <asp:TableCell HorizontalAlign="center" width="15%" id="tdComments" runat="server">
                                                             <asp:TextBox TextMode="MultiLine" Rows="2" Width="150" ID="txtComments" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Comments") %>' CssClass="altText"></asp:TextBox>
                                                              <asp:RegularExpressionValidator ID="regGridComments" ValidationGroup="Submit" ControlToValidate="txtComments" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br> & < > ' + % \ ; ? | ^ = ! ` [ ] { } : # $ @ ~ &#34; は無効な記号です" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                                        </asp:TableCell>
                                                        <asp:TableCell HorizontalAlign="center" width="15%" id="tdDescription" runat="server">
                                                            <asp:TextBox ID="txtDescription" TextMode="MultiLine" Rows="2" Width="150" CssClass="altText" Text='<%# DataBinder.Eval(Container, "DataItem.Description") %>' runat="server"/>
                                                             <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ValidationGroup="Submit" ControlToValidate="txtDescription" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br> & < > ' + % \ ; ? | ^ = ! ` [ ] { } : # $ @ ~ &#34; は無効な記号です" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                                        </asp:TableCell>
                                                        <asp:TableCell HorizontalAlign="center" width="5%">
                                                             <asp:CheckBox ID="chkDelete" runat="server" />
                                                        </asp:TableCell>
                                                        
                                                        <asp:TableCell HorizontalAlign="center" width="15%" id="TableCell1" runat="server" Visible="false">
                                                             <asp:Label ID="LblImageId" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ImageId") %>' Visible="false"></asp:Label>
                                                        </asp:TableCell>
                                                        <asp:TableCell HorizontalAlign="center" width="15%" id="TableCell2" runat="server" Visible="false">
                                                             <asp:Label ID="LblImageName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ImageName") %>' Visible="false"></asp:Label>
                                                        </asp:TableCell>
                                                        
                                                    </asp:TableRow>
                                                    <asp:TableRow>
                                                        <asp:TableCell ColumnSpan="9" HorizontalAlign="center">
                                                            <asp:DataGrid ID="dgDeliveryType" Visible='<%# "1"==DataBinder.Eval(Container, "DataItem.Type").ToString() %>' runat="server" AllowSorting="false" AllowPaging="false" AutoGenerateColumns="false"
                                                            DataSource='<%# getDeliveryTypesDataSource( (String)DataBinder.Eval(Container.DataItem, "Item_Id").ToString() ) %>' Width="90%" style="border-collapse:separate"> <%--Edited for FF--%>
                                                                  <%--Window Dressing - Start--%>
                                                                <AlternatingItemStyle CssClass="tableBody" />
                                                                <ItemStyle CssClass="tableBody"/>
                                                                <%--Window Dressing - End--%>
                                                                <Columns>
                                                                   <asp:BoundColumn DataField="DeliveryTypeID" Visible="false"></asp:BoundColumn>
                                                                   <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="デリバリータイプ">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblDeliveryTypeName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DeliveryName") %>' CssClass="tableBody"></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="デリバリーコスト (USD)<span class='reqfldstarText'>*</span>">
                                                                        <ItemTemplate>
                                                                            <asp:TextBox ID="txtDeliveryCost" CssClass="altText"  runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DeliveryCost") %>'></asp:TextBox>
                                                                            <asp:RequiredFieldValidator ID="reqDC" runat="server" ValidationGroup="Submit" ErrorMessage="必須" Display="Dynamic" ControlToValidate="txtDeliveryCost" ></asp:RequiredFieldValidator>
                                                                            <%--<asp:RegularExpressionValidator ID="regDC" runat="server" ValidationGroup="Submit" Display="Dynamic" ErrorMessage="<br>Positive Numeric Values Only" SetFocusOnError="True" ControlToValidate="txtDeliveryCost" ValidationExpression="^\d+(?:\.\d{0,2})?$"></asp:RegularExpressionValidator>--%>
                                                                            <asp:CustomValidator ID="cusDCost" runat="server" ControlToValidate="txtDeliveryCost"  SetFocusOnError="True" ValidationGroup="Submit" Display="dynamic" ErrorMessage="<br>無効量" ClientValidationFunction="ClientValidate"></asp:CustomValidator>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="サービス料 (USD)<span class='reqfldstarText'>*</span>">
                                                                        <ItemTemplate>
                                                                            <asp:TextBox ID="txtServiceCharge" CssClass="altText" runat="server"  Text='<%# DataBinder.Eval(Container, "DataItem.ServiceCharge") %>'></asp:TextBox>
                                                                            <asp:RequiredFieldValidator ID="reqSC" runat="server" ValidationGroup="Submit" ErrorMessage="必須" Display="Dynamic" ControlToValidate="txtServiceCharge" ></asp:RequiredFieldValidator>
                                                                            <%--<asp:RegularExpressionValidator ID="regSC" runat="server" ValidationGroup="Submit" Display="Dynamic" ErrorMessage="<br>Positive Numeric Values Only" SetFocusOnError="True" ControlToValidate="txtServiceCharge" ValidationExpression="^\d+(?:\.\d{0,2})?$"></asp:RegularExpressionValidator>--%>
                                                                            <asp:CustomValidator ID="cusDCharge" runat="server" ControlToValidate="txtServiceCharge"  SetFocusOnError="True" ValidationGroup="Submit" Display="dynamic" ErrorMessage="<br>無効量" ClientValidationFunction="ClientValidate"></asp:CustomValidator>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                </Columns>
                                                            </asp:DataGrid>
                                                        </asp:TableCell>
                                                    </asp:TableRow>
                                                </asp:Table>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                  </Columns>
                            </asp:DataGrid>
                    </td>
                </tr>
                <tr id="trCatering">
                    <td colspan="2" align="center">
                        <asp:DataGrid runat="server" id="dgCateringMenus" AllowPaging="false" AllowSorting="false" AutoGenerateColumns="false" Width="100%"
                         OnItemDataBound="LoadPrerequisite" OnDeleteCommand="DeleteMenu" style="border-collapse:separate"> <%-- Edited for FF and FB 2050 --%>
                            <AlternatingItemStyle CssClass="tableBody" />
                            <EditItemStyle CssClass="tableBody" />
                            <HeaderStyle CssClass="tableHeader" HorizontalAlign="left" Height="30px" />
                            <FooterStyle CssClass="tableBody" />
                            <ItemStyle CssClass="tableBody" VerticalAlign="top" HorizontalAlign="left" />
                            <Columns>
                                <asp:BoundColumn DataField="ID" Visible="false"></asp:BoundColumn>
                                <%--Window Dressing--%>
                                <asp:TemplateColumn HeaderText="メニュー名"  HeaderStyle-CssClass="tableHeader" ItemStyle-HorizontalAlign="left" ItemStyle-VerticalAlign="top">
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtMenuName" runat="server" CssClass="altText" Text='<%# DataBinder.Eval(Container, "DataItem.Name") %>'></asp:TextBox>
                                         <asp:RequiredFieldValidator ID="reqInvName" runat="server" ControlToValidate="txtMenuName" ErrorMessage="必須" Display="Dynamic" ValidationGroup="Submit"></asp:RequiredFieldValidator>
                                         <asp:RegularExpressionValidator ID="regInvName" ValidationGroup="Submit" ControlToValidate="txtMenuName" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br> & < > ' + % \ ; ? | ^ = ! ` [ ] { } : # $ @ ~ &#34; は無効な記号です" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()'~]*$"></asp:RegularExpressionValidator>  
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="サービスの種類" ItemStyle-HorizontalAlign="left" ItemStyle-VerticalAlign="top">
                                    <ItemTemplate>
                                        <asp:CheckBoxList ID="chkLstServices" runat="server" DataValueField="ID" DataTextField="Name" RepeatDirection="Vertical" RepeatColumns="2"></asp:CheckBoxList>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <%--Window Dressing--%>
                                <asp:TemplateColumn HeaderText="メニュー<BR>価格(USD)"  HeaderStyle-CssClass="tableHeader" ItemStyle-HorizontalAlign="left" ItemStyle-VerticalAlign="top">
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtPrice" Width="50" CssClass="altText" runat="server"   Text='<%# DataBinder.Eval(Container, "DataItem.Price") %>' />
                                        <asp:RequiredFieldValidator ID="reqPrice" runat="server" ValidationGroup="Submit" Display="dynamic" ErrorMessage="必須" ControlToValidate="txtPrice"></asp:RequiredFieldValidator>
                                        <%--<asp:RegularExpressionValidator ID="regCATPrice" ValidationGroup="Submit" runat="server" Display="Dynamic" ErrorMessage="<br>Invalid Format" SetFocusOnError="True" ControlToValidate="txtPrice" ValidationExpression="^\d+(?:\.\d{0,2})?$"></asp:RegularExpressionValidator>--%>
                                        <asp:CustomValidator ID="cusHPrice" runat="server" ControlToValidate="txtPrice"  SetFocusOnError="True" ValidationGroup="Submit" Display="dynamic" ErrorMessage="<br>無効量" ClientValidationFunction="ClientValidate"></asp:CustomValidator>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <%--Window Dressing--%>
                                <asp:TemplateColumn HeaderText="メニュー項目"  HeaderStyle-CssClass="tableHeader" ItemStyle-HorizontalAlign="left" ItemStyle-VerticalAlign="top">
                                    <ItemTemplate>
                                        <asp:DataGrid ID="dgMenuItems" runat="server" AutoGenerateColumns="false" Width="100%" BackColor="transparent" BorderStyle="none" GridLines="none" style="border-collapse:separate"> <%--Edited for FF--%>
                                            <Columns>
                                                <asp:BoundColumn DataField="ID" Visible="false"></asp:BoundColumn>
                                                 <%--Window Dressing--%>
                                                <asp:TemplateColumn HeaderText="の名前"  HeaderStyle-CssClass="tableHeader" HeaderStyle-Font-Bold="true" ItemStyle-Width="60%">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblItemName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Name") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                 <%--Window Dressing--%>
                                                <asp:TemplateColumn HeaderText="項目を削除"  HeaderStyle-CssClass="tableHeader" HeaderStyle-Font-Bold="true" ItemStyle-Width="40%">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkItemDelete" runat="server" EnableViewState="true" />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                            </Columns>
                                        </asp:DataGrid>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn>
                                    <ItemTemplate>
                                        <asp:Button ID="btnAddNewItem" CssClass="altShortBlueButtonFormat" Text="アイテムを追加" CommandArgument='<%# DataBinder.Eval(Container, "DataItem.ID")%>' runat="server" OnClientClick="javascript:OpenItemsList(this); return false;" />
                                        <asp:HiddenField ID="selCateringItems" runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <%--Window Dressing--%>
                                <asp:TemplateColumn HeaderText="特別な指示"  HeaderStyle-CssClass="tableHeader" HeaderStyle-Font-Bold="true">
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtComments" TextMode="MultiLine" runat="server" CssClass="altText" Rows="2" Text='<%# DataBinder.Eval(Container, "DataItem.Comments") %>'></asp:TextBox><%----%>
                                         <asp:RegularExpressionValidator ID="regMenuComments" ValidationGroup="Submit" ControlToValidate="txtComments" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br> & < > ' + % \ ; ? | ^ = ! ` [ ] { } : # $ @ ~ &#34; は無効な記号です" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()'~]*$"></asp:RegularExpressionValidator>  
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <%--Window Dressing--%>
                                <asp:TemplateColumn HeaderText="削除"  HeaderStyle-CssClass="tableHeader">
                                    <ItemTemplate>
                                        <%--<asp:CheckBox ID="chkMenuDelete" runat="server" />--%>
                                        <asp:LinkButton ID="btnMenuDelete" runat="server" Text="削除" CommandName="Delete"></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                    </td> 
                </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td>
            <table style="width: 90%">
              <tr>
                <td align="center">
                    <asp:Button ID="btnCancel" runat="server" CssClass="altLongBlueButtonFormat" Text="キャンセル" OnClick="CancelMenu" OnClientClick="DataLoading(1)" />                
                </td>
                <td align="center">
                    <input type="button" ID="btnAddNewItem" runat="server" class="altLongBlueButtonFormat" Value="新しいアイテムを追加" OnClick="javascript:OpenItemsList(this)"/> <!--  OnClick="btnAddNewItem_Click" -->
                    &nbsp;
                    <asp:Button ID="btnAddNewMenu" runat="server" CssClass="altLongBlueButtonFormat" ValidationGroup="Submit" Text="新しいメニューを追加/送信する" OnClick="AddNewMenu" OnClientClick="ValidateSubmit(1)" />
                </td>
                <td align="center">
                    <asp:Button ID="btnSubmit" runat="server" ValidationGroup="Submit" CssClass="altLongBlueButtonFormat" Text="送信" OnClick="btnSubmit_Click" OnClientClick="ValidateSubmit(1)" />
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <asp:ValidationSummary ID="sumaryValidate" ValidationGroup="Submit" runat="server" CssClass="lblError" ShowMessageBox="True" Visible="False" />
                    <asp:HiddenField ID="selItems" runat="server" />
                </td>
            </tr>
            </table>
          </td>
        </tr>
    </table>  
    </center>
<div id="divPic" style="display:none;  z-index:1;">
    <img src="" name="myPic" id="myPic" width="200" height="200" alt="mypic"/>
</div>
    <input type="hidden" id="CreateBy" value="7" />
       <script language="javascript">
         if (document.getElementById("txtType").value == "1")
            document.getElementById("helpPage").value = "3";
         if (document.getElementById("txtType").value == "2")
            document.getElementById("helpPage").value = "5";
         if (document.getElementById("txtType").value == "3")
            document.getElementById("helpPage").value = "40";
         //document.getElementById("txtApprover1").disabled="true";

//FB 1830 - Starts
fnChangCurrencyFormat();
function fnChangCurrencyFormat()
{
    changeCurrencyFormat("itemsGrid_ctl01_Table1",'<%=currencyFormat %>');
    
    var num = 0;
    var incre = 1;
    var incstr = "";
    while(num == 0)
    {
        incre = incre + 1;            
        incstr = incre;
        
        if(incre < 10 )
            incstr = "0" + incre;
      
        var tName = "itemsGrid_ctl" + incstr + "_dgDeliveryType";
        
        if(document.getElementById(tName))
            changeCurrencyFormat(tName,'<%=currencyFormat %>');    
        else
            num = 1;
    }    
    changeCurrencyFormat("dgCateringMenus",'<%=currencyFormat %>');
}
//FB 1830 - End
    </script>

  </form>
  </body>
  <%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->

﻿<%@ Page Language="C#" AutoEventWireup="true"  Inherits="en_TemplateDetails" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
  
  <meta name="Description" content="myVRM (Videoconferencing Resource Management) is a revolutionary Web-based software application that manages any video conferencing environment.">
  <meta name="Keywords" content="myVRM, Videoconferencing Resource Management, video conferencing, video bridges, video endpoints">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta NAME="LANGUAGE" CONTENT="en">
  <meta NAME="DOCUMENTCOUNTRYCODE" CONTENT="us">
   
  <LINK title="Expedite base styles" href="<%=Session["OrgCSSPath"]%>" type="text/css" rel="stylesheet" />
  <script type="text/javascript" src="script/errorList.js"></script>
  <script type="text/javascript" src="script\mytree.js"></script>
  
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>myVRM</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
         <table width="90%" >
            <tr>
                <td align="center">
                  <h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;テンプレートの詳細</h3>
                              
                </td>
            </tr>  
            <tr>
                <td align="center" style="width: 1168px">
                    <asp:Label ID="ErrLabel" runat="server" Text="" CssClass="lblError"></asp:Label>
                </td>
            </tr>              
        </table> 
         <table border="0" cellpadding="2" cellspacing="3" width="90%">
            <tr>
                <td colspan=2>
                <%--Window Dressing--%>
                <b><font class="subtitleblueblodtext">テンプレートの概要</font></b><br /><br />      
                </td>
            </tr>
            <tr>
              <td align="left" style="width:30%"><span class="blackblodtext">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;説明</span></td>
              <td align="left">
                <asp:Label ID="LblDescription" runat="server"></asp:Label>
              </td>
            </tr>
            <tr>
              <td align="left" style="width:30%"><span class="blackblodtext">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%if (Application["Client"].ToString().ToUpper() == "MOJ"){%>公聴会<%}else{ %>公開会議<%}%></span></td><%--added for FB 1428 Start--%>
              <td align="left">
                <asp:Label ID="LblConference" runat="server"></asp:Label>
              </td>
            </tr>
            <tr>
              <td align="left"><span class="blackblodtext">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;サービス（有料）</span></td>
              <td align="left">                               
                <asp:Label ID="LblRoom" runat="server"></asp:Label>
              </td>
            </tr>
     </table>
     <table id="tbPrt" runat="server"> <%--Edited for FB 1425 QA Bug--%>
        <tr>
            <%--Window Dressing--%>
            <td align="center" class="subtitleblueblodtext">
                <br /><b><span class="subtitleblueblodtext">参加者（S）ステータス </span></b><br /><br />
             </td>
        </tr>
     </table>
     <table style="margin:0 auto" width="100%" id="tbDgPrt" runat="server">        
        <tr>
            <td align="center">
                <asp:DataGrid ID="DgTemplates"  runat="server" AutoGenerateColumns="False" CellPadding="0" GridLines="None" AllowSorting="true" 
                     BorderColor="blue" BorderStyle="solid" BorderWidth="1" ShowFooter="true" OnItemDataBound="BindGrid" 
                       Width="100%" Visible="true" style="border-collapse:separate"> <%--Edited for FF--%>
                    <SelectedItemStyle  CssClass="tableBody"/>
                     <AlternatingItemStyle CssClass="tableBody" />
                     <ItemStyle CssClass="tableBody"  />
                    <HeaderStyle CssClass="tableHeader" Height="30px" />
                    <EditItemStyle CssClass="tableBody" />                    
                     <%--Window Dressing START--%>
                    <FooterStyle CssClass="tableBody" />
                    <Columns>                       
                        <asp:BoundColumn  HeaderStyle-CssClass="tableHeader" HeaderText="名前" ItemStyle-CssClass="tableBody"></asp:BoundColumn>
                        <asp:BoundColumn  HeaderStyle-CssClass="tableHeader" HeaderText="メール" ItemStyle-CssClass="tableBody"></asp:BoundColumn>                        
                        <asp:BoundColumn  HeaderStyle-CssClass="tableHeader" HeaderText="状況" ItemStyle-CssClass="tableBody"></asp:BoundColumn>                                    
                    </Columns>
                    <%--Window Dressing END--%>
                </asp:DataGrid>
                <asp:Table  runat="server" ID="TblNoTemplates" Visible="false" CellPadding="1" CellSpacing="0" Width="100%" >
                    <asp:TableRow>                                       
                        <asp:TableCell width="90%" align="left">
                             <span class="lightgrayboldstext">
                                  このテンプレートは、参加者が含まれていません。
                                </SPAN>
                        </asp:TableCell>
                    </asp:TableRow>
                    
                </asp:Table>                    
            </td>
          </tr>
     </table>
     
     <table border="0" cellpadding="1" cellspacing="0" width="100%">
        <tr>
          <td align="right"><br />
<%--code added for Soft Edge button--%>
            <input type="button" name="close" value="閉じる" class="altShortBlueButtonFormat" onclick="Javascript: window.close();" />
          </td>
       </tr>
    </table>
    </div>
    </form>
</body>
</html>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
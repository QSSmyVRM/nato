﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="HolidayImport.aspx.cs" Inherits="ns_HolidayImport.HolidayImport" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <!-- FB 2050 -->
<meta http-equiv="X-UA-Compatible" content="IE=7" /> <!-- FB 2050 -->
<!-- #INCLUDE FILE="inc/maintopNET.aspx" --> 

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
<script language="javascript">

function DataLoading()
{
    var obj = document.getElementById("tblDataImport");
    if (obj != null)
        obj.style.display="";
}
</script>
    <title>データベースのインポート</title>
  <link rel="stylesheet" title="Expedite base styles" type="text/css" href="<%=Session["OrgCSSPath"]%>"> <%--Organization CSS Module --%>
</head>
<body>
    <form id="frmDataImport" runat="server">
    <br /><br />
    <center>
        <asp:Label ID="errLabel" runat="server" CssClass="lblError"></asp:Label>
    </center>
    <br /><br /><br />
        <h3 style="text-align: center">
            休日のインポートツール</h3>
            <br /><br />            
            <table width="100%" bgcolor="white" cellpadding="1" cellspacing="0">
                <tr>
                    <td width="40%" align="center" style="display:none;">
                        <b>外部データベースの種類:</b>&nbsp;
                        <asp:DropDownList CssClass="altLong0SelectFormat" ID="lstDatabaseType" runat="server">
                            <asp:ListItem Selected="True" Text="Rendezvous" Value="1"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td align="left">
                        <asp:FileUpload ID="fleMasterCSV" Width="20%" EnableViewState="true" runat="server" CssClass="altText" />  
                        <asp:Button ID="btnGetDataTable" Width="20%" runat="server" OnClick="GenerateDataTable" CssClass="altShortBlueButtonFormat" Text="祝日をアップロード" />
                    </td>
                </tr>
            </table>
            <br /><br />
            
        <br />
        <table>
            <tr>
                <td align="left">
                    <b>注釈:</b> このセクションでは、localhostのみを使用してWebサーバからアクセスする必要があり. データファイルは、Webサーバーは、自己に常駐している必要があり.
                </td>
            </tr>
        </table>
        <table id="tblDataImport">
            <tr>
                <td>
                    <b><font color="#FF00FF" size="2">データローディング中 ...</font></b>&nbsp;&nbsp;&nbsp;&nbsp;<img border="0" src="image/wait1.gif" width="100" height="12">
                </td>
            </tr>
        </table>
    </form>
<br />
<br />
<p>&nbsp;</p>
<p>&nbsp;</p>
<script language="javascript">
document.getElementById("tblDataImport").style.display="none";
</script>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" --> 
</body>
</html>

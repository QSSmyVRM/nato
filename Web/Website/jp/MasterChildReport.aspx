<%@ Page Language="C#" AutoEventWireup="true" Inherits="MasterChildReport" EnableEventValidation="false"  Debug="true" %>
<meta http-equiv="X-UA-Compatible" content="IE=7" /> 
<!-- #INCLUDE FILE="inc/maintopNET.aspx" --> <%--FB 2050--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<%@ Register Assembly ="DevExpress.Web.ASPxEditors.v10.2, Version=10.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v10.2, Version=10.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v10.2, Version=10.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v10.2, Version=10.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxMenu" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v10.2, Version=10.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" 
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v10.2.Export, Version=10.2.3.0, Culture=neutral,PublicKeyToken=b88d1754d700e49a" 
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %> 

<%--Basic validation function--%>
<script type="text/javascript">   
  
  var servertoday = new Date();
     
    function OnShowButtonClick(s) 
    {    
           
        var hdnSubmitValue = document.getElementById("hdnSubmitValue");
        var ctrl;
         // Code for Department Selection
//        if(hdnSubmitValue.value == "3")
//           ctrl = chk_DeptR;
//        else if(hdnSubmitValue.value == "4")
//           ctrl = chk_DeptE;
//        else if(hdnSubmitValue.value == "5")
//           ctrl = chk_DeptM;
        
        if (s == "dptPopup" || s == "dptPopupChk")   
        {
            var canShow = 0;
             // Code for Department Selection
//            if( (s== "dptPopup" && (hdnSubmitValue.value == "1" || hdnSubmitValue.value == "2")) 
//             || (s== "dptPopup" && (hdnSubmitValue.value == "3" || hdnSubmitValue.value == "4" || hdnSubmitValue.value == "5") && ctrl != undefined && ctrl.GetChecked())    
//             || (s=="dptPopupChk" && ctrl != undefined && ctrl.GetChecked()) )
//                canShow = 1;
            
            canShow = 1;
                
            if(OrgList.GetSelectedItems().length > 0 && canShow == 1) 
                dptPopup.ShowAtPos(365, 30);
            else
                dptPopup.Hide();
        }
        else if (s == "assignmentPopup") 
        {
            if(lstDepartment.GetSelectedItems().length > 0)
            {
               if(hdnSubmitValue.value == "1")
               {
                    showAssignmentCtrols('block')                
                    assignmentPopup.ShowAtPos(530, 30);        
                    resourcePopup.ShowAtPos(700, 30);  
               } 
               else if(hdnSubmitValue.value == "2")
               {
                    showAssignmentCtrols('block')                
                    assignmentPopup.ShowAtPos(530, 30);        
                    confPopup.ShowAtPos(700, 30);  
               }
            }
            else
            {
                assignmentPopup.Hide();
                resourcePopup.Hide();
                confPopup.Hide();
            }
            
           if(hdnSubmitValue.value == "3")
           {
                if(chk_TiersR.GetChecked())
                    tierPopup.ShowAtPos(530, 30);  
                else 
                    tierPopup.Hide();
           }
           
           if(hdnSubmitValue.value == "4")
           {
                if(chk_RmNameE.GetChecked())
                {
                    tierPopup.ShowAtPos(690, 30);  
                    rmInfoPopup.ShowAtPos(850, 30);  
                }
                else
                {
                    tierPopup.Hide();
                    rmInfoPopup.Hide();
                }   
                
                if(chk_ENameE.GetChecked())
                    eptPopup.ShowAtPos(530, 30);
                else
                    eptPopup.Hide();
           }
           
           if(hdnSubmitValue.value == "5")
           {
                if(chk_RmNameM.GetChecked())
                {
                    tierPopup.ShowAtPos(690, 30);  
                    rmInfoPopup.ShowAtPos(850, 30);  
                }
                else
                {
                    tierPopup.Hide();
                    rmInfoPopup.Hide();
                }
                    
                if(chk_ENameM.GetChecked())
                    eptPopup.ShowAtPos(530, 30);
                else
                    eptPopup.Hide();
           }
        }
        else if(s == "rmInfoPopup")
        {
//            if(lstTiers.GetSelectedItems().length > 0)
//            {
                if(hdnSubmitValue.value == "3" && chk_NameR.GetChecked())
                   rmInfoPopup.ShowAtPos(700, 30);  
                else if((hdnSubmitValue.value == "4" || hdnSubmitValue.value == "5") && chk_RmNameE.GetChecked())
                   rmInfoPopup.ShowAtPos(850, 30);  
                else
                   rmInfoPopup.Hide();
                    
//            }
//            else
//               rmInfoPopup.Hide();
        }
        else if(s == "rmAssetPopup")
        {
            if(chk_AssetsR.GetChecked())
                rmAssetPopup.ShowAtPos(700, 30);  
            else
                rmAssetPopup.Hide();
        }
        else if(s == "assignment")
        {
            if(chk_UserAssignmentR.GetChecked())
            {
                showAssignmentCtrols('None')
                if(assignmentPopup_lstRmApprover)
                    assignmentPopup_lstRmApprover.style.display = 'block';
                assignmentPopup.ShowAtPos(530, 30);        
            }
            else
                assignmentPopup.Hide();
        }        
        else if(s == "tierPopup")
        {
            if(lstEptDetailsM.GetSelectedItems().length > 0)
                tierPopup.ShowAtPos(690, 30);        
            else
                tierPopup.Hide();
        }  
         else if(s == "confOccurencePopup")
        {
            if(lstConfStatus.GetSelectedItems().length > 0)
                confOccurencePopup.ShowAtPos(355, 265);        
            else
                confOccurencePopup.Hide();
        }
         else if(s == "confDetailsPopup")
        {
            if(lstConfOccurence.GetSelectedItems().length > 0)
                confDetailsPopup.ShowAtPos(505, 365);        
            else
                confDetailsPopup.Hide();
        }
        else
        {
            var isChecked
            
            if(s == "popupU")
                isChecked = chk_OrgU.GetChecked();    
            else if(s == "popupR")
                isChecked = chk_OrgR.GetChecked();        
            else if(s == "popupE")
                isChecked = chk_OrgE.GetChecked();        
            else if(s == "popupM")
                isChecked = chk_OrgM.GetChecked();                
            else
                isChecked = chk_Org.GetChecked();
            
            if(isChecked)
                popup.ShowAtPos(202, 30);
            else
            {
                popup.Hide(); 
                fnClearListSelection(OrgList);
                
                dptPopup.Hide();                
                if(ctrl != undefined)
                    ctrl.SetChecked(false);                    
                             
                if(hdnSubmitValue.value == "1" || hdnSubmitValue.value == "2")
                {
                    fnRightParamsClear('O')
                    assignmentPopup.Hide();
                    resourcePopup.Hide();
                    confPopup.Hide();
                }
                //rmInfoPopup.Hide();                
                //eptPopup.Hide();
            }
        }
            
        if(OrgList.GetSelectedItems().length > 0) 
        {        
            if ((s == "dptPopup" && (hdnSubmitValue.value == "1" || hdnSubmitValue.value == "2")) || s == "dptPopupChk" || (ctrl != undefined && ctrl.GetChecked() && s != "assignmentPopup" && s != "rmInfoPopup"))        
                getArrayList();
        }
        else if(ctrl != undefined && s == "dptPopupChk")
            ctrl.SetChecked (false);
            
        if(hdnSubmitValue.value == "3" || hdnSubmitValue.value == "4" || hdnSubmitValue.value == "5")
            PopupPositions(hdnSubmitValue.value)
    }
    
    function PopupPositions(popVal)
    {
        if(popVal == "2")
        {
            
        }
        else if(popVal == "3")
        {
        
        }
        else if(popVal == "4")
        {
        
        }
    }
    
    function showAssignmentCtrols(SorHVar)
    {   
        if(assignmentPopup_lstRmApprover)
            assignmentPopup_lstRmApprover.style.display = SorHVar;
        if(document.getElementById(assignmentPopup_ASPxMenu1))
            document.getElementById(assignmentPopup_ASPxMenu1).style.display = SorHVar;
        if(document.getElementById(assignmentPopup_ASPxMenu2))
            document.getElementById(assignmentPopup_ASPxMenu2).style.display = SorHVar;
        if(document.getElementById(assignmentPopup_ASPxMenu3))
            document.getElementById(assignmentPopup_ASPxMenu3).style.display = SorHVar;
        if(assignmentPopup_lstMCUApprover)
            assignmentPopup_lstMCUApprover.style.display = SorHVar;
        if(assignmentPopup_lstWOAdmin)
            assignmentPopup_lstWOAdmin.style.display = SorHVar;
    }
    
    function getArrayList()
    {        
        var hdnDeptList = document.getElementById("hdnDepartmentList");        
        
        //hdnDeptList.value = "11!1!Sales``11!2!Engineering``11!3!larry``12!4!New Dept";
        var AllDeptList = hdnDeptList.value.split("``");
        
        var deptList;
        
        lstDepartment.BeginUpdate();
        
        var lstCnt = lstDepartment.GetItemCount();
         for (var c = 0; c < lstCnt; c++) 
		    lstDepartment.RemoveItem(0);		 
        
        var n =0;
        for (var i = 0; i < AllDeptList.length; i++) 
        {                
            deptList = AllDeptList[i].split('!'); 
            var s = OrgList.GetSelectedItems()
            if(deptList[0] == s[0].value)
            {
                lstDepartment.InsertItem(n,deptList[2],deptList[1]);                    
                n = n + 1;
            }
        }
        
        if(lstDepartment.GetItemCount() > 0)
            lstDepartment.InsertItem(n,"すべて","0");        
        else
            lstDepartment.InsertItem(n,"いいえ 部門/部署","-1");        

        lstDepartment.EndUpdate();
    }
    
     
</script>

<%--Show or Hide Popup function --%>
<script type="text/javascript">
    
    function HidePopups()
    {
        popup.Hide();
        dptPopup.Hide();
        assignmentPopup.Hide();
        resourcePopup.Hide();
        confPopup.Hide();
        tierPopup.Hide();
        rmInfoPopup.Hide();
        rmAssetPopup.Hide();
        confStatusPopup.Hide();
        confOccurencePopup.Hide();
        confDetailsPopup.Hide();
        eptDetailsPopup.Hide();
        woDetailsPopup.Hide();
        mcuDetailsPopup.Hide();
        eptPopup.Hide();
    }
    
    function fnShow(submitVal)
    {
        var hdnSubmitValue = document.getElementById("hdnSubmitValue");    
        var popup2 = $find('ConferencePopUp');
        
        if(rbAdhoc.GetChecked())
        {
            SavedRptListPopup.Hide();
            
            if(popup2)
            {
                if(hdnSubmitValue.value != submitVal)
                {
                    HidePopups();
                    fnClear();
                }
                
                hdnSubmitValue.value = submitVal;
                popup2.show();
                MainPopup.ShowAtPos(0, 0);
                
                var tblConference = document.getElementById("tblConference");            
                var tblUser = document.getElementById("tblUser");            
                var tblRoom = document.getElementById("tblRoom");    
                var tblEndPoint = document.getElementById("tblEndPoint");    
                var tblMCU = document.getElementById("tblMCU");    
                    
                tblConference.style.display = 'None';
                tblUser.style.display = 'None';
                tblRoom.style.display = 'None';
                tblEndPoint.style.display = 'None';
                tblMCU.style.display = 'None';
                
                if(submitVal == "1")
                {
                    tblConference.style.display = 'block';
                    MainPopup.SetHeaderText("会議");
                }
                else  if(submitVal == "2")
                {
                    tblUser.style.display = 'block';
                    MainPopup.SetHeaderText("ユーザー");
                }                
                else  if(submitVal == "3")
                {
                    tblRoom.style.display = 'block';
                    MainPopup.SetHeaderText("部屋");
                }    
                else  if(submitVal == "4")
                {
                    tblEndPoint.style.display = 'block';
                    MainPopup.SetHeaderText("エンドポイント");
                }    
                else  if(submitVal == "5")
                {
                    tblMCU.style.display = 'block';
                    MainPopup.SetHeaderText("MCU");
                }
            }
        }
        else if (rbSaved.GetChecked())
        {
            var hdnReportNames = document.getElementById("hdnReportNames");        
            var hdnAllReports = document.getElementById("hdnAllReports");        
            var hdnSubmitValue = document.getElementById("hdnSubmitValue");
            var rptList;
            var detailsList;
            var subdetailsList;
            
            hdnSubmitValue.value = submitVal;            
            lstReportList.ClearItems()
            
            var AllRptList = hdnAllReports.value.split("Õ");
            var n = 0;
            
            for (var i = 0; i < AllRptList.length; i++)
            {
                rptList = AllRptList[i].split('Ô');
                
                if(rptList[0] == hdnSubmitValue.value)
                {
                    detailsList = rptList[1].split('Ä');
                                        
                    for (var j = 0; j < detailsList.length; j++)
                    {
                        if(detailsList[j] != "")
                        {
                            subdetailsList = detailsList[j].split('Ö');
                            
                            lstReportList.InsertItem(n, subdetailsList[0], subdetailsList[1]);
                            n = n + 1;
                        }
                    }
                }
            }
            
            if(lstReportList.GetItemCount() == 0)
                lstReportList.InsertItem(n,"No Saved Reports","-1");
            
            SavedRptListPopup.ShowAtPos(160, 220);
            
        }
    }
    
    function SavedReportSelection()
    {
        var hdnMainIPValue = document.getElementById("hdnMainIPValue");
        var hdnRightMenuValue = document.getElementById("hdnRightMenuValue");
        
        if(lstReportList.GetSelectedItems().length > 0)
        {
            var sr = lstReportList.GetSelectedItems();
            var rptValue = sr[0].value; 
            
            if(rptValue != "" && rptValue != "-1")
            {
                var subdetailsList = rptValue.split('??');
                
                if(subdetailsList[0] != 'undefined' && subdetailsList[0] != null)                
                    hdnMainIPValue.value = subdetailsList[0];
                    
                if(subdetailsList[1] != 'undefined' && subdetailsList[1] != null)                
                    hdnRightMenuValue.value = subdetailsList[1];
                    
                //fnSubmit();
                //fnClosePopup();
                var hdnOkValue = document.getElementById("hdnOkValue");
                hdnOkValue.value = "1";
            
                SavedRptListPopup.Hide();
                
                btnPreview.DoClick(); 
                
            }
        }
    }
    
    function fnClose()
    {    
        var closePup = document.getElementById("ClosePUp");
        
        if(closePup != null)
            closePup.click();        
            
        return false;
    }
    
    function fnPersonAll()
    { 
        if(chk_AllPerson.GetChecked())
        {
            chk_PFN.SetChecked (true); 
            chk_PLN.SetChecked (true); 
            chk_PMail.SetChecked (true); 
            chk_PRole.SetChecked (true); 
            //chk_PPerson.SetChecked (true); 
        }
        else
        {
            chk_PFN.SetChecked (false); 
            chk_PLN.SetChecked (false); 
            chk_PMail.SetChecked (false); 
            chk_PRole.SetChecked (false); 
            //chk_PPerson.SetChecked (false);
        }
    }
    
    function fnHostAll()
    { 
        if(chk_AllHost.GetChecked())
        {
            chk_HFN.SetChecked (true); 
            chk_HLN.SetChecked (true); 
            chk_HMail.SetChecked (true); 
            chk_HRole.SetChecked (true); 
            //chk_HPerson.SetChecked (true); 
        }
        else
        {
            chk_HFN.SetChecked (false); 
            chk_HLN.SetChecked (false); 
            chk_HMail.SetChecked (false); 
            chk_HRole.SetChecked (false); 
            //chk_HPerson.SetChecked (false);             
        }
    }
        
    function fnScheduledAll()
    { 
        if(chk_AllConf.GetChecked())
        {
            chk_Completed.SetChecked (true); 
            chk_Scheduled.SetChecked (true); 
            chk_Deleted.SetChecked (true); 
            chk_Terminated.SetChecked (true); 
            chk_Pending.SetChecked (true); 
        }
        else
        {
            chk_Completed.SetChecked (false); 
            chk_Scheduled.SetChecked (false); 
            chk_Deleted.SetChecked (false); 
            chk_Terminated.SetChecked (false); 
            chk_Pending.SetChecked (false); 
        }
    }
    
    function fnConfTypeAll()
    { 
        if(chk_AllConfTypes.GetChecked())
        {
            chk_Audio.SetChecked (true); 
            chk_Video.SetChecked (true); 
            chk_Room.SetChecked (true); 
            chk_PP.SetChecked (true); 
            //chk_TP.SetChecked (true); 
            
        }
        else
        {
            chk_Audio.SetChecked (false); 
            chk_Video.SetChecked (false); 
            chk_Room.SetChecked (false); 
            chk_PP.SetChecked (false); 
            //chk_TP.SetChecked (false); 
        }
    }
    
    function fnRecurAll()
    { 
        if(chk_AllMode.GetChecked())
        {
            chk_Single.SetChecked (true); 
            chk_Recurring.SetChecked (true); 
        }
        else
        {
            chk_Single.SetChecked (false); 
            chk_Recurring.SetChecked (false); 
        }
    }
    
    function fnRmTypeAll()
    { 
        if(chk_AllTypeR.GetChecked())
        {
            chk_VideoRoomR.SetChecked (true); 
            chk_AudioRoomR.SetChecked (true); 
            chk_MeetingRoomR.SetChecked (true); 
        }
        else
        {
            chk_VideoRoomR.SetChecked (false); 
            chk_AudioRoomR.SetChecked (false); 
            chk_MeetingRoomR.SetChecked (false); 
        }
    }
    
    function fnBehaviorAll()
    { 
        if(chk_AllBehaviorR.GetChecked())
        {
            chk_VideoConferenceR.SetChecked (true); 
            chk_AudioConferenceR.SetChecked (true); 
            chk_RoomConferenceR.SetChecked (true); 
            
            fnBehaviorAllChecked();
        }
        else
        {
            chk_VideoConferenceR.SetChecked (false); 
            chk_AudioConferenceR.SetChecked (false); 
            chk_RoomConferenceR.SetChecked (false);  
            
            confStatusPopup.Hide();
            confOccurencePopup.Hide();
            confDetailsPopup.Hide();
            eptDetailsPopup.Hide();
            woDetailsPopup.Hide();
            mcuDetailsPopup.Hide();
        }
    }
    
    function RemoveALL(e1,e2)
    {
        if(e1.GetChecked() == false)
            e2.SetChecked(false);
    }
        
</script>

<%--fnSubmit function --%>
<script type="text/javascript">

    function fnCheck()
    {
        var hdnSubmitValue = document.getElementById("hdnSubmitValue");
        if(hdnSubmitValue.value == "")
            return false;
            
        return true;        
    }
    
    function fnSubmit()
    {    
        var hdnSubmitValue = document.getElementById("hdnSubmitValue");
        
        if(rbAdhoc.GetChecked())
        {            
            if(hdnSubmitValue.value == "1")
                fnGetConferenceMenuInput();
            else if(hdnSubmitValue.value == "2")
                fnGetUserMenuInput();
            else if(hdnSubmitValue.value == "3")
                fnGetRoomMenuInput();
            else if(hdnSubmitValue.value == "4" || hdnSubmitValue.value == "5")
                fnGetEndPointMenuInput()
        }
    }
    
    function fnAssignValue(element,type,cellVal)
    {
        memo.SetText(cellVal.replace(new RegExp(",", "g"), ";\n")); 
        MoreInfoPopup.ShowAtElement(element)
        
        if(type == "R")
            MoreInfoPopup.SetHeaderText("サービス（有料）");
        else if(type == "E")
            MoreInfoPopup.SetHeaderText("エンドポイント");
        else if(type == "D")
            MoreInfoPopup.SetHeaderText("部門");
        
        return false; 
    }
</script>

<%--Clear functions--%>
<script type="text/javascript">
    
    function fnClear()
    {
        var hdnSubmitValue = document.getElementById("hdnSubmitValue");
        
        if(hdnSubmitValue.value == "1")
            fnConferenceMenuClear();
        else if(hdnSubmitValue.value == "2")
            fnUserMenuClear();
        else if(hdnSubmitValue.value == "3")
            fnRoomMenuClear();
        else if(hdnSubmitValue.value == "4")
            fnEndPointMenuClear()
        else if(hdnSubmitValue.value == "5")
            fnMCUMenuClear()
            
        return false;
    }
    
    function fnConferenceMenuClear()
    {
        chk_ConfID.SetChecked (false); 
        chk_ConfTitle.SetChecked (false); 
        
        fnRightParamsClear('');
        
        chk_Org.SetChecked (false); 
            OnShowButtonClick();
            
        chk_AllHost.SetChecked (false); 
        fnHostAll()
        
        chk_AllPerson.SetChecked (false); 
        fnPersonAll()
        
        chk_AllConf.SetChecked (false);         
        fnScheduledAll()
        
        chk_AllConfTypes.SetChecked (false);         
        fnConfTypeAll();
        
        chk_AllMode.SetChecked (false);         
        fnRecurAll();
        
        chk_Date.SetChecked (false); 
        chk_Time.SetChecked (false); 
        //chk_Hours.SetChecked (false); 
        chk_Minutes.SetChecked (false); 
    }
    
    function fnUserMenuClear()
    {
        chk_OrgU.SetChecked (false); 
            OnShowButtonClick('popupU');
        
        fnRightParamsClear('');
       
        chk_DeptU.SetChecked (false); 
            OnShowButtonClick('assignmentPopup');
            
        chk_FNU.SetChecked (false); 
        chk_LNU.SetChecked (false); 
        //chk_PersonU.SetChecked (false); 
        
        chk_UserEmailU.SetChecked (false); 
        chk_SecondaryEmailU.SetChecked (false); 
        chk_RoleU.SetChecked (false); 
        chk_TimeZoneU.SetChecked (false); 
        chk_ConfHostU.SetChecked (false); 
        chk_ConfPartU.SetChecked (false); 
        
        chk_AccExpirU.SetChecked (false); 
        chk_MinU.SetChecked (false); 
        chk_ExchangeU.SetChecked (false); 
        chk_DominoU.SetChecked (false); 
    }
    
    function fnRoomMenuClear()
    {
        chk_OrgR.SetChecked (false); 
            OnShowButtonClick('popupR');
            
        fnRightParamsClear('');
        
        chk_DeptR.SetChecked (false); 
            OnShowButtonClick('assignmentPopup');
            
        chk_TiersR.SetChecked (false); 
            OnShowButtonClick('assignmentPopup');
            
        chk_NameR.SetChecked (false); 
            OnShowButtonClick('rmInfoPopup');
            
        chk_HostR.SetChecked (false); 
    
        chk_AllTypeR.SetChecked (false);         
        fnRmTypeAll();
        
        chk_AllBehaviorR.SetChecked (false);         
        fnBehaviorAll();
    }
    
    function fnEndPointMenuClear()
    {
        chk_OrgE.SetChecked (false); 
            OnShowButtonClick('popupE');
            
        fnRightParamsClear('');
        
        chk_DeptE.SetChecked (false); 
            
        chk_ENameE.SetChecked (false); 
        //chk_SpecEptE.SetChecked (false); 
        chk_RmNameE.SetChecked (false); 
        //chk_SpecRoomE.SetChecked (false); 
        
            OnShowButtonClick('assignmentPopup');
        
        chk_ConfE.SetChecked (false); 
            fnBehaviorAllChecked();
    }
    
    function fnMCUMenuClear()
    {
        chk_OrgM.SetChecked (false); 
            OnShowButtonClick('popupM');
            
        fnRightParamsClear('');

        chk_DeptM.SetChecked (false); 
            OnShowButtonClick('assignmentPopup');
        chk_MCUNameM.SetChecked (false); 
        //chk_SpecMCUM.SetChecked (false); 
        chk_ENameM.SetChecked (false); 
        //chk_SpecEptM.SetChecked (false); 
        chk_RmNameM.SetChecked (false); 
            OnShowButtonClick('assignmentPopup');
        //chk_SpecRoomM.SetChecked (false); 
        
        chk_ConfM.SetChecked (false); 
            fnBehaviorAllChecked();
    }
    
    function fnRightParamsClear(m)
    {
        var hdnSubmitValue = document.getElementById("hdnSubmitValue");
        
        if(hdnSubmitValue.value == "1")
        {
            fnClearListSelection(OrgList);
            fnClearListSelection(lstDepartment);
            fnClearListSelection(lstRmApprover);
            fnClearListSelection(lstMCUApprover);
            fnClearListSelection(lstWOAdmin);
            fnClearListSelection(lstAttendee);
            fnClearListSelection(lstRmType);
            fnClearListSelection(lstEPYes);
            fnClearListSelection(lstMCUYes);
            fnClearListSelection(lstConfSpeedYes);
            fnClearListSelection(lstConfProYes);
            fnClearListSelection(lstWOYes);
        }   
        else if(hdnSubmitValue.value == "2")
        {
            fnClearListSelection(OrgList);
            fnClearListSelection(lstDepartment);
            fnClearListSelection(lstRmApprover);
            fnClearListSelection(lstMCUApprover);
            fnClearListSelection(lstWOAdmin);
            fnClearListSelection(lstConfType);
        }       
        else if(hdnSubmitValue.value == "3" || hdnSubmitValue.value == "4" || hdnSubmitValue.value == "5")
        {
            if(m == "" || m == "O")
            {
                fnClearListSelection(OrgList);
                fnClearListSelection(lstDepartment);

                if(hdnSubmitValue.value == "3")
                    fnClearListSelection(lstRmInfo);
                
                if(hdnSubmitValue.value == "4" || hdnSubmitValue.value == "5")
                    fnClearListSelection(lstEptDetailsM);
                    
                fnClearListSelection(lstTiers);
                fnClearListSelection(lstRmInfo);
            }
            
            if(m == "" || m == "C")
            {
            
                fnClearListSelection(lstConfStatus);
                fnClearListSelection(lstConfOccurence);
                fnClearListSelection(lstConfDetails);
                fnClearListSelection(lstEptDetails);
                fnClearListSelection(lstWODetails);
                fnClearListSelection(lstMCUDetails);
            }
        }
    }
    
    function fnClearListSelection(e)
    {   
        //OrgList.UnselectAll();
        
        if(e.GetSelectedItems().length > 0)
        {   
            var lstCnt = e.GetItemCount();
            
            var n =0;
            for (var i = 0; i < lstCnt; i++) 
            {
                TempList.InsertItem(n,e.GetItem(i).value,e.GetItem(i).text);                    
                n = n + 1;
            }
        
            e.ClearItems()
            
            n = 0;
            for (var j = 0; j < lstCnt; j++) 
            {
                e.InsertItem(n,TempList.GetItem(j).value,TempList.GetItem(j).text);                    
                n = n + 1;
            } 
            
            TempList.ClearItems()
        }
    }
    
</script>

<%--Get Input Values from Conference Menu--%>
<script type="text/javascript">

    function fnClosePopup()
    {
        MainPopup.Hide();
//        popup.Hide();
//        dptPopup.Hide();
        //assignmentPopup.Hide();
        //resourcePopup.Hide();
    }
    
    function fnGetConferenceMenuInput()
    {
        var Conference = "";
        var hosts = "";
        var participants = "";
        var scheduledConf = "";
        var confTypes = "";
        var confBehavior = "";
        var confTime = "";
        var duraion = "";
        var hdnMainIPValue = document.getElementById("hdnMainIPValue");
            hdnMainIPValue.value = "";
        var orgvalue = "";
        var departments = "";
        var rmIncharge = "";
        var mcuIncharge = "";
        var woIncharge = "";
        var attendees = "";
        var roomsParty = "";
        var endPoints = "";
        var mcu = "";
        var confSpeed = "";
        var conProtocol = "";
        var confWO = "";
        
        var hdnOkValue = document.getElementById("hdnOkValue");
            hdnOkValue.value = "1";
        
        if(chk_ConfID.GetChecked())
            Conference += "ConfnumName as [Conf.ID] ,";
        if(chk_ConfTitle.GetChecked())
            Conference += "ExternalName as [Conf.Title],";
        
        var hdnRightMenuValue = document.getElementById("hdnRightMenuValue");
            
        if(chk_Org.GetChecked())
        {
            //Conference += "OrgID,";
            
            hdnRightMenuValue.value = "";
            //Right Side Popups
            if(OrgList.GetSelectedItems().length >0)
            {
                var s = OrgList.GetSelectedItems()
                
                hdnRightMenuValue.value = "1:" + s[0].value + "|";
                //1:orgid;2:Departmentid;3(Rooms):LastName [Assistant Name],LastName as [Pri Appr.],LastName as [Sec. Appr. 1],LastName as [Sec. Appr. 2]
                //;4(MCU):LastName [Assistant Name],LastName as [Pri Appr.],LastName as [Sec. Appr. 1],LastName as [Sec. Appr. 2]
                //;5(WO):LastName [In Charge],
                //6:[Company Relationship];7:(invitee) 1 - External / 2 - Internal
                //8:(EP)0|1 End Point refer "GetConferenceEndpoint"
                //9:(MCU)0|1 can get from End Point
                //10:(Conf Speed)0|1 can get from End Point
                //11:(Conn Protocol)0|1 can get from End Point
                //12:(WO) 0|1 Name check with type is it A/V | CAT | HK
                if(lstDepartment.GetSelectedItems().length > 0)
                {
                    var i = 0;
                    var d = lstDepartment.GetSelectedItems()
                    
                    for(i = 0; i < d.length; i++)
                    {
                        if(d[i].value == "0")
                        {
                            var lstCnt = lstDepartment.GetItemCount();  
                            departments = "";                          
                            for (var lc = 0; lc < lstCnt; lc++)                             
                                departments += lstDepartment.GetItem(lc).value + ",";
                        }
                        else if(d[i].value == "-1")
                            departments = "";
                        else
                            departments += d[i].value +  ",";
                    }
                    
                    if(departments != "")
                        hdnRightMenuValue.value += "2:" + departments.substring(0, departments.length-1) + "|";                    
                    
                    //Assignments Menu Start
                    if(lstRmApprover.GetSelectedItems().length > 0)
                    {
                        var r = lstRmApprover.GetSelectedItems()
                    
                        for(i = 0; i < r.length; i++)
                        {
                           if(r[i].value == 5)
                            rmIncharge = "1,2,3,4,";
                           else
                            rmIncharge += r[i].value +  ",";
                        }
                        
                        if(rmIncharge != "")
                            hdnRightMenuValue.value += "3:" + rmIncharge.substring(0, rmIncharge.length-1) + "|";    
                    }
                    
                    if(lstMCUApprover.GetSelectedItems().length > 0)
                    {
                        lr = "";
                        var m = lstMCUApprover.GetSelectedItems()
                        
                        for(i = 0; i < m.length; i++)
                        {
                            if(m[i].value == 5)
                                mcuIncharge = "1,2,3,4,";
                            else
                                mcuIncharge += m[i].value +  ",";
                        }
                        
                        if(mcuIncharge != "")
                            hdnRightMenuValue.value += "4:" + mcuIncharge.substring(0, mcuIncharge.length-1) + "|";    
                    }
                    
                    if(lstWOAdmin.GetSelectedItems().length > 0)
                    {
                        var w = lstWOAdmin.GetSelectedItems()
                        for(i = 0; i < w.length; i++)
                        {
                            if(w[i].value == 3) 
                                woIncharge = "1,2,";
                            else                            
                                woIncharge += w[i].value +  ",";
                        }
                        if(woIncharge != "")
                            hdnRightMenuValue.value += "5:" + woIncharge.substring(0, woIncharge.length-1) + "|";    
                    }
                    //Assignments Menu End
                    
                    //Resource Menu Start
                    if(lstAttendee.GetSelectedItems().length > 0)
                    {
                        var at = lstAttendee.GetSelectedItems()
                        for(i = 0; i < at.length; i++)
                        {
                            if(at[i].value == 3) 
                                attendees = "1,2,";
                            else   
                                attendees += at[i].value +  ",";
                        }
                        
                        if(attendees != "")
                            hdnRightMenuValue.value += "6:" + attendees.substring(0, attendees.length-1) + "|";    
                    }
                    
                    if(lstRmType.GetSelectedItems().length > 0)
                    {
                        var rm = lstRmType.GetSelectedItems()
                        
                        for(i = 0; i < rm.length; i++)
                        {
                            if(rm[i].value == 3) 
                                attendees = "1,2,";
                            else                            
                                roomsParty += rm[i].value +  ",";
                        }
                        
                        if(roomsParty != "")
                            hdnRightMenuValue.value += "7:" + roomsParty.substring(0, roomsParty.length-1) + "|";    
                    }
                    
                    if(lstEPYes.GetSelectedItems().length > 0)
                    {
                        var ep = lstEPYes.GetSelectedItems()
                    
                        for(i = 0; i < ep.length; i++)
                            endPoints += ep[i].value +  ",";
                        
                        if(endPoints != "")
                            hdnRightMenuValue.value += "8:" + endPoints.substring(0, endPoints.length-1) + "|";    
                    }
                    
                    if(lstMCUYes.GetSelectedItems().length > 0)
                    {
                        var mc = lstMCUYes.GetSelectedItems()
                        for(i = 0; i < mc.length; i++)
                            mcu += mc[i].value +  ",";
                        
                        if(mcu != "")
                            hdnRightMenuValue.value += "9:" + mcu.substring(0, mcu.length-1) + "|";    
                    }
                    
                    if(lstConfSpeedYes.GetSelectedItems().length > 0)
                    {
                        var sp = lstConfSpeedYes.GetSelectedItems()
                        for(i = 0; i < sp.length; i++)
                            confSpeed += sp[i].value +  ",";
                        
                        if(confSpeed != "")
                            hdnRightMenuValue.value += "10:" + confSpeed.substring(0, confSpeed.length-1) + "|";    
                    }
                    var conProtocol = "";
                    if(lstConfProYes.GetSelectedItems().length > 0)
                    {
                        var pr = lstConfProYes.GetSelectedItems()
                        for(i = 0; i < pr.length; i++)
                            conProtocol += pr[i].value +  ",";
                        
                        if(conProtocol != "")
                            hdnRightMenuValue.value += "11:" + conProtocol.substring(0, conProtocol.length-1) + "|";    
                    }
                    
                    if(lstWOYes.GetSelectedItems().length > 0)
                    {
                        var wo = lstWOYes.GetSelectedItems()
                        for(i = 0; i < wo.length; i++)
                            confWO += wo[i].value +  ",";
                        
                        if(confWO != "")
                            hdnRightMenuValue.value += "12:" + confWO.substring(0, confWO.length-1) + "|";    
                    }
                    
                    //Resource Menu End                    
                }
            }
        }
        
        if(hdnRightMenuValue.value != "")
            Conference += "right,";
            
        if(Conference != "")
            hdnMainIPValue.value = "1:" + Conference.substring(0, Conference.length-1) + "|";
        
        if(chk_HFN.GetChecked())
            hosts += "h.FirstName as [Host First Name],";
        if(chk_HLN.GetChecked())
            hosts += "h.LastName as [Host Last Name],";
        if(chk_HMail.GetChecked())
            hosts += "h.Email as [Host Email],";
        if(chk_HRole.GetChecked())
            hosts += "hr.roleName as [Host Role],";
//        if(chk_HPerson.GetChecked())
//            hosts += "Person,";
            
        if(hosts != "")
            hdnMainIPValue.value += "2:" + hosts.substring(0, hosts.length-1) + "|";
            
        if(chk_PFN.GetChecked())
            participants += "p.FirstName as [Part. First Name],";
        if(chk_PLN.GetChecked())
            participants += "p.LastName as [Part. Last Name],";
        if(chk_PMail.GetChecked())
            participants += "p.Email as [Part. Email],";
        if(chk_PRole.GetChecked())
            participants += "p.roleName as [Part. Role],";
//        if(chk_PPerson.GetChecked())
//            participants += "Person,";
            
        if(participants != "")
            hdnMainIPValue.value += "3:" + participants.substring(0, participants.length-1) + "|";
        
        if(chk_Completed.GetChecked())
            scheduledConf += "7,";
        if(chk_Scheduled.GetChecked())
            scheduledConf += "0,";
        if(chk_Deleted.GetChecked())
            scheduledConf += "9,";
        if(chk_Terminated.GetChecked())
            scheduledConf += "3,";
        if(chk_Pending.GetChecked())
            scheduledConf += "1,";
            
        if(scheduledConf != "")
            hdnMainIPValue.value += "4:" + scheduledConf.substring(0, scheduledConf.length-1) + "|";
            
        if(chk_Audio.GetChecked())
            confTypes += "6,";
        if(chk_Video.GetChecked())
            confTypes += "2,";
        if(chk_Room.GetChecked())
            confTypes += "7,";    
        if(chk_PP.GetChecked())
            confTypes += "4,";   
//        if(chk_TP.GetChecked())
//            confTypes += "3,";      
       
        if(confTypes != "")
            hdnMainIPValue.value += "5:" + confTypes.substring(0, confTypes.length-1) + "|";
        
        if(chk_Single.GetChecked())
            confBehavior += "0,";
        if(chk_Recurring.GetChecked())
            confBehavior += "1,";          
       
        if(confBehavior != "")
            hdnMainIPValue.value += "6:" + confBehavior.substring(0, confBehavior.length-1) + "|";
            
        if(chk_Date.GetChecked())
            confTime += "ConfDate as [Date of Conf.],";
        if(chk_Time.GetChecked())
            confTime += "ConfTime [Start Time],";   
       
        if(confTime != "")
            hdnMainIPValue.value += "7:" + confTime.substring(0, confTime.length-1) + "|";
            
//        if(chk_Hours.GetChecked())
//            duraion += "Hours,";

        if(chk_Minutes.GetChecked())
            duraion += "a.Duration as [Duration],";          
       
        if(duraion != "")
            hdnMainIPValue.value += "8:" + duraion.substring(0, duraion.length-1);
    }
    
</script>

<%--Get Input Values from User Menu--%>
<script type="text/javascript">
    
    function fnGetUserMenuInput()
    {
        //hdnMainIPValue Values Start from 9
        var hdnMainIPValue = document.getElementById("hdnMainIPValue");
        var commonSelect = "";
        var conference = "";
        hdnMainIPValue.value = "";
        var hdnOkValue = document.getElementById("hdnOkValue");
        hdnOkValue.value = "1";
        
        var hdnRightMenuValue = document.getElementById("hdnRightMenuValue");
        
        if(chk_OrgU.GetChecked())
        {
            //conference += "OrgID,";
            hdnRightMenuValue.value = "";
            if(OrgList.GetSelectedItems().length >0)
            {
                var s = OrgList.GetSelectedItems()
                var departments = "";
                hdnRightMenuValue.value = "1:" + s[0].value + "|";
                if(lstDepartment.GetSelectedItems().length > 0)
                {
                    var i = 0;
                    var d = lstDepartment.GetSelectedItems()
                    
                    for(i = 0; i < d.length; i++)
                    {
                        if(d[i].value == "0")
                        {
                            var lstCnt = lstDepartment.GetItemCount();  
                            departments = "";                          
                            for (var lc = 0; lc < lstCnt; lc++)                             
                                departments += lstDepartment.GetItem(lc).value + ",";
                        }
                        else if(d[i].value == "-1")
                            departments = "";
                        else
                            departments += d[i].value +  ",";
                    }
                    
                    if(departments != "")
                        hdnRightMenuValue.value += "2:" + departments.substring(0, departments.length-1) + "|";     
                        
                    if(lstConfType.GetSelectedItems().length > 0)
                    {
                        var r = lstConfType.GetSelectedItems()
                        var conftypeVal = "";
                        for(i = 0; i < r.length; i++)
                        {
                           if(r[i].value == 4)
                            conftypeVal = "6,2,7,";
                           else
                            conftypeVal += r[i].value +  ",";
                        }
                        
                         if(conftypeVal != "")
                            hdnMainIPValue.value += "5:" + conftypeVal.substring(0, conftypeVal.length-1) + "|";    
                    }
                    
                    var rmIncharge = "";
                    var mcuIncharge = "";
                    var woIncharge = "";
                    //Assignments Menu Start
                    if(lstRmApprover.GetSelectedItems().length > 0)
                    {
                        var r = lstRmApprover.GetSelectedItems()
                    
                        for(i = 0; i < r.length; i++)
                        {
                           if(r[i].value == 5)
                            rmIncharge = "1,2,3,4,";
                           else
                            rmIncharge += r[i].value +  ",";
                        }
                        
                        if(rmIncharge != "")
                            hdnRightMenuValue.value += "3:" + rmIncharge.substring(0, rmIncharge.length-1) + "|";    
                    }
                    
                    if(lstMCUApprover.GetSelectedItems().length > 0)
                    {
                        lr = "";
                        var m = lstMCUApprover.GetSelectedItems()
                        
                        for(i = 0; i < m.length; i++)
                        {
                            if(m[i].value == 5)
                                mcuIncharge = "1,2,3,4,";
                            else
                                mcuIncharge += m[i].value +  ",";
                        }
                        
                        if(mcuIncharge != "")
                            hdnRightMenuValue.value += "4:" + mcuIncharge.substring(0, mcuIncharge.length-1) + "|";    
                    }
                    
                    if(lstWOAdmin.GetSelectedItems().length > 0)
                    {
                        var w = lstWOAdmin.GetSelectedItems()
                        for(i = 0; i < w.length; i++)
                        {
                            if(w[i].value == 3) 
                                woIncharge = "1,2,";
                            else                            
                                woIncharge += w[i].value +  ",";
                        }
                        if(woIncharge != "")
                            hdnRightMenuValue.value += "5:" + woIncharge.substring(0, woIncharge.length-1) + "|";    
                    }
                    //Assignments Menu End
                }
            }
        }        
        
        if(hdnRightMenuValue.value != "")
            conference += "right,";
        
        if(conference != "")
            hdnMainIPValue.value = "1:" + conference.substring(0, conference.length-1) + "|";
            
        var hosts = "";
        if(chk_ConfHostU.GetChecked())
            hosts += "h.FirstName as [Host First Name], h.LastName as [Host Last Name],";
            
        if(hosts != "")
            hdnMainIPValue.value += "2:" + hosts.substring(0, hosts.length-1) + "|";
          
        var participants = "";
        if(chk_ConfPartU.GetChecked())
            participants = "p.FirstName as [Part. First Name],p.LastName as [Part. Last Name],p.Email as [Part. Email],";
            
        if(participants != "")
            hdnMainIPValue.value += "3:" + participants.substring(0, participants.length-1) + "|";
        
        if(chk_FNU.GetChecked())
            commonSelect += "a.FirstName as [First Name],";
        if(chk_LNU.GetChecked())
            commonSelect += "a.LastName as [Last Name],";
        if(chk_UserEmailU.GetChecked())
            commonSelect += "a.Email,";
        if(chk_SecondaryEmailU.GetChecked())
            commonSelect += "a.AlternativeEmail as [Secondary Email],";
        if(chk_RoleU.GetChecked())
            commonSelect += "c.RoleName as [Role Name],";
        if(chk_TimeZoneU.GetChecked())
            commonSelect += "d.StandardName as [Time Zone],";
        if(chk_AccExpirU.GetChecked())
            commonSelect += "a.AccountExpiry as [Account Expiration],";
        if(chk_MinU.GetChecked())
            commonSelect += "e.Timeremaining as [Minutes],";
            
        if(commonSelect != "")
            hdnMainIPValue.value += "9:" + commonSelect.substring(0, commonSelect.length-1) + "|";
        
        if(chk_ExchangeU.GetChecked())                     
            hdnMainIPValue.value += "10:1|";
        
        if(chk_DominoU.GetChecked())                     
            hdnMainIPValue.value += "11:1|";
    }
</script>

<%--Get Input Values from Room Menu--%>
<script type="text/javascript">
    
    function ShowConfMenu(s)
    {   
    
//        if(s.GetSelectedItems().length >0)
//        {
            var items = s.GetSelectedItems()
            var isEptChecked = 0
            var isMCUChecked = 0
            var isWOChecked = 0
            
            for(i = 0; i < items.length; i++)
            {
                if(items[i].value == 3)
                    isEptChecked = 1;
                else if(items[i].value == 5)
                    isMCUChecked = 1;
                else if(items[i].value == 6)
                    isWOChecked = 1;
                else if(items[i].value == 7)
                {
                    isEptChecked = 1;
                    isWOChecked = 1;
                    isMCUChecked = 1;
                }
            }
            
            if(isEptChecked == 1)
                eptDetailsPopup.ShowAtPos(680, 310);
            else
                eptDetailsPopup.Hide();
                
            if(isMCUChecked == 1)
            {
                if("<%=Session["browser"]%>" == "Firefox") 
                    mcuDetailsPopup.ShowAtPos(365, 442);
                else
                    mcuDetailsPopup.ShowAtPos(365, 610);
            }
            else
                mcuDetailsPopup.Hide();
                
            if(isWOChecked == 1)
                woDetailsPopup.ShowAtPos(665, 665);
            else
                woDetailsPopup.Hide();
//        }
    }
    
    function fnBehaviorAllChecked()
    {   
        if(chk_VideoConferenceR.GetChecked() || chk_AudioConferenceR.GetChecked() || chk_RoomConferenceR.GetChecked() || chk_AllBehaviorR.GetChecked() 
        || chk_ConfE.GetChecked() || chk_ConfM.GetChecked()) 
            confStatusPopup.ShowAtPos(205, 265);
        else
        {
            fnRightParamsClear('C');
            
            confStatusPopup.Hide();
            confOccurencePopup.Hide();
            confDetailsPopup.Hide();
            eptDetailsPopup.Hide();
            woDetailsPopup.Hide();
            mcuDetailsPopup.Hide();
        }
    }
    
    function fnGetRoomMenuInput()
    {
        //hdnMainIPValue Values Start from 12
        //hdnRightMenuValue Values Start from 13
        var hdnMainIPValue = document.getElementById("hdnMainIPValue");
        hdnMainIPValue.value = "";
        var commonSelect = "";
        var conference = "";
        var hdnOkValue = document.getElementById("hdnOkValue");
        hdnOkValue.value = "1";
            
        if(chk_TiersR.GetChecked())
            commonSelect += " [Top Tier], [Middle Tier],";
        if(chk_NameR.GetChecked())
            commonSelect += " a.[Name],";
            
        var hdnRightMenuValue = document.getElementById("hdnRightMenuValue");
            hdnRightMenuValue.value = "";
            
        if(chk_OrgR.GetChecked())
        {
            //conference += "OrgID,";
            
            if(OrgList.GetSelectedItems().length >0)
            {
                var s = OrgList.GetSelectedItems()
                
                var departments = "";
                hdnRightMenuValue.value = "1:" + s[0].value + "|";
                if(lstDepartment.GetSelectedItems().length > 0)
                {
                    var i = 0;
                    var d = lstDepartment.GetSelectedItems()
                    
                    for(i = 0; i < d.length; i++)
                    {
                        if(d[i].value == "0")
                        {
                            var lstCnt = lstDepartment.GetItemCount();  
                            departments = "";                          
                            for (var lc = 0; lc < lstCnt; lc++)                             
                                departments += lstDepartment.GetItem(lc).value + ",";
                        }
                        else if(d[i].value == "-1")
                            departments = "";
                        else
                            departments += d[i].value +  ",";
                    }
                    
                    if(departments != "")
                        hdnRightMenuValue.value += "2:" + departments.substring(0, departments.length-1) + "|";     
                    
                }
            }
        }
        
        var rmIncharge = "";
       
        //Assignments Menu Start
        if(lstRmApprover.GetSelectedItems().length > 0)
        {
            var r = lstRmApprover.GetSelectedItems()
        
            for(i = 0; i < r.length; i++)
            {
               if(r[i].value == "5")
                rmIncharge = "1,2,3,4,";
               else
                rmIncharge += r[i].value +  ",";
            }
            
            if(rmIncharge != "")
                hdnRightMenuValue.value += "3:" + rmIncharge.substring(0, rmIncharge.length-1) + "|";    
        }
        //Assignments Menu End
                
        var tierInfo = "";
        if(lstTiers.GetSelectedItems().length > 0)
        {
            var ti = lstTiers.GetSelectedItems()
            for(i = 0; i < ti.length; i++)
                tierInfo += ti[i].value + ",";
        }
        
        if(tierInfo != "")
            hdnRightMenuValue.value += "20:" + tierInfo.substring(0, tierInfo.length-1) + "|";  
                        
        var rminfo = "";
        if(lstRmInfo.GetSelectedItems().length > 0)
        {
            var l = lstRmInfo.GetSelectedItems()
            for(i = 0; i < l.length; i++)
            {
                if(l[i].value == "1") commonSelect += " RoomPhone,";
                if(l[i].value == "2") commonSelect += " Capacity,";
                if(l[i].value == "3") commonSelect += " Address1,";
                if(l[i].value == "4") commonSelect += " Address2,";
                if(l[i].value == "5") commonSelect += " RoomFloor,";
                if(l[i].value == "6") commonSelect += " RoomNumber,";
                if(l[i].value == "7") commonSelect += " City,";
                if(l[i].value == "8") commonSelect += " State,";
                if(l[i].value == "9") commonSelect += " Country,";
                if(l[i].value == "10") commonSelect += " ZipCode,";
                if(l[i].value == "11") commonSelect += " TimeZone,";
                if(l[i].value == "12") commonSelect += " [Primary Approver],";
                if(l[i].value == "13") commonSelect += " [Sec. Approver 1],";
                if(l[i].value == "14") commonSelect += " [Sec. Approver 2],";
                
                if(l[i].value == "15")
                {
                    commonSelect += "RoomPhone,Capacity,Address1,Address2,RoomFloor,RoomNumber,City,State,Country,ZipCode,[Primary Approver]";
                    commonSelect += ",[Sec. Approver 1],[Sec. Approver 2],";
                }
            }
        }
        
         var rmAssetinfo = "";
        if(lstRmAsset.GetSelectedItems().length > 0)
        {
            var ra = lstRmAsset.GetSelectedItems()
            for(i = 0; i < ra.length; i++)
            {
                if(ra[i].value == "3")
                    rmAssetinfo = "1,2";
                else 
                    rmAssetinfo += ra[i].value;
            }
            
            if(rmAssetinfo != "")
                hdnRightMenuValue.value += "18:" + rmAssetinfo.substring(0, rmAssetinfo.length-1) + "|";    
        }
                
        if(hdnRightMenuValue.value != "")
            conference += "right,"
        
        if(conference != "")
            hdnMainIPValue.value = "1:" + conference.substring(0, conference.length-1) + "|";
            
        var hosts = "";
        if(chk_HostR.GetChecked())
            hosts += "h.FirstName as [Host First Name], h.LastName as [Host Last Name],";
            
        if(hosts != "")
            hdnMainIPValue.value += "2:" + hosts.substring(0, hosts.length-1) + "|";
        
        var rmType = "";
        if(chk_VideoRoomR.GetChecked())
            rmType += "2,";
        if(chk_AudioRoomR.GetChecked())
            rmType += "1,";
        if(chk_MeetingRoomR.GetChecked())
            rmType += "0,";  
        
        if(rmType != "")
            hdnMainIPValue.value += "12:" + rmType.substring(0, rmType.length-1) + "|";
            
        var confType = "";
        if(chk_VideoConferenceR.GetChecked())
            confType += "2,";
        if(chk_AudioConferenceR.GetChecked())
            confType += "6,";
        if(chk_RoomConferenceR.GetChecked())
            confType += "7,";  
        
        if(confType != "") //If Behavior Checkbox's clicked need to handle right menus
        {
            hdnMainIPValue.value += "5:" + confType.substring(0, confType.length-1) + "|";
        
            var scheduledConf = "";
            var immediate = "";
            if(lstConfStatus.GetSelectedItems().length > 0)
            {
                var cs = lstConfStatus.GetSelectedItems()
                        
                for(i = 0; i < cs.length; i++)
                {
                    if(cs[i].value == "15")
                        immediate = "1," ;
                    if(cs[i].value == "16")
                        scheduledConf = "7,0,9,3,1,";
                    else
                        scheduledConf += cs[i].value +  ",";
                }
            }
            
            if(scheduledConf != "")
                hdnMainIPValue.value += "4:" + scheduledConf.substring(0, scheduledConf.length-1) + "|";
            
            if(immediate != "")
                hdnMainIPValue.value += "13:" + immediate.substring(0, immediate.length-1) + "|";
            
            var behavConf = "";
            if(lstConfOccurence.GetSelectedItems().length > 0)
            {
                var co = lstConfOccurence.GetSelectedItems()
                for(i = 0; i < co.length; i++)
                {
                    if(co[i].value == "2")
                        behavConf = "0,1,";
                    else
                        behavConf += co[i].value +  ",";
                }
            }
           
            if(behavConf != "")
                hdnMainIPValue.value += "6:" + behavConf.substring(0, behavConf.length-1) + "|";
            
            var subConfSelect = "";
            if(lstConfDetails.GetSelectedItems().length > 0)
            {
                var cd = lstConfDetails.GetSelectedItems()
                
                var eptDetails = "";
                var mcuDetails = "";
                var woDetails = "";
                       
                for(i = 0; i < cd.length; i++)
                {
                    if(cd[i].value == "1")
                        subConfSelect += "ConfNumName,";
                    else if(cd[i].value == "2")
                        subConfSelect += "ExternalName,";
                    else if(cd[i].value == "3")
                    {
                        if(lstEptDetails.GetSelectedItems().length > 0)
                        {
                            var e = lstEptDetails.GetSelectedItems()
                            for(j = 0; j < e.length; j++)
                            {
                                if(e[j].value == "5")
                                    eptDetails = "1,2,3,4,";
                                else 
                                    eptDetails += e[j].value +  ",";
                            }
                                
                           if(eptDetails != "")
                                hdnMainIPValue.value += "15:" + eptDetails.substring(0, eptDetails.length-1) + "|";
                        }
                    }
                    else if(cd[i].value == "5")
                    {
                        if(lstMCUDetails.GetSelectedItems().length > 0)
                        {
                        var k = 0;
                            var m = lstMCUDetails.GetSelectedItems()
                            for(k = 0; k < m.length; k++)       
                            {
                                if(m[k].value == "4")                         
                                    mcuDetails = "1,2,3,";
                                else
                                    mcuDetails += m[k].value +  ",";
                            }
                                
                           if(mcuDetails != "")
                                hdnMainIPValue.value += "16:" + mcuDetails.substring(0, mcuDetails.length-1) + "|";
                        }
                    }
                    else if(cd[i].value == "6")
                    {
                        if(lstWODetails.GetSelectedItems().length > 0)
                        {
                            var m = lstWODetails.GetSelectedItems()
                            for(l = 0; l < m.length; l++)      
                            {                          
                                if(m[l].value == "4")
                                    woDetails =  "1,2,3,";
                                else
                                    woDetails += m[l].value +  ",";
                            }
                                
                           if(woDetails != "")
                                hdnMainIPValue.value += "17:" + woDetails.substring(0, woDetails.length-1) + "|";
                        }
                    }
                }
            }
           
            if(subConfSelect != "")
                hdnMainIPValue.value += "14:" + subConfSelect.substring(0, subConfSelect.length-1) + "|";
        }
       
        if(commonSelect == "")
            commonSelect = " a.[Name],";
            
        if(commonSelect != "")
            hdnMainIPValue.value += "9:" + commonSelect.substring(0, commonSelect.length-1) + "|";
        
    }
</script>

<%--Get Input Values from Endpoint Menu--%>
<script type="text/javascript">
    
    function fnGetEndPointMenuInput()
    {
        //hdnMainIPValue Values Start from 12
        //hdnRightMenuValue Values Start from 13
        var hdnMainIPValue = document.getElementById("hdnMainIPValue");
        var commonSelect = "";
        
        if(chk_DeptE.GetChecked())
            commonSelect += "[Top Tier], [Middle Tier],";
        if(chk_RmNameE.GetChecked())
            commonSelect += " a.[Name],";
            
        var hdnOkValue = document.getElementById("hdnOkValue");
        hdnOkValue.value = "1";
        var hdnRightMenuValue = document.getElementById("hdnRightMenuValue");
        
        var conference = ""; 
        
        if(chk_OrgE.GetChecked() || chk_OrgM.GetChecked())
        {
            //conference += "OrgID,";            
            
            if(OrgList.GetSelectedItems().length >0)
            {
                var s = OrgList.GetSelectedItems()
                
                var departments = "";
                hdnRightMenuValue.value = "1:" + s[0].value + "|";
                if(lstDepartment.GetSelectedItems().length > 0)
                {
                    var i = 0;
                    var d = lstDepartment.GetSelectedItems()
                    
                    for(i = 0; i < d.length; i++)
                    {
                        if(d[i].value == "0")
                        {
                            var lstCnt = lstDepartment.GetItemCount();  
                            departments = "";                          
                            for (var lc = 0; lc < lstCnt; lc++)                             
                                departments += lstDepartment.GetItem(lc).value + ",";
                        }
                        else if(d[i].value == "-1")
                            departments = "";
                        else
                            departments += d[i].value +  ",";
                    }
                    
                    if(departments != "")
                        hdnRightMenuValue.value += "2:" + departments.substring(0, departments.length-1) + "|";     
                }
            }
        }
        
        
        var rmIncharge = "";
        var mcuIncharge = "";
        var woIncharge = "";
        //Assignments Menu Start
        if(lstRmApprover.GetSelectedItems().length > 0)
        {
            var r = lstRmApprover.GetSelectedItems()
        
            for(i = 0; i < r.length; i++)
            {
               if(r[i].value == "5")
                rmIncharge = "1,2,3,4,";
               else
                rmIncharge += r[i].value +  ",";
            }
            
            if(rmIncharge != "")
                hdnRightMenuValue.value += "3:" + rmIncharge.substring(0, rmIncharge.length-1) + "|";    
        }
        if(lstMCUApprover.GetSelectedItems().length > 0)
        {
            lr = "";
            var m = lstMCUApprover.GetSelectedItems()
            
            for(i = 0; i < m.length; i++)
            {
                if(m[i].value == 5)
                    mcuIncharge = "1,2,3,4,";
                else
                    mcuIncharge += m[i].value +  ",";
            }
            
            if(mcuIncharge != "")
                hdnRightMenuValue.value += "4:" + mcuIncharge.substring(0, mcuIncharge.length-1) + "|";    
        }
        
        if(lstWOAdmin.GetSelectedItems().length > 0)
        {
            var w = lstWOAdmin.GetSelectedItems()
            for(i = 0; i < w.length; i++)
            {
                if(w[i].value == 3) 
                    woIncharge = "1,2,";
                else                            
                    woIncharge += w[i].value +  ",";
            }
            if(woIncharge != "")
                hdnRightMenuValue.value += "5:" + woIncharge.substring(0, woIncharge.length-1) + "|";    
        }
        
        //Assignments Menu End
        
        var tierInfo = "";
        if(lstTiers.GetSelectedItems().length > 0)
        {
            var ti = lstTiers.GetSelectedItems()
            for(i = 0; i < ti.length; i++)
                tierInfo += ti[i].value + ",";
        }
        
        if(tierInfo != "")
            hdnRightMenuValue.value += "20:" + tierInfo.substring(0, tierInfo.length-1) + "|";  
            
        var rminfo = "";
        if(lstRmInfo.GetSelectedItems().length > 0)
        {
            var l = lstRmInfo.GetSelectedItems()
            for(i = 0; i < l.length; i++)
            {
                if(l[i].value == "1") commonSelect += " RoomPhone,";
                if(l[i].value == "2") commonSelect += " Capacity,";
                if(l[i].value == "3") commonSelect += " Address1,";
                if(l[i].value == "4") commonSelect += " Address2,";
                if(l[i].value == "5") commonSelect += " RoomFloor,";
                if(l[i].value == "6") commonSelect += " RoomNumber,";
                if(l[i].value == "7") commonSelect += " City,";
                if(l[i].value == "8") commonSelect += " State,";
                if(l[i].value == "9") commonSelect += " Country,";
                if(l[i].value == "10") commonSelect += " ZipCode,";
                if(l[i].value == "11") commonSelect += " TimeZone,";
                if(l[i].value == "12") commonSelect += " [Primary Approver],";
                if(l[i].value == "13") commonSelect += " [Sec. Approver 1],";
                if(l[i].value == "14") commonSelect += " [Sec. Approver 2],";
                
                if(l[i].value == "15")
                {
                    commonSelect += "RoomPhone,Capacity,Address1,Address2,RoomFloor,RoomNumber,City,State,Country,ZipCode,[Primary Approver]";
                    commonSelect += ",[Sec. Approver 1],[Sec. Approver 2],";
                }
            }
        }
        
        var rmAssetinfo = "";
        if(lstRmAsset.GetSelectedItems().length > 0)
        {
            var ra = lstRmAsset.GetSelectedItems()
            for(i = 0; i < ra.length; i++)
            {
                if(ra[i].value == "3")
                    rmAssetinfo = "1,2";
                else 
                    rmAssetinfo += ra[i].value;
            }
            
            if(rmAssetinfo != "")
                hdnRightMenuValue.value += "18:" + rmAssetinfo.substring(0, rmAssetinfo.length-1) + "|";    
        }
        
        var eptInfo = "";
        
        if(chk_ENameE.GetChecked())
            eptInfo += "[Endpoint Name],";
            
        if(lstEptDetailsM.GetSelectedItems().length > 0)
        {
            var ed = lstEptDetailsM.GetSelectedItems()
            for(i = 0; i < ed.length; i++)
            {
                if(ed[i].value == "1") eptInfo += " [Default Profile Name],";
                if(ed[i].value == "2") eptInfo += " [Number of Profiles],";
                if(ed[i].value == "3") eptInfo += " [Address Type],";
                if(ed[i].value == "4") eptInfo += " Address,";
                if(ed[i].value == "5") eptInfo += " Model,";
                if(ed[i].value == "6") eptInfo += " [Preferred Bandwidth],";
                if(ed[i].value == "7") eptInfo += " [MCU Assignment],";
                if(ed[i].value == "8") eptInfo += " [Pre. Dialing Option],";
                if(ed[i].value == "9") eptInfo += " [Default Protocol],";
                if(ed[i].value == "10") eptInfo += " [Web Access URL],";
                if(ed[i].value == "11") eptInfo += " [Network Location],";                            
                if(ed[i].value == "12") eptInfo += " [Telnet Enabled],";
                if(ed[i].value == "13") eptInfo += " [Email ID],";
                if(ed[i].value == "14") eptInfo += " [API Port],";
                if(ed[i].value == "15") eptInfo += " [iCal Invite],";
                
                if(ed[i].value == "16")
                {
                     eptInfo = " [Endpoint Name],[Default Profile Name], [Number of Profiles], [Address Type], Address, Model, [Preferred Bandwidth],";
                     eptInfo += " [MCU Assignment], [Pre. Dialing Option], [Default Protocol], [Web Access URL], [Network Location], ";
                     eptInfo += " [Primary Approver], [Telnet Enabled], [Email ID], [API Port], [iCal Invite],";
                }
            }
            
            if(eptInfo != "")
                hdnMainIPValue.value += "19:" + eptInfo.substring(0, eptInfo.length-1) + "|";
        }
        
        if(hdnRightMenuValue.value != "")
            conference += "right,"
            
        if(conference != "")
            hdnMainIPValue.value += "1:" + conference.substring(0, conference.length-1) + "|";
                
        if(chk_ConfE.GetChecked())//If Behavior Checkbox's clicked need to handle right menus
        {
            var scheduledConf = "";
            var immediate = "";
            if(lstConfStatus.GetSelectedItems().length > 0)
            {
                var cs = lstConfStatus.GetSelectedItems()
                        
                for(i = 0; i < cs.length; i++)
                {
                    if(cs[i].value == "9")
                        immediate = "1," ;
                    if(cs[i].value == "16")
                        scheduledConf = "7,0,9,3,1,";
                    else
                        scheduledConf += cs[i].value +  ",";
                }
            }
            
            if(scheduledConf != "")
                hdnMainIPValue.value += "4:" + scheduledConf.substring(0, scheduledConf.length-1) + "|";
            
            if(immediate != "")
                hdnMainIPValue.value += "13:" + immediate.substring(0, immediate.length-1) + "|";
            
            var behavConf = "";
            if(lstConfOccurence.GetSelectedItems().length > 0)
            {
                var co = lstConfOccurence.GetSelectedItems()
                for(i = 0; i < co.length; i++)
                {
                    if(co[i].value == "2")
                        behavConf = "0,1,";
                    else
                        behavConf += co[i].value +  ",";
                }
            }
           
            if(behavConf != "")
                hdnMainIPValue.value += "6:" + behavConf.substring(0, behavConf.length-1) + "|";
            
            var subConfSelect = "";
            if(lstConfDetails.GetSelectedItems().length > 0)
            {
                var cd = lstConfDetails.GetSelectedItems()
                
                var eptDetails = "";
                var mcuDetails = "";
                var woDetails = "";
                       
                for(i = 0; i < cd.length; i++)
                {
                    if(cd[i].value == "1")
                        subConfSelect += "ConfNumName,";
                    else if(cd[i].value == "2")
                        subConfSelect += "ExternalName,";
                    else if(cd[i].value == "3")
                    {
                        if(lstEptDetails.GetSelectedItems().length > 0)
                        {
                            var e = lstEptDetails.GetSelectedItems()
                            for(i = 0; i < e.length; i++)                                
                                eptDetails += e[i].value +  ",";
                                
                           if(eptDetails != "")
                                hdnMainIPValue.value += "15:" + eptDetails.substring(0, eptDetails.length-1) + "|";
                        }
                    }
                    else if(cd[i].value == "5")
                    {
                        if(lstMCUDetails.GetSelectedItems().length > 0)
                        {
                            var m = lstMCUDetails.GetSelectedItems()
                            for(i = 0; i < m.length; i++)                                
                                mcuDetails += m[i].value +  ",";
                                
                           if(mcuDetails != "")
                                hdnMainIPValue.value += "16:" + mcuDetails.substring(0, mcuDetails.length-1) + "|";
                        }
                    }
                    else if(cd[i].value == "6")
                    {
                        if(lstWODetails.GetSelectedItems().length > 0)
                        {
                            var m = lstWODetails.GetSelectedItems()
                            for(i = 0; i < m.length; i++)                                
                                woDetails += m[i].value +  ",";
                                
                           if(woDetails != "")
                                hdnMainIPValue.value += "17:" + woDetails.substring(0, woDetails.length-1) + "|";
                        }
                    }
                }
            }
           
            if(subConfSelect != "")
                hdnMainIPValue.value += "14:" + subConfSelect.substring(0, subConfSelect.length-1) + "|";
        }
       
            
        if(commonSelect != "")
            hdnMainIPValue.value += "9:" + commonSelect.substring(0, commonSelect.length-1) + "|";
    }
</script>

<%--Save funtion--%>
<script  type="text/javascript">

function fnSave()
{

    //var errLabel = document.getElementById('errLabel');
    
    if(txtSaveReport.GetValue().trim() == "Report Name..." || txtSaveReport.GetValue().trim() == "")
    {
        alert("有効なレポート名を入力してください。");
//        if(errLabel)
//        {
//            errLabel.innerText = "Please enter the valid Report Name.";
//            errLabel.style.display = "";
//        }
        return false;
    }
    else
    {
        var hdnSave = document.getElementById("hdnSave");
        hdnSave.value = "1";
        fnSubmit();
        fnClose();
        
        return true;
        //cmdConfOk.DoClick();
    }
}

function OntextFocus(s, e) 
{
    var ele = s.GetInputElement();
	if (ele.value == "Report Name...")
		ele.value = "";
	else if(ele.value.trim() == "")
		ele.value = "Report Name...";
}

function OntextunFocus(s, e) 
{
    var ele = s.GetInputElement();
	
	if(ele.value.trim() == "")
		ele.value = "Report Name...";
}

function fnValidation()
{
    var sDate = startDateedit.GetDate();
    var eDate = endDateedit.GetDate();
    
    if(sDate == null || eDate == null)
    {
        var isExport = confirm("任意の開始/終了時刻を選択されていません。この要求は、あなたが続行してもよろしいです時間がかかる場合があります。!")
        
        if(isExport == false)
            return false;
        else
            return true;
    }
    
    return true;
}

    
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head> 
    <title>レポート</title>    
    <script type="text/javascript" src="inc/functions.js"></script>
    <style type="text/css">   
    LABEL 
    {
        FONT-SIZE: 8pt;
        VERTICAL-ALIGN: top;
        COLOR: black;
        FONT-FAMILY: Arial, Helvetica;
        TEXT-DECORATION: none;
    }    
   
    </style>
</head>
<body>
    <form id="frmReport" runat="server">   
    <asp:ScriptManager ID="ScriptManager1" runat="server" > </asp:ScriptManager>
    <input type="hidden" id="hdnMainIPValue" runat="server" />
    <input type="hidden" id="hdnDepartmentList" runat="server" />
    <input type="hidden" id="hdnRightMenuValue" runat="server" />
    <input type="hidden" id="hdnOkValue" runat="server" />
    <input type="hidden" id="hdnSubmitValue" runat="server" />
    <input type="hidden" id="hdnSave" runat="server" />
    <input type="hidden" id="hdnReportNames" runat="server" />
    <input type="hidden" id="hdnAllReports" runat="server" />
    <table width="100%" border="1" style="border-color:Blue;border-style:solid;" cellpadding="0" cellspacing="0">
        <tr>
            <td colspan="2" style="border-bottom-color:Blue;" bgcolor="#666699" align="center" height="10px" >
                <h3 style="color:White"> 
                      <asp:Label ID="lblHeading" runat="server">レポート</asp:Label>
                </h3>                
            </td>
        </tr>
        <tr>
            <td colspan="2" style="border-bottom-color:Blue;">
                <table width="100%">
                    <tr>
                        <td align="center">
                            <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError" ></asp:Label><br />
                        </td>
                    </tr>
                    <tr>
                        <td >
                            <table width="100%" border="0" style="left: 0;" id="tbleBtns">
                                <tr>
                                    <td width="10%" class="blackblodtext">レポートタイプ
                                         <dx:ASPxRadioButton ID="rbAdhoc" runat="server" Text="アドホック" GroupName="ReportType" Checked="True" TextWrap="False" ClientInstanceName="rbAdhoc">                                            
                                            <ClientSideEvents CheckedChanged="function(s, e){SavedRptListPopup.Hide();btnPreview.SetVisible(true);}" />
                                        </dx:ASPxRadioButton>  
                                    </td>
                                    <td width="8%" class="blackblodtext">&nbsp;
                                        <dx:ASPxRadioButton ID="rbSaved" runat="server" Text="保存" GroupName="ReportType"  TextWrap="False" ClientInstanceName="rbSaved">   
                                            <ClientSideEvents CheckedChanged="function(s, e){btnPreview.SetVisible(false);}" />
                                        </dx:ASPxRadioButton> 
                                    </td>
                                    <td width="10%" class="blackblodtext">開始日
                                       <dx:ASPxDateEdit ID="startDateedit" ClientInstanceName="startDateedit" runat="server" Width="100px">                                        
                                       </dx:ASPxDateEdit>
                                    </td>
                                    <td width="15%" class="blackblodtext">終了日
                                       <dx:ASPxDateEdit ID="endDateedit" ClientInstanceName="endDateedit" runat="server" Width="100px">                                       
                                       </dx:ASPxDateEdit>
                                    </td>	
                                     <td width="10%" class="blackblodtext">&nbsp;
                                       <dx:ASPxButton ID="btnPreview" runat="server" CssFilePath="../en/App_Themes/Glass/{0}/styles.css"
                                            CssPostfix="Glass" SpriteCssFilePath="../en/App_Themes/Glass/{0}/sprite.css"
                                            Text="一覧" Width="82px" OnClick="btnOk_Click">
                                            <ClientSideEvents Click="function(s, e) {e.processOnServer = fnCheck();fnSubmit(); } " />
                                        </dx:ASPxButton>                                      
                                    </td>							                                    
                                    <td align="right">
                                        <dx:ASPxMenu ID="menuExport" runat="server" AutoSeparators="RootOnly" CssFilePath="../en/App_Themes/Plastic Blue/{0}/styles.css"
                                            CssPostfix="PlasticBlue" ImageFolder="../en/App_Themes/Plastic Blue/{0}/" ItemSpacing="0px"
                                            SeparatorHeight="100%" SeparatorWidth="2px" OnItemClick="menuExport_OnItemClick">
                                            <SeparatorBackgroundImage ImageUrl="../en/App_Themes/Plastic Blue/Web/mSeparatorHor.gif"
                                                VerticalPosition="Top" />
                                            <Items>
                                                <dx:MenuItem Text="XLS" Name="mXls">
                                                    <Image Url="../en/App_Themes/Plastic Blue/Web/Ntable-excel.png" />
                                                </dx:MenuItem>
                                                <dx:MenuItem Text="PDF" Name="mPdf">
                                                    <Image Url="../en/App_Themes/Plastic Blue/Web/NPDFpage_white_acrobat.png" />
                                                </dx:MenuItem>
                                                <dx:MenuItem Text="RTF" Name="mDoc">
                                                    <Image Url="../en/App_Themes/Plastic Blue/Web/Nwordpage_white_word.png" />
                                                </dx:MenuItem>                                               
                                            </Items>
                                            <ItemSubMenuOffset FirstItemY="-1" LastItemY="-1" Y="-1" />
                                            <RootItemSubMenuOffset FirstItemX="1" LastItemX="1" X="1" />
                                            <SubMenuStyle GutterWidth="0px" />
                                            <ClientSideEvents ItemClick="function(s, e) {e.processOnServer = fnValidation();} " />
                                        </dx:ASPxMenu>
                                        <span class="lblError"> * メニュー/サブメニューを選択した後、"プレビュー"をクリックしてください.</span>
                                    </td>
                                </tr>
                             </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td width="130px" style="border-right-color:Blue;" valign="top"> <%--FB 2050--%>
                <table cellpadding="0" cellspacing="0" >
                    <tr>
                        <td>           
                            <ajax:ModalPopupExtender ID="ConferencePopUp" runat="server" TargetControlID="ChgOrg"
                                 PopupControlID="ConferencePnl" DropShadow="false" Drag="true" CancelControlID="ClosePUp">
                            </ajax:ModalPopupExtender>                
                            <asp:Panel ID="ConferencePnl" runat="server" HorizontalAlign="Center" >
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <dx:ASPxPopupControl ID="MainPopupup" runat="server" CssFilePath="../en/App_Themes/Plastic Blue/{0}/styles.css"
                                                CssPostfix="PlasticBlue" HeaderText="会議 選択" ImageFolder="../en/App_Themes/Plastic Blue/{0}/"
                                                 PopupHorizontalAlign="WindowCenter"  HeaderStyle-HorizontalAlign="Center"
                                                PopupVerticalAlign="WindowCenter" ShowPageScrollbarWhenModal="True" ClientInstanceName="MainPopup"
                                                CloseAction="None" ShowCloseButton="False">                                                
                                                <ContentStyle>
                                                    <BorderBottom BorderColor="#E0E0E0" BorderStyle="Solid" BorderWidth="1px" />
                                                    <Paddings Padding="0px" />
                                                </ContentStyle>
                                                <SizeGripImage Height="16px" Url="../en/App_Themes/Plastic Blue/Web/pcSizeGrip.gif"
                                                    Width="16px" />
                                                <CloseButtonStyle>
                                                    <Paddings Padding="0px" />
                                                </CloseButtonStyle>                                                
                                                <ContentCollection>
                                                    <dx:PopupControlContentControl>
                                                        <table width="180px" cellpadding="0" cellspacing="0" border="0">
                                                            <tr>
                                                                <td colspan="3">
                                                                   <span style="color:Red;font-size:smaller;"> * - サブメニューを複数選択/選択解除を使用するCtrl +クリック </span>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="3">
                                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0" id="tblConference" style="display:none;" >
                                                                        <tr valign="top">
                                                                            <td width="50%" align="left" >
                                                                                <dx:ASPxLabel ID="ASPxLabel13" runat="server"  Text="会議:" ForeColor="#003366"></dx:ASPxLabel>
                                                                            </td>
                                                                            <td valign="top" align="right" >
                                                                                 <dx:ASPxButton ID="btnCClear" runat="server" CssFilePath="../en/App_Themes/Glass/{0}/styles.css"
                                                                                    CssPostfix="Glass" SpriteCssFilePath="../en/App_Themes/Glass/{0}/sprite.css"
                                                                                    Text="Clear" Width="52px" AutoPostBack="False">
                                                                                    <ClientSideEvents Click="fnClear" />
                                                                                </dx:ASPxButton> 
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td width="50%" align="left"  colspan="2">
                                                                                <dx:ASPxCheckBox ID="chk_ConfID" runat="server" ClientInstanceName="chk_ConfID" Text="会議の識別" Font-Size="5px">
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_ConfTitle" runat="server" ClientInstanceName="chk_ConfTitle" Text="会議タイトル">
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_Org" runat="server" ClientInstanceName="chk_Org" Text="組織">
                                                                                    <ClientSideEvents CheckedChanged="OnShowButtonClick" />
                                                                                </dx:ASPxCheckBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr valign="top">
                                                                            <td align="left" colspan="2">
                                                                                <dx:ASPxLabel ID="ASPxLabel1" runat="server" Font-Bold="True" Text="  主催者:" ForeColor="#003366">
                                                                                </dx:ASPxLabel>
                                                                            </td>
                                                                        </tr>
                                                                        <tr valign="top">
                                                                            <td align="left">                                                                  
                                                                                <dx:ASPxCheckBox ID="chk_HFN" runat="server" ClientInstanceName="chk_HFN" Text="ファーストネーム（名）">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_AllHost.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_HLN" runat="server" ClientInstanceName="chk_HLN" Text="姓">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_AllHost.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_HMail" runat="server" ClientInstanceName="chk_HMail" Text="選択">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_AllHost.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                            </td>
                                                                            <td>                                                                     
                                                                                <dx:ASPxCheckBox ID="chk_HRole" runat="server" ClientInstanceName="chk_HRole" Text="役割">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_AllHost.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_HPerson" runat="server" ClientInstanceName="chk_HPerson" Text="者" Enabled="false">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_AllHost.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_AllHost" runat="server" ClientInstanceName="chk_AllHost" Text="すべて">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e) {fnHostAll();}" />
                                                                                </dx:ASPxCheckBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr valign="top">
                                                                            <td align="left" colspan="2">
                                                                                <dx:ASPxLabel ID="ASPxLabel5" runat="server" Font-Bold="True" Text="  参加者:" ForeColor="#003366">
                                                                                </dx:ASPxLabel>
                                                                            </td>
                                                                        </tr>
                                                                        <tr valign="top">
                                                                            <td align="left">                                                                
                                                                                <dx:ASPxCheckBox ID="chk_PFN" runat="server" ClientInstanceName="chk_PFN" Text="ファーストネーム（名）">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_AllPerson.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_PLN" runat="server" ClientInstanceName="chk_PLN" Text="姓">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_AllPerson.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_PMail" runat="server" ClientInstanceName="chk_PMail" Text="メール">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_AllPerson.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                            </td>
                                                                            <td>                                                                     
                                                                                <dx:ASPxCheckBox ID="chk_PRole" runat="server" ClientInstanceName="chk_PRole" Text="役割">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_AllPerson.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_PPerson" runat="server" ClientInstanceName="chk_PPerson" Text="者"  Enabled="false">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_AllPerson.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_AllPerson" runat="server" ClientInstanceName="chk_AllPerson" Text="すべて">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e) {fnPersonAll();}" />
                                                                                </dx:ASPxCheckBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr valign="top">
                                                                            <td align="left" colspan="2">
                                                                                <dx:ASPxLabel ID="ASPxLabel2" runat="server" Font-Bold="True" Text="  予定された 会議:"
                                                                                    ForeColor="#003366">
                                                                                </dx:ASPxLabel>
                                                                            </td>
                                                                        </tr>
                                                                        <tr valign="top">
                                                                            <td align="left" >                                                                
                                                                                <dx:ASPxCheckBox ID="chk_Completed" runat="server" ClientInstanceName="chk_Completed" Text="完了">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_AllConf.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_Scheduled" runat="server" ClientInstanceName="chk_Scheduled" Text="スケジュール済み">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_AllConf.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_Deleted" runat="server" ClientInstanceName="chk_Deleted" Text="消去済み">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_AllConf.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                            </td>
                                                                            <td>
                                                                                <dx:ASPxCheckBox ID="chk_Terminated" runat="server" ClientInstanceName="chk_Terminated" Text=" 終了しました">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_AllConf.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_Pending" runat="server" ClientInstanceName="chk_Pending" Text="承認待ち">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_AllConf.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_AllConf" runat="server" ClientInstanceName="chk_AllConf" Text="すべて">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e) {fnScheduledAll();}" />
                                                                                </dx:ASPxCheckBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr valign="top">
                                                                            <td align="left" colspan="2">
                                                                                <dx:ASPxLabel ID="ASPxLabel4" runat="server" Font-Bold="True" Text="  会議タイプ:" ForeColor="#003366">
                                                                                </dx:ASPxLabel>
                                                                            </td>
                                                                        </tr>
                                                                        <tr valign="top">
                                                                            <td align="left">                                                                    
                                                                                <dx:ASPxCheckBox ID="chk_Audio" runat="server" ClientInstanceName="chk_Audio" Text="音声のみ">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_AllConfTypes.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_Video" runat="server" ClientInstanceName="chk_Video" Text="ビデオののみ">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_AllConfTypes.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_Room" runat="server" ClientInstanceName="chk_Room" Text="お部屋のみ">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_AllConfTypes.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                            </td>
                                                                            <td>
                                                                                 <dx:ASPxCheckBox ID="chk_PP" runat="server" ClientInstanceName="chk_PP" Text="ポイント・ツー・ポイント会議" >
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_AllConfTypes.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_TP" runat="server" ClientInstanceName="chk_TP" Text="テレプレゼンス" Enabled="False">
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_AllConfTypes" runat="server" ClientInstanceName="chk_AllConfTypes" Text="すべて">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e) {fnConfTypeAll();}" />
                                                                                </dx:ASPxCheckBox>
                                                                            </td>
                                                                        </tr>
                                                                         <tr valign="top">
                                                                            <td align="left" colspan="2">
                                                                                <dx:ASPxLabel ID="ASPxLabel6" runat="server" Font-Bold="True" Text="  Conferences Behavior:" ForeColor="#003366">
                                                                                </dx:ASPxLabel>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td >                                                                   
                                                                                <dx:ASPxCheckBox ID="chk_Single" runat="server" ClientInstanceName="chk_Single" Text="シングル">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_AllMode.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_Recurring" runat="server" ClientInstanceName="chk_Recurring" Text="定期的な">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_AllMode.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                            </td>
                                                                            <td>
                                                                                <dx:ASPxCheckBox ID="chk_AllMode" runat="server" ClientInstanceName="chk_AllMode" Text="すべて">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e) {fnRecurAll();}" />
                                                                                </dx:ASPxCheckBox>
                                                                            </td>
                                                                        </tr>
                                                                         <tr valign="top">
                                                                            <td align="left" colspan="2">
                                                                                <dx:ASPxLabel ID="ASPxLabel7" runat="server" Font-Bold="True" Text="  会議 タイム:" ForeColor="#003366">
                                                                                </dx:ASPxLabel>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>      
                                                                                <dx:ASPxCheckBox ID="chk_Date" runat="server" ClientInstanceName="chk_Date" Text="完全">
                                                                                </dx:ASPxCheckBox>
                                                                            </td>
                                                                            <td>
                                                                                <dx:ASPxCheckBox ID="chk_Time" runat="server" ClientInstanceName="chk_Time" Text="タイム">
                                                                                </dx:ASPxCheckBox>
                                                                            </td>
                                                                        </tr>
                                                                         <tr valign="top">
                                                                            <td align="left" colspan="2">
                                                                                <dx:ASPxLabel ID="ASPxLabel8" runat="server" Font-Bold="True" ClientInstanceName="ASPxLabel8" Text=" 継続時間:" ForeColor="#003366">
                                                                                </dx:ASPxLabel>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>                                                                   
                                                                                <dx:ASPxCheckBox ID="chk_Hours" runat="server" ClientInstanceName="chk_Hours" Text="時間" Enabled="False">
                                                                                </dx:ASPxCheckBox>
                                                                            </td>
                                                                            <td>
                                                                                <dx:ASPxCheckBox ID="chk_Minutes" runat="server" ClientInstanceName="chk_Minutes" Text="分">
                                                                                </dx:ASPxCheckBox>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr> 
                                                                <td colspan="3">
                                                                    <table width="100%" cellpadding="0" cellspacing="0" id="tblUser" style="display:none;">
                                                                        <tr valign="top">
                                                                            <td width="50%" align="left">
                                                                                <dx:ASPxLabel ID="ASPxLabel14" runat="server"  Text="会議:"
                                                                                    ForeColor="#003366">
                                                                                </dx:ASPxLabel>
                                                                            </td>
                                                                             <td valign="top" align="right">
                                                                                 <dx:ASPxButton ID="btnUClear" runat="server" CssFilePath="../en/App_Themes/Glass/{0}/styles.css"
                                                                                    CssPostfix="Glass" SpriteCssFilePath="../en/App_Themes/Glass/{0}/sprite.css"
                                                                                    Text="Clear" Width="52px" AutoPostBack="False">
                                                                                    <ClientSideEvents Click="fnClear" />
                                                                                </dx:ASPxButton> 
                                                                            </td>
                                                                        </tr>
                                                                        <tr valign="top">
                                                                            <td width="50%" align="left" colspan="2">   
                                                                                <dx:ASPxCheckBox ID="chk_OrgU" runat="server" ClientInstanceName="chk_OrgU" Text="組織" Font-Size="5px">
                                                                                    <ClientSideEvents CheckedChanged="function(s,e){OnShowButtonClick('popupU');}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_DeptU" runat="server" ClientInstanceName="chk_DeptU" Text="部門">                                                                                    
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_FNU" runat="server" ClientInstanceName="chk_FNU" Text="ファーストネーム（名）">
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_LNU" runat="server" ClientInstanceName="chk_LNU" Text="姓">
                                                                                </dx:ASPxCheckBox>
                                                                                 <dx:ASPxCheckBox ID="chk_PersonU" runat="server" ClientInstanceName="chk_PersonU" Text="者" Enabled="false">
                                                                                </dx:ASPxCheckBox>
                                                                                <hr />
                                                                                <dx:ASPxCheckBox ID="chk_UserEmailU" runat="server" ClientInstanceName="chk_UserEmailU" Text="ユーザーEメール">
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_SecondaryEmailU" runat="server" ClientInstanceName="chk_SecondaryEmailU" Text="第二Eメール">
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_RoleU" runat="server" ClientInstanceName="chk_RoleU" Text="役割">
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_TimeZoneU" runat="server" ClientInstanceName="chk_TimeZoneU" Text="タイムゾーン">
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_ConfHostU" runat="server" ClientInstanceName="chk_ConfHostU" Text="会議主催者">
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_ConfPartU" runat="server" ClientInstanceName="chk_ConfPartU" Text="会議 参加者">
                                                                                </dx:ASPxCheckBox>
                                                                                <hr />
                                                                                <dx:ASPxCheckBox ID="chk_AccExpirU" runat="server" ClientInstanceName="chk_AccExpirU" Text="アカウント有効期限">
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_MinU" runat="server" ClientInstanceName="chk_MinU" Text="分">
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_ExchangeU" runat="server" ClientInstanceName="chk_ExchangeU" Text="を有効にする">
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_DominoU" runat="server" ClientInstanceName="chk_DominoU" Text="ドミノ">
                                                                                </dx:ASPxCheckBox>
                                                                            </td>
                                                                            
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>    
                                                             <tr> 
                                                                <td colspan="3">
                                                                    <table width="100%" cellpadding="0" cellspacing="0" id="tblRoom" style="display:none;">
                                                                        <tr valign="top">
                                                                            <td width="50%" align="left" >   
                                                                                <dx:ASPxLabel ID="ASPxLabel15" runat="server"  Text="  場所:"
                                                                                    ForeColor="#003366">
                                                                                </dx:ASPxLabel>     
                                                                            <td valign="top" align="right">
                                                                                 <dx:ASPxButton ID="btnRClear" runat="server" CssFilePath="../en/App_Themes/Glass/{0}/styles.css"
                                                                                    CssPostfix="Glass" SpriteCssFilePath="../en/App_Themes/Glass/{0}/sprite.css"
                                                                                    Text="Clear" Width="52px" AutoPostBack="False">
                                                                                    <ClientSideEvents Click="fnClear" />
                                                                                </dx:ASPxButton> 
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td width="50%" align="left" colspan="2">
                                                                                <dx:ASPxCheckBox ID="chk_OrgR" runat="server" ClientInstanceName="chk_OrgR" Text="組織" Font-Size="5px">
                                                                                    <ClientSideEvents CheckedChanged="function(s,e){OnShowButtonClick('popupR');}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_DeptR" runat="server" ClientInstanceName="chk_DeptR" Text="部門">
                                                                                    <%--<ClientSideEvents CheckedChanged="function(s,e){OnShowButtonClick('dptPopupChk');}" />--%>
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_TiersR" runat="server" ClientInstanceName="chk_TiersR" Text="階層">
                                                                                    <ClientSideEvents CheckedChanged="function(s,e){OnShowButtonClick('assignmentPopup');}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_NameR" runat="server" ClientInstanceName="chk_NameR" Text="部屋名">
                                                                                    <ClientSideEvents CheckedChanged="function(s,e){OnShowButtonClick('rmInfoPopup');}" />
                                                                                </dx:ASPxCheckBox>
                                                                                 <dx:ASPxCheckBox ID="chk_HostR" runat="server" ClientInstanceName="chk_HostR" Text="主催者">
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxLabel ID="ASPxLabel16" runat="server"  Text="部屋 タイプ:"
                                                                                    ForeColor="#003366">
                                                                                </dx:ASPxLabel>
                                                                                <dx:ASPxCheckBox ID="chk_VideoRoomR" runat="server" ClientInstanceName="chk_VideoRoomR" Text="非ビデオ部屋">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e) {if(!s.GetChecked()) chk_AllTypeR.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_AudioRoomR" runat="server" ClientInstanceName="chk_AudioRoomR" Text="音声 部屋">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e) {if(!s.GetChecked()) chk_AllTypeR.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_MeetingRoomR" runat="server" ClientInstanceName="chk_MeetingRoomR" Text="会議 部屋">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e) {if(!s.GetChecked()) chk_AllTypeR.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_AllTypeR" runat="server" ClientInstanceName="chk_AllTypeR" Text="すべてのタイプ">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e) {fnRmTypeAll();}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxLabel ID="ASPxLabel17" runat="server"  Text="行動:"
                                                                                    ForeColor="#003366">
                                                                                </dx:ASPxLabel>
                                                                                <dx:ASPxCheckBox ID="chk_VideoConferenceR" runat="server" ClientInstanceName="chk_VideoConferenceR" Text="ビジュアル会議">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e) {if(!s.GetChecked()) chk_AllBehaviorR.SetChecked(false);fnBehaviorAllChecked();}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_AudioConferenceR" runat="server" ClientInstanceName="chk_AudioConferenceR" Text="オーディオ会議">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e) {if(!s.GetChecked()) chk_AllBehaviorR.SetChecked(false);fnBehaviorAllChecked();}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_RoomConferenceR" runat="server" ClientInstanceName="chk_RoomConferenceR" Text="部屋会議">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e) {if(!s.GetChecked()) chk_AllBehaviorR.SetChecked(false);fnBehaviorAllChecked();}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_AllBehaviorR" runat="server" ClientInstanceName="chk_AllBehaviorR" Text="すべてのタイプ">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e) {fnBehaviorAll();}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <hr />
                                                                                <dx:ASPxLabel ID="ASPxLabel18" runat="server"  Text="  他:"
                                                                                    ForeColor="#003366" Visible="false" >
                                                                                </dx:ASPxLabel>
                                                                                <dx:ASPxCheckBox ID="chk_UserAssignmentR" runat="server" ClientInstanceName="chk_UserAssignmentR" Text="ユーザ 割り当て" Visible="false">
                                                                                    <ClientSideEvents CheckedChanged="function(s,e){OnShowButtonClick('assignment');}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_MaximumCapacityR" runat="server" ClientInstanceName="chk_MaximumCapacityR" Text="最大容量" Visible="false">
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_AssetsR" runat="server" ClientInstanceName="chk_AssetsR" Text="Assets" Visible="false">
                                                                                    <ClientSideEvents CheckedChanged="function(s,e){OnShowButtonClick('rmAssetPopup');}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_ScheduledDurationR" runat="server" Visible="false" ClientInstanceName="chk_ScheduledDurationR" Text="予定された 継続時間">
                                                                                </dx:ASPxCheckBox>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>      
                                                            <tr> 
                                                                <td colspan="3">
                                                                    <table width="100%" cellpadding="0" cellspacing="0" id="tblEndPoint" style="display:none;">
                                                                        <tr valign="top">
                                                                            <td width="50%" align="left" >   
                                                                                <dx:ASPxLabel ID="ASPxLabel19" runat="server"  Text="  エンドポイント:"
                                                                                    ForeColor="#003366">
                                                                                </dx:ASPxLabel>   
                                                                            </td>
                                                                            <td valign="top" align="right">
                                                                                 <dx:ASPxButton ID="btnEClear" runat="server" CssFilePath="../en/App_Themes/Glass/{0}/styles.css"
                                                                                    CssPostfix="Glass" SpriteCssFilePath="../en/App_Themes/Glass/{0}/sprite.css"
                                                                                    Text="Clear" Width="52px" AutoPostBack="False">
                                                                                    <ClientSideEvents Click="fnClear" />
                                                                                </dx:ASPxButton> 
                                                                            </td>  
                                                                        </tr>                                                                        
                                                                        <tr>
                                                                            <td width="50%" align="left" colspan="2">
                                                                                <dx:ASPxCheckBox ID="chk_OrgE" runat="server" ClientInstanceName="chk_OrgE" Text="組織" Font-Size="5px">
                                                                                    <ClientSideEvents CheckedChanged="function(s,e){OnShowButtonClick('popupE');}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_DeptE" runat="server" ClientInstanceName="chk_DeptE" Text="部門">
                                                                                    <%--<ClientSideEvents CheckedChanged="function(s,e){OnShowButtonClick('dptPopupChk');}" />--%>
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_ENameE" runat="server" ClientInstanceName="chk_ENameE" Text="エンドポイント名">
                                                                                    <ClientSideEvents CheckedChanged="function(s,e){OnShowButtonClick('assignmentPopup');}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_SpecEptE" runat="server" ClientInstanceName="chk_SpecEptE" Text="特定 エンドポイント" Enabled="false">
                                                                                </dx:ASPxCheckBox>
                                                                                 <dx:ASPxCheckBox ID="chk_RmNameE" runat="server" ClientInstanceName="chk_RmNameE" Text="部屋名">
                                                                                    <ClientSideEvents CheckedChanged="function(s,e){OnShowButtonClick('assignmentPopup');}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_SpecRoomE" runat="server" ClientInstanceName="chk_SpecRoomE" Text="特定 部屋" Enabled="false" >
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_ConfE" runat="server" ClientInstanceName="chk_ConfE" Text="会議">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e) {fnBehaviorAllChecked();}" />
                                                                                </dx:ASPxCheckBox>
                                                                            </td> 
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>  
                                                            <tr> 
                                                                <td colspan="3">
                                                                    <table width="100%" cellpadding="0" cellspacing="0" id="tblMCU" style="display:none;">
                                                                        <tr valign="top">
                                                                            <td width="50%" align="left" >   
                                                                                <dx:ASPxLabel ID="ASPxLabel20" runat="server"  Text="  エンドポイント:"
                                                                                    ForeColor="#003366">
                                                                                </dx:ASPxLabel>  
                                                                            </td>
                                                                             <td valign="top" align="right">
                                                                                 <dx:ASPxButton ID="btnMClear" runat="server" CssFilePath="../en/App_Themes/Glass/{0}/styles.css"
                                                                                    CssPostfix="Glass" SpriteCssFilePath="../en/App_Themes/Glass/{0}/sprite.css"
                                                                                    Text="Clear" Width="52px" AutoPostBack="False">
                                                                                    <ClientSideEvents Click="fnClear" />
                                                                                </dx:ASPxButton> 
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td width="50%" align="left" colspan="2">                                                                       
                                                                                <dx:ASPxCheckBox ID="chk_OrgM" runat="server" ClientInstanceName="chk_OrgM" Text="組織" Font-Size="5px">
                                                                                    <ClientSideEvents CheckedChanged="function(s,e){OnShowButtonClick('popupM');}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_DeptM" runat="server" ClientInstanceName="chk_DeptM" Text="部門">
                                                                                    <%--<ClientSideEvents CheckedChanged="function(s,e){OnShowButtonClick('dptPopupChk');}" />--%>
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_MCUNameM" runat="server" ClientInstanceName="chk_MCUNameM" Text="MCU 名前">
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_SpecMCUM" runat="server" ClientInstanceName="chk_SpecMCUM" Text="特定 MCU" Enabled="false">
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_ENameM" runat="server" ClientInstanceName="chk_ENameM" Text="エンドポイント名">
                                                                                    <ClientSideEvents CheckedChanged="function(s,e){OnShowButtonClick('assignmentPopup');}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_SpecEptM" runat="server" ClientInstanceName="chk_SpecEptM" Text="特定 エンドポイント" Enabled="false">
                                                                                </dx:ASPxCheckBox>
                                                                                 <dx:ASPxCheckBox ID="chk_RmNameM" runat="server" ClientInstanceName="chk_RmNameM" Text="部屋名">
                                                                                    <ClientSideEvents CheckedChanged="function(s,e){OnShowButtonClick('assignmentPopup');}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_SpecRoomM" runat="server" ClientInstanceName="chk_SpecRoomM" Text="特定 部屋" Enabled="false" >
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_ConfM" runat="server" ClientInstanceName="chk_ConfM" Text="会議">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e) {fnBehaviorAllChecked();}" />
                                                                                </dx:ASPxCheckBox>
                                                                            </td>
                                                                           
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>      
                                                            <tr>
                                                                <td align="left" colspan="3">
                                                                    <dx:ASPxTextBox ID="txtSaveReport" ClientInstanceName="txtSaveReport" runat="server" Width="180px" Text="レポート の名前...">
                                                                        <ClientSideEvents GotFocus="OntextFocus" LostFocus="OntextunFocus" />
                                                                    </dx:ASPxTextBox>                                                                   
                                                                </td>
                                                            </tr>                                                       
                                                            <tr valign="middle">
                                                                <td align="center">
                                                                    <dx:ASPxButton ID="btnClose" runat="server" CssFilePath="../en/App_Themes/Glass/{0}/styles.css"
                                                                        CssPostfix="Glass" SpriteCssFilePath="../en/App_Themes/Glass/{0}/sprite.css"
                                                                        Text="閉じる" Width="52px">
                                                                        <ClientSideEvents Click="function(s, e) {e.processOnServer = fnClose(); } " />
                                                                    </dx:ASPxButton>
                                                                </td>
                                                                <td valign="middle" align="center">
                                                                    <dx:ASPxButton ID="btnSave" runat="server" CssFilePath="../en/App_Themes/Glass/{0}/styles.css"
                                                                        CssPostfix="Glass" SpriteCssFilePath="../en/App_Themes/Glass/{0}/sprite.css"
                                                                        Text="保存" Width="52px"  OnClick="SaveReport">
                                                                        <ClientSideEvents Click="function(s, e) {e.processOnServer = fnSave();}" />
                                                                    </dx:ASPxButton> 
                                                                </td>
                                                                <td align="center">
                                                                    
                                                                    <dx:ASPxButton ID="cmdConfOk" ClientInstanceName="cmdConfOk" runat="server" CssFilePath="../en/App_Themes/Glass/{0}/styles.css"
                                                                        CssPostfix="Glass" SpriteCssFilePath="../en/App_Themes/Glass/{0}/sprite.css"
                                                                        Text="Ok" Width="52px">
                                                                        <ClientSideEvents Click="function(s, e) {fnSubmit(); e.processOnServer = fnClose();}" />
                                                                    </dx:ASPxButton> 
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </dx:PopupControlContentControl>
                                                </ContentCollection>
                                                 <CloseButtonImage Height="14px" Width="14px" />
                                                <HeaderStyle>
                                                    <Paddings PaddingBottom="4px" PaddingLeft="10px" PaddingRight="4px" PaddingTop="4px" />
                                                </HeaderStyle>
                                            </dx:ASPxPopupControl>
                                            
                                            <dx:ASPxPopupControl ID="pcPopup" runat="server" ClientInstanceName="popup" EncodeHtml="false"
                                                CssFilePath="../en/App_Themes/Plastic Blue/{0}/styles.css" Width="150px" CssPostfix="PlasticBlue"
                                                HeaderText="組織" CloseAction="CloseButton" >
                                                <ContentCollection>
                                                    <dx:PopupControlContentControl>
                                                        <dx:ASPxListBox ID="OrgList" runat="server"  Width="100%" ClientInstanceName="OrgList">
                                                            <ClientSideEvents SelectedIndexChanged="function(s,e){OnShowButtonClick('dptPopup');}" />
                                                            <ItemStyle><Paddings Padding="2px" /> </ItemStyle>
                                                         </dx:ASPxListBox>      
                                                         <dx:ASPxListBox ID="TempList" runat="server" Height="0px" Width="100%" Border-BorderStyle=None ClientInstanceName="TempList" > </dx:ASPxListBox>                                                 
                                                    </dx:PopupControlContentControl>
                                                </ContentCollection>
                                                 <ContentStyle> 
                                                    <Paddings PaddingBottom="8px" />
                                                </ContentStyle>
                                            </dx:ASPxPopupControl>
                                            <dx:ASPxPopupControl ID="dptPopup" runat="server" ClientInstanceName="dptPopup"
                                                EncodeHtml="false" HeaderText="部門" Width="150px" CloseAction="CloseButton" CssFilePath="../en/App_Themes/Plastic Blue/Web/styles.css"
                                                CssPostfix="PlasticBlue" >
                                                <ContentCollection>
                                                    <dx:PopupControlContentControl>
                                                         <dx:ASPxListBox ID="lstDepartment" SelectionMode="Multiple"  runat="server"  Width="100%"  ClientInstanceName="lstDepartment">
                                                            <ClientSideEvents SelectedIndexChanged="function(s,e){OnShowButtonClick('assignmentPopup');}" />
                                                            <ItemStyle><Paddings Padding="2px" /> </ItemStyle>
                                                         </dx:ASPxListBox>                                                          
                                                    </dx:PopupControlContentControl>
                                                </ContentCollection>
                                                 <ContentStyle> 
                                                    <Paddings PaddingBottom="8px" />
                                                </ContentStyle>
                                            </dx:ASPxPopupControl>
                                            <dx:ASPxPopupControl ID="assignmentPopup" runat="server" ClientInstanceName="assignmentPopup"
                                                EncodeHtml="false" HeaderText="割り当て" Width="150px" CloseAction="CloseButton" CssFilePath="../en/App_Themes/Plastic Blue/Web/styles.css"
                                                CssPostfix="PlasticBlue">
                                                <ContentCollection>
                                                    <dx:PopupControlContentControl>
                                                        <dx:ASPxMenu ID="ASPxMenu1" runat="server" EncodeHtml="False" Orientation="Vertical"
                                                            Width="100%">
                                                            <ItemStyle HorizontalAlign="Center" Font-Size="11px" >
                                                            <Paddings Padding="0px" /> </ItemStyle>
                                                            <Items>
                                                                <dx:MenuItem Text="部屋">
                                                                </dx:MenuItem>
                                                            </Items>
                                                        </dx:ASPxMenu>
                                                        <dx:ASPxListBox ID="lstRmApprover" runat="server" SelectionMode="Multiple" Height="100px" Width="100%" ClientInstanceName="lstRmApprover" Rows="5" >
                                                            <ItemStyle><Paddings Padding="2px" /> </ItemStyle>
                                                            <Items>
                                                              <dx:ListEditItem Text="担当のアシスタント" Value="1" />
                                                              <dx:ListEditItem Text="第一承認者" Value="2" />
                                                              <dx:ListEditItem Text="第二承認者 I" Value="3" />
                                                              <dx:ListEditItem Text="第二承認者 II" Value="4" />
                                                              <dx:ListEditItem Text="すべて" Value="5" />
                                                            </Items>
                                                         </dx:ASPxListBox>                                                         
                                                        <dx:ASPxMenu ID="ASPxMenu2" runat="server" EncodeHtml="False" Orientation="Vertical"
                                                            Width="100%">
                                                            <ItemStyle HorizontalAlign="Center" Font-Size="11px" >
                                                            <Paddings Padding="0px" /> </ItemStyle>
                                                            <Items>
                                                                <dx:MenuItem Text="MCU">
                                                                </dx:MenuItem>
                                                            </Items>
                                                        </dx:ASPxMenu>
                                                        <dx:ASPxListBox ID="lstMCUApprover" runat="server" SelectionMode="Multiple" Height="100px" Width="100%" ClientInstanceName="lstMCUApprover" Rows="5" >
                                                            <ItemStyle><Paddings Padding="2px" /> </ItemStyle>
                                                            <Items>
                                                              <dx:ListEditItem Text="管理者" Value="1" />
                                                              <dx:ListEditItem Text="第一承認者" Value="2" />
                                                              <dx:ListEditItem Text="第二承認者 I" Value="3" />
                                                              <dx:ListEditItem Text="第二承認者 II" Value="4" />
                                                              <dx:ListEditItem Text="すべて" Value="5" />
                                                            </Items>
                                                         </dx:ASPxListBox>                                                         
                                                        <dx:ASPxMenu ID="ASPxMenu3" runat="server" EncodeHtml="False" Orientation="Vertical"
                                                            Width="100%">
                                                            <ItemStyle HorizontalAlign="Center" Font-Size="11px" >
                                                            <Paddings Padding="0px" /> </ItemStyle>
                                                            <Items>
                                                                <dx:MenuItem Text="ワークオーダー">
                                                                </dx:MenuItem>
                                                            </Items>
                                                        </dx:ASPxMenu>
                                                        <dx:ASPxListBox ID="lstWOAdmin" runat="server" SelectionMode="Multiple" Height="100px" Width="100%" ClientInstanceName="lstWOAdmin" Rows="5" >
                                                            <ItemStyle><Paddings Padding="2px" /> </ItemStyle>
                                                            <Items>
                                                              <dx:ListEditItem Text="人の電荷" Value="1" />
                                                              <dx:ListEditItem Text="Staff" Value="2" />
                                                              <dx:ListEditItem Text="すべて" Value="3" />                                                             
                                                            </Items>
                                                         </dx:ASPxListBox>                                                         
                                                    </dx:PopupControlContentControl>
                                                </ContentCollection>
                                                 <ContentStyle> 
                                                    <Paddings PaddingBottom="5px" />
                                                </ContentStyle>
                                            </dx:ASPxPopupControl>
                                            <dx:ASPxPopupControl ID="resourcePopup"  AllowDragging ="true" Height="250px" runat="server" ClientInstanceName="resourcePopup"
                                                EncodeHtml="false" HeaderText="資源" Width="150px" CloseAction="CloseButton" CssFilePath="../en/App_Themes/Plastic Blue/Web/styles.css"
                                                CssPostfix="PlasticBlue" >
                                                <ContentCollection>
                                                    <dx:PopupControlContentControl BackColor="Red">
                                                        <dx:ASPxMenu ID="ASPxMenu10" runat="server" EncodeHtml="False" Orientation="Vertical"
                                                            Width="100%">
                                                            <ItemStyle HorizontalAlign="Center" Font-Size="11px" >
                                                            <Paddings Padding="0px" /> </ItemStyle>
                                                            <Items>
                                                                <dx:MenuItem Text="出席者" >
                                                                </dx:MenuItem>
                                                            </Items>
                                                        </dx:ASPxMenu>
                                                        <dx:ASPxListBox ID="lstAttendee" runat="server" SelectionMode="Multiple" Height="60px" Width="100%" ClientInstanceName="lstAttendee" Rows="3" >
                                                            <ItemStyle><Paddings Padding="2px" /> </ItemStyle>
                                                            <Items>
                                                              <dx:ListEditItem Text="Staff" Value="1" />
                                                              <dx:ListEditItem Text="ゲスト" Value="2" />
                                                              <dx:ListEditItem Text="すべて" Value="3" />
                                                            </Items>
                                                         </dx:ASPxListBox>                                                        
                                                        <dx:ASPxMenu ID="ASPxMenu4" runat="server" EncodeHtml="False" Orientation="Vertical"
                                                            Width="100%">
                                                            <ItemStyle HorizontalAlign="Center" Font-Size="11px" >
                                                            <Paddings Padding="0px" /> </ItemStyle>
                                                            <Items>
                                                                <dx:MenuItem Text="参加者">
                                                                </dx:MenuItem>
                                                            </Items>
                                                        </dx:ASPxMenu>
                                                        <dx:ASPxListBox ID="lstRmType" runat="server" SelectionMode="Multiple" Height="60px" Width="100%" ClientInstanceName="lstRmType" Rows="3" >
                                                            <ItemStyle><Paddings Padding="2px" /> </ItemStyle>
                                                            <Items>
                                                              <dx:ListEditItem Text="内部" Value="1" />
                                                              <dx:ListEditItem Text="外部" Value="2" />
                                                              <dx:ListEditItem Text="すべて" Value="3" />
                                                            </Items>
                                                         </dx:ASPxListBox>                                                           
                                                        <dx:ASPxMenu ID="ASPxMenu5" runat="server" EncodeHtml="False" Orientation="Vertical"
                                                            Width="100%">
                                                            <ItemStyle HorizontalAlign="Center" Font-Size="11px" >
                                                            <Paddings Padding="0px" /> </ItemStyle>
                                                            <Items>
                                                                <dx:MenuItem Text="エンドポイント">
                                                                </dx:MenuItem>
                                                            </Items>
                                                        </dx:ASPxMenu>
                                                        <dx:ASPxListBox ID="lstEPYes" runat="server" Height="40px" Width="100%" ClientInstanceName="lstEPYes" Rows="2" >
                                                            <ItemStyle><Paddings Padding="2px" /> </ItemStyle>
                                                            <Items>
                                                              <dx:ListEditItem Text="はい" Value="1" />
                                                              <dx:ListEditItem Text="いいえ" Value="0" />
                                                            </Items>
                                                         </dx:ASPxListBox>                                                           
                                                        <dx:ASPxMenu ID="ASPxMenu6" runat="server" EncodeHtml="False" Orientation="Vertical"
                                                            Width="100%">
                                                            <ItemStyle HorizontalAlign="Center" Font-Size="11px" >
                                                            <Paddings Padding="0px" /> </ItemStyle>
                                                            <Items>
                                                                <dx:MenuItem Text="MCU">
                                                                </dx:MenuItem>
                                                            </Items>
                                                        </dx:ASPxMenu>
                                                        <dx:ASPxListBox ID="lstMCUYes" runat="server" Height="40px" Width="100%" ClientInstanceName="lstMCUYes" Rows="2" >
                                                            <ItemStyle><Paddings Padding="2px" /> </ItemStyle>
                                                            <Items>
                                                              <dx:ListEditItem Text="はい" Value="1" />
                                                              <dx:ListEditItem Text="いいえ" Value="0" />
                                                            </Items>
                                                         </dx:ASPxListBox>
                                                        <dx:ASPxMenu ID="ASPxMenu7" runat="server" EncodeHtml="False" Orientation="Vertical"
                                                            Width="100%">
                                                            <ItemStyle HorizontalAlign="Center" Font-Size="11px" >
                                                            <Paddings Padding="0px" /> </ItemStyle>
                                                            <Items>
                                                                <dx:MenuItem Text="会議 スピード">
                                                                </dx:MenuItem>
                                                            </Items>
                                                        </dx:ASPxMenu>
                                                       <dx:ASPxListBox ID="lstConfSpeedYes" runat="server" Height="40px" Width="100%" ClientInstanceName="lstConfSpeedYes" Rows="2" >
                                                            <ItemStyle><Paddings Padding="2px" /> </ItemStyle>
                                                            <Items>
                                                              <dx:ListEditItem Text="はい" Value="1" />
                                                              <dx:ListEditItem Text="いいえ" Value="0" />
                                                            </Items>
                                                         </dx:ASPxListBox>
                                                        <dx:ASPxMenu ID="ASPxMenu8" runat="server" EncodeHtml="False" Orientation="Vertical"
                                                            Width="100%">
                                                            <ItemStyle HorizontalAlign="Center" Font-Size="11px" >
                                                            <Paddings Padding="0px" /> </ItemStyle>
                                                            <Items>
                                                                <dx:MenuItem Text="会議  プロトコル">
                                                                </dx:MenuItem>
                                                            </Items>
                                                        </dx:ASPxMenu>
                                                        <dx:ASPxListBox ID="lstConfProYes" runat="server" Height="40px" Width="100%" ClientInstanceName="lstConfProYes" Rows="2" >
                                                            <ItemStyle><Paddings Padding="2px" /> </ItemStyle>
                                                            <Items>
                                                              <dx:ListEditItem Text="はい" Value="1" />
                                                              <dx:ListEditItem Text="いいえ" Value="0" />
                                                            </Items>
                                                         </dx:ASPxListBox>
                                                         <dx:ASPxMenu ID="ASPxMenu9" runat="server" EncodeHtml="False" Orientation="Vertical"
                                                            Width="100%">
                                                            <ItemStyle HorizontalAlign="Center" Font-Size="11px" >
                                                            <Paddings Padding="0px" /> </ItemStyle>
                                                            <Items>
                                                                <dx:MenuItem Text="ワークオーダー">
                                                                </dx:MenuItem>
                                                            </Items>
                                                        </dx:ASPxMenu>
                                                        <dx:ASPxListBox ID="lstWOYes" runat="server" Height="40px" Width="100%" ClientInstanceName="lstWOYes" Rows="2" >
                                                            <ItemStyle><Paddings Padding="2px" /> </ItemStyle>
                                                            <Items>
                                                              <dx:ListEditItem Text="はい" Value="1" />
                                                              <dx:ListEditItem Text="いいえ" Value="0" />
                                                            </Items>
                                                         </dx:ASPxListBox>
                                                    </dx:PopupControlContentControl>
                                                </ContentCollection>
                                                <ContentStyle> 
                                                    <Paddings PaddingBottom="1px" />
                                                </ContentStyle>
                                            </dx:ASPxPopupControl>
                                            <dx:ASPxPopupControl ID="confPopup" runat="server" ClientInstanceName="confPopup"
                                                EncodeHtml="false" HeaderText="会議" Width="150px" CloseAction="CloseButton" CssFilePath="../en/App_Themes/Plastic Blue/Web/styles.css"
                                                CssPostfix="PlasticBlue">
                                                <ContentCollection>
                                                    <dx:PopupControlContentControl>
                                                        <dx:ASPxMenu ID="ASPxMenu11" runat="server" EncodeHtml="False" Orientation="Vertical"
                                                            Width="100%">
                                                            <ItemStyle HorizontalAlign="Center" Font-Size="11px" >
                                                            <Paddings Padding="0px" /> </ItemStyle>
                                                            <Items>
                                                                <dx:MenuItem Text="タイプ">
                                                                </dx:MenuItem>
                                                            </Items>
                                                        </dx:ASPxMenu>
                                                        <dx:ASPxListBox ID="lstConfType" runat="server" SelectionMode="Multiple" Height="100px" Width="100%" ClientInstanceName="lstConfType" Rows="4" >
                                                            <ItemStyle><Paddings Padding="2px" /> </ItemStyle>
                                                            <Items>
                                                              <dx:ListEditItem Text="オーディオ/ビデオセッティング" Value="2" />
                                                              <dx:ListEditItem Text="音声のみ" Value="6" />
                                                              <dx:ListEditItem Text="お部屋のみ" Value="7" />
                                                              <dx:ListEditItem Text="すべて" Value="4" />
                                                            </Items>
                                                         </dx:ASPxListBox>                                                         
                                                    </dx:PopupControlContentControl>
                                                </ContentCollection>
                                                 <ContentStyle> 
                                                    <Paddings PaddingBottom="5px" />
                                                </ContentStyle>
                                            </dx:ASPxPopupControl>
                                            <dx:ASPxPopupControl ID="tierPopup" runat="server" ClientInstanceName="tierPopup"
                                                EncodeHtml="false" HeaderText="階層" Width="150px" CloseAction="CloseButton" CssFilePath="../en/App_Themes/Plastic Blue/Web/styles.css"
                                                CssPostfix="PlasticBlue">
                                                <ContentCollection>
                                                    <dx:PopupControlContentControl>                                                       
                                                        <dx:ASPxListBox ID="lstTiers" runat="server" SelectionMode="Multiple" Height="100px" Width="100%" ClientInstanceName="lstTiers" Rows="4" >
                                                            <ItemStyle><Paddings Padding="2px" /> </ItemStyle>
                                                            <Items>
                                                              <dx:ListEditItem Text="階層１" Value="1" />
                                                              <dx:ListEditItem Text="階層２" Value="2" />
                                                              <dx:ListEditItem Text="すべて" Value="3" />
                                                            </Items>
                                                         </dx:ASPxListBox>                                                         
                                                    </dx:PopupControlContentControl>
                                                </ContentCollection>
                                                 <ContentStyle> 
                                                    <Paddings PaddingBottom="5px" />
                                                </ContentStyle>
                                            </dx:ASPxPopupControl>
                                            <dx:ASPxPopupControl ID="rmInfoPopup" runat="server" ClientInstanceName="rmInfoPopup"
                                                EncodeHtml="false" HeaderText="部屋 情報" Width="150px" CloseAction="CloseButton" CssFilePath="../en/App_Themes/Plastic Blue/Web/styles.css"
                                                CssPostfix="PlasticBlue">
                                                <ContentCollection>
                                                    <dx:PopupControlContentControl>                                                       
                                                        <dx:ASPxListBox ID="lstRmInfo" runat="server" SelectionMode="Multiple" Height="100px" Width="100%" ClientInstanceName="lstRmInfo" Rows="14" >
                                                            <ItemStyle><Paddings Padding="2px" /> </ItemStyle>
                                                            <Items>
                                                              <dx:ListEditItem Text="電話番号" Value="1" />
                                                              <dx:ListEditItem Text="許容範囲" Value="2" />
                                                              <dx:ListEditItem Text="アドレス 1" Value="3" />
                                                              <dx:ListEditItem Text="アドレス 2" Value="4" />
                                                              <dx:ListEditItem Text="フロア" Value="5" />
                                                              <dx:ListEditItem Text="部屋" Value="6" />
                                                              <dx:ListEditItem Text="市" Value="7" />
                                                              <dx:ListEditItem Text="州/地方" Value="8" />
                                                              <dx:ListEditItem Text="国" Value="9" />
                                                              <dx:ListEditItem Text="郵便番号/国コード" Value="10" />
                                                              <dx:ListEditItem Text="タイムゾーン" Value="11" />
                                                              <dx:ListEditItem Text="第一承認者" Value="12" />
                                                              <dx:ListEditItem Text="第二承認者 I" Value="13" />
                                                              <dx:ListEditItem Text="第二承認者 II" Value="14" />
                                                              <dx:ListEditItem Text="すべて" Value="15" />
                                                            </Items>
                                                         </dx:ASPxListBox>                                                         
                                                    </dx:PopupControlContentControl>
                                                    
                                                </ContentCollection>
                                                 <ContentStyle> 
                                                    <Paddings PaddingBottom="5px" />
                                                </ContentStyle>
                                            </dx:ASPxPopupControl>
                                            <dx:ASPxPopupControl ID="rmAssetPopup" runat="server" ClientInstanceName="rmAssetPopup"
                                                EncodeHtml="false" HeaderText="部屋の資産" Width="150px" CloseAction="CloseButton" CssFilePath="../en/App_Themes/Plastic Blue/Web/styles.css"
                                                CssPostfix="PlasticBlue">
                                                <ContentCollection>
                                                    <dx:PopupControlContentControl>                                                       
                                                        <dx:ASPxListBox ID="lstRmAsset" runat="server" SelectionMode="Multiple" Height="100px" Width="100%" ClientInstanceName="lstRmAsset" Rows="3" >
                                                            <ItemStyle><Paddings Padding="2px" /> </ItemStyle>
                                                            <Items>
                                                              <dx:ListEditItem Text="プロジェクタ" Value="1" />
                                                              <dx:ListEditItem Text="ケータリング" Value="2" />
                                                              <dx:ListEditItem Text="すべて" Value="3" />
                                                            </Items>
                                                         </dx:ASPxListBox>                                                         
                                                    </dx:PopupControlContentControl>
                                                </ContentCollection>
                                                 <ContentStyle> 
                                                    <Paddings PaddingBottom="5px" />
                                                </ContentStyle>
                                            </dx:ASPxPopupControl>
                                            <dx:ASPxPopupControl ID="confStatusPopup" runat="server" ClientInstanceName="confStatusPopup"
                                                EncodeHtml="false" HeaderText="会議 状況" Width="150px" CloseAction="CloseButton" CssFilePath="../en/App_Themes/Plastic Blue/Web/styles.css"
                                                CssPostfix="PlasticBlue">
                                                <ContentCollection>
                                                    <dx:PopupControlContentControl>                                                       
                                                        <dx:ASPxListBox ID="lstConfStatus" runat="server" SelectionMode="Multiple" Height="100px" Width="100%" ClientInstanceName="lstConfStatus" Rows="7" >
                                                            <ItemStyle><Paddings Padding="2px" /> </ItemStyle>
                                                            <Items>
                                                             <dx:ListEditItem Text="予定された" Value="0" />
                                                              <dx:ListEditItem Text="承認待ち" Value="1" />
                                                              <dx:ListEditItem Text="完了" Value="7" />
                                                              <dx:ListEditItem Text="消去済み" Value="9" />
                                                              <dx:ListEditItem Text="終了しました" Value="3" />
                                                              <dx:ListEditItem Text="即" Value="15" />
                                                              <dx:ListEditItem Text="すべて" Value="16" />                                                              
                                                            </Items>
                                                            <ClientSideEvents SelectedIndexChanged="function(s,e){OnShowButtonClick('confOccurencePopup');}" />
                                                         </dx:ASPxListBox>                                                         
                                                    </dx:PopupControlContentControl>
                                                </ContentCollection>
                                                 <ContentStyle> 
                                                    <Paddings PaddingBottom="5px" />
                                                </ContentStyle>
                                            </dx:ASPxPopupControl>
                                            <dx:ASPxPopupControl ID="confOccurencePopup" runat="server" ClientInstanceName="confOccurencePopup"
                                                EncodeHtml="false" HeaderText="Occurence" Width="150px" CloseAction="CloseButton" CssFilePath="../en/App_Themes/Plastic Blue/Web/styles.css"
                                                CssPostfix="PlasticBlue">
                                                <ContentCollection>
                                                    <dx:PopupControlContentControl>                                                       
                                                        <dx:ASPxListBox ID="lstConfOccurence" runat="server" SelectionMode="Multiple" Height="100px" Width="100%" ClientInstanceName="lstConfOccurence" Rows="7" >
                                                            <ItemStyle><Paddings Padding="2px" /> </ItemStyle>
                                                            <Items>
                                                              <dx:ListEditItem Text="シングル" Value="0" />
                                                              <dx:ListEditItem Text="定期的な" Value="1" />
                                                              <dx:ListEditItem Text="すべて" Value="2" />
                                                            </Items>
                                                            <ClientSideEvents SelectedIndexChanged="function(s,e){OnShowButtonClick('confDetailsPopup');}" />
                                                         </dx:ASPxListBox>                                                         
                                                    </dx:PopupControlContentControl>
                                                </ContentCollection>
                                                 <ContentStyle> 
                                                    <Paddings PaddingBottom="5px" />
                                                </ContentStyle>
                                            </dx:ASPxPopupControl>
                                            <dx:ASPxPopupControl ID="confDetailsPopup" runat="server" ClientInstanceName="confDetailsPopup"
                                                EncodeHtml="false" HeaderText="会議 詳細" Width="150px" CloseAction="CloseButton" CssFilePath="../en/App_Themes/Plastic Blue/Web/styles.css"
                                                CssPostfix="PlasticBlue">
                                                <ContentCollection>
                                                    <dx:PopupControlContentControl>                                                       
                                                        <dx:ASPxListBox ID="lstConfDetails" runat="server" SelectionMode="Multiple" Height="100px" Width="100%" ClientInstanceName="lstConfDetails" Rows="7" >
                                                            <ClientSideEvents SelectedIndexChanged="function(s,e){ShowConfMenu(s);}" />
                                                            <ItemStyle><Paddings Padding="2px" /> </ItemStyle>
                                                            <Items>
                                                              <dx:ListEditItem Text="会議の識別" Value="1" />
                                                              <dx:ListEditItem Text="会議タイトル" Value="2" />
                                                              <dx:ListEditItem Text="エンドポイント" Value="3" />
                                                              <dx:ListEditItem Text="MCU" Value="5" />
                                                              <dx:ListEditItem Text="ワークオーダー" Value="6" />
                                                              <dx:ListEditItem Text="すべて" Value="7" />
                                                            </Items>
                                                         </dx:ASPxListBox>                                                         
                                                    </dx:PopupControlContentControl>
                                                </ContentCollection>
                                                 <ContentStyle> 
                                                    <Paddings PaddingBottom="5px" />
                                                </ContentStyle>
                                            </dx:ASPxPopupControl>
                                            <dx:ASPxPopupControl ID="eptDetailsPopup" runat="server" ClientInstanceName="eptDetailsPopup"
                                                EncodeHtml="false" Width="150px" HeaderText="エンドポイント" CloseAction="CloseButton" CssFilePath="../en/App_Themes/Plastic Blue/Web/styles.css"
                                                CssPostfix="PlasticBlue">
                                                <ContentCollection>
                                                    <dx:PopupControlContentControl>                                                       
                                                        <dx:ASPxListBox ID="lstEptDetails" runat="server" SelectionMode="Multiple" Height="100px" Width="100%" ClientInstanceName="lstEptDetails" Rows="3" >
                                                            <ItemStyle><Paddings Padding="2px" /> </ItemStyle>
                                                            <Items>
                                                              <dx:ListEditItem Text="の名前" Value="1" />
                                                              <dx:ListEditItem Text="アドレスタイプ" Value="2" />
                                                              <dx:ListEditItem Text="アドレス" Value="3" />
                                                              <dx:ListEditItem Text="オプションをダイヤル" Value="4" />
                                                              <dx:ListEditItem Text="すべて" Value="5" />
                                                            </Items>
                                                         </dx:ASPxListBox>                                                         
                                                    </dx:PopupControlContentControl>
                                                </ContentCollection>
                                                 <ContentStyle> 
                                                    <Paddings PaddingBottom="5px" />
                                                </ContentStyle>
                                            </dx:ASPxPopupControl>
                                            <dx:ASPxPopupControl ID="woDetailsPopup" runat="server" ClientInstanceName="woDetailsPopup"
                                                EncodeHtml="false" Width="150px" HeaderText="ワークオーダー"  CloseAction="CloseButton" CssFilePath="../en/App_Themes/Plastic Blue/Web/styles.css"
                                                CssPostfix="PlasticBlue">
                                                <ContentCollection>
                                                    <dx:PopupControlContentControl>                                                       
                                                        <dx:ASPxListBox ID="lstWODetails" runat="server" SelectionMode="Multiple" Height="100px" Width="100%" ClientInstanceName="lstWODetails" Rows="3" >
                                                            <ItemStyle><Paddings Padding="2px" /> </ItemStyle>
                                                            <Items>
                                                              <dx:ListEditItem Text="インベントリ" Value="1" />
                                                              <dx:ListEditItem Text="ケータリング" Value="2" />
                                                              <dx:ListEditItem Text="ハウスキーピング" Value="3" />
                                                              <dx:ListEditItem Text="すべて" Value="4" />
                                                            </Items>
                                                         </dx:ASPxListBox>                                                         
                                                    </dx:PopupControlContentControl>
                                                </ContentCollection>
                                                 <ContentStyle> 
                                                    <Paddings PaddingBottom="5px" />
                                                </ContentStyle>
                                            </dx:ASPxPopupControl>
                                            <dx:ASPxPopupControl ID="mcuDetailsPopup" runat="server" ClientInstanceName="mcuDetailsPopup"
                                                EncodeHtml="false" Width="150px" HeaderText="MCU" CloseAction="CloseButton" CssFilePath="../en/App_Themes/Plastic Blue/Web/styles.css"
                                                CssPostfix="PlasticBlue">
                                                <ContentCollection>
                                                    <dx:PopupControlContentControl>                                                       
                                                        <dx:ASPxListBox ID="lstMCUDetails" runat="server" SelectionMode="Multiple" Height="100px" Width="100%" ClientInstanceName="lstMCUDetails" Rows="7" >
                                                            <ItemStyle><Paddings Padding="2px" /> </ItemStyle>
                                                            <Items>
                                                              <dx:ListEditItem Text="会議で使用されている" Value="1" />
                                                              <dx:ListEditItem Text="デフォルト" Value="2" />
                                                              <dx:ListEditItem Text="アドレス" Value="3" />
                                                              <dx:ListEditItem Text="すべて" Value="4" />
                                                            </Items>
                                                         </dx:ASPxListBox>                                                         
                                                    </dx:PopupControlContentControl>
                                                </ContentCollection>
                                                 <ContentStyle> 
                                                    <Paddings PaddingBottom="5px" />
                                                </ContentStyle>
                                            </dx:ASPxPopupControl>
                                            <dx:ASPxPopupControl ID="eptPopup" runat="server" ClientInstanceName="eptPopup"
                                                EncodeHtml="false" HeaderText="エンドポイント 詳細" Width="150px" CloseAction="CloseButton" CssFilePath="../en/App_Themes/Plastic Blue/Web/styles.css"
                                                CssPostfix="PlasticBlue">
                                                <ContentCollection>
                                                    <dx:PopupControlContentControl>                                                       
                                                        <dx:ASPxListBox ID="lstEptDetailsM" runat="server" SelectionMode="Multiple" Height="100px" Width="100%" ClientInstanceName="lstEptDetailsM" Rows="16" >
                                                            <ItemStyle><Paddings Padding="2px" /> </ItemStyle>
                                                            <Items>
                                                              <dx:ListEditItem Text="デフォルト プロフィール名" Value="1" />
                                                              <dx:ListEditItem Text="プロファイルの数" Value="2" />
                                                              <dx:ListEditItem Text="アドレスタイプ" Value="3" />
                                                              <dx:ListEditItem Text="アドレス" Value="4" />
                                                              <dx:ListEditItem Text="モデル" Value="5" />
                                                              <dx:ListEditItem Text="優先バンド幅" Value="6" />
                                                              <dx:ListEditItem Text="MCU 割り当て" Value="7" />
                                                              <dx:ListEditItem Text="優先ダイヤルオプション" Value="8" />
                                                              <dx:ListEditItem Text="デフォルトプロトコル" Value="9" />
                                                              <dx:ListEditItem Text="ウェブアクセスURL" Value="10" />
                                                              <dx:ListEditItem Text="ネットワーク場所" Value="11" />
                                                              <dx:ListEditItem Text="Telnet 有効にした" Value="12" />
                                                              <dx:ListEditItem Text="電子メールID" Value="13" />
                                                              <dx:ListEditItem Text="APIモジュール" Value="14" />
                                                              <dx:ListEditItem Text="カレンダーに招待です" Value="15" />
                                                              <dx:ListEditItem Text="すべて" Value="16" />
                                                            </Items>
                                                         </dx:ASPxListBox>                                                         
                                                    </dx:PopupControlContentControl>
                                                </ContentCollection>
                                                 <ContentStyle> 
                                                    <Paddings PaddingBottom="5px" />
                                                </ContentStyle>
                                            </dx:ASPxPopupControl>
                                            
                                            
                                        </td>
                                    </tr>
                                    <tr style="display: none; background-color: red;">
                                        <td>
                                            <input align="middle" type="button" runat="server" style="width: 100px; height: 21px;
                                                display: none;" id="ClosePUp" value=" 閉じる " class="altButtonFormat" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>                    
                    <tr valign="top" id="trSwt" runat="server" style="display: none">
                        <td align="right" valign="top">
                            <a id="ChgOrg" runat="server" href="#" class="blueblodtext">組織を切り替える
                            </a>
                        </td>
                    </tr>
                    <tr valign="top">
                        <td>
                            <dx:ASPxPanel ID="ASPxPanel1" runat="server" Width="130px" BackColor="#666699">
                                <PanelCollection>
                                    <dx:PanelContent ID="PanelContent1" runat="server">
                                        <dx:ASPxImage ID="ASPxImage1" runat="server" ImageUrl="../Image/category.png">
                                        </dx:ASPxImage>
                                        <dx:ASPxLabel ID="ASPxLabel3" runat="server" Text="カテゴリー" ForeColor="White">
                                        </dx:ASPxLabel>
                                    </dx:PanelContent>
                                </PanelCollection>
                            </dx:ASPxPanel>
                        </td>
                    </tr>
                    <%--Menu Start--%>
                    <tr>
                        <td>
                        <dx:ASPxPopupControl ID="SavedRptListPopup" runat="server" ClientInstanceName="SavedRptListPopup" ShowCloseButton="true"
                                EncodeHtml="false" HeaderText="レポート リスト" Width="150px" CloseAction="CloseButton" CssFilePath="../en/App_Themes/Plastic Blue/Web/styles.css"
                                CssPostfix="PlasticBlue">
                                <ContentCollection>
                                    <dx:PopupControlContentControl>
                                         <dx:ASPxListBox ID="lstReportList" SelectionMode="Multiple"  runat="server" Height="80px" Width="100%" ClientInstanceName="lstReportList">
                                            <ItemStyle><Paddings Padding="2px" /> </ItemStyle>
                                            <ClientSideEvents SelectedIndexChanged="function(s,e){SavedReportSelection();}" />
                                         </dx:ASPxListBox>                                                          
                                    </dx:PopupControlContentControl>
                                </ContentCollection>
                                 <ContentStyle> 
                                    <Paddings PaddingBottom="8px" />
                                </ContentStyle>
                            </dx:ASPxPopupControl>
                        </td>
                    </tr>
                    <tr valign="top">
                        <td>
                            <dx:ASPxMenu ID="MenuConference" runat="server" AutoSeparators="RootOnly" CssFilePath="../en/App_Themes/Plastic Blue/{0}/styles.css"
                                CssPostfix="PlasticBlue" ImageFolder="../en/App_Themes/Plastic Blue/{0}/" ItemSpacing="0px"
                                SeparatorHeight="100%" SeparatorWidth="2px" Width="100%">                                                                        
                                <ClientSideEvents ItemClick="function(s, e){fnShow('1');}" />
                                <RootItemSubMenuOffset FirstItemX="1" LastItemX="1" X="1" />
                                <Items>
                                    <dx:MenuItem Text="会議" Name="conf_1">
                                    </dx:MenuItem>
                                </Items>
                                <SeparatorBackgroundImage ImageUrl="../en/App_Themes/Plastic Blue/Web/mSeparatorHor.gif"
                                    VerticalPosition="Top" />
                                <ItemSubMenuOffset FirstItemY="-1" LastItemY="-1" Y="-1" />
                                <SubMenuStyle GutterWidth="0px" />
                            </dx:ASPxMenu>
                        </td>
                    </tr>
                    <tr >
                        <td>
                            <dx:ASPxMenu ID="MenuUsers" runat="server" AutoSeparators="RootOnly" CssFilePath="../en/App_Themes/Plastic Blue/{0}/styles.css"
                                CssPostfix="PlasticBlue" ImageFolder="../en/App_Themes/Plastic Blue/{0}/" ItemSpacing="0px"
                                SeparatorHeight="100%" SeparatorWidth="2px" Width="100%" >
                                <ClientSideEvents ItemClick="function(s, e){fnShow('2');}" />
                                <RootItemSubMenuOffset FirstItemX="1" LastItemX="1" X="1" />
                                <Items>
                                    <dx:MenuItem Text="ユーザー" Name="Users_1">
                                    </dx:MenuItem>
                                </Items>
                                <SeparatorBackgroundImage ImageUrl="../en/App_Themes/Plastic Blue/Web/mSeparatorHor.gif"
                                    VerticalPosition="Top" />
                                <ItemSubMenuOffset FirstItemY="-1" LastItemY="-1" Y="-1" />
                                <SubMenuStyle GutterWidth="0px" />
                            </dx:ASPxMenu>
                        </td>
                    </tr>
                    <tr >
                        <td>
                            <dx:ASPxMenu ID="MenuRooms" runat="server" AutoSeparators="RootOnly" CssFilePath="../en/App_Themes/Plastic Blue/{0}/styles.css"
                                CssPostfix="PlasticBlue" ImageFolder="../en/App_Themes/Plastic Blue/{0}/" ItemSpacing="0px"
                                SeparatorHeight="100%" SeparatorWidth="2px" Width="100%" >
                                <ClientSideEvents ItemClick="function(s, e){fnShow('3');}" />
                                <RootItemSubMenuOffset FirstItemX="1" LastItemX="1" X="1" />
                                <Items>
                                    <dx:MenuItem Text="部屋" Name="Rooms_1">
                                    </dx:MenuItem>
                                </Items>
                                <SeparatorBackgroundImage ImageUrl="../en/App_Themes/Plastic Blue/Web/mSeparatorHor.gif"
                                    VerticalPosition="Top" />
                                <ItemSubMenuOffset FirstItemY="-1" LastItemY="-1" Y="-1" />
                                <SubMenuStyle GutterWidth="0px" />
                            </dx:ASPxMenu>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <dx:ASPxMenu ID="MenuEndpt" runat="server" AutoSeparators="RootOnly" CssFilePath="../en/App_Themes/Plastic Blue/{0}/styles.css"
                                CssPostfix="PlasticBlue" ImageFolder="../en/App_Themes/Plastic Blue/{0}/" ItemSpacing="0px"
                                SeparatorHeight="100%" SeparatorWidth="2px" Width="100%" >
                                <ClientSideEvents ItemClick="function(s, e){fnShow('4');}" />
                                <RootItemSubMenuOffset FirstItemX="1" LastItemX="1" X="1" />
                                <Items>
                                    <dx:MenuItem Text="エンドポイント" Name="Ept_1">
                                    </dx:MenuItem>
                                </Items>
                                <SeparatorBackgroundImage ImageUrl="../en/App_Themes/Plastic Blue/Web/mSeparatorHor.gif"
                                    VerticalPosition="Top" />
                                <ItemSubMenuOffset FirstItemY="-1" LastItemY="-1" Y="-1" />
                                <SubMenuStyle GutterWidth="0px" />
                            </dx:ASPxMenu>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <dx:ASPxMenu ID="MenuMCU" runat="server" AutoSeparators="RootOnly" CssFilePath="../en/App_Themes/Plastic Blue/{0}/styles.css"
                                CssPostfix="PlasticBlue" ImageFolder="../en/App_Themes/Plastic Blue/{0}/" ItemSpacing="0px"
                                SeparatorHeight="100%" SeparatorWidth="2px" Width="100%" >
                                <ClientSideEvents ItemClick="function(s, e){fnShow('5');}" />
                                <RootItemSubMenuOffset FirstItemX="1" LastItemX="1" X="1" />
                                <Items>
                                    <dx:MenuItem Text="MCU" Name="Mcu_1">
                                    </dx:MenuItem>
                                </Items>
                                <SeparatorBackgroundImage ImageUrl="../en/App_Themes/Plastic Blue/Web/mSeparatorHor.gif"
                                    VerticalPosition="Top" />
                                <ItemSubMenuOffset FirstItemY="-1" LastItemY="-1" Y="-1" />
                                <SubMenuStyle GutterWidth="0px" />
                            </dx:ASPxMenu>
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 350px;">
                        </td>
                    </tr>
                    <%--Menu End--%>
                </table>
            </td>
            <td width="850px" valign="top" > <%--FB 2050--%>
                <table width="100%" align="center" border="0">
                    <tr runat="server" id="rptImgRow">
                        <td align="center">
                            <img src="../Image/ReportsII.png" alt="" runat="server" id="imgRpt" width="50" height="50" />
                        </td>
                    </tr>
                    <tr valign="top" runat="server" id="trDetails">
                        <td>
                            <dx:ASPxPopupControl ID="MoreInfoPopup" runat="server" ClientInstanceName="MoreInfoPopup" Width="150px" >
                                <ContentCollection>
                                    <dx:PopupControlContentControl>   
                                        <dx:ASPxMemo ID="txtMemo" runat="server" Height="91px" Width="170px" ClientInstanceName="memo" Border-BorderStyle="None">
                                            <ClientSideEvents Init="function(s, e) {s.GetInputElement().style.overflowY='hidden';}" />
                                        </dx:ASPxMemo>                                        
                                    </dx:PopupControlContentControl>
                                </ContentCollection>
                                 <ContentStyle> 
                                    <Paddings PaddingBottom="5px" />
                                </ContentStyle>
                            </dx:ASPxPopupControl>
                            
                            <%--Main Grid--%>
                            <div id="MainDiv" style="width:1000px;overflow-y:auto;overflow-x:auto; word-break:break-all;HEIGHT: 440px;" >
                            <dx:ASPxGridView ID="MainGrid" ClientInstanceName="MainGrid" runat="server" Width="100%" EnableCallBacks="false" AllowSort="true"
                             CssFilePath="../en/App_Themes/Plastic Blue/{0}/styles.css" OnHtmlRowCreated="MainGrid_HtmlRowCreated"   >
                                <Templates>
                                    <DetailRow>
                                        <div style="width:1000px;overflow-y:auto;overflow-x:auto; word-break:break-all;" runat="server" id="DetailsDiv" >
                                            <dx:ASPxGridView ID="DetailGrid"  ClientInstanceName="DetailGrid" runat="server" Width="100%" OnPageIndexChanged="detailGrid_DataSelect" OnInit="detailGrid_DataSelect" >
                                                <Settings ShowFooter="True" />    
                                                <SettingsPager Mode="ShowAllRecords"/>  
                                            </dx:ASPxGridView>
                                        </div>
                                    </DetailRow>
                                </Templates>
                                <SettingsDetail ShowDetailRow="true" />
                                <Styles CssFilePath="../en/App_Themes/Plastic Blue/{0}/styles.css"  CssPostfix="PlasticBlue">
                                    <Header ImageSpacing="9px" SortingImageSpacing="9px" ForeColor="White" Font-Size="9pt"></Header>
                                    <Cell    Font-Size="9pt"></Cell>                                 
                                </Styles>
                                <Images ImageFolder="../en/App_Themes/Plastic Blue/{0}/">
                                    <CollapsedButton Height="10px" Url="../en/App_Themes/Plastic Blue/GridView/gvCollapsedButton.png" Width="9px" />
                                    <ExpandedButton Height="9px" Url="../en/App_Themes/Plastic Blue/GridView/gvExpandedButton.png" Width="9px" />
                                    <HeaderFilter Height="11px" Url="../en/App_Themes/Plastic Blue/GridView/gvHeaderFilter.png" Width="11px" />
                                    <HeaderActiveFilter Height="11px" Url="../en/App_Themes/Plastic Blue/GridView/gvHeaderFilterActive.png" Width="11px" />
                                    <HeaderSortDown Height="11px" Url="../en/App_Themes/Plastic Blue/GridView/gvHeaderSortDown.png" Width="11px" />
                                    <HeaderSortUp Height="11px" Url="../en/App_Themes/Plastic Blue/GridView/gvHeaderSortUp.png" Width="11px" />
                                    <FilterRowButton Height="13px" Width="13px" />
                                    <CustomizationWindowClose Height="14px" Width="14px" />
                                    <PopupEditFormWindowClose Height="14px" Width="14px" />
                                    <FilterBuilderClose Height="14px" Width="14px" />
                                </Images>  
                                <Settings ShowFilterRow="True" ShowGroupPanel="True" ShowHeaderFilterButton="True" ShowTitlePanel="True" 
                                ShowHeaderFilterBlankItems="False" />
                                <SettingsPager ShowDefaultImages="False" Mode="ShowPager" AlwaysShowPager="true" Position="Top">
                                    <AllButton Text="すべて"></AllButton>
                                    <NextPageButton Text="次 &gt;"></NextPageButton>
                                    <PrevPageButton Text="&lt; 以前の"></PrevPageButton>
                                </SettingsPager>
                            </dx:ASPxGridView>
                            </div>
                            <dx:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="MainGrid"></dx:ASPxGridViewExporter>
                         </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
<%--code added for Soft Edge button--%>

<script type="text/javascript">
    var mainDiv = document.getElementById("MainDiv");
    if(mainDiv)
    {  //Difference 180
    
        if (window.screen.width <= 1024)
            mainDiv.style.width = "845px";
        else
            mainDiv.style.width = "1184px";        
    }
    
    if (rbSaved.GetChecked())
         btnPreview.SetVisible(false); 
         
</script>


<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->

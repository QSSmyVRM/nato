<%@ Page Language="C#" AutoEventWireup="true" Inherits="en_Thankyou" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">


<html xmlns="http://www.w3.org/1999/xhtml" >
  
<head runat="server">
    <title>myVRMビデオ会議予約スケジューラ</title>
  
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr valign="top"> 
              <td id="thankTop" runat="server"  width="100%" height="72" colspan="2">
              </td>
            </tr>
            <tr>
               <td valign="top" align="right" style="width:20%;" >
	              <br/><br/><br/><br/><br/><br/><br/><br/>
                  <img src="../image/company-logo/SiteLogo.jpg"> <%--FB 1830--%>
                </td>
                <td align="center">
	                 <p>ありがとうございました！ログアウトしました。</p>
	                    <p>クリック <a href="../jp/genlogin.aspx">ここ</a> 再度ログインする</p> <%--FB 1830--%>
	                    <%
                            if (Application["ssoMode"].ToString().ToUpper() == "NO")
	                        {
                                if (Application["Client"] != null)
                                    if (Application["Client"].ToString().ToUpper().Equals("PSU"))
                                        Response.Redirect("https://webaccess.psu.edu/cgi-bin/logout");
		                                Response.Cookies["VRMuser"]["act"] = "";
		                                Response.Cookies["VRMuser"]["pwd"] = "";
                                        Response.Redirect("~/en/genlogin.aspx?m=1"); //FB 1830
                            }
	                           
                        %>
                </td>
            </tr>            
        </table>
    </div>
    </form>
    <script language="javascript" type="text/javascript">
            var obj = document.getElementById('thankTop');
            if(obj != null)
            {
//            if (window.screen.width <= 1024)
//                obj.background = "../en/image/company-logo/StdBanner.jpg"; // Organization Css Module 
//            else
//                obj.background = "../en/image/company-logo/HighBanner.jpg";  //Organization Css Module 
            //FB 1830
            if (window.screen.width <= 1024)
                obj.background = "../image/company-logo/StdBanner.jpg";
            else
                obj.background = "../image/company-logo/HighBanner.jpg";  
            }
   
 
</script>
</body>
</html>

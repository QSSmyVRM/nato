﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DataImport.aspx.cs" Inherits="ns_DataImport.DataImport" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <!-- FB 2050 -->
<meta http-equiv="X-UA-Compatible" content="IE=7" /> <!-- FB 2050 -->
<!-- #INCLUDE FILE="inc/maintopNET.aspx" --> 

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
<script language="javascript">

function DataLoading()
{
    var obj = document.getElementById("tblDataImport");
    if (obj != null)
        obj.style.display="";
}
</script>
    <title>データベースのインポート</title>
  <link rel="stylesheet" title="Expedite base styles" type="text/css" href="<%=Session["OrgCSSPath"]%>"> <%--Organization CSS Module --%>
</head>
<body>
    <form id="frmDataImport" runat="server">
    <br /><br />
    <center>
        <asp:Label ID="errLabel" runat="server" CssClass="lblError"></asp:Label>
    </center>
    <br /><br /><br />
        <h3 style="text-align: center">
            データインポートツール</h3>
            <br /><br />
            <table id="tblDataImport">
            <tr>
                 <td align="center">
                    <b><img border="0" src="image/wait1.gif" width="100" height="25">
               </td>
            </tr>
           </table>            
            <table width="100%" bgcolor="white" cellpadding="1" cellspacing="0">
                <tr>
                    <td width="40%" align="center">
                        <b>外部データベースの種類:</b>&nbsp;
                        <asp:DropDownList CssClass="altLong0SelectFormat" ID="lstDatabaseType" runat="server">
                            <asp:ListItem Selected="True" Text="ランデブー" Value="1"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td align="left">
                        <asp:FileUpload ID="fleMasterCSV" Width="80%" EnableViewState="true" runat="server" CssClass="altText" />  
                        <asp:Button ID="btnGetDataTable" runat="server" OnClick="GenerateDataTable" CssClass="altShortBlueButtonFormat" Text="アップロード" />
                    </td>
                </tr>
            </table>
            <br /><br />
            <table width="100%" bgcolor="white" cellpadding="1" cellspacing="0" border="1" >
                <tr>
                    <td style="text-align: center">
                        1</td>
                    <td>
                        インポート層1</td>
                    <td width="20%" align="center">
                        <asp:Button ID="btnImportTier1" runat="server" CssClass="altShortBlueButtonFormat"
                            Text="送信" OnClick="ImportTier1s" OnClientClick="javascript:DataLoading()" /></td>
                </tr>
                <tr bgcolor=Gainsboro>
                    <td style="text-align: center">
                        2</td>
                    <td>
                        インポート層2</td>
                    <td align="center">
                        <asp:Button ID="btnImportTier2" runat="server" CssClass="altShortBlueButtonFormat"
                            Text="送信" OnClick="ImportTier2s" OnClientClick="javascript:DataLoading()" /></td>
                </tr>
                <tr>
                    <td style="text-align: center">
                        3</td>
                    <td>
                        インポート部門</td>
                    <td align="center">
                        <asp:Button ID="btnImportDepartment" runat="server" CssClass="altShortBlueButtonFormat"
                            Text="送信" OnClick="ImportDepartments" OnClientClick="javascript:DataLoading()" /></td>
                </tr>
                
                
                <tr bgcolor=Gainsboro>
                    <td style="text-align: center">
                        4</td>
                    <td>
                        インポート MCU(s)</td>
                    <td align="center">
                        <asp:Button ID="btnImportmcu" runat="server" CssClass="altShortBlueButtonFormat"
                            Text="送信" OnClick="Importmcu" OnClientClick="javascript:DataLoading()"  /></td>
                </tr>
                
                <tr>
                    <td style="text-align: center">
                        5</td>
                    <td>
                        インポートエンドポイ</td>
                    <td align="center">
                        <asp:Button ID="btnImportEndpoints" runat="server" CssClass="altShortBlueButtonFormat"
                            Text="送信" OnClick="ImportEndpoints" OnClientClick="javascript:DataLoading()" /></td>
                </tr>
                <tr bgcolor=Gainsboro>
                    <td style="text-align: center">
                        6</td>
                    <td>
                        インポート部屋</td>
                    <td align="center">
                        <asp:Button ID="btnImportRooms" runat="server" CssClass="altShortBlueButtonFormat"
                          Text="送信" OnClick="ImportRooms" OnClientClick="javascript:DataLoading()" /></td>
                </tr>
                <tr>
                    <td style="text-align: center">
                        7</td>
                    <td>
                        インポートユーザー</td>
                    <td align="center">
                        <asp:Button ID="btnImportUsers" runat="server" CssClass="altShortBlueButtonFormat"
                            Text="送信" OnClick="ImportUsers" OnClientClick="javascript:DataLoading()" /></td>
                </tr>
                <tr bgcolor=Gainsboro>
                    <td style="text-align: center">
                        8</td>
                    <td>
                        インポート会議</td>
                    <td align="center">
                        <asp:Button ID="btnImportConferences" runat="server" CssClass="altShortBlueButtonFormat" Text="送信" OnClick="ImportConferences" OnClientClick="javascript:DataLoading()" /></td>
                </tr>
                 <tr>
                    <td style="text-align: center">
                        9</td>
                    <td>
                        インポートのデフォルトCSS、XML
                        
                        <asp:FileUpload ID="cssXMLFileUpload" Width="50%" EnableViewState="true" runat="server" CssClass="altText" />  
                        
                    </td>
                    <td align="center">
                        <asp:Button ID="btnImportDefaultCSSXML" runat="server" CssClass="altShortBlueButtonFormat" OnClick="ImportDefaultCSSXML" Text="送信" OnClientClick="javascript:DataLoading()" />
                   </td>
                </tr>
            </table>
        <br />
        <table>
            <tr>
                <td align="left">
                    <b>注釈:</b> このセクションでは、localhostのみを使用してWebサーバからアクセスする必要があり. データファイルは、Webサーバーは、自己に常駐している必要があり.
                </td>
            </tr>
        </table>
        <%--<table id="tblDataImport">
            <tr>
                <td>
                    <b><font color="#FF00FF" size="2">データローディング中 ...</font></b>&nbsp;&nbsp;&nbsp;&nbsp;<img border="0" src="image/wait1.gif" width="100" height="12">
                </td>
            </tr>
        </table>--%>
    </form>
<br />
<br />
<p>&nbsp;</p>
<p>&nbsp;</p>
<script language="javascript">
document.getElementById("tblDataImport").style.display="none";
</script>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" --> 
</body>
</html>

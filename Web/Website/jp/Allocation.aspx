<%@ Page Language="C#" AutoEventWireup="true"  Inherits="en_Allocation" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <!-- FB 2050 -->
<meta http-equiv="X-UA-Compatible" content="IE=6" /> <!-- FB 2050 -->
<!--Window Dressing-->
<!-- #INCLUDE FILE="inc/maintopNet.aspx" -->
<%
    if(Session["userID"] == null)
    {
        Response.Redirect("~/en/genlogin.aspx"); //FB 1830

    }
    else
    {
        Application.Add("COM_ConfigPath", "C:\\VRMSchemas_v1.8.3\\ComConfig.xml");
        Application.Add("MyVRMServer_ConfigPath", "C:\\VRMSchemas_v1.8.3\\");
    }
%>

<script language="JavaScript1.2" src="inc/functions.js" type="text/javascript" />

 
    <script src="inc/menuinc.js" type="text/javascript"></script>



<html xmlns="http://www.w3.org/1999/xhtml"> 
<head runat="server">
    <title>バルクツール</title>
       <link rel="stylesheet" type="text/css" media="all" href="css/calendar-win2k-1.css" title="win2k-1" />
         <link rel="stylesheet" title="Expedite base styles" type="text/css" href="<%=Session["OrgCSSPath"]%>" > <%-- Organization Css Module --%>
    <link rel="stylesheet" type="text/css" media="all" href="css/calendar-blue.css" /> <%--FB 1861--%> <%--FB 1982--%>

    <script type="text/javascript">
      var servertoday = new Date(parseInt("<%=DateTime.Now.Year%>", 10), parseInt("<%=DateTime.Now.Month%>", 10)-1, parseInt("<%=DateTime.Now.Date%>", 10),
      parseInt("<%=DateTime.Now.Hour%>", 10), parseInt("<%=DateTime.Now.Minute%>", 10), parseInt("<%=DateTime.Now.Second%>", 10));
    </script>
<%--FB 1861--%>
  <%--<script type="text/javascript" src="script/cal.js"></script>--%>
<script type="text/javascript" src="script/cal-flat.js"></script>
<script type="text/javascript" src="lang/calendar-en.js"></script>
<script type="text/javascript" src="script/calendar-setup.js"></script>
<script type="text/javascript" src="script/calendar-flat-setup.js"></script>
    <script language="JavaScript" type="text/javascript">
<!--

	tabs=new Array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","ALL")
	
//	sortimg = "";

	function setOprValue()
	{
		var m1 = "", m2="", oprval="", cb = document.frmAllocation.DdlOperation;
		
		switch (cb.options[cb.selectedIndex].value) {
		    case "15"://FB 1979
		        m1 = "<label class='blackblodtext'>モバイルを有効に</label>"
		        oprval = document.frmAllocation.enableMobile.value;
		        break;
		    case "14"://FB 1599
				m1 = "<label class='blackblodtext'>エクスチェンジを有効にする</label>"
				oprval = document.frmAllocation.enableExchange.value;
				break;
		    case "13"://FB 1599
				m1 = "<label class='blackblodtext'>ドミノを有効にする</label>"
				oprval = document.frmAllocation.enableDomino.value;
				break;
		    case "12"://window dressing
		        m1 = "<label class='blackblodtext'>オーディオ/ビデオ]タブを有効にする</label>"
				oprval = document.frmAllocation.enableAV.value;
				break;
		    case "11": //window dressing
				m1 = "<label class='blackblodtext'>表示のタイムゾーン <BR>全画面での</label>" 
				oprval = document.frmAllocation.tzdisplay.value;
				break;
			case "10"://window dressing
				m1 = "<label class='blackblodtext'>タイムフォーマット</label>"
				oprval = document.frmAllocation.tformat.value;
				break;
			case "9"://window dressing
				m1 = "<label class='blackblodtext'>データフォーマット</label>"
				oprval = document.frmAllocation.dtformat.value;
				break;
			case "6"://window dressing
				m1 = "<label class='blackblodtext'>ユーザーのタイムゾーン</label>"
				oprval = document.frmAllocation.timezones.value;
				break;
			case "5"://window dressing
				m1 = "<label class='blackblodtext'>ユーザーロール</label>"
				oprval = document.frmAllocation.roles.value;
				break;
			case "4"://window dressing
				m1 = "<label class='blackblodtext'>優先する言語</label>"
				oprval = document.frmAllocation.languages.value;
				break;
			case "3"://window dressing
				m1 = "<label class='blackblodtext'>割り当てられた MCU</label>"
				oprval = document.frmAllocation.bridges.value;
				break;
			case "0":
                //Window Dressing
				m1 = "<label for='OprValue' class='blackblodtext'>財布に数分を追加します。</label>"
				m2 = "<input type='text' name='OprValue' id='OprValue' size='28' value='' class='altText'>";
				break;
			case "2":
                //Window Dressing
				m1 = "<label for='OprValue' class='blackblodtext'>新しいアカウントの有効期限</label>"
				m2 = "<input type='text' name='OprValue' id='OprValue' size='16' value='' class='altText' readonly='true'> ";
				m2 += "<img src='image/calendar.gif' valign='centre' border='0' width='20' height='20' id='cal_trigger' style='cursor: pointer; z-index:10' title='日付セレクタ'"; // FB 2050
                m2 += " onclick='return showCalendar(\"OprValue\", \"cal_trigger\", 1, \"<%=format%>\");' />";//FB 1073
				break;
			case "1":
                //Window Dressing
				m1 = "<label for='OprValue' class='blackblodtext'>部署に割り当てる</label>"
				oprval = document.frmAllocation.departments.value;
				break;
		}

		if (oprval != "") {
			valsary = oprval.split("||"); // FB 1888
			if (cb.options[cb.selectedIndex].value == "1")
				m2  = "<select size='5' name='OprValue' id='OprValue' class='altLong0SelectFormat' multiple>"
			else
				m2  = "<select size='1' name='OprValue' id='OprValue' class='altLong0SelectFormat'>"
			for (i = 0; i < valsary.length - 1; i++) {
				valary = valsary[i].split("``");
				
				m2 += "  <option value='" + valary[0] + "'>" + valary[1] + "</option>"
			}
			m2 += "</select>"
			
		}
		
		document.getElementById ("OprValueDIV0").innerHTML = m1;
		document.getElementById ("OprValueDIV1").innerHTML = m2;

		switch (cb.options[cb.selectedIndex].value) {
			//Code Removed temporarily to be added
			case "0":
				setFieldValue ("OprValue", "28800"); 
				break;
		}
	}
	
	
	
	function chgpageoption(totalpagenum)
	{
	
		tpn = parseInt(totalpagenum, 10)
		cb = document.frmAllocation.SelPage;
		
		RemoveAllOptions (cb);
		
		for (var i=1; i<=tpn; i++) {
			addopt(cb, i, i, false, false);
		}
		
		
	}



	function typeimg(ifrmname, sb)
	{
	    
		imgsrc = document.getElementById("img_" + ifrmname + "_" + sb).src;
		sorting = (imgsrc.indexOf(".gif") == -1) ? 0 : ((imgsrc.indexOf("up.gif")==-1) ? -1 : 1);
		
		for (i = 1; i < 5; i++) {
			if (i != 3)
				document.getElementById("img_" + ifrmname + "_" + i).src = "image/bg.gif";
		}
		document.getElementById("img_" + ifrmname + "_" + sb).src = ((sorting == 0) ? "image/sort_up.gif" : ((sorting == 1) ? "image/sort_up.gif" : "image/sort_up.gif")) ;
	}
	

	function nameimg(prename, sel)
	{
	    
		for (i in tabs) {
			document.getElementById(prename + tabs[i]).style.backgroundColor = ""; 
		}

		document.getElementById(prename + sel).style.backgroundColor = "#FF6699"; 
	}
	
	
	function pageimg(selectedpn)
	{
	
		document.frmAllocation.pageno.value = selectedpn;
		
		if (document.frmAllocation.SelPage.options.length == 1)
			document.frmAllocation.SelPage.options.selected = true;
		else
			document.frmAllocation.SelPage.options[parseInt(selectedpn, 10)-1].selected = true;
	}
	

	function seltype(ifrmname, sb)
	{	
		document.frmAllocation.sortby.value = sb;
		
		document.frmAllocation.pageno.value = 1;
		if (document.frmAllocation.SelPage.options.length>0)
			document.frmAllocation.SelPage.options[0].selected = true; 

        eval(ifrmname).window.location.href = "ifrmaduserlist.aspx?f=GetAllocation&f=frmAllocation&sb=" + document.frmAllocation.sortby.value + "&a=" + document.frmAllocation.alphabet.value + "&pn=" + document.frmAllocation.pageno.value+"&wintype=ifr";
		//eval(ifrmname).window.location.href = "dispatcher/admindispatcher.asp?cmd=GetAllocation&f=frmAllocation&sb=" + document.frmAllocation.sortby.value + "&a=" + document.frmAllocation.alphabet.value + "&pn=" + document.frmAllocation.pageno.value;

		document.frmAllocation.CheckAllAdUser.checked = false;
	}


	function selname(ifrmname, sel)
	{
		document.frmAllocation.alphabet.value = sel;
		document.frmAllocation.pageno.value = 1;
		if (document.frmAllocation.SelPage.options.length>0)
			document.frmAllocation.SelPage.options[0].selected = true;
			
		eval(ifrmname).window.location.href = "ifrmaduserlist.aspx?f=GetAllocation&f=frmAllocation&sb=" + document.frmAllocation.sortby.value + "&a=" + document.frmAllocation.alphabet.value + "&pn=" + document.frmAllocation.pageno.value+"&wintype=ifr";
		//eval(ifrmname).window.location.href = "dispatcher/admindispatcher.asp?cmd=GetAllocation&f=frmAllocation&sb=" + document.frmAllocation.sortby.value + "&a=" + document.frmAllocation.alphabet.value + "&pn=" + document.frmAllocation.pageno.value;
		document.frmAllocation.CheckAllAdUser.checked = false;
	}
	

	function selpage(ifrmname, cb)
	{
		document.frmAllocation.pageno.value = cb.options[cb.selectedIndex].value;

		eval(ifrmname).window.location.href = "ifrmaduserlist.aspx?f=GetAllocation&f=frmAllocation&sb=" + document.frmAllocation.sortby.value + "&a=" + document.frmAllocation.alphabet.value + "&pn=" + document.frmAllocation.pageno.value+"&wintype=ifr";
		//eval(ifrmname).window.location.href = "dispatcher/admindispatcher.asp?cmd=GetAllocation&f=frmAllocation&sb=" + document.frmAllocation.sortby.value + "&a=" + document.frmAllocation.alphabet.value + "&pn=" + document.frmAllocation.pageno.value;

		document.frmAllocation.CheckAllAdUser.checked = false;
		
		
	}
	

	function Sort(ifrmname, id, totalnum)
	{
		if ( eval("document." + ifrmname).sortlist ) {
			eval("document." + ifrmname).sortlist(id);

			imgsrc = document.getElementById("img_" + ifrmname + "_" + id).src;
			sorting = (imgsrc.indexOf(".gif") == -1) ? 0 : ((imgsrc.indexOf("up.gif")==-1) ? -1 : 1);
			for (i = 1; i <= totalnum; i++) {
				document.getElementById("img_" + ifrmname + "_" + i).src = "image/bg.gif";
			}
			document.getElementById("img_" + ifrmname + "_" + id).src = ((sorting == 0) ? "image/sort_up.gif" : ((sorting == 1) ? "image/sort_down.gif" : "image/sort_up.gif")) ;
		}
	}


	function checkallusr(chk)
	{
		var needstop = false;
					
		if (ifrmADuserlist.document.frmIfrmuserlist.adusers) {
			xmlstr = ifrmADuserlist.document.frmIfrmuserlist.adusers.value;
			usersary = xmlstr.split("||"); // FB 1888
			
			cb = ifrmADuserlist.document.frmIfrmuserlist.seladuser

			if (usersary.length == 2) {
				usersary[0] = usersary[0].split("``");
				if (cb.checked != chk) {
					cb.checked = chk;
					needstop = ifrmADuserlist.adUserClicked(cb, usersary[0][0], usersary[0][1], usersary[0][2], usersary[0][3]);
				}
			}
			
			if (usersary.length > 2) {
				for (i = 0; i < cb.length; i++) {
					usersary[i] = usersary[i].split("``");
					if (cb[i].checked != chk) {
						cb[i].checked = chk;
						needstop = ifrmADuserlist.adUserClicked(cb[i], usersary[i][0], usersary[i][1], usersary[i][2], usersary[i][3]);
						
						if (needstop)
							break;
					}
				}
			}

		}
	}
	
	
	function checkallgrp(chk)
	{
		var needstop = false;
		if (ifrmAllogrouplist.document.frmIfrmgrouplist.selgrp) {
			cb = ifrmAllogrouplist.document.frmIfrmgrouplist.selgrp

			if (cb.length > 0)
				for (i = 0; i < cb.length; i++) {
					
					if (cb[i].checked != chk) {
						cb[i].checked = chk;
						needstop = ifrmAllogrouplist.grpClicked(cb[i], i);
						if (needstop) break;
					}
					
				}
			else {
				if (cb.checked != chk) {
					cb.checked = chk;
					ifrmAllogrouplist.grpClicked(cb, 0);
				}
			}
		}
	}
	
	
	function deleteall()
	{
		if (ifrmVRMuserlist.delUser) {
			xmlstr = document.frmAllocation.SelectedUser.value;
			usersary = xmlstr.split("||"); // FB 1888
			
			for (i = 0; i < usersary.length; i++) {
				usersary[i] = usersary[i].split("``");
				
				ifrmVRMuserlist.delUser(usersary[i][0]);
			}
			
			document.frmAllocation.CheckAllAdUser.checked = false;
		}
	}


	function frmAllocation_Validator ()
	{
		if (document.frmAllocation.SelectedUser.value == "") {
			alert("ユーザーを最初に選択してください.") 
			return false;
		}
		
		var cb = document.frmAllocation.DdlOperation;
		switch (cb.options[cb.selectedIndex].value) {
		    case "0": 
		        var obj = document.getElementById("OprValue");
		        if (obj != null)
		        {
		            if (isNaN(obj.value))
		            {
		                    alert("ウォレットへの追加分で正の整数を入力してください。");
		                    return false;  
		            }
		            else if (obj.value <= 0)
		            {
		                    alert("ウォレットへの追加分で正の整数を入力してください。");
		                    return false;  
                    }
                    else if (obj.value > 999999)
                    {
                        alert("ウォレットへの追加分で1000000未満の正の整数を入力してください");
		                return false;  
                    }
		        }
		        break;
		    case "2":
		       
		        var objDt = document.getElementById("OprValue");
		        if(objDt != null)
		        {
		            
		            if(objDt.value == "")
		            {
		                alert("有効な日付を入力してください！");
		                return false;
		            } 
		        
		        }
		        break;
		        case "1":
		        var objDt = document.getElementById("OprValue");
		        if(objDt != null)
		        {
		            
		            if(objDt.value == "")
		            {
		                alert("部門を選択してください");
		                return false;
		            } 
		        
		        }
		        break;
		    case "3": 
                // Disable submit button if there are no MCU present
                var obj = document.getElementById("OprValue");
                if (obj == null)
                {
                    alert("ＭＣＵを最初に作成してください!");
                    document.getElementById('AllocationSubmit').disabled=true;
                    return false;
                }
                break;

			case "7":
			    var isConfirm = confirm("あなたはこれらのユーザー·アカウントを削除してもよろしいです ?\n")
				return isConfirm;
				break;
			case "8":
			    var isConfirm = confirm("あなたはこれらのユーザー·アカウントをロックしてもよろしいです ?\n")
				return isConfirm;
				break;
		}

		
		return true;
	}

//-->
</script>
    
</head>
<body>
    <form lang="JavaScript" method="post" id="frmAllocation" onsubmit="return frmAllocation_Validator()" runat="server">
        <div>
            <center>
                <h3>
                    バルクツール</h3>
            </center>
            <table width="95%">
                <tr align="center">
                    <td align="center">
                        <asp:Label ID="LblError" runat="server" CssClass="lblError"></asp:Label> <%--FB 1599--%>
                    </td>
                </tr>
            </table>
            
                <input name="cmd" id="cmd" type="hidden" value="SetAllocation" />
                <input name="users" id="hdnusers" type="hidden" runat="server" />
                <input name="Action" id="Action" type="hidden" />
                <input name="SelectedUser" id="SelectedUser" type="hidden" runat="server" />
                <input name="sortby" id="sortby" type="hidden"  runat="server" />
                <input name="alphabet" id="alphabet" type="hidden"  runat="server" />
                <input name="pageno" id="pageno" type="hidden"  runat="server" />
                <input name="timezones" id="timezones" type="hidden"  runat="server" />
                <input name="roles" id="roles" type="hidden"  runat="server" />
                <input name="languages" id="languages" type="hidden"  runat="server" />
                <input name="bridges" id="bridges" type="hidden"  runat="server" />
                <input name="departments" id="departments" type="hidden"  runat="server" />
                <input name="canAll" id="canAll" type="hidden"  runat="server" />
                <input name="totalPages" id="totalPages" type="hidden"  runat="server" />
                <input name="totalNumber" id="totalNumber" type="hidden"  runat="server" />
                <input name="dtformat" id="dtformat" type="hidden"  runat="server" />
                <input name="tformat" id="tformat" type="hidden"  runat="server" />
                <input name="tzdisplay" id="tzdisplay" type="hidden"  runat="server" />
                <input name="enableAV" id="enableAV" type="hidden"  runat="server" />
                <%--FB 1599--%>
                <input name="enableDomino" id="enableDomino" type="hidden"  runat="server" />
                <input name="enableExchange" id="enableExchange" type="hidden"  runat="server" />
                <%--FB 1979--%>
                <input name="enableMobile" id="enableMobile" type="hidden"  runat="server" />

                <input id="helpPage" type="hidden" value="94" />
                <center>
                    <table border="0" cellpadding="0" cellspacing="0" height="104" width="950">
                        <tr align="left">
                            <td valign="top" style="width: 2%; height: 53px;">
                                &nbsp;
                            </td>
                            <td style="width: 1%; height: 53px">
                                &nbsp;</td>
                            <td style="width: 97%; height: 53px">
                                <span class="subtitleblueblodtext">ユーザー選択して</span><br />
                                <span class="blackblodtext">
                                <%--Window Dressing--%>
                                <font size="2"><span class="cmtfldstarText">-</span> ユーザーを追加するには: アクティブユーザーの1つまたは複数のチェックボックスをオンにし.<br />
                                    <span class="cmtfldstarText">-</span> ユーザーを削除する：クリック
                                    <img style="border: 0; height: 12; width: 12" src="image/btn_delete.gif" alt="削除 ボタン" />
                                    選択したユーザーのユーザー名の横に。. </font></span>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" style="height: 10">
                            </td>
                        </tr>
                        <tr>
                            <td style="height: 651px">
                            </td>
                            <td style="height: 651px">
                            </td>
                            <td align="center" valign="middle" style="height: 651px">
                                <table border="0" cellpadding="2" cellspacing="2" width="100%">
                                    <tr>
                                        <td style="width: 4px; height: 624px">
                                        </td>
                                        <td style="height: 624px; vertical-align: top">
                                            <table border="0" cellpadding="0" cellspacing="4" width="100%">
                                                <tr>
                                                    <td colspan="2" style="height: 15" valign="top">
                                                        <table width="100%">
                                                            <tr>
                                                                <td align="right">
                                                                    <table width="100%" style="height: 100%">
                                                                        <tr>
                                                                        <%--Window Dressing--%>
                                                                            <td valign="top" class="blackblodtext">
                                                                                <span class="blackblodtext"><b>で始まる:</b></span> <%--Edited for FF--%>
                                                                            </td>
                                                                            <td>
                                                                                <a onclick="JavaScript: selname('ifrmADuserlist', 'A')"><span id="uA" class="tabtext">
                                                                                    0-A</span></a> <a onclick="JavaScript: selname('ifrmADuserlist', 'B')"><span id="uB"
                                                                                        class="tabtext">B</span></a> <a onclick="JavaScript: selname('ifrmADuserlist', 'C')">
                                                                                            <span id="uC" class="tabtext">C</span></a> <a onclick="JavaScript: selname('ifrmADuserlist', 'D')">
                                                                                                <span id="uD" class="tabtext">D</span></a> <a onclick="JavaScript: selname('ifrmADuserlist', 'E')">
                                                                                                    <span id="uE" class="tabtext">E</span></a> <a onclick="JavaScript: selname('ifrmADuserlist', 'F')">
                                                                                                        <span id="uF" class="tabtext">F</span></a> <a onclick="JavaScript: selname('ifrmADuserlist', 'G')">
                                                                                                            <span id="uG" class="tabtext">G</span></a> <a onclick="JavaScript: selname('ifrmADuserlist', 'H')">
                                                                                                                <span id="uH" class="tabtext">H</span></a> <a onclick="JavaScript: selname('ifrmADuserlist', 'I')">
                                                                                                                    <span id="uI" class="tabtext">I</span></a> <a onclick="JavaScript: selname('ifrmADuserlist', 'J')">
                                                                                                                        <span id="uJ" class="tabtext">J</span></a>
                                                                                <a onclick="JavaScript: selname('ifrmADuserlist', 'K')"><span id="uK" class="tabtext">
                                                                                    K</span></a> <a onclick="JavaScript: selname('ifrmADuserlist', 'L')"><span id="uL"
                                                                                        class="tabtext">L</span></a> <a onclick="JavaScript: selname('ifrmADuserlist', 'M')">
                                                                                            <span id="uM" class="tabtext">M</span></a><br />
                                                                                <a onclick="JavaScript: selname('ifrmADuserlist', 'N')"><span id="uN" class="tabtext">
                                                                                    N</span></a> <a onclick="JavaScript: selname('ifrmADuserlist', 'O')"><span id="uO"
                                                                                        class="tabtext">O</span></a> <a onclick="JavaScript: selname('ifrmADuserlist', 'P')">
                                                                                            <span id="uP" class="tabtext">P</span></a> <a onclick="JavaScript: selname('ifrmADuserlist', 'Q')">
                                                                                                <span id="uQ" class="tabtext">Q</span></a> <a onclick="JavaScript: selname('ifrmADuserlist', 'R')">
                                                                                                    <span id="uR" class="tabtext">R</span></a> <a onclick="JavaScript: selname('ifrmADuserlist', 'S')">
                                                                                                        <span id="uS" class="tabtext">S</span></a> <a onclick="JavaScript: selname('ifrmADuserlist', 'T')">
                                                                                                            <span id="uT" class="tabtext">T</span></a> <a onclick="JavaScript: selname('ifrmADuserlist', 'U')">
                                                                                                                <span id="uU" class="tabtext">U</span></a> <a onclick="JavaScript: selname('ifrmADuserlist', 'V')">
                                                                                                                    <span id="uV" class="tabtext">V</span></a> <a onclick="JavaScript: selname('ifrmADuserlist', 'W')">
                                                                                                                        <span id="uW" class="tabtext">W</span></a>
                                                                                <a onclick="JavaScript: selname('ifrmADuserlist', 'X')"><span id="uX" class="tabtext">
                                                                                    X</span></a> <a onclick="JavaScript: selname('ifrmADuserlist', 'Y')"><span id="uY"
                                                                                        class="tabtext">Y</span></a> <a onclick="JavaScript: selname('ifrmADuserlist', 'Z')">
                                                                                            <span id="uZ" class="tabtext">Z-</span></a>
                                                                            </td>
                                                                            <td id="tabAll" align="right" style="width:10%">
                                                                                <a onclick="JavaScript: selname('ifrmADuserlist', 'ALL')"><span id="uALL" class="tabtext">
                                                                                    すべて</span></a>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <%--Window Dressing--%>
                                                                <td align="right" class="blackblodtext">
                                                                    行く
                                                                    <select name="SelPage" id="SelPage" class="altText" onchange="selpage('ifrmADuserlist', this)" size="1" runat="server">
                                                                    </select>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                    </td>
                                                    <td align="left">
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                            <tr>
                                                                <td align="center" style="height: 18px; width: 7%">
                                                                </td>
                                                                <td align="center" style="height: 18px; width: 25%">
                                                                    <a class="e" href="javascript:seltype('ifrmADuserlist', 1)">ファーストネーム（名）</a><img id="img_ifrmADuserlist_1"
                                                                        height="10" src="image/bg.gif" width="10" /></td>
                                                                <td align="center" style="height: 18px; width: 25%">
                                                                    <a class="e" href="javascript:seltype('ifrmADuserlist', 2)">姓</a><img id="img_ifrmADuserlist_2"
                                                                        height="10" src="image/bg.gif" width="10" /></td>
                                                                <td align="center" style="height: 18px; width: 43%">
                                                                    <a class="e" href="javascript:seltype('ifrmADuserlist', 4)">メール</a><img id="img_ifrmADuserlist_4"
                                                                        height="10" src="image/bg.gif" width="10" /></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                   <%--Window Dressing--%>
                                                    <td align="right" valign="top" style="width: 10%" class="blackblodtext">
                                                        <b>アクティブユーザー</b><span class="reqfldText">*</span>
                                                    </td>
                                                   <%--Window Dressing--%>
                                                     <td align="left" class="blackblodtext">

                               
                                                      <iframe src="ifrmaduserlist.aspx?f=frmAllocation&wintype=ifr" name="ifrmADuserlist" width="100%" height="450" align="left" valign="top">
                                                        <p>行く <a href="">アクティブなユーザーリスト</a></p>
                                                      </iframe> 

                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="height: 20px">
                                                    </td>
                                                    <td align="left" style="height: 20px" class="blackblodtext"> <%--window dressing--%>
                                                        <input name="CheckAllAdUser" id="CheckAllAdUser" onclick="JavaScript: checkallusr(this.checked);" type="checkbox" runat="server" value="1" />
                                                        すべて
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td align="center" valign="middle" style="height: 624px">
                                        </td>
                                        <td align="center" valign="middle" style="height: 624px">
                                            <table border="0" cellpadding="0" cellspacing="4" width="100%">
                                                <tr>
                                                    <td>
                                                    </td>
                                                    <td align="left">
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                            <tr>
                                                                <td align="center" style="width: 7%">
                                                                </td>
                                                                <td align="center" style="width: 25%">
                                                                    <a class="e" href="javascript:Sort('ifrmVRMuserlist', 1, 3)">ファーストネーム（名）</a><img id="img_ifrmVRMuserlist_1"
                                                                        height="10" src="image/bg.gif" width="10" alt="ifrmVRMuserlist1" /></td>
                                                                <td align="center" style="width: 25%">
                                                                    <a class="e" href="javascript:Sort('ifrmVRMuserlist', 2, 3)">姓</a><img id="img_ifrmVRMuserlist_2"
                                                                        height="10" src="image/bg.gif" width="10" alt="ifrmVRMuserlist2" /></td>
                                                                <td align="center" style="width: 25%">
                                                                    <a class="e" href="javascript:Sort('ifrmVRMuserlist', 3, 3)">メール</a><img id="img_ifrmVRMuserlist_3"
                                                                        height="10" src="image/bg.gif" width="10" alt="ifrmVRMuserlist3" /></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                <%--Window Dressing--%>
                                                    <td align="right" valign="top" style="width: 10%" class="blackblodtext">
                                                        <b>選択されたユーザー</b>
                                                    </td> 
                                                      <td style="width:90%" align="left" valign="top">

                                                      <iframe src="ifrmvrmuserlist.aspx?f=frmAllocation&wintype=ifr" name="ifrmVRMuserlist" width="100%" height="500" align="left" valign="top">
                                                        <p>行く <a href="">選択したユーザーリスト</a></p>
                                                      </iframe> 

                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                    </td>
                                                    <td align="right">
                                                        <input class="altShort4BlueButtonFormat"  name="AllocationSubmit" onclick="JavaScript: deleteall();"
                                                            type="button" value="すべて削除" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr align="left">
                            <td valign="top">
                                &nbsp;
                            </td>
                            <td >
                                &nbsp;</td>
                            <td >
                                <span class="subtitleblueblodtext">選択したユーザーを設定する</span><br />
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td align="right">
                                <table border="0" cellpadding="2" cellspacing="0" width="100%">
                                    <tr>
                                        <td align="right" style="height: 10px; width: 12%">
                                            <%--Window Dressing--%>
                                            <label class="blackblodtext">
                                                アクション</label>
                                        </td>
                                        <td align="left" style="height: 26px; width: 30%">
                                            <asp:DropDownList ID="DdlOperation" CssClass="altLong0SelectFormat" runat="server" onChange="JavaScript:setOprValue();">
                                                <asp:ListItem Value="0">財布に数分を追加します。</asp:ListItem>
                                                <asp:ListItem Value="1">部署に割り当てる</asp:ListItem>
                                                <asp:ListItem Value="2">アカウント有効期限変更</asp:ListItem>
                                                <asp:ListItem Value="3">割り当てられたMCUの変更</asp:ListItem>
                                                <%--FB 1404 <asp:ListItem Value="4">Change Preferred Language</asp:ListItem> --%>
                                                <asp:ListItem Value="4">優先言語を変更する</asp:ListItem> <%--FB 2027--%>
                                                <asp:ListItem Value="5">ユーザーの役割変更</asp:ListItem>
                                                <asp:ListItem Value="6">ユーザーのタイムゾーン変更</asp:ListItem>
                                                <asp:ListItem Value="7">ユーザー削除</asp:ListItem>
                                                <asp:ListItem Value="8">ロックユーザー</asp:ListItem>
                                                <asp:ListItem Value="9">データフォーマット</asp:ListItem>
                                                <asp:ListItem Value="10">タイムフォーマット</asp:ListItem>
                                                <asp:ListItem Value="11">タイムゾーン表示する</asp:ListItem>
                                                <asp:ListItem Value="12">有効にするAV</asp:ListItem>
                                                <%--FB 1599--%>
                                                <asp:ListItem Value="13">ノートを有効にする</asp:ListItem><%--FB 2164--%>
                                                <asp:ListItem Value="14">有効にする  見通し</asp:ListItem>
                                                <asp:ListItem Value="15">携帯電話を有効にする</asp:ListItem> <%--FB 1979--%>
                                            </asp:DropDownList>
                                        </td>
                                        <td style="height: 26px; width: 5%">
                                        </td>
                                        <td id="OprValueDIV0" align="right" style="width: 20%; height: 26px;">
                                        </td>
                                        <td id="OprValueDIV1" align="left" style="width: 32%; height: 26px;">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <br />
                    <table border="0" cellpadding="2" cellspacing="0" width="600">
                        <tr>
                            <td align="center">
                                <input class="altBlueButtonFormat" name="reset" onclick="JavaScript: history.go(0);"
                                    type="reset" value="リセット" />
                            </td>
                            <td align="center">
                                    <%--code added for Soft Edge button--%>
                                <asp:Button  CssClass="altBlueButtonFormat" onfocus="this.blur()" ID="AllocationSubmit" Text="送信" runat="server" OnClick="Submit_Click" />
                            </td>
                        </tr>
                        <tr>
                        </tr>
                    </table>
                </center>
            </form>

            <script type="text/javascript" >
            <!--
                setOprValue();
                //-->
            </script>
</body>
</html>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->

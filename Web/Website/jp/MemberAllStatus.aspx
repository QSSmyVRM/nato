<%@ Page Language="C#" Inherits="ns_MyVRM.en_MemberAllStatus" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">


  <link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/border-table.css">
  <link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/main-table.css">
  <link rel="stylesheet" title="Expedite base styles" type="text/css" href="<%=Session["OrgCSSPath"]%>">  

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>グループの詳細</title>
</head>
<body>
    <form id="form1" runat="server">
    <div style="position:static">
    <table align="center">
     <%--Window Dressing--%>
    <tr> <td align="center"><h3><asp:Label ID="GroupName" runat="server" ></asp:Label></h3> </td></tr>
        <tr>
         <td align="center">
           <asp:DataGrid ID="dgViewDetails" ShowHeader="true" Width="100%" CellPadding="5" CellSpacing="0"  BorderStyle="None" BorderWidth="0" runat="server" AutoGenerateColumns="false"> <%--Edited for FF--%>
             <ItemStyle CssClass="tableBody"  />
             <HeaderStyle CssClass="tableHeader" Height="30px" />
            <AlternatingItemStyle HorizontalAlign="left" VerticalAlign="Top" />
            <Columns>
                <asp:BoundColumn DataField="userID" Visible="false"></asp:BoundColumn>
     <%--Window Dressing--%>
               <asp:TemplateColumn HeaderText = "メンバー 名前" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass ="tableHeader" ItemStyle-HorizontalAlign="Left">
                   <ItemTemplate>
                   <asp:Label runat="server" ID="lblUserName"></asp:Label>
                   </ItemTemplate>
                   <ItemTemplate>
                   <asp:Label runat="server" ID="lblUserName" Text='<%# DataBinder.Eval(Container, "DataItem.userFirstName") + "&nbsp;" + DataBinder.Eval(Container, "DataItem.userLastName") %>'></asp:Label>
                   </ItemTemplate>
               </asp:TemplateColumn>
     <%--Window Dressing--%>
                 <asp:BoundColumn ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="tableBody"  DataField="userEmail" HeaderStyle-CssClass="tableHeader" HeaderText="メンバー メール"></asp:BoundColumn>
             </Columns>
        </asp:DataGrid>
         </td>
        </tr>
        <tr>
        <td>
          <center>
        <%--code changed for Soft Edge button--%>
        <input type="button" onfocus="this.blur()" name="closewindow" value="閉じる ウィンドウ" class="altYellowButtonFormat" onClick='window.close()'/>
      </center>
        </td>
        </tr>
    </table>
    </div>
    </form>
</body>
</html>

﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="ns_MyVRM.en_ManageVirtualMeetingRoom" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
<meta http-equiv="X-UA-Compatible" content="IE=7" />

<script type="text/javascript">

    function getYourOwnEmailList(i) {
        if (i == -2) {
            if (queryField("sb") > 0)
                url = "emaillist2.aspx?t=e&frm=roomassist&wintype=ifr&fn=frmVirtualMettingRoom&n=";
            else
                url = "emaillist2main.aspx?t=e&frm=roomassist&fn=frmVirtualMettingRoom&n=";
        }
        else {
            url = "emaillist2main.aspx?t=e&frm=approver&fn=frmVirtualMettingRoom&n=" + i;
        }
        if (!window.winrtc) {	// has not yet been defined
            winrtc = window.open(url, "", "width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
            winrtc.focus();
        } else // has been defined
            if (!winrtc.closed) {     // still open
            winrtc.close();
            winrtc = window.open(url, "", "width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
            winrtc.focus();
        } else {
            winrtc = window.open(url, "", "width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
            winrtc.focus();
        }
    }


    function deleteAssistant() {
        eval("document.frmVirtualMettingRoom.AssistantID").value = "";
        eval("document.frmVirtualMettingRoom.Assistant").value = "";
    }

    function fnClose() {
        window.location.replace("manageroom.aspx?hf=&m=&pub=&d=&comp=&f=&frm=");
        return true;
    }

    function frmMainroom_Validator() {

        if (!Page_ClientValidate())
            return Page_IsValid;

        var txtroomname = document.getElementById('<%=txtRoomName.ClientID%>');
        if (txtroomname.value == "") {
            reqName.style.display = 'block';
            txtroomname.focus();
            return false;
        }
        else if (txtroomname.value.search(/^(a-z|A-Z|0-9)*[^\\<>^+?|!`\[\]{}\=@$%&~]*$/) == -1) {
            regRoomName.style.display = 'block';
            txtroomname.focus();
            return false;
        }

        var hdnmultipledept = document.getElementById('<%=hdnMultipleDept.ClientID%>');
        var departmentlist = document.getElementById('<%=DepartmentList.ClientID%>');

        if (hdnmultipledept.value == 1) {
            if ((departmentlist.value == "") && (departmentlist.length > 0)) {
                isConfirm = confirm("Are you sure you want to set up this room with no department(s) assigned?\n")
                if (isConfirm == false) {
                    return (false);
                }
            }
        }

        return (true);
    }
</script>

<script type="text/javascript" src="inc/functions.js"></script>
<script type="text/javascript" src="script/errorList.js"></script>
<script type="text/javascript" src="extract.js"></script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Manage Virtual Meeting Room</title>
    <link rel="StyleSheet" href="css/divtable.css" type="text/css" />
    <link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/border-table.css" />
    <link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/main-table.css" />
    <link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/main.css" />
    <link rel="stylesheet" title="Expedite base styles" type="text/css" href="<%=Session["OrgCSSPath"]%>" />
</head>
<body>
    <form id="frmVirtualMettingRoom" runat="server">
    <input type="hidden" id="hdnMultipleDept" runat="server" />
    <input type="hidden" id="AssistantID" runat="server" />
    <input type="hidden" id="hdnRoomID" runat="server" />
    <input type="hidden" id="AssistantName" runat="server" />
    
    <div>
        <center>
            <table border="0" width="100%" cellpadding="2" cellspacing="2">
                <tr>
                    <td align="center">
                        <h3>
                            <asp:Label ID="lblTitle" runat="server" Text="仮想会議室"></asp:Label></h3>
                        <br />
                        <asp:Label ID="errLabel" runat="server" CssClass="lblError"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <table id="Table4" cellpadding="2" cellspacing="2" border="0" style="width: 100%">
                            <tr align="left">
                                <td style="width: 10%" align="left" valign="top" class="blackblodtext" nowrap>
                                    最後のによって変更さ :
                                </td>
                                <td style="width: 85%" align="left" valign="top">
                                    <asp:Label ID="lblMUser" runat="server" CssClass="active"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 10%" align="left" valign="top" class="blackblodtext" nowrap>
                                    最後に修正 :
                                </td>
                                <td style="width: 85%" align="left" valign="top">
                                    <asp:Label ID="lblMdate" runat="server" CssClass="active"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="subtitleblueblodtext" align="left">
                       ベーシック環境設定
                    </td>
                </tr>
                <tr>
                    <td>
                        <table border="0" width="100%" cellpadding="2" cellspacing="2">
                            <tr>
                                <td style="width: 15%" align="right" valign="top" class="blackblodtext">
                                    <b>部屋名</b> <span class="reqfldText">*</span>
                                </td>
                                <td style="width: 35%" align="left" valign="top">
                                    <asp:TextBox ID="txtRoomName" runat="server" CssClass="altText"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="reqName" runat="server" ControlToValidate="txtRoomName"
                                        Display="dynamic" CssClass="lblError" SetFocusOnError="true" Text="必須"
                                        ValidationGroup="Submit"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="regRoomName" ControlToValidate="txtRoomName"
                                        Display="dynamic" runat="server" ValidationGroup="Submit" SetFocusOnError="true"
                                        ErrorMessage="<br> & < > + % \ ? | ^ = ! ` [ ] { } $ @  and ~ 無効な文字があります."
                                        ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>^+?|!`\[\]{}\=@$%&~]*$"></asp:RegularExpressionValidator>
                                </td>
                                <td style="width: 15%" align="right" valign="top" class="blackblodtext">
                                    <b>アシスタント責任者</b><span class="reqfldText">*</span>
                                </td>
                                <td style="width: 30%" align="left" valign="top">
                                    <asp:TextBox ID="Assistant" runat="server" CssClass="altText"></asp:TextBox>
                                    <a id="EditHref" href="javascript:getYourOwnEmailList(-2);" onmouseover="window.status='';return true;">
                                        <img border="0" src="image/edit.gif" alt="edit" width="17" height="15" /></a>
                                    <a href="javascript: deleteAssistant();" onmouseover="window.status='';return true;">
                                        <img border="0" src="image/btn_delete.gif" alt="delete" width="16" height="16" /></a>
                                    <asp:RequiredFieldValidator ID="AssistantValidator" runat="server" ControlToValidate="Assistant"
                                        Display="dynamic" CssClass="lblError" SetFocusOnError="true" Text="必須"
                                        ValidationGroup="Submit"></asp:RequiredFieldValidator><%--FB 2448--%>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" class="blackblodtext">
                                    内部番号
                                </td>
                                <td align="left">
                                    <asp:TextBox CssClass="altText" ID="txtInternalnum" MaxLength="25" runat="server"></asp:TextBox>
                                </td>
                                <td align="right" class="blackblodtext">
                                    外線番号
                                </td>
                                <td align="left">
                                    <asp:TextBox CssClass="altText" ID="txtExternalnum" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <span class="subtitleblueblodtext">部門</span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table id="tbRoomsDept" cellpadding="2" cellspacing="2" border="0" style="width: 95%">
                            <tr>
                                <td width="15%" align="right" valign="top" class="blackblodtext">
                                    <b>部屋のカテゴリーを見る</b>
                                </td>
                                <td style="width: 75%" align="left" valign="top">
                                    <asp:ListBox ID="DepartmentList" runat="server" CssClass="altText" DataTextField="Name"
                                        DataValueField="ID" SelectionMode="multiple"></asp:ListBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="height: 8px">
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <table id="tblButtons" cellpadding="2" cellspacing="2" border="0" style="width: 90%">
                            <tr>
                                <td align="center" style="width: 33%">
                                    <asp:Button ID="btnReset" runat="server" Text="リセット" CssClass="altLongBlueButtonFormat"
                                        OnClick="ResetRoomProfile" />
                                </td>
                                <td align="center" style="width: 33%">
                                    <input name="Go" type="button" class="altLongBlueButtonFormat" onclick="javascript:fnClose();"
                                        value=" 戻る " />
                                </td>
                                <td align="center" style="width: 33%">
                                    <asp:Button ID="btnSubmitAddNew" runat="server" ValidationGroup="Submit" Text="送信 / 新しいルーム"
                                        CssClass="altLongBlueButtonFormat" OnClick="SetRoomProfile" OnClientClick="javascript:return frmMainroom_Validator()" />
                                </td>
                                <td align="center" style="width: 33%">
                                    <asp:Button ID="btnSubmit" ValidationGroup="Submit" runat="server" Text="送信"
                                        CssClass="altLongBlueButtonFormat" OnClick="SetRoomProfile" OnClientClick="javascript:return frmMainroom_Validator()" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </center>
    </div>
    </form>
</body>
</html>

<script type="text/javascript">

    if (document.getElementById("hdnMultipleDept"))
        if (document.getElementById("hdnMultipleDept").value == "0") {
        if (document.getElementById("trRoomsDept") != null)
            document.getElementById("trRoomsDept").style.display = "none";
    }
    //FB 2448        
</script>

<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->

﻿<%@ Page Language="C#" ValidateRequest="false" Inherits="ns_ConfMCUInfo.ConfMCUInfo" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ConfMCUInfo</title>
    <link title="Expedite base styles" href="<%=Session["OrgCSSPath"]%>" type="text/css"
        rel="stylesheet" />

    <script type="text/javascript" src="script/errorList.js"></script>

</head>
<body>
    <form id="frmconfMCUinfo" runat="server">
    <div>
        <center>
            <br />
            <h3>
                <asp:Label ID="lblMCUInfo" runat="server" Font-Bold="true" Text="サーバ時間の詳細"></asp:Label></h3>
            <asp:Label ID="lblError" runat="server" CssClass="lblError"></asp:Label>
        </center>
        <table border="0" width="70%" align="center" cellpadding="2" cellspacing="3">
            <tr>
                <td class="subtitlexxsblueblodtext" colspan="3">
                    会議詳細
                </td>
            </tr>
            <tr>
                <td width="10%">
                </td>
                <td align="left" nowrap="nowrap" width="25%">
                    <b>タイトル</b>
                </td>
                <td width="65%">
                    <asp:Label ID="lblConfName" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="left">
                </td>
                <td align="left" nowrap="nowrap">
                    <b>開始日/時間</b>
                </td>
                <td>
                    <asp:Label ID="lblStartTime" runat="server"></asp:Label>
                    <br />
                </td>
            </tr>
            <tr>
                <td class="subtitlexxsblueblodtext" nowrap="nowrap" colspan="3">
                    サーバーの時刻
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td align="left" nowrap="nowrap">
                    <b>ウェブサーバー </b>
                </td>
                <td>
                    <asp:Label ID="lblWebServerTime" runat="server"></asp:Label>
                </td>
            </tr>
            <tr id="trDatabaseServerTime" runat="server">
                <td>
                </td>
                <td align="left" nowrap="nowrap">
                    <b>データベースサーバー </b>
                    <br />
                </td>
                <td>
                    <asp:Label ID="lblDatabaseServerTime" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="subtitlexxsblueblodtext" colspan="3">
                    MCU
                </td>
            </tr>
            <tr class="blackblodtext">
                <td align="center" colspan="2">
                    名前
                </td>
                <td align="center">
                    ローカライズされた時間
                </td>
            </tr>
            <tr>
                <td height="1" colspan="3" bgcolor="CCCCCC">
                </td>
            </tr>
            <tr align="center">
                <td colspan="3">
                    <table id="tblConfMCUinfo" runat="server" border="0" width="90%">
                    </table>
                    <asp:Label runat="server" ID="tdNoMCU" Visible="false" Text="MCUは見つかりません"></asp:Label>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>

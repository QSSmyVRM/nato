﻿<%@ Page Language="C#" Inherits="ns_MyVRM.ManageOrganization" Buffer="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <!-- FB 2050 -->
<meta http-equiv="X-UA-Compatible" content="IE=7" /> <!-- FB 2050 -->

<!-- #INCLUDE FILE="inc/maintopNET.aspx" --> 
<script type="text/javascript" src="inc/functions.js"></script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <script language="javascript" type="text/javascript">
        function FnCancel()
		{
			window.location.replace('SuperAdministrator.aspx');
		}
    </script>
    <title>組織を管理する</title>
    <link rel="stylesheet" title="Expedite base styles" type="text/css" href="<%=Session["OrgCSSPath"]%>" />
</head>
<body>
    <form id="frmManageOrganization" runat="server">
    
    <div>
        <table cellpadding="0" cellspacing="0" id="OuterTable" width="100%" border="0">
            <tr>
                <td align="center">
                    <h3>
                        <asp:Label ID="lblHeader" runat="server" Text="組織を管理する"></asp:Label>
                    </h3>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError"></asp:Label>
                    <div id="dataLoadingDIV" style="z-index:1"></div>
                </td>
            </tr>
            <tr>
                <td align="Left">
                    <table width="100%">
                        <tr>
                            <td >&nbsp;</td>
                            <td>
                                <SPAN class="subtitleblueblodtext"></SPAN>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:DataGrid ID="dgOrganizations" runat="server" AutoGenerateColumns="False" CellPadding="3" GridLines="None"
                         BorderColor="blue" BorderStyle="solid" BorderWidth="1" OnEditCommand="EditOrganizationProfile" OnDeleteCommand="DeleteOrganizationProfile" OnCancelCommand="PurgeNow_Click"  OnItemDataBound="BindRowsDeleteMessage" ShowFooter="false" Width="90%" Visible="true" style="border-collapse:separate"><%-- Edited for FF--%> <%--FB 1753 //FB 2074--%>
                        <%--Window Dressing - Start--%>                        
                        <SelectedItemStyle CssClass="tableBody" />
                        <EditItemStyle CssClass="tableBody" />
                        <AlternatingItemStyle CssClass="tableBody" />
                        <ItemStyle CssClass="tableBody" />
                         <%--Window Dressing - End--%> 
                        <HeaderStyle CssClass="tableHeader" HorizontalAlign="left" />
                        <Columns>
                            <asp:BoundColumn DataField="orgId" Visible="false" ><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn> 
                            <asp:BoundColumn DataField="organizationName" ItemStyle-CssClass="tableBody"  HeaderStyle-CssClass="tableHeader" ItemStyle-HorizontalAlign="Left" HeaderText="組織名"></asp:BoundColumn>
                            <asp:BoundColumn DataField="phone" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" ItemStyle-HorizontalAlign="Left" HeaderText="電話番号"></asp:BoundColumn>
                            <asp:BoundColumn DataField="emailID" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" ItemStyle-HorizontalAlign="Left" HeaderText="電子メールID"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="アクション" ItemStyle-CssClass="tableBody" HeaderStyle-HorizontalAlign="Center">
                                <HeaderStyle CssClass="tableHeader" HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <table width="50%" align="center">
                                        <tr>
                                            <td nowrap="nowrap">
                                                <asp:LinkButton ID="lnkEdit" runat="server" Text="編集" CommandName="Edit" OnClientClick="DataLoading(1)"></asp:LinkButton>
                                            </td>
                                            <td nowrap="nowrap">
                                                <asp:LinkButton ID="lnkDelete" runat="server" Text="削除" CommandName="Delete" Enabled='<%# !DataBinder.Eval(Container, "DataItem.orgId").ToString().Trim().Equals("11")%>'></asp:LinkButton>
                                            </td>
                                            <td nowrap="nowrap">
                                                <asp:LinkButton ID="lnkPurge" runat="server" Text="パージする" CommandName="Cancel"  Enabled='<%# !DataBinder.Eval(Container, "DataItem.orgId").ToString().Trim().Equals("11")%>'></asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:Table runat="server" ID="tblNoOrganizations" Visible="false" Width="90%">
                        <asp:TableRow CssClass="lblError">
                            <%--Windows Dressing--%>
                            <asp:TableCell CssClass="lblError" HorizontalAlign="center">
                                いいえ組織見つかった.  <%--Edited for FB 1405--%>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                    <table cellspacing="0" cellpadding="0" width="90%" border="0" align="center">
                        <tr>
                            <td width="60%">&nbsp;</td>
                            <td class="blackblodtext" align="right" nowrap>合計組織:&nbsp;
                                <span id="SpnActiveOrgs" runat="server" class="blackblodtext"></span>
                            </td>
                            <td class="blackblodtext" align="right" nowrap>ライセンス残り:&nbsp;
                                <span id="SpnLicense" runat="server" class="blackblodtext"></span>
                            </td>
                        </tr>
                    </table>                    
                </td>
            </tr>
            <tr>
                <td align="center" width="90%"><br />
                    <table cellspacing="0" cellpadding="1" width="90%" border="0">
                        <tr>
                            <td>
                                <SPAN class=subtitleblueblodtext>新規組織作成</SPAN>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center"><br />
                    <table cellspacing="0" cellpadding="3" width="90%" border="0" align="center">
                        <tr >
                            <td align="right">
								<input class="altShort0BlueButtonFormat" onclick="FnCancel()" type="button" value="キャンセル" name="btnCancel">
								&nbsp;&nbsp;
								<asp:Button ID="btnNewOrganization" runat="server" CssClass="altShort0BlueButtonFormat" Text="送信" OnClick="CreateNewOrganization" />
                            </td>
                            
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
         <script type="text/javascript" src="inc/softedge.js"></script>
    </div>
    </form>
</body>
</html>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->

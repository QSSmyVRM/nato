﻿<html>
<head>
<title>ありがとうございました</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<body bottommargin="0" leftmargin="0" rightmargin="0" topmargin="0" marginheight="0" marginwidth="0" background="image/background.gif">
  <br>
  <center>
	<br><br><br>
	<font size="4">
      <p>許可が同時に同じアカウントから2回ログインすることを拒否する.<br>
      いつか後にログインをお試しください。.</p><br><br>
      <p>このメッセージの考えられる理由は、かもしれません :<br><br>
      <li><font color="red">アカウントは現在すでにアクティブになっている.</font><br>
      <li><font color="red">Webサーバとのセッションが突然停止し、又は切断されている.</font></p>
      <br><br>
      <p><font size="2">でテクニカルサポートの連絡先myVRMための <%=Application["contactPhone"]%>.</font></p>
	</font>
  </center>
</body>
</html>
<%@ Page Language="C#" AutoEventWireup="true" Inherits="ns_MyVRM.ViewCustomAttributes"%>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>

<%--Edited for FF--%>
<%if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("MSIE"))
{%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%}
else {%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN"> 
<%} %>
<!-- #INCLUDE FILE="inc/maintopNET.aspx" --> 

<link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/border-table.css">
<link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/main-table.css">
<link rel="stylesheet" title="Expedite base styles" type="text/css" href="<%=Session["OrgCSSPath"]%>">  

<script>

function FnCancel()
{
	window.location.replace('organisationsettings.aspx');
}
</script>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server" id="Head1">
<%--FB 1975 - Start--%>
<script>
function suppressBackspace(evt) 
{
     evt = evt || window.event;
     var target = evt.target || evt.srcElement; 
     if (evt.keyCode == 8 && !/input|textarea/i.test(target.nodeName)) 
     { 
        window.location.replace('OrganisationSettings.aspx');        
        return false;
     } 
} 
document.onkeydown = suppressBackspace;
document.onkeypress = suppressBackspace; 

//FB 2045 - Start
function fnconfirmdel(status)
{
    if(status == true)
        return false;

    if(confirm("あなたは、このカスタムオプションを削除してもよろしいです?"))
    {
        return true;
    }
    else
    {
        return false;
    }
}
//FB 2045 - End


</script>
<%--FB 1975 - End--%>
    <title>カスタムオプションを見る</title>
</head>
<body >
    <form id="frmCustomAttribute" runat="server" method="post" >
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
     <input type="hidden" id="helpPage" value="73"/>
     <input type="hidden" id="HdnCustOptID" runat="server" value="<%=customAttrID%>" />
        <table width="100%">
            <tr>
                <td align="center" colspan="2">
                    <h3>
                       カスタムオプションを見る
                    </h3>
                </td>
            </tr>
            <tr>
                <td align="center" style="width: 1168px" colspan="2">
                    <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="center" colspan="2"> <%--FB 1779 ItemDatabound added--%>
                    <asp:DataGrid ID="dgCustomAttribute" runat="server" AutoGenerateColumns="False" CellPadding="2" GridLines="None" AllowSorting="true" 
                         BorderColor="blue" BorderStyle="solid" BorderWidth="1" ShowFooter="False" Width="85%" OnItemCreated="BindRowsDeleteMessage"
                         OnItemDataBound="BindRowsToGrid" OnDeleteCommand="DeleteCustomGrid" OnEditCommand="EditCustomGrid" Visible="true" style="border-collapse:separate"> <%--Edited for FF--%><%--FB 1779--%>
                        <SelectedItemStyle  CssClass="tableBody"/>
                          <AlternatingItemStyle CssClass="tableBody" />
                         <ItemStyle CssClass="tableBody"  />
                        <HeaderStyle CssClass="tableHeader" Height="30px" />
                        <EditItemStyle CssClass="tableBody" />
                        <FooterStyle CssClass="tableBody"/>
                        <Columns>
                            <asp:BoundColumn DataField="CustomAttributeID" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" HeaderText="カスタム属性のID" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="RowUID" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" HeaderText="シリアル番号"></asp:BoundColumn>
                            <asp:BoundColumn ItemStyle-Width="10%" DataField="CreateType" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" HeaderText="タイプを作成する"></asp:BoundColumn><%--FB 1779--%>
                            <asp:BoundColumn ItemStyle-Width="20%" DataField="Title" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" HeaderText="ディスプレイのタイトル" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ></asp:BoundColumn> <%-- FB 2050 --%>
                            <asp:BoundColumn ItemStyle-Width="25%" DataField="Description" ItemStyle-CssClass="tableBody"  HeaderStyle-CssClass="tableHeader" HeaderText="説明" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ></asp:BoundColumn> <%-- FB 2050 --%>
                            <asp:BoundColumn ItemStyle-Width="15%" DataField="Type" ItemStyle-CssClass="tableBody"  HeaderStyle-CssClass="tableHeader" HeaderText="タイプ" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ></asp:BoundColumn> <%-- FB 2050 --%>
                            <asp:BoundColumn DataField="CAStatus" ItemStyle-CssClass="tableBody"  HeaderStyle-CssClass="tableHeader" HeaderText="ステータスを有効にする"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Mandatory" ItemStyle-CssClass="tableBody"  HeaderStyle-CssClass="tableHeader" HeaderText="必須ステータス"></asp:BoundColumn>
                            <asp:BoundColumn DataField="IncludeInEmail" ItemStyle-CssClass="tableBody"  HeaderStyle-CssClass="tableHeader" HeaderText="Eメールに含む"></asp:BoundColumn>
                            
                            <asp:TemplateColumn Visible="false" HeaderText="オプション値" ItemStyle-CssClass="tableBody"  HeaderStyle-CssClass="tableHeader">
                                <ItemTemplate>
                                <table>
                                <tr>
                                    <td>
                                        <asp:LinkButton runat="server" Text="オプションの値を表示" ID="ViewOption" CommandName="ViewOption"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                <td colspan="8">
                                  <asp:Table runat="server" ID="tblCustomOptions" Visible="true" BorderColor="black" Width="100%" CellPadding="0" CellSpacing="0" BorderWidth="1" BorderStyle="Inset">
                                    <asp:TableRow>
                                        <asp:TableCell>                                                
                                            <asp:DataGrid ID="dgCustomOptions" runat="server" AutoGenerateColumns="False" Font-Names="Verdana" Font-Size="Small"
                                             Width="100%" BorderColor="blue" BorderStyle="Solid" BorderWidth="0px" OnItemCreated="BindRowsDeleteMessage" Visible="true" style="border-collapse:separate"> <%--Edited for FF--%>
                                                <HeaderStyle CssClass="tableHeader" Height ="30"/>
                                                <Columns>
                                                    <asp:BoundColumn DataField="OptionID" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" HeaderText="オプションID" Visible="false"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="DisplayCaption" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" HeaderText="ディスプレイのタイトル"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="HelpText" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" HeaderText="テキストヘルプ"></asp:BoundColumn>
                                                   </Columns>
                                              </asp:DataGrid>
                                             </asp:TableCell>
                                       </asp:TableRow>
                                    </asp:Table>
                                   </td>               
                                </tr>
                                </table>
                               </ItemTemplate>
                             </asp:TemplateColumn>
                              <asp:TemplateColumn HeaderText="アクション" HeaderStyle-HorizontalAlign="center">
                                <HeaderStyle CssClass="tableHeader" />
                                <ItemTemplate>
                                    <table width="100%">
                                        <tr>
                                            <td>
                                                <asp:LinkButton runat="server" Text="編集" ID="btnEdit" CommandName="Edit"></asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton runat="server" Text="削除" ID="btnDelete" CommandName="Delete"></asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:Table runat="server" ID="tblNoCustomAttribute" Visible="false" Width="90%">
                        <asp:TableRow CssClass="lblError">
                            <asp:TableCell CssClass="lblError" HorizontalAlign="center" >
                               <font class="blackblodtext"> カスタムオプションはありません.</font>  
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                    <asp:Button ID="CustomTrigger" runat="server" style="display:none;"  />
                    <ajax:ModalPopupExtender ID="CustomPopUp" runat="server" TargetControlID="CustomTrigger"  PopupControlID="PopupCustomPanel"  DropShadow="false" Drag="true" BackgroundCssClass="modalBackground" CancelControlID="ClosePUp" BehaviorID="CustomTrigger"></ajax:ModalPopupExtender> 
                    <asp:Panel ID="PopupCustomPanel" Width="60%" runat="server" HorizontalAlign="Center"  CssClass="treeSelectedNode" Height="600px"  ScrollBars="Auto"> 
                        
                        <table align="center" border="0" cellpadding="3" cellspacing="0" width="95%">
                            <tr><td><br></td></tr>
                            <tr>
                                <td align="right">
                                    <asp:ImageButton ID="btnExcel" OnClick="ExportExcel" src="image/excel.gif"  runat="server" style="vertical-align:middle;" ToolTip="Excelにエクスポート"/>
                                    &nbsp;&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td align="center" class="lblError" id="ConfMessage" runat="server"></td>
                            </tr>
                            <tr>
                                <td>
                                    <br>
                                    <div style="width:auto;overflow:auto;"> <%--FB 1750--%>
                                        <asp:Table ID="ConfListTbl" runat="server" BorderColor="Blue" BorderStyle="Solid" BorderWidth="1" CellPadding="3" CellSpacing="1" Height="445px" HorizontalAlign="Center" >
                                        </asp:Table>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <br>
                        <asp:Button ID="BtnDeleteAll" runat="server" Text="カスタムオプションを削除します。" OnClick="DeleteCustomOptions" CssClass="altShort0BlueButtonFormat" />
                        <asp:Button ID="BtnEditCA" runat="server" Text="カスタムオプションを編集します。" OnClick="EditCustomOptions" CssClass="altShort0BlueButtonFormat" />
                        <input align="middle" type="button" runat="server" style="width:150px" id="ClosePUp" value=" 閉じる " class="altShort0BlueButtonFormat" />
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td align="center">
                   <asp:Button ID="btnCreateCusAtt" onfocus="this.blur()"  Text="新しいカスタムオプションを作成する" runat="server" CssClass="altLongBlueButtonFormat" OnClick="CreateNewCustomAtt" />&nbsp;&nbsp;
                   <input class="altShort0BlueButtonFormat" onclick="FnCancel()" type="button" value="キャンセル" name="btnCancel" />
                </td>
            </tr>
        </table>
    
</form>
</body>
</html>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
<%@ Page Language="C#" Inherits="ns_MyVRM.Template" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <!-- FB 2050 -->
<meta http-equiv="X-UA-Compatible" content="IE=7" /> <!-- FB 2050 -->

<!--Window Dressing-->
<!-- #INCLUDE FILE="inc/maintopNET.aspx" --> 
<script runat="server">

</script>
<script type="text/javascript" src="script/mousepos.js"></script>
<script type="text/javascript" src="script/managemcuorder.js"></script>

<script language="javascript">

	function ManageOrder ()
	{
		change_mcu_order_prompt('image/pen.gif', 'テンプレート注文を管理', document.frmManagebridge.Bridges.value, "テンプレート");
	}
	function CreateNewConference(confid)
    {
//        window.location.href = "aspToAspNet.asp?tp=ConferenceSetup.aspx&t=t&confid=" + confid; //Login Management
          window.location.href = "ConferenceSetup.aspx&t=t&confid=" + confid;
    }
	function showTemplateDetails(tid)
	{
//		popwin = window.open("dispatcher/conferencedispatcher.asp?cmd=GetTemplate&f=td&tid=" + tid,'templatedetails','status=yes,width=750,height=400,scrollbars=yes,resizable=yes')
        popwin = window.open("TemplateDetails.aspx?nt=1&f=td&tid=" + tid,'templatedetails','status=yes,width=750,height=400,scrollbars=yes,resizable=yes') //Login Management
		if (popwin)
			popwin.focus();
		else
			alert(EN_132);
	}
	function frmsubmit(save, order)
	{
	    document.getElementById("__EVENTTARGET").value="ManageOrder";
	    document.frmManagebridge.submit();
	}
</script>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
 
    <title>マイテンプレート</title>
    <script type="text/javascript" src="inc/functions.js"></script>

</head>
<body>
    <form id="frmManagebridge" runat="server" method="post" onsubmit="return true">
    <div>
      <input type="hidden" id="helpPage" value="73">

        <table width="100%">
            <tr>
                <td align="center">
                    <h3>
                        <asp:Label ID="lblHeader" runat="server" Text="マイテンプレート"></asp:Label>
                    </h3>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td width="20" >&nbsp;</td>
                            <td>
                                <SPAN class=subtitleblueblodtext>既存テンプレート</SPAN>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:DataGrid ID="dgTemplates" runat="server" AutoGenerateColumns="False" CellPadding="4" GridLines="None" AllowSorting="true" OnSortCommand="SortTemplates"
                         BorderColor="blue" BorderStyle="solid" BorderWidth="1" ShowFooter="true" OnItemCreated="BindRowsDeleteMessage"
                        OnDeleteCommand="DeleteTemplate" OnEditCommand="EditTemplate" OnCancelCommand="CreateConference" Width="90%" Visible="true" style="border-collapse:separate"> <%--Edited for FF--%>
                        <SelectedItemStyle  CssClass="tableBody"/>
                         <AlternatingItemStyle CssClass="tableBody" />
                         <ItemStyle CssClass="tableBody"  />
                        <HeaderStyle CssClass="tableHeader" Height="30px" />
                        <EditItemStyle CssClass="tableBody" />
                         <%--Window Dressing--%>
                        <FooterStyle CssClass="tableBody" />
                        <Columns>
                            <asp:BoundColumn DataField="ID" Visible="false" ><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>
                            <asp:BoundColumn DataField="name" ItemStyle-CssClass="tableBody" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="tableHeader" HeaderText="名前" SortExpression="1"></asp:BoundColumn> <%-- FB 2050 --%>
                            <asp:BoundColumn DataField="description" ItemStyle-CssClass="tableBody" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="tableHeader" HeaderText="説明"></asp:BoundColumn> <%-- FB 2050 --%>
                            <asp:BoundColumn DataField="administrator" ItemStyle-CssClass="tableBody" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="tableHeader" HeaderText="所有者" SortExpression="2"></asp:BoundColumn> <%-- FB 2050 --%>
                            <asp:BoundColumn DataField="public" ItemStyle-CssClass="tableBody"  HeaderStyle-CssClass="tableHeader" HeaderText="個人的な/公の" SortExpression="3"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="詳細を見る" ItemStyle-CssClass="tableBody"  HeaderStyle-CssClass="tableHeader">
                                <ItemTemplate>
                                    <asp:Button ID="btnViewDetails" Text="見る" runat="server" CssClass="altShortBlueButtonFormat" />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="アクション" ItemStyle-CssClass="tableBody"  ItemStyle-Width="25%" FooterStyle-HorizontalAlign="Right">
                                <HeaderStyle CssClass="tableHeader" />
                                <ItemTemplate>
                                    <table width="100%">
                                        <tr>
                                            <td>
                                                <%--Code Changed for FB 1428--%>
                                                <asp:LinkButton runat="server" ID="btnCreateConf" CommandName="Cancel">
                                                    <span id="Field1">会議を作成</span>
                                                </asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton runat="server" Text="編集" ID="btnEdit" CommandName="Edit"></asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton runat="server" Text="削除" ID="btnDelete" CommandName="Delete"></asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <span class="blackblodtext"> <b>総テンプレート: </span><asp:Label ID="lblTotalRecords" runat="server" Text=""></asp:Label> </b>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:Table runat="server" ID="tblNoTemplates" Visible="false" Width="90%">
                        <asp:TableRow CssClass="lblError">
                            <asp:TableCell CssClass="lblError">
                                いいえテンプレートが見つかりませんでした。
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>                    
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table width="90%">
                        <tr>
                            <td align="center">
                                <asp:Button ID="btnManageOrder" runat="server" OnClientClick="javascript:ManageOrder();return false;" Text="テンプレート注文を管理" CssClass="altLongBlueButtonFormat" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td width="20" >&nbsp;</td>
                            <td>
                                <SPAN class=subtitleblueblodtext>テンプレート検索</SPAN>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                           <%--Removed Class for Window Dressing                      --%>
                            <td class="blackblodtext">
                                すべての秘密鍵と公開テンプレートを検索する検索基準を入力してください。
                                <br />あなただけのプライベートテンプレートを編集することもできますが、新しいテンプレートを作成するための基礎として、パブリックまたはプライベートのテンプレートを使用することができます。    
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table width="90%"> 
                        <tr>
                            <td align="center">
                                <table width="100%">
                                    <tr>
                                        <td align="right" class="blackblodtext">テンプレート名</td>
                                        <td align="left">
                                            <asp:TextBox ID="txtSTemplateName" runat="server" CssClass="altText"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator4" ControlToValidate="txtSTemplateName" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~  &#34; は無効な記号です" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+^;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                        </td>
                                        <%if(!(Application["Client"].ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.MOJ))){%> <%--Added for FB 1425 MOJ--%>
                                        <td align="right" class="blackblodtext">含まれた参加者</td>
                                        <td align="left">
                                            <asp:TextBox ID="txtSParticipant" runat="server" CssClass="altText"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txtSParticipant" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > + % \ /  ? | ^ = ! ` [ ] { } # $ @  ~ は無効な記号です" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+^?|!`\[\]{}\=@#$%&~]*$"></asp:RegularExpressionValidator> <%--FB 1888--%>
                                        </td> 
                                        <%}%> <%--Added for FB 1425 MOJ--%>                                      
                                    </tr>
                                    <tr>
                                        <td align="right" class="blackblodtext">説明</td>
                                        <td align="left">
                                            <asp:TextBox ID="txtSDescription" TextMode="multiline" Rows="2" Width="200" runat="server" CssClass="altText"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" ControlToValidate="txtSDescription" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~  &#34; は無効な記号です" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+^;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                        </td>
                                        <td align="right">&nbsp;</td>
                                        <td align="left">&nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Button ID="btnSearch" OnClick="SearchTemplate" runat="server" CssClass="altLongBlueButtonFormat" Text="送信" />
                            </td>                            
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td width="20" >&nbsp;</td>
                            <td>
                                <!--<SPAN class=subtitleblueblodtext></SPAN>--><%--Commented for FB 2094--%>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
               <td align="center">
                    <table width="90%">
                        <tr>
                            <td align="right">
                               <asp:Button ID="btnCreate" OnClick="CreateNewTemplate" runat="server" CssClass="altLongBlueButtonFormat" Text="新規テンプレート作成" /><%--FB 2094--%> 
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <asp:TextBox ID="Bridges" runat="server" Width="0" Height="0" ForeColor="transparent" BackColor="transparent" BorderColor="transparent"></asp:TextBox>
    <asp:TextBox ID="txtSortBy" runat="server" Visible="false"></asp:TextBox>
</form>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
    <!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
</body>
</html>


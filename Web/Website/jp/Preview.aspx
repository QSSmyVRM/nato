﻿<%@ Page language="c#" AutoEventWireup="false" Inherits="myVRMAdmin.Web.en.Preview" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
	<head>
		<title>プレビュー</title>
		<meta name="Description" content="VRM (Videoconferencing Resource Management) is a revolutionary Web-based software application that manages any video conferencing environment."/>
		<meta name="Keywords" content="VRM, Videoconferencing Resource Management, video conferencing, video bridges, video endpoints"/>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
		<link title="Expedite base styles" href="css/border-table.css" type="text/css" rel="stylesheet"/>
		<link title="Expedite base styles" href="css/main-table.css" type="text/css" rel="stylesheet"/>
        <link href="App_Themes/CSS/main.css" rel="stylesheet" type="text/css" />
    <%--code added for Soft Edge button--%>		
		<link title="Expedite base styles" href="<%=Session["OrgCSSPath"]%>" type="text/css" rel="stylesheet"/>
	</head>
	<body >
		<form id="Form1" method="post" runat="server">
			<table width="100%">
				<tr>
					<td>
						<font face="Verdana" size="2pt" color="blue">
						 &nbsp;&nbsp;以下のサンプルのページがあります。<br/>&nbsp;&nbsp;操作は実行できません。
						</font>
					</td>
				</tr
				<tr style="height:540px;">
					<td width="100%">
						<iframe src="PreviewLogin.aspx" id="previewframe" scrolling="yes" width="100%" height="500" style="TEXT-DECORATION: none">
						</iframe>
					</td>
				</tr>
				<tr>
					<td>
						
						<table cellspacing="0" cellpadding="0" width="100%" border="0" id="Table8">
							<tbody>
								<tr>
									<td align="center">
										<input type="button" class="altBlueButtonFormat" name="btnLogin" value="プレビューログイン" onclick="fnOpen('PreviewLogin.aspx');"/>
										<input type="button" class="altBlueButtonFormat" name="btnLobby" value="ロビープレビュー" onclick="fnOpen('settingselect2Preview.aspx');"/>
										<input type="button" class="altBlueButtonFormat" name="btnvrm" value="[詳細設定]のプレビュー" onclick="fnOpen('AdvSettings.aspx');"/>
										<input type="button" class="altBlueButtonFormat" name="Close" value="閉じる" onclick="FnClose()"/>
									</td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>

<script language="javascript">

	function FnClose()
	{
		window.self.close();
	}

	function fnOpen()
	{
		args = fnOpen.arguments;
		var previewframe = document.getElementById("previewframe");
		previewframe.src = args[0];
	}
</script>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
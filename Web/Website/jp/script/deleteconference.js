// following var is global to used in page
var confRemoved = 0, delconfs = ";", issgl=0;

// for drop-down recur conf list
var allrecurids = "";


function clickrecur(cno, cid)
{
    click_recur(cno, cid, allrecurids, 2, (delconfs.indexOf(";" + cid + ";") != -1), document.getElementById("Reason" + cid).value, -1);
}


function ajSetXML(str)
{
   if (str.length > 2) {
        show_recur(str, allrecurids);
    		
	    if (str.split("<tr>").length > 1)
		    issgl = 0;
	}
}


function removeConf(theCheckbox, mode, DIVid)
{
	var m=parseInt(mode, 10), cid = theCheckbox.value;
    var cb, cb2=document.getElementsByName("SelectedConfIDs");

	if (theCheckbox.checked == true) {
		var isRemoveConf = confirm("をキャンセルしてもよろしいです" + ( (m == 3) ? "このインスタンス" : ( (m==2) ? "この定期的な会議のすべてのインスタンス" : "この会議") ) + "? \nすべての外部参加者は、電子メールでキャンセルを通知されます。\n")
		if (isRemoveConf == true) {
			theCheckbox.checked = true;
			confRemoved = confRemoved + 1;
			
			// add
			delconfs = (delconfs.indexOf(";" + cid + ";") == -1) ? delconfs + cid + ";" : delconfs;

			if (m == 2) {
				if (document.getElementById("instanceDIV" + DIVid).innerHTML != "") {
					for (var j=0; j<cb2.length; j++) {
						cb = cb2[j];
						if ( (cb.value).indexOf(cid+",") != -1 )
							cb.checked = true;
					} // for
				}
			}
		} else {
			theCheckbox.checked = false;
		}
	} else {
		confRemoved = confRemoved - 1;
		// remove
		delconfs = delconfs.replace((";" + cid + ";"), ";")

		if (m == 2) {
			if (document.getElementById("instanceDIV" + DIVid).innerHTML != "") {
				for (var j=0; j<cb2.length; j++) {
					cb = cb2[j];
					if ( (cb.value).indexOf(cid+",") != -1 )
						cb.checked = false;
				}
			}
		}
		
		if (m == 3) {
			for (var j=0; j<cb2.length; j++) {
				cb = cb2[j];
				if ( cb.value == cid.split(",")[0] )
					cb.checked = false;
			}
		}
	}
	
	if (confRemoved == 0) {
		document.getElementById("DeleteConferenceSubmit").disabled = true;
	} else {
		document.getElementById("DeleteConferenceSubmit").disabled = false;
	}

	if (confRemoved == cb2.length)
		document.getElementById("selectall").checked = true;
	else
		document.getElementById("selectall").checked = false;
}



function inputReason(cb, cid, mode, DIVid)
{
	var j, chkboxcb, m = parseInt(mode, 10);
	var cb2 = document.getElementsByName("SelectedConfIDs");
	

	if (cb.value != "") {
		for (var j=0; j<cb2.length; j++) {
			chkboxcb = cb2[j];
			if ( chkboxcb.value == cid ) {
				chkboxcb.checked = true;

			    // add
			    delconfs = (delconfs.indexOf(";" + cid + ";") == -1) ? (delconfs + cid + ";") : delconfs;
			} // if
		}
	}

	// recur
	if (m == 2) {
		if (document.getElementById("instanceDIV" + DIVid).innerHTML != "") {
		    
			//f = document.frmDeleteconference.elements;
			f= document.getElementsByTagName('*')

			for (i=0; i<f.length; i++) {
			    if (f[i].name) {
				    if ( (f[i].name).indexOf("Reason" + cid + ",") != -1 )
					    f[i].value = cb.value;
				}
			}
		}
	}
	if (m == 3) {
		document.getElementById("Reason" + cid.split(",")[0]).value = "";
	}
}


function selall(theAll)
{
    var cb=document.getElementsByName("SelectedConfIDs");
    
	if (theAll.checked == true) {
		var isRemoveConf = confirm("これらの会議（s）をキャンセルしてもよろしいです? \n すべての外部参加者は、電子メールでキャンセルを通知されます。\n")
		if (isRemoveConf == true) {		
			if (issgl) {
				cb.checked = true;
				var cid = cb.value;
				delconfs = (delconfs.indexOf(";" + cid + ";") == -1) ? delconfs + cid + ";" : delconfs
				confRemoved = 1;
			} else {
				for (var i = 0; i < cb.length; i++) {
					cb[i].checked = true;
					// add 
					var cid = cb[i].value;
					delconfs = (delconfs.indexOf(";" + cid + ";") == -1) ? delconfs + cid + ";" : delconfs
				}
			
				confRemoved = cb.length;
			}
		} else {
			theAll.checked = false;
		}
	} else {
		if (issgl)
			cb.checked = false;
		else {
			for (var i = 0; i < cb.length; i++)
				cb[i].checked = false;
		}
		confRemoved = 0;
		delconfs = ";"
	}
	
	if (confRemoved == 0) {
		document.getElementById("DeleteConferenceSubmit").disabled = true;
	} else {
		document.getElementById("DeleteConferenceSubmit").disabled = false;
	}
}



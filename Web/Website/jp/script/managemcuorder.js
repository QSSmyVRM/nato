function movePosition(to) {
	var list = document.getElementById("BridgeList");
	var index = list.selectedIndex;
	var total = list.options.length-1;

	if (index == -1) return false;
	if (to == +1 && index == total) return false;
	if (to == -1 && index == 0) return false;

	var items = new Array;
	var values = new Array;

	for (i = total; i >= 0; i--) {
		items[i] = list.options[i].text;
		values[i] = list.options[i].value;
	}

	for (i = total; i >= 0; i--) {
		if (index == i) {
			list.options[i + to] = new Option(items[i],values[i], 0, 1);
			list.options[i] = new Option(items[i + to], values[i+to]);
			i--;
		}
		else {
			list.options[i] = new Option(items[i], values[i]);
		   }
	}
	list.focus();
}


function change_mcu_order_prompt(promptpicture, prompttitle, bridges, title1) 
{
	promptbox = document.createElement('div'); 
	promptbox.setAttribute ('id' , 'prompt');
	document.getElementsByTagName('body')[0].appendChild(promptbox);
	promptbox = eval("document.getElementById('prompt').style");
    // FB 2050 Starts
	promptbox.position = 'absolute';
	promptbox.top = mousedownY - 5 + 'px';
	promptbox.left = mousedownX + 10 + 'px';
	promptbox.width = '250px';
	promptbox.height = '120px';
	promptbox.backgroundColor = 'white';
	promptbox.border = 'outset 1px #bbbbbb';
    // FB 2050 Ends
	bsary = bridges.split("||")

	m = "<table cellspacing='0' cellpadding='0' border='0' width='100%'><tr valign='middle'><td class='tableHeader'>" + prompttitle + "</td></tr></table>" 
//	Window Dressing
	m += "<table cellspacing='0' cellpadding='0' border='0' width='100%' class='tablebody'>";
	m += "  <tr>";
//	Window Dressing
	m += "    <td align='left' class='blackblodtext'><b>" + title1 + "<b></td>";
	m += "    <td></td>";
	m += "  </tr>"
	m += "  <tr>";
	m += "    <td>"
    m += "      <select multiple name='BridgeList' style='height:65px' id='BridgeList' class='SelectFormat'>" // FB 2050
    for (i = 0; i < bsary.length - 1; i++) {
		bary = bsary[i].split("``");
		m += "<option value='" + bary[0] + "'>" + bary[1] + "</option>"
    }
    m += "      </select>"
	m += "    </td>"
	m += "    <td valign='middle'>"
	m += "      <img border='0' src='image/cyan_up.gif' width='20' height='20' onClick='movePosition(-1)'>"
	m += "      <br>"
	m += "      <img border='0' src='image/cyan_down.gif' width='20' height='20' onClick='movePosition(+1)'>"
	m += "    </td>";
	m += "  </tr>"
	m += "  <tr>"
	m += "    <td colspan=2></td>";
	m += "  </tr>"
	m += "  <tr><td align='right' colspan=2>"
	//code added for Soft Edge button
	//m += "    <input type='button' class='altShortBlueButtonFormat' value='Submit' onMouseOver='this.style.border=\"1 outset #dddddd\"' onMouseOut='this.style.border=\"1 solid transparent\"' onClick='saveOrder(document.getElementById(\"BridgeList\"));'>"
	//m += "    <input type='button' class='altShortBlueButtonFormat' value='Cancel' onMouseOver='this.style.border=\"1 outset transparent\"' onMouseOut='this.style.border=\"1 solid transparent\"' onClick='cancelthis();'>"
    m += "    <input type='button' onfocus='this.blur()' class='altShortBlueButtonFormat' value='送信'  onClick='saveOrder(document.getElementById(\"BridgeList\"));'>"
	m += "    <input type='button' onfocus='this.blur()' class='altShortBlueButtonFormat' value='キャンセル'  onClick='cancelthis();'>"
	m += "  </td></tr>"
	m += "</table>" 
	
	document.getElementById('prompt').innerHTML = m;
} 


function saveOrder(cb) 
{ 
	var list = document.getElementById("BridgeList");
	var neworder = "";
	
	for (i = 0; i < list.options.length; i++) {
		neworder += list.options[i].value + ";"
	}
	document.frmManagebridge.Bridges.value = neworder;
	frmsubmit('SAVE', '');
} 


function cancelthis()
{
	document.getElementsByTagName("body")[0].removeChild(document.getElementById("prompt"));
}
<%@ Page Language="C#" Inherits="ns_ConferenceOrders.ConferenceOrders" Buffer="true" %>
<%@ Register Assembly="MetaBuilders.WebControls.ComboBox" Namespace="MetaBuilders.WebControls"
    TagPrefix="mbcbb" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <!-- FB 2050 -->
<meta http-equiv="X-UA-Compatible" content="IE=7" /> <!-- FB 2050 -->
<!--Window Dressing-->
<!-- #INCLUDE FILE="inc/maintopNET.aspx" --> 

<style type="text/css">
#Menu2 div
{
	width:123px; /* FB 2050 */
	height:35px;
}
</style>

<script runat="server">

    protected void Menu2_MenuItemClick(object sender, MenuEventArgs e) // FB 2050
    {
        int index = Int32.Parse(e.Item.Value);
        MultiView1.ActiveViewIndex = index;
        //WO Bug Fix
        //if (index.Equals(1))
        //{   
        //    txtWOEndTime.Text = "05:00 PM";
        //    txtWOStartTime.Text = "08:00 AM";
        //}
        if (index.Equals(2))
            Response.Redirect("CalendarWorkorder.aspx?t=" + txtType.Text);
    }
</script>
<script type="text/javascript">
  var servertoday = new Date();
</script>
<script type="text/javascript" src="inc/functions.js"></script>
<%--FB 1861--%>
  <%--<script type="text/javascript" src="script/cal.js"></script>--%>
<script type="text/javascript" src="script/cal-flat.js"></script>
<script type="text/javascript" src="lang/calendar-en.js"></script>
<script type="text/javascript" src="script/calendar-setup.js"></script>
<script type="text/javascript" src="script/calendar-flat-setup.js"></script>
<link rel="stylesheet" type="text/css" media="all" href="css/calendar-blue.css" /> <%--FB 1861--%> <%--FB 1982--%> 
<script language="VBScript" src="script/outlook.vbs"></script>
<script type="text/javascript" src="script/mytreeNET.js"></script>
<script type="text/javascript" src="script/Workorder.js"></script>
<script type="text/javascript" src="script/roomsearch.js"></script>

<script language="javascript">
function ViewDetails(id, confid)
{
    ViewWorkorderDetails(id, confid, document.getElementById("txtType").value);
}

function changeRoomSelection(objValue)
{
    if(objValue == "2")
        document.getElementById("trRooms").style.display = "";
    else
        document.getElementById("trRooms").style.display = "none";
}

function ChangeView(t,view)
{
    document.frmInventoryManagement.action = "ConferenceOrders.aspx?t=" + t + "&view=" + view;
    document.frmInventoryManagement.submit();
    return true;
}

function listOrders(id, view)
{
    document.frmInventoryManagement.action = "ConferenceOrders.aspx?cmd=2&id=" + id + "&view=" + view;
    document.frmInventoryManagement.submit();
    return true;
}

function EditConferenceOrder(id)
{
    document.frmInventoryManagement.action = "EditConferenceOrder.aspx?id=" + id + "&t=" + document.getElementById("<%=txtType.ClientID %>").value;
    document.frmInventoryManagement.submit();
    return true;
}

function viewconf(cid)
{
	//url = "dispatcher/conferencedispatcher.asp?cmd=ViewConference&cid=" + cid;
	url = "ManageConference.aspx?confid=" + cid + "&t=hf";
	confdetail = window.open(url, "viewconference", "width=1,height=1,resizable=yes,scrollbars=yes,status=no");
	confdetail.focus();
}

function hideOrders()
{
    var temp = document.getElementById("detailsRow");
    var temp1 = document.getElementById("hideItem");
    if (temp1.src.indexOf("plus") >0)
    {
        temp1.src = "image/loc/nolines_minus.gif";
        temp.style.display = ""; //Edite for FF
    }
    else 
    {
        temp1.src = "image/loc/nolines_plus.gif";
        //FB 2181
        document.frmInventoryManagement.action = "ConferenceOrders.aspx?t=1&view=1";
        document.frmInventoryManagement.submit();
        return true;
    }
}
function convertControls(id)
{
    var t = document.getElementById("Item" + id);
    t.style.display="none";
    t = document.getElementById("hdnItemList" + id);
    t.style.display="";
}
function getYourOwnEmailList (i)
{
//	url = "dispatcher/conferencedispatcher.asp?frm=approverNET&frmname=Setup&no=" + i + "&cmd=GetEmailList&emailListPage=1&wintype=pop";//Login Management
	url = "emaillist2main.aspx?t=e&frm=approverNET&fn=Setup&n=" + i; //Login Management
	if (!window.winrtc) {	// has not yet been defined
		winrtc = window.open(url, "", "width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
		winrtc.focus();
	} else // has been defined
	    if (!winrtc.closed) {     // still open
	    	winrtc.close();
	        winrtc = window.open(url, "", "width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
			winrtc.focus();
		} else {
	        winrtc = window.open(url, "", "width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
	        winrtc.focus();
		}
}

//WO Bug Fixing
function fnValidate()
{
    var stdate = '';
    stdate = GetDefaultDate(document.getElementById("txtWOStartDate").value,'<%=format%>');
    
    var enddate = '';
    enddate = GetDefaultDate(document.getElementById("txtWOEndDate").value,'<%=format%>');
    
    if(stdate != "" && enddate != "")
    {
        if (Date.parse(stdate + " " + document.getElementById("txtWOStartTime_Text").value) > Date.parse(enddate + " " + document.getElementById("txtWOEndTime_Text").value) )
        {           
            var errlabel = document.getElementById("errLabel");           
            if(errlabel)
            {
                errlabel.innerText = "Por favor, introduzca Fecha v�lida";
                errlabel.style.display = "";
            }                      
            return false;
        }
    }
    
    return true;
}
</script>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Work Orders Management</title>
</head>
<body>
    <form id="frmInventoryManagement" runat="server" method="post" onsubmit="return true">
     <input type="hidden" id="helpPage" value="40">
     <input type="hidden" id="Hidden1" value="" runat="server" />
     <input name="locstrname" type="hidden" id="locstrname" runat="server"  /> <!--Added room search-->
     <input name="selectedloc" type="hidden" id="selectedloc" runat="server"  /> <!--Added room search-->

    <div>
        <table width="100%" cellspacing="0" cellpadding="0">
            <tr>
                <td align="center"  >
                    <h3>
                        <asp:Label ID="lblType" runat="server"></asp:Label></h3>
                        <asp:Label ID="errLabel" runat="server" CssClass="lblError" style="display:none;"></asp:Label>   <%--WO Bug Fix--%>
                        <div id="dataLoadingDIV" style="z-index:1"></div>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Menu
                        id="Menu2"
                        Orientation="Horizontal"
                        StaticMenuItemStyle-CssClass="tab"
                        StaticSelectedStyle-CssClass="selectedTab"
                        CssClass="tabs"
                        ItemWrap="true" 
                        OnMenuItemClick="Menu2_MenuItemClick"
                        Runat="server">
                        <Items>
                        <asp:MenuItem Text="<div align='center' style='width:123'><b>Detalles de las</b><br>�rdenes de trabajo</b></div>" Value="0" Selected="true" /> <%--Edited for FF--%>
                        <asp:MenuItem Text="<div align='center' style='width:123'><b>Buscar �rdenes</b><br>de trabajo</b></div>" Value="1" />
                        <asp:MenuItem Text="<div align='center' style='width:123'><b>Calendario de</b><br>�rdenes de Trabajo</b></div>" Value="2" />
                        </Items>    
                    </asp:Menu> <%-- FB 2050 --%>
                        <div class="tabContents" style="width:98%" >
                            <asp:MultiView
                                id="MultiView1"
                                ActiveViewIndex="0"
                                Runat="server">
                                <asp:View ID="WorkorderView" runat="server">
                                    <table width="100%">
                                        <tr height="30"><td></td></tr>
                                        <tr>
                                            <%--Window Dressing--%>
                                            <td class="blackblodtext"><b>Filtro de Orden de trabajo:</b> 
                                               <asp:DropDownList runat="server" ID="lstViews" Enabled=true AutoPostBack="true" OnSelectedIndexChanged="lstViews_SelectedIndexChanged" onchange="javascript:DataLoading(1);" CssClass="altText">
                                                    <asp:ListItem Text="Por favor, seleccione ..." Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="Ver �rdenes de trabajo de conferencias" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="ver mis �rdenes de trabajo pendientes" Value="3"></asp:ListItem>
                                                    <asp:ListItem Text="Ver �rdenes de trabajo incompletas" Value="6"></asp:ListItem>
                                                    <asp:ListItem Text="Ver todas las �rdenes de trabajo" Value="2"></asp:ListItem>  
                                                    <asp:ListItem Text="Ver las �rdenes de trabajo de hoy" Value="4"></asp:ListItem>                      
                                               </asp:DropDownList> 
                                                <asp:TextBox ID="txtType" runat="server" BackColor="transparent" BorderColor="transparent" BorderWidth="0" BorderStyle="None" ForeColor="transparent" Width="0px"></asp:TextBox>
                                           </td>
                                        </tr>
                                        <tr>
                                            <td align="center">
                                                <asp:Table ID="InvMainTable" runat="server" BorderColor="Blue" BorderStyle="Solid" Visible="false"
                                                    BorderWidth="1px" CellPadding="0" CellSpacing="0" Width="90%" EnableViewState="False">
                                                    <asp:TableRow runat="server" CssClass="tableHeader" Height="30px" HorizontalAlign="Center" VerticalAlign="Middle">
                                                        <asp:TableCell runat="server" CssClass="tableHeader"></asp:TableCell>
                                                        <asp:TableCell runat="server" CssClass="tableHeader">ID �nica</asp:TableCell>
                                                        <asp:TableCell runat="server" CssClass="tableHeader">Nombre</asp:TableCell>
                                                        <asp:TableCell runat="server" CssClass="tableHeader">Fecha/Hora</asp:TableCell>
                                                        <asp:TableCell runat="server" CssClass="tableHeader">Detalles de Conferencia</asp:TableCell>
                                                        <asp:TableCell runat="server" CssClass="tableHeader">Editar</asp:TableCell>
                                                    </asp:TableRow>
                                                    <asp:TableRow runat="server">
                                                    <asp:TableCell runat="server" ></asp:TableCell>
                                                    </asp:TableRow>
                                                </asp:Table>
                                                <asp:DataGrid ID="WorkOrderMainGrid" runat="server" AutoGenerateColumns="False" CellPadding="4" GridLines="None"
                                                    OnItemCreated="BindRowsDeleteMessage" BorderColor="blue" BorderStyle="solid" BorderWidth="1" ShowFooter="true"
                                                    OnDeleteCommand="WorkOrderMainGrid_Delete" OnEditCommand="WorkOrderMainGrid_Edit" Width="90%" Visible="true" FooterStyle-BorderStyle="None" style="border-collapse:separate">
                                                      <%--Window Dressing - start--%>
                                                    <SelectedItemStyle CssClass="tableBody" Font-Bold="True" />
                                                    <ItemStyle CssClass="tableBody" Height="15" />
                                                    <AlternatingItemStyle CssClass="tableBody" Height="15" />
                                                    <HeaderStyle CssClass="tableHeader" Height ="30"/>
                                                     <FooterStyle HorizontalAlign="right" BorderStyle="solid" BorderColor="blue" CssClass="tableBody" BorderWidth="0" Font-Bold="True" />
                                                    <%--Window Dressing - end--%>
                                                    <Columns>
                                                        <asp:BoundColumn DataField="ID" Visible="False" ><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="ConfID" Visible="False" ><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="Name" HeaderText="Nombre" ItemStyle-CssClass="tableBody" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"> <HeaderStyle CssClass="tableHeader" /></asp:BoundColumn> <%-- FB 2050 --%>
                                                        <asp:TemplateColumn HeaderText="Fecha/Hora de Terminaci�n" ItemStyle-CssClass="tableBody">
                                                            <HeaderStyle CssClass="tableHeader" />
                                                            <ItemTemplate>
                                                                <asp:Label ID="Label1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CompletedByDate") + " " +DataBinder.Eval(Container, "DataItem.CompletedByTime") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:BoundColumn DataField="AssignedToId" Visible="false"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="AssignedToName" ItemStyle-CssClass="tableBody" HeaderText="Persona encargada" Visible="true" HeaderStyle-CssClass="tableHeader"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="RoomName" ItemStyle-CssClass="tableBody" HeaderText="Sal�n"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="Status" ItemStyle-CssClass="tableBody" HeaderText="Estado"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="TotalCost" HeaderText="Coste<br>total (D�lares Am)" ItemStyle-CssClass="tableBody"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>
                                                        <asp:TemplateColumn HeaderText="Comentarios" ItemStyle-CssClass="tableBody">
                                                            <HeaderStyle CssClass="tableHeader" />
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblComments" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Comments") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn  HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody" ItemStyle-HorizontalAlign="Right">
                                                            <ItemTemplate>
                                                                <a href="#" onclick="ViewDetails('<%#DataBinder.Eval(Container, "DataItem.ID") %>', '<%#DataBinder.Eval(Container, "DataItem.ConfID") %>')">Ver</a>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="Acciones" ItemStyle-HorizontalAlign="Center" >
                                                            <HeaderStyle CssClass="tableHeader" />
                                                            <ItemTemplate>
                                                                <asp:LinkButton runat="server" Text="Editar" ID="btnEditWorkOrder" OnClientClick="DataLoading(1)" CommandName="Edit" Visible='<%# DataBinder.Eval(Container, "DataItem.Status") == "Pending" %>'></asp:LinkButton>
                                                                <asp:LinkButton runat="server" Text="eliminar" ID="btnDelete" Visible='<%# DataBinder.Eval(Container, "DataItem.Status") == "Pending" %>' CommandName="Delete"></asp:LinkButton> <%--WO Bug Fix--%>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                            <%--Window Dressing--%>
                                                                <asp:Label runat="server" CssClass="blackblodtext" Text="�rdenes&nbsp;totales:&nbsp;" ID="lblTotalRecords"></asp:Label>
                                                            </FooterTemplate>
                                                        </asp:TemplateColumn>
                                                    </Columns>
                                                </asp:DataGrid>
                                                <asp:Table runat="server" ID="tblNoWorkOrders" Visible="false" Width="90%">
                                                    <asp:TableRow CssClass="lblError">
                                                        <asp:TableCell CssClass="lblError" HorizontalAlign="center">
                                                            No se encontraron �rdenes de trabajo
                                                        </asp:TableCell>
                                                    </asp:TableRow>
                                                </asp:Table>                    
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center">
                                                <asp:Table ID="tblPage" Visible="false" runat="server">
                                                    <asp:TableRow ID="TableRow1" runat="server">
                                                            <%--Window Dressing--%>
                                                        <asp:TableCell ID="TableCell1" Font-Bold="True" Font-Names="Verdana" Font-Size="Small" ForeColor="Blue" runat="server"><label class="blackblodtext">p�ginas:</label> </asp:TableCell>
                                                        <asp:TableCell ID="TableCell2" runat="server"></asp:TableCell>
                                                    </asp:TableRow>
                                                </asp:Table>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center"  >
                                                <table border="0" cellpadding="0" cellspacing="0" width="90%">
                                                    <tr>
                                                        <td align="right"   rowspan="3" style="height: 16px">
                                                            <%--Window Dressing--%>
                                                            <asp:Label ID="lblCount" runat="server" Font-Bold="True" 
                                                                ForeColor="Blue"></asp:Label></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center"   style="height: 188px">
                                                <asp:DataGrid ID="MainGrid" runat="server" AutoGenerateColumns="False" CellPadding="4"
                                                ForeColor="Yellow" Width="100%" AllowSorting="True" BackColor="LemonChiffon" BorderColor="#C04000" BorderStyle="Solid" BorderWidth="1px" Font-Names="Verdana" Font-Size="Small" EnableViewState="False">
                                                     <%--Window Dressing--%>
                                                    <FooterStyle CssClass="tableBody" />
                                                    <Columns>
                                                        <asp:BoundColumn DataField="Name" HeaderText="Nombre" />
                                                        <asp:BoundColumn DataField="AssignedToName" HeaderText="Asignado a" />
                                                        <asp:BoundColumn DataField="CompletedByDate" HeaderText="Fecha de finalizaci�n" />
                                                        <asp:BoundColumn DataField="CompletedByTime" HeaderText="Hora de finalizaci�n" />
                                                        <asp:BoundColumn DataField="Status" HeaderText="Estado" />
                                                        <asp:BoundColumn DataField="RoomName" HeaderText="Sal�n" />
                                                        <asp:BoundColumn DataField="Comments" HeaderText="Comentarios" />
                                                    </Columns>
                                                    <%--Window Dressing - Start--%>
                                                    <SelectedItemStyle CssClass="tableBody" Font-Bold="True"/>
                                                    <ItemStyle CssClass="tableBody" Height="15" />
                                                    <AlternatingItemStyle CssClass="tableBody" Height="15" />
                                                    <HeaderStyle CssClass="tableHeader" Height ="30"/>
                                                    <PagerStyle CssClass="tableBody" HorizontalAlign="Center" />
                                                   <%--Window Dressing - End--%>
                                                </asp:DataGrid>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center">
                                                <asp:Button runat="server" Visible="false" ID="btnCreatePhantomWorkorder" OnClick="CreatePhantomWorkorder" Text="Crear Orden de trabajo nueva" CssClass="altLongBlueButtonFormat" /> <%--FB 2181--%>
                                            </td>
                                        </tr>
                                    </table>
                                    
    <script language="javascript">
     if (document.getElementById("txtType").value == "1")
     {
        document.getElementById("helpPage").value = "3";
        //document.getElementById("btnCreatePhantomWorkorder").style.display = ""; FB 2181
     }
     if (document.getElementById("txtType").value == "2")
     {
        document.getElementById("helpPage").value = "5";
        //document.getElementById("btnCreatePhantomWorkorder").style.display = "none"; FB 2181
     }
     if (document.getElementById("txtType").value == "3")
     {
        document.getElementById("helpPage").value = "40";
        //document.getElementById("btnCreatePhantomWorkorder").style.display = "none"; FB 2181
     }
     
     changeCurrencyFormat("WorkOrderMainGrid",'<%=currencyFormat %>'); //FB 1830
        
    </script>
                                    
                                </asp:View>        
                                <asp:View ID="SearchView" runat="server" >
                                    <table width="100%" border="0">
                                        <tr>
                                            <td>
                                                <table cellspacing="5" cellpadding="2" width="100%">
<%--                                                    <tr>
                                                        <td colspan="2" align="right">
                                                            <asp:Button ID="btnViewWOCalendar" runat="server" OnClick="OpenWOCalendar" ValidationGroup="Calendar" Text="View Workorder Calendar" CssClass="altLongBlueButtonFormat" />
                                                        </td>
                                                    </tr>
--%>                                                    <tr>
                                                            <%--Window Dressing--%>
                                                        <td align="right" style="font-weight:bold" class="blackblodtext">Nombre</td>
                                                        <td align="left">
                                                            <asp:TextBox ID="txtsWorkorderName" runat="server" CssClass="altText"></asp:TextBox>
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator4" ControlToValidate="txtsWorkorderName" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < and > are invalid characters." ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator> <%--FB 2321--%>
                                                        </td>
                                                        <td width="50%" rowspan="7" valign="top">
                                                            <table width="100%">
                                                                <tr>
                                                            <%--Window Dressing--%>
                                                                    <td align="left" valign="top" class="blackblodtext">
                                                                        <b>Ubicaciones</b>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" class="blackblodtext"> <%--Window Dressing--%>
                                                                        <asp:RadioButtonList ID="rdRoomOption" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" >
<%--                                                                            <asp:ListItem Selected="True" Text="None" Value="0"></asp:ListItem>
--%>                                                                         <asp:ListItem Selected="true" Value="1"><font class="blackblodtext">Cualquier</font> </asp:ListItem> <%--Window Dressing--%>
                                                                            <asp:ListItem Selected="False" Value="2"><font class="blackblodtext">Seleccionado</font> </asp:ListItem> <%--Window Dressing--%>
                                                                        </asp:RadioButtonList>
                                                                    </td>
                                                                </tr>
                                                                <tr id="trRooms" style="display:none">
                                                                  <td>
                                                                    <table>
                                                                        <tr>
                                                                            <td align="right" valign="top" style="width:30%">
                                                                            <input name="opnRooms" type="button" id="opnRooms" onclick="javascript:OpenRoomSearch('frmInventoryManagement');" value="A�adir Sal�n" class="altShortBlueButtonFormat" />
                                                                        
                                                                        <input name="addRooms" type="button" id="addRooms" onclick="javascript:AddRooms();" style="display:none;" /><br />
                                                                        <span class="blackblodtext"> <font size="1">Haga 'doble-clic' sobre el sal�n para eliminarlo de la lista.</font></span>
                                                                            </td>
                                                                            <td align="left" style="width:70%">
                                                                            <select size="4" wrap="false" name="RoomList" id="RoomList" class="treeSelectedNode" onDblClick="javascript:Delroms(this.value)"  style="height:350px;width:70%;" runat="server"></select>
                                                                            </td>
                                                                            </tr>
                                                                        </table>
                                                                        <asp:Panel ID="pnlLevelView"  runat="server" style="display:none;" Height="300px" Width="100%" ScrollBars="Auto" BorderColor="Blue" BorderStyle="Solid" BorderWidth="1px" HorizontalAlign="Left">
                                                                            <asp:TreeView ID="treeRoomSelection" runat="server" BorderColor="White" Height="90%" ShowCheckBoxes="All" onclick="javascript:getRooms(event)" OnInit="GetLocations"
                                                                                ShowLines="True" Width="95%" >
                                                                                <NodeStyle BackColor="#F7F6F3" Font-Size="Smaller" />
                                                                                <RootNodeStyle BackColor="Transparent" BorderStyle="None" Font-Size="Smaller" ForeColor="Blue" />
                                                                                <SelectedNodeStyle BackColor="#E0E0E0" BorderColor="Gray" BorderStyle="Solid" BorderWidth="1px" />
                                                                                <ParentNodeStyle BorderStyle="None" ForeColor="#404040" />
                                                                                <LeafNodeStyle Font-Size="Smaller" />
                                                                            </asp:TreeView>
                                                                        </asp:Panel>
                                                                        <asp:Panel ID="pnlListView"  style="display:none;" runat="server" BorderColor="Blue" BorderStyle="Solid"
                                                                            BorderWidth="1px" Height="300px" ScrollBars="Auto" Visible="False" Width="100%" HorizontalAlign="Left" Direction="LeftToRight" Font-Bold="True" Font-Names="Verdana" Font-Size="Small" ForeColor="Green">
                                                                            <asp:CheckBoxList ID="lstRoomSelection" runat="server" Height="95%" Width="95%" Font-Size="Smaller" ForeColor="ForestGreen" Font-Names="Verdana" RepeatLayout="Flow">
                                                                            </asp:CheckBoxList>
                                                                        </asp:Panel>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr><tr>
                                                        <%--Window Dressing--%>
                                                        <td align="right" style="font-weight:bold" class="blackblodtext">Fecha/Hora de Inicio
                                                        <td align="left" nowrap="nowrap" class="blackblodtext">
                                                                 <asp:TextBox ID="txtWOStartDate" runat="server" CssClass="altText"></asp:TextBox>
                                                                 <%-- Code changed by Offshore for FB Issue 1073 -- Start
                                                                 <img src="image/calendar.gif" border="0" width="20" height="20" id="cal_triggerd" style="cursor: pointer;" title="Date selector" onclick="return showCalendar('<%=txtWOStartDate.ClientID %>', 'cal_triggerd', 0, '%m/%d/%Y');" />--%>
                                                                 <img src="image/calendar.gif" border="0" width="20" height="20" id="cal_triggerd" style="cursor: pointer;" title="Selector de Fecha" onclick="return showCalendar('<%=txtWOStartDate.ClientID %>', 'cal_triggerd', 0, '<%=format%>');" />
                                                                 <%-- Code changed by Offshore for FB Issue 1073 -- End--%>
                                                                 @<mbcbb:combobox id="txtWOStartTime" runat="server" CssClass="altSelectFormat" Rows="10" CausesValidation="True" width="100px"> <%--Edited for FF--%> <%--FB 2181 FB 2050--%>
                                                                    <asp:ListItem Value="01:00 AM"></asp:ListItem>
                                                                    <asp:ListItem Value="02:00 AM"></asp:ListItem>
                                                                    <asp:ListItem Value="03:00 AM"></asp:ListItem>
                                                                    <asp:ListItem Value="04:00 AM"></asp:ListItem>
                                                                    <asp:ListItem Value="05:00 AM"></asp:ListItem>
                                                                    <asp:ListItem Value="06:00 AM"></asp:ListItem>
                                                                    <asp:ListItem Value="07:00 AM"></asp:ListItem>
                                                                    <asp:ListItem Value="08:00 AM"></asp:ListItem>
                                                                    <asp:ListItem Value="09:00 AM"></asp:ListItem>
                                                                    <asp:ListItem Value="10:00 AM"></asp:ListItem>
                                                                    <asp:ListItem Value="11:00 AM"></asp:ListItem>
                                                                    <asp:ListItem Value="12:00 PM"></asp:ListItem>
                                                                    <asp:ListItem Value="01:00 PM"></asp:ListItem>
                                                                    <asp:ListItem Value="02:00 PM"></asp:ListItem>
                                                                    <asp:ListItem Value="03:00 PM"></asp:ListItem>
                                                                    <asp:ListItem Value="04:00 PM"></asp:ListItem>
                                                                    <asp:ListItem Value="05:00 PM"></asp:ListItem>
                                                                    <asp:ListItem Value="06:00 PM"></asp:ListItem>
                                                                    <asp:ListItem Value="07:00 PM"></asp:ListItem>
                                                                    <asp:ListItem Value="08:00 PM"></asp:ListItem>
                                                                    <asp:ListItem Value="09:00 PM"></asp:ListItem>
                                                                    <asp:ListItem Value="10:00 PM"></asp:ListItem>
                                                                    <asp:ListItem Value="11:00 PM"></asp:ListItem>
                                                                    <asp:ListItem Value="12:00 AM"></asp:ListItem>
                                                            </mbcbb:combobox>
                                                            
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <%--Window Dressing--%>
                                                        <td align="right" style="font-weight:bold" class="blackblodtext">Hora/Fecha Final</td>
                                                        <td align="left" nowrap="nowrap" class="blackblodtext"> <%-- FB 2050 --%>
                                                                 <asp:TextBox ID="txtWOEndDate" runat="server" CssClass="altText"></asp:TextBox>
                                                                 <%--  <!-- Code changed by Offshore for FB Issue 1073 -- Start
                                                                 <img src="image/calendar.gif" border="0" width="20" height="20" id="Img1" style="cursor: pointer;" title="Date selector" onclick="return showCalendar('<%=txtWOEndDate.ClientID %>', 'cal_triggerd', 0, '%m/%d/%Y');" />-->--%>
                                                                 <img src="image/calendar.gif" border="0" width="20" height="20" id="cal_triggerd1" style="cursor: pointer;" title="Selector de Fecha" onclick="return showCalendar('<%=txtWOEndDate.ClientID %>', 'cal_triggerd1', 0, '<%=format%>');" />
                                                                 <%-- Code changed by Offshore for FB Issue 1073 -- End--%>
                                                                 @<mbcbb:combobox id="txtWOEndTime" runat="server"  CssClass="altSelectFormat" Rows="10" CausesValidation="True" width="100px"> <%--Edited for FF--%> <%--FB 2181 FB 2050 --%>
                                                                    <%--<asp:ListItem Value="-1">Please Select</asp:ListItem>--%> 
                                                                    <asp:ListItem Value="01:00 AM"></asp:ListItem>
                                                                    <asp:ListItem Value="02:00 AM"></asp:ListItem>
                                                                    <asp:ListItem Value="03:00 AM"></asp:ListItem>
                                                                    <asp:ListItem Value="04:00 AM"></asp:ListItem>
                                                                    <asp:ListItem Value="05:00 AM"></asp:ListItem>
                                                                    <asp:ListItem Value="06:00 AM"></asp:ListItem>
                                                                    <asp:ListItem Value="07:00 AM"></asp:ListItem>
                                                                    <asp:ListItem Value="08:00 AM"></asp:ListItem>
                                                                    <asp:ListItem Value="09:00 AM"></asp:ListItem>
                                                                    <asp:ListItem Value="10:00 AM"></asp:ListItem>
                                                                    <asp:ListItem Value="11:00 AM"></asp:ListItem>
                                                                    <asp:ListItem Value="12:00 PM"></asp:ListItem>
                                                                    <asp:ListItem Value="01:00 PM"></asp:ListItem>
                                                                    <asp:ListItem Value="02:00 PM"></asp:ListItem>
                                                                    <asp:ListItem Value="03:00 PM"></asp:ListItem>
                                                                    <asp:ListItem Value="04:00 PM"></asp:ListItem>
                                                                    <asp:ListItem Value="05:00 PM"></asp:ListItem>
                                                                    <asp:ListItem Value="06:00 PM"></asp:ListItem>
                                                                    <asp:ListItem Value="07:00 PM"></asp:ListItem>
                                                                    <asp:ListItem Value="08:00 PM"></asp:ListItem>
                                                                    <asp:ListItem Value="09:00 PM"></asp:ListItem>
                                                                    <asp:ListItem Value="10:00 PM"></asp:ListItem>
                                                                    <asp:ListItem Value="11:00 PM"></asp:ListItem>
                                                                    <asp:ListItem Value="12:00 AM"></asp:ListItem>
                                                            </mbcbb:combobox>
                                                                                                
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <%--Window Dressing--%>
                                                        <td align="right" style="font-weight:bold" class="blackblodtext">Estado</td>
                                                        <td align="left">
                                                            <asp:DropDownList ID="lstStatus" runat="server" CssClass="altSelectFormat">
                                                                <asp:ListItem Text="Cualquier" Value="2"></asp:ListItem>
                                                                <asp:ListItem Text="Pendiente" Value="0"></asp:ListItem>
                                                                <asp:ListItem Text="Completado" Value="1"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <%--Window Dressing--%>
                                                        <td align="right" style="font-weight:bold; width:20%" class="blackblodtext">Persona encargada</td>
                                                        <td align="left">
                                                            <asp:TextBox ID="txtApprover1" runat="server" CssClass="altText" Enabled="false"></asp:TextBox>
                                                            &nbsp;<img id="ImageButton1" onclick="javascript:getYourOwnEmailList(0)" src="image/edit.gif" />
                                                            <asp:TextBox ID="hdnApprover1" runat="server" BackColor="Transparent" BorderColor="White"
                                                                BorderStyle="None" Width="0px" ForeColor="Transparent"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2" align="right">
                                                            <asp:Button runat="Server" ID="btnCancelSearch" Text="Cancelar" CssClass="altShortBlueButtonFormat" ValidationGroup="Cancel" OnClick="CancelSearch" />
                                                            <asp:Button runat="Server" ID="btnSubmitSearch" Text="Entregar" CssClass="altShortBlueButtonFormat" OnClick="SearchWorkorders" OnClientClick="javascript:return fnValidate();" /> <%--WO Bug Fixing--%>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>                                    
                                    </table>
                                </asp:View>
                                <asp:View ID="viewCalendar" runat="server"></asp:View>        
                            </asp:MultiView>
                        </div>
                </td>
            </tr>
        </table>
    </div>
<img src="keepalive.asp" name="myPic" width="1px" height="1px">
                    <input type="hidden" id="selectedloc" value="">
</form>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
    <!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
</body>
</html>


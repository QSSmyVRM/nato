<%@ Page Language="C#" Inherits="ns_MyVRM.UserHistoryReport" %>

<%@ Register Assembly="MetaBuilders.WebControls.ComboBox" Namespace="MetaBuilders.WebControls"
    TagPrefix="mbcbb" %>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <!-- FB 2050 -->
<meta http-equiv="X-UA-Compatible" content="IE=7" /> <!-- FB 2050 -->
<!-- #INCLUDE FILE="inc/maintopNET.aspx" --> 

<script type="text/javascript">
  var servertoday = new Date();
</script>
<script type="text/javascript" language="javascript" src='script/lib.js'></script>
<script type="text/javascript" src="script/calview.js"></script>
<script type="text/javascript" src="inc/functions.js"></script>
<script type="text/javascript" language="javascript"> 
 var rtn;
    function fnSubmit()
        {  
                       
            if(document.form1.txtFromDate.value != "" && document.form1.txtToDate.value == "")
                {
                    alert("Por favor, introduzca la fecha HASTA.");
                    document.form1.txtToDate.focus();
                    return false;
                }
                else if(document.form1.txtToDate.value != "" && document.form1.txtFromDate.value == "")
                {
                    alert("Por favor, introduzca la fecha DESDE.");
                    document.form1.txtFromDate.focus();
                    return false;
                }
                textValue = document.form1.txtFromDate.value;
                index = textValue.length;
                startDate = textValue.substring(0,index);
                textValue = document.form1.txtToDate.value;
                index = textValue.length;
                endDate = textValue.substring(0,index); 
                             
                var compareVal = fnCompareDate(startDate,endDate)
                if(compareVal == "L")
                {
                    alert("La Fecha DESDE deber�a ser anterior a la Fecha HASTA");
                    return false;
                }  
                
                return true;      
        }
        function ChangeStartDate(frm)
        {
            var confenddate = '';
            confenddate = GetDefaultDate(document.getElementById("txtToDate").value,'<%=format%>');
            
            var confstdate = '';
            confstdate = GetDefaultDate(document.getElementById("txtFromDate").value,'<%=format%>');
            
            if(document.getElementById("txtToDate").value != "")
                reqtodate.style.display = 'none';
                
            if (Date.parse(confstdate) > Date.parse(confenddate))
            {
                if (frm == "0") 
                {
                    alert("La Fecha DESDE deber�a ser anterior a la Fecha HASTA");
                    rtn = false;
                    return false;
                }
            }
            else
            {
                rtn = true;
                return true;
            }
        }
        function ChangeEndDate(frm)
        {
            var confenddate = '';
            if(document.getElementById("txtFromDate").value != "")
                confenddate = GetDefaultDate(document.getElementById("txtToDate").value,'<%=format%>');
                
            var confstdate = '';
            if(document.getElementById("txtFromDate").value != "")
                confstdate = GetDefaultDate(document.getElementById("txtFromDate").value,'<%=format%>');
                
            if(document.getElementById("txtFromDate").value != "")
                reqfromdate.style.display = 'none';
            
            if (Date.parse(confenddate) < Date.parse(confstdate))
            {
                if (frm == "0") 
                {
                    alert("La Fecha DESDE deber�a ser anterior a la Fecha HASTA");
                    rtn = false;
                    return false;
                }
            }
            else
            {
                rtn = true;
                return true;
            }
        }
   </script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Informe del Historial de inicio de sesi�n del usuario</title>
    <script type="text/javascript" src="script/cal-flat.js"></script>
    <script type="text/javascript" src="lang/calendar-en.js"></script>
    <script type="text/javascript" src="script/calendar-setup.js"></script>
    <script type="text/javascript" src="script/calendar-flat-setup.js"></script>
    <link rel="stylesheet" type="text/css" media="all" href="css/calendar-blue.css" /> <%--FB 1982--%>
    
</head>
<body>
    <form id="form1" runat="server">
    <center>
        <asp:Label ID="errLabel" runat="server" CssClass="lblError"></asp:Label>
    </center>
    
    <h3 style="text-align: center">
            Historial de inicio de sesi�n del usuario</h3>
            <br /><br />  
    <div>
        <table width="95%" align="center" bgcolor="white" cellpadding="1" cellspacing="0" border ="0"> <%-- FB 2050 --%>
        <tr id="trSearchH" runat="server">
                <td align="Left">
                                <span class="subtitleblueblodtext">buscar usuarios</span>
                            
                </td>
            </tr>
            <tr>
                             <td align="right" style="font-weight:bold; width:13%" class="blackblodtext">Nombre</td> <%-- FB 2050 --%>
                            <td align="left" style="width:20%"> <%-- FB 2050 --%>
                                <asp:TextBox ID="txtFirstName" CssClass="altText" runat="server"></asp:TextBox>
                                 <asp:RegularExpressionValidator ID="regfirstname" ControlToValidate="txtFirstName" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > + % \ / ? | ^ = ! `[ ] { } # $ y ~ son caracteres no v�lidos." ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>^+?|!`\[\]{}\=@#$%&~]*$"></asp:RegularExpressionValidator>
                            </td>
                            <td align="right" style="font-weight:bold; width:13%" class="blackblodtext">Apellido</td> <%-- FB 2050 --%>
                            <td align="left" style="width:20%"> <%-- FB 2050 --%>
                                <asp:TextBox ID="txtLastName" CssClass="altText" runat="server"></asp:TextBox>
                                 <asp:RegularExpressionValidator ID="reglastname" ControlToValidate="txtLastName" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > + % \ / ? | ^ = ! `[ ] { } # $ y ~ son caracteres no v�lidos." ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>^+?|!`\[\]{}\=@#$%&~]*$"></asp:RegularExpressionValidator>
                            </td>
                            <td align="right" style="font-weight:bold; width:13%" class="blackblodtext">Direcci�n de correo-e</td> <%-- FB 2050 --%>
                                <td align="left" style="width:20%"> <%-- FB 2050 --%>
                                    <asp:TextBox ID="txtLogin" CssClass="altText" runat="server"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="reglogin" ControlToValidate="txtLogin" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ ~ y &#34; son caracteres no v�lidos." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:#$%&()'~]*$"></asp:RegularExpressionValidator>
                                </td>
                        </tr>
                        <tr></tr>
                        <tr></tr>
                         <tr>
                                    <td align="right" class="blackblodtext"> Fecha DESDE</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtFromDate" runat="server" CssClass="altText" EnableViewState="true" ></asp:TextBox>
                                        <img src="image/calendar.gif" border="0" width="20" height="20" id="cal_triggerd" style="cursor: pointer;vertical-align:middle" title="Selector de Fecha" onblur="javascript:ChangeStartDate(0)" onclick="return showCalendar('<%=txtFromDate.ClientID %>', 'cal_triggerd', 0, '<%=format%>');" />
                                    </td>
                                    <td align="right" class="blackblodtext"> Fecha HASTA</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtToDate" runat="server" CssClass="altText" EnableViewState="true" onblur="javascript:ChangeEndDate(0)" ></asp:TextBox>
                                        <img src="image/calendar.gif" border="0" width="20" height="20" id="cal_trigger1" style="cursor: pointer;vertical-align:middle" title="Selector de Fecha" onblur="javascript:ChangeEndDate(0)" onclick="return showCalendar('<%=txtToDate.ClientID %>', 'cal_trigger1', 0, '<%=format%>');" />                                              
                                    </td>
                                    <td></td>
                                    <td align="left" class="blackblodtext">
                                         <asp:Button ID="btnSearch" runat="server" Text="buscar" CssClass="altShortBlueButtonFormat" ValidationGroup="DateSubmit" OnClick="btnSearch_Click" OnClientClick="javascript:return fnSubmit() " />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:RegularExpressionValidator ID="reqfromdate" runat="server" ControlToValidate="txtFromDate"
                                            ErrorMessage="Formato de Fecha no v�lido <%=format%>" ValidationExpression="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d|(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d"></asp:RegularExpressionValidator>
                                    </td>
                                    <td>
                                        <asp:RegularExpressionValidator ID="reqtodate" runat="server" ControlToValidate="txtToDate"
                                            ErrorMessage="Formato de Fecha no v�lido <%=format%>" ValidationExpression="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d|(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d"></asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                                </table>
                     <table width="100%" align="center">          
                    <tr>
                    
                <td align="center" width="100%">
                        <asp:DataGrid id="dgUserLoginReport" Width="90%" HorizontalAlign="Center" runat="server" AllowPaging="True" PageSize="15" PagerStyle-Mode="NumericPages" PagerStyle-HorizontalAlign="Right"
                            PagerStyle-NextPageText="Next" PagerStyle-PrevPageText="Prev" BorderColor="blue" BorderWidth="1" HeaderStyle-HorizontalAlign="Center" OnPageIndexChanged="dgUserLoginReport_PageIndexChanging" 
                            OnSortCommand="dgUserLoginReport_Sorting" GridLines="None" CellPadding="3" CellSpacing="0" HeaderStyle-Font-Bold="true" AutoGenerateColumns="false" CssClass="tableBody">
                            <SelectedItemStyle  CssClass="tableBody"/>
                            <AlternatingItemStyle CssClass="tableBody" />
                            <ItemStyle CssClass="tableBody" HorizontalAlign="Center"  />
                            <HeaderStyle CssClass="tableHeader" Height="30px" />
                            <EditItemStyle CssClass="tableBody" />
                                    <%--Window Dressing--%>
                            <FooterStyle CssClass="tableBody"/>
                            <Columns>
                                <asp:BoundColumn DataField="FirstName" HeaderStyle-HorizontalAlign="Center" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" HeaderText="Nombre" SortExpression="1"></asp:BoundColumn>
                                <asp:BoundColumn DataField="LastName" HeaderStyle-HorizontalAlign="Center" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" HeaderText="Apellido" SortExpression="2"></asp:BoundColumn>
                                <asp:BoundColumn DataField="Email" HeaderStyle-HorizontalAlign="Center" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" HeaderText="Correo-e" ></asp:BoundColumn>
                                <asp:BoundColumn DataField="LoginDateTime" HeaderStyle-HorizontalAlign="Center" ItemStyle-CssClass="tableBody"  HeaderStyle-CssClass="tableHeader" HeaderText="Fecha/Hora de inicio de sesi�n" SortExpression="3"></asp:BoundColumn>
                            </Columns>
                    </asp:DataGrid>
                    <asp:Label runat="server" ID="lblusrloginrecord" Visible="false"  CssClass="lblError"></asp:Label>
                </td>
            </tr>
            </table>
         </div>
         <asp:TextBox ID="txtSortBy" runat="server" Visible="false"></asp:TextBox>
    </form>
</body>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" --> 
</html>

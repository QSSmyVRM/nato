﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="ns_MyVRM.AudioAddOnBridge" %>

<meta http-equiv="X-UA-Compatible" content="IE=6" />
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
    <link rel="StyleSheet" href="css/divtable.css" type="text/css" />
    <link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/border-table.css" />
    <link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/main-table.css" />
    <link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/main.css" />
    <link rel="stylesheet" title="Expedite base styles" type="text/css" href="<%=Session["OrgCSSPath"]%>" />
</head>
<body>
    <form id="frmAudioAddOnBridge" runat="server">
    <input id="hdnEndpointID" name="hdnWebAccUR" runat="server" value="new" type="hidden" />
    <input id="hdnUserID" name="hdnWebAccUR1" runat="server" type="hidden" />
    <input id="hdninitialTime" name="hdninitialTime" runat="server" type="hidden" />
    <div>
        <table style="width: 90%" border="0" cellpadding="5" align="center">
            <tr>
                <td align="center" colspan="4">
                    <h3>
                        <asp:Label ID="lblHeader" runat="server" Text="Puente de Audio"></asp:Label>
                    </h3>
                </td>
            </tr>
            <tr>
                <td align="center" colspan="4">
                    <asp:Label ID="lblError" runat="server" CssClass="lblError"></asp:Label>
                </td>
            </tr>
            
            <tr>
                <td style="width: 20%" align="right" visible="false" class="blackblodtext" id="td2"
                    runat="server">
                    Correo-e para notificar
                </td>
                <td style="width: 30%" align="left" visible="false" id="td3" runat="server">
                    <asp:TextBox CssClass="altText" ID="txtEmailtoNotify" runat="server" onblur="CheckSecondaryEmail()"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegtxtEmailtoNotify" ControlToValidate="txtEmailtoNotify" ValidationGroup="Submit"
                        Display="dynamic" runat="server" ErrorMessage="<br>Dirección de correo electrónico Inválida." ValidationExpression="[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"></asp:RegularExpressionValidator>
                    <asp:RegularExpressionValidator ID="RegtxtEmailtoNotify1" ControlToValidate="txtEmailtoNotify" ValidationGroup="Submit"
                        Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ /() ; ? | ^= ! ` , [ ] { } : # $ and &#34; son caracteres no válidos."
                        ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=:#$%&'~]*$"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td style="width: 20%" align="right" class="blackblodtext" valign="top">
                    Nombre<span class="reqfldstarText">*</span>
                </td>
                <td style="width: 30%" align="left" valign="top">
                    <asp:TextBox ID="txtBridgName" Enabled='<%# Application["ssoMode"].ToString().ToUpper().Equals("NO") %>'
                        CssClass="altText" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="reqtxtBridgName" runat="server" ControlToValidate="txtBridgName"
                        ValidationGroup="Submit" Display="dynamic" ErrorMessage="Necesario"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegtxtBridgName" ControlToValidate="txtBridgName" ValidationGroup="Submit"
                        Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > + % \ / ? | = ! ` [ ] { } # $ @ and ~ son caracteres no válidos."
                        ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+?|!`\[\]{}\=@#$%&~]*$"></asp:RegularExpressionValidator>
                </td>
                <td style="width: 20%" align="right" class="blackblodtext" valign="top" rowspan="2" >
                    Departamento del puente
                </td>
                <td style="width: 30%" align="left" valign="top" rowspan="2">
                    <asp:ListBox runat="server" ID="lstBridgeDepts" CssClass="altSelectFormat" DataTextField="name"
                        DataValueField="id" Rows="6" SelectionMode="Multiple"></asp:ListBox>
                </td>
            </tr>
            <tr>
                <td style="width: 20%" align="right" class="blackblodtext" id="Td4" runat="server"
                    valign="top">
                    Zona horaria<span class="reqfldstarText">*</span>
                </td>
                <td style="width: 30%" align="left" id="Td5" runat="server" valign="top">
                    <asp:DropDownList ID="lstBridgetimezone" runat="server" CssClass="altSelectFormat"
                        DataTextField="timezoneName" DataValueField="timezoneID">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="ReqlstBridgetimezone" ControlToValidate="lstBridgetimezone"
                        ValidationGroup="Submit" ErrorMessage="Necesario" InitialValue="-1" Display="dynamic"
                        runat="server"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr id="trLblAV" runat="server" style="display: none">
                <td align="left" colspan="4">
                    <table cellspacing="5">
                        <tr>
                            <td style="width: 20">
                                &nbsp;
                            </td>
                            <td>
                                <span class="subtitleblueblodtext">Determinar Parámetros de Conexión Audio/Visual</span>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="right" class="blackblodtext">
                    Nombre de Perfil de Punto final<span class="reqfldstarText">*</span>
                </td>
                <td align="left" colspan="3">
                    <asp:TextBox ID="txtEndpointName" runat="server" CssClass="altText" Width="300px"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="regConfName" ControlToValidate="txtEndpointName" ValidationGroup="Submit"
                        Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > + % \ / ( ) ? | ^ = ! ` [ ] { } # $ @ and ~ son caracteres no válidos."
                        ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+?|!`\[\]{}\=@#$%&()~]*$"></asp:RegularExpressionValidator>
                    <asp:RequiredFieldValidator ID="ReqtxtEndpointName" runat="server" ControlToValidate="txtEndpointName"
                        ValidationGroup="Submit" Display="dynamic" ErrorMessage="Necesario"></asp:RequiredFieldValidator>
                </td>
                
            </tr>
            <tr>
                <td align="right" class="blackblodtext">
                    tipo de dirección<span class="reqfldstarText">*</span>
                </td>
                <td align="left">
                    <asp:DropDownList ID="lstAddressType" CssClass="altSelectFormat" DataTextField="Name"
                        DataValueField="ID" runat="server">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="ReqlstAddressType" ControlToValidate="lstAddressType"
                        ValidationGroup="Submit" ErrorMessage="Necesario" InitialValue="-1" Display="dynamic"
                        runat="server"></asp:RequiredFieldValidator>
                </td>
                <td align="right" class="blackblodtext">
                    dirección<span class="reqfldstarText">*</span>
                </td>
                <td align="left">
                    <asp:TextBox ID="txtAddress" runat="server" CssClass="altText"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator4" ControlToValidate="txtAddress" ValidationGroup="Submit"
                        Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > + % \ / ( ) ? | ^ = `<br> [ ] { } $ and ~ son caracteres no válidos."
                        ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+?|`\[\]{}\=$%&()~]*$"></asp:RegularExpressionValidator>
                    <asp:RequiredFieldValidator ID="ReqtxtAddress" runat="server" ControlToValidate="txtAddress"
                        ValidationGroup="Submit" Display="dynamic" ErrorMessage="Necesario"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td align="right" class="blackblodtext">
                    Protocolo por defecto<span class="reqfldstarText">*</span>
                </td>
                <td align="left">
                    <asp:DropDownList CssClass="altSelectFormat" ID="lstProtocol" runat="server" DataTextField="Name"
                        DataValueField="ID">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="ReqlstProtocol" ControlToValidate="lstProtocol" ValidationGroup="Submit"
                        ErrorMessage="Necesario" InitialValue="-1" Display="dynamic" runat="server"></asp:RequiredFieldValidator>
                </td>
                <td align="right" class="blackblodtext">
                    ¿Fuera de la Red?
                </td>
                <td align="left">
                    <asp:DropDownList ID="lstIsOutsideNetwork" CssClass="altText" runat="server">
                        <asp:ListItem Value="0" Text="No"></asp:ListItem>
                        <asp:ListItem Value="1" Text="Si"></asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr id="trMCUnumber" runat="server">
                <td align="right" class="blackblodtext">
                    Número MCU de vídeo interno
                </td>
                <td align="left">
                    <asp:TextBox ID="txtIntVideoMcuNo" CssClass="altText" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" ControlToValidate="txtIntVideoMcuNo" ValidationGroup="Submit"
                        Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ ( ) ; ? | ^ = ! ` , [ ] { } # $ @ ~ and &#34; son caracteres no válidos."
                        ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>+?|!`,;\[\]{}\x22;=@#$%&()'~]*$"></asp:RegularExpressionValidator>
                </td>
                <td align="right" class="blackblodtext">
                    Número MCU de vídeo interno
                </td>
                <td align="left">
                    <asp:TextBox ID="txtExtVideoMcuNo" CssClass="altText" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator10" ControlToValidate="txtExtVideoMcuNo" ValidationGroup="Submit"
                        Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ` , [ ] { } : $ @ ~ and &#34; son caracteres no válidos."
                        ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>^+;?|`,\[\]{}\x22;=:@$%&()'~]*$"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td align="right" class="blackblodtext">
                    PIN de líder
                </td>
                <td align="left">
                    <asp:TextBox CssClass="altText" ID="txtLeaderPin" runat="server"></asp:TextBox>
                </td>
                <td align="right" class="blackblodtext">
                    Código de Conferencia
                </td>
                <td align="left">
                    <asp:TextBox CssClass="altText" ID="txtConfCode" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
              <td style="height:20px" colspan="4">
              </td>
            </tr>
            <tr>
                <td colspan="4" align="center">
                    <asp:Button runat="server" ID="btnReset" Text="reajustar" ValidationGroup="Reset" CssClass="altShortBlueButtonFormat"
                        OnClick="ResetPage" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Button runat="server" ID="btnGoBack" Text="Regresar" CssClass="altShortBlueButtonFormat"
                        OnClick="GobackToParentPage" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Button runat="server" ID="btnnewSubmi" Width="280px" CssClass="altLongBlueButtonFormat" Text="Entregar / Añadir Puente de Audio nuevo"
                        ValidationGroup="Submit" OnClick="SubmitAudioBridge" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="btnnewSubmit" runat="server" CssClass="altShortBlueButtonFormat"
                        Text="Entregar" ValidationGroup="Submit" OnClick="SubmitAudioBridge" />
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->

<%@ Page Language="C#" Inherits="ns_Department.Department" Buffer="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <!-- FB 2050 -->
<meta http-equiv="X-UA-Compatible" content="IE=7" /> <!-- FB 2050 -->
<!--Window Dressing-->
<!-- #INCLUDE FILE="inc/maintopNET.aspx" --> 

<script runat="server">

</script>
<script language="javascript">
function CheckName()
{
    if (document.getElementById("<%=txtNewDepartmentName.ClientID %>").value == "")
    {
        document.getElementById("<%=lblRequired.ClientID %>").innerText = "Required";
        return false;
    }
    else
    {
        alert("Para a�adir usuarios a este departamento, edite cada perfil de usuario como sea necesario."); // this alert has been added after discussing with Bri
        document.getElementById("<%=lblRequired.ClientID %>").innerText = "";
        return true;
    }
}
</script>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Administrar Departamentos</title>
    <script type="text/javascript" src="inc/functions.js"></script>

</head>
<body>
    <form id="frmInventoryManagement" runat="server" method="post" onsubmit="return true">
      <input type="hidden" id="helpPage" value="97">

    <div>
        <table width="100%">
            <tr>
                <td align="center">
                    <h3>
                        <asp:Label ID="lblHeader" runat="server"></asp:Label>
                    </h3>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td >&nbsp;</td>
                            <td>
                                <SPAN class=subtitleblueblodtext>Departamentos existentes</SPAN>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:DataGrid ID="dgDepartments" runat="server" AutoGenerateColumns="False" CellPadding="4" GridLines="None"
                         BorderColor="blue" BorderStyle="solid" BorderWidth="1" ShowFooter="true" OnItemCreated="BindRowsDeleteMessage"
                        OnDeleteCommand="DeleteDepartment" OnEditCommand="EditDepartment" OnUpdateCommand="UpdateDepartment" OnCancelCommand="CancelDepartment" Width="90%" Visible="true" style="border-collapse:separate"> <%--Edited for FF--%>
                       <%--Window Dressing - Start--%>
                        <SelectedItemStyle  CssClass="tableBody"  Font-Bold="True"  />
                        <EditItemStyle CssClass="tableBody" />
                        <AlternatingItemStyle CssClass="tableBody" />
                        <ItemStyle CssClass="tableBody" />
                        <FooterStyle CssClass="tableBody" />
                        <%--Window Dressing - End--%>
                        <HeaderStyle CssClass="tableHeader" />
                        <Columns>
                            <asp:BoundColumn DataField="id" Visible="false" ><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="Nombre" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader"> <%-- FB 2050 --%>
                                <ItemTemplate>
                                    <asp:Label ID="lblDepartmentName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.name") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtDepartmentName" CssClass="altText" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.name")%>'></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="reqDepartmentName1" ControlToValidate="txtDepartmentName" runat="server" ErrorMessage="Necesario" Display="dynamic" ValidationGroup="Update" ></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator4" ControlToValidate="txtDepartmentName" Display="dynamic" runat="server" ValidationGroup="Update" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ and &#34; son caracteres no v�lidos." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Acciones" ItemStyle-HorizontalAlign="Center" > <%-- FB 2050 --%>
                                <HeaderStyle CssClass="tableHeader" />
                                <ItemTemplate>
                                    <asp:LinkButton runat="server" Text="Editar" ID="btnEdit" CommandName="Edit"></asp:LinkButton>
                                    &nbsp;<asp:LinkButton runat="server" Text="Eliminar" ID="btnDelete" CommandName="Delete"></asp:LinkButton>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:LinkButton runat="server" Text="Actualizar" ID="btnUpdate" CommandName="Update" ValidationGroup="Update"></asp:LinkButton>
                                    &nbsp;<asp:LinkButton runat="server" Text="Cancelar" ID="btnCancel" CommandName="Cancel"></asp:LinkButton>
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <%--Window Dressing --%>
                                    <b class="blackblodtext">Departamentos &nbsp;totales:</b><b> <asp:Label ID="lblTotalRecords" runat="server" Text=""></asp:Label> </b>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:Table runat="server" ID="tblNoDepartments" Visible="false" Width="90%">
                        <asp:TableRow CssClass="lblError">
                            <asp:TableCell CssClass="lblError">
                                No se encontraron Departamentos.
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>                    
                </td>
            </tr>
            <tr>
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <SPAN class=subtitleblueblodtext><asp:Label ID="lblCreateEditDepartment" runat="server" Text="Crear Nuevo"></asp:Label> Department</SPAN>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table border="0" cellpadding="5" cellspacing="5" width="90%">
                        <tr>
                             <%--Window Dressing --%>
                            <td align="center" width="20%" class="blackblodtext">
                                nombre de departamento: 
                                <asp:TextBox ID="txtNewDepartmentName" CssClass="altText" runat="server" Text="" ></asp:TextBox>
                                <asp:Label ID="lblRequired" CssClass="lblError" runat="server"></asp:Label>
                                <asp:Button runat="server" ID="btnCreateNewDepartment" Text="Entregar" CssClass="altShortBlueButtonFormat" OnClick="CreateNewDepartment" ValidationGroup="Submit" />
                                <asp:RequiredFieldValidator ID="reqName1" ControlToValidate="txtNewDepartmentName" ErrorMessage="Necesario" ValidationGroup="Submit" runat="server" Display="dynamic"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator4" ControlToValidate="txtNewDepartmentName" Display="dynamic" runat="server" ValidationGroup="Submit" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ and &#34; son caracteres no v�lidos." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center"> <%--fogbugz case 413--%>
                    <table border="0" cellpadding="5" cellspacing="5" width="90%">
                        <tr>
                            <td align="center" width="20%" class="blackblodtext"><b>
                                (Para a�adir usuarios existentes a este departamento, realice las modificaciones necesarias en el perfil de usuario.)</b>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="txtMultiDepartment" runat="server" Text="" Visible="false"></asp:TextBox>
                </td>
            </tr>
        </table>
    </div>

<img src="keepalive.asp" name="myPic" width="1px" height="1px">
    </form>

<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
    <!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
</body>
</html>


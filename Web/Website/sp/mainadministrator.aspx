<%@ Page Language="C#" Debug="true" Inherits="MYVRM.MainAdministrator" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register Assembly="MetaBuilders.WebControls.ComboBox" Namespace="MetaBuilders.WebControls" TagPrefix="mbcbb" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- FB 2050 -->
<meta http-equiv="X-UA-Compatible" content="IE=7" />
<!-- FB 2050 -->
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
<style type="text/css">
table
{
	border-collapse:collapse;
}
</style>
<script type="text/javascript" src="script/myprompt.js"></script>
<!-- JavaScript begin -->

<script language="JavaScript1.2" src="inc/functions.js"></script>
<script language="JavaScript">
<!--
    //<%--FB 1490 Start--%>
    function fncheckTime() {
        var stdate = '';
        if (document.getElementById("systemEndTime_Text") && document.getElementById("systemStartTime_Text")) {
            stdate = GetDefaultDate('01/01/1901', '<%=((Session["timeFormat"] == null) ? "1" : Session["timeFormat"])%>');
            if (Date.parse(stdate + " " + document.getElementById("systemEndTime_Text").value) < Date.parse(stdate + " " + document.getElementById("systemStartTime_Text").value)) {
                alert("La hora de Finalizaci�n deber�a ser posterior a la Hora de Inicio."); //FB 2148
                document.getElementById("systemStartTime_Text").focus();
                return false;
            }
            else if (Date.parse(stdate + " " + document.getElementById("systemEndTime_Text").value) == Date.parse(stdate + " " + document.getElementById("systemStartTime_Text").value)) {
            alert("La hora de Finalizaci�n deber�a ser posterior a la Hora de Inicio.");
                document.getElementById("systemEndTime_Text").focus();
                return false;
            }
        }
        return true;
    }
    //<%--FB 1490 End--%>
    //FB 2486
    function toggle() {
        var ele = document.getElementById("toggleText");
        var text = document.getElementById("displayText");
        if (ele.style.display == "block" || ele.style.display == "") {
            ele.style.width = "100%";
            ele.style.display = "none";
            text.innerHTML = "M�s";
        }
        else {
            ele.style.display = "";
            ele.style.width = "100%";
            text.innerHTML = "Menos";
        }
    }
    function open24() {
        t = (document.frmMainadminiatrator.Open24.checked) ? "none" : ''; //Edited For FF...
        for (var i = 1; i < 5; i++) {
            document.getElementById("Open24DIV" + i).style.display = t;
        }
        document.getElementById("systemStartTime_Text").style.width = "100px";
        document.getElementById("systemEndTime_Text").style.width = "100px";
    }


    function ValidateInput() {
        if ((document.getElementById("lstDefaultConferenceType").value == "2") && (document.getElementById("lstEnableAudioVideoConference").value == "0")) {
            alert("Por favor, habilite primero la Conferencia Audio/V�deo para hacerla del tipo de conferencia por defecto.");
            document.getElementById("lstEnableAudioVideoConference").focus();
            return false;
        }
        if ((document.getElementById("lstDefaultConferenceType").value == "7") && (document.getElementById("lstEnableRoomConference").value == "0")) {
            alert("Por favor, habilite primero la Conferencia de Sal�n para hacerla del tipo de conferencia por defecto.");
            document.getElementById("lstEnableRoomConference").focus();
            return false;
        }
        if ((document.getElementById("lstDefaultConferenceType").value == "6") && (document.getElementById("lstEnableAudioOnlyConference").value == "0")) {
            alert("Por favor, habilite primero la Conferencia S�lo-Audio para hacerla del tipo de conferencia por defecto.");
            document.getElementById("lstEnableAudioOnlyConference").focus();
            return false;
        }
        if ((document.getElementById("lstDefaultConferenceType").value == "4") && (document.getElementById("p2pConfEnabled").value == "0")) {
            alert("Por favor, habilite primero la Conferencia P2P para hacerla del tipo de conferencia por defecto.");
            document.getElementById("p2pConfEnabled").focus();
            return false;
        }
        //<%--FB 1490 Start--%>
        if (!fncheckTime())
            return false;
        //<%--FB 1490 End--%>

        return true;
    }

    //FB 2136
    function modedisplay() {
        var mode = document.getElementById("drpenablesecuritybadge");
        var type = document.getElementById("drpsecuritybadgetype");

        if (document.getElementById("drpenablesecuritybadge").value == "1") {
            document.getElementById("tdSecurityType").style.display = "block";
            type.style.display = "block";
            emaildisplay();
        }
        else {
            document.getElementById("tdSecurityType").style.display = "none";
            type.style.display = "none";
            document.getElementById("tdsecdeskemailid").style.visibility = "hidden";
        }
    }

    function emaildisplay() {
        var type = document.getElementById("drpsecuritybadgetype");

        if (document.getElementById("drpsecuritybadgetype").value == "2" || document.getElementById("drpsecuritybadgetype").value == "3") {
            document.getElementById("tdsecdeskemailid").style.visibility = "visible";
        }
        else {
            document.getElementById("tdsecdeskemailid").style.visibility = "hidden";
        }
    }
    //FB 2348 Start
    function modedisplay1() {
        if (document.getElementById("drpenablesurvey").value == "1") {
            document.getElementById("tdSurveyengine").style.visibility = "visible";
            document.getElementById("tdsurveyoption").style.visibility = "visible";
            modesurvey()

        }
        else {
            document.getElementById("divSurveytimedur").style.display = "none";
            document.getElementById("divSurveyURL").style.display = "none";
            document.getElementById("tdSurveyengine").style.visibility = "hidden";
            document.getElementById("tdsurveyoption").style.visibility = "hidden";
            ValidatorEnable(document.getElementById("ReqSurWebsiteURL"), false);
            ValidatorEnable(document.getElementById("RegSurWebsiteURL"), false);
            ValidatorEnable(document.getElementById("RegTimeDur"), false);

        }
    }
    function modesurvey() {
        if (document.getElementById("drpsurveyoption").value == "2") {
            document.getElementById("divSurveyURL").style.display = "block";
            document.getElementById("divSurveytimedur").style.display = "block";
            ValidatorEnable(document.getElementById("ReqSurWebsiteURL"), true);
            ValidatorEnable(document.getElementById("RegSurWebsiteURL"), true);
            ValidatorEnable(document.getElementById("RegTimeDur"), true);
        }
        else {
            document.getElementById("divSurveyURL").style.display = "none";
            document.getElementById("divSurveytimedur").style.display = "none";
            ValidatorEnable(document.getElementById("ReqSurWebsiteURL"), false);
            ValidatorEnable(document.getElementById("RegSurWebsiteURL"), false);
            ValidatorEnable(document.getElementById("RegTimeDur"), false);

        }
    }
    //FB 2347T
    function ChangeValidator() {
        if (document.getElementById("drpenablesurvey").value == "2" || document.getElementById("drpsurveyoption").value == "1") {
            ValidatorEnable(document.getElementById("ReqSurWebsiteURL"), false);
            ValidatorEnable(document.getElementById("RegSurWebsiteURL"), false);
            ValidatorEnable(document.getElementById("RegTimeDur"), false);
        }
    }

    function ExpandCollapse(img, str, frmCheck) {
        obj = document.getElementById(str);

        if (str == "trFLY") // FB 2426
        {
            var drptopObj = document.getElementById("lstTopTier");
            var drpmiddleObj = document.getElementById("lstMiddleTier");
            var selectop = drptopObj.options[drptopObj.selectedIndex].text;
            var selecmiddle = drpmiddleObj.options[drpmiddleObj.selectedIndex].text;
            if (selectop == "Por favor, seleccione..." && obj.style.display == "")
                return false;
            if (selecmiddle == "Por favor, seleccione..." && obj.style.display == "")
                return false;
        }

        if (obj != null) {
            if (frmCheck == true) {
                if (document.getElementById("chkExpandCollapse").checked) {
                    img.src = img.src.replace("minus", "plus");
                    obj.style.display = "none";
                    //alert("in if");
                }
                else {
                    img.src = img.src.replace("plus", "minus");
                    obj.style.display = "";
                    //alert("in else");
                }
            }
            if (frmCheck == false) {
                //alert("in else");
                if (img.src.indexOf("minus") >= 0) {
                    img.src = img.src.replace("minus", "plus");
                    obj.style.display = "none";
                }
                else {
                    img.src = img.src.replace("plus", "minus");
                    obj.style.display = "";
                    // FB 2565 Starts
                    if(str == "trCONFOPT")
                    {
                    document.getElementById("trCONFDEF").style.display = "";
                    var imgobj = document.getElementById("img_CONFDEF");
                    imgobj.src = imgobj.src.replace("plus", "minus")
                    
                    var idArray = new Array("CONFTYPE","FEAT","AUD","CONFMAIL");
                        for(var k = 0; k<idArray.length; k++)
                        {
                        document.getElementById("tr"+idArray[k]).style.display = "none";
                        imgobj = document.getElementById("img_"+idArray[k]);
                        imgobj.src = imgobj.src.replace("minus", "plus")
                        }
                    }
                    // FB 2565 Ends
                }
            }
        }
        document.getElementById("systemStartTime_Text").style.width = "100px";
        document.getElementById("systemEndTime_Text").style.width = "100px";
    }
    //FB 2348 End

    function fnBufferOptions()//FB 2398
    {
        if (document.getElementById("EnableBufferZone").value == "1") {
            document.getElementById("trBufferOptions").style.visibility = "visible";
            document.getElementById("trMCUBufferOptions").style.visibility = "visible"; //FB 2440       
            document.getElementById("trForceMCUBuffer").style.visibility = "visible"; //FB 2440
        }
        else {
            document.getElementById("trBufferOptions").style.visibility = "hidden";
            document.getElementById("trMCUBufferOptions").style.visibility = "hidden"; //FB 2440
            document.getElementById("trForceMCUBuffer").style.visibility = "hidden"; //FB 2440
        }
    }

    //FB 2426 Start
    function Submit() {
        var lsttoptier = document.getElementById('<%=lstTopTier.ClientID%>');
        var lstmiddletier = document.getElementById('<%=lstMiddleTier.ClientID%>');
        //FB 2501
        var duration = document.getElementById('txtDefaultConfDuration').value;
        if (duration < 15 || duration > 1440)
            return false;

        if (lsttoptier.value == "-1") {
            //alert("Please select the Top Tier");
            reqTopTier.style.display = 'block';
            lsttoptier.focus();
            return false;
        }

        if (lstmiddletier.value == "-1") {
            //alert("Please select the Middle Tier");
            reqMiddleTier.style.display = 'block';
            lstmiddletier.focus();
            return false;
        }
    }
    //FB 2426 End
    
//-->
</script>
<%--FB 2486--%>
<script type="text/javascript">

    function fnCheck(arg) {
        var srcID = document.getElementById(arg);
        var ckboxName = "chkmsg";
        var ctrlIDNo = arg.substring(arg.length, arg.length - 1)
        var drpName = srcID.id.replace(ctrlIDNo, "");
        var pVal = srcID.getAttribute("PreValue");
        var ckboxSel = document.getElementById(ckboxName + "" + ctrlIDNo);

        if (ckboxSel && ckboxSel.checked == false)
            return true;

        if (pVal == null)
            pVal = srcID.options[0].text;

        for (var i = 1; i <= 9; i++) {
            var ckbox = document.getElementById(ckboxName + "" + i);
            if (ckbox) {
                if (i == ctrlIDNo)
                    continue;

                if (ckbox.checked) {
                    var destDrpName = document.getElementById(drpName + i);
                    if (destDrpName) {
                        if (destDrpName.value == srcID.value) {
                            srcID.value = pVal;
                            alert("El tiempo seleccionado ya est� definido para otro mensaje");
                            if (ckboxSel)
                                ckboxSel.checked = false;
                            return false;
                        }
                    }
                }
            }
        }

        srcID.setAttribute("PreValue", srcID.value);
        return true;
    }
    //FB 2632
    function fnUpdateCngSupport() {
        var e = document.getElementById("drpCngSupport");
        var drpVal = e.options[e.selectedIndex].value;
        if (drpVal == 0) {
            document.getElementById("chkMeetandGreet").disabled = true;
            document.getElementById("chkOnSiteAVSupport").disabled = true;
            document.getElementById("chkConciergeMonitoring").disabled = true;
            document.getElementById("chkDedicatedVNOCOperator").disabled = true;
        }
        else {
            document.getElementById("chkMeetandGreet").disabled = false;
            document.getElementById("chkOnSiteAVSupport").disabled = false;
            document.getElementById("chkConciergeMonitoring").disabled = false;
            document.getElementById("chkDedicatedVNOCOperator").disabled = false;

        }
    }
    

</script>

<!-- JavaScript finish -->

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <title></title>
    <link rel="StyleSheet" href="css/divtable.css" type="text/css" />
    <link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/border-table.css" />
    <link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/main-table.css" />
    <link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/main.css" />
    <link rel="stylesheet" title="Expedite base styles" type="text/css" href="<%=Session["OrgCSSPath"]%>" />
</head>
<body>
    <div style="text-align: left">
        <form name="frmMainadminiatrator" id="frmMainadminiatrator" method="Post" action="mainadministrator.aspx"
        onsubmit="return ValidateInput()" language="JavaScript" runat="server">
        <asp:ScriptManager ID="OrgOptionScriptManager" runat="server" AsyncPostBackTimeout="600">
        </asp:ScriptManager>
        <input type="hidden" name="cmd" value="SetSystemDetails" />
        <input type="hidden" name="ClosedDay" value="" />
        <input type="hidden" id="helpPage" value="91" />
        <input type="hidden" id="Poly2MGC" runat="server" value="" />
        <input type="hidden" id="Poly2RMX" runat="server" value="" />
        <input type="hidden" id="CTMS2Cisco" runat="server" value="" />
        <input type="hidden" id="CTMS2Poly" runat="server" value="" />
        <input type="hidden" id="hdnSurURL" runat="server" value="" />
        <input type="hidden" id="hdnTimeDur" runat="server" value="" />
        <input type="hidden" id="hdnTierIDs" runat="server" value="" /> <%--FB 2637--%>
        
        <div align="center">
            <h3>
                Opciones de organizaci�n</h3>
            <br />
            <asp:Label ID="errLabel" CssClass="lblError" runat="server" Text="Label" Visible="False"></asp:Label>
        </div>
        
        <div align="center">
        
        <table cellpadding="2"  cellspacing="0" width="100%" border="0">
            <tr></tr>
            <tr>
                <td colspan="7" height="10"></td>
            </tr>
            
            <tr>
                <td colspan="7" align="left">
                    <table width="100%">
                        <tr>
                            <td width="100%" height="20" valign="bottom"align="left" >
                                <table>
                                    <tr>
                                        <td>
                                            <asp:ImageButton ID="img_USROPT" runat="server" ImageUrl="image/loc/nolines_minus.gif"
                                                Height="25" Width="25" vspace="0" hspace="0" />
                                        </td>
                                        <td valign="top">
                                            <span class="subtitleblueblodtext">Opciones para el usuario</span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                         </tr>
                    </table>
                </td>
            </tr>
            <tr id="trUSROPT" runat="server">
                <td colspan="7">
                    <table width="100%" border="0" cellpadding="5">
                        <tr>
                            <td style="width: 1%;"></td>
                            <td align="left" class="blackblodtext" style="width: 25%;">
                                <b>Mostrar solamente estas Zonas Horarias</b>
                                <%--<asp:ImageButton ID="Imgshowotime" valign="center" src="image/info.png" runat="server" ToolTip="Timezone control in overall application."/>--%>
                            </td>
                            <td align="left" style="width: 25%">
                                <asp:DropDownList ID="TimezoneSystems" runat="server" CssClass="altLong0SelectFormat"
                                        Width="170px">
                                    </asp:DropDownList>
                            </td>
                            <td style="width: 1%"></td>
                            <td align="left" class="blackblodtext" style="width: 27%">
                                <b>Habilitar departamentos</b>
                                <%--<asp:ImageButton ID="Imgdeptuser" valign="center" src="image/info.png" runat="server" ToolTip="Control active user display in addressbook."/>--%>
                            </td>
                            <td align="left" style="width: 20%">
                                <asp:DropDownList ID="DrpDwnListDeptUser" runat="server" CssClass="alt2SelectFormat">
                                    <asp:ListItem Value="1">Si</asp:ListItem>
                                    <asp:ListItem Value="0">No</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td style="width: 1%"></td>
                        </tr>
                        <tr>
                            <td style="width: 2%;"></td>
                            <td align="left" class="blackblodtext" style="width: 25%;">
                                <strong>Habilitar Regla de Contrase�a</strong>
                                <%--<asp:ImageButton ID="Imgpasswordrule" valign="center"  src="image/info.png" runat="server" ToolTip="Strong password rule for users"/>--%>
                            </td>
                            <td valign="top" align="left" style="width: 25%">
                                <asp:DropDownList ID="DrpDwnPasswordRule" runat="server" CssClass="alt2SelectFormat">
                                    <asp:ListItem Value="0">No</asp:ListItem>
                                    <asp:ListItem Value="1">Si</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td style="width: 1%;"></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </table>
                </td> 
            </tr>
            
            <tr style="display: none;">
                <td colspan="7" align="left">
                    <table width="100%">
                        <tr>
                            <td width="100%" height="20" valign="bottom"align="left" >
                                <table>
                                    <tr>
                                        <td>
                                            <asp:ImageButton ID="img_ROOM" runat="server" ImageUrl="image/loc/nolines_minus.gif"
                                                Height="25" Width="25" vspace="0" hspace="0" />
                                        </td>
                                        <td valign="top">
                                            <span class="subtitleblueblodtext">Opciones del sal�n</span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                         </tr>
                    </table>
                </td>
            </tr>
            <tr id="trROOM" runat="server" style="display: none;">
                <td colspan="8">
                    <table border="0" width="100%">
                        <tr>
                            <td style="width: 6%;"></td>
                            <td align="left" valign="top" style="width: 185px;" class="blackblodtext">
                                <b>Nivel de Expansi�n del �rbol de Salones</b>
                            </td>
                            <td style="width: 0px"></td>
                            <td valign="top" align="left">
                              <%--  <asp:listitem value="0">
                                </asp:listitem>--%>
                                <asp:DropDownList ID="lstRoomTreeLevel" runat="server" CssClass="altLong0SelectFormat"
                                    Width="205px">
                                    <asp:ListItem Value="1">Expanded - Top Tier Only</asp:ListItem>
                                    <asp:ListItem Value="2">Expanded - Middle Tier Only</asp:ListItem>
                                    <asp:ListItem Value="3" Selected="True">Expanded - All Levels</asp:ListItem>
                                    <asp:ListItem Value="list">List View</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td colspan="4"></td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            <tr>
                <td colspan="7" align="left" style="height: 49px">
                    <table width="100%">
                        <tr>
                            <td width="100%" height="20" align="left">
                                <table>
                                    <tr> 
                                        <td>
                                            <asp:ImageButton ID="img_CONFOPT" runat="server" ImageUrl="image/loc/nolines_plus.gif"
                                                Height="25" Width="25" vspace="0" hspace="0" />
                                        </td>
                                        <td valign="top">
                                            <span class="subtitleblueblodtext">
                                                <%if (Application["Client"].ToString().ToUpper() == "MOJ")
                                                    {%>Hearing Options<%}
                                                else
                                                { %>opciones de conferencia<%}%></span>
                                                
                                         </td> 
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trCONFOPT" runat="server" style="display:none">
                <td colspan="7">
                    <table border="0" width="100%" cellpadding="5">
                    
                        <tr>
                            <td colspan="7" align="left" style="height: 49px">
                                <table width="100%">
                                    <tr>
                                         <td width="100%" height="20" align="left">
                                            <table>
                                                <tr> 
                                                    <td>
                                                        <asp:ImageButton ID="img_CONFDEF" runat="server" ImageUrl="image/loc/nolines_plus.gif"
                                                            Height="25" Width="25" vspace="0" hspace="0" />
                                                     </td>
                                                    <td valign="top">
                                                        <span class="subtitleblueblodtext">
                                                        configuraciones por defalto
                                                    </span>
                                                    </td> 
                                                 </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>                  
                        <tr id="trCONFDEF" runat="server" style="display: none">
                            <td colspan="7">
                                <table border="0" width="100%" cellpadding="5">
                                    <tr>
                                        <td style="width: 1%;"></td>
                                        <td align="left" class="blackblodtext" rowspan="1" style="width: 25%;" valign="top">
                                            <strong>
                                                <%if (Application["Client"].ToString().ToUpper() == "MOJ")
                                          {%>Default hearing type<%}
                                          else
                                          { %>Tipos de Conferencias por defecto<%}%><%--added for FB 1428 Start--%></strong>
                                          <%--<asp:ImageButton ID="Imgdeconftype"  valign="center" src="image/info.png" runat="server" ToolTip="Default all conference to selected type."/>--%>
                                        </td>
                                        <td valign="top" align="left" style="width: 25%;">
                                            <asp:DropDownList ID="lstDefaultConferenceType" runat="server" CssClass="alt2SelectFormat">
                                                <asp:ListItem Selected="True" Value="6">S�lo audio</asp:ListItem>
                                                <asp:ListItem Value="2">Audio/Video</asp:ListItem>
                                                <asp:ListItem Value="4">Punto a Punto</asp:ListItem>
                                                <asp:ListItem Value="7">S�lo sal�n</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td align="right" style="width: 1%;" valign="top">
                                        </td>
                                         <td style="width: 27%;" align="left" class="blackblodtext">
                                            <asp:Label ID="Label7" runat="server" Text="Velocidad de L�nea por defecto"></asp:Label>
                                            <%--<asp:ImageButton ID="Imgdelinerate"  valign="center" src="image/info.png" runat="server" ToolTip="Default linerate to all calls."/>--%>
                                        </td>
                                        <td style="width: 20%;" align="left">
                                            <asp:DropDownList CssClass="altSelectFormat" Width="120px" ID="lstLineRate" runat="server"
                                                DataTextField="LineRateName" DataValueField="LineRateID">
                                            </asp:DropDownList>
                                        </td>
                                        <td style="width: 1%;"></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 1%;"></td>
                                        <td align="left" class="blackblodtext" valign="top" style="width: 25%">
                                            <strong style="align:left">Duraci�n predeterminada de la Conferencia</strong>
                                          <%--<asp:ImageButton ID="Imgdeconfdur"  valign="center" src="image/info.png" runat="server" ToolTip="Default all calls to given duration."/>--%>
                                        </td>
                                        <td valign="top" align="left" style="width: 25%">
                                            <asp:TextBox ID="txtDefaultConfDuration" runat="server" CssClass="altText" Width="50px"> </asp:TextBox>(mins)
                                            <asp:RangeValidator ID="RangeDefaultConfDuration" SetFocusOnError="true" Type="Integer"
                                                MinimumValue="15" MaximumValue="1440" Display="Dynamic" ControlToValidate="txtDefaultConfDuration"
                                                ValidationGroup="Submit" runat="server" ErrorMessage="La duraci�n de las conferencias es de 15 a 1440 minutos."></asp:RangeValidator>
                                            <asp:RegularExpressionValidator ID="RegDefaultConfDuration" ValidationGroup="Submit"
                                                ControlToValidate="txtDefaultConfDuration" Display="dynamic" runat="server" SetFocusOnError="true"
                                                ErrorMessage="S�lo valores num�ricos." ValidationExpression="\d+"></asp:RegularExpressionValidator>
                                                <asp:RequiredFieldValidator ID="reqdefaultduration"  runat="server" ControlToValidate="txtDefaultConfDuration" Display="dynamic" ErrorMessage="La duraci�n de las conferencias es de 15 a 1440 minutos"></asp:RequiredFieldValidator><%--FB 2635--%>
                                        </td>
                                        <td style="width: 1%;"></td>
                                        <td align="left" style="width: 27%;" class="blackblodtext" valign="top">
                                            <strong>Superposici�n de mensajes</strong>
                                            <%--<asp:ImageButton ID="Imgmsgolay"  valign="center"  src="image/info.png" runat="server" ToolTip="Default conference end message for Audio/Video calls."/>--%>
                                        </td>
                                        <td valign="top" style="width: 20%;" align="left">
                                            <asp:Button Width="50%" runat="server" ID="btmTxtmsgPopup" CssClass="altBlueButtonFormat" Text="personalizar" />
                                        </td>
                                        <td style="width: 1%;"></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    
                        <tr>
                            <td colspan="7" align="left" style="height: 49px">
                                <table width="100%">
                                    <tr>
                                         <td width="100%" height="20" align="left">
                                            <table>
                                                <tr> 
                                                    <td>
                                                        <asp:ImageButton ID="img_CONFTYPE" runat="server" ImageUrl="image/loc/nolines_plus.gif"
                                                            Height="25" Width="25" vspace="0" hspace="0" />
                                                     </td>
                                                    <td valign="top">
                                                        <span class="subtitleblueblodtext">
                                                        Tipos
                                                        
                                                        </span>
                                                    </td> 
                                                 </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>                       
                        <tr id="trCONFTYPE" runat="server" style="display: none">
                            <td colspan="7">
                                <table border="0" width="100%" cellpadding="5">
                                    <tr>
                                        <td style="width: 1%;">
                                        </td>
                                        <td align="left" style="width: 25%;" class="blackblodtext" valign="top">
                                            <b>
                                                <%if (Application["Client"].ToString().ToUpper() == "MOJ")
                                          {%>Enable audio/video hearing<%}
                                          else
                                          { %>Habilitar Conferencias Audio/Video<%}%><%--added for FB 1428 Start--%></b> 
                                        <%--<asp:ImageButton ID="Imgeavconf" valign="center" src="image/info.png" runat="server" ToolTip="Default all calls to audio/video type."/>--%>
                                        </td>
                                        <td valign="top" style="width: 25%;" align="left">
                                            <asp:DropDownList ID="lstEnableAudioVideoConference" runat="server" CssClass="alt2SelectFormat">
                                                <asp:ListItem Value="0">No</asp:ListItem>
                                                <asp:ListItem Value="1">Si</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                        <td align="left" style="width: 27%;" class="blackblodtext" rowspan="1" valign="top">
                                            <strong>
                                                <%if (Application["Client"].ToString().ToUpper() == "MOJ")
                                          {%>Enable audio only hearing<%}
                                          else
                                          { %>Habilitar Conferencias solo-Audio<%}%><%--added for FB 1428 Start--%></strong>
                                          <%--<asp:ImageButton ID="Imgeaconf" valign="center"  src="image/info.png" runat="server" ToolTip="Default all calls to audio type."/>--%>
                                        </td>
                                        <td style="width: 20%;" valign="top" align="left">
                                            <asp:DropDownList ID="lstEnableAudioOnlyConference" runat="server" CssClass="alt2SelectFormat">
                                                <asp:ListItem Value="0">No</asp:ListItem>
                                                <asp:ListItem Selected="True" Value="1">Si</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                    </tr>                                   
                                    <tr>
                                        <td style="width: 1%;">
                                        </td>
                                        <td align="left" style="width: 25%;" class="blackblodtext" valign="top">
                                            <strong>
                                                <%if (Application["Client"].ToString().ToUpper() == "MOJ")
                                          {%>Enable room hearing<%}
                                          else
                                          { %>Habilitar Conferencias solo-sala<%}%>
                                          <%--<asp:ImageButton ID="Imgeroomconf" valign="center"  src="image/info.png" runat="server" ToolTip="Default all calls to room type."/></strong>--%>
                                          
                                        </td>
                                        <td valign="top" style="width: 25%;" align="left">
                                            <asp:DropDownList ID="lstEnableRoomConference" runat="server" CssClass="alt2SelectFormat">
                                                <asp:ListItem Value="0">No</asp:ListItem>
                                                <asp:ListItem Selected="True" Value="1">Si</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                        <td align="left" style="width: 27%;" class="blackblodtext" valign="top">
                                            <strong style="align: left">
                                                <%if (Application["Client"].ToString().ToUpper() == "MOJ")
                                          {%>Enable point-to-point hearing<%}
                                          else
                                          { %>Habilitar Conferencias punto-a-punto<%}%><%--added for FB 1428 Start--%></strong>
                                          <%--<asp:ImageButton ID="Imgppconf" valign="center" src="image/info.png" runat="server" ToolTip="Default all calls to point-to-point type."/>--%>
                                        </td>
                                        <td valign="top" style="width: 20%;" align="left">
                                            <%--FB 2430--%>
                                            <asp:DropDownList ID="p2pConfEnabled" runat="server" CssClass="alt2SelectFormat"
                                                onclick="javascript:ChangeEnableSmartP2P();">
                                                <asp:ListItem Value="0">No</asp:ListItem>
                                                <asp:ListItem Value="1">Si</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 1%;">
                                        </td>
                                         <td style="width: 25%;" align="left" class="blackblodtext">
                                            <asp:Label ID="lblEnableSmartP2P" runat="server" Text="Habilitar Punto a Punto inteligente"></asp:Label>
                                            <%--<asp:ImageButton ID="Imgesmartp2p" valign="center"  src="image/info.png" runat="server" ToolTip="Default all calls to point-to-point when only two endpoints."/>--%>
                                        </td>
                                        <td style="width: 25%;" align="left">
                                            <asp:DropDownList ID="lstEnableSmartP2P" runat="server" CssClass="alt2SelectFormat">
                                                <asp:ListItem Value="0" Selected="True" Text="No"></asp:ListItem>
                                                <asp:ListItem Value="1" Text="Si"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                </table>
                           </td>
                        </tr>
                                              
                        <tr>
                            <td colspan="7" align="left" style="height: 49px">
                                <table width="100%">
                                    <tr>
                                         <td width="100%" height="20" align="left">
                                            <table>
                                                <tr> 
                                                    <td>
                                                        <asp:ImageButton ID="img_FEAT" runat="server" ImageUrl="image/loc/nolines_plus.gif"
                                                            Height="25" Width="25" vspace="0" hspace="0" />
                                                     </td>
                                                    <td valign="top">
                                                        <span class="subtitleblueblodtext">
                                                        Caracteristicas
                                                        
                                                        </span>
                                                    </td> 
                                                 </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>            
                        <tr id="trFEAT" runat="server" style="display: none">
                            <td colspan="7">
                                <table border="0" width="100%" cellpadding="5">
                                    <tr>
                                        <td style="width: 1%;">
                                        </td>
                                         <td style="width: 25%;" align="left" class="blackblodtext">
                                            <asp:Label ID="lblEnablePublicConf" runat="server" Text="Habilitar Conferencia P�blica"></asp:Label>
                                            <%--<asp:ImageButton ID="Imgenpublicconf" valign="center" src="image/info.png" runat="server" ToolTip="Public conference feature will be enabled."/>--%>
                                        </td>
                                        <td style="width: 25%;" align="left">
                                            <asp:DropDownList ID="lstEnablePublicConf" runat="server" CssClass="alt2SelectFormat">
                                                <asp:ListItem Value="0" Selected="True" Text="No"></asp:ListItem>
                                                <asp:ListItem Value="1" Text="Si"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                         <td align="left" style="width: 27%;" valign="top" class="blackblodtext">
                                            <b>Habilitar Registracion del participante</b>
                                            <%--<asp:ImageButton ID="Imgeopenfreg" valign="center"  src="image/info.png" runat="server" ToolTip="All public calls will be open for registration based on this switch."/>--%>
                                        </td>
                                        <td valign="top" style="width: 20%;" align="left">
                                            <asp:DropDownList ID="DynamicInviteEnabled" runat="server" CssClass="alt2SelectFormat">
                                                <asp:ListItem Value="1">Si</asp:ListItem>
                                                <asp:ListItem Value="0">No</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                    </tr>        
                                    <tr>
                                        <td style="width: 1%;">
                                        </td>
                                         <td align="left" style="width: 25%;" valign="top" class="blackblodtext">
                                            <b>
                                                <%if (Application["Client"].ToString().ToUpper() == "MOJ")
                                                {%>Default all hearing to public<%}
                                                else
                                                { %>Predeterminar todas las conferencias como P�blicas<%}%><%--added for FB 1428 Start--%></b>
                                                <%--<asp:ImageButton ID="Imgdeconfpublic" valign="center"  src="image/info.png" runat="server" ToolTip="All calls will be default to public/private"/>--%>
                                        </td>
                                        <td valign="top" style="width: 25%;" align="left">
                                            <asp:DropDownList ID="DefaultPublic" runat="server" CssClass="alt2SelectFormat">
                                                <asp:ListItem Value="0">No</asp:ListItem>
                                                <asp:ListItem Value="1">Si</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                         <td style="width: 27%;" align="left" class="blackblodtext">
                                            <asp:Label ID="lblEnableVMR" runat="server" Text="Tipo VMR "></asp:Label>
                                            <%--<asp:ImageButton ID="ImgeVMR" valign="center"  src="image/info.png" runat="server" ToolTip="Enable VMR features for calls"/>--%>
                                        </td>
                                        <td style="width: 20%;" align="left">
                                            <asp:DropDownList ID="lstEnableVMR" runat="server" CssClass="alt2SelectFormat">
                                                <%--FB 2448--%>
                                                <asp:ListItem Value="0" Selected="True" Text="Ninguno"></asp:ListItem>
                                                <asp:ListItem Value="1" Text="Personal"></asp:ListItem>
                                                <asp:ListItem Value="2" Text="Sal�n"></asp:ListItem>
                                                <asp:ListItem Value="3" Text="Externa"></asp:ListItem>
                                                <%--FB 2481--%>
                                            </asp:DropDownList>
                                        </td>
                                      
                                        <td style="width: 1%;">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 1%;"></td>
                                        <td align="left" style="width: 25%;" class="blackblodtext" rowspan="1" valign="top">
                                            <div id="tdMaxParty" runat="server" style="display: none">
                                            <strong>Max. Public VMR Parties</strong>
                                            <%--<asp:ImageButton ID="ImageButton1" valign="center"  src="image/info.png" runat="server" ToolTip="Maximum VMR parties allowed to create conference"/>--%>
                                            </div>
                                        </td>
                                        <td id="Td1" valign="top" style="width: 25%;" align="left">
                                            <div id="tdMaxPartyCount" runat="server" style="display: none">
                                            <asp:TextBox ID="txtMaxVMRParty" runat="server" CssClass="altText" Width="50px" MaxLength="4"> </asp:TextBox>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator6" ValidationGroup="Submit" ControlToValidate="txtMaxVMRParty"
                                                Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="S�lo valores num�ricos."
                                                ValidationExpression="\d+"></asp:RegularExpressionValidator>
                                            </div>
                                        </td>
                                         <%--FB 2609 Start--%>
                                        <td style="width: 1%;">
                                        </td>
                                         <td align="left" style="width: 25%;" class="blackblodtext" rowspan="1" valign="top">
                                            <strong>Meet & Greet Buffer</strong>
                                        </td>
                                        <td id="Td2" valign="top" style="width: 25%;" align="left">
                                            <asp:TextBox ID="txtMeetandGreetBuffer" runat="server" CssClass="altText" Width="50px" MaxLength="4"> </asp:TextBox>(hrs)
                                            <asp:RegularExpressionValidator ID="regmeetandgreet" ValidationGroup="Submit" ControlToValidate="txtMeetandGreetBuffer"
                                                Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="Numeric values only."
                                                ValidationExpression="\d+"></asp:RegularExpressionValidator>
                                        </td>
                                        <%--FB 2609 End--%>
                                    </tr>
                                    <tr>
                                        <td style="width: 1%;">
                                        </td>
                                        <td align="left" class="blackblodtext" style="width: 25%">
                                            <b>
                                                <%if (Application["Client"].ToString().ToUpper() == "MOJ")
                                                {%>Enable recurring hearing<%}
                                                else
                                                { %>Habilitar Conferencias recurrentes<%}%><%--added for FB 1428 Start--%></b>
                                                <%--<asp:ImageButton ID="Imgrecurrconf" valign="center"  src="image/info.png" runat="server" ToolTip="Recurrence feature will be enabled for calls."/>--%>
                                        </td>
                                        <td align="left" style="width: 25%">
                                            <asp:DropDownList ID="RecurEnabled" runat="server" CssClass="alt2SelectFormat">
                                                <asp:ListItem Value="1">Si</asp:ListItem>
                                                <asp:ListItem Value="0">No</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                         <td align="left" class="blackblodtext" style="width: 27%;" rowspan="1" valign="top">
                                            <%--FB 2052 - Start--%>
                                            <strong>Habilitar recurrencia especial</strong>
                                            <%--<asp:ImageButton ID="Imgesplrecurr" valign="baseline"  src="image/info.png" runat="server" ToolTip="Special recurrence will be enabled for calls."/>--%>
                                        </td>
                                        <td valign="top" style="width: 20%;" align="left">
                                            <asp:DropDownList ID="DrpDwnListSpRecur" runat="server" CssClass="alt2SelectFormat">
                                                <asp:ListItem Value="0">No</asp:ListItem>
                                                <asp:ListItem Selected="True" Value="1">Si</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                    </tr>                           
                                    <tr>
                                        <td style="width: 1%;">
                                        </td>
                                        <td align="left" class="blackblodtext" style="width: 25%;" rowspan="1" valign="top">
                                            <strong>Habilitar Conferencia VIP</strong>
                                            <%--<asp:ImageButton ID="Imgvipconf" valign="center"   src="image/info.png" runat="server" ToolTip="VIP mode will be enabled."/>--%>
                                        </td>
                                        <td valign="top" align="left" style="width: 25%;">
                                            <asp:DropDownList ID="DrpVIP" runat="server" CssClass="alt2SelectFormat">
                                                <asp:ListItem Value="0">No</asp:ListItem>
                                                <asp:ListItem Selected="True" Value="1">Si</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                          <td align="left" style="width: 27%;" class="blackblodtext" rowspan="1" valign="top">
                                            <strong>Habilitar Conferencia "Empezar inmediatamente" </strong>
                                            <%--<asp:ImageButton ID="Imgimmconf" valign="center"  src="image/info.png" runat="server" ToolTip="Switch to enable start now conference features."/>--%>
                                        </td>
                                        <td valign="top" align="left" style="width: 20%;">
                                            <asp:DropDownList ID="lstEnableImmediateConference" runat="server" CssClass="alt2SelectFormat">
                                                <asp:ListItem Value="0">No</asp:ListItem>
                                                <asp:ListItem Value="1">Si</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 1%;">
                                        </td>
                                        <td style="width: 25%;" align="left" class="blackblodtext">
                                            <asp:Label ID="lblEnableConfPassword" runat="server" Text="Habilitar la Contrase�a de Conferencia"></asp:Label>
                                            <%--<asp:ImageButton ID="ImgEconfpass" valign="center"  src="image/info.png" runat="server" ToolTip="Enable conference password feature for calls."/>--%>
                                        </td>
                                        <td style="width: 25%;" align="left">
                                            <asp:DropDownList ID="lstEnableConfPassword" runat="server" CssClass="alt2SelectFormat">
                                                <asp:ListItem Value="0" Selected="True" Text="No"></asp:ListItem>
                                                <asp:ListItem Value="1" Text="Si"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                        <td align="left" style="width: 27%;" class="blackblodtext" rowspan="1" valign="top">
                                            <strong>contrasena unica requerida para conferencia</strong>
                                            <%--<asp:ImageButton ID="ImgEuniquepass" valign="center" src="image/info.png" runat="server" ToolTip="Switch will allow to set unique password for calls."/>--%>
                                        </td>
                                        <td valign="top" align="left" style="width: 20%;">
                                            <asp:DropDownList ID="DrpUniquePassword" runat="server" CssClass="alt2SelectFormat">
                                                <asp:ListItem Value="0">No</asp:ListItem>
                                                <asp:ListItem Selected="True" Value="1">Si</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 1%;">
                                        </td>
                                        <td align="left" style="width: 25%;" class="blackblodtext" rowspan="1" valign="top">
                                            <strong>Salas de Audio/Video demonstrando solo-sala</strong>
                                            <%--<asp:ImageButton ID="Imgvidroomconf" valign="center"  src="image/info.png" runat="server" ToolTip="Switch to control Video room display for room only conference."/>--%>
                                        </td>
                                        <td valign="top" style="width: 25%;" align="left">
                                            <asp:DropDownList ID="DrpDwnDedicatedVideo" runat="server" CssClass="alt2SelectFormat">
                                                <asp:ListItem Value="0">No</asp:ListItem>
                                                <asp:ListItem Value="1">Si</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                        <td align="left" style="width: 27%;" class="blackblodtext" rowspan="1" valign="top">
                                            <strong>Salas/Salones Telepresencia demonstrando solo-sala</strong>
                                            <%--<asp:ImageButton ID="Imgtelepresenceroomconf" valign="center" src="image/info.png" runat="server" ToolTip="Switch to control Telepresence room display for room only conference."/>--%>
                                        </td>
                                        <td valign="top" style="width: 20%;" align="left">
                                            <asp:DropDownList ID="DrpDwnFilterTelepresence" runat="server" CssClass="alt2SelectFormat">
                                                <asp:ListItem Value="0">No</asp:ListItem>
                                                <asp:ListItem Selected="True" Value="1">Si</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 1%;">
                                        </td>
                                        <td align="left" style="width: 25%;" class="blackblodtext" rowspan="1" valign="top">
                                            <strong>Habilitar Tipo de Servicio de Sal�n</strong>
                                            <%--<asp:ImageButton ID="Imgeroomser" valign="center"  src="image/info.png" runat="server" ToolTip="Room service type feature can be enabled"/>--%>
                                        </td>
                                        <td valign="top" style="width: 25%;" align="left">
                                            <asp:DropDownList ID="lstEnableRoomServiceType" runat="server" CssClass="alt2SelectFormat">
                                                <asp:ListItem Value="0">No</asp:ListItem>
                                                <asp:ListItem Selected="True" Value="1">Si</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                        <td align="left" style="width: 27%;" class="blackblodtext" valign="top" style="width: 25%">
                                            <strong style="align: left">Habilitar Conferencia horaria de amortiguamiento</strong>
                                            <%--<asp:ImageButton ID="Imgbuffzone" valign="center" src="image/info.png" runat="server" ToolTip="Buffer period can be set to calls."/>--%>
                                            
                                        </td>
                                        <td valign="top" align="left" style="width: 20%">
                                            <asp:DropDownList ID="EnableBufferZone" runat="server" CssClass="alt2SelectFormat"
                                                onclick="javascript:fnBufferOptions()">
                                                <%-- FB 2398 --%>
                                                <asp:ListItem Value="0">No</asp:ListItem>
                                                <asp:ListItem Value="1">Si</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td style="width: 1%;">
                                        </td>
                                    </tr>
                                    <tr id="trBufferOptions" runat="server">
                                        <%-- FB 2398 --%>
                                        <td style="width: 1%"></td>
                                        <td align="left" class="blackblodtext" valign="top" style="width: 25%">
                                            <strong style="align: left">derribar tiempo</strong>
                                            <%--<asp:ImageButton ID="Imgsetuptime" valign="center"  src="image/info.png" runat="server" ToolTip="Default pre start time can be set for all calls."/>--%>
                                        </td>
                                        <td valign="top" align="left" style="width: 25%">
                                            <asp:TextBox ID="txtSetupTime" runat="server" CssClass="altText" Width="50px"> </asp:TextBox>(mins)
                                            <asp:RangeValidator ID="rangesetuptime" SetFocusOnError="true" Type="Integer" MinimumValue="0"
                                                MaximumValue="60" Display="Dynamic" ControlToValidate="txtSetupTime" runat="server"
                                                ErrorMessage="No est� permitido un tiempo de configuraci�n de m�s de 60 minutos"></asp:RangeValidator>
                                            <asp:RegularExpressionValidator ID="RegtxtSetupTime" ValidationGroup="Submit" ControlToValidate="txtSetupTime"
                                                Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="S�lo valores num�ricos."
                                                ValidationExpression="\d+"></asp:RegularExpressionValidator>
                                        </td>
                                        <td style="width: 1%"></td>
                                        <td align="left" class="blackblodtext" rowspan="1" valign="top" width="27%">
                                            <strong>Hora de desmontaje</strong>
                                             <%--<asp:ImageButton ID="Imgteardowntime" valign="center"  src="image/info.png" runat="server" ToolTip="Default post conference time can be set for all calls"/>--%>
                                         
                                        </td>
                                        <td style="height: 33px; width: 20%" align="left" valign="top">
                                            <asp:TextBox ID="txtTearDownTime" runat="server" CssClass="altText" Width="50px"> </asp:TextBox>(mins)
                                            <asp:RangeValidator ID="Rangeteardowntime" SetFocusOnError="true" Type="Integer"
                                                MinimumValue="0" MaximumValue="60" Display="Dynamic" ControlToValidate="txtTearDownTime"
                                                runat="server" ErrorMessage="No est� permitido un tiempo de desmontaje de m�s de 60 minutos"></asp:RangeValidator>
                                            <asp:RegularExpressionValidator ID="RegtxtTearDownTime" ValidationGroup="Submit"
                                                ControlToValidate="txtTearDownTime" Display="dynamic" runat="server" SetFocusOnError="true"
                                                ErrorMessage="S�lo valores num�ricos." ValidationExpression="\d+"></asp:RegularExpressionValidator>
                                        </td>
                                        <td width="1%"></td>
                                    </tr>
                                    <tr id="trMCUBufferOptions" runat="server">
                                        <%-- FB 2440 --%>
                                        <td style="width: 1%"></td>
                                        <td align="left" class="blackblodtext" valign="top" style="width: 25%">
                                            <strong style="align: left">MCU Pre-Start Time</strong>
                                             <%--<asp:ImageButton ID="Imgmcuprestart" valign="center"  src="image/info.png" runat="server" ToolTip="Default pre lauch calls to MCU"/>--%>
                                        </td>
                                        <td valign="top" align="left" style="width: 25%">
                                            <asp:TextBox ID="txtMCUSetupTime" runat="server" CssClass="altText" Width="50px"> </asp:TextBox>(mins)
                                            <asp:RangeValidator ID="RangeValidator1" SetFocusOnError="true" Type="Integer" MinimumValue="-15"
                                                MaximumValue="15" Display="Dynamic" ControlToValidate="txtMCUSetupTime" runat="server"
                                                ErrorMessage="No est� autorizado una duraci�n del Previo MCU mayor de 15 minutos"></asp:RangeValidator>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" ValidationGroup="Submit"
                                                ControlToValidate="txtMCUSetupTime" Display="dynamic" runat="server" SetFocusOnError="true"
                                                ErrorMessage="S�lo valores num�ricos." ValidationExpression="^-{0,1}\d+$"></asp:RegularExpressionValidator>
                                        </td>
                                        <td style="width: 1%"></td>
                                        <td align="left" class="blackblodtext" rowspan="1" valign="top" width="27%">
                                            <strong>MCU Pre-End Time</strong>
                                             <%--<asp:ImageButton ID="Imgmcuprend" valign="center"  src="image/info.png" runat="server" ToolTip="Default post conference calls to MCU"/>--%>
                                        </td>
                                        <td style="height: 33px; width: 20%" align="left" valign="top">
                                            <asp:TextBox ID="txtMCUTearDownTime" runat="server" CssClass="altText" Width="50px"> </asp:TextBox>(mins)
                                            <asp:RangeValidator ID="RangeValidator2" SetFocusOnError="true" Type="Integer" MinimumValue="-15"
                                                MaximumValue="15" Display="Dynamic" ControlToValidate="txtMCUTearDownTime" runat="server"
                                                ErrorMessage="No est� autorizada una hora final del Previo MCU superior a 15 minutos"></asp:RangeValidator>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator5" ValidationGroup="Submit"
                                                ControlToValidate="txtMCUTearDownTime" Display="dynamic" runat="server" SetFocusOnError="true"
                                                ErrorMessage="S�lo valores num�ricos." ValidationExpression="^-{0,1}\d+$"></asp:RegularExpressionValidator>
                                        </td>
                                        <td width="1%"></td>
                                    </tr> 
                                     <%--FB 2637 Starts--%>
                                    <tr>
                                         <td style="width: 1%"></td>
                                            <td align="left" class="blackblodtext" valign="top" style="width: 25%">
                                            <b>
                                            <%if (Application["Client"].ToString().ToUpper() == "MOJ")
                                            {%>Auto-accept modified hearing<%}
                                            else
                                            { %>Auto-Approve Conference Modifications<%}%><%--added for FB 1428 Start--%></b>
                                            <%--<asp:ImageButton ID="Imgaamodconf" valign="center" src="image/info.png" runat="server" ToolTip="Switch to enable auto-accept modified conference."/>--%>
                                        </td>
                                        <td align="left" style="width: 25%" valign="top">
                                            <asp:DropDownList ID="AutoAcpModConf" runat="server" CssClass="alt2SelectFormat">
                                                <asp:ListItem Value="1">Si</asp:ListItem>
                                                <asp:ListItem  Value="0">No</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>                                        
                                       <td></td>
                                        <td align="left" class="blackblodtext" valign="top" width="27%">
                                            <strong>Alert on Tier1 Selection</strong>
                                            </td>
                                            <td valign="top" style="width: 25%;" align="left">
                                            <asp:ListBox runat="server" ID="lstTier1" CssClass="altSelectFormat" DataTextField="Name"
                                             DataValueField="ID" Rows="6" SelectionMode="Multiple"></asp:ListBox>
                                            </td>
                                        </tr>                         
                                    <tr style="display:none">
                                        <td style="width: 2%;"></td>
                                        <td width="25%" align="left" class="blackblodtext">
                                            <b>
                                                <%if (Application["Client"].ToString().ToUpper() == "MOJ")
                                                {%>Enable Hearing Real-time Display?<%}
                                                else
                                                { %>Enable Conference Monitoring<%}%><%--added for FB 1428 Start--%></b>
                                        </td>
                                        <td align="left" style="width: 25%">
                                            <asp:DropDownList ID="RealtimeType" runat="server" CssClass="alt2SelectFormat">
                                            <asp:ListItem Value="1">Si</asp:ListItem>
                                            <asp:ListItem Value="0">No</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td style="width: 1%"></td>
                                        <td></td>
                                        <td></td>
                                        <td width="2%"></td>
                                    </tr>
                                     <%--FB 2637 Ends--%>
                                      <%--FB 2636 Starts --%>
                                    <tr>
                                        <td style="width: 1%;"></td>
                                        <td align="left" style="width: 27%;" class="blackblodtext" rowspan="1" valign="top">
                                            <strong>Enable Dial Plan</strong>                                           
                                        </td>
                                        <td valign="top" style="width: 20%;" align="left">
                                            <asp:DropDownList ID="DrpEnableE164DialPlan" runat="server" CssClass="alt2SelectFormat">
                                                <asp:ListItem Value="0">No</asp:ListItem>
                                                <asp:ListItem Selected="True" Value="1">Yes</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>   
                                        <td style="width: 1%;">
                                        </td> 
                                    </tr>
                                    <%--FB 2636 Ends --%>
                                </table>
                           </td>
                        </tr>
                        
                        <tr>
                            <td colspan="7" align="left" style="height: 49px">
                                <table width="100%" border="0">
                                    <tr>
                                        <td width="100%" height="20" align="left">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:ImageButton ID="img_AUD" runat="server" ImageUrl="image/loc/nolines_plus.gif"
                                                            Height="25" Width="25" vspace="0" hspace="0" />
                                                    </td>
                                                    <td valign="top">
                                                        <span class="subtitleblueblodtext">Audio Add-On</span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr id="trAUD" runat="server" style="display: none">
                            <td colspan="7">
                                <table border="0" width="100%" cellpadding="5">
                                    <tr id="trUser" runat="server">
                                     <td style="width: 1%;"></td>
                                        <td style="height: 33px; width: 25%" align="left" class="blackblodtext">
                                            <asp:Label ID="LblExc" runat="server" Text="Habilitar C�digo de Conferencia"></asp:Label>
                                            <%--<asp:ImageButton ID="Imgeconfcode" valign="center" src="image/info.png" runat="server" ToolTip="Switch to enable conference code will be visible in Audio/Video tab for audio bridge user."/>--%>
                                        </td>
                                        <td style="width: 25%;" align="left">
                                            <asp:DropDownList ID="DrpConfcode" runat="server" Width="125px" CssClass="altSelectFormat">
                                                <asp:ListItem Value="0" Selected="True" Text="No"></asp:ListItem>
                                                <asp:ListItem Value="1" Text="Si"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td style="width: 1%;"></td>
                                        <td style="width: 27%;" align="left" class="blackblodtext">
                                            <asp:Label ID="LblDom" runat="server" Text="Habilitar PIN de L�der"></asp:Label>
                                            <%--<asp:ImageButton ID="Imgleaderpin" valign="center" src="image/info.png" runat="server" ToolTip="Switch to enable leaderpin will be visible in Audio/Video tab for audio bridge user."/>--%>
                                        </td>
                                        <td style="width: 20%;" align="left">
                                            <asp:DropDownList ID="DrpLedpin" runat="server" Width="125px" CssClass="altSelectFormat">
                                                <asp:ListItem Value="0" Selected="True" Text="No"></asp:ListItem>
                                                <asp:ListItem Value="1" Text="Si"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td style="width: 1%;"></td>
                                    </tr>
                                    <tr id="tr1" runat="server">
                                        <td style="width: 1%;"></td>
                                        <td style="width: 25%;" align="left" class="blackblodtext">
                                            <asp:Label ID="Label1" runat="server" Text="Habilitar la configuracion de la conferencia Audio/Video"></asp:Label>
                                            <%--<asp:ImageButton ID="ImgEadvavparams"  valign="center" src="image/info.png" runat="server" ToolTip="Switch to enable advanced Audio/video params will be visible in Audio/video tab."/>--%>
                                        </td>
                                        <td style="width: 25%;" align="left">
                                            <asp:DropDownList ID="Drpavprm" runat="server" Width="125px" CssClass="altSelectFormat">
                                                <asp:ListItem Value="0" Selected="True" Text="No"></asp:ListItem>
                                                <asp:ListItem Value="1" Text="Si"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td style="width: 1%;"></td>
                                        <td style="width: 27%;" align="left" class="blackblodtext">
                                            <asp:Label ID="Label2" runat="server" Text="mostrar parametros de audio"></asp:Label>
                                            <%--<asp:ImageButton ID="Imgeaudioparams" valign="center" src="image/info.png" runat="server" ToolTip="Switch to enable audio params will be visible in Audio/Video tab."/>--%>
                                
                                        </td>
                                        <td style="width: 20%;" align="left">
                                            <asp:DropDownList ID="DrpAudprm" runat="server" Width="125px" CssClass="altSelectFormat">
                                                <asp:ListItem Value="0" Selected="True" Text="No"></asp:ListItem>
                                                <asp:ListItem Value="1" Text="Si"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td style="width: 1%;"></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 1%;"></td>
                                        <td style="width: 25%;" align="left" class="blackblodtext">
                                            <asp:Label ID="Label3" runat="server" Text="Habilitar las puentes de Audio"></asp:Label>
                                            <%--<asp:ImageButton ID="Imgabridge" valign="center"  src="image/info.png" runat="server" ToolTip="Switch to enable audio bridge field visible in menu list."/>--%>
                                        </td>
                                        <td style="width: 25%;" align="left">
                                            <asp:DropDownList ID="lstEnableAudioBridges" runat="server" Width="125px" CssClass="altSelectFormat">
                                                <asp:ListItem Value="0" Selected="True" Text="No"></asp:ListItem>
                                                <asp:ListItem Value="1" Text="Si"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td style="width: 1%;"></td>
                                        <td style="width: 27%;" align="left" class="blackblodtext">
                                            <asp:Label ID="Label4" runat="server" Text="mostrar parametros de sala/salon"></asp:Label>
                                            <%--<asp:ImageButton ID="ImgEroomparams" valign="center" src="image/info.png" runat="server" ToolTip="Switch to enable room params visible for a conference."/>--%>
                                        </td>
                                        <td style="width: 20%;" align="left">
                                            <asp:DropDownList ID="lstRoomprm" runat="server" Width="125px" CssClass="altSelectFormat">
                                                <asp:ListItem Value="0" Selected="True" Text="No"></asp:ListItem>
                                                <asp:ListItem Value="1" Text="Si"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td style="width: 1%;"></td>
                                    </tr>
                                    <%--FB 2571 Start--%>
                                    <tr>
                            <td style="width: 1%;"></td>
                            <td align="left" style="width: 25%;" class="blackblodtext" rowspan="1" valign="top">
                                <strong>Habilitar FECC</strong>
                                <%--<asp:ImageButton ID="ImgeFECC" valign="center"  src="image/info.png" runat="server" ToolTip="Switch to enable FECC checkbox while conference creation."/>--%>
                            </td>
                            <td valign="top" style="width: 25%;" align="left">
                                <asp:DropDownList ID="lstenableFECC" runat="server" Width="125px" CssClass="alt2SelectFormat" onclick="javascript:fnFeccOptions()"><%--FB 2571--%>
                                    <asp:ListItem Value="0">Hide</asp:ListItem>
                                    <asp:ListItem Selected="True" Value="1">Show</asp:ListItem>
                                    <asp:ListItem Value="2">None</asp:ListItem><%--FB 2571--%>
                                </asp:DropDownList>
                            </td>
                            <td style="width: 1%;"></td>
                            <td id ="DefaultFECC" runat="server" align="left" style="width: 27%;" class="blackblodtext" valign="top">
                                <strong style="left">FECC por defecto</strong>
                                <%--<asp:ImageButton ID="ImgdFECC" valign="center" src="image/info.png" runat="server" ToolTip="Switch to make default Selection for FECC while conference creation."/>--%>
                            </td>
                            <td id ="DefaultFECCoptions" runat="server" valign="top" align="left" style="width: 20%">
                                <asp:DropDownList ID="lstdefaultFECC" runat="server" Width="125px" CssClass="alt2SelectFormat">
                                    <asp:ListItem Value="0">No</asp:ListItem>
                                    <asp:ListItem Selected="True" Value="1">Si</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td style="width: 1%;"></td>
                        </tr>
                                    <%--FB 2571 End--%>
                                </table>
                            </td>
                        </tr>
            
                        <tr>
                        <td colspan="7" align="left" style="height: 49px">
                            <table width="100%" border="0">
                        <tr>
                            <td width="100%" height="20" align="left">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:ImageButton ID="img_CONFMAIL" runat="server" ImageUrl="image/loc/nolines_plus.gif"
                                                Height="25" Width="25" vspace="0" hspace="0" />
                                        </td>
                                        <td valign="top">
                                            <span class="subtitleblueblodtext">Mail Options</span>
                                            
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                        </td>
                        </tr>
                        <tr id="trCONFMAIL" runat="server" style="display: none">
                <td colspan="7">
                    <table border="0" width="100%" cellpadding="5">
                        <tr>
                            <td style="width: 1%;"></td>
                            <td style="width: 25%;" align="left" class="blackblodtext">
                                <asp:Label ID="Label9" runat="server" Text="Enviar Confirmacion de correo-e"></asp:Label>
                                <%--<asp:ImageButton ID="Imgsendconfirmemail" valign="center" src="image/info.png" runat="server" ToolTip="Switch to enable send confirmation emails to host only or all."/>--%>
                            </td>
                            <td style="width: 25%;" align="left">
                                <asp:DropDownList ID="lstSendConfirmationEmail" runat="server" CssClass="alt2SelectFormat">
                                    <asp:ListItem Value="0" Selected="True" Text="All"></asp:ListItem>
                                    <asp:ListItem Value="1" Text="Host"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td style="width: 1%;"></td>
                            <td style="width: 27%;" align="left" class="blackblodtext">
                                <asp:Label ID="Label10" runat="server" Text="Enviar aviso correo-e a MCU "></asp:Label>
                                <%--<asp:ImageButton ID="Imgsendmcuemail" valign="center" src="image/info.png" runat="server" ToolTip="Switch to send mcu alert mails to host  only or to all."/>--%>
                            </td>
                            <td style="width: 20%;" align="left">
                                <asp:DropDownList ID="lstMcuAlert" runat="server" CssClass="alt2SelectFormat">
                                    <asp:ListItem Value="0" Selected="True" Text="Ninguno"></asp:ListItem>
                                    <asp:ListItem Value="1" Text="organizador"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="Admin MCU"></asp:ListItem>
                                    <asp:ListItem Value="3" Text="Ambos"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td style="width: 1%;"></td>
                        </tr>
                         <%--FB 2631--%>
                         <tr>
                            <td style="width: 1%"></td>
                            <td align="left" class="blackblodtext" rowspan="1" valign="top" width="25%">
                                <strong>Enable Room Admin Details</strong>
                                
                            </td>
                            <td style="height: 33px; width: 25%" align="left" valign="top">
                                <asp:DropDownList ID="lstEnableRoomAdminDetails" runat="server" CssClass="alt2SelectFormat">
                                    <asp:ListItem Value="0" Selected="True" Text="No"></asp:ListItem>
                                    <asp:ListItem Value="1" Text="Yes"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td width="1%"></td>
                            <td style="width: 27%;" align="left" class="blackblodtext">&nbsp; </td>
                            <td style="width: 20%;" align="left">
                               
                            </td>
                         </tr>
                        <tr>
                            <td style="width: 1%;"></td>
                            <td align="left" style="width: 25%;" class="blackblodtext" rowspan="1" valign="top">
                                <strong>Enviar archivos adjuntos a dominios externos</strong>
                                <%--<asp:ImageButton ID="Imgsendatteparty" valign="center" src="image/info.png" runat="server" ToolTip="Switch to control mail attachment for external party."/>--%>
                            </td>
                            <td valign="top" style="width: 25%;" align="left">
                                <asp:DropDownList ID="DrpDwnAttachmnts" runat="server" CssClass="alt2SelectFormat">
                                    <asp:ListItem Value="0">No</asp:ListItem>
                                    <asp:ListItem Selected="True" Value="1">Si</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td style="width: 1%"></td>
                            <td style="width: 27%;" align="left" class="blackblodtext">
                                <asp:Label ID="Label8" runat="server" Text="Mostrar Tiempo local en sitios web en correo-e"></asp:Label>
                                <%--<asp:ImageButton ID="Imgeconftimel" valign="center" src="image/info.png" runat="server" ToolTip="Switch to enable conference time in locations details in mails."/>--%>
                            </td>
                            <td style="width: 20%;" align="left">
                                <asp:DropDownList ID="lstEnableConfTZinLoc" runat="server" CssClass="alt2SelectFormat">
                                    <asp:ListItem Value="0" Selected="True" Text="No"></asp:ListItem>
                                    <asp:ListItem Value="1" Text="Si"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td style="width: 1%;"></td>
                        </tr>
                        <tr>
                            <td style="width: 1%;"></td>
                            <td style="width: 25%;" align="left" class="blackblodtext">
                                <asp:Label ID="Label5" runat="server" Text="Mostrar punto final en correo-e"></asp:Label>
                                <%--<asp:ImageButton ID="Imgeendpoint" valign="center" src="image/info.png" runat="server" ToolTip="Switch to enable endpoints details in emails."/>--%>
                            </td>
                            <td style="width: 25%;" align="left">
                                <asp:DropDownList ID="lstEPinMail" runat="server" CssClass="alt2SelectFormat">
                                    <asp:ListItem Value="0" Selected="True" Text="No"></asp:ListItem>
                                    <asp:ListItem Value="1" Text="Si"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td style="width: 1%;"></td>
                            <%--FB 2610 Starts--%>
                            <td style="width: 27%;" align="left" class="blackblodtext">
                                <asp:Label ID="lblShwBrdge" runat="server" Text="Show Bridge Ext # in Email"></asp:Label> 
                            </td>                                
                            <td style="width: 20%;" align="left">
                                <asp:DropDownList ID="drpBrdgeExt" runat="server" CssClass="alt2SelectFormat">
                                    <asp:ListItem Value="0" Selected="True" Text="No"></asp:ListItem>
                                    <asp:ListItem Value="1" Text="Si"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <%--FB 2610 Ends--%>
                        </tr>
                        <%--FB 2632 Starts--%>
                        <tr>
                            <td style="width: 1%;"></td>
                            <td style="width: 25%;" align="left" class="blackblodtext">
                                <asp:Label ID="lblCngSupport" runat="server" Text="Enable Concierge Support in Email"></asp:Label>
                            </td>
                            <td style="width: 25%;" align="left">
                                <asp:DropDownList ID="drpCngSupport" runat="server" CssClass="alt2SelectFormat">
                                    <asp:ListItem Value="0" Selected="True" Text="No"></asp:ListItem>
                                    <asp:ListItem Value="1" Text="si"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td></td>
                            <%--FB 2419--%>
                            <td style="width: 27%;" align="left" class="blackblodtext">
                                <asp:Label ID="Label6" runat="server" Text="Enable Accept/Decline Links in Invitations"></asp:Label>
                                <%--<asp:ImageButton ID="Imgeaccdec" valign="center" src="image/info.png" runat="server" ToolTip="Switch to enable accept/decline link in confirmation mails."/>--%>
                            </td>
                            <td style="width: 20%;" align="left">
                                <asp:DropDownList ID="lstAcceptDecline" runat="server" CssClass="alt2SelectFormat">
                                    <asp:ListItem Value="0" Selected="True" Text="No"></asp:ListItem>
                                    <asp:ListItem Value="1" Text="Si"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td style="width: 1%;"></td>
                        </tr>
                        
                        <tr>
                            <td style="width: 1%"></td>
                            <td align="left" class="blackblodtext" valign="top" style="width: 25%">
                                <strong style="align: left">Incluir en correo-e</strong>
                            </td>
                            <td valign="top" align="left" style="width: 25%">
                                <table>
                                    <tr>
                                        <td>
                                            <input id="chkOnSiteAVSupport" type="checkbox" runat="server" />
                                            <strong style="align: left">Soporte A/V sobre el terreno</strong>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input id="chkConciergeMonitoring" type="checkbox" runat="server"  />
                                            <strong style="align: left">Concierge Monitoring</strong>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td style="width: 1%"></td>
                            <td valign="top" align="left" style="width: 25%">
                            <table>
                                    <tr>
                                        <td>
                                            <input id="chkMeetandGreet" type="checkbox" runat="server"  />
                                            <strong style="align: left">reuni�n informal</strong>
                                        </td>
                                    </tr><tr>
                                        <td>
                                            <input id="chkDedicatedVNOCOperator" type="checkbox" runat="server"  />
                                            <strong style="align: left">operador VNOC dedicado</strong>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <%--FB 2632 Ends--%>
                        <tr>
                            <td style="width: 1%;"></td>
                            <td align="left" style="width: 25%;" class="blackblodtext" rowspan="1" valign="top">
                                <strong>Enviar Ical a invitados</strong>
                                <%--<asp:ImageButton ID="Imginviteeical" valign="center" src="image/info.png" runat="server" ToolTip="Switch to send ical to parties when conference is scheduled."/>--%>
                            </td>
                            
                            <td valign="top" align="left" style="width: 25%;">
                                <asp:DropDownList ID="DrpListIcal" runat="server" CssClass="alt2SelectFormat">
                                    <asp:ListItem Value="0">No</asp:ListItem>
                                    <asp:ListItem Selected="True" Value="1">Si</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td align="right" valign="top" style="width: 1%;">
                                &nbsp;
                            </td>
                            <td align="left" style="width: 27%;" class="blackblodtext" rowspan="1" valign="top">
                                <strong>Enviar Ical de aprobaci�n</strong>
                                <%--<asp:ImageButton ID="Imgapproical" valign="center" src="image/info.png" runat="server" ToolTip="Switch to send ical to application when conference is scheduled from outlook."/>--%>
                            </td>
                            <td valign="top" align="left" style="width: 20%;">
                                <asp:DropDownList ID="DrpAppIcal" runat="server" CssClass="alt2SelectFormat">
                                    <asp:ListItem Value="0">No</asp:ListItem>
                                    <asp:ListItem Selected="True" Value="1">Si</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td style="width: 1%;"></td>
                        </tr>
                        <tr>
                            <td style="width: 1%"></td>
                            <td align="left" class="blackblodtext" valign="top" style="width: 25%">
                                <strong style="align: left">Enviar Recordatorios</strong>
                                <%--<asp:ImageButton ID="Imgsendremain" valign="center" src="image/info.png" runat="server" ToolTip="Remainder mail options for participants."/>--%>
                            </td>
                            <td valign="top" align="left" style="width: 25%">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:CheckBox ID="WklyChk" runat="server" />
                                            <strong style="align: left">1 Semana</strong>
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="DlyChk" runat="server" />
                                            <strong style="align: left">1 d�a</strong>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:CheckBox ID="HourlyChk" runat="server" />
                                            <strong style="align: left">1 Hora</strong>
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="MinChk" runat="server" />
                                            <strong style="align: left">15 Minutos</strong>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td style="width: 1%"></td>
                            <td colspan="3" style="width: 48%" rowspan="1">
                                <%-- FB 2440 --%>
                                <table id="tblForceMCUBuffer" runat="server" width="100%" style="display: none">
                                    <tr id="trForceMCUBuffer" style="display: none;" runat="server">
                                        <td align="left" class="blackblodtext" valign="top" style="width: 21%">
                                            <strong style="align: left">Forzar el inicio y final del Previo MCU</strong>
                                        </td>
                                        <td valign="top" align="left" style="width: 33%">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <asp:DropDownList ID="DrpForceMCUBuffer" runat="server" CssClass="alt2SelectFormat">
                                                <asp:ListItem Value="1">Si</asp:ListItem>
                                                <asp:ListItem Value="0">No</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 1%"></td>
                            <td align="left" class="blackblodtext" rowspan="1" valign="top" width="25%">
                                <strong>ical solicita cuenta de correo</strong>
                                <%--<asp:ImageButton ID="Imgicalreqemail" valign="center" src="image/info.png" runat="server" ToolTip="Ical requestor mail for CTP room in  ical invitations."/>--%>
                            </td>
                            <td style="height: 33px; width: 25%" align="left" valign="top">
                                <asp:TextBox ID="txtiCalEmailId" runat="server" CssClass="altText" Width="150px"
                                    MaxLength="512"></asp:TextBox><br />
                                <asp:RegularExpressionValidator ID="regTestemail" runat="server" ControlToValidate="txtiCalEmailId"
                                    ErrorMessage="Direcci�n de correo electr�nico Inv�lida" Display="dynamic" ValidationExpression="^[a-zA-Z()^][()^\w\.-]*[a-zA-Z0-9()^]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"></asp:RegularExpressionValidator>
                                <asp:RegularExpressionValidator ID="iCalReqValid2" ControlToValidate="txtiCalEmailId"
                                    Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ; ? | = ! ` , [ ] { } :<br> # $ ~ y &#34; son caracteres no v�lidos.."
                                    ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=:#$%&'~]*$"></asp:RegularExpressionValidator>
                            </td>
                            <td width="1%"></td>
                             
                         </tr>
                    </table>
                </td>
            </tr>  
            
                    </table>
                </td>
            </tr>
                       
            <tr>
                <td colspan="7" align="left" style="height: 49px">
                    <table width="100%" border="0">
                        <tr>
                            <td width="100%" height="20" align="left">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:ImageButton ID="img_EPT" runat="server" ImageUrl="image/loc/nolines_plus.gif"
                                                Height="25" Width="25" vspace="0" hspace="0" />
                                        </td>
                                        <td valign="top">
                                            <span class="subtitleblueblodtext">Configuracion de punto final</span>
                                            
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>           
            <tr id="trEPT" runat="server" style="display: none">
                <td colspan="7">
                    <table border="0" width="100%" cellpadding="5">
                        <tr>
                            <td style="width: 1%;"></td>
                             <td align="left" style="width: 25%;" class="blackblodtext" rowspan="1" valign="top">
                                <strong>Habilitar MCU asignado en punto final</strong>
                                <%--<asp:ImageButton ID="ImgeassMcu" valign="center"  src="image/info.png" runat="server" ToolTip="Switch to enable must select mcu from assigned to mcu dropdownlist to create a new endpoint."/>--%>
                             </td>
                             <td valign="top" align="left" style="width: 25%;">
                                <asp:DropDownList ID="DrpDwnListAssignedMCU" runat="server" CssClass="alt2SelectFormat">
                                    <asp:ListItem Value="0">No</asp:ListItem>
                                    <asp:ListItem Selected="True" Value="1">Si</asp:ListItem>
                                </asp:DropDownList>
                             </td>
                            <td style="width: 1%;"></td>
                            <td align="left" valign="top" class="blackblodtext" style="width: 27%;">
                                <b>Enable ISDN Dial-Out</b>
                                <%--<asp:ImageButton ID="Imgedialout"  valign="center" src="image/info.png" runat="server" ToolTip="Switch to enable dial out option in response screen."/>--%>
                            </td>
                            <td valign="top" align="left" style="width: 20%;">
                                <asp:DropDownList ID="DialoutEnabled" runat="server" CssClass="alt2SelectFormat">
                                    <asp:ListItem Value="0">No</asp:ListItem>
                                    <asp:ListItem Value="1">Si</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td style="width: 1%;"></td>
                        </tr>
                    </table>
                </td>
            </tr>             
            
            <tr id="trOnfly" runat="server">
                <td colspan="7" align="left" style="height: 49px">
                    <table width="100%" border="0">
                        <tr>
                            <td width="100%" height="20" align="left">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:ImageButton ID="img_FLY" runat="server" ImageUrl="image/loc/nolines_plus.gif"
                                                Height="25" Width="25" vspace="0" hspace="0" />
                                        </td>
                                        <td valign="top">
                                            <span class="subtitleblueblodtext">On-the-Fly Room Creation</span>
                                            
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>           
            <tr id="trFLY" runat="server" style="display: none">
                <td colspan="7">
                    <table border="0" width="100%" cellpadding="5">
                        <tr>
                            <td style="width: 1%"></td>
                            <td align="left" class="blackblodtext" style="width: 25%">
                                <b>Nivel superior</b> <span class="reqfldText">*</span>
                                <%--<asp:ImageButton ID="Imgtoptier" valign="center" src="image/info.png" runat="server" ToolTip="Default top tier for on the fly rooms."/>--%>
                            </td>
                            <td style="width: 25%" align="left" valign="top">
                                <asp:DropDownList ID="lstTopTier" Width="120px" DataTextField="Name" DataValueField="ID"
                                    runat="server" CssClass="altSelectFormat" OnSelectedIndexChanged="UpdateMiddleTiers"
                                    AutoPostBack="true" onchange="javascript:DataLoading(1)">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="reqTopTier" runat="server" ControlToValidate="lstTopTier"
                                    Display="dynamic" CssClass="lblError" SetFocusOnError="true" Text="Necesario"
                                    ValidationGroup="Submit"></asp:RequiredFieldValidator>
                            </td>
                            <td style="width: 1%"></td>
                            <td align="left" class="blackblodtext" style="width: 27%">
                                <b>Nivel medio</b><span class="reqfldText">*</span>
                                <%--<asp:ImageButton ID="Imgmiddletier" valign="center" src="image/info.png" runat="server" ToolTip="Default middle tier for on the fly rooms."/>--%>
                            </td>
                            <td style="width: 20%" align="left" valign="top">
                                <asp:DropDownList ID="lstMiddleTier" Width="120px" DataTextField="Name" DataValueField="ID"
                                    runat="server" CssClass="altSelectFormat">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="reqMiddleTier" runat="server" ControlToValidate="lstMiddleTier"
                                    Display="dynamic" CssClass="lblError" SetFocusOnError="true" Text="Necesario"
                                    ValidationGroup="Submit"></asp:RequiredFieldValidator>
                            </td>
                            <td style="width: 1%"></td>
                        </tr>
                    </table>
                </td>
            </tr>    
            
            <tr>
                <td colspan="7" align="left" style="height: 49px">
                    <table width="100%" border="0">
                        <tr>
                            <td width="100%" height="20" align="left">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:ImageButton ID="img_AUTO" runat="server" ImageUrl="image/loc/nolines_plus.gif"
                                                Height="25" Width="25" vspace="0" hspace="0" />
                                        </td>
                                        <td valign="top">
                                            <span class="subtitleblueblodtext">demonstracion de disposicion de video por defecto</span>
                                        </td>
                                     </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trAUTO" runat="server" style="display: none">
                <td colspan="7" style="padding-bottom: 0px; padding-left: 0px; padding-right: 0px;
                    padding-top: 0px;">
                    <table border="0" width="100%" cellpadding="1">
                        <tr>
                            <td style="width: 1%">
                            </td>
                            <td align="left" class="blackblodtext" style="width: 25%">
                                Polycom MGC &nbsp;&nbsp;<asp:Image ID="Image6" src="image/info.png" runat="server"
                                    ToolTip="Polycom RMX o Polycom MGC MCU a Polycom RPX400 Telepresencia para un Polycom HDX o Polycom VSX." />
                            </td>
                            <td align="left" style="width: 25%">
                                <asp:Image ID="imgLayoutMapping1" runat="server" />
                                <input style="vertical-align:10px" id="butLayoutMapping1" type="button" runat="server" name="ConfLayoutSubmit"
                                    value="Cambiar" class="altShortBlueButtonFormat" onclick="javascript: changeLayout('Poly2MGC',butLayoutMapping1,1);" />
                            </td>
                            <td style="width: 1%">
                            </td>
                            <td align="left" class="blackblodtext" style="width: 27%">
                                Polycom RMX &nbsp;&nbsp;<asp:Image ID="Image1" src="image/info.png" runat="server"
                                    ToolTip="La llamada conecta desde Polycom RMX hasta Polycom OTX Telepresencia y hasta un Polycom HDX o Polycom VSX." />
                            </td>
                            <td align="left" style="width: 20%">
                                <asp:Image ID="imgLayoutMapping2" runat="server" />
                                <input style="vertical-align:10px" id="butLayoutMapping2" type="button" runat="server" name="ConfLayoutSubmit"
                                    value="Cambiar" class="altShortBlueButtonFormat" onclick="javascript: changeLayout('Poly2RMX',butLayoutMapping2,2);" />
                            </td>
                            <td style="width: 1%">
                            </td>
                        </tr>
                        <tr style="visibility: hidden">
                            <td>
                            </td>
                            <td align="left" class="blackblodtext">
                                CTMS to Cisco
                            </td>
                            <td>
                                <asp:Image ID="imgLayoutMapping3" runat="server" />
                                <input style="vertical-align:10px" id="butLayoutMapping3" type="button" runat="server" name="ConfLayoutSubmit"
                                    value="Cambiar" class="altShortBlueButtonFormat" onclick="javascript: changeLayout('CTMS2Cisco',butLayoutMapping3,1);" />
                            </td>
                            <td>
                            </td>
                            <td align="left" class="blackblodtext">
                                CTMS to Polycom
                            </td>
                            <td>
                                <asp:Image ID="imgLayoutMapping4" runat="server" />
                                <input style="vertical-align:10px" id="butLayoutMapping4" type="button" runat="server" name="ConfLayoutSubmit"
                                    value="Cambiar" class="altShortBlueButtonFormat" onclick="javascript: changeLayout('CTMS2Poly',butLayoutMapping4,2);" />
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            <tr>
                <td colspan="7" align="left" style="height: 49px">
                    <table width="100%" border="0">
                        <tr>
                            <td width="100%" height="20" align="left">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:ImageButton ID="img_SYS" runat="server" ImageUrl="image/loc/nolines_plus.gif"
                                                Height="25" Width="25" vspace="0" hspace="0" />
                                        </td>
                                        <td valign="top">
                                            <span class="subtitleblueblodtext">General Options</span>
                                            
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>           
            <tr id="trSYS" runat="server" style="display: none">
                <td colspan="7">
                    <table border="0" width="100%" cellpadding="5">
                        <tr>
                            <td style="width: 1%"></td>
                           <td align="left" style="width: 25%" class="blackblodtext" rowspan="1" valign="top">
                                <strong>Enable Multi-Lingual Features</strong>
                                <%--<asp:ImageButton ID="Imgmultilingual" valign="center" src="image/info.png" runat="server" ToolTip="Switch to support Multilingual all over Application (Themes/UI Text)."/>--%>
                            </td>
                            <td valign="top" align="left" style="width: 25%">
                                <asp:DropDownList ID="DrpDwnListMultiLingual" runat="server" CssClass="alt2SelectFormat">
                                    <asp:ListItem Value="0">No</asp:ListItem>
                                    <asp:ListItem Selected="True" Value="1">Si</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td style="width: 1%"></td>
                            <td align="left" class="blackblodtext" valign="top" style="width: 27%">
                                <b>predeterminar todos los calendarios en horas de oficinas</b>
                                <%--<asp:ImageButton ID="Imgdecaloffice" valign="center" src="image/info.png" runat="server" ToolTip="Switch to enable conference hours to office hours."/>--%>
                            </td>
                            <td valign="top" align="left" style="width: 20%">
                                <asp:DropDownList ID="lstDefaultOfficeHours" runat="server" CssClass="alt2SelectFormat">
                                    <asp:ListItem Value="0">No</asp:ListItem>
                                    <asp:ListItem Value="1">Si</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td style="width: 1%"></td>
                        </tr>
                        <%--FB 2598 Starts (Switch for CallMonitor,EM7,CDR)--%>
                        <tr>
                            <td style="width: 1%"></td>
                           <td align="left" style="width: 25%;" class="blackblodtext" rowspan="1" valign="top">
                                            <strong>Enable Call Monitor</strong>                                           
                                        </td>
                                        <td valign="top" style="width: 25%;" align="left">
                                            <asp:DropDownList ID="DrpDwnEnableCallmonitor" runat="server" CssClass="alt2SelectFormat">
                                                <asp:ListItem Value="0">No</asp:ListItem>
                                                <asp:ListItem Selected="True" Value="1">Si</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>                                        
                                        <td style="width: 1%;"></td>
                                        <td align="left" style="width: 27%;" class="blackblodtext" rowspan="1" valign="top">
                                            <strong>Enable EM7</strong>                                           
                                        </td>
                                        <td valign="top" style="width: 20%;" align="left">
                                            <asp:DropDownList ID="DrpDwnEnableEM7" runat="server" CssClass="alt2SelectFormat">
                                                <asp:ListItem Value="0">No</asp:ListItem>
                                                <asp:ListItem Selected="True" Value="1">Si</asp:ListItem>
                                            </asp:DropDownList>
                                        </td> 
                                    </tr>
                                    <tr>  
                                        <td style="width: 1%;">
                                        </td> 
                                        <td align="left" style="width: 25%;" class="blackblodtext" rowspan="1" valign="top">
                                            <strong>Enable CDR</strong>                                           
                                        </td>
                                        <td valign="top" style="width: 25%;" align="left">
                                            <asp:DropDownList ID="DrpDwnEnableCDR" runat="server" CssClass="alt2SelectFormat">
                                                <asp:ListItem Value="0">No</asp:ListItem>
                                                <asp:ListItem Selected="True" Value="1">Si</asp:ListItem>
                                            </asp:DropDownList>
                                        </td> 
                                        </tr> 
                                 <%--FB 2598 Ends (Switch for CallMonitor,EM7,CDR)--%> 
                                    <tr>
                            <td style="width: 1%" rowspan="4"></td>
                            <td align="left" class="blackblodtext" valign="top" style="width: 25%">
                                <b>abierto 24 horas</b>
                                <%--<asp:ImageButton ID="Imgopen24hours" valign="center" src="image/info.png" runat="server" ToolTip="Office Timings/Conference Timings."/>--%>
                            </td>
                            <td align="left" valign="top" style="width: 25%">
                                <asp:CheckBox ID="Open24" runat="server" onclick="javascript:open24()" />
                            </td>
                            <td rowspan="4" style="width: 1%"></td>
                            <td align="left" valign="top" style="padding-top: 6px;width: 27%;" class="blackblodtext" rowspan="3">
                                <b>D�as Cerrados</b>
                                <%--<asp:ImageButton ID="Imgdaysclosed" valign="center" src="image/info.png" runat="server" ToolTip="Conference can not be scheduled in closed days."/>--%>
                            </td>
                            <td rowspan="4" align="left" style="width: 20%">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td align="left" nowrap="nowrap"><%-- FB 2613--%>
                                            <asp:CheckBoxList ID="DayClosed" runat="server" CellPadding="0" CellSpacing="6" RepeatColumns="2"
                                                RepeatDirection="Horizontal" RepeatLayout="Table" TextAlign="right">
                                                <asp:ListItem Text="&lt;font class='blackblodtext'&gt;Domingo&lt;/font&gt;" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="&lt;font class='blackblodtext'&gt;Lunes&lt;/font&gt;" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="&lt;font class='blackblodtext'&gt;Martes&lt;/font&gt;" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="&lt;font class='blackblodtext'&gt;Mi�rcoles&lt;/font&gt;" Value="4"></asp:ListItem>
                                                <asp:ListItem Text="&lt;font class='blackblodtext'&gt;Jueves&lt;/font&gt;" Value="5"></asp:ListItem>
                                                <asp:ListItem Text="&lt;font class='blackblodtext'&gt;Viernes&lt;/font&gt;" Value="6"></asp:ListItem>
                                                <asp:ListItem Text="&lt;font class='blackblodtext'&gt;s�bado&lt;/font&gt;" Value="7"></asp:ListItem>
                                            </asp:CheckBoxList>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td rowspan="4" style="width: 1%"></td>
                        </tr>
                        <tr>
                            <td id="Open24DIV1" class="blackblodtext" align="left" style="width: 190px" valign="baseline">
                                <br />
                                <b>Hora de inicio</b>
                                <%--<asp:ImageButton ID="Imgstarttime" valign="center" src="image/info.png" runat="server" ToolTip="Office Timing :Start Time"/>--%>
                            </td>
                            <td id="Open24DIV2" align="left" style="width: 10px">
                                <br />
                                <mbcbb:ComboBox ID="systemStartTime" runat="server" CssClass="altSelectFormat" Rows="10"
                                    CausesValidation="True" Style="width: auto" AutoPostBack="false">
                                    <asp:ListItem Value="01:00 AM" Selected="True">
                                    </asp:ListItem>
                                    <asp:ListItem Value="02:00 AM">
                                    </asp:ListItem>
                                    <asp:ListItem Value="03:00 AM">
                                    </asp:ListItem>
                                    <asp:ListItem Value="04:00 AM">
                                    </asp:ListItem>
                                    <asp:ListItem Value="05:00 AM">
                                    </asp:ListItem>
                                    <asp:ListItem Value="06:00 AM">
                                    </asp:ListItem>
                                    <asp:ListItem Value="07:00 AM">
                                    </asp:ListItem>
                                    <asp:ListItem Value="08:00 AM">
                                    </asp:ListItem>
                                    <asp:ListItem Value="09:00 AM">
                                    </asp:ListItem>
                                    <asp:ListItem Value="10:00 AM">
                                    </asp:ListItem>
                                    <asp:ListItem Value="11:00 AM">
                                    </asp:ListItem>
                                    <asp:ListItem Value="12:00 PM">
                                    </asp:ListItem>
                                    <asp:ListItem Value="01:00 PM">
                                    </asp:ListItem>
                                    <asp:ListItem Value="02:00 PM">
                                    </asp:ListItem>
                                    <asp:ListItem Value="03:00 PM">
                                    </asp:ListItem>
                                    <asp:ListItem Value="04:00 PM">
                                    </asp:ListItem>
                                    <asp:ListItem Value="05:00 PM">
                                    </asp:ListItem>
                                    <asp:ListItem Value="06:00 PM">
                                    </asp:ListItem>
                                    <asp:ListItem Value="07:00 PM">
                                    </asp:ListItem>
                                    <asp:ListItem Value="08:00 PM">
                                    </asp:ListItem>
                                    <asp:ListItem Value="09:00 PM">
                                    </asp:ListItem>
                                    <asp:ListItem Value="10:00 PM">
                                    </asp:ListItem>
                                    <asp:ListItem Value="11:00 PM">
                                    </asp:ListItem>
                                    <asp:ListItem Value="12:00 AM">
                                    </asp:ListItem>
                                </mbcbb:ComboBox>
                            </td>
                        </tr>
                        <tr>
                            <td id="Open24DIV3" class="blackblodtext" style="width: 190px" align="left" valign="baseline">
                                <br />
                                <b>Hora de finalizaci�n</b>
                                <%--<asp:ImageButton ID="Imgendtime" valign="center" src="image/info.png" runat="server" ToolTip="Office Timing:End Time"/>--%>
                            </td>
                            <td id="Open24DIV4" align="left" style="width: 10px">
                                <br />
                                <mbcbb:ComboBox ID="systemEndTime" runat="server" CssClass="altSelectFormat" Rows="10"
                                    CausesValidation="True" Style="width: auto">
                                    <asp:ListItem Value="01:00 AM" Selected="True">
                                    </asp:ListItem>
                                    <asp:ListItem Value="02:00 AM">
                                    </asp:ListItem>
                                    <asp:ListItem Value="03:00 AM">
                                    </asp:ListItem>
                                    <asp:ListItem Value="04:00 AM">
                                    </asp:ListItem>
                                    <asp:ListItem Value="05:00 AM">
                                    </asp:ListItem>
                                    <asp:ListItem Value="06:00 AM">
                                    </asp:ListItem>
                                    <asp:ListItem Value="07:00 AM">
                                    </asp:ListItem>
                                    <asp:ListItem Value="08:00 AM">
                                    </asp:ListItem>
                                    <asp:ListItem Value="09:00 AM">
                                    </asp:ListItem>
                                    <asp:ListItem Value="10:00 AM">
                                    </asp:ListItem>
                                    <asp:ListItem Value="11:00 AM">
                                    </asp:ListItem>
                                    <asp:ListItem Value="12:00 PM">
                                    </asp:ListItem>
                                    <asp:ListItem Value="01:00 PM">
                                    </asp:ListItem>
                                    <asp:ListItem Value="02:00 PM">
                                    </asp:ListItem>
                                    <asp:ListItem Value="03:00 PM">
                                    </asp:ListItem>
                                    <asp:ListItem Value="04:00 PM">
                                    </asp:ListItem>
                                    <asp:ListItem Value="05:00 PM">
                                    </asp:ListItem>
                                    <asp:ListItem Value="06:00 PM">
                                    </asp:ListItem>
                                    <asp:ListItem Value="07:00 PM">
                                    </asp:ListItem>
                                    <asp:ListItem Value="08:00 PM">
                                    </asp:ListItem>
                                    <asp:ListItem Value="09:00 PM">
                                    </asp:ListItem>
                                    <asp:ListItem Value="10:00 PM">
                                    </asp:ListItem>
                                    <asp:ListItem Value="11:00 PM">
                                    </asp:ListItem>
                                    <asp:ListItem Value="12:00 AM">
                                    </asp:ListItem>
                                </mbcbb:ComboBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>     
            
            <tr>
                <td colspan="7" align="left" style="height: 49px">
                    <table width="100%" border="0">
                        <tr>
                            <td width="100%" height="20" align="left">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:ImageButton ID="img_CONFSECDESK" runat="server" ImageUrl="image/loc/nolines_plus.gif"
                                                Height="25" Width="25" vspace="0" hspace="0" />
                                        </td>
                                        <td valign="top">        
                                            <span class="subtitleblueblodtext">opciones de seguridad para escritorio</span>
                                           
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trCONFSECDESK" runat="server" style="display: none">
                <td colspan="7">
                    <table border="0" width="100%" cellpadding="5">
                        <tr>
                            <td style="width: 1%">
                            </td>
                            <td align="left" class="blackblodtext" valign="top" style="width: 25%">
                                <strong style="align: left">Habilitar Distintivo de seguridad</strong>
                                <%--<asp:ImageButton ID="Imgesecuritybad" valign="center" src="image/info.png" runat="server" ToolTip="Switch to enable security badge functions."/>--%>
                            </td>
                            <td valign="top" align="left" style="width: 25%">
                                <asp:DropDownList ID="drpenablesecuritybadge" runat="server" Width="125px" CssClass="alt2SelectFormat"
                                    onchange="JavaScript:modedisplay()">
                                    <asp:ListItem Value="1">Si</asp:ListItem>
                                    <asp:ListItem Value="0">No</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td style="width: 1%">
                            </td>
                            <td id="tdSecurityType" class="blackblodtext" align="left" runat="server" style="display: none;width: 27%;"
                                valign="top" >
                                <strong>Tipo de Distintivo de seguridad</strong>
                                <%--<asp:ImageButton ID="Imgsecuritybadgetype" valign="center" src="image/info.png" runat="server" ToolTip="Switch to send security Badge mails according to selections."/>--%>
                            </td>
                            <td valign="top" align="left" style="width: 20%">
                                <asp:DropDownList ID="drpsecuritybadgetype" runat="server" DataTextField="Name" DataValueField="ID"
                                    Width="125px" CssClass="alt2SelectFormat" onchange="JavaScript:emaildisplay()">
                                </asp:DropDownList>
                            </td>
                            <td style="width: 1%">
                            </td>
                        </tr>
                        <tr id="tdsecdeskemailid" runat="server">
                            <td style="height: 33px; width: 1%">
                            </td>
                            <td align="left" class="blackblodtext" style="width: 25%">
                                <strong>ID del correo-e del Escritorio de Seguridad</strong>
                                <%--<asp:ImageButton ID="Imgsecdeskemailid" valign="center" src="image/info.png" runat="server" ToolTip="Email id to which security Badge will be sent."/>--%>
                            </td>
                            <td align="left" style="width: 25%;">
                                <asp:TextBox ID="txtsecdeskemailid" runat="server" CssClass="altText" Width="187px"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="regsecdeskemailid" runat="server" ControlToValidate="txtsecdeskemailid"
                                    ErrorMessage="Direcci�n de correo electr�nico Inv�lida" Display="dynamic" ValidationExpression="^[a-zA-Z()^][()^\w\.-]*[a-zA-Z0-9()^]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"></asp:RegularExpressionValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator4" ControlToValidate="txtiCalEmailId"
                                    Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ; ? | = ! ` , [ ] { } :<br> # $ ~ y &#34; son caracteres no v�lido."
                                    ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=:#$%&'~]*$"></asp:RegularExpressionValidator>
                            </td>
                            <td style="width: 1%">
                            </td>
                            <td style="width: 27%">
                            </td>
                            <td style="width: 20%">
                            </td>
                            <td style="width: 1%">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            <tr>
                <td colspan="7" align="left" style="height: 49px">
                    <table width="100%" border="0">
                        <tr>
                            <td width="100%" height="20" align="left">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:ImageButton ID="img_PIM" runat="server" ImageUrl="image/loc/nolines_plus.gif"
                                                Height="25" Width="25" vspace="0" hspace="0" />
                                         </td>
                                         <td valign="top">
                                            <span class="subtitleblueblodtext">Caracter�sticas del PIM</span>
                                            
                                         </td>
                                     </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trPIM" runat="server" style="display: none">
                <td colspan="7" style="padding-bottom: 0px; padding-left: 0px; padding-right: 0px;
                    padding-top: 0px;">
                    <table border="0" width="100%" cellpadding="0">
                        <tr>
                            <td align="left" class="blackblodtext" rowspan="1" valign="top" style="width: 1%"></td>
                            <td align="left" class="blackblodtext" rowspan="1" valign="top" style="width: 25%">
                                <strong>Confirmaciones conectados</strong>
                                <%--<asp:ImageButton ID="Imgpluginconfirm" valign="center" src="image/info.png" runat="server" ToolTip="Switch to send confirmation mails when conference is scheduled via outlook."/>--%>
                            </td>
                            <td valign="top" align="left" style="width: 25%">
                                <asp:DropDownList ID="DrpPluginConfirm" runat="server" CssClass="alt2SelectFormat">
                                    <asp:ListItem Value="0">No</asp:ListItem>
                                    <asp:ListItem Selected="True" Value="1">Si</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td rowspan="1" style="width: 1%"></td>
                            <td align="left" class="blackblodtext" rowspan="1" valign="top" style="width: 27%">
                                <strong>Habilitar Salones disponibles</strong>
                                <%--<asp:ImageButton ID="Imgeavailrooms" valign="center" src="image/info.png" runat="server" ToolTip="Switch to show availble rooms  during conference creation via outlook."/>--%>
                            </td>
                            <td valign="top" align="left" style="width: 20%">
                                <asp:DropDownList ID="lstEnablePIMServiceType" runat="server" CssClass="alt2SelectFormat">
                                    <asp:ListItem Value="0">No</asp:ListItem>
                                    <asp:ListItem Selected="True" Value="1">Si</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td style="width: 1%"></td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            <tr>
                <td colspan="7" align="left" style="height: 49px">
                    <table width="100%" border="0">
                        <tr>
                            <td width="100%" height="20" align="left">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:ImageButton ID="img_SUR" runat="server" ImageUrl="image/loc/nolines_plus.gif"
                                                Height="25" Width="25" vspace="0" hspace="0" />
                                        </td>
                                        <td valign="top">
                                            <span class="subtitleblueblodtext">Encuesta</span>
                                            
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trSUR" runat="server" style="display: none">
                <td colspan="7" style="padding-bottom: 0px; padding-left: 0px; padding-right: 0px;
                    padding-top: 0px;">
                    <table border="0" width="100%" cellpadding="1">
                        <tr>
                            <td style="width: 1%"></td>
                            <td align="left" class="blackblodtext" style="width: 25%">
                                Habilitar Encuesta 
                                <%--<asp:ImageButton ID="Imgensurvey" valign="center" src="image/info.png" runat="server" ToolTip="Switch to enable survey engine field will display.Survey engine  contain two options.There are Internal Survey,External Survey."/>--%>
                            </td>
                            <td align="left" style="width: 25%">
                                <asp:DropDownList ID="drpenablesurvey" runat="server" Width="125px" CssClass="alt2SelectFormat"
                                    onchange="JavaScript:modedisplay1()">
                                    <asp:ListItem Value="1">Si</asp:ListItem>
                                    <asp:ListItem Value="0" Selected="True">No</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td style="width: 1%"></td>
                            <td colspan="2" width="47%">
                                <div id="divSurveyURL" runat="server" style="display: none">
                                    <table width="100%">
                                        <tr>
                                            <td align="left" style="font-weight: bold; width: 58%" class="blackblodtext">
                                                URL del Sitio Web de la Encuesta
                                                <%--<asp:ImageButton ID="Imgsurveyweburl" valign="center" src="image/info.png" runat="server" ToolTip="Survey link to user for the organzation Options."/>--%>
                                            </td>
                                            <td align="left" style="height: 21px;" width="42%">
                                                <asp:TextBox ID="txtSurWebsiteURL" runat="server" CssClass="altText"></asp:TextBox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br />
                                                <asp:RequiredFieldValidator ID="ReqSurWebsiteURL" runat="server" ControlToValidate="txtSurWebsiteURL"
                                                    ErrorMessage="Es necesaria la URL del Sitio Web de la Encuesta." Font-Names="Verdana" Font-Size="X-Small"
                                                    Font-Bold="False"><font color="red" size="1pt"> required</font></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="RegSurWebsiteURL" ControlToValidate="txtSurWebsiteURL"
                                                    Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="& < y > son caracteres no v�lidos."
                                                    ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                            <td style="width: 1%"></td>
                        </tr>
                        <tr>
                            <td style="width: 1%">
                            </td>
                            <td id="tdSurveyengine" runat="server" align="left" class="blackblodtext" width="25%"
                                style="visibility: hidden">
                                Motor de Encuesta
                                <%--<asp:ImageButton ID="Imgsurveyeng" valign="center" src="image/info.png" runat="server" ToolTip="If select External survey Option Survey Website Url,send survey will display."/>--%>
                            </td>
                            <td id="tdsurveyoption" runat="server" align="left" width="25%" style="visibility: hidden">
                                <asp:DropDownList ID="drpsurveyoption" runat="server" Width="125px" CssClass="alt2SelectFormat"
                                    onchange="JavaScript:modesurvey()">
                                    <asp:ListItem Value="1">Internal survey</asp:ListItem>
                                    <asp:ListItem Value="2">External survey</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td style="width: 1%">
                            </td>
                            <td colspan="2" width="47%">
                                <div id="divSurveytimedur" runat="server" style="display: none">
                                    <table width="100%">
                                        <tr>
                                            <td align="left" style="font-weight: bold; width: 58%" class="blackblodtext">
                                                Enviar encuesta
                                                <%--<asp:ImageButton ID="Imgsendsurvey" valign="center" src="image/info.png" runat="server" ToolTip="How long after the completion of the conference where the survey would have been required."/>--%>
                                            </td>
                                            <td style="height: 21px;" align="left" width="42%">
                                                <asp:TextBox ID="txtTimeDur" runat="server" MaxLength="9" CssClass="altText"></asp:TextBox> (mins)<br />
                                                <asp:RegularExpressionValidator ID="RegTimeDur" ControlToValidate="txtTimeDur" ValidationExpression="\d+"
                                                    Display="Dynamic" ErrorMessage="Por favor, introduzca solamente n�meros." runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                            <td style="width: 1%">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            <tr>
                <td colspan="7" align="left" style="height: 49px">
                    <table width="100%">
                        <tr>
                            <td width="100%" height="20" align="left" class="blackblodtext">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:ImageButton ID="img_ADMOPT" runat="server" ImageUrl="image/loc/nolines_plus.gif"
                                                Height="25" Width="25" vspace="0" hspace="0" />
                                        </td>
                                        <td>
                                            <span class="subtitleblueblodtext">Informacion tecnica</span>
                                       </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trADMOPT" runat="server" style="display: none">
                <td colspan="7">
                    <table border="0" width="100%" cellspacing="1" cellpadding="2">
                        <tr>
                            <td style="width: 1%"></td>
                            <td align="left" valign="baseline" class="blackblodtext" style="width: 25%; bottom: auto">
                                <b>Contacto de Soporte T�cnico</b>
                            </td>
                            <td align="left" valign="baseline" style="width: 25%">
                                <asp:TextBox ID="ContactName" runat="server" CssClass="altText" MaxLength="256" Width="187px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ContactName"
                                    ErrorMessage="Es necesario el No" Font-Names="Verdana" Font-Size="X-Small"
                                    Font-Bold="False"><font color="red" size="1pt"> required</font></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="regContactName" ControlToValidate="ContactName"
                                    Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > + % \ ? | ^ = ! `[ ] { } # $ y ~ son caracteres no v�lidos.."
                                    ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>^+?|!`\[\]{}\=#$%&~]*$"></asp:RegularExpressionValidator><%--FB 1888--%>
                            </td>
                            <td style="width: 1%"></td>
                            <td align="left" style="height: 47px; width: 27%" valign="top" class="blackblodtext">
                                <b>Correo-e del Soporte T�cnico</b>
                            </td>
                            <td align="left" style="height: 47px; width: 20%" valign="top">
                                <asp:TextBox ID="ContactEmail" runat="server" CssClass="altText" MaxLength="512"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="ContactEmail"
                                    ErrorMessage="Correo-e de soporte t�cnico inv�lido." ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                    Font-Names="Verdana" Font-Size="X-Small" Font-Bold="False"><font color="red" size="1pt">invalid</font></asp:RegularExpressionValidator>
                            </td>
                            <td style="width: 1%"></td>
                        </tr>
                        <tr>
                            <td style="width: 1%"></td>
                            <td align="left" valign="top" style="width: 25%" class="blackblodtext">
                                <b>Tel�fono del Soporte T�cnico</b>
                            </td>
                            <td align="left" valign="middle" style="width: 25%">
                                <asp:TextBox ID="ContactPhone" runat="server" CssClass="altText" Width="187px" MaxLength="250"></asp:TextBox><%--FB 2498--%>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ContactPhone"
                                    ErrorMessage="Es necesario el Nombre del Contacto." Font-Names="Verdana" Font-Size="X-Small"
                                    Font-Bold="False"><font color="red" size="1pt"> required</font></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" ControlToValidate="ContactPhone"
                                    Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' % \ / ; ? | ^ = ! ` [ ] { } : # $ @ ~ y &#34; son caracteres no v�lidos.."
                                    ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>^;?|!`\[\]{}\x22;=@#$%&'~]*$"></asp:RegularExpressionValidator><%--FB 2319--%>
                            </td>
                            <td style="width: 1%"></td>
                            <td align="left" class="blackblodtext" valign="top" style="width: 27%">
                                <b>Informaci�n adicional</b>
                            </td>
                            <td align="left" valign="top" style="width: 20%">
                                <asp:TextBox ID="ContactAdditionInfo" runat="server" CssClass="altText" MaxLength="256"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="regContactAdditionInfo" ControlToValidate="ContactAdditionInfo"
                                    Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ y &#34; son caracteres no v�lidos.."
                                    ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                            </td>
                            <td style="width: 1%"></td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            
            </table>
            
            <table width="800" border="0" cellspacing="4" cellpadding="4" align="center">
                <tr height="50">
                    <td align="right">
                        <input id="btnReset" type="Reset" name="Reset" value="reajustar" class="altBlueButtonFormat">
                    </td>
                    <td align="center">
                    </td>
                    <td align="center">
                        <asp:Button runat="server" ID="btnSubmit" CssClass="altBlueButtonFormat" OnClick="btnSubmit_Click"
                            OnClientClick="javascript:return Submit()" Text="someter" />
                    </td>
                </tr>
                <tr>
                    <td colspan="3" align="center">
                        <ajax:ModalPopupExtender ID="MessagePopup" runat="server" TargetControlID="btmTxtmsgPopup"
                            BackgroundCssClass="modalBackground" PopupControlID="MessagePanel" DropShadow="false"
                            Drag="true" CancelControlID="btnMsgClose">
                        </ajax:ModalPopupExtender>
                        <asp:Panel ID="MessagePanel" runat="server" HorizontalAlign="Center" Width="50%"
                            CssClass="treeSelectedNode">
                            <table width="100%" align="center" border="0">
                                <tr>
                                    <td align="center">
                                        <span class="subtitleblueblodtext">MCU Message Overlay</span><br />
                                    </td>
                                </tr>
                                <tr>
                                    <td id="tdTxtMsgDetails" runat="server">
                                        <table width="100%" border="0">
                                            <tr>
                                                <td>
                                                    <table cellpadding="3" cellspacing="0" width="100%" border="0">
                                                        <tr id="tr2" runat="server">
                                                            <td align="left" width="5%">
                                                                <asp:CheckBox ID="chkmsg1" runat="server" onclick="javascript:return fnCheck('drpdownmsgduration1');" />
                                                            </td>
                                                            <td align="left" width="68%">
                                                                <asp:DropDownList ID="drpdownconfmsg1" CssClass="altSelectFormat" runat="server"
                                                                    DataTextField="ConfMsg" DataValueField="ConfMsgID" Width="100%">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td align="left" width="20%">
                                                                <asp:DropDownList ID="drpdownmsgduration1" CssClass="altText" runat="server" Width="55px"
                                                                    onChange="javascript:return fnCheck(this.id);">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td align="left" width="7%">
                                                            </td>
                                                        </tr>
                                                        <tr id="tr3" runat="server">
                                                            <td align="left">
                                                                <asp:CheckBox ID="chkmsg2" runat="server" onclick="javascript:return fnCheck('drpdownmsgduration2');" />
                                                            </td>
                                                            <td align="left">
                                                                <asp:DropDownList ID="drpdownconfmsg2" CssClass="altSelectFormat" runat="server"
                                                                    DataTextField="ConfMsg" DataValueField="ConfMsgID" Width="100%">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td align="left">
                                                                <asp:DropDownList ID="drpdownmsgduration2" CssClass="altText" runat="server" Width="55px"
                                                                    onChange="javascript:return fnCheck(this.id);">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td>
                                                            </td>
                                                        </tr>
                                                        <tr id="tr4" runat="server">
                                                            <td align="left">
                                                                <asp:CheckBox ID="chkmsg3" runat="server" onclick="javascript:return fnCheck('drpdownmsgduration3');" />
                                                            </td>
                                                            <td align="left">
                                                                <asp:DropDownList ID="drpdownconfmsg3" CssClass="altSelectFormat" runat="server"
                                                                    DataTextField="ConfMsg" DataValueField="ConfMsgID" Width="100%">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td align="left">
                                                                <asp:DropDownList ID="drpdownmsgduration3" CssClass="altText" runat="server" Width="55px"
                                                                    onChange="javascript:return fnCheck(this.id);">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td align="left" style="display: none;">
                                                                <a id="displayText" href="javascript:toggle();">more</a>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table id="toggleText" cellpadding="3" cellspacing="0" width="100%" border="0">
                                                        <tr id="tr5" runat="server">
                                                            <td align="left" width="5%">
                                                                <asp:CheckBox ID="chkmsg4" runat="server" onclick="javascript:return fnCheck('drpdownmsgduration4');" />
                                                            </td>
                                                            <td align="left" width="68%">
                                                                <asp:DropDownList ID="drpdownconfmsg4" CssClass="altSelectFormat" runat="server"
                                                                    DataTextField="ConfMsg" DataValueField="ConfMsgID" Width="100%">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td align="left" width="20%">
                                                                <asp:DropDownList ID="drpdownmsgduration4" CssClass="altText" runat="server" Width="55px"
                                                                    onChange="javascript:return fnCheck(this.id);">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td align="left" width="7%">
                                                            </td>
                                                        </tr>
                                                        <tr id="tr6" runat="server">
                                                            <td align="left">
                                                                <asp:CheckBox ID="chkmsg5" runat="server" TextAlign="Left" onclick="javascript:return fnCheck('drpdownmsgduration5');" />
                                                            </td>
                                                            <td align="left">
                                                                <asp:DropDownList ID="drpdownconfmsg5" CssClass="altSelectFormat" runat="server"
                                                                    DataTextField="ConfMsg" DataValueField="ConfMsgID" Width="100%">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td align="left">
                                                                <asp:DropDownList ID="drpdownmsgduration5" CssClass="altText" runat="server" Width="55px"
                                                                    onChange="javascript:return fnCheck(this.id);">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td>
                                                            </td>
                                                        </tr>
                                                        <tr id="tr7" runat="server">
                                                            <td align="left">
                                                                <asp:CheckBox ID="chkmsg6" runat="server" TextAlign="Left" onclick="javascript:return fnCheck('drpdownmsgduration6');" />
                                                            </td>
                                                            <td align="left">
                                                                <asp:DropDownList ID="drpdownconfmsg6" CssClass="altSelectFormat" runat="server"
                                                                    DataTextField="ConfMsg" DataValueField="ConfMsgID" Width="100%">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td align="left">
                                                                <asp:DropDownList ID="drpdownmsgduration6" CssClass="altText" runat="server" Width="55px"
                                                                    onChange="javascript:return fnCheck(this.id);">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td>
                                                            </td>
                                                        </tr>
                                                        <tr id="tr8" runat="server">
                                                            <td align="left">
                                                                <asp:CheckBox ID="chkmsg7" runat="server" TextAlign="Left" onclick="javascript:return fnCheck('drpdownmsgduration7');" />
                                                            </td>
                                                            <td align="left">
                                                                <asp:DropDownList ID="drpdownconfmsg7" CssClass="altSelectFormat" runat="server"
                                                                    DataTextField="ConfMsg" DataValueField="ConfMsgID" Width="100%">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td align="left">
                                                                <asp:DropDownList ID="drpdownmsgduration7" CssClass="altText" runat="server" Width="55px"
                                                                    onChange="javascript:return fnCheck(this.id);">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td>
                                                            </td>
                                                        </tr>
                                                        <tr id="tr9" runat="server">
                                                            <td align="left">
                                                                <asp:CheckBox ID="chkmsg8" runat="server" TextAlign="Left" onclick="javascript:return fnCheck('drpdownmsgduration8');" />
                                                            </td>
                                                            <td align="left">
                                                                <asp:DropDownList ID="drpdownconfmsg8" CssClass="altSelectFormat" runat="server"
                                                                    DataTextField="ConfMsg" DataValueField="ConfMsgID" Width="100%">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td align="left">
                                                                <asp:DropDownList ID="drpdownmsgduration8" CssClass="altText" runat="server" Width="55px"
                                                                    onChange="javascript:return fnCheck(this.id);">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td>
                                                            </td>
                                                        </tr>
                                                        <tr id="tr10" runat="server">
                                                            <td align="left">
                                                                <asp:CheckBox ID="chkmsg9" runat="server" TextAlign="Left" onclick="javascript:return fnCheck('drpdownmsgduration9');" />
                                                            </td>
                                                            <td align="left">
                                                                <asp:DropDownList ID="drpdownconfmsg9" CssClass="altSelectFormat" runat="server"
                                                                    DataTextField="ConfMsg" DataValueField="ConfMsgID" Width="100%">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td align="left">
                                                                <asp:DropDownList ID="drpdownmsgduration9" CssClass="altText" runat="server" Width="55px"
                                                                    onChange="javascript:return fnCheck(this.id);">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <br />
                                        <input align="middle" type="button" runat="server" id="btnMsgClose" value="Cerrar"
                                            class="altShortBlueButtonFormat" />
                                        <asp:Button ID="btnMsgSubmit" runat="server" Text="Entregar" OnClick="fnOrgTxtMsgSubmit"
                                            class="altShortBlueButtonFormat" ValidationGroup="Submit1" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
            
        </div>

        <script type="text/javascript">
            //Edited For FF...
            if (document.getElementById("systemStartTime_Container") != null)
                document.getElementById("systemStartTime_Container").style.width = "auto"
            if (document.getElementById("systemEndTime_Container") != null)
                document.getElementById("systemEndTime_Container").style.width = "auto"
        </script>

        <script language="javascript" type="text/javascript">
            open24();
        </script>

        </form>
    </div>
</body>
</html>

<%--code added for Soft Edge button--%>

<script type="text/javascript" src="inc/softedge.js"></script>

<!----------------------------------------->
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->

<script type="text/javascript">

    // FB 2384 Starts
    function findPos(obj) {
        var curleft = curtop = 0;

        if (obj.offsetParent) {
            curleft = obj.offsetLeft
            curtop = obj.offsetTop
            while (obj = obj.offsetParent) {
                curleft += obj.offsetLeft
                curtop += obj.offsetTop
            }
        }
        return [curleft, curtop];
    }
    //FB 2384 Ends
    // FB 2335 Starts
    var invokingObject;
    function changeLayout(invoker, myObj, posval) // FB 2384
    {
        invokingObject = invoker;
        //alert('Promptpicture: ' + promptpicture + '\nprompttitle: ' + prompttitle + '\nepid: ' + epid + '\nepty: ' + epty + '\ndl: ' + dl + '\nrowsize: ' + rowsize + '\nimages: ' + images + '\nimgpath: ' + imgpath);
        promptpicture = "image/pen.gif";
        prompttitle = "Administrar Mapeado Autom�tico de la Distribuci�n en Pantalla";
        epid = "01";
        rowsize = 5;
        images = "01:02:03:04:05:06:07:08:09:10:11:12:13:14:15:16:17:18:19:20:21:22:23:24:25:26:27:28:29:30:31:32:33:34:35:36:37:38:39:40:41:42:43:44:45:46:47:48:49:50:51:52:53:54:55:56:57:58:59:";

        if (invokingObject == "Poly2MGC" || invokingObject == "Poly2RMX") {

            images = "01:02:03:04:05:06:12:13:14:15:16:17:18:19:20:24:25:33:60:61:62:63:";

        }

        imgpath = "image/displaylayout/";
        var title = new Array()
        title[0] = "Default ";
        title[1] = "Custom ";
        promptbox = document.createElement('div');
        promptbox.setAttribute('id', 'prompt');
        document.getElementsByTagName('body')[0].appendChild(promptbox);
        promptbox = document.getElementById('prompt').style; // FB 2050
        //FB 2384 starts
        var pos = findPos(myObj);
        if (posval == 1) {
            pos[0] = pos[0] - 90;
            pos[1] = pos[1] - 20;
        }
        else
            pos[0] = pos[0] - 350;
        pos[1] = pos[1] - 160;
        promptbox.position = 'absolute'
        promptbox.top = pos[1] + 'px'; //FB 1373 start FB 2050
        promptbox.left = pos[0] + 'px'; // FB 2050
        //FB 2384 ends
        promptbox.width = rowsize * 125 + 'px'; // FB 2050
        promptbox.border = 'outset 1 #bbbbbb';
        promptbox.height = '400px'; // FB 2050
        promptbox.overflow = 'auto'; //FB 1373 End


        m = "<table cellspacing='0' cellpadding='0' border='0' width='100%'><tr valign='middle'><td width='22' height='22' style='text-indent:2;' class='tableHeader'>&nbsp;</td><td class='tableHeader'>" + prompttitle + "</td></tr></table>"
        m += "<table cellspacing='2' cellpadding='2' border='0' width='100%' class='promptbox'>";
        imagesary = images.split(":");
        rowNum = parseInt((imagesary.length + rowsize - 2) / rowsize, 10);
        m += "  <tr><td align='right' colspan='" + (rowsize * 2) + "'>"
        //Code Changed for Soft Edge Button
        //m += "    <input type='button' class='altShortBlueButtonFormat' value='Submit' onMouseOver='this.style.border=\"1 outset #dddddd\"' onMouseOut='this.style.border=\"1 solid transparent\"' onClick='saveOrder(epid);'>"
        //m += "    <input type='button' class='altShortBlueButtonFormat' value='Cancel' onMouseOver='this.style.border=\"1 outset transparent\"' onMouseOut='this.style.border=\"1 solid transparent\"' onClick='cancelthis();'>"
        m += "    <input type='button' onfocus='this.blur()' class='altShortBlueButtonFormat' value='Cancelar'  onClick='cancelthis();'>"
        m += "    <input type='button' onfocus='this.blur()' class='altShortBlueButtonFormat' value='Entregar'  onClick='saveOrder(epid);'>"
        m += "  </td></tr>"
        m += "	<tr>";
        //Window Dressing
        m += "    <td colspan='" + (rowsize * 2) + "' align='left' class='blackblodtext'><b>Presentar Distribuci�n</b></td>";
        m += "  </tr>"
        m += "  <tr>"
        m += "    <td colspan='" + (rowsize * 2) + "' height='5'></td>";
        m += "  </tr>"

        imgno = 0;
        for (i = 0; i < rowNum; i++) {
            m += "  <tr>";
            for (j = 0; (j < rowsize) && (imgno < imagesary.length - 1); j++) {


                m += "    <td valign='middle'>";
                m += "      <input type='radio' name='layout' id='layout' value='" + imagesary[imgno] + "' onClick='epid=" + imagesary[imgno] + ";'>";
                m += "    </td>";
                m += "    <td valign='middle'>";
                m += "      <img src='" + imgpath + imagesary[imgno] + ".gif' width='57' height='43'>";
                m += "    </td>";
                imgno++;
            }
            m += "  </tr>";
        }

        m += "  <tr>";
        m += "    <td colspan='" + (rowsize * 2) + "' height='5'></td>";
        m += "  </tr>"
        m += "  <tr><td align='right' colspan='" + (rowsize * 2) + "'>"
        //Code Changed for Soft Edge Button
        //m += "    <input type='button' class='altShortBlueButtonFormat' value='Submit' onMouseOver='this.style.border=\"1 outset #dddddd\"' onMouseOut='this.style.border=\"1 solid transparent\"' onClick='saveOrder(epid);'>"
        //m += "    <input type='button' class='altShortBlueButtonFormat' value='Cancel' onMouseOver='this.style.border=\"1 outset transparent\"' onMouseOut='this.style.border=\"1 solid transparent\"' onClick='cancelthis();'>"
        m += "    <input type='button' onfocus='this.blur()' class='altShortBlueButtonFormat' value='Cancelar' onClick='cancelthis();'>"
        m += "    <input type='button' onfocus='this.blur()' class='altShortBlueButtonFormat' value='Entregar' onClick='saveOrder(epid);'>"
        m += "  </td></tr>"
        m += "</table>"

        document.getElementById('prompt').innerHTML = m;
    }

    function saveOrder(id) {






        if (id < 10)
            id = "0" + id;

        if (invokingObject == "Poly2MGC") {
            if (id == "001")
                id = document.getElementById("Poly2MGC").value;
            document.getElementById("Poly2MGC").value = id;
            document.getElementById("imgLayoutMapping1").src = "image/displaylayout/" + id + ".gif";
        }
        else if (invokingObject == "Poly2RMX") {
            if (id == "001")
                id = document.getElementById("Poly2RMX").value;
            document.getElementById("Poly2RMX").value = id;
            document.getElementById("imgLayoutMapping2").src = "image/displaylayout/" + id + ".gif";
        }
        else if (invokingObject == "CTMS2Cisco") {
            document.getElementById("CTMS2Cisco").value = id;
            document.getElementById("imgLayoutMapping3").src = "image/displaylayout/" + id + ".gif";
        }
        else if (invokingObject == "CTMS2Poly") {
            document.getElementById("CTMS2Poly").value = id;
            document.getElementById("imgLayoutMapping4").src = "image/displaylayout/" + id + ".gif";
        }


        cancelthis();
    }


    function cancelthis() {
        document.getElementsByTagName("body")[0].removeChild(document.getElementById("prompt"));
        //window.resizeTo(750,450); //FB Case 536 Saima
    }

    ////    var obj = document.getElementById("dgTxtMsg_ctl03_btnEdit");
    ////    getLabel = function(elem){
    ////    if (elem.id && elem.id=="label") {
    ////    elem.id = "disabledLabel";
    ////    }
    ////    };
    ////    Dom.getElementsBy(getLabel ,'td', obj);


    // FB 2335 Ends
    ChangeEnableSmartP2P();  //FB 2430
    function ChangeEnableSmartP2P() //FB 2430
    {
        document.getElementById("lstEnableSmartP2P").disabled = false;
        if (document.getElementById("p2pConfEnabled").value == "0") {
            document.getElementById("lstEnableSmartP2P").value = "0";
            document.getElementById("lstEnableSmartP2P").disabled = true;
        }
    }
    //FB 2571 START
    function fnFeccOptions() {

        if (document.getElementById("lstenableFECC").value == "2") {

            document.getElementById("DefaultFECC").style.visibility = "hidden";
            document.getElementById("DefaultFECCoptions").style.visibility = "hidden";
        }
        else {
            document.getElementById("DefaultFECC").style.visibility = "visible";
            document.getElementById("DefaultFECCoptions").style.visibility = "visible";

        }
        if (document.getElementById("lstenableFECC").value == "0") {
            document.getElementById("lstdefaultFECC").disabled = true;
            document.getElementById("DefaultFECC").disabled = true;
            document.getElementById("lstdefaultFECC").value = "1";
        }
        else {
            document.getElementById("lstdefaultFECC").disabled = false;
        }
    }
    //FB 2571 END
</script>


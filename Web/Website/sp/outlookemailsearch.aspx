﻿<%@ Page Language="C#" AutoEventWireup="true"  %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
  <title>myVRM</title>
  <meta name="Description" content="myVRM (Videoconferencing Resource Management) is a revolutionary Web-based software application that manages any video conferencing environment." />
  <meta name="Keywords" content="VRM, myVRM, Videoconferencing Resource Management, video conferencing, video bridges, video endpoints" />
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  
  <link title="Expedite base styles" href="<%=Session["OrgCSSPath"]%>" type="text/css" rel="stylesheet" />
  
<script language="VBScript" src="script\outlook.vbs"></script>
<script language="Javascript" src="inc/Functions.js"></script>
<script language="VBScript" src="script/settings2.vbs"></script>
  

<script language="VBScript">
Function getOutLookEmailSearch()

	foundcontacts = FindContacts(frmOutlookemailsearch.FirstName.value, frmOutlookemailsearch.LastName.value, "False", "True")
	if foundcontacts <> "-1" then
		url = "outlookemaillist2main.aspx?frm=" & document.frmOutlookemailsearch.frm.value & "&page=1&outlookEmails=" & foundcontacts & "&srch=y"	
		window.navigate url		
	else
		window.close ()
	end if
End Function
</script>


<%
if (Request.QueryString["frm"] != "party2" && Request.QueryString["frm"] != "party2NET" && Request.QueryString["frm"] != "group" && Request.QueryString["frm"] != "users" ){
%>
 
<script language="JavaScript">
<!--                
// main part
	opener.top.location.reload();
//-->
</script>

<%
}
%>
</head>
<body bottommargin="0" leftmargin="5" rightmargin="3" topmargin="8" marginheight="0" marginwidth="0">

   <form name="frmOutlookemailsearch" method="POST" action="">
  <input type="hidden" name="frm" value="<% =Request.QueryString["frm"] %>">
  <!-- Code Added  on 19Mar09 FB 412 Start -->
  <br /><br /><br /><br />
  <!-- Code Added  on 19Mar09 FB 412 Start -->
  <center>
    <h3>Búsqueda en Libro de direcciones</h3>
  
    <table cellpadding="6" cellspacing="5">
      <tr>
        <td colspan="5" align="left" class="blackblodtext"> 
          Por favor use la 'wildcard' o escriba las primeras letras para buscar en su libro de direcciones corporativas.
        </td>
      </tr>
      <tr>
        <td colspan="5" height="10"></td>
      </tr>
      <tr>
        <td align="right" class="blackblodtext">Nombre</td>
        <td> 
          <input type="text" name="FirstName" maxlength="256"  onkeyup="javascript:chkLimit(this,'2');" size="15" class="altText" />
        </td>
        <td>&nbsp;</td>
        <td align="right" class="blackblodtext">Apellido</td>
        <td> 
          <input type="text" name="LastName" maxlength="256"  onkeyup="javascript:chkLimit(this,'2');" size="15" class="altText" />
        </td>
      </tr>
    </table>

    <br /><br />

    <table cellpadding="4" cellspacing="6">
      <tr>
        <td>
         <input type="button" onfocus="this.blur()" name="OutlookEmailSearchSubmit" value="Cerrar Ventana" class="altBlueButtonFormat" onclick="window.close();" language="JavaScript" />
        </td>
        <td>
        
<%
    if (Request.ServerVariables["HTTP_USER_AGENT"].IndexOf("MSIE") >= 0)
    {
%>
<%--code added for Soft Edge button--%>
         <input type="button" onfocus="this.blur()" name="OutlookEmailSearchSubmit" value="buscar" class="altBlueButtonFormat" onclick="getOutLookEmailSearch()" language="VBScript" />
<% }else{%>
		<input type="button" onfocus="this.blur()" name="OutlookEmailSearchSubmit" value="buscar" class="altBlueButtonDisabled" title='Netscape/Firefox browser cannot save appointments to personal MS Outlook' />
<% } %>
        </td>
      </tr>
    </table>

  </center>
  </form>	 

</body>
</html>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
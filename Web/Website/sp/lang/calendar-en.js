// ** I18N

// Calendar EN language
// Author: Mihai Bazon, <mishoo@infoiasi.ro>
// Encoding: any
// Distributed under the same terms as the calendar itself.

// For translators: please use UTF-8 if possible.  We strongly believe that
// Unicode is the answer to a real internationalized world.  Also please
// include your contact information in the header, as can be seen above.

// full day names
Calendar._DN = new Array
("Domingo",
 "Lunes",
 "Martes",
 "Miercoles",
 "Jueves",
 "Viernes",
 "Sabado",
 "Domingo");

// Please note that the following array of short day names (and the same goes
// for short month names, _SMN) isn't absolutely necessary.  We give it here
// for exemplification on how one can customize the short day names, but if
// they are simply the first N letters of the full name you can simply say:
//
//   Calendar._SDN_len = N; // short day name length
//   Calendar._SMN_len = N; // short month name length
//
// If N = 3 then this is not needed either since we assume a value of 3 if not
// present, to be compatible with translation files that were written before
// this feature.

// short day names
Calendar._SDN = new Array
("Sol",
 "Lun",
 "Mar",
 "Cas",
 "Jue",
 "Vie",
 "S�b",
 "Sol");

// full month names
Calendar._MN = new Array
("Enero",
 "Febrero",
 "Marzo",
 "Abril",
 "Mayo",
 "Junio",
 "Julio",
 "Agosto",
 "Septiembre",
 "Octubre",
 "Noviembre",
 "Diciembre");

// short month names
Calendar._SMN = new Array
("Jan",
 "Feb",
 "Mar",
 "Abr",
 "May",
 "Jun",
 "Jul",
 "Ago",
 "Sep",
 "Oct",
 "Nov",
 "Dic");

// tooltips
Calendar._TT = {};
Calendar._TT["INFO"] = "Acerca del calendario";

Calendar._TT["ABOUT"] =
//"DHTML Date/Time Selector\n" +
//"(c) dynarch.com 2002-2003\n" + // don't translate this this ;-)
//"For latest version visit: http://dynarch.com/mishoo/calendar.epl\n" +
//"Distributed under GNU LGPL.  See http://gnu.org/licenses/lgpl.html for details." +
//"\n\n" +
"Fecha de selecci�n:\n" +
"- Utilice la carpeta \ \ xab, botones para seleccionar el a�o xbb\n" +
"- Utilice el " + String.fromCharCode(0x2039) + ", " + String.fromCharCode(0x203a) + " para seleccionar el mes\n" +
"- Mantenga pulsado el bot�n del rat�n sobre cualquiera de los botones para una selecci�n r�pida.";
Calendar._TT["ABOUT_TIME"] = "\n\n" +
"Tiempo de selecci�n:\n" +
"- Haga clic en cualquiera de las partes de la hora para aumentar\n" +
"- o May�s y haga clic para disminuirlo\n" +
"- o haga clic y arrastre para una selecci�n m�s r�pida.";

Calendar._TT["PREV_YEAR"] = "Prev. a�o (mantener para men�)";
Calendar._TT["PREV_MONTH"] = "Prev. meses (mantener para men�))";
Calendar._TT["GO_TODAY"] = "Ir Hoy";
Calendar._TT["NEXT_MONTH"] = "Mes siguiente (mantener para men�)";
Calendar._TT["NEXT_YEAR"] = "A�o siguiente (mantener para men�)";
Calendar._TT["SEL_DATE"] = "Seleccione la fecha de";
Calendar._TT["DRAG_TO_MOVE"] = "Mostrar% s primero";
Calendar._TT["PART_TODAY"] = " (hoy)";

// the following is to inform that "%s" is to be the first day of week
// %s will be replaced with the day name.
Calendar._TT["DAY_FIRST"] = "Display %s first";

// This may be locale-dependent.  It specifies the week-end days, as an array
// of comma-separated numbers.  The numbers are from 0 to 6: 0 means Sunday, 1
// means Monday, etc.
Calendar._TT["WEEKEND"] = "0,6";

Calendar._TT["CLOSE"] = "Cerrar";
Calendar._TT["TODAY"] = "Hoy";
Calendar._TT["TIME_PART"] = "(Shift-)Click or drag to change value";

// date formats
Calendar._TT["DEF_DATE_FORMAT"] = "%Y-%m-%d";
Calendar._TT["TT_DATE_FORMAT"] = "%a, %b %e";

Calendar._TT["WK"] = "sem";
Calendar._TT["TIME"] = "Tiempo:";

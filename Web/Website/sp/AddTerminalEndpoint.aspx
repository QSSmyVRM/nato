<%@ Page Language="C#" Inherits="ns_MyVRM.AddTerminalEndpoint" %>

<%--Edited for FF--%>
<%if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("MSIE"))
{%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="X-UA-Compatible" content="IE=7" /> <!-- FB 2050 -->

<%}
else {%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN"> 
<%} %>
<!-- #INCLUDE FILE="inc/maintopNET.aspx" --> 

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Create/Edit Endpoint</title>
    <script language="javascript">
function viewMCU(val)
{
	//url = "dispatcher/admindispatcher.asp?cmd=ViewBridge&bid=" + val.split("@")[0];
    var mcuid =  val.split("@")[0];
    
    if(mcuid != "-1" && mcuid != "")
    {
        url = "BridgeDetailsViewOnly.aspx?hf=1&bid="+ mcuid;
        window.open(url, "BrdigeDetails", "width=900,height=800,resizable=yes,scrollbars=yes,status=no");
    }
    return false;
}

    function CheckIPSelection(obj)
    {
        if (obj.tagName == "INPUT" && obj.type == "radio")
        {
            for (i=0; i<document.frmTerminalControl.elements.length;i++)
            {
                var obj1 = document.frmTerminalControl.elements[i];
                if (obj1.tagName == "INPUT" && obj1.type == "radio")
                    if (obj1.name != obj.name)
                        obj1.checked = false;
//                    else
//                    {
//                        var temp = obj.parentElement.parentElement.innerHTML;
//                        alert(temp);
//                    }
            }
        }
    }
        
    </script>
</head>
<body>
    <form id="frmTerminalControl" runat="server" method="post">
        <center><table border="0" width="98%" cellpadding="2" cellspacing="2">
            <tr>
                <td align="center">
                    <h3><asp:Label ID="lblHeader" runat="server" Text="Administrar punto final"></asp:Label></h3><br />
                     <asp:Label ID="errLabel" runat="server" CssClass="lblError"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <table width="100%">
                        <tr>
                            <td width="20" class="tableHeader">1</td>
                            <td class="subtitleblueblodtext" align="left">Informaci�n b�sica</td>                            
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <table width="90%">
                        <tr>
                            <%--Window Dressing--%>
                            <td align="right" width="20%" class="blackblodtext">
                                <b>nombre de punto final</b><span class="reqfldText">*</span>
                            </td>
                            <td align="left" width="30%">
                                <asp:TextBox ID="txtEndpointName" runat="server" CssClass="altText" Text="" ></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reqEndpointName" runat="server" ControlToValidate="txtEndpointName" ErrorMessage="Necesario"  ValidationGroup="Submit"></asp:RequiredFieldValidator>
                                <asp:TextBox CssClass="altText"  ID="txtEndpointID" runat="server" Visible="false"></asp:TextBox>
                            </td>
                            <%--Window Dressing--%>
                            <td align="right" width="24%" class="blackblodtext"><b>Tipo de Terminal</b></td>
                            <td align="left" width="28%"><asp:Label ID="lblTerminalType" CssClass="subtitleblueblodtext" runat="server" /></td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <asp:Table ID="tblEndpointName" runat="server" Width="100%" CellPadding="0" CellSpacing="2" >
                                    <asp:TableRow VerticalAlign="Top">
                                    <%--Window Dressing--%>
                                        <asp:TableCell Width="20%" HorizontalAlign="right" CssClass="blackblodtext"><b>Nombre del �ltimo Punto Final</b></asp:TableCell>
                                        <asp:TableCell Width="30%" HorizontalAlign="left">
                                            <asp:TextBox ID="txtEndpointLastName" runat="server" CssClass="altText" Text="" ></asp:TextBox>
                                             <%--Code added for FB : 1175 Start--%>
                                             <%--<asp:RequiredFieldValidator ID="reqtxtEndpointLastName" ControlToValidate="txtEndpointLastName" ValidationGroup="Submit" runat="server" ErrorMessage="  Required" ></asp:RequiredFieldValidator>--%><%--FB 2528--%>
                                             <%--Code added for FB : 1175 End--%>
                                        </asp:TableCell>
                                    <%--Window Dressing--%>
                                        <asp:TableCell Width="24%" HorizontalAlign="right"  class="blackblodtext" ><b>Direcci�n de correo-e del Punto Final</b><span class="reqfldText">*</span>
                                           </asp:TableCell>
                                          <asp:TableCell Width="26%">
                                            <asp:TextBox ID="txtEndpointEmail" runat="server" CssClass="altText" Text="" ></asp:TextBox>
                                             <%--Code added for FB : 1175 Start--%>
                                            <asp:RequiredFieldValidator ID="reqtxtEndpointEmail" ControlToValidate="txtEndpointEmail" ValidationGroup="Submit" runat="server" ErrorMessage="  Required" ></asp:RequiredFieldValidator>
                                             <%--Code added for FB : 1175 End--%>
                                        </asp:TableCell>                                        
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell ColumnSpan="2">
                                        </asp:TableCell>
                                        <asp:TableCell ColumnSpan="2" HorizontalAlign="Right">
                                         <%--Code added for FB : 1640 --%>
                                            <asp:RegularExpressionValidator ID="regEndPtEmail" ControlToValidate="txtEndpointEmail"  ValidationGroup="Submit" runat="server" SetFocusOnError="true" ErrorMessage="& < > ' + % \ ; ? | ^ = ! ` [ ] { } : # $ , ~ and &#34; son caracteres no v�lidos." ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>^+;?|!`\[\]{}\x22;=:#$,%&'~]*$"></asp:RegularExpressionValidator>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table width="100%">
                        <tr>
                            <td width="20" class="tableHeader">2</td>
                            <td class="subtitleblueblodtext" align="left">Par�metros del Punto Final</td>                            
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table width="100%">
                        <tr>
                                    <%--Window Dressing--%>
                            <td align="right" class="blackblodtext">
                                protocolo<span class="reqfldText">*</span></td>
                            <td align="left">
                                <asp:DropDownList CssClass="altLong0SelectFormat" ID="lstProtocol" runat="server" DataTextField="Name" DataValueField="ID">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="reqlstProtocol" ErrorMessage="Necesario" runat="server" ControlToValidate="lstProtocol" Display="dynamic" InitialValue="-1" ValidationGroup="Submit"></asp:RequiredFieldValidator>
                                </td>
                                    <%--Window Dressing--%>
                            <td align="right" class="blackblodtext">
                                Tipo de conexi�n<span class="reqfldText">*</span></td>
                            <td align="left">
                                <asp:DropDownList ID="lstConnectionType" runat="server" DataTextField="Name" DataValueField="ID" CssClass="altText"></asp:DropDownList> <%--Fogbugz case 427--%>
                                <%--Code added for FB : 1175 Start--%>
                                <asp:RequiredFieldValidator ID="reqConnectionType" ErrorMessage="Necesario" runat="server" ControlToValidate="lstConnectionType" Display="dynamic" InitialValue="-1" ValidationGroup="Submit"></asp:RequiredFieldValidator>
                                <%--Code added for FB : 1175 End--%>
                        </tr>
                        <tr>
                                    <%--Window Dressing--%>
                            <td align="right" class="blackblodtext">
                                tipo de direcci�n<span class="reqfldText">*</span></td>
                            <td align="left">
                                <asp:DropDownList CssClass="altLong0SelectFormat" ID="lstAddressType" runat="server" DataTextField="Name" DataValueField="ID"></asp:DropDownList>
                                 <%--Code added for FB : 1175 Start--%>
                                <asp:RequiredFieldValidator ID="reqAddressType" runat="server" InitialValue="-1" ControlToValidate="lstAddressType" ValidationGroup="Submit" ErrorMessage="Necesario"></asp:RequiredFieldValidator>
                                 <%--Code added for FB : 1175 End--%>
                                </td>
                                    <%--Window Dressing--%>
                            <td align="right" class="blackblodtext">
                                direcci�n<span class="reqfldText">*</span></td>
                            <td align="left">
                                <asp:TextBox CssClass="altText"  ID="txtAddress" runat="server" ></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reqAddress" ControlToValidate="txtAddress" ValidationGroup="Submit" runat="server" ErrorMessage="Necesario" ></asp:RequiredFieldValidator>
                                <%--FB 1972--%>
                                <asp:RegularExpressionValidator ID="regAddress" ControlToValidate="txtAddress" ValidationGroup="Submit" Display="dynamic" runat="server"  SetFocusOnError="true" ErrorMessage="<br>& < > + % \ / ( ) ? | ^ = ` [ ] { } $ and ~ son caracteres no v�lidos." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+?|=`\[\]{}\=^$%&()~]*$"></asp:RegularExpressionValidator> <%--FB 2267--%>
                            </td>
                        </tr>
                        <%--FB 2365 Start--%>
                        <tr>
                            <%--Window Dressing--%>
                            <td align="right" class="blackblodtext">
                                C�digo de Conferencia</td>
                            <td align="left">
                                <asp:TextBox CssClass="altText"  ID="txtconfcode" runat="server" ></asp:TextBox>
                                <asp:RegularExpressionValidator ID="regconfcode" ControlToValidate="txtconfcode" ValidationGroup="Submit" Display="dynamic" runat="server"  SetFocusOnError="true" ErrorMessage="<br>& < > + % \ / ( ) ? | ^ = ` [ ] { } $ and ~ son caracteres no v�lidos." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+?|=`\[\]{}\=^$%&()~]*$"></asp:RegularExpressionValidator> 
                            </td>
                            <%--Window Dressing--%>
                            <td align="right" class="blackblodtext">
                                PIN de l�der</td>
                            <td align="left">
                                <asp:TextBox CssClass="altText"  ID="txtLeaderpin" runat="server" ></asp:TextBox>
                                <asp:RegularExpressionValidator ID="regleaderpin" ControlToValidate="txtLeaderpin" ValidationGroup="Submit" Display="dynamic" runat="server"  SetFocusOnError="true" ErrorMessage="<br>& < > + % \ / ( ) ? | ^ = ` [ ] { } $ and ~ son caracteres no v�lidos." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+?|=`\[\]{}\=^$%&()~]*$"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <%--FB 2365 End--%>
                        <tr>
                                    <%--Window Dressing--%>
                            <td align="right" class="blackblodtext">
                                modelo<span class="reqfldText">*</span></td>
                            <td align="left">
                                <asp:DropDownList CssClass="altLong0SelectFormat" ID="lstVideoEquipment" runat="server" DataTextField="VideoEquipmentName" DataValueField="VideoEquipmentID"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="reqVideoEquipment" runat="server" InitialValue="-1" ControlToValidate="lstVideoEquipment" ValidationGroup="Submit" ErrorMessage="Necesario"></asp:RequiredFieldValidator>                                    
                                </td>
                                    <%--Window Dressing--%>
                            <td align="right" class="blackblodtext">
                                Ancho de banda preferido<span class="reqfldText">*</span></td>
                            <td align="left">
                                <asp:DropDownList CssClass="altLong0SelectFormat" ID="lstLineRate" runat="server" DataTextField="LineRateName" DataValueField="LineRateID"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="reqLineRate" runat="server" InitialValue="-1" ControlToValidate="lstLineRate" ValidationGroup="Submit" ErrorMessage="Necesario"></asp:RequiredFieldValidator>                                    
                                </td>
                        </tr>
                        <tr>
                                    <%--Window Dressing--%>
                            <td align="right" class="blackblodtext">
                                URL de acceso a la red</td>
                            <td align="left">
                                <asp:TextBox CssClass="altText"  ID="txtURL" runat="server" TextMode="SingleLine"></asp:TextBox>
                            </td>
                                    <%--Window Dressing--%>
                            <td align="right" class="blackblodtext">
                                Red exterior localizada</td>
                            <td align="left">
                                <asp:CheckBox ID="chkIsOutside" runat="server"  />
                            </td>
                        </tr>
                        <tr>
                                    <%--Window Dressing--%>
                             <td align="right" class="blackblodtext">
                                Conexi�n<span class="reqfldText">*</span></td>
                            <td align="left">
                                <%--Code added for FB : 1475 End
                                <asp:DropDownList CssClass="altLong0SelectFormat" ID="lstConnection"   DataTextField="Nombre" DataValueField="ID"></asp:DropDownList>
                                --%>
                                <asp:DropDownList CssClass="altLong0SelectFormat" runat="server" ID="lstConnection">
                                    <asp:ListItem Text="Por favor, seleccione.." Value="-1"></asp:ListItem> 
                                    <asp:ListItem Text="S�lo audio" Value="1"></asp:ListItem>  <%--FB 1744 --%>
                                    <asp:ListItem Text="Audio/video" Value="2"></asp:ListItem> <%--FB 1744 --%>
                                </asp:DropDownList>
                                 <%--Code added for FB : 1475 End --%>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" InitialValue="-1" ControlToValidate="lstConnection" ValidationGroup="Submit" ErrorMessage="Necesario"></asp:RequiredFieldValidator>                                    
                                </td>
                                    <%--Window Dressing--%>
                           <td align="right" class="blackblodtext">
                                encriptaci�n preferida
                            </td>
                            <td align="left">
                                <asp:CheckBox ID="chkEncryptionPreferred" runat="server" />
                            </td>
                        </tr>
                        <tr>
                        <td align="right" class="blackblodtext">ID de Correo-e</td> <%-- ICAL Cisco Telepresence fix--%>
                            <td align="left">
                                <asp:TextBox CssClass="altText"  ID="txtExchangeID" runat="server" Width="230px" TextMode="SingleLine"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" ControlToValidate="txtExchangeID" ValidationGroup="Submit" Display="dynamic" runat="server"  SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ ( ) ; ? | ^ = ! ` , [ ] { } # $ ~ and &#34; son caracteres no v�lidos." ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>+;?|!`,\[\]{}\x22;=^#$%&()'~]*$"></asp:RegularExpressionValidator>
                            </td>
                            <%--Api Port Starts--%>
                        <td align="right" class="blackblodtext">Puerto API</td>
                        <td align="left">
                        <asp:TextBox CssClass="altText"  ID="txtapiportno" runat="server" MaxLength="5"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txtapiportno" ValidationGroup="Submit" Display="dynamic" runat="server"  SetFocusOnError="true" ErrorMessage="Numeric values only." ValidationExpression="\d+"></asp:RegularExpressionValidator>
                        </td>
                        <%--Api Port Ends--%>
                        </tr>
                    </table>
                </td>
             </tr>
            <tr>
                <td>
                    <table width="100%">
                        <tr>
                            <td width="20" class="tableHeader">3</td>
                            <td class="subtitleblueblodtext" align="left">Par�metros MCU</td>                            
                        </tr>
                    </table>
                </td>
            </tr>
             <tr>
                <td>
                    <table width="100%">
                        <tr>
                                    <%--Window Dressing--%>
                            <td align="right" width="15%" class="blackblodtext">
                                Asignado a MCU<span class="reqfldText">*</span></td>
                            <td align="left" colspan="3">
                                <asp:DropDownList CssClass="altLong0SelectFormat" OnSelectedIndexChanged="DisplayBridgeDetails" AutoPostBack="true" ID="lstBridges" runat="server" DataTextField="BridgeName" DataValueField="BridgeID"></asp:DropDownList>
                                <input type="button" name="btnViewMCU" value="Ver" class="altShortBlueButtonFormat" onClick="javascript: viewMCU(document.frmTerminalControl.lstBridges.options[document.frmTerminalControl.lstBridges.selectedIndex].value);">
                                <asp:RequiredFieldValidator ID="reqBridges" runat="server" InitialValue="-1" ControlToValidate="lstBridges" ValidationGroup="Submit" ErrorMessage="Necesario"></asp:RequiredFieldValidator>                                    
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" align="center" >
                                <h5><font class="subtitleblueblodtext">Servicios IP </font></h5> <%-- Organization Css Module --%>
                                 <asp:DataGrid runat="server" EnableViewState="true" ID="dgIPServices" AutoGenerateColumns="false"
                                      CellSpacing="0" CellPadding="4" GridLines="None" BorderColor="blue" BorderStyle="solid" BorderWidth="1"  Width="90%">
                                   <%--Window Dressing start--%>
                                    <SelectedItemStyle CssClass="tableBody" Font-Bold="True"/>
                                    <EditItemStyle CssClass="tableBody" />
                                    <AlternatingItemStyle CssClass="tableBody" HorizontalAlign="center" />
                                    <ItemStyle CssClass="tableBody" HorizontalAlign="Center" />
                                    <%--Window Dressing end--%>
                                    <HeaderStyle CssClass="tableHeader" HorizontalAlign="center" />
                                    <Columns>
                                        <%--<asp:BoundColumn DataField="ID" Visible="false"></asp:BoundColumn>--%>
                                        <asp:TemplateColumn HeaderText="Select" HeaderStyle-CssClass="tableHeader">
                                            <ItemTemplate>
                                                <asp:RadioButton ID="rdIP" AutoPostBack="true" onclick="javascript:CheckIPSelection(this)" OnCheckedChanged="ChangeIPSettings" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:BoundColumn DataField="name" HeaderText="Nombre" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="addressType" HeaderText="tipo de direcci�n" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="address" HeaderText="direcci�n" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="networkAccess" HeaderText="Acceso a la red" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="usage" HeaderText="Uso" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody"></asp:BoundColumn> 
                                    </Columns>
                                 </asp:DataGrid>
                                 <asp:Label ID="lblNoIPServices" CssClass="lblError" Text="No hay servicios IP para este puente"  runat="server" Visible="true"></asp:Label>
                           </td>
                        </tr>
                        <tr>
                            <td colspan="4" align="center">
                                <br /><h5><font class="subtitleblueblodtext">Servicios ISDN</font></h5> <%-- Organization Css Module --%>
                                 <asp:DataGrid runat="server" EnableViewState="true" ID="dgISDNServices" AutoGenerateColumns="false"
                                      CellSpacing="0" CellPadding="4" GridLines="None" BorderColor="blue" BorderStyle="solid" BorderWidth="1"  Width="90%">
                                     <%--Window Dressing start--%>
                                    <SelectedItemStyle CssClass="tableBody" Font-Bold="True"/>
                                    <EditItemStyle CssClass="tableBody" />
                                    <AlternatingItemStyle CssClass="tableBody" />
                                    <ItemStyle CssClass="tableBody" />
                                    <%--Window Dressing end--%>
                                    <HeaderStyle CssClass="tableHeader" HorizontalAlign="center" />
                                    <Columns>
                                        <asp:BoundColumn DataField="SortID" Visible="false"></asp:BoundColumn>
                                        <asp:TemplateColumn HeaderText="Select" HeaderStyle-CssClass="tableHeader">
                                            <ItemTemplate>
                                                <asp:RadioButton ID="rdISDN" onclick="javascript:CheckIPSelection(this)" AutoPostBack=true  OnCheckedChanged="ChangeISDNSettings" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:BoundColumn DataField="name" HeaderText="Nombre" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="prefix" HeaderText="Prefijo" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="startRange" HeaderText="Rango de Inicio" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="endRange" HeaderText="Rango de Finalizaci�n" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="networkAccess" HeaderText="Acceso a la red" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="usage" HeaderText="Uso" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody"></asp:BoundColumn> 
                                    </Columns>
                                 </asp:DataGrid>
                                 <asp:Label ID="lblNoISDNServices" CssClass="lblError" Text="No hay servicios ISDN para este puente" runat="server" Visible="true"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                                    <%--Window Dressing--%>
                            <td align="right" class="blackblodtext">Direcci�n del Servicio MCU<span class="reqfldText">*</span></td>
                            <td align="left"><asp:TextBox CssClass="altText" ID="txtMCUServiceAddress" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reqMCU" ControlToValidate="txtMCUServiceAddress" ErrorMessage="Necesario" Display="Dynamic" runat="server" ValidationGroup="Submit" ></asp:RequiredFieldValidator>
                            </td>
                                    <%--Window Dressing--%>
                            <td align="right" class="blackblodtext">Tipo de Direcci�n MCU</td>
                            <td align="left">
                                <asp:DropDownList ID="lstMCUAddressType" CssClass="altLong0SelectFormat" runat="server" DataTextField="Name" DataValueField="ID"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="reqMCUAT" ErrorMessage="Necesario" runat="server" ControlToValidate="lstMCUAddressType" Display="dynamic" InitialValue="-1" ValidationGroup="Submit"></asp:RequiredFieldValidator>
                            </td>
                            
                        </tr>
                    </table>
                </td>
             </tr>   
             <tr>
                <td align="center">
                    <asp:Button ID="btnCancel" runat="server" CssClass="altShortBlueButtonFormat" Text="Cancelar" OnClick="CancelEndpoint" />
                    <asp:Button ID="btnSubmit" runat="server" CssClass="altLongBlueButtonFormat" Text="Entregar/Regresar" OnClick="SubmitEndpoint" ValidationGroup="Submit" />
                </td>
             </tr>
        </table>
</center>
                <input type="hidden" id="helpPage" value="29">
    </form>
    <script language="javascript">
</script>
</body>
</html>
<script type="text/javascript" src="inc/softedge.js"></script>

<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
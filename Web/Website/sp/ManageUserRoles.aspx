<%@ Page Language="C#" AutoEventWireup="true" EnableEventValidation="false" Inherits="en_ManageUserRoles" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <!-- FB 2050 -->
<meta http-equiv="X-UA-Compatible" content="IE=7" /> <!-- FB 2050 -->

<!--Window Dressing-->
<!-- #INCLUDE FILE="inc/maintopNET.aspx" --> 

<html >
<head runat="server">
    <title>myVRM</title>
    <script type="text/javascript" src="inc/functions.js"></script>
    <script type="text/javascript" src="script/errorList.js"></script>
    <script language="javascript">
<!--

	cansubmit = true;
	newroleno = 0;
	
	function selectRole(cb)
	{
	    
	    
		document.frmManageuserroles.RoleName.disabled = false;	
		
		document.getElementById("RNtxt").innerHTML="";
		urary = ((document.frmManageuserroles.userroles.value).split("##"))[cb.selectedIndex].split("@@");
		
		document.frmManageuserroles.roleidx.value = cb.selectedIndex;
		document.frmManageuserroles.RoleID.value = urary[0];
		document.frmManageuserroles.RoleName.value = urary[1];
		document.frmManageuserroles.MenuMask.value = urary[2];
		document.frmManageuserroles.Active.value = urary[3];
		document.frmManageuserroles.Locked.value = urary[4];
		document.frmManageuserroles.CreateType.value = urary[5]; // FB 1968
		
		ifrmMenumasklist.init();
		
		if (urary[3] == "1") {
			document.frmManageuserroles.RoleName.disabled = true;
			document.frmManageuserroles.ManageuserrolesDelete.disabled = true;
			document.getElementById("RoleMessage").innerHTML = "<b><font size='1'>El rol seleccionado est� actualmente asignado a una plantilla de grupo y no puede ser editado ni eliminado.</font></b>";  // Active: role is being used by some user template(s)
			return 1;
		}
		// FB 1968 Starts
		if (urary[4] == "1")
		 {
		    if((urary[5]) =="1")
		    {
			    document.frmManageuserroles.RoleName.disabled = true;
			    document.frmManageuserroles.ManageuserrolesDelete.disabled = true;
			    document.getElementById("RoleMessage").innerHTML = "<b><font color='#FF00FF' size='1'>El Rol del Sistema por defecto no se puede eliminar ni editar.</font></b>";
			    return 1;
			}
			else
			{
			    document.frmManageuserroles.RoleName.disabled = true;
			    document.frmManageuserroles.ManageuserrolesDelete.disabled = true;
			    document.getElementById("RoleMessage").innerHTML = "<b><font color='#FF00FF' size='1'>El Rol Personalizado no se puede eliminar ni editar.</font></b>";
			    return 1;
			}
		}
		// FB 1968 Ends 
		
		document.frmManageuserroles.RoleName.disabled = false;
		document.frmManageuserroles.ManageuserrolesDelete.disabled = false;
		document.getElementById("RoleMessage").innerHTML="";
	}


	function DelUserRole()
	{
		cb = document.frmManageuserroles.UserRole;

		if (cb.selectedIndex >-1) {
		    var isRemoveGroup = confirm("�Est� seguro de que quiere eliminar este rol del usuario?")
			if (isRemoveGroup == false) {
				return;
			}
			
			ursary = (document.frmManageuserroles.userroles.value).split("##");
		
			newstr = "";
			for (i = 0; i < cb.selectedIndex; i++)
				newstr += ursary[i] + "##";
			for (i = cb.selectedIndex+1; i < ursary.length-1; i++)
				newstr += ursary[i] + "##";
			document.frmManageuserroles.userroles.value = newstr;

			cb.options[cb.selectedIndex] = null;
			cb.selectedIndex = -1;
			document.frmManageuserroles.roleidx.value = "-1";
			document.frmManageuserroles.RoleID.value = "";
			document.frmManageuserroles.RoleName.value = "";
			document.frmManageuserroles.MenuMask.value = "8*0-4*0+8*0+5*0+2*0+8*0+2*0+2*0+2*0+1*0-6*0"; //FB 1779 //FB 2023 DD2
			ifrmMenumasklist.init();
		}

		document.frmManageuserroles.ManageuserrolesDelete.disabled = true;
		document.frmManageuserroles.RoleName.disabled = true;	
		document.getElementById("RNtxt").innerHTML="<b><font size='1'><li>Role Name field is now uneditable until one specific role in left list is selected.<li>Please see above instruction for details.</font></b>";
	}
	
	//Function added by Vivek as a fix for Issue number 267
	//This function will iterate through existing UserRoles and if it exists then won't add to the list
	function checkifUserRoleExists()
	{
	   
//	    alert(document.frmManageuserroles.RoleName.value);
//	    alert(document.frmManageuserroles.UserRole.length);
	    
	    for(i = 0;i< document.frmManageuserroles.UserRole.length-1;i++)
	    {
        //alert(document.frmManageuserroles.UserRole.options[i].text);
	        //alert(document.frmManageuserroles.RoleName.value + " (Custom)");
            if(document.frmManageuserroles.UserRole.options[i].text == document.frmManageuserroles.RoleName.value + "(Custom)")
            {
                alert("Error: Ya existe un usuario con el mismo nombre");
                return false;
            }
        }
        return true;
    }

	function AddUserRole()
	{
	    //Function added by Vivek as a fix for Issue number 267
	    if(checkifUserRoleExists()== false)
	        return 0;
	        
		newroleno++;
		
		var newoption = new Option("[New User Role " + newroleno +"] (Custom)", "new", true, true);
		
 		cb = document.frmManageuserroles.UserRole;
		cb.options[cb.length] = newoption;
		
		document.frmManageuserroles.userroles.value += "new@@[New User Role " + newroleno +"]@@8*120-4*0+8*0+5*0+2*0+8*0+2*0+2*0+2*0+1*0-6*0@@0##"; //FB 1779 //FB 2023 DD2
		document.frmManageuserroles.roleidx.value = cb.length-1; 
		document.frmManageuserroles.RoleID.value = "new";
		document.frmManageuserroles.RoleName.value = "[New User Role " + newroleno + "]";
		document.frmManageuserroles.MenuMask.value = "8*120-4*0+8*0+5*0+2*0+8*0+2*0+2*0+2*0+1*0-6*0"; //FB 1779 //FB 2023 DD2
		document.getElementById("Active").value = "0";
		document.getElementById("Locked").value = "0";
		document.getElementById("RoleMessage").innerHTML=""; //FB 311
		ifrmMenumasklist.init(); 

		document.frmManageuserroles.RoleName.disabled = false;	
		document.getElementById("RNtxt").innerHTML="";
		document.frmManageuserroles.ManageuserrolesDelete.disabled = ( (document.frmManageuserroles.userroles.value == "") ? true : false);
		
	}
	
	
	function save () 
	{
		if (!ifrmMenumasklist.needblock) {
			idx = parseInt(document.frmManageuserroles.roleidx.value, 10);
			ursary = (document.frmManageuserroles.userroles.value).split("##");
			ursary[idx] = document.frmManageuserroles.RoleID.value + "@@" 
						+ document.frmManageuserroles.RoleName.value + "@@"
						+ document.frmManageuserroles.MenuMask.value + "@@"
						+ document.frmManageuserroles.Active.value + "@@"
						+ document.frmManageuserroles.CreateType.value + "@@"  // FB 1968
						+ document.frmManageuserroles.Locked.value;
		
			newstr = "";
			for (i = 0; i < ursary.length-1; i++) {
				newstr += ursary[i] + "##";
			}
			document.frmManageuserroles.userroles.value = newstr;
		}
	}
		
	function syn (val) 
	{
		document.frmManageuserroles.UserRole.options[parseInt(document.frmManageuserroles.roleidx.value, 10)].text = val + "(Custom)";
	}
	
	function chksyn (val) 
	{
		syn(val);
		if (Trim(val) == "") {
		    alert("Error: por favor, introduzca alg�n valor para el Nombre del Rol.");
			document.frmManageuserroles.RoleName.focus();
			cansubmit = false;
			save ();
		} else {
		
			cansubmit = true;
			save ();
			
		}
	}

    
	function frmManageuserroles_Validator ()
	{
	    var sharedEnv, admin
	    
	    sharedEnv = '<%=Application["sharedEnv"]%>';
	    admin = '<%=Session["admin"]%>';
	    
		if ( (sharedEnv == "1") && (admin != "2"))
		{
		    alert("Esta funci�n est� desactivada debido a que se comparte el sitio de evaluaci�n. En la versi�n completa, esta funci�n estar� activa.");
			return false;
		}
        //Added by manisha to fix the issue no 267
          for(i = 0;i< document.frmManageuserroles.UserRole.length;i++)
	    {
	        
	        for(j = 0;j< document.frmManageuserroles.UserRole.length;j++)
	       {	         
	           if(i!=j)
	           { 
                   if(document.frmManageuserroles.UserRole.options[i].text == document.frmManageuserroles.UserRole.options[j].text)
                    {
                        alert("Error: Nombre de Rol ya existe. Por favor, introduzca un nombre �nico.");
                       document.frmManageuserroles.UserRole.options[j].selected = true;
                       selectRoleaftersubmit(j);
                       return false;
                    }
                }
                
                 
          }
        }
        

        //end
        if (!cansubmit) {
            alert("Error: Hay un Rol de usuario sin nombre. Por favor, as�gnele un nombre.")
            return false;
        }
        else
            return true;
	}
	
	function selectRoleaftersubmit(cb)
	{
	  if(cb!= "")
	  {  
	    
		document.frmManageuserroles.RoleName.disabled = false;	
		
		document.getElementById("RNtxt").innerHTML="";
		urary = ((document.frmManageuserroles.userroles.value).split("##"))[cb].split("@@");
		
		document.frmManageuserroles.roleidx.value = cb;
		document.frmManageuserroles.RoleID.value = urary[0];
		document.frmManageuserroles.RoleName.value = urary[1];
		document.frmManageuserroles.MenuMask.value = urary[2];
		document.frmManageuserroles.Active.value = urary[3];
		document.frmManageuserroles.Locked.value = urary[4];
		document.frmManageuserroles.CreateType.value = urary[5]; // FB 1968
		
		ifrmMenumasklist.init();
		
		
		
		document.frmManageuserroles.RoleName.disabled = false;
		document.frmManageuserroles.ManageuserrolesDelete.disabled = false;
		document.getElementById("RoleMessage").innerHTML="";
	}
	}


//-->
</script>
</head>
<body>
    
    
        <h3>Administrar funciones del usuario</h3>
        <%--Window Dressing--%>
        <h3><asp:Label ID="MsgLbl" runat="server" CssClass="lblError" ></asp:Label></h3>
  
    <form id="frmManageuserroles" method="post" runat="server">
    
    <input type="hidden" name="cmd" value="SetUserRoles" />
    <input type="hidden" name="userroles" id="userroles" runat="server" />
    <input type="hidden" name="submit" id="submit" runat="server" />
    <input type="hidden" name="roleidx" value="-1" />
    <input type="hidden" name="RoleID" value="" />
    <input type="hidden" name="MenuMaskOLD" value="5*0-6*0+7*0+3*0+6*0+2*0+2*0+5*0+8*0+8*0+2*0+3*0+3*0+3*0+3*0-6*0" />
    <input type="hidden" name="MenuMask" value="8*0-4*0+8*0+5*0+2*0+8*0+2*0+2*0+2*0+1*0-6*0" /> <%--//FB 1779 && FB 2023 DD2--%>
    <input type="hidden" name="Active" id="Active" value="" />
    <input type="hidden" name="Locked" id="Locked" value="" />
    <input type="hidden" id="helpPage" value="103" />
    <input type="hidden" name="CreateType" id="CreateType" value= "" /> <%--FB 1968--%>
    
    <center>
            
            <table border="0" cellpadding="0" cellspacing="0" style="width:90%" >
                <tr>
                    <td style="width:3%" valign="top">
                      &nbsp;
                    </td>
                    <td>&nbsp;</td>
                    <td  style="width:96%;height:20" align="left">
                    
		                <span class="subtitleblueblodtext">
		                    Funcion del Usuario 
		                    <br />
		                </span>
                        
                        <ul>
                            <li>
                                <span class="blackblodtext">
                                    <b>Para crear:</b> haga 'clic' en el bot�n "Crear", introduzca un nombre nuevo para la funcion del usuario, luego defina la funcion del usuario usando el panel de selecci�n del men�.
                                </span>
                            </li>
                            <li>
                                <span class="blackblodtext">
		                            <b>Para Editar:</b> seleccione la funcion del usuario en el panel "funcion del usuario", despues modifique el "nombre de la funcion" y "seleccion del menu" como requisitos.
		                        </span>
		                    </li>
                            <li>
                                <span class="blackblodtext">
		                            <b>Para eliminar un rol personalizado:</b> Para eliminar una funcion personalizada: Seleccione una funcion personalizada del panel del usuario, luego haga 'clic' en el boton "Eliminar".
		                            <br />
                                </span>
                                <span class="blackblodtext">
		                            <b>Cuando haya completado los cambios, haga 'clic' en el bot�n "Entregar".</b>
		                            <br />
                                </span>
                            </li>
                        </ul>
                    </td>
              </tr>
              <tr>
                <td style="height:5"></td>
                <td></td>
                <td></td>
              </tr>
              <tr>
                <td></td>
                <td></td>
                <td align="center" >
                        <table width="100%" border="0" cellspacing="1" cellpadding="1">
                            <tr> 
                                <td align="left" style="width:25%"> 
                                    <span class="tableblackblodtext">Rol de Usuario</span>
                                </td>
                                <td style="width:5%"></td>
                                <td align="left" style="width:15%">
                                    <span class="tableblackblodtext">Nombre del Rol</span>
                                </td>
                                <td style="width:5%"></td>
                                <td align="left" style="width:50%"> 
                                    <span class="tableblackblodtext">selecci�n de men�</span>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top">
                                    <asp:ListBox ID="UserRole" runat="server" Height="300"  CssClass="altLong0SelectFormat" onchange='JavaScript:selectRole(this);'></asp:ListBox>
                                    <table width="250">
				                        <tr>
				                            <td id="RNtxt" runat="server">
				                            </td>
				                        </tr>
				                        <tr>
					                        <td align="left" valign="top" id="RoleMessage"></td>
				                        </tr>
				                        
				                    </table>
                                </td>
                                <td rowspan="4"></td>
                                <td align="left" valign="top" rowspan="4">
                                    <input type="text" id="RoleName" size="20" value="" class="altText" onchange="chksyn(this.value)"  runat="server" onkeyup="syn(this.value);chkLimit(this,'2');" /><br>
                                </td>
                                <td style="width:5%" rowspan="4"></td>
                                <td align="left" valign="top" rowspan="4">
                                    <iframe src="usermenucontroller.aspx?f=frmManageuserroles&n=0&wintype=ifr" id="ifrmMenumasklist" name="ifrmMenumasklist" width="400" height="400"> <%--Edited for FF--%>
                                        <p>Ir a<a href="usermenucontroller.asp?f=frmManageuserroles&n=0&wintype=ifr">participantes</a></p>
                                    </iframe>
                                </td>
                            </tr>
                            <tr>
                              <td align="right">
                                    <input type="button" id="ManageuserrolesDelete" value="eliminar" runat="server" class="altShortBlueButtonFormat" onclick="JavaScript: DelUserRole();" />
                              </td>
                            </tr>
                            <tr>
                              <td align="right">
                                <input type="button" id="ManageuserrolesCreate" value="Crear" runat="server" onClick="JavaScript: AddUserRole();" class="altShortBlueButtonFormat" />
                              </td>
                            </tr>
                        </table> 
                    </td>
                </tr>
                 <tr>
                    <td style="height:5"  colspan="3" align="center" valign="middle">
                        
                    </td>
                  </tr>
                  
                  <tr>
                    <td style="height:5" colspan="3"></td>
                  </tr>
                  <tr>
                    <td></td>
                    <td></td>
                    <td align="right">
                        <table width="70%" border="0" cellspacing="0" cellpadding="2">
                          <tr>
	                        <td>
	                            <input type="reset" name="reset" value="reajustar" class="altLongBlueButtonFormat" onClick="JavaScript:history.go(0);"/><!-- onclick="JavaScript: history.go(0);" -->
	                        </td>
	                        <td>
	                            <asp:Button ID="ManageuserrolesSubmit" Text="Entregar" CssClass="altLongYellowButtonFormat" runat="server" OnClick="SubmitClick" />
	                        </td>
                          </tr>
                        </table>
                    </td>
                </tr>
            </table>
    </center>
    </form>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
     <!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
</body>
</html>

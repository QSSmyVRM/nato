﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RoomSearch.aspx.cs" Inherits="en_RoomSearch"
    EnableEventValidation="false" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v10.2, Version=10.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v10.2, Version=10.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="MetaBuilders.WebControls.ComboBox" Namespace="MetaBuilders.WebControls"
    TagPrefix="mbcbb" %>

<script type="text/javascript" src="script/errorList.js"></script>

<script type="text/javascript" language="JavaScript" src="inc/functions.js"></script>

<script type="text/javascript" src="extract.js"></script>

<script type="text/javascript" src="script\mousepos.js"></script>

<script type="text/javascript" src="script\showmsg.js"></script>

<script type="text/javascript" src="script/CallMonitorJquery/jquery.1.4.2.js"></script>

<script type="text/javascript" src="script/CallMonitorJquery/jquery.bpopup-0.7.0.min.js"></script>

<script type="text/javascript" src="script/CallMonitorJquery/Point2Point.js"></script>

<script type="text/javascript" src="script/CallMonitorJquery/json2.js"></script>

<script type="text/javascript">
    // <![CDATA[


    function pageBarFirstButton_Click() {

        try {

            var drp = document.getElementById("DrpDwnListView");

            if (drp) {
                if (drp.value == "1")
                    grid2.GotoPage(0);
                else
                    grid.GotoPage(0);
            }
        }
        catch (exception)
{ }

    }

    function pageBarPrevButton_Click() {
        try {
            var drp = document.getElementById("DrpDwnListView");

            if (drp) {

                if (drp.value == "1")
                    grid2.PrevPage();
                else
                    grid.PrevPage();
            }
        }
        catch (exception)
{ }



    }
    function pageBarNextButton_Click() {
        try {
            var drp = document.getElementById("DrpDwnListView");

            if (drp) {
                if (drp.value == "1")
                    grid2.NextPage();
                else
                    grid.NextPage();
            }
        }
        catch (exception)
{ }


    }
    function pageBarLastButton_Click(s, e) {
        try {
            var drp = document.getElementById("DrpDwnListView");

            if (drp) {
                if (drp.value == "1")
                    grid2.GotoPage(grid2.cpPageCount - 1);
                else
                    grid.GotoPage(grid.cpPageCount - 1);
            }
        }
        catch (exception)
{ }

    }
    function pageBarTextBox_Init(s, e) {
        try {
            s.SetText(s.cpText);
        }
        catch (exception)
{ }
    }
    function pageBarTextBox_KeyPress(s, e) {
        try {

            if (e.htmlEvent.keyCode != 13)
                return;
            e.htmlEvent.cancelBubble = true;
            e.htmlEvent.returnValue = false;
            var pageIndex = (parseInt(s.GetText()) <= 0) ? 0 : parseInt(s.GetText()) - 1;


            var drp = document.getElementById("DrpDwnListView");

            if (drp) {
                if (drp.value == "1")
                    grid2.GotoPage(pageIndex);
                else
                    grid.GotoPage(pageIndex);
            }
        }
        catch (exception)
{ }



    }
    function pageBarTextBox_ValueChanged(s, e) {
        try {

            var pageIndex = (parseInt(s.GetText()) <= 0) ? 0 : parseInt(s.GetText()) - 1;

            var drp = document.getElementById("DrpDwnListView");

            if (drp) {
                if (drp.value == "1")
                    grid2.GotoPage(pageIndex);
                else
                    grid.GotoPage(pageIndex);
            }
        }
        catch (exception)
{ }

    }
    function pagerBarComboBox_SelectedIndexChanged() {
        try {
            var drp = document.getElementById("DrpDwnListView");

            if (drp) {
                if (drp.value == "1")
                    grid2.PerformCallback(pagerBarComboBox_SelectedIndexChanged.arguments[0].value);
                else
                    grid.PerformCallback(pagerBarComboBox_SelectedIndexChanged.arguments[0].value);
            }
        }
        catch (exception)
{ }
    }

    // ]]>
</script>

<link rel="stylesheet" title="Expedite base styles" type="text/css" href="<%=Session["OrgCSSPath"]%>" />

<script type="text/javascript" src="script/mytreeNET.js"></script>

<script type="text/javascript">
    var servertoday = new Date(parseInt("<%=DateTime.Now.Year%>", 10), parseInt("<%=DateTime.Now.Month%>", 10) - 1, parseInt("<%=DateTime.Now.Day%>", 10),
      parseInt("<%=DateTime.Now.Hour%>", 10), parseInt("<%=DateTime.Now.Minute%>", 10), parseInt("<%=DateTime.Now.Second%>", 10));
</script>

<%--FB 1861--%>
<%--<script type="text/javascript" src="script/cal.js"></script>--%>

<script type="text/javascript" src="script/cal-flat.js"></script>

<script type="text/javascript" src="lang/calendar-en.js"></script>

<script type="text/javascript" src="script/calendar-setup.js"></script>

<script type="text/javascript" src="script/calendar-flat-setup.js"></script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Búsqueda de Salón</title>
    <%--<link rel="stylesheet" type="text/css" media="all" href="css/aqua/theme.css" title="Aqua" />--%>
    <link rel="stylesheet" type="text/css" media="all" href="css/calendar-blue.css" />
    <%--FB 1861--%>
    <%--FB 1982--%>
</head>
<%--FB 1861--%>
<!--  #INCLUDE FILE="../en/inc/Holiday.aspx"  -->
<body style="margin: 0 0 0 0">
    <form id="FrmRoomSearch" runat="server" class="tabContents">
    <asp:ScriptManager ID="RoomsearchScript" runat="server">
    </asp:ScriptManager>
    <input type="hidden" id="cmd" value="GetSettingsSelect" />
    <input type="hidden" id="helpPage" value="84" />
    <input type="hidden" id="hdnRoomIDs" runat="server" />
    <input runat="server" id="IsSettingsChange" type="hidden" />
    <input type="hidden" id="hdnCapacityH" runat="server" />
    <input type="hidden" id="hdnCapacityL" runat="server" />
    <input type="hidden" id="hdnAV" runat="server" value="0" />
    <input type="hidden" id="hdnMedia" runat="server" />
    <input type="hidden" id="hdnLoc" runat="server" value="0" />
    <input type="hidden" id="hdnName" runat="server" />
    <input type="hidden" id="hdnZipCode" runat="server" />
    <input type="hidden" id="hdnAvailable" runat="server" />
    <div id="dataLoadingDIV">
    </div>
    <%
        if (Request.QueryString["hf"] != null)
        {
            if (Request.QueryString["hf"].ToString() == "1")
            {
    %>
    <table width="100%" border="0">
        <tr>
            <td align="center">
                <h3>
                    Búsqueda de Salón
                    <input type="button" name="Cerrar" onfocus="this.blur()" id="close" value="Cerrar"
                        class="altShort2BlueButtonFormat" onclick="javascript:ClosePopup();">
                </h3>
            </td>
        </tr>
    </table>
    <%              
                
        }
        }   
    %>
    <div class="tabContents" style="height: 545px; vertical-align: super; width: 100%;">
        <table width="100%" class="tabContents" border="0px">
            <tr valign="top">
                <td style="width: 22%">
                    <br />
                    <asp:Panel ID="Filters" runat="server" Height="550px" CssClass="treeSelectedNode"
                        BorderColor="Blue" BorderStyle="Solid" BorderWidth="1px" ScrollBars="Auto">
                        <table width="100%">
                            <tr>
                                <td>
                                    <asp:Panel ID="FavPnl" runat="server">
                                        <table width="94%">
                                            <tr id="trActDct" runat="server" style="display: none;">
                                                <td>
                                                    <span class="blackblodtext">Mostrar :</span>
                                                    <asp:DropDownList ID="DrpActDct" CssClass="altText" Width="125" runat="server" AutoPostBack="false"
                                                        onchange="javascript:ShowActDct()">
                                                        <asp:ListItem Value="0">Sólo Activado</asp:ListItem>
                                                        <asp:ListItem Value="1">Sólo Desactivado</asp:ListItem>
                                                        <asp:ListItem Value="2">Todo</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:CheckBox runat="server" ID="chkFavourites" Text=" Buscar sólo en favoritos "
                                                        onclick="javascript:ChkFavorites()" />
                                                </td>
                                            </tr>
                                            <%--FB 2426 Start--%>
                                            <tr id="trGuestRooms" runat="server">
                                                <td>
                                                    <asp:CheckBox runat="server" ID="chkGuestRooms" Text=" Buscar Salones de invitados " onclick="javascript:ChkGuestRooms()" />
                                                </td>
                                            </tr>
                                            <%--FB 2426 End--%>
                                            <tr id="trchkVMR" runat="server">
                                                <%--FB 2448--%>
                                                <td>
                                                    <asp:CheckBox runat="server" ID="chkIsVMR" Text=" Buscar VMR " onclick="javascript:ChkVirtualMeetingRooms()" />
                                                </td>
                                            </tr>
                                            <tr id="trAvlChk" runat="server">
                                                <td>
                                                    <asp:CheckBox runat="server" ID="Available" Text=" Mostrar sólo disponibles" onclick="javascript:EndDateValidation()" />
                                                </td>
                                            </tr>
                                            <tr id="trDateFromTo" runat="server">
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td align="left" class="blackblodtext" nowrap>
                                                                Desde:
                                                                <asp:TextBox ID="txtRoomDateFrom" runat="server" Width="65px" Text="" CssClass="altText" />
                                                                <img src="image/calendar.gif" border="0" width="20" height="20" id="cal_triggerFrom"
                                                                    style="cursor: pointer;" title="Date selector" onclick="return showCalendar('txtRoomDateFrom', 'cal_triggerFrom', 0, '<%=format%>');" />
                                                                <asp:RequiredFieldValidator ID="reqRoomFrom" Enabled="false" ControlToValidate="txtRoomDateFrom"
                                                                    Display="dynamic" ErrorMessage="Necesario" ValidationGroup="DateSubmit" runat="server" />
                                                                <mbcbb:ComboBox ID="confRoomStartTime" runat="server" CssClass="altSelectFormat"
                                                                    Rows="10" CausesValidation="True" Width="60px" AutoPostBack="false">
                                                                    <asp:ListItem Text="12:00 AM">
                                                                    </asp:ListItem>
                                                                </mbcbb:ComboBox>
                                                                <asp:RequiredFieldValidator ID="reqRoomStartTime" runat="server" ControlToValidate="confRoomStartTime"
                                                                    Display="Dynamic" ErrorMessage="La hora es necesaria" />
                                                                <asp:RegularExpressionValidator ID="regRoomStartTime" runat="server" ControlToValidate="confRoomStartTime"
                                                                    Display="Dynamic" ErrorMessage="Hora no válida (HH:MM AM/PM)" ValidationExpression="[0|1][0-9]:[0-5][0-9] [a|A|p|P][M|m]" />
                                                                <%-- FB Case 371 Saima --%>
                                                            </td>
                                                        </tr>
                                                        <tr id="TrRoomAvaible" runat="server">
                                                            <td align="left" class="blackblodtext" nowrap>
                                                                To:&nbsp;&nbsp;&nbsp;&nbsp;
                                                                <asp:TextBox ID="txtRoomDateTo" Width="65px" runat="server" Text="" CssClass="altText" />
                                                                <img src="image/calendar.gif" border="0" width="20" height="20" id="cal_triggerTo"
                                                                    style="cursor: pointer;" title="Date selector" onclick="return showCalendar('txtRoomDateTo', 'cal_triggerTo', 0, '<%=format%>');" />
                                                                &nbsp;<mbcbb:ComboBox ID="confRoomEndTime" runat="server" CssClass="altSelectFormat"
                                                                    Rows="10" Width="60px" CausesValidation="True" AutoPostBack="false">
                                                                    <asp:ListItem Text="12:00 AM">
                                                                    </asp:ListItem>
                                                                </mbcbb:ComboBox>
                                                                <asp:RequiredFieldValidator ID="reqRoomEndTime" runat="server" ControlToValidate="confRoomEndTime"
                                                                    Display="Dynamic" ErrorMessage="Es necesaria la Hora" />
                                                                <asp:RegularExpressionValidator ID="regRoomEndTime" runat="server" ControlToValidate="confRoomEndTime"
                                                                    Display="Dynamic" ErrorMessage="Hora inválida (HH:MM AM/PM)" ValidationExpression="[0-1][0-9]:[0-5][0-9] [A|a|P|p][M|m]" />
                                                                <asp:RequiredFieldValidator ID="reqRoomTo" Enabled="false" ControlToValidate="txtRoomDateTo"
                                                                    Display="dynamic" ErrorMessage="Necesario" ValidationGroup="DateSubmit" runat="server" />
                                                                <br />
                                                                <input type="button" name="DateSubmit" runat="server" id="DateSubmit" value="Entregar"
                                                                    class="altShortBlueButtonFormat" style="width: 65px;" onclick="javascript:EndDateValidation('1')">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <hr style="height: 2px; color: Black;" />
                                </td>
                            </tr>
                            <tr style="display: none;">
                                <td>
                                    <ajax:CollapsiblePanelExtender ID="ExtenderName" runat="server" TargetControlID="NameTable"
                                        ImageControlID="RmNameImg" CollapseControlID="RmNameImg" ExpandControlID="RmNameImg"
                                        ExpandedImage="image/loc/nolines_minus.gif" CollapsedImage="image/loc/nolines_plus.gif"
                                        Collapsed="false" CollapsedSize="30" />
                                    <asp:Panel ID="NameTable" runat="server">
                                        <table class="treeSelectedNode" width="100%">
                                            <tr>
                                                <td class="tableHeader">
                                                    <img id="RmNameImg" runat="server" src="image/loc/nolines_plus.gif" />
                                                    <span class="">nombre del salón</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="TxtNameSearch" CssClass="altText" runat="server" />
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    <input type="button" name="NameSubmit" value="Entregar" class="altShortBlueButtonFormat"
                                                        onfocus="this.blur()" style="width: 65px;" onclick="javascript:NameSearch()">
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr align="center" style="display: none;">
                                <td align="center">
                                    <span class="blackbigblodtext">OR</span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <ajax:CollapsiblePanelExtender ID="CapacityExtender" runat="server" TargetControlID="CapacityPanel"
                                        ImageControlID="CapacitImg" CollapseControlID="CapacitImg" ExpandControlID="CapacitImg"
                                        ExpandedImage="image/loc/nolines_minus.gif" Collapsed="true" CollapsedImage="image/loc/nolines_plus.gif"
                                        CollapsedSize="30" />
                                    <asp:Panel ID="CapacityPanel" runat="server">
                                        <table class="treeSelectedNode" width="100%">
                                            <tr>
                                                <td class="tableHeader">
                                                    <img id="CapacitImg" runat="server" src="image/loc/nolines_plus.gif" />
                                                    <span class="">Capacidad</span>&nbsp;&nbsp;[&nbsp;<asp:Label ID="LBLCapacity" CssClass="blueblodtexthover"
                                                        Text="Cualquier" ForeColor="Blue" runat="server" />
                                                    &nbsp;]
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:HyperLink ID="Any" Style="cursor: pointer;" runat="server" Text="Cualquier" onclick="javascript:ChangeLbl('Any','Any')" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:HyperLink ID="Ten" Style="cursor: pointer;" runat="server" Text="0 - 10" onclick="javascript:ChangeLbl('Ten','0 - 10')" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:HyperLink ID="Twenty" Style="cursor: pointer;" runat="server" Text="11 - 20"
                                                        onclick="javascript:ChangeLbl('Twenty','11 - 20')"></asp:HyperLink>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:HyperLink ID="TwentFive" Style="cursor: pointer;" runat="server" Text="20+"
                                                        onclick="javascript:ChangeLbl('TwentFive','20+')" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="TxtSearchL" CssClass="altText" onkeyup="javascript:chkLimit(this,'u');"
                                                        Width="30px" runat="server" />
                                                    &nbsp;&nbsp;-&nbsp;&nbsp;
                                                    <asp:TextBox ID="TxtSearchH" CssClass="altText" onkeyup="javascript:chkLimit(this,'u');"
                                                        Width="30px" runat="server" />
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    <input type="button" name="Submit" value="Entregar" class="altShortBlueButtonFormat"
                                                        style="width: 65px;" onclick="javascript:ChangeLbl('','')">
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <ajax:CollapsiblePanelExtender ID="AvExtender" runat="server" TargetControlID="AVTable"
                                        CollapseControlID="AVImg" ImageControlID="AVImg" ExpandControlID="AVImg" ExpandedImage="image/loc/nolines_minus.gif"
                                        Collapsed="true" CollapsedImage="image/loc/nolines_Plus.gif" CollapsedSize="30" />
                                    <asp:Panel ID="AvTable" runat="server">
                                        <table class="treeSelectedNode" width="100%">
                                            <tr>
                                                <td class="tableHeader">
                                                    <img id="AVImg" runat="server" src="image/loc/nolines_plus.gif" />
                                                    <span class="">Artículos de AV</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:CheckBoxList runat="server" ID="AVlist" onclick="javascript:AVItemChanged()"
                                                        DataValueField="Name" DataTextField="Name" />
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional" RenderMode="Inline">
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="BtnUpdateStates" />
                                        </Triggers>
                                        <ContentTemplate>
                                            <ajax:CollapsiblePanelExtender ID="LocExtender" runat="server" TargetControlID="LocPanel"
                                                ImageControlID="CntryImg" CollapseControlID="CntryImg" ExpandControlID="CntryImg"
                                                CollapsedImage="image/loc/nolines_plus.gif" Collapsed="true" ExpandedImage="image/loc/nolines_Minus.gif"
                                                CollapsedSize="30" />
                                            <asp:Panel ID="LocPanel" runat="server">
                                                <table class="treeSelectedNode" width="100%">
                                                    <tr>
                                                        <td class="tableHeader">
                                                            <img id="CntryImg" runat="server" src="image/loc/nolines_plus.gif" />
                                                            <span class="">País/Estado/Código Postal</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <span class="blackblodtext">País</span>
                                                            <asp:DropDownList ID="lstCountry" CssClass="altText" Width="125" runat="server" DataTextField="Name"
                                                                DataValueField="ID" AutoPostBack="false" onchange="javascript:ChangeCountryorState()" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <span class="blackblodtext">Estado</span> &nbsp;&nbsp;&nbsp;
                                                            <asp:DropDownList ID="lstStates" CssClass="altText" Width="50" runat="server" DataTextField="Code"
                                                                AutoPostBack="false" DataValueField="ID" onchange="javascript:ChangeCountryorState('1')" />
                                                            &nbsp;&nbsp;&nbsp;
                                                            <asp:DropDownList ID="lstStates2" CssClass="altText" Width="50" runat="server" DataTextField="Code"
                                                                AutoPostBack="false" DataValueField="ID" onchange="javascript:ChangeCountryorState('1')" />
                                                            &nbsp;&nbsp;&nbsp;
                                                            <asp:DropDownList ID="lstStates3" CssClass="altText" Width="50" runat="server" DataTextField="Code"
                                                                AutoPostBack="false" DataValueField="ID" onchange="javascript:ChangeCountryorState('1')" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="blackblodtext" align="center">
                                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            <input type="button" name="ZipSubmit" value="Entregar" class="altShortBlueButtonFormat"
                                                                onfocus="this.blur()" style="width: 65px;" onclick="javascript:RefreshRooms()">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            <span class="blackblodtext">OR</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <span class="blackblodtext">Código postal</span>&nbsp;&nbsp;&nbsp;
                                                            <asp:TextBox ID="txtZipCode" Width="50" runat="server" CssClass="altText" onkeyup="javascript:chkZip();"></asp:TextBox>
                                                            <asp:RegularExpressionValidator ID="regZipCode" ControlToValidate="txtZipCode" Display="dynamic"
                                                                runat="server" SetFocusOnError="true" ErrorMessage="<br> & < > ' + % \ ; ? | ^ = ! ` [ ] { } : # $ @ ~ and &#34; are invalid characters."
                                                                ValidationGroup="Submit" ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>^+;?|!`\[\]{}\x22;=:@#$%&'~]*$" /><%--FB 2222--%>
                                                            &nbsp;&nbsp;
                                                            <input type="button" name="ZipSubmit" onfocus="this.blur()" value="Entregar" class="altShortBlueButtonFormat"
                                                                style="width: 65px;" onclick="javascript:ZipCodeCheck()">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                            <asp:Button ID="BtnUpdateStates" Style="display: none;" runat="server" OnClick="BindCountry" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <ajax:CollapsiblePanelExtender ID="MediaExtender" runat="server" TargetControlID="MediaPanel"
                                        ImageControlID="MediaImg" CollapseControlID="MediaImg" ExpandControlID="MediaImg"
                                        ExpandedImage="image/loc/nolines_minus.gif" Collapsed="true" CollapsedImage="image/loc/nolines_plus.gif"
                                        CollapsedSize="30" />
                                    <asp:Panel ID="MediaPanel" runat="server">
                                        <table class="treeSelectedNode" width="100%">
                                            <tr>
                                                <td class="tableHeader">
                                                    <img id="MediaImg" runat="server" src="image/loc/nolines_plus.gif" />
                                                    <span class="">Medios</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:CheckBox runat="server" ID="MediaNone" Text=" Ninguno" Checked="true" onclick="javascript:RefreshRooms()" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:CheckBox runat="server" ID="MediaAudio" Text=" Audio" Checked="true" onclick="javascript:RefreshRooms()" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:CheckBox runat="server" ID="MediaVideo" Text=" Video" Checked="true" onclick="javascript:RefreshRooms()" />
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <ajax:CollapsiblePanelExtender ID="PhotoExtender" runat="server" TargetControlID="PhotoTable"
                                        ImageControlID="PhotImg" CollapseControlID="PhotImg" ExpandControlID="PhotImg"
                                        ExpandedImage="image/loc/nolines_minus.gif" Collapsed="true" CollapsedImage="image/loc/nolines_plus.gif"
                                        CollapsedSize="30" />
                                    <asp:Panel ID="PhotoTable" runat="server">
                                        <table class="treeSelectedNode" width="100%">
                                            <tr>
                                                <td class="tableHeader">
                                                    <img id="PhotImg" runat="server" src="image/loc/nolines_plus.gif" />
                                                    <span class="">Fotos</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:CheckBox runat="server" ID="PhotosOnly" Text=" Sólo Fotos" onclick="javascript:RefreshRooms()" />
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <ajax:CollapsiblePanelExtender ID="HandiExtend" runat="server" TargetControlID="HandicapTable"
                                        CollapseControlID="HandAccs" ImageControlID="HandAccs" ExpandControlID="HandAccs"
                                        ExpandedImage="image/loc/nolines_Minus.gif" Collapsed="true" CollapsedImage="image/loc/nolines_plus.gif"
                                        CollapsedSize="30" />
                                    <asp:Panel ID="HandicapTable" runat="server">
                                        <table class="treeSelectedNode" width="100%">
                                            <tr>
                                                <td class="tableHeader">
                                                    <img id="HandAccs" runat="server" src="image/loc/nolines_plus.gif" />
                                                    <span class="">acceso discapacitado</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:CheckBox runat="server" ID="HandiCap" Text="acceso discapacitado" onclick="javascript:RefreshRooms()" />
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <input type="reset" name="reset" onfocus="this.blur()" value="reajustar" class="altShort2BlueButtonFormat"
                                        id="Reset1" onclick="JavaScript:history.go(0);" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
                <td style="width: 78%; height: 540px;">
                    <asp:UpdatePanel ID="RoomsUpdate" UpdateMode="Conditional" runat="server" RenderMode="Inline">
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnRefreshRooms" />
                        </Triggers>
                        <ContentTemplate>
                            <%--alignment fix--%>
                            <table width="100%">
                                <tr>
                                    <td>
                                        <center>
                                            <asp:Label ID="LblError" CssClass="lblError" runat="server"></asp:Label></center>
                                            <label id="errormsg" style="text-align: center; font-family: Verdana; color: Red; display:none;" >Only one Endpoint can be selected as Caller</label>
                                        <table width="100%">
                                            <tr>
                                                <td style="width: 70%">
                                                    <%--<span class="blackblodtext">ver tipo :</span>&nbsp;--%>
                                                    <asp:Label runat="server" ID="lblViewType" Text="ver tipo:" CssClass="blackblodtext"></asp:Label>&nbsp; <%--FB 2262 //FB 2599--%>
                                                    <asp:DropDownList ID="DrpDwnListView" CssClass="altText" runat="server" AutoPostBack="true"
                                                        OnSelectedIndexChanged="DrpDwnListView_SelectedIndexChanged">
                                                        <asp:ListItem Text="Ver lista" Value="1"></asp:ListItem>
                                                        <asp:ListItem Text="Ver Detalles" Value="2"></asp:ListItem>
                                                    </asp:DropDownList>
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span class="blackblodtext"><font size="1">Haga clic en los encabezados para ordenar.</font></span>
                                                    <input type="hidden" id="hdnView" runat="server" />
                                                    <input type="hidden" id="addroom" value="0" runat="server" />
                                                    <input type="hidden" id="hdnDelRoom" value="0" runat="server" />
                                                    <input type="hidden" id="hdnDelRoomID" runat="server" />
                                                    <input type="hidden" id="hdnEditroom" value="0" runat="server" />
                                                    <%--FB 1796--%>
                                                    <input runat="server" id="hdnTimeZone" type="hidden" />
                                                    <input runat="server" id="hdnServiceType" type="hidden" /><%--FB 2219--%>
                                                    <input runat="server" id="selectedlocframe" type="hidden" />
                                                    <input type="hidden" id="hdnVMRRoomadded" runat="server" /><%--FB 2448--%>
                                                    <input type="hidden" id="locstr" name="locstr" value="" runat="server" />
                                                    <input type="hidden" id="Tierslocstr" name="Tierslocstr" value="" runat="server" />
                                                    <asp:Panel ID="PanelRooms" runat="server" Height="540px" CssClass="treeSelectedNode"
                                                        BorderColor="Blue" BorderStyle="Solid" BorderWidth="1px">
                                                        <div align="center" id="conftypeDIV" style="width: 100%;" class="treeSelectedNode">
                                                            <table border="0" cellpadding="3" cellspacing="0" width="100%">
                                                                <tr id="DetailsView" runat="server" style="display: none;">
                                                                    <td width="40%" align="left" style="font-weight: bold; font-size: small; color: green;
                                                                        font-family: arial" valign="middle">
                                                                        <%--Edited for FF--%>
                                                                        <dxwgv:ASPxGridView AllowSort="true" OnHtmlRowCreated="ASPxGridView1_HtmlRowCreated"
                                                                            ID="grid" ClientInstanceName="grid" runat="server" KeyFieldName="RoomID" Width="100%"
                                                                            EnableRowsCache="True" OnCustomCallback="Grid_CustomCallback" OnDataBound="Grid_DataBound">
                                                                            <Columns>
                                                                                <dxwgv:GridViewCommandColumn VisibleIndex="0" Width="50px">
                                                                                    <ClearFilterButton Visible="True" Text="Borrar" />
                                                                                </dxwgv:GridViewCommandColumn>
                                                                                <dxwgv:GridViewDataColumn FieldName="Tier1Name" Caption="nivel1" VisibleIndex="1"
                                                                                    HeaderStyle-HorizontalAlign="Center" />
                                                                                <dxwgv:GridViewDataColumn FieldName="Tier2Name" Caption="nivel2" VisibleIndex="2"
                                                                                    HeaderStyle-HorizontalAlign="Center" />
                                                                                <dxwgv:GridViewDataColumn FieldName="RoomName" Caption="nombre del salón" VisibleIndex="3"
                                                                                    HeaderStyle-HorizontalAlign="Center" />
                                                                                <dxwgv:GridViewDataColumn FieldName="MaximumCapacity" Caption="Capacidad Máxima"
                                                                                    VisibleIndex="4" HeaderStyle-HorizontalAlign="Center" Width="32%" />
                                                                                <dxwgv:GridViewDataColumn FieldName="RoomID" Visible="False" />
                                                                                <dxwgv:GridViewDataColumn FieldName="IsVMR" Visible="False" />
                                                                                <dxwgv:GridViewDataColumn FieldName="Tier1ID" Visible="False" /> <%--FB 2637--%>
                                                                                <%--FB 2448 --%>
                                                                            </Columns>
                                                                            <Styles>
                                                                                <CommandColumn Paddings-Padding="1" />
                                                                            </Styles>
                                                                            <Settings ShowFilterRow="True" />
                                                                            <SettingsBehavior AllowMultiSelection="false" />
                                                                            <SettingsPager Mode="ShowPager" PageSize="5" AlwaysShowPager="true" Position="Top">
                                                                            </SettingsPager>
                                                                            <SettingsText EmptyDataRow="No se encontraron datos." />
                                                                            <Templates>
                                                                                <DataRow>
                                                                                    <div style="padding: 5px">
                                                                                        <table class="templateTable" cellpadding="0" cellspacing="1" width="100%">
                                                                                            <tr>
                                                                                                <td rowspan="4" width="70px">
                                                                                                    <asp:Image ID="Image1" runat="server" ImageUrl='<%#  DataBinder.Eval(Container, "DataItem.ImageName")%> '
                                                                                                        Height="65px" Width="70px" />
                                                                                                </td>
                                                                                                <td class="templateCaption" colspan="2">
                                                                                                    <%#  DataBinder.Eval(Container, "DataItem.Tier1Name")%>
                                                                                                    >
                                                                                                    <%#  DataBinder.Eval(Container,"DataItem.Tier2Name")%>
                                                                                                    >
                                                                                                    <asp:HyperLink ID="btnViewDetailsDev" Style="cursor: pointer;" runat="server" Text='<%#  DataBinder.Eval(Container, "DataItem.RoomName") %>' />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="templateCaption" width="50%">
                                                                                                    Capacidad :
                                                                                                    <%# DataBinder.Eval(Container, "DataItem.MaximumCapacity")%>
                                                                                                </td>
                                                                                                <td class="templateCaption">
                                                                                                    &nbsp;
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td class="templateCaption" width="50%">
                                                                                                    medios :
                                                                                                    <%# DataBinder.Eval(Container, "DataItem.Video")%>
                                                                                                </td>
                                                                                                <td class="templateCaption">
                                                                                                    Aprobación :
                                                                                                    <%# DataBinder.Eval(Container, "DataItem.ApprovalReq")%>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td style="white-space: normal">
                                                                                                    <%#DataBinder.Eval(Container, "DataItem.City")%>
                                                                                                    <%#DataBinder.Eval(Container, "DataItem.StateName")%>
                                                                                                    <%#DataBinder.Eval(Container, "DataItem.ZipCode")%>
                                                                                                    <%#DataBinder.Eval(Container, "DataItem.CountryName")%>
                                                                                                </td>
                                                                                                <td align="center">
                                                                                                    <asp:HyperLink ID="Hyper1" Text="Seleccione Salón" Style="cursor: pointer; color: Green"
                                                                                                        runat="server" />&nbsp;&nbsp;&nbsp;
                                                                                                    <asp:HyperLink ID="DelRoom" Text="Desactivar" Style="cursor: pointer; color: Red"
                                                                                                        runat="server" />&nbsp;&nbsp;&nbsp;
                                                                                                    <asp:HyperLink ID="Editroom" Text="Editar" Style="cursor: pointer;" runat="server" />
                                                                                                    <%--FB 2426 Start--%>
                                                                                                    <asp:HyperLink ID="importRoom" Text="Importar" Style="cursor: pointer; color: Green"
                                                                                                        runat="server" />&nbsp;&nbsp;&nbsp;
                                                                                                    <asp:HyperLink ID="DelGuestRoom" Text="Eliminar" Style="cursor: pointer; color: Red"
                                                                                                        runat="server" />&nbsp;&nbsp;&nbsp;
                                                                                                    <%--FB 2426 End--%>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </div>
                                                                                </DataRow>
                                                                            </Templates>
                                                                        </dxwgv:ASPxGridView>
                                                                    </td>
                                                                </tr>
                                                                <tr id="ListView" runat="server">
                                                                    <td width="40%" align="left" style="font-weight: bold; font-size: small; color: green;
                                                                        font-family: arial" valign="middle">
                                                                        <%--Edited for FF--%>
                                                                        <dxwgv:ASPxGridView AllowSort="true" OnHtmlRowCreated="ASPxGridView1_HtmlRowCreated"
                                                                            ID="grid2" ClientInstanceName="grid2" runat="server" KeyFieldName="RoomID" Width="100%"
                                                                            EnableRowsCache="True" OnCustomCallback="Grid2_CustomCallback" OnDataBound="Grid2_DataBound">
                                                                            <Columns>
                                                                                <dxwgv:GridViewCommandColumn VisibleIndex="0" Width="50px">
                                                                                    <ClearFilterButton Text="Borrar" Visible="True" />
                                                                                </dxwgv:GridViewCommandColumn>
                                                                                <dxwgv:GridViewDataColumn FieldName="Tier1Name" Caption="nivel1" VisibleIndex="1"
                                                                                    HeaderStyle-HorizontalAlign="Center" />
                                                                                <dxwgv:GridViewDataColumn FieldName="Tier2Name" Caption="nivel2" VisibleIndex="2"
                                                                                    HeaderStyle-HorizontalAlign="Center" />
                                                                                <dxwgv:GridViewDataColumn FieldName="RoomName" Caption="nombre del salón" VisibleIndex="3"
                                                                                    HeaderStyle-HorizontalAlign="Center" />
                                                                                <dxwgv:GridViewDataColumn FieldName="MaximumCapacity" Caption="Capacidad Máxima"
                                                                                    VisibleIndex="4" HeaderStyle-HorizontalAlign="Center" Width="32%" />
                                                                                <dxwgv:GridViewDataColumn FieldName="RoomID" Visible="False" />
                                                                                <dxwgv:GridViewDataColumn FieldName="IsVMR" Visible="False" />
                                                                                <dxwgv:GridViewDataColumn FieldName="Tier1ID" Visible="False" /> <%--FB 2637--%>
                                                                                <%--FB 2448 --%>
                                                                            </Columns>
                                                                            <Styles>
                                                                                <CommandColumn Paddings-Padding="1" />
                                                                            </Styles>
                                                                            <Settings ShowFilterRow="True" />
                                                                            <SettingsBehavior AllowMultiSelection="false" />
                                                                            <SettingsText EmptyDataRow="No se encontraron datos." />
                                                                            <SettingsPager Mode="ShowPager" PageSize="100" AlwaysShowPager="true" Position="Top">
                                                                            </SettingsPager>
                                                                            <Templates>
                                                                                <DataRow>
                                                                                    <div style="padding: 5px">
                                                                                        <table class="templateTable" cellpadding="0" cellspacing="1" width="100%">
                                                                                            <tr>
                                                                                                <td class="templateCaption" style="width: 75%;">
                                                                                                    <%#DataBinder.Eval(Container,"DataItem.Tier1Name")%>
                                                                                                    >
                                                                                                    <%#DataBinder.Eval(Container,"DataItem.Tier2Name")%>
                                                                                                    >
                                                                                                    <asp:HyperLink ID="btnViewDetailsDev" Style="cursor: pointer;" runat="server" Text='<%#DataBinder.Eval(Container,"DataItem.RoomName")%>' />
                                                                                                </td>
                                                                                                <td align="center">
                                                                                                    <asp:HyperLink ID="Hyper1" Text="Seleccione Salón" Style="cursor: pointer; color: Green"
                                                                                                        runat="server" />&nbsp;&nbsp;&nbsp;
                                                                                                    <asp:HyperLink ID="DelRoom" Text="Desactivar" Style="cursor: pointer; color: Red"
                                                                                                        runat="server" />&nbsp;&nbsp;&nbsp;
                                                                                                    <asp:HyperLink ID="Editroom" Text="Editar" Style="cursor: pointer;" runat="server" />
                                                                                                    <%--FB 2426 Start--%>
                                                                                                    <asp:HyperLink ID="importRoom" Text="Importar" Style="cursor: pointer; color: Green"
                                                                                                        runat="server" />&nbsp;&nbsp;&nbsp;
                                                                                                    <asp:HyperLink ID="DelGuestRoom" Text="Eliminar" Style="cursor: pointer; color: Red"
                                                                                                        runat="server" />&nbsp;&nbsp;&nbsp;
                                                                                                    <%--FB 2426 End--%>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </div>
                                                                                </DataRow>
                                                                            </Templates>
                                                                        </dxwgv:ASPxGridView>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                            <tr id="TrLicense" runat="server" style="display: none;">
                                                <td align="left">
                                                    <asp:Label ID="lblTtlRooms" CssClass="blackblodtext" Text="total de salones de vídeo:" runat="server" />
                                                    <span class="blackblodtext">
                                                        <asp:Label ID="totalNumber" runat="server" />
                                                        <asp:Label ID="Label1" CssClass="blackblodtext" Text="&#59;&nbsp; &nbsp;Salones sin vídeo totales:" runat="server" />
                                                        <asp:Label ID="ttlnvidLbl" runat="server" />
                                                        <asp:Label ID="lblVMRRooms" CssClass="blackblodtext" Text="&#59;&nbsp; &nbsp;Total VMR Rooms: " runat="server" /><%--FB 2586--%>
                                                        <asp:Label ID="tntvmrrooms" runat="server" /></br>
                                                        <asp:Label ID="lblPublicRoom" CssClass="blackblodtext" Text=" ; Total  Public Rooms :" runat="server" /> 
                                                        <asp:Label ID="ttlPublicLbl" runat="server" /> 
                                                    </span><span class="blackblodtext"> Salones de Vídeo restantes:</span> <span class="summaryText">
                                                        <asp:Label ID="vidLbl" runat="server" CssClass="blackblodtext" />
                                                    </span><span class="blackblodtext">; &nbsp;&nbsp;Salones sin Vídeo restantes:</span> <span
                                                        class="summaryText">
                                                        <asp:Label ID="nvidLbl" runat="server" CssClass="blackblodtext" />
                                                        <%--FB 2586--%>
                                                    </span>; &nbsp; <span class="blackblodtext">&nbsp;&nbsp;VMR Rooms Remaining: </span> <span
                                                        class="summaryText">
                                                        <asp:Label ID="vmrvidLbl" runat="server" CssClass="blackblodtext" />

                                                    </span>
                                                </td>
                                            </tr>
                                        </table>
                                        <asp:Button ID="btnRefreshRooms" Style="display: none;" runat="server" OnClick="ChangeCalendarDate" />
                                    </td>
                                    <td style="vertical-align: top" width="30%" id="TDSelectedRoom" runat="server">
                                        <br />
                                        <asp:Panel ID="SelectedRooms" runat="server" Height="540px" CssClass="treeSelectedNode"
                                            ScrollBars="Auto">
                                            <div width="100%" style="border-style: solid; border-width: 1px; border-color: Blue;">
                                                <table width="100%">
                                                    <tr class="tableHeader">
                                                        <td class="tableHeader">
                                                            Salones Seleccionados
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left">
                                                            <img border='0' src='image/btn_delete.gif' tooltip="Quitar Todos" runat="server" id="ImageDel"
                                                                width='18' height='18' onclick="JavaScript:ClearAllSelection()" />
                                                            <span class="treeRootNode" onclick="JavaScript:ClearAllSelection()">Quitar todo</span>
                                                        </td>
                                                    </tr>
                                                    <tr valign="top">
                                                        <td>
                                                            <asp:DataGrid ShowHeader="false" Width="100%" ID="SelectedGrid" runat="server" AutoGenerateColumns="False"
                                                                OnItemDataBound="SetRoomAttributes" Font-Names="Verdana" Font-Size="Small">
                                                                <Columns>
                                                                    <asp:BoundColumn DataField="RoomID" Visible="false"></asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="RoomName" Visible="false"></asp:BoundColumn>
                                                                    <asp:TemplateColumn>
                                                                        <ItemTemplate>
                                                                            <table width="100%" cellspacing="3">
                                                                                <tr>
                                                                                    <td align="left" width="90%">
                                                                                        <asp:HyperLink ID="btnViewDetails" Style="cursor: pointer;" runat="server" Text='<%#DataBinder.Eval(Container,"DataItem.RoomName") %>' />
                                                                                    </td>
                                                                                    <td align="left" width="10%">
                                                                                        <img border='0' src='image/btn_delete.gif' runat="server" id="ImageDel" width='18'
                                                                                            height='18' />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                </Columns>
                                                            </asp:DataGrid>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>

<script type="text/javascript">
    var roomNamesStr, roomIdsStr
    function chkresources(id) {
        if (id != "") {
            if (id.indexOf(",") < 0)
                id += ",";
            url = "roomresourcecomparesel.aspx?wintype=pop&f=pop&rms=" + id;
            rmresPopup = window.open(url, 'roomresource', 'status=no,width=450,height=480,resizable=yes,scrollbars=yes');
            rmresPopup.focus();
            if (!rmresPopup.opener) {
                rmresPopup.opener = self;
            }
        }
    }

    function Addroms() {
        CorrectHdnString();

        var args = Addroms.arguments;
        args = args[0].split(';');
        var locs = document.getElementById("selectedlocframe");
        var adlocs = document.getElementById("addroom");
        var vmrRoomadded = document.getElementById("hdnVMRRoomadded"); //FB 2448
        var chkboxVMR = document.getElementById("chkIsVMR"); //FB 2448
        var prnt;
        roomIdsStr = locs.value.split(',');
        //FB 2637 Starts
        var tier1Alert = "<%=Session["AlertforTier1"]%>";
        tier1Alert = tier1Alert.split('|');
        if (Loccontains(tier1Alert, args[1])) {
            alert("Only Dial-in connection is permitted when connecting to a Meeting Space");
        }
        //FB 2637 Ends

        if ('<%=Parentframe%>' == "frmCalendarRoom" || '<%=Parentframe%>' == "frmUserProfile") {

            if (roomIdsStr.length > 20) {
                alert("Se pueden seleccionar un máximo de 20 salones");
                return false;
            }
        }
        if (!Loccontains(roomIdsStr, args[0])) {
            //FB 2448 Starts
            if (getQueryVariable('isVMR') != null) {

                if (getQueryVariable('isVMR') == "1") {
                    var locdummy = "";
                    locdummy = vmrRoomadded.value;
                    if (chkboxVMR.checked) {
                        if (parent && locdummy == "")
                            if (parent.document.getElementById("hdnSelectVMRRoom"))
                            locdummy = parent.document.getElementById("hdnSelectVMRRoom").value;

                        if (locdummy != "") {
                            alert("Only one VMR Room selection is allowed");
                            return false;
                        }
                        else {
                            vmrRoomadded.value = args[0];
                        }
                    }
                }
            }

            //FB 2448 Ends

            if (locs.value == "")
                locs.value = args[0];
            else
                locs.value += "," + args[0];

            if (adlocs)
                adlocs.value = "1";


            if (opener) {

                prnt = opener.document.getElementById("selectedList");
                if (prnt)
                    prnt.value = locs.value;

                if (opener.document.getElementById("btnfrmSearch"))
                    opener.document.getElementById("btnfrmSearch").click();

                var selprnt = opener.document.getElementById("selectedloc");

                if (selprnt)
                    selprnt.value = locs.value;
            }
            else if (parent) {
                prnt = parent.document.getElementById("selectedloc");
                if (prnt)
                    prnt.value = locs.value;


            }
        }
        else
            alert("Salón ya añadido");
        var refrsh = document.getElementById("btnRefreshRooms");
        if (refrsh)
            refrsh.click();
    }

    function CorrectHdnString() {
        var locs = document.getElementById("selectedlocframe");
        var vlue = "";

        roomIdsStr = locs.value.split(',');

        var i = roomIdsStr.length;

        while (i--) {
            if (roomIdsStr[i] != "") {
                if (vlue == "")
                    vlue = roomIdsStr[i].trim();
                else
                    vlue += "," + roomIdsStr[i].trim();
            }

        }

        locs.value = vlue;

    }


    function delRoom() {

        var args = delRoom.arguments;

        var locs = document.getElementById("hdnDelRoomID");
        var adlocs = document.getElementById("hdnDelRoom");
        var prnt;
        if (locs.value == "")
            locs.value = args[0];

        if (adlocs)
            adlocs.value = "Desactivar";
        if (opener) {
            prnt = opener.document.getElementById("selectedList");
            if (prnt)
                prnt.value = locs.value;

            if (opener.document.getElementById("btnfrmSearch"))
                opener.document.getElementById("btnfrmSearch").click();
        }
        else if (parent) {
            prnt = parent.document.getElementById("selectedloc");
            if (prnt)
                prnt.value = locs.value;
        }
        var refrsh = document.getElementById("btnRefreshRooms");
        if (refrsh)
            refrsh.click();


    }

    function ActivateRoom() {

        var args = ActivateRoom.arguments;

        var locs = document.getElementById("hdnDelRoomID");
        var adlocs = document.getElementById("hdnDelRoom");

        if (locs.value == "")
            locs.value = args[0];

        if (adlocs)
            adlocs.value = "Activate";

        if (parent) {
            prnt = parent.document.getElementById("selectedloc");
            if (prnt)
                prnt.value = locs.value;
        }
        var refrsh = document.getElementById("btnRefreshRooms");
        if (refrsh)
            refrsh.click();


    }


    function Delroms() {
        CorrectHdnString();

        var args = Delroms.arguments;
        var locs = document.getElementById("selectedlocframe");
        var adlocs = document.getElementById("addroom");
        var hdNm = document.getElementById("locstr");
        var vmrRoomadded = document.getElementById("hdnVMRRoomadded"); //FB 2448
        var vmrRoomdeleted = "";
        if (parent.document.getElementById("hdnSelectVMRRoom") != null) {
            vmrRoomdeleted = parent.document.getElementById("hdnSelectVMRRoom").value; //FB 2448
        }
        var prnt;

        roomIdsStr = locs.value.split(',');
        // FB2448
        if (vmrRoomadded.value.trim() == args[0]) {
            vmrRoomadded.value = "";
        }
        if (vmrRoomdeleted.trim() == args[0]) {
            parent.document.getElementById("hdnSelectVMRRoom").value = "";
        }
        //FB 2448

        if (Loccontains(roomIdsStr, args[0])) {

            var i = roomIdsStr.length;

            while (i--) {
                if (roomIdsStr[i] == args[0]) {
                    roomIdsStr[i] = "";
                }

            }

            i = roomIdsStr.length;
            locs.value = "";
            while (i--) {
                if (roomIdsStr[i] != "") {
                    if (locs.value == "")
                        locs.value = roomIdsStr[i];
                    else
                        locs.value += "," + roomIdsStr[i];
                }
            }

            if (adlocs)
                adlocs.value = "1";

            if (locs.value == "")
                hdNm.value = "";


            if (opener) {
                prnt = opener.document.getElementById("selectedList");
                if (prnt)
                    prnt.value = locs.value;

                if (opener.document.getElementById("btnfrmSearch"))
                    opener.document.getElementById("btnfrmSearch").click();

                prntsellocs = opener.document.getElementById("selectedloc");
                if (prntsellocs)
                    prntsellocs.value = locs.value;
            }
            else if (parent) {
                prnt = parent.document.getElementById("selectedloc");
                if (prnt)
                    prnt.value = locs.value;
            }


            var refrsh = document.getElementById("btnRefreshRooms");
            if (refrsh)
                refrsh.click();



        }
    }


    function Loccontains(a, obj) {
        var i = a.length;
        while (i--) {
            if (a[i] === obj) {
                return true;
            }
        }
        return false;
    }


</script>

<script type="text/javascript">
    // FB 1797
    function getQueryVariable(variable) {
        var query = window.location.search.substring(1);
        var vars = query.split("&");
        for (var i = 0; i < vars.length; i++) {
            var pair = vars[i].split("=");
            if (pair[0] == variable) {
                return pair[1];
            }
        }

        return "";

    }
    // FB 1797

    function ChangeLbl() {

        var args = ChangeLbl.arguments;

        if (args) {


            var txt = document.getElementById(args[0]);
            var lbl = document.getElementById("LBLCapacity");
            var hdnH = document.getElementById("hdnCapacityH");
            var hdnL = document.getElementById("hdnCapacityL");
            var txtL = document.getElementById("TxtSearchL");
            var txtH = document.getElementById("TxtSearchH");
            if (txt) {
                if (lbl)
                    lbl.innerHTML = args[1];
            }

            if (args[1] != "") {
                if (args[1] == "Any") {
                    if (hdnL)
                        hdnL.value = "";
                    if (hdnH)
                        hdnH.value = "";
                }
                else {
                    var vlues = args[1].split(' ');
                    if (vlues) {
                        if (vlues.length > 1) {
                            if (hdnH)
                                hdnL.value = vlues[0];
                            if (hdnL)
                                hdnH.value = vlues[2];
                        }
                        else {
                            if (hdnH)
                                hdnL.value = "20";
                            if (hdnL)
                                hdnH.value = "";
                        }
                    }
                }
                if (txt)
                    RefreshRooms(txt);
            }
            else {
                if (lbl && txtL && txtH) {
                    var Hval = txtH.value;
                    if (hdnH)
                        hdnH.value = txtH.value;
                    if (hdnL)
                        hdnL.value = txtL.value;


                    if (Hval == "")
                        Hval = "Cualquier";


                    try {
                        if (Hval != "Any")
                            eval(hdnH.value);

                        eval(hdnL.value);
                    }
                    catch (exception) {
                        alert("Por favor, compruebe los valores");
                        return false;
                    }

                    if (Hval != "Any") {
                        if (eval(hdnH.value) < eval(hdnL.value)) {
                            alert("Por favor, compruebe los valores");
                            return false;
                        }
                    }


                    lbl.innerHTML = txtL.value + " - " + Hval;

                }

                RefreshRooms(txtL);
            }

        }
    }

    function RefreshRooms() {
        var args = RefreshRooms.arguments;

        if (args) {
            if (args[0]) {
                if (args[0].value == "") {
                    alert("Por favor, introduzca un valor válido.");
                    return false;
                }

                var isfilter = document.getElementById("hdnIsFilterChanged");
                if (isfilter)
                    isfilter.value = "Y";
            }
        }

        var mnone = document.getElementById("MediaNone");
        var maud = document.getElementById("MediaAudio");
        var mvid = document.getElementById("MediaVideo");

        if (!mnone.checked && !maud.checked && !mvid.checked) {
            alert("Por favor, seleccione un Tipo de Medio.");
            return false;
        }

        var hdNm = document.getElementById("hdnName");
        if (hdNm)
            hdNm.value = "0";

        var hdNm = document.getElementById("hdnView");
        if (hdNm)
            hdNm.value = "0";

        var refrsh = document.getElementById("btnRefreshRooms");
        if (refrsh)
            refrsh.click();

    }


    function AVItemChanged() {
        var avchg = document.getElementById("hdnAV");
        if (avchg)
            avchg.value = "1";
        RefreshRooms();

    }



    function ChkFavorites() {
        var avchg = '<%=favRooms%>';

        var chkfav = document.getElementById("chkFavourites");

        if (avchg == "0") {
            alert("No se han añadido favoritos para el usuario");
            if (chkfav)
                chkfav.checked = false;

        }
        else
            RefreshRooms();

    }
    //FB 2426 Start
    function ChkGuestRooms() {
        var avchg = '<%=GuestRooms%>';

        var chkfav = document.getElementById("chkGuestRooms");

        if (avchg == "0") {
            alert("No Guest Room have been added for the user");
            if (chkfav)
                chkfav.checked = false;

        }
        else
            RefreshRooms();

    }
    //FB 2426 End
    //FB 2448 Start
    function ChkVirtualMeetingRooms() {
        var avchg = '<%=VMR%>';

        var chkfav = document.getElementById("chkIsVMR");

        if (avchg == "0") {
            alert("No Virtual Meeting Rooms have been added for the user");
            if (chkfav)
                chkfav.checked = false;

        }
        else
            RefreshRooms();

    }
    //FB 2448 End
    function ZipCodeCheck() {
        var zphg = document.getElementById("hdnZipCode");
        if (zphg)
            zphg.value = "1";
        RefreshRooms(zphg);
    }

    function chkZip() {
        var zpTxt = document.getElementById("txtZipCode");
        var cntry = document.getElementById("lstCountry");
        var stt1 = document.getElementById("lstStates");
        var stt2 = document.getElementById("lstStates2");
        var stt3 = document.getElementById("lstStates3");
        var zphg = document.getElementById("hdnZipCode");
        if (zpTxt) {
            if (zpTxt.value == "") {
                cntry.disabled = false;
                stt1.disabled = false;
                stt2.disabled = false;
                stt3.disabled = false;
                if (zphg)
                    zphg.value = "0";

                RefreshRooms(zphg);

            }
            else {
                cntry.disabled = true;
                stt1.disabled = true;
                stt2.disabled = true;
                stt3.disabled = true;
                chkLimit(zpTxt, 'u');

            }
        }

    }

    function ChangeCountryorState() {
        var zphg = document.getElementById("hdnZipCode");
        if (zphg)
            zphg.value = "0";
        var loc = document.getElementById("hdnLoc");
        if (loc)
            loc.value = "0";

        var arg = ChangeCountryorState.arguments;

        if (arg) {
            if (arg[0] == "1") {
                if (loc)
                    loc.value = "1";
            }

        }

        var btnst = document.getElementById("BtnUpdateStates");

        if (btnst)
            btnst.click();
    }

    function NameSearch() {
        var hdNm = document.getElementById("hdnName");
        if (hdNm)
            hdNm.value = "1";

        var txtNm = document.getElementById("TxtNameSearch");

        if (txtNm.value == "") {
            alert("Por favor, introduzca un valor válido.");
            return false;
        }

        var refrshNm = document.getElementById("btnRefreshRooms");
        if (refrshNm)
            refrshNm.click();


    }

    function ViewChng() {
        var hdNm = document.getElementById("hdnView");
        if (hdNm)
            hdNm.value = "1";

    }



    var prm = Sys.WebForms.PageRequestManager.getInstance();
    prm.add_initializeRequest(initializeRequest);

    prm.add_endRequest(endRequest);

    var postbackElement;

    function initializeRequest(sender, args) {
        document.body.style.cursor = "wait";
        DataLoading(1);
        //document.getElementById("btnCompare").disabled = true;



    }



    function endRequest(sender, args) {
        document.body.style.cursor = "default"; DataLoading(0);
        //document.getElementById("btnCompare").disabled =  false;



    }

    function EndDateValidation() {
        var args = EndDateValidation.arguments;
        var endb = document.getElementById("Available");


        var sDate = Date.parse(document.getElementById("txtRoomDateFrom").value + " " + document.getElementById("confRoomStartTime_Text").value);
        var eDate = Date.parse(document.getElementById("txtRoomDateTo").value + " " + document.getElementById("confRoomEndTime_Text").value);


        if (args) {
            if (args[0] == "1")
                endb.checked = true;
        }

        if (endb.checked) {

            if ((sDate >= eDate)) {

                if (document.getElementById("txtRoomDateFrom").value == document.getElementById("txtRoomDateTo").value) {
                    if (sDate > eDate)
                        alert("La hora HASTA debería ser posterior que la hora DESDE");
                    else if (eDate == sDate)
                        alert("La hora HASTA debería ser posterior que la hora DESDE.");
                }
                else
                    alert("La Fecha HASTA debería ser igual/posterior a la Fecha DESDE");

                endb.checked = false;

                return;

            }
        }

        if (args)// FB 1797
        {
            if (args[0] == "1") {

                if (getQueryVariable('confID') != "")
                    alert('Cambiando la fecha/hora en esta pestaña puede quitar validez a las selecciones de salones previas. Una vez seleccionada un hora/fecha mejor, por favor actualice los campos correspondientes en la pestaña "Detalles Básicos"'); //FB 2367
            }
        }

        RefreshRooms();
    }

    function ClosePopup() {
        try {
            var url = window.location.href;
            if (url.indexOf("pageID") == -1) {
                var hdNm = document.getElementById("locstr");
                var locsmain = document.getElementById("selectedlocframe");
                if (opener) {
                    var f = top.opener.document.forms['<%=Parentframe%>'];
                    var add = parent.opener.document.getElementById("addRooms"); //Edited for FF   START        
                    var prnt = parent.opener.document.getElementById("locstrname");
                    var calen = parent.opener.document.getElementById("btnDate");
                    var calensettings = parent.opener.document.getElementById("IsSettingsChange"); //Edited for FF End
                    if (prnt)
                        prnt.value = hdNm.value;
                    if (add)
                        add.click();
                }
                window.close()
            }
            else {
                var prnt = document.getElementById("selectedlocframe").value; //"12,13";////parent.opener.document.getElementById("locstrname");
                var xprnt = prnt.split(",");
                if (xprnt.length == "1") {
                    window.close()
                }
                else if (xprnt.length > 2) {
                    window.parent.document.getElementById('errormsg').style.display = 'block';
                }
                else {
                    alert("Please wait, Terminateing your existing endpoint and connect with new endpoint");
                    window.close()
                }
            }
        }
        catch (exception) {
            window.parent.document.getElementById('errormsg').style.display = 'block';
        }
    }

    function EditRoom() {
        var rmids = EditRoom.arguments;

        var rmid = "";

        if (rmids) {
            rmid = rmids[0];
        }

        var hdNm = document.getElementById("locstr");

        if (parent) {
            var isReplace = true;  //FB 2448 start
            if (document.getElementById("chkIsVMR"))
                if (document.getElementById("chkIsVMR").checked) {
                isReplace = false;
                parent.location.replace("ManageVirtualMeetingRoom.aspx?rID=" + rmid);
            }

            if (isReplace) //FB 2448 end
                parent.location.replace("ManageRoomProfile.aspx?cal=2&rid=" + rmid);
        }
    }
    //FB 2426 Start
    function ImportRoom() {
        var args = ImportRoom.arguments;

        var locs = document.getElementById("hdnDelRoomID");
        var adlocs = document.getElementById("hdnDelRoom");

        if (locs.value == "")
            locs.value = args[0];

        if (adlocs)
            adlocs.value = "Import";

        if (parent) {
            prnt = parent.document.getElementById("selectedloc");
            if (prnt)
                prnt.value = locs.value;
        }
        var refrsh = document.getElementById("btnRefreshRooms");
        if (refrsh)
            refrsh.click();


    }


    function delGuestRoom() {
        var args = delGuestRoom.arguments;

        var locs = document.getElementById("hdnDelRoomID");
        var adlocs = document.getElementById("hdnDelRoom");

        if (locs.value == "")
            locs.value = args[0];

        if (adlocs)
            adlocs.value = "Delete";

        if (parent) {
            prnt = parent.document.getElementById("selectedloc");
            if (prnt)
                prnt.value = locs.value;
        }
        var refrsh = document.getElementById("btnRefreshRooms");
        if (refrsh)
            refrsh.click();

    }
    //FB 2426 End
    function ClearAllSelection() {
        try {
            var locs = document.getElementById("selectedlocframe");
            var adlocs = document.getElementById("addroom");
            var hdNm = document.getElementById("locstr");

            locs.value = "";
            hdNm.value = "";

            if (adlocs)
                adlocs.value = "1";

            if (opener) {
                prnt = opener.document.getElementById("selectedList");
                if (prnt)
                    prnt.value = locs.value;

                if (opener.document.getElementById("btnfrmSearch"))
                    opener.document.getElementById("btnfrmSearch").click();

                var selprnt = opener.document.getElementById("selectedloc");

                if (selprnt)
                    selprnt.value = "";
            }
            else if (parent) {
                prnt = parent.document.getElementById("selectedList");

                if (prnt)
                    prnt.value = locs.value;


                var selprnt = parent.document.getElementById("selectedloc");

                if (selprnt)
                    selprnt.value = "";

            }



            var refrsh = document.getElementById("btnRefreshRooms");
            if (refrsh)
                refrsh.click();

        }
        catch (exception) {
            window.close()
        }
    }

    function ChangeViewType() {

        var tr2 = document.getElementById("DetailsView");
        var tr1 = document.getElementById("ListView");
        var drp = document.getElementById("DrpDwnListView");

        if (drp) {
            tr1.style.display = 'none';
            tr2.style.display = 'none';

            if (drp.value == "1")
                tr1.style.display = 'block';
            else
                tr2.style.display = 'block';
        }


    }
    //FB 2426 Start 
    function ShowActDct() {

        document.getElementById("trGuestRooms").style.display = 'block';
        document.getElementById("chkGuestRooms").checked = false;

        var DrpActDct = document.getElementById("DrpActDct");
        if (DrpActDct)
            if (DrpActDct.value == "1" || DrpActDct.value == "2")
            document.getElementById("trGuestRooms").style.display = 'none';

        var refrsh = document.getElementById("btnRefreshRooms");
        if (refrsh)
            refrsh.click();
    }


    function getQueryString(par) {
        par = par + '=';
        var url = window.location.href;
        var splited = url.split(par);
        var extracted = splited[1].split('&');
        return extracted[0];
    }

    //FB 2426 End


    //alert(window.opener.parent.document.getElementById("selectedloc").value);
</script>

</html>

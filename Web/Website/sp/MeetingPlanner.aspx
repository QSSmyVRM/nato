<%@ Page Language="C#" AutoEventWireup="true" Inherits="ns_MeetingPlanner.MeetingPlanner" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script type="text/javascript" src="script\mousepos1.js"></script>
<script type="text/javascript" src="script\showmsg.js"></script>
<link rel="StyleSheet" href="css/divtable.css" type="text/css" />
<!-- Changed Code for Window Dressing start-->
<link rel="stylesheet" title="Expedite base styles" type="text/css" href="Mirror/styles/main.css">  
<link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/meetingstyleplan.css">  
<!-- Changed Code for Window Dressing End-->
<script>


function mouseoverdiv(confname, confstart, confdur, confloc)
{
	show_message_prompt('image/pen.gif','Conference Name', confname.toString().replace("+","\"").replace("+", ","), confstart, confdur, confloc); //FB 2321
}

function mousemovediv()
{
	move_message_prompt();
}
function mouseoutdiv()
{
  closethis();
}
</script>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Organizador de reuniones</title>
      <%--  Removed Code for Window Dressing
    <link rel="stylesheet" type="text/css" href="css/meetingstyleplan.css" />--%>
    
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <table align="center" width="100%">
     <tr>
         <td colspan="2" align="center"><h3>Organizador de reuniones</h3>
         </td>
     </tr>
    <tr>
        <td align="center">
        <asp:RadioButton runat="server" GroupName="ChangeTime"  ID="OfficeHours" Text="Show Office Hours" AutoPostBack="true" OnCheckedChanged="BindDataFromSearch"/>
        <asp:RadioButton runat="server" GroupName="ChangeTime"  ID="ShowAll" Text="Mostrar Todo"  AutoPostBack="true" OnCheckedChanged="BindDataFromSearch"/></td></tr>
    <tr>
    <td align="center">
        <%--Changed Code for Window Dressing
        <asp:Table Width="100%" runat="server" ID="Table1" class="bordermeetingstyle" border="0" cellpadding="1" cellspacing="1" align="center"></asp:Table></td></tr>--%>
        <asp:Table Width="100%" runat="server" ID="MeetingPlannerTable" border="0" cellpadding="1" cellspacing="1" align="center"></asp:Table></td></tr>
    </table>
    <table align="center">
   <tr>
     <td height="20"></td></tr>
		<tr>
			<%--Changed Code for Window Dressing - Start--%>
			<td width="20" id="tdSuit" runat="server" height="20" class="moresuitSched" >
			</td>
			<td class="blackblodtext">M�s Adecuado</td><!-- Changed Code for Window Dressing -->
			<td width="20" height="20"  class="conferenceSched" > 			 
			</td>
			<td class="blackblodtext">Conferencia</td>
		<%--Changed Code for Window Dressing - End--%>
		</tr>
	</table>
   </div>
    </form>
</body>
</html>

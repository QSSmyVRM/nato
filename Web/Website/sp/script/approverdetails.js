function approver_para_prompt(promptpicture, prompttitle, sendto) 
{ 
	promptbox = document.createElement('div'); 
	promptbox.setAttribute ('id' , 'prompt');
	document.getElementsByTagName('body')[0].appendChild(promptbox);
	promptbox = eval("document.getElementById('prompt').style");

	promptbox.position = 'relative'
	promptbox.top = (formname == "frmMainroom") ? -640 : ((formname == "frmBridgesetting") ? jsAPh : ((formname == "frmMainsuperadministrator") ? -635 : 0) );
	promptbox.left = (formname == "frmMainroom") ? 565 : ((formname == "frmBridgesetting") ? jsAPl : ((formname == "frmMainsuperadministrator") ? 550 : 500) );
	promptbox.width = 400 
	promptbox.border = 'outset 1 #bbbbbb' 

	txt = eval("document." + formname + ".ApproverMsg").value;
	tim = parseInt(eval("document." + formname + ".ApproverTime").value, 10);
	if (isNaN (tim)) {
		dy = "";
		hr = "";
		mn = "";
	} else {
		dy = parseInt(tim/60/24, 10);
		hr = parseInt(tim/60, 10) - dy * 24;
		mn = tim - (dy * 24 + hr) * 60;
	}

	m = "<table cellspacing='0' cellpadding='0' border='0' width='100%'><tr valign='middle'><td width='22' height='22' style='text-indent:2;' class='titlebar'><img src='" + promptpicture + "' height='18' width='18'></td><td class='titlebar'>" + prompttitle + "</td></tr></table>" 
	m += "<table cellspacing='0' cellpadding='0' border='0' width='100%' class='promptbox'>";
	m += "  <tr><td colspan=2>Response Time:";
	m += "    <input type='text' name='appday' maxlength='2' value='" + dy + "' style='width: 20pt'> Day(s)";
	m += "    <input type='text' name='apphr' size='2' maxlength='2' value='" + hr + "' style='width: 20pt'> Hour(s)";
	m += "    <input type='text' name='appmin' size='2' maxlength='2' value='" + mn + "' style='width: 20pt'> Min(s)";
	m += "  </td></tr>"
	m += "  <tr><td valign=top>Response Message: </td><td valign=middle><textarea rows='2' name='appmsg' cols='20'>" + txt + "</textarea></td></tr>"
	m += "  <tr><td align='right' colspan=2>"
	m += "    <input type='button' class='prompt' value='Entregar' onMouseOver='this.style.border=\"1 outset #dddddd\"' onMouseOut='this.style.border=\"1 solid transparent\"' onClick='" + sendto + "(document.getElementById(\"appday\").value,document.getElementById(\"apphr\").value,document.getElementById(\"appmin\").value,document.getElementById(\"appmsg\").value);'>"
	m += "    <input type='button' class='prompt' value='Cancelar' onMouseOver='this.style.border=\"1 outset transparent\"' onMouseOut='this.style.border=\"1 solid transparent\"' onClick='document.getElementsByTagName(\"body\")[0].removeChild(document.getElementById(\"prompt\"))'>"
	m += "  </td></tr>"
	m += "</table>" 
	
	document.getElementById('prompt').innerHTML = m;
} 

function saveInput(d, h, m, txt) 
{ 
	if (!validateInt(d)) {
		alert("Error: Por favor, introduzca un n�mero entero positivo en el campo D�a(s).");
		return false;
	}
	if (!validateInt(h)) {
		alert("Error: Por favor, introduzca un n�mero entero positivo en el campo Hora(s).");
		return false;
	}
	if (!validateInt(m)) {
		alert("Error: Por favor, introduzca un n�mero entero positivo en el campo Min(s).");
		return false;
	}
	
	tim = parseInt(d, 10) * 24 * 60 + parseInt(h, 10) * 60 + parseInt(m, 10);
	eval("document." + formname + ".ApproverMsg").value = txt;
	eval("document." + formname + ".ApproverTime").value = tim;
		
	document.getElementsByTagName("body")[0].removeChild(document.getElementById("prompt"));
} 

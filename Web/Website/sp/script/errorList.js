// max 220

//user info
var EN_1 = "Por favor, introduzca un nombre de usuario de inicio de sesi&oacute;n.";
var EN_2 = "Por favor, introduzca la contrase&ntilde;a.";
var EN_3 = "Sus entradas de contrase&ntilde;a no coinciden.";
var EN_4 = "Por favor, introduzca un Nombre.";
var EN_5 = "Por favor, introduzca un Apellido.";
var EN_6 = "Por favor, escriba una direcci&oacute;n de correo electr&oacute;nico v&aacute;lida.";
var EN_51 = "Por favor, seleccione un Usuario de la Lista de Usuarios";
var EN_81 = "Por favor, introduzca un correo-e de empresa v&aacute;lido";
var EN_89 = "Por favor, introduzca su nombre completo"
var EN_90 = "Por favor, introduzca un Asunto";
var EN_91 = "Por favor, introduzca un Comentario"
var EN_129 = "Por favor, introduzca un nombre de contacto.";
var EN_130 = "Por Favor, introduzca el n&uacute;mero de tel&eacute;fono de contacto";
var EN_172 = "Por favor, introduzca el correo-e del inicio de sesi&oacute;n de usuario.";
var EN_200 = "Las dos direcciones de correo-e no coinciden.";


//date and time info
var EN_7 = "Por favor, compruebe la fecha DESDE y escr&iacute;bala de nuevo.";
var EN_8 = "La fecha DESDE no puede ser una fecha futura.";
var EN_9 = "Por favor, escriba una fecha DESDE anterior a la fecha HASTA.";
var EN_17 = "Por favor,  escriba un n&uacute;mero en el campo Tiempo Antes.";
var EN_18 = "Por favor,  escriba un n&uacute;mero en el campo Tiempo Delta.";
var EN_69 = "Por favor, introduzca la fecha HASTA.";
var EN_70 = "Por favor, introduzca la fecha DESDE.";
var EN_92 = "La fecha HASTA no puede ser una fecha futura.";
var EN_93 = "Por favor, escriba una fecha DESDE anterior a la fecha HASTA."
var EN_117 = "Por favor, introduzca la fecha Antes de.";
var EN_118 = "La fecha DESDE no puede ser una fecha pasada.";
var EN_119 = "La fecha 'Antes de' no puede ser una fecha pasada.";
var EN_120 = "Esta fecha ya fue seleccionada.  Por favor, seleccione otra.";
var EN_177 = "Por favor, introduzca una Hora de finalizaci&oacute;n al menos 15 minutos posterior a la Hora de inicio.";
var EN_202 = "El cambio de fecha para personalizaci&oacute;n del caso no est&aacute; permitido. Por favor, arrastre el caso a otra franja horaria. La hora no cambiar&aacute;.";
var EN_215 = "Por favor, introduzca la hora en el formato correcto (hh:mm AM/PM)";
var EN_216 = "Por favor, compruebe la hora de inicio del sal&oacute;n con la hora final de la conferencia";
var EN_217 = "Por favor, compruebe la hora final del sal&oacute;n con la hora final de la conferencia";
var EN_218 = "Por favor, compruebe la hora de inicio del sal&oacute;n con la hora de inicio de la conferencia.";
var EN_220 = "Por favor, compruebe la hora final del sal&oacute;n con la hora de inicio de la conferencia";

// bridge and port info
var EN_11 = "Por favor, introduzca un nombre MCU.";
var EN_12 = "Por favor, introduzca una direcci&oacute;n de MCU.";
var EN_13 = "Por favor, introduzca un inicio de sesi&oacute;n de MCU.";
var EN_19 = "Por favor,  introduzca un n&uacute;mero en el Intervalo de la encuesta.";
var EN_20 = "Por favor, introduzca un n&uacute;mero (entre 1 y 12) en Canales T1 por tarjeta.";
var EN_21 = "Por favor, introduzca un n&uacute;mero (entre 1 y 12) en N&uacute;mero de puertos IP por tarjeta.";
var EN_22 = "Por favor, introduzca un n&uacute;mero (entre 1 y 12) en N&uacute;mero de canales T1 reservados por tarjeta.";
var EN_23 = "Por favor, introduzca un n&uacute;mero (entre 1 y 12) en N&uacute;mero de puertos IP reservados por tarjeta.";
var EN_24 = "Por favor, introduzca un n&uacute;mero (entre 1 y 12) en N&uacute;mero de puertos de audio por tarjeta.";
var EN_25 = "Por favor, introduzca un n&uacute;mero (entre 1 y 12) en N&uacute;mero de puertos IP reservados.";
var EN_28 = "Por favor, introduzca una direcci&oacute;n de IP v&aacute;lida.";
var EN_82 = "Por favor, introduzca una IP o Nombre SMTP remoto.";
var EN_83 = "Por favor, introduzca un n&uacute;m. de puerto.";
var EN_94 = "Por favor, introduzca un N&uacute;mero de tel&eacute;fono ISDN";
var EN_95 = "Por favor, compruebe el n&uacute;mero de tel&eacute;fono ISDN y escr&iacute;balo de nuevo.";
var EN_96 = "Por favor, compruebe la direcci&oacute;n IP y escr&iacute;bala de nuevo.";
var EN_98 = "Por favor, introduzca un n&uacute;mero ISDN v&aacute;lido.";
var EN_122 = "Por favor, introduzca un nombre de servicio.";
var EN_123 = "Por favor, introduzca una direcci&oacute;n.";
var EN_124 = "Por favor, introduzca un Rango Inicial v&aacute;lido.";
var EN_125 = "Por favor, introduzca un Rango Final v&aacute;lido.";
var EN_126 = "Por favor, introduzca un Rango final mayor que el Rango inicial.";
var EN_133 = "Por favor, introduzca un Nombre Cascada.";
var EN_134 = "Por favor, seleccione un Tipo de Protocolo.";
var EN_135 = "Por favor, seleccione un Tipo de Conexi&oacute;n.";
var EN_136 = "Por favor, seleccione un Puente.";
var EN_137 = "Por favor, introduzca una direcci&oacute;n IP o ISDN v&aacute;lidas para el puente.";
var EN_138 = "Por favor, seleccione un Servicio IP por defecto.";
var EN_139 = "Por favor, seleccione un Servicio ISDN por defecto.";
var EN_141 = "Por favor, seleccione un servicio privado.";
var EN_142 = "Por favor, seleccione un servicio p&uacute;blico.";
var EN_143 = "Por favor, seleccione un Medio.";
var EN_171 = "Por favor, seleccione un tipo de interfaz.";
var EN_181 = "Por favor, introduzca un Prefijo.";
var EN_182 = "Por favor, introduzca un nombre de Punto final.";
var EN_185 = "Por favor, introduzca una direcci&oacute;n de Punto final.";
var EN_189 = "Por favor, rellene con un n&uacute;mero entre 0 y 100 en el campo % de puerto reservado.";
var EN_213 = "Por favor, introduzca una direcci&oacute;n IP v&aacute;lida para el Puerto de Control.";
var EN_214 = "Por favor, introduzca una direcci&oacute;n IP v&aacute;lida para el Puerto A.";
var EN_314 = "Por favor, introduzca una Duraci&oacute;n v&aacute;lida."; 
var EN_315 = "La duraci&oacute;n de la conferencia debe ser de al menos 15 min. (Diferencia entre la duraci&oacute;n y el periodo regulador)";
var EN_316 = "Por favor, introduzca una Duraci&oacute;n de Inicio de Conferencia v&aacute;lida.";
var EN_317 = "Por favor, introduzca una Duraci&oacute;n de finalizaci&oacute;n de Conferencia v&aacute;lida.";

//Group info
var EN_10 = "Por favor, introduzca un nombre de Grupo.";
var EN_39 = "Por favor, seleccione grupos diferentes para Grupos disponibles y Grupos CC."
var EN_53 = "Por favor, haga �doble-clic� sobre el Nombre del Grupo.";
var EN_99 = "El Grupo debe tener al menos un miembro.";
var EN_105 = "Por favor, seleccione grupos diferentes para Grupo por defecto y Grupo CC por defecto.";
var EN_128 = "Por favor, a&ntilde;ada al menos un usuario al grupo."; //FB 1914

//recurring
var EN_32 = "Por favor, compruebe la Hora Inicial Recurrente y vu&eacute;lvala a escribir.";
var EN_33 = "Por favor, compruebe la Duraci&oacute;n Recurrente y vu&eacute;lvala a escribir.";
var EN_34 = "Por favor, compruebe la Zona horaria Recurrente y vu&eacute;lvala a escribir.";
var EN_35 = "Por favor, compruebe el Tipo de Patr&oacute;n Recurrente y vu&eacute;lvalo a escribir.";
var EN_36 = "Por favor, compruebe el Tipo final del Rango de recurrencia y vu&eacute;lvalo a escribir.";
var EN_37 = "Por favor, compruebe el Patr&oacute;n del Rango Recurrente y vu&eacute;lvalo a escribir.";
var EN_38 = "Por favor, compruebe el Tipo de Rango Recurrente y vu&eacute;lvalo a escribir.";
var EN_74 = "Por favor, introduzca una Fecha inicial recurrente futura.\n(formato de fecha: mm/dd/yyyy)";
var EN_79 = "El n&uacute;mero de participantes para una Conferencia Recurrente no puede ser mayor de";
var EN_107 = "Por favor, seleccione un d&iacute;a de la semana.";
var EN_108 = "Por favor, introduzca una Fecha Final antes de futura.\n(formato de fecha: mm/dd/yyyy)";
var EN_109 = "Por favor, introduzca una Fecha Final futura.\n(formato de fecha: mm/dd/yyyy)";
var EN_193 = "Por favor, seleccione al menos una fecha.";
var EN_211 = "Ha alcanzado el l&iacute;mite m&aacute;ximo de fechas seleccionadas por el cliente.";

//conference and conference room
var EN_26 = "Por favor, introduzca un nombre del sal&oacute;n de conferencia.";
var EN_27 = "Por favor,  escriba un n&uacute;mero en el campo Capacidad.";
var EN_30 = "Por favor, introduzca un nombre de conferencia.";
//var EN_31 = "Please select a Conference Duration of at least 15 minutes.";
var EN_31 = "La conferencia tiene que ser de al menos 15 minutos.";
var EN_40 = "Por favor, introduzca un n&uacute;mero en N&uacute;mero de Participantes adicionales.";
var EN_41 = "Por favor, introduzca un n&uacute;mero positivo en N&uacute;mero de Participantes adicionales.";
var EN_43 = "Por favor, seleccione un sal&oacute;n de conferencia.";
var EN_44 = "Por favor, introduzca una contrase&ntilde;a de conferencia.";
var EN_45 = "Por favor, compruebe el formato de la fecha de la conferencia y escr&iacute;balo de nuevo.";
var EN_49 = "Por favor, introduzca una fecha de conferencia futura.";
var EN_52 = "Por favor, introduzca toda la informaci&oacute;n  del Nuevo Usuario\nantes de remplazar el usuario previo.";
var EN_54 = "Por favor, haga doble-clic sobre el Nombre del sal&oacute;n.";
var EN_61 = "El(Los) sal&oacute;n(es) siguiente(s) no tiene(n) equipo de video.  �Contin&uacute;a estableciendo la videoconferencia? ";
var EN_62 = "Durante este periodo, la(s) siguiente(s) conferencia(s) est&aacute;n tambi&eacute;n planificadas"
var EN_71 = "Por favor, seleccione el sal&oacute;n para los Asistentes Externos.";
var EN_72 = "Por favor, selecciones Asistentes Externos antes de seleccionar cualquier sal&oacute;n.";
var EN_75 = "Por favor, seleccione el sal&oacute;n para los Asistentes Externos haciendo clic en Salones Programados.";
var EN_76 = "Debe seleccionar Asistentes Externos y salones \nantes de programar una conferencia en un sal&oacute;n.";
var EN_87 = "Por favor, seleccione una de las opciones anteriores.";
var EN_97 = "Por favor, introduzca un n&uacute;mero de sal&oacute;n de conferencia.";
var EN_102 = "Por favor, introduzca la fecha de la conferencia.";
var EN_103 = "Por favor, compruebe el N&uacute;mero de Participantes adicionales y vu&eacute;lvalo a escribir.";
var EN_104 = "Por favor, seleccione el sal&oacute;n para los Asistentes Externos.";
var EN_112 = "Por favor, seleccione su sal&oacute;n antes de enviar la solicitud.";
var EN_113 = "Por favor, seleccione una Conferencia.";
var EN_114 = "Por favor, seleccione un sal&oacute;n de la lista de Salones Seleccionados.";
var EN_131 = "Por favor, introduzca un n&uacute;mero en el campo N&uacute;mero m&aacute;ximo de llamadas de tel&eacute;fono concurrentes.";
var EN_140 = "Se ha alcanzado el n&uacute;mero m&aacute;ximo de 512 caracteres permitidos para la Descripci&oacute;n de la Conferencia.";
var EN_144 = "Usted ha elegido uno o m&aacute;s salones sin participantes. �Desea continuar?"
var EN_145 = "Por favor, seleccione al menos un Protocolo.";
var EN_146 = "Por favor, seleccione al menos un Protocolo de Audio.";
var EN_147 = "Por favor, seleccione al menos un Protocolo de V&iacute;deo.";
var EN_148 = "Por favor, seleccione al menos una Velocidad de l&iacute;nea.";
var EN_149 = "Por favor, seleccione el Equipo por defecto";
var EN_150 = "Por favor, introduzca el n&uacute;mero de tel&eacute;fono del sal&oacute;n.";
var EN_186 = "No hay participantes seleccionados en el sal&oacute;n. �Est&aacute; seguro de que desea continuar?";
var EN_187 = "La contrase&ntilde;a de la conferencia deber&iacute;a ser solo num&eacute;rica (0-99999).";//FB 2017
var EN_190 = "Por favor, seleccione un punto final para ver los detalles.";
var EN_192 = "Por favor, introduzca hasta 10 correos-e para M&uacute;ltiples correos-e de asistente.";
var EN_194 = "Por favor, seleccione un Asistente encargado del libro de direcciones myVRM.\nPara ver el libro de direcciones myVRM, hga clic sobre el bot&oacute;n Editar a la derecha del campo de texto.";
var EN_195 = "Por favor, seleccione un sal&oacute;n de la lista o proporcione su propia informaci&oacute;n de conexi&oacute;n";
var EN_203 = "Por favor, seleccione al menos un departamento para asociar con el sal&oacute;n.";
var EN_204 = "Cuando seleccione el punto final, por favor seleccione el Medio para &eacute;l.";
var EN_205 = "Por favor, seleccione un punto final, porque selecciona un tipo de medio.";
var EN_208 = "Ha alcanzado el l&iacute;mite m&aacute;ximo de caracteres permitidos para este campo.";
var EN_212 = "La asignaci&oacute;n avanzada del sal&oacute;n s&oacute;lo puede ser asignada a una conferencia sin-v&iacute;deo.\n Por favor, o elimine la asignaci&oacute;n avanzada del sal&oacute;n o cree una conferencia sin v&iacute;deo.";
var EN_219 = "El sal&oacute;n ya ha sido a&ntilde;adido.";

//Template
var EN_50 = "Por favor, introduzca el Nombre de plantilla.";
var EN_63 = "Se puede seleccionar un m&aacute;ximo de cinco plantillas.";

var EN_55 = "�Est&aacute; seguro de que desea quitar este grupo?";
var EN_56 = "�Est&aacute; seguro de que desea quitar los grupos seleccionados?";
var EN_57 = "�Est&aacute; seguro de que desea quitar esta conferencia? \nTodos los participantes invitados previamente ser&aacute;n notificados de la cancelaci&oacute;n.";
var EN_58 = "�Est&aacute; seguro de que desea quitar la(s) conferencia(s) seleccionada(s)?";
var EN_59 = "�Est&aacute; seguro de que desea quitar esta plantilla?";
var EN_60 = "�Est&aacute; seguro de que desea quitar las plantillas seleccionadas?";
var EN_77 = "Por favor, haga 'doble-clic' sobre la Plantilla o la Conferencia.";
var EN_191 = "Por favor, introduzca un n&uacute;mero entero positivo en la Hora inicial.";

// Food/Resource
var EN_156 = "Por favor, introduzca el Nombre del Pedido.";
var EN_157 = "Un pedido debe contener al menos un alimento. Si elije continuar, este pedido ser&aacute; borrado. \n�Est&aacute; seguro de que desea continuar?";
var EN_158 = "Por favor, seleccione una imagen.";
var EN_159 = "Por favor, seleccione una imagen del recurso.";
var EN_160 = "Por favor, seleccione un art&iacute;culo del recurso.";
var EN_161 = "Por favor, introduzca el Nombre del art&iacute;culo.";
var EN_162 = "Por favor, introduzca una Cantidad correcta del art&iacute;culo. Deber&iacute;a ser un n&uacute;mero entero positivo.";
var EN_163 = "Si la cantidad del art&iacute;culo es 0, este art&iacute;culo se eliminar&aacute;.\n�Seguro de que desea continuar?";
var EN_164 = "Este art&iacute;culo del rescurso ya existe en la lista. Por favor, introduzca un nombre diferente o selecci&oacute;nelo para editarlo.";
var EN_165 = "Por favor, introduzca un precio correcto. Deber&iacute;a ser un n&uacute;mero positivo y hasta con dos decimales.";
var EN_166 = "Todos los campos est&aacute;n vacios.\nEste pedido no se guardar&aacute; si es un pedido nuevo;\n o ser&aacute; eliminado si es un pedido viejo.\n\n�Est&aacute; seguro de que desea continuar?";
var EN_167 = "Su pedido para este sal&oacute;n ser&aacute; guardado.\nAdvertencia: Un pedido debe contener al menos un art&iacute;culo del recurso. Si elije continuar, este pedido ser&aacute; borrado. \n�Est&aacute; seguro de que desea continuar?";
var EN_168 = "Por favor, introduzca el Nombre del art&iacute;culo, o borre este art&iacute;culo nuevo.";
var EN_169 = "Por favor, seleccione una Categor&iacute;a.";
var EN_170 = "Por favor, introduzca el Precio de la unidad del art&iacute;culo.";
var EN_178 = "El sistema s&oacute;lo soporta im&aacute;genes en formato JPG y JPEG. Por favor, convi&eacute;rtala a formato JPG o JPEG usando alguna herramienta gr&aacute;fica, p.ej. Microsoft Photo Editor.";
var EN_179 = "Por favor, no cargue archivos ejecutables para evitar cualquier virus.";
var EN_188 = "Por favor, introduzca un n&uacute;mero entero positivo en Cantidad del art&iacute;culo.";

// terminal control
var EN_183 = "Por favor, introduzca el Nombre del punto final.";
var EN_184 = "Por favor, introduzca un correo-e de Invitado v&aacute;lido.";
var EN_206 = "�Est&aacute; seguro de que desea concluir con el punto final seleccionado?";


//misc
var EN_64 = "Debe introducir un Usuario nuevo para reemplazar al Usuario previo.";
var EN_65 = "Conferencia guardada correctamente en su Calendario de Lotus Notes.";
var EN_66 = "Conferencia guardada correctamente en su Calendario de Outlook.";
var EN_67 = "Usted autoriz&oacute; a este usuario como Usuario General pero le ha dado acceso de usuario para el men&uacute; de Preferencias de Super Administrador. \nSugerencias: 1) Autorice a este usuario como Super Administrador o \n2) Inhabilite su men&uacute; de Preferencias de Super Administrador.";
var EN_68 = "Lo sentimos, esta p&aacute;gina ha caducado. Por favor, haga clic aqu&iacute; para iniciar sesi&oacute;n otra vez.";
var EN_14 = "Por favor, introduzca un n&uacute;mero positivo o negativo en la Posici&oacute;n en Cadena.";
var EN_15 = "El n&uacute;mero de la tarjeta no es un n&uacute;mero positivo ni negativo, \no es mayor que el n&uacute;mero m&aacute;ximo.";
var EN_16 = "Por favor, introduzca un Enlace de la p&aacute;gina de inicio del usuario.";
var EN_73 = "La etiqueta de la URL en la descripci&oacute;n de la conferencia deber&iacute;a ir en parejas (<url></url>). \nPor favor, corr&iacute;jala y pruebe de nuevo.";
var EN_78 = "Por favor, use el bot&oacute;n SELECCIONAR para seleccionar un Sal&oacute;n de Conferencia Principal";
var EN_80 = "Por favor, haga doble-clic sobre Ubicaci&oacute;n.";
var EN_84 = "Por favor, introduzca un Tiempo de desactivaci&oacute;n de la conexi&oacute;n.";
var EN_85 = "Por favor, compruebe la fecha HASTA y vu&eacute;lvala a escribir.";
var EN_86 = "Sorry, ning&uacute;n Recurso tiene el ID de recurso de ";
var EN_88 = "La Ventana Original est&aacute; perdida. Esta ventana se cerrar&aacute;.\nPor favor, vuelva a la p&aacute;gina original e int&eacute;ntelo otra vez.";
var EN_100 = "El comentario no puede contener m&aacute;s de 500 caracteres.";
var EN_106 = "Por favor, espere mientras procesamos su solicitud.";
var EN_110 = "Por favor, introduzca la Informaci&oacute;n del Usuario nuevo.";
var EN_111 = "El Usuario nuevo y el Usuario previo tienen el mismo correo-e. Por favor, escr&iacute;balo de nuevo.";
var EN_115 = "Por favor, seleccione un valor en la Sesi&oacute;n de V&iacute;deo.";
var EN_116 = "Su navegador no soporta la impresi&oacute;n. Por favor, use la opci&oacute;n del men&uacute; para imprimir."
var EN_127 = "Advertencia: Ha cambiado las preferencias de la plantilla.\n�Guardar estos cambios nuevos?";

var EN_151 = "Por favor, introduzca el Nombre de la Categor&iacute;a.";
var EN_152 = "Por favor, introduzca el Nombre del archivo.";
var EN_154 = "Lo sentimos, no puede seleccionar Ordenar por sal&oacute;n ni no ha elegido ning&uacute;n sal&oacute;n.";
var EN_155 = "Lo sentimos, no puede seleccionar Ordenar por sal&oacute;n ni ha elegido Ninguno o Cualquiera en la opci&oacute;n Sal&oacute;n.";
var EN_174 = "�Est&aacute; seguro de que desea quitar este archivo?";
var EN_175 = "Error: fallo al cargar el archivo [raz&oacute;n - carpeta]. Por favor, proporcione esta informaci&oacute;n al administrador.";
var EN_176 = "Error: hay algunos archivos duplicados en la lista de archivos de carga.";
var EN_180 = "Por favor, seleccione al menos un informe para eliminar.";
var EN_196 = "Por favor, introduzca una Tarifa del Puerto MCU ISDN correcta. Deber&iacute;a ser un n&uacute;mero positivo con hasta dos decimales.";
var EN_197 = "Por favor, introduzca un coste de L&iacute;nea ISDN correcto. Deber&iacute;a ser un n&uacute;mero positivo con hasta dos decimales.";
var EN_198 = "Por favor, introduzca una Tarifa del Puerto IP correcta. Deber&iacute;a ser un n&uacute;mero positivo con hasta dos decimales.";
var EN_199 = "Por favor, introduzca un coste de L&iacute;nea IP correcto. Deber&iacute;a ser un n&uacute;mero positivo con hasta dos decimales.";
var EN_201 = "Esta p&aacute;gina ha caducado por inactividad.\nPor favor, inicie sesi&oacute;n e int&eacute;ntelo otra vez.";
var EN_207 = "Lo sentimos, ha ocurrido alg&uacute;n error en la SIM. Puede ser causado por Internet o por el Sistema,\Por favor, int&eacute;ntelo otra vez. Si contin&uacute;a ocurriendo, por favor notif&iacute;quelo al administrador.\nLa SIM se desconectar&aacute; ahora."
var EN_209 = "S&oacute;lo se permiten caracteres alfanum&eacute;ricos en los nombres de archivos.";
var EN_210 = "Por favor, introduzca una ID de correo-e &uacute;nica."

// browser
var EN_132 = "Su navegador tiene habilitado el bloqueo de pop-ups. Por favor, inhabil&iacute;telo para el Sitio Web myVMR, ya que myVMR usa los pop-ups que son necesarios para que usted introduzca la informaci&oacute;n.";



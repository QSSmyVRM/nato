﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="ns_MyVRM.en_ViewUserDetails" %>
<script language="javascript" type="text/javascript">
    function ClosePopUp()
    {   
        parent.document.getElementById("viewHostDetails").style.display = 'none';
        return false;
    }
</script>

<table cellpadding="0" cellspacing="0" border="0" align="center" width="100%">
	<tr>
		<td align="center">
            <table cellpadding="2" cellspacing="1" style="border-color:Black;border-width:1px;border-style:Solid;"  class="tableBody" align="center" width="50%">
              <tr>
                <td class="subtitleblueblodtext" align="center" colspan="2">
                    Detalles del usuario<br />
                </td>            
              </tr>
              <tr>
               <td align="right" style="width:40%" class="blackblodtext"><b>Nombre :</b></td> 
               <td align="left">
                   <asp:Label ID="lblUsrName" runat="server" Text="N/D"></asp:Label>
               </td>
              </tr>
              <tr>
               <td align="right" class="blackblodtext"><b>ID de Correo-e :</b></td> 
               <td align="left">
                   <asp:Label ID="lblUsrEmail" runat="server"></asp:Label>
               </td>
              </tr>
              <tr>
               <td align="right" class="blackblodtext"><b>Inicio de sesión AD/LDAP :</b></td> 
               <td align="left">
                   <asp:Label ID="lblUsrLogin" runat="server" Text="N/D"></asp:Label>
               </td>
              </tr>
              <% if(Session["timezoneDisplay"].ToString() == "1") { %> 
              <tr>
               <td align="right" class="blackblodtext"><b>Zona horaria :</b></td> 
               <td align="left">
                   <asp:Label ID="lblUsrTimeZone" runat="server"></asp:Label>
               </td>
              </tr>
              <% } else {%>
              <tr>
               <td align="right" class="blackblodtext"><b>Mostrar Zona horaria :</b></td> 
               <td align="left">Off</td>
              </tr>
              <% }%>
              <tr>
               <td align="right" class="blackblodtext"><b>Idioma preferido :</b></td> 
               <td align="left">
                   <asp:Label ID="lblUsrLang" runat="server"></asp:Label>
               </td>
              </tr>
              <tr>
               <td align="right" class="blackblodtext"><b>Idioma de Correo-e :</b></td> 
               <td align="left">
                   <asp:Label ID="lblUsrEmailLang" runat="server" Text="N/D"></asp:Label>
               </td>
              </tr>
              <tr>
               <td align="right" class="blackblodtext"><b>Bloquear Correos Electrónicos  :</b></td> 
               <td align="left">
                   <asp:Label ID="lblUsrBlockedEmail" runat="server"></asp:Label>
               </td>
              </tr>
              <tr>
               <td align="right" class="blackblodtext"><b>trabajo :</b></td> 
               <td align="left">
                   <asp:Label ID="lblUsrWork" runat="server" Text="N/D"></asp:Label>
               </td>
              </tr>
              <tr>
               <td align="right" class="blackblodtext"><b>móvil :</b></td> 
               <td align="left">
                   <asp:Label ID="lblUsrCell" runat="server" Text="N/D"></asp:Label>
               </td>
              </tr>
              <tr>
               <td align="center" colspan="2"><br />
                  <asp:Button ID="BtnUsrDetailClose" Text="Cerrar" CssClass="altShortBlueButtonFormat" runat="server" OnClientClick="javascript:return ClosePopUp()"></asp:Button>
               </td>
              </tr>
           </table>
		</td>
	</tr>
</table>

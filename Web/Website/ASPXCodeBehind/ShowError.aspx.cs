using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace en_ShowError
{
    public partial class ShowError : System.Web.UI.Page
    {
        #region Private Data Members

        protected String paramWinType = "";
        #endregion

        #region Page Load

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        #endregion

        #region Set QueryString

        private void SetQueryString()
        {
            if (Request.QueryString["winType"] != null)
                paramWinType = Request.QueryString["winType"].ToString();

        }

        #endregion
    }
}

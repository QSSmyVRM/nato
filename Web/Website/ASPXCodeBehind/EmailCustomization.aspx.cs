using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Text;
using System.Xml;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

namespace ns_EmailCustomization
{
    public partial class EmailCustomization : System.Web.UI.Page
    {
        #region protected Members
        protected System.Web.UI.WebControls.DataGrid dgPlaceHolders;
        protected System.Web.UI.WebControls.DropDownList lstEmailCategory;
        protected System.Web.UI.WebControls.DropDownList lstEmailMode;
        protected System.Web.UI.WebControls.DropDownList lstAllEmailMode;
        protected System.Web.UI.WebControls.DropDownList lstEmailType;
        protected System.Web.UI.WebControls.TextBox txtEmailSubject;
        protected System.Web.UI.WebControls.TextBox txtEmailLang;
        protected System.Web.UI.WebControls.Button cancel;
        protected System.Web.UI.WebControls.Button submit;
        protected System.Web.UI.WebControls.Label errLabel;
        protected DevExpress.Web.ASPxHtmlEditor.ASPxHtmlEditor dxHTMLEditor;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnEmailContID;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnEmailLangID;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnEmailLangName;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnCreateType;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnPlaceHolders;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnEmailMode;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnuserid;
        protected System.Web.UI.HtmlControls.HtmlGenericControl spnHeader; // FB 2570
        
        protected ns_Logger.Logger log = null;
        protected myVRMNet.NETFunctions obj = null;
        ArrayList colNames = null;
        String tp = "o";
        #endregion

        public EmailCustomization()
        {
            obj = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger();
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {
                errLabel.Text = "";
                //FB 1948 - Start
                if (Request.QueryString["tp"] != null)
                    tp = Request.QueryString["tp"].ToString();
                //FB 1948 - End
                // FB 2570 Starts
                if (Request.QueryString["tp"] != null)
                {
                    if (Request.QueryString["tp"].ToLower().Equals("o"))
                        spnHeader.InnerText = obj.GetTranslatedText("Customize Organization Emails");
                    else if (Request.QueryString["tp"].ToLower().Equals("u") || Request.QueryString["tp"].ToLower().Equals("au"))
                        spnHeader.InnerText = obj.GetTranslatedText("Customize My Email");
                    //else
                        //spnHeader.InnerText = "E-mail Customization";
                }
                // FB 2570 Ends
                if (!IsPostBack)
                {
                    hdnuserid.Value = Session["userID"].ToString();
                    hdnCreateType.Value = "1";
                    txtEmailLang.Enabled = true;
                    //Code Modified For FB 1948 - Start
                    //if (Request.QueryString["tp"] != null)
                    //{
                    //    tp = Request.QueryString["tp"].ToString();
                        if (Request.QueryString["tp"].ToLower().Equals("au"))
                        {

                            if (Session["UserToEdit"] != null)
                                hdnuserid.Value = Session["UserToEdit"].ToString();
                        }
                    //}
                    //FB 1948 - End
					//FB 2283 - Start
                    if (tp.Trim().ToLower() == "o")
                    {
                        if (Session["OrgEmailLangID"] != null) //Email Language
                            if (Session["OrgEmailLangID"].ToString() != "")
                            {
                                hdnEmailLangID.Value = Session["OrgEmailLangID"].ToString();
                                hdnCreateType.Value = "2";
                                txtEmailLang.Enabled = false;
                            }

                        if (hdnEmailLangID.Value.Trim() == "")
                        {
                            if (Session["OrgBaseLang"] != null) //Prefered Language
                                if (Session["OrgBaseLang"].ToString() != "")
                                    hdnEmailLangID.Value = Session["OrgBaseLang"].ToString();
                        }

                    }
                    else
                    {
                        if (Session["UsrEmailLangID"] != null) //Email Language
                            if (Session["UsrEmailLangID"].ToString() != "")
                            {
                                hdnEmailLangID.Value = Session["UsrEmailLangID"].ToString();
                                hdnCreateType.Value = "2";
                                txtEmailLang.Enabled = false;
                            }

                        if (hdnEmailLangID.Value.Trim() == "")
                        {
                            if (Session["UsrBaseLang"] != null) //Prefered Language
                                if (Session["UsrBaseLang"].ToString() != "")
                                    hdnEmailLangID.Value = Session["UsrBaseLang"].ToString();
                        }
                    }
					//2283 End
                    GetEmailType();
                    BindMailTypes(null, null);
                    if (Request.QueryString["m"] != null)
                        if (Request.QueryString["m"].ToString().Equals("1"))
                        {
                            errLabel.Text = obj.GetTranslatedText("Operation Successful!"); //FB 1830 - Translation
                            errLabel.Visible = true;
                        }

                }
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                errLabel.Text = ex.Message;
            }

        }


        #region GetEmailType
        /// <summary>
        /// GetEmailType
        /// </summary>
        protected void GetEmailType()
        {
            try
            {
                StringBuilder inXML = new StringBuilder();
                inXML.Append("<GetEmailTypes>");
                inXML.Append(obj.OrgXMLElement());
                inXML.Append("<userid>" + hdnuserid.Value + "</userid>");
                inXML.Append("</GetEmailTypes>");
                log.Trace(inXML.ToString());
                String outXML = obj.CallMyVRMServer("GetEmailTypes", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
                else
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);
                    XmlNodeList Nodes = xmldoc.SelectNodes("//emailtypes/mailtype");
                    CreateDtColumnNames();
                    DataTable dtable = obj.LoadDataTable(Nodes, colNames);
                    Session.Remove("MailTypeTable");
                    if (Session["MailTypeTable"] == null)
                        Session.Add("MailTypeTable", dtable);
                    else
                        Session["MailTypeTable"] = dtable;
                }
            }
            catch (Exception ex)
            {
                log.Trace("EmailCustomization: " + ex.Message);
            }
        }
        #endregion

        #region GetEmailContent
        /// <summary>
        /// GetEmailContent
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void GetEmailContent(Object sender, EventArgs e)
        {
            try
            {
                StringBuilder inXML = new StringBuilder();

                if (lstEmailCategory.SelectedValue == "3")
                    hdnEmailMode.Value = "4";
                else if (lstEmailCategory.SelectedValue != "2" && lstEmailCategory.SelectedValue != "4")
                    hdnEmailMode.Value = "0";
                                    

                inXML.Append("<GetEmailContent>");
                inXML.Append("<userid>" + hdnuserid.Value + "</userid>");
                inXML.Append("<emaillangid>" + hdnEmailLangID.Value + "</emaillangid>");
                inXML.Append("<emailmode>" + hdnEmailMode.Value + "</emailmode>");
                inXML.Append("<emailtype>" + lstEmailType.SelectedValue + "</emailtype>");
                inXML.Append("</GetEmailContent>");
                log.Trace(inXML.ToString());
                String outXML = obj.CallMyVRMServer("GetEmailContent", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
                else
                {
                    dxHTMLEditor.Html = "";
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);
                    XmlNode Node = null;

                    Node = xmldoc.SelectSingleNode("//emailcontent/emaillanguage");
                    if (Node != null)
                    {
                        txtEmailLang.Text = Node.InnerText.Trim().ToString();
                        hdnEmailLangName.Value = txtEmailLang.Text;
                    }

                    Node = xmldoc.SelectSingleNode("//emailcontent/emailtype");
                    if (Node != null)
                        lstEmailType.SelectedValue = Node.InnerText.ToString();

                    string confMode = "1";
                    Node = xmldoc.SelectSingleNode("//emailcontent/emailmode");
                    if (Node != null)
                        confMode = Node.InnerText.Trim();

                    try
                    {
                        if (lstEmailCategory.SelectedValue == "2")
                            lstEmailMode.SelectedValue = Node.InnerText.ToString();
                        else if (lstEmailCategory.SelectedValue == "4")
                            lstAllEmailMode.SelectedValue = Node.InnerText.ToString();
                    }
                    catch { }

                    Node = xmldoc.SelectSingleNode("//emailcontent/subject");
                    if (Node != null)
                        txtEmailSubject.Text = Node.InnerText.ToString();
                    
                    Node = xmldoc.SelectSingleNode("//emailcontent/body");
                    if (Node != null)
                        dxHTMLEditor.Html = Node.InnerXml;

                    Node = xmldoc.SelectSingleNode("//emailcontent/contentid");
                    if (Node != null)
                        hdnEmailContID.Value = Node.InnerText.ToString();

                    Node = xmldoc.SelectSingleNode("//emailcontent/placeholders/placeholderstr");
                    if (Node != null)
                        hdnPlaceHolders.Value = Node.InnerText.ToString();

                    XmlNodeList placeholdlist = null;
                    placeholdlist = xmldoc.SelectNodes("//emailcontent/placeholders/placeholder");
                    if (placeholdlist != null)
                    {
                        DataTable dtable = new DataTable();
                        colNames = new ArrayList();
                        colNames.Add("PlaceHolderID");
                        colNames.Add("Description");
                        dtable = obj.LoadDataTable(placeholdlist, colNames);

                        for (Int32 i = 0; i < dtable.Rows.Count; i++)
                        {
                            dtable.Rows[i]["PlaceHolderID"] = "{" + dtable.Rows[i]["PlaceHolderID"] + "}";
                            dtable.Rows[i]["Description"] = " - " + obj.GetTranslatedText(dtable.Rows[i]["Description"].ToString());//FB 2272
                        }

                        dgPlaceHolders.DataSource = dtable;
                        dgPlaceHolders.DataBind();
                    }
                             
                }
            }
            catch (Exception ex)
            {
                log.Trace("EmailCustomization: : " + ex.Message);
            }
        }
        #endregion

        #region SetEmailContent
        /// <summary>
        /// SetEmailContent
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SetEmailContent(Object sender, EventArgs e)
        {
            try
            {
                String targetPage = "" ;
                StringBuilder inXML = new StringBuilder();

                if (lstEmailCategory.SelectedValue == "2")
                    hdnEmailMode.Value = lstEmailMode.SelectedValue;
                else if (lstEmailCategory.SelectedValue == "3")
                    hdnEmailMode.Value = "4";
                else if (lstEmailCategory.SelectedValue == "4")
                    hdnEmailMode.Value = lstAllEmailMode.SelectedValue;
                else
                    hdnEmailMode.Value = "0";

                
                if (Request.QueryString["tp"] != null)
                {
                    if (Request.QueryString["tp"].ToLower().Equals("o"))
                        targetPage = "c";
                    else if (Request.QueryString["tp"].ToLower().Equals("u") || Request.QueryString["tp"].ToLower().Equals("au"))
                        targetPage = "u";
                }

                if (hdnCreateType.Value == "1")
                {
                    if (hdnEmailLangName.Value == txtEmailLang.Text.Trim())
                              txtEmailLang.Text += " 1";
                }

                inXML.Append("<SetEmailContent>");
                inXML.Append("<userid>" + hdnuserid.Value + "</userid>");
                inXML.Append(obj.OrgXMLElement());
                inXML.Append("<contentid>" + hdnEmailContID.Value + "</contentid>");
                inXML.Append("<emaillangid>" + hdnEmailLangID.Value + "</emaillangid>");
                inXML.Append("<emaillanguage>" + txtEmailLang.Text + "</emaillanguage>");
                inXML.Append("<emailtype>" + lstEmailType.SelectedValue + "</emailtype>");
                inXML.Append("<emailmode>" + hdnEmailMode.Value + "</emailmode>");
                inXML.Append("<subject>" + txtEmailSubject.Text + "</subject>");
                inXML.Append("<body>" + dxHTMLEditor.Html + "</body>");
                inXML.Append("<langOrigin>" + targetPage + "</langOrigin>");
                inXML.Append("<createType>" + hdnCreateType.Value + "</createType>");
                inXML.Append("<placeholderstr>" + hdnPlaceHolders.Value + "</placeholderstr>");
                inXML.Append("</SetEmailContent>");
                log.Trace(inXML.ToString());
                String outXML = obj.CallMyVRMServer("SetEmailContent", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
                else
                {
                    hdnEmailLangID.Value = "";
                    Response.Redirect("emailcustomization.aspx?m=1&tp="+tp);
                    //RedirectToTargetPage(null,null);
                }
            }
            catch (Exception ex)
            {
                log.Trace("EmailCustomization: : " + ex.Message);
            }
        }
        #endregion

        #region BindMailTypes
        /// <summary>
        /// BindMailTypes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BindMailTypes(Object sender, EventArgs e)
        {
            try
            {
                
                DataTable dtable = null;
                if (Session["MailTypeTable"] != null)
                {
                    DataTable sessiontable = (DataTable)Session["MailTypeTable"];
                    
                    dtable = sessiontable.Clone();
 
                    string filterExp = "emailcategory ='" + lstEmailCategory.SelectedValue + "'";
                    DataRow[] drArr = sessiontable.Select(filterExp);
                    for (int row = 0; row < drArr.Length; row++)
                    {
                        if (dtable.Columns.Contains("emailtype"))//FB 2272
                            drArr[row]["emailtype"] = obj.GetTranslatedText(drArr[row]["emailtype"].ToString());
                        dtable.ImportRow(drArr[row]);
                    }
                    lstEmailType.DataSource = dtable;
                    lstEmailType.DataBind();
                    lstAllEmailMode.SelectedValue = "1";
                    lstEmailMode.SelectedValue = "1";
                }
                GetEmailContent(null, null);
            }
            catch (Exception ex)
            {
                log.Trace("EmailCustomization: "+ ex.Message);
            }
        }
        #endregion

        #region Create Column Names
        /// <summary>
        /// CreateDtColumnNames
        /// </summary>
        private void CreateDtColumnNames()
        {
            colNames = new ArrayList();
            colNames.Add("emailtypeid");
            colNames.Add("emailtype");
            colNames.Add("emailcategory");
        }
        #endregion

        #region Redirect To TargetPage
        /// <summary>
        /// RedirectToTargetPage
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RedirectToTargetPage(Object sender, EventArgs e)
        {
            try
            {
                Session["EmailLanguageID"] = null;
                Session["Baselanguage"] = null;
                if (Request.QueryString["tp"] != null)
                {
                    if (Request.QueryString["tp"].ToLower().Equals("o"))
                        Response.Redirect("OrganisationSettings.aspx");
                    else
                    {
                        if (Request.QueryString["tp"].ToLower().Equals("au"))
                            Response.Redirect("ManageUserProfile.aspx?t=1");
                        else
                            Response.Redirect("ManageUserProfile.aspx");
                    }
                }
            }
            catch (Exception ex)
            {
                log.Trace("EmailCustomization:" + ex.Message);
            }
        }
        #endregion

    }
}
using System;
using System.Data;
using System.Xml;
using System.Web.UI.WebControls;
using System.Text;

/// <summary>
/// Summary description for ConferenceOrders
/// </summary>
/// 
namespace ns_HolidayDetails
{
    public partial class EditHolidayDetails : System.Web.UI.Page
    {
        #region Protected Data Members

        protected System.Web.UI.WebControls.Label lblHeader;
        protected System.Web.UI.WebControls.Label errLabel;
        protected System.Web.UI.WebControls.TextBox txtHolidayName;
        protected System.Web.UI.WebControls.TextBox txtbgcolor;
        protected System.Web.UI.WebControls.ListBox CustomDate;
        protected System.Web.UI.WebControls.CheckBox chkBlocked;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnTypeID;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnDateList;
        
        #endregion

        #region private Data Members

        myVRMNet.NETFunctions obj;
        ns_Logger.Logger log;
        protected String typeID = "";
        protected String format = "MM/dd/yyyy";
        protected Int32 CustomSelectedLimit = 100;

        #endregion

        #region EditHolidayDetails

        public EditHolidayDetails()
        {
            obj = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger();
        }

        #endregion

        #region Page Load 

        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
              
                errLabel.Text = "";
                if (Request.QueryString["m"] != null)
                    if (Request.QueryString["m"].ToString().Equals("1"))
                    {
                        errLabel.Text = obj.GetTranslatedText("Operation Successful!"); //FB 2272
                        errLabel.Visible = true;
                    }

                if (Request.QueryString["TypeID"] != null)
                {
                    if (Request.QueryString["TypeID"].ToString().Trim() != "")
                    {
                        typeID = Request.QueryString["TypeID"].ToString().Trim();
                        hdnTypeID.Value = typeID;
                    }
                }
                if (Session["FormatDateType"] != null)
                {
                    if (Session["FormatDateType"].ToString() != "")
                        format = Session["FormatDateType"].ToString();
                }

                if(typeID != "")
                    lblHeader.Text = obj.GetTranslatedText("Edit Color Day Details"); //FB 2272
                else
                    lblHeader.Text = obj.GetTranslatedText("Create New Color Day"); //FB 2272

                if (!IsPostBack)
                {
                    //CustomDate.Items.Clear();
                    BindData();
                }
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                errLabel.Text = "PageLoad: " + ex.StackTrace;
            }
        }
        #endregion

        #region BindData 

        private void BindData()
        {
            try
            {
                String inXML = "";
                inXML += "<GetOrgHolidays>";
                inXML += "  <userID>" + Session["userID"].ToString() + "</userID>";
                inXML += obj.OrgXMLElement();
                if (hdnTypeID.Value != "")
                    inXML += "  <typeID>" + hdnTypeID.Value + "</typeID>";
                else
                    inXML += "  <typeID>new</typeID>";
                inXML += "</GetOrgHolidays>";

                String outXML = obj.CallMyVRMServer("GetOrgHolidays", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") < 0)
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);
                    if(xmldoc.SelectSingleNode("//SystemHolidays/HolidayDescription") != null)
                        txtHolidayName.Text = xmldoc.SelectSingleNode("//SystemHolidays/HolidayDescription").InnerText;
                    if (xmldoc.SelectSingleNode("//SystemHolidays/Color") != null)
                        txtbgcolor.Text = xmldoc.SelectSingleNode("//SystemHolidays/Color").InnerText;

                    XmlNodeList nodes = xmldoc.SelectNodes("//SystemHolidays/Holidays/Holiday");
                    if (nodes != null && nodes.Count > 0)
                    {
                        for (Int32 n = 0; n < nodes.Count; n++)
                            CustomDate.Items.Add(myVRMNet.NETFunctions.GetFormattedDate(nodes[n].SelectSingleNode("Date").InnerText));
                    }
                }
                else
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                errLabel.Text = "BindData: " + ex.StackTrace;
            }
        }

        #endregion

        #region SetOrgHolidays 
        protected void SetOrgHolidays(Object sender, EventArgs e)
        {
            String outxml = "";
            StringBuilder outXML = new StringBuilder();
            String[] strDateList = null;
            try
            {
                outXML.Append("<SetOrgHolidays>");
                outXML.Append(obj.OrgXMLElement());
                outXML.Append("<SystemHolidays>");
                outXML.Append("<HolidayName>" + txtHolidayName.Text + "</HolidayName>");
                outXML.Append("<Color>" + txtbgcolor.Text + "</Color>");
                if (hdnTypeID.Value != "")
                    outXML.Append("<HolidayType>" + hdnTypeID.Value + "</HolidayType>");
                else
                    outXML.Append("<HolidayType></HolidayType>");
                
                if (hdnDateList.Value != "")
                {
                    strDateList = hdnDateList.Value.Split(',');

                    outXML.Append("<Holidays>");
                    if (strDateList != null)
                    {
                        for (int dts = 0; dts < strDateList.Length; dts++)
                        {
                            outXML.Append("<Holiday>");
                            outXML.Append("<Date>" + myVRMNet.NETFunctions.GetDefaultDate(strDateList[dts]) + "</Date>");
                            outXML.Append("<HolidayType></HolidayType>");
                            outXML.Append("</Holiday>");
                        }
                    }
                    outXML.Append("</Holidays>");
                }
                outXML.Append("</SystemHolidays>");
                outXML.Append("</SetOrgHolidays>");

                outxml = obj.CallCommand("SetOrgHolidays", outXML.ToString());

                if (outxml.IndexOf("<error>") < 0)
                {
                    MyVRMNet.LoginManagement objLogin = new MyVRMNet.LoginManagement();
                    objLogin.LoadOrgHolidays();
                    Response.Redirect("HolidayDetails.aspx?m=1");
                }
                else
                {
                    hdnDateList.Value = "";
                    errLabel.Visible = true;
                    errLabel.Text = obj.ShowErrorMessage(outxml);
                    if (CustomDate.Items.Count <= 0 && strDateList != null)
                    {
                      for (int dts = 0; dts < strDateList.Length ; dts++)
                        CustomDate.Items.Add(myVRMNet.NETFunctions.GetFormattedDate(strDateList[dts]));
                    }
                    return;
                }
            }
            catch (System.Threading.ThreadAbortException) { }
            catch (Exception ex)
            {
                errLabel.Text = ex.StackTrace;
                errLabel.Visible = true;
            }
        }
        #endregion
    }
}

using System;
using System.Xml;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using ExpertPdf.HtmlToPdf;

/// <summary>
/// Summary description for ConferenceOrders
/// </summary>
/// 
namespace ns_UserTemplates
{
    public partial class UserTemplates : System.Web.UI.Page
    {
        protected System.Web.UI.WebControls.Label errLabel;
        protected System.Web.UI.WebControls.DataGrid dgUserTemplates;
        protected System.Web.UI.WebControls.Table tblNoUserTemplates;

        myVRMNet.NETFunctions obj;
        public UserTemplates()
        {
            //
            // TODO: Add constructor logic here
            //
            obj = new myVRMNet.NETFunctions();
        }

        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                errLabel.Text = "";
                BindData();
                if (Request.QueryString["m"] != null)
                    if (Request.QueryString["m"].ToString().Equals("1"))
                    {
                        errLabel.Text = obj.GetTranslatedText("Operation Successful!");//FB 1830 - Translation
                        errLabel.Visible = true;
                    }
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                errLabel.Text = "PageLoad: " + ex.StackTrace;
            }

        }

        protected void BindData()
        {
            try
            {
                String inXML = "<login>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "<userID>" + Session["userID"].ToString() + "</userID>";
                inXML += "</login>";
                //Response.Write(obj.Transfer(inXML));
                String outXML = obj.CallMyVRMServer("GetUserTemplateList", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                //Response.Write(obj.Transfer(outXML));
                //                Response.End();
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);

                XmlTextReader xtr;
                DataSet ds = new DataSet();
                XmlNodeList nodes = xmldoc.SelectNodes("//UserTemplates/Template");
                foreach (XmlNode node in nodes)
                {
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.InferSchema);
                }
                if (ds.Tables.Count > 0)
                {
                    DataView dv = new DataView(ds.Tables[0]);
                    DataTable dt = dv.Table;

                    dgUserTemplates.DataSource = dv;
                    dgUserTemplates.DataBind();

                    Label lblTemp = new Label();
                    DataGridItem dgFooter = (DataGridItem)dgUserTemplates.Controls[0].Controls[dgUserTemplates.Controls[0].Controls.Count - 1];
                    lblTemp = (Label)dgFooter.FindControl("lblTotalRecords");
                    lblTemp.Text = nodes.Count.ToString();

                }
                else
                    tblNoUserTemplates.Visible = true;
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                errLabel.Text = ex.StackTrace;
                //Response.End();
            }

        }
        protected void BindRowsDeleteMessage(Object sender, DataGridItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType.Equals(ListItemType.Item) || e.Item.ItemType.Equals(ListItemType.AlternatingItem))
                {
                    LinkButton btnTemp = (LinkButton)e.Item.FindControl("btnDelete");
                    btnTemp.Attributes.Add("onclick", "if (confirm('" + obj.GetTranslatedText("Are you sure you want to delete this user template?") + "')){DataLoading(1); return true;} else {return false;}");
                }
            }
            catch (Exception ex)
            {
                errLabel.Text = ex.StackTrace;
                errLabel.Visible = true;
            }
        }

        protected void DeleteUserTempate(object sender, DataGridCommandEventArgs e)
        {
            try
            {
                String inXML = ""; // <login><userID>11</userID><edit/><delete><departmentID>1</departmentID></delete></login>
                inXML += "<login>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "  <userID>" + Session["userID"].ToString() + "</userID>";
                inXML += "  <userTemplateID>" + e.Item.Cells[0].Text + "</userTemplateID>";
                inXML += "</login>";
                //Response.Write(obj.Transfer(inXML));
                String outXML = obj.CallMyVRMServer("DeleteUserTemplate", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                //Response.Write(obj.Transfer(outXML));
                //Response.End();
                if (outXML.IndexOf("<error>") < 0)
                {
                    Response.Redirect("ManageUserTemplatesList.aspx?m=1");
                }
                else
                {
                    errLabel.Visible = true;
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                }
            }
            catch (Exception ex)
            {
                errLabel.Text = ex.StackTrace;
                errLabel.Visible = true;
            }
        }

        protected void EditUserTemplate(object sender, DataGridCommandEventArgs e)
        {
            try
            {
                Session.Add("UTID", e.Item.Cells[0].Text.ToString());
                Response.Redirect("EditUserTemplate.aspx");
            }
            catch (Exception ex)
            {
                errLabel.Text = ex.StackTrace;
                errLabel.Visible = true;
            }
        }
        protected void CreateNewTemplate(Object sender, EventArgs e)
        {
            try
            {
                Session.Add("UTID", "new");
                Response.Redirect("EditUserTemplate.aspx");
            }
            catch (Exception ex)
            {
                errLabel.Text = ex.StackTrace;
                errLabel.Visible = true;
            }
        }

    }


}
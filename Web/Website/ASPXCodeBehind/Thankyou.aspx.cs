using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class en_Thankyou : System.Web.UI.Page
{
    ns_Logger.Logger log;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Session["userID"] != null && Session["userID"].ToString() != "")
            {
                String localUserList = Application["OnlineUserList"].ToString();
                String user = Session["userID"].ToString() + ",";

                if (localUserList.Contains(user))
                {
                    String newlocalUserList = localUserList.Replace(user, "");
                    Application.Lock();
                    Application["OnlineUserList"] = newlocalUserList;
                    Application.UnLock();
                }
            }
            
            Session.Contents.RemoveAll();
            Session.Abandon();

            Response.Cookies.Remove("VRMuser");
            
        }
        catch (Exception ex)
        {
            log.Trace("Thankyou PageLoad: " + ex.StackTrace + " : " + ex.Message);
            
        }
    }
}

using System;
using System.Data;
using System.Xml;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using System.Text; //FB 1830 DeleteEmailLang
//FB 2481 Start
using System.IO;
using System.Web;
using System.Collections.Generic;
using DevExpress.Web.ASPxClasses;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxHtmlEditor;
//FB 2481 End

/// <summary>
/// Summary description for ConferenceOrders
/// </summary>
/// 
namespace ns_MyVRM
{
    public partial class UserProfile : System.Web.UI.Page
    {
        #region Private Data Members

        myVRMNet.NETFunctions obj;
        ns_Logger.Logger log;

        //code added for ticker -- Start
        private String tickerDisplay = "";
        private String tickerDisplay1 = "";
        //code added for ticker -- End
        #endregion

        #region Protected Members
        protected System.Web.UI.WebControls.Label lblHeader;
        protected System.Web.UI.WebControls.Label errLabel;
        protected System.Web.UI.WebControls.DropDownList lstTimeZone;
        protected System.Web.UI.WebControls.DropDownList lstDefaultGroup;
        protected System.Web.UI.WebControls.DropDownList lstSearchTemplate;
        protected System.Web.UI.WebControls.DropDownList lstLineRate;
        protected System.Web.UI.WebControls.DropDownList lstBridges;
        protected System.Web.UI.WebControls.DropDownList lstAddressType;
        protected System.Web.UI.WebControls.DropDownList lstMCUAddressType;
        protected System.Web.UI.WebControls.DropDownList lstVideoEquipment;
        protected System.Web.UI.WebControls.DropDownList lstProtocol;
        protected System.Web.UI.WebControls.ListBox lstDepartment;
        protected System.Web.UI.WebControls.DropDownList lstAddressBook;
        protected System.Web.UI.WebControls.DropDownList lstAdmin;
        protected System.Web.UI.WebControls.DropDownList lstEmailNotification;
        protected System.Web.UI.WebControls.DropDownList lstIsOutsideNetwork;
        protected System.Web.UI.WebControls.DropDownList lstSendBoth;
        protected System.Web.UI.WebControls.DropDownList lstUserRole;
        protected System.Web.UI.WebControls.DropDownList lstConnectionType;
        protected System.Web.UI.WebControls.DropDownList lstEncryption;

        protected System.Web.UI.WebControls.TextBox txtUserID;
        protected System.Web.UI.WebControls.TextBox txtUserFirstName;
        protected System.Web.UI.WebControls.TextBox txtUserLastName;
        protected System.Web.UI.WebControls.TextBox txtUserEmail;
        protected System.Web.UI.WebControls.TextBox txtUserEmail2;
        protected System.Web.UI.WebControls.TextBox txtUserLogin;
        protected System.Web.UI.WebControls.TextBox txtPassword1;
        protected System.Web.UI.WebControls.TextBox txtPassword2;
        protected System.Web.UI.WebControls.TextBox txtAccountExpiry;
        protected System.Web.UI.WebControls.TextBox txtTimeRemaining;
        protected System.Web.UI.WebControls.TextBox txtAddress;
        protected System.Web.UI.WebControls.TextBox txtEndpointID;
        protected System.Web.UI.WebControls.TextBox txtLocation;
        protected System.Web.UI.WebControls.TextBox txtEndpointName;
        protected System.Web.UI.WebControls.TextBox txtEPPassword1;
        protected System.Web.UI.WebControls.TextBox txtEPPassword2;
        protected System.Web.UI.WebControls.TextBox txtWebAccessURL;
        protected System.Web.UI.WebControls.TextBox txtAssociateMCUAddress;
        protected System.Web.UI.WebControls.TextBox txtLotusLoginName;
        protected System.Web.UI.WebControls.TextBox txtLotusPassword;
        protected System.Web.UI.WebControls.TextBox txtLotusDBPath;

        protected System.Web.UI.HtmlControls.HtmlInputHidden txtLevel;
        protected System.Web.UI.HtmlControls.HtmlInputHidden txtPassword1_1;
        protected System.Web.UI.HtmlControls.HtmlInputHidden txtPassword1_2;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnLocation;

        protected System.Web.UI.WebControls.Label lblLicencesRemaining;
        protected System.Web.UI.WebControls.Label lblTotalUsers;

        protected System.Web.UI.WebControls.RequiredFieldValidator reqPassword1;
        protected System.Web.UI.WebControls.RequiredFieldValidator reqBridges;
        protected System.Web.UI.WebControls.RequiredFieldValidator reqLastName;
        protected System.Web.UI.WebControls.RequiredFieldValidator reqFirstName;
        protected System.Web.UI.WebControls.RequiredFieldValidator reqUserEmail;

        protected System.Web.UI.WebControls.RegularExpressionValidator regPassword1;
        protected System.Web.UI.WebControls.RegularExpressionValidator regPassword2;
        protected System.Web.UI.WebControls.RegularExpressionValidator regEmail1_1;
        protected System.Web.UI.WebControls.RegularExpressionValidator regEmail1_2;
        protected System.Web.UI.WebControls.RegularExpressionValidator regEmail2_1;
        protected System.Web.UI.WebControls.RegularExpressionValidator regEmail2_2;

        protected System.Web.UI.WebControls.CompareValidator cmpValPassword1;
        protected System.Web.UI.WebControls.CompareValidator cmpValPassword2;
        protected System.Web.UI.WebControls.CustomValidator cusVal1;

        protected System.Web.UI.HtmlControls.HtmlTableRow EnableTR;//Code changed for MOJ Phase 2 QA Bug
        //Added for License modification START
        protected System.Web.UI.HtmlControls.HtmlTableRow trUser;
        protected System.Web.UI.WebControls.DropDownList DrpExc;
        protected System.Web.UI.WebControls.DropDownList DrpDom;
        protected System.Web.UI.WebControls.DropDownList DrpMob; //FB 1979
        //Added for License modification END

        //FB 1830 Starts
        protected System.Web.UI.WebControls.DropDownList lstLanguage;
        protected System.Web.UI.WebControls.TextBox txtEmailLang;
        protected System.Web.UI.WebControls.Button btnDefine;
        //FB 1830 Ends

        /* *** Code added for FB 1425 QA Bug -Start *** */
        protected System.Web.UI.HtmlControls.HtmlTableCell TzTD1;
        protected System.Web.UI.HtmlControls.HtmlTableCell TzTD2;
        protected System.Web.UI.HtmlControls.HtmlTableCell TzTD3;
        protected System.Web.UI.HtmlControls.HtmlTableCell TzTD4;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdntzone;
        /* *** Code added for FB 1425 QA Bug -End *** */

        //Code added by Offshore for FB Issue 1073 -- Start
        protected String format = "";
        protected System.Web.UI.WebControls.DropDownList DrpDateFormat;
        protected System.Web.UI.HtmlControls.HtmlInputHidden oldFormat;
        //Code added by Offshore for FB Issue 1073 -- End

        //Code added for AV Switch FB 1426,1429,1425 -- Start
        protected Boolean isAVEnabled = true;
        protected String enableAV = "0";
        protected System.Web.UI.WebControls.DropDownList DrpEnableAV;
        protected System.Web.UI.WebControls.Label AVLBL;


        protected Boolean isParEnabled = true;
        protected String enablePar = "0";
        protected System.Web.UI.WebControls.DropDownList DrpEnvPar;
        protected System.Web.UI.WebControls.Label ParLBL;

        protected System.Web.UI.HtmlControls.HtmlTableRow AVParams;

        protected String timeFormat = "";
        protected String tzDisplay = "";
        protected System.Web.UI.WebControls.DropDownList lstTimeFormat;
        protected System.Web.UI.WebControls.DropDownList lstTimeZoneDisplay;

        //Code added for AV Switch -- End

        protected System.Web.UI.HtmlControls.HtmlTableRow trLblAV; //Code Added For MOJ - Phase2

        //Code added for Ticker -- Start
        protected System.Web.UI.WebControls.DropDownList drpTickerStatus;
        protected System.Web.UI.WebControls.DropDownList drpTickerPosition;
        protected System.Web.UI.WebControls.DropDownList drpTickerSpeed;
        protected System.Web.UI.WebControls.DropDownList drpTickerDisplay;
        protected System.Web.UI.WebControls.TextBox txtTickerBknd;
        protected System.Web.UI.WebControls.TextBox txtFeedLink;
        protected System.Web.UI.WebControls.DropDownList drpTickerStatus1;
        protected System.Web.UI.WebControls.DropDownList drpTickerPosition1;
        protected System.Web.UI.WebControls.DropDownList drpTickerSpeed1;
        protected System.Web.UI.WebControls.DropDownList drpTickerDisplay1;
        protected System.Web.UI.WebControls.TextBox txtTickerBknd1;
        protected System.Web.UI.WebControls.TextBox txtFeedLink1;
        protected System.Web.UI.HtmlControls.HtmlTableCell feedLink;
        protected System.Web.UI.HtmlControls.HtmlTableCell feedLink1;
        //Ticker issues START
        //protected System.Web.UI.WebControls.RequiredFieldValidator reqtxtfeed;
        //protected System.Web.UI.WebControls.RequiredFieldValidator reqtxtfeed1;
        //protected System.Web.UI.WebControls.RequiredFieldValidator reqTxtTickerBknd1;
        //public System.Web.UI.WebControls.RequiredFieldValidator reqTxtTickerBknd;
        //Ticker issues End
        //Code added fro Ticker -- End

        /**** code added for room search  ****/

        protected System.Web.UI.HtmlControls.HtmlInputHidden selectedloc;
        protected System.Web.UI.HtmlControls.HtmlInputHidden locstrname;
        protected System.Web.UI.HtmlControls.HtmlSelect RoomList;
        protected System.Web.UI.WebControls.TextBox txtExchangeID; //Cisco Telepresence fix
        protected System.Web.UI.HtmlControls.HtmlTableCell TDprefLocation;
        protected System.Web.UI.HtmlControls.HtmlAnchor roomclick;
        protected System.Web.UI.HtmlControls.HtmlImage roomimage;
        protected System.Web.UI.WebControls.TextBox txtApiportno;//API Port...
        //Added for FB 1642-Audio add on - Starts 
        protected System.Web.UI.WebControls.CheckBox chkAudioAddon;
        protected System.Web.UI.WebControls.TextBox txtWorkPhone;
        protected System.Web.UI.WebControls.TextBox txtCellPhone;
        protected System.Web.UI.WebControls.TextBox txtConfCode;
        protected System.Web.UI.WebControls.TextBox txtLeaderPin;
        //Added for FB 1642-Audio add on - End 

        protected System.Web.UI.WebControls.CheckBox ChkBlockEmails;//FB 1860
        protected System.Web.UI.WebControls.Button BtnBlockEmails;//FB 1860
        protected System.Web.UI.WebControls.Label LblBlockEmails;//FB 1860

        protected System.Web.UI.HtmlControls.HtmlTableRow tdOtherSettings; //FB 1985
        protected System.Web.UI.WebControls.Button btnlogin;

        protected System.Web.UI.WebControls.DropDownList DrpPIMNotifications;//FB 2141
        //FB 2227 Start
        protected System.Web.UI.WebControls.TextBox txtIntvideonum;
        protected System.Web.UI.WebControls.TextBox txtExtvideonum;
        //FB 2227 End
        
        //FB 2268 start
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnHelpReq;
        protected System.Web.UI.WebControls.ListBox lstHelpReqEmail;
        protected System.Web.UI.WebControls.TextBox txtHelpReqEmail;
        protected System.Web.UI.WebControls.TextBox txtHelpReqPhone;
        protected System.Web.UI.WebControls.Button btnAddHelpReq;
        protected System.Web.UI.WebControls.Button btnnewSubmit; // FB 2025
        //FB 2268 start
        protected System.Web.UI.HtmlControls.HtmlGenericControl spanpassword;//FB 2339
        protected String enablePass = "0";//FB 2339
        protected System.Web.UI.WebControls.DropDownList drpSurveyEmail;//FB 2348

        protected System.Web.UI.WebControls.CheckBox chkVNOC;//FB 2608

        //FB 2481 Start
        protected AjaxControlToolkit.ModalPopupExtender PrivateVMRPopup;
        protected DevExpress.Web.ASPxHtmlEditor.ASPxHtmlEditor PrivateVMR;
       //FB 2481 End
		protected System.Web.UI.WebControls.ImageButton delEmailLang; //FB 2502
        //FB 2599 Starts
        //FB 2262
        protected System.Web.UI.WebControls.TextBox txtVidyoURL;
        protected System.Web.UI.WebControls.TextBox txtPin;
        protected System.Web.UI.WebControls.TextBox txtConfirmPin;
        protected System.Web.UI.WebControls.TextBox txtExtension;
        protected System.Web.UI.HtmlControls.HtmlTableRow trpin;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdMeetlink;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdExten;
        protected int Cloud = 0;//FB 2262
        //FB 2599 End
        //FB 2392-Whygo Starts
        protected System.Web.UI.HtmlControls.HtmlInputText txtParentReseller;
        protected System.Web.UI.HtmlControls.HtmlInputText txtCorpUser;
        protected System.Web.UI.WebControls.DropDownList lstRegion;
        protected System.Web.UI.HtmlControls.HtmlInputText txtESUserID;
        protected System.Web.UI.HtmlControls.HtmlTable whygouserSettings;
        //FB 2392-Whygo End
        protected System.Web.UI.WebControls.CheckBox chkSecured; //FB 2595
        protected System.Web.UI.WebControls.Label LblSecured;//FB 2595
        #endregion

        public UserProfile()
        {
            obj = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger();
            //
            // TODO: Add constructor logic here
            //
        }

        #region Methods Executed on Page Load

        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            { //FB 2025 Starts
                if (Request.QueryString["t"] == null)
                    btnnewSubmit.Visible = false;
                // Fb 2025 end
                //PSU Fix
                if (Application["CosignEnable"] == null)
                    Application["CosignEnable"] = "0";

                //FB 1985 - Start
                if (Application["Client"].ToString().ToUpper() == "DISNEY")  //FB 2611
                    tdOtherSettings.Attributes.Add("style", "display:none");
                //FB 1985 - End

                if (Session["UsrCrossAccess"] != null && Session["organizationID"] != null) //FB 1598
                {
                    if (Session["UsrCrossAccess"].ToString().Equals("1") && !Session["organizationID"].ToString().Equals("11"))
                    {
                        lstDefaultGroup.Enabled = true;
                        lstSearchTemplate.Enabled = true;
                        lstBridges.Enabled = true;
                        //roomimage.Attributes.Add("style", "display:block");//FB 1598
                        roomimage.Visible = true; //FB 1598

                    }
                }
                //FB 2339 Start
                if (Session["EnablePasswordRule"] != null)
                {
                    if (Session["EnablePasswordRule"].ToString() != "")
                        enablePass = Session["EnablePasswordRule"].ToString();
                    if (enablePass == "1")
                    spanpassword.Attributes.Add("style", "display:block");
                    else
                    spanpassword.Attributes.Add("style", "display:none");
                }
                //FB 2339 End
                //Code added by Offshore for FB Issue 1073 -- Start
                if (Session["FormatDateType"] != null)
                {
                    if (Session["FormatDateType"].ToString() != "")
                        format = Session["FormatDateType"].ToString();
                }
                //Code added by Offshore for FB Issue 1073 -- End

                //Code added for AV Switch  FB 1425,1426,1429-- Start
                if (Session["enableAV"] != null)
                {
                    if (Session["enableAV"].ToString() != "")
                        enableAV = Session["enableAV"].ToString();
                }

                if (Session["enableParticipants"] != null)
                {
                    if (Session["enableParticipants"].ToString() != "")
                        enablePar = Session["enableParticipants"].ToString();
                }

                if (Session["timeFormat"] != null)
                {
                    if (Session["timeFormat"].ToString() != "")
                        timeFormat = Session["timeFormat"].ToString();
                }
                if (Session["timeZoneDisplay"] != null)
                {
                    if (Session["timeZoneDisplay"].ToString() != "")
                        tzDisplay = Session["timeZoneDisplay"].ToString();
                }
                //code added for ticker --- Start

                if (Session["tickerDisplay"] != null)
                {
                    if (Session["tickerDisplay"].ToString() != "")
                        tickerDisplay = Session["tickerDisplay"].ToString();
                }

                if (Session["tickerDisplay1"] != null)
                {
                    if (Session["tickerDisplay1"].ToString() != "")
                        tickerDisplay1 = Session["tickerDisplay1"].ToString();
                }

                if (drpTickerDisplay.SelectedValue.Trim() == "1")
                {
                    txtFeedLink.Attributes.Add("style", "display:block");
                    feedLink.Attributes.Add("style", "display:block");
                }
                else
                {

                    txtFeedLink.Attributes.Add("style", "display:none");
                    feedLink.Attributes.Add("style", "display:none");
                }

                if (drpTickerDisplay1.SelectedValue.Trim() == "1")
                {
                    txtFeedLink1.Attributes.Add("style", "display:block");
                    feedLink1.Attributes.Add("style", "display:block");

                }
                else
                {
                    txtFeedLink1.Attributes.Add("style", "display:none");
                    feedLink1.Attributes.Add("style", "display:none");

                }
                //code added for ticker --- End
                if (Session["admin"] != null)
                {
                    if (Session["admin"].ToString().Equals("0"))
                    {
                        isAVEnabled = false;
                        isParEnabled = false;

                        //FB 1860
                        LblBlockEmails.Attributes.Add("style", "display:none");
                        ChkBlockEmails.Attributes.Add("style", "display:none");
                        BtnBlockEmails.Attributes.Add("style", "display:none");
                        //FB 1860

                        chkVNOC.Enabled = false;//FB 2608
                        
                    }
                    //FB 2097 start
                    if (Session["admin"].ToString().Trim() != "2")
                    {
                        DrpMob.Enabled = false;
                        DrpPIMNotifications.Enabled = false;
                        DrpExc.Enabled = false;
                        DrpDom.Enabled = false;
                    }
                    //FB 2097 end
                }
                //FB 2595 Start
                if (Session["SecureSwitch"] != null)
                {
                    if (Session["SecureSwitch"].ToString() == "1")
                    {
                        LblSecured.Visible = true;
                        chkSecured.Visible = true;
                    }
                    else
                    {
                        LblSecured.Visible = false;
                        chkSecured.Visible = false;
                    }
                }
                if (Session["admin"] != null)
                {
                    if (Session["admin"].ToString().Trim() == "2")
                        chkSecured.Enabled = true;
                    else
                        chkSecured.Enabled = false;
                }
                //FB 2595 End
                if (!isAVEnabled)
                {
                    AVLBL.Attributes.Add("style", "display:none");
                    DrpEnableAV.Attributes.Add("style", "display:none");
                }

                if (!isParEnabled)
                {
                    ParLBL.Attributes.Add("style", "display:none");
                    DrpEnvPar.Attributes.Add("style", "display:none");
                }

                if (Application["Client"].ToString().ToUpper().Equals("BCS") || Application["Client"].ToString().ToUpper().Equals("MOJ"))
                    AVParams.Attributes.Add("style", "display:none");

                //Code Modified For MOJ Phase2 - Start
                if (Application["Client"].ToString().ToUpper().Equals("MOJ"))
                {
                    trLblAV.Attributes.Add("style", "display:none");
                }
                //Code Modified For MOJ Phase2 - End  

                //Ticker changes - Start
                if (txtUserID.Text.Trim().ToLower().Equals("new"))
                {
                    if (drpTickerDisplay.SelectedValue == "0")
                    {
                        txtFeedLink.Attributes.Add("style", "display:none");
                        feedLink.Attributes.Add("style", "display:none");

                    }
                    if (drpTickerDisplay1.SelectedValue == "0")
                    {
                        txtFeedLink1.Attributes.Add("style", "display:none");
                        feedLink1.Attributes.Add("style", "display:none");

                    }
                }
                //Ticker changes - End
                
                //FB 2594 - Starts
                whygouserSettings.Visible = false;
                if (Session["EnablePublicRooms"] != null)
                {
                    if (Session["EnablePublicRooms"].ToString() == "1")
                        whygouserSettings.Visible = true;
                }
                //FB 2594 - End

                if (!IsPostBack)
                {
                    //Response.Write(Application["ssoMode"].ToString().ToUpper().Equals("NO"));
                    txtUserID.Text = Session["userID"].ToString();
                    if (Request.QueryString["t"] != null)
                        if (Request.QueryString["t"].ToString().Equals("1"))
                            txtUserID.Text = Session["UserToEdit"].ToString();
                    BindDataUser();
                }
                if (Request.QueryString["m"] != null)
                    if (Request.QueryString["m"].ToString().Equals("1"))
                    {
                        //Code added by Offshore for FB Issue 1073 -- Start
                        if (Request.QueryString["dt"] != null)
                        {
                            if (Request.QueryString["dt"].ToString().Equals("1"))
                            {
                                errLabel.Text = obj.GetTranslatedText("Operation Successful! Please logout and re-login to see the changes.");//FB 1830 - Translation
                                errLabel.Visible = true;
                            }
                            //Code added by Offshore for FB Issue 1073 -- End
                            else
                            {

                                errLabel.Text = obj.GetTranslatedText("Operation Successful!");//FB 1830 - Translation
                                errLabel.Visible = true;
                            }
                        }
                    }
                /* *** Code added for FB 1425 QA Bug -Start *** */

                if (Application["Client"].ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.MOJ))
                {
                    EnableTR.Attributes.Add("style", "display:none");//Code changed for MOJ Phase 2 QA Bug
                    TzTD1.Attributes.Add("style", "display:none");
                    TzTD2.Attributes.Add("style", "display:none");
                    TzTD3.Attributes.Add("style", "display:none");
                    TzTD4.Attributes.Add("style", "display:none");

                    if (hdntzone.Value == "")
                    {
                        lstTimeZone.SelectedValue = "31";
                        lstTimeZone.SelectedValue = Session["systemTimeZoneID"].ToString();

                        //String timezne = obj.GetSystemTimeZone(Application["COM_ConfigPath"].ToString());
                        String timezne = obj.GetSystemTimeZone(Application["MyVRMServer_ConfigPath"].ToString());//FB 2027
                        if (timezne != "")
                        {
                            lstTimeZone.ClearSelection();
                            lstTimeZone.SelectedValue = timezne;
                        }
                    }
                }

                /* *** Code added for FB 1425 QA Bug -End *** */
                //Added for FB 1428 Start
                if (Application["Client"].ToString().ToUpper() == "MOJ")
                {
                    lstUserRole.Items[2].Text = "Hearing Administrator";

                    if (lstUserRole.Items.FindByValue("4") != null)
                        lstUserRole.Items.Remove(lstUserRole.Items.FindByValue("4"));
                    if (lstUserRole.Items.FindByValue("5") != null)
                        lstUserRole.Items.Remove(lstUserRole.Items.FindByValue("5"));
                    if (lstUserRole.Items.FindByValue("6") != null)
                        lstUserRole.Items.Remove(lstUserRole.Items.FindByValue("6"));

                    if (lstAdmin.Items.FindByValue("4") != null)
                        lstAdmin.Items.Remove(lstAdmin.Items.FindByValue("4"));
                    if (lstAdmin.Items.FindByValue("5") != null)
                        lstAdmin.Items.Remove(lstAdmin.Items.FindByValue("5"));
                    if (lstAdmin.Items.FindByValue("6") != null)
                        lstAdmin.Items.Remove(lstAdmin.Items.FindByValue("6"));

                }

                //Added for FB 1428 End

                if (Session["UsrCrossAccess"] != null)
                {
                    if (Session["UsrCrossAccess"].ToString() != "1" || Session["organizationID"].ToString() != "11")
                    {
                        if (lstUserRole.Items.FindByText("Site Administrator") != null)
                        {
                            for (int i = 0; i < lstAdmin.Items.Count; i++)
                            {
                                if (lstAdmin.Items[i].Value == lstUserRole.Items.FindByText("Site Administrator").Value)
                                {
                                    lstAdmin.Items.Remove(lstAdmin.Items[i]);
                                    break;
                                }
                            }

                            lstUserRole.Items.Remove(lstUserRole.Items.FindByText("Site Administrator"));
                        }
                    }
                }
               
				//FB 2262,//FB 2599 - Starts
                if (Session["Cloud"] != null)
                {
                    if (Session["Cloud"].ToString() != "")
                        Int32.TryParse(Session["Cloud"].ToString(), out Cloud);
                }
                if (Cloud == 1)
                {
                    btnnewSubmit.Visible = false;
                    txtVidyoURL.Visible = true;
                    trpin.Visible = true;
                    txtExtension.Visible = true;
                    tdExten.Visible = true;
                    tdMeetlink.Visible = true;
                }
                //FB 2262,//FB 2599 - Ends

                BindRoomToList();
            }
            catch (Exception ex)
            {
                //                Response.Write(ex.Message);
                errLabel.Visible = true;
                errLabel.Text = "PageLoad: " + ex.StackTrace;
                log.Trace(ex.StackTrace);
            }

        }
        protected void BindData(Object sender, EventArgs e)
        {
            BindDataUser();
        }

        protected void BindDataUser()
        {
            try
            {
                //PSU Fix
                if (Application["ssoMode"].ToString().ToUpper().Equals("YES")
                    || Application["CosignEnable"].ToString().Equals("1"))
                {
                    txtUserEmail.Enabled = false;
                    txtUserEmail2.Enabled = false;
                    reqUserEmail.Enabled = false;
                    regEmail1_1.Enabled = false;
                    regEmail1_1.Enabled = false;
                    regEmail1_2.Enabled = false;
                    regEmail2_1.Enabled = false;
                    regEmail2_2.Enabled = false;
                    if (Application["Client"].ToString().ToUpper().Equals("WASHU"))
                    {
                        txtUserEmail.Enabled = false;
                        txtUserEmail2.Enabled = false;
                        reqUserEmail.Enabled = false;
                        regEmail1_1.Enabled = false;
                        regEmail1_1.Enabled = false;
                        regEmail1_2.Enabled = false;
                        regEmail2_1.Enabled = false;
                        regEmail2_2.Enabled = false;
                    }
                    txtUserFirstName.Enabled = false;
                    txtUserLastName.Enabled = false;
                    txtPassword1.Enabled = false;
                    txtPassword2.Enabled = false;
                    txtUserLogin.Enabled = false;
                    reqPassword1.Enabled = false;
                    cmpValPassword1.Enabled = false;
                    cmpValPassword2.Enabled = false;
                    reqFirstName.Enabled = false;
                    reqLastName.Enabled = false;
                    regPassword1.Enabled = false;
                    regPassword2.Enabled = false;
                    //Ticker Fixes START
                    //reqtxtfeed.Enabled = false; //Ticker changes
                    //reqtxtfeed1.Enabled = false; //Ticker Changes
                    //reqTxtTickerBknd.Enabled = false; //Ticker Changes
                    //reqTxtTickerBknd1.Enabled = false; //Ticker Changes
                    //Ticker Fixes End
                }
                lblHeader.Text = obj.GetTranslatedText("Create New User");//FB 1830 - Translation
                /* Get Timezones list from the system */
                String selTZ = "-1";
                lstTimeZone.ClearSelection();
                obj.GetTimezones(lstTimeZone, ref selTZ);

                /* Get Groups List in the system */
                if (txtUserID.Text.Trim() != "" && !(txtUserID.Text.Trim().ToLower().Contains("new"))) //FB 1598
                    obj.GetGroups(lstDefaultGroup, txtUserID.Text.Trim());
                else
                    obj.GetGroups(lstDefaultGroup);

                /* Get User Roles in the system */
                obj.GetUserRoles(lstUserRole, "ID", "name");
                obj.GetUserRoles(lstAdmin, "ID", "level");
                /* Get Search Templates in the system */
                obj.GetSearchTemplateList(lstSearchTemplate);
                /* Get Line Rate in the system */
                obj.BindLineRate(lstLineRate);
                /* Get All bridges/MCUs in the system */
                obj.BindBridges(lstBridges);
                /* Get Address Type in the system */
                obj.BindAddressType(lstAddressType);
                /* Get Address Type in the system for MCU Address Type dropdown */
                obj.BindAddressType(lstMCUAddressType);
                /* Get Video Equipment list in the system */
                obj.BindVideoEquipment(lstVideoEquipment);
                /* Get All available Video protocols */
                obj.BindVideoProtocols(lstProtocol);
                /* Get Departments in the system */
                obj.GetManageDepartment(lstDepartment);
                /* Get All Connection types */
                obj.BindDialingOptions(lstConnectionType);

                //FB 2392-WhyGo Starts
                int r = 0;
                foreach (myVRMNet.NETFunctions.Region myValue in Enum.GetValues(typeof(myVRMNet.NETFunctions.Region)))
                {
                    ListItem li = new ListItem(myValue.ToString(), ((Int32)myValue).ToString());
                    lstRegion.Items.Insert(r++, li);
                }
                //FB 2392-WhyGo End

                txtUserFirstName.Text = "";
                txtUserLastName.Text = "";
                txtUserLogin.Text = "";
                txtPassword1.Text = "";
                txtPassword2.Text = "";
                txtPassword1_1.Value = "";
                txtPassword1_2.Value = "";
                txtUserEmail.Text = "";
                txtUserEmail2.Text = "";
                //Added for FB 1642-Audio add on - Starts 
                txtWorkPhone.Text = "";
                txtCellPhone.Text = "";
                txtConfCode.Text = "";
                txtLeaderPin.Text = "";
                //Added for FB 1642-Audio add on - End 
                //FB 2227 Start
                txtIntvideonum.Text = "";
                txtExtvideonum.Text = "";
                //FB 2227 End
                txtEndpointName.Text = "";
                txtEPPassword1.Text = "";
                txtEPPassword2.Text = "";
                txtAddress.Text = "";
                txtWebAccessURL.Text = "";
                txtAssociateMCUAddress.Text = "";
                txtAccountExpiry.Text = "";
                txtTimeRemaining.Text = "";
                lstAddressBook.ClearSelection();
                lstAddressType.ClearSelection();
                lstBridges.ClearSelection();
                lstConnectionType.ClearSelection();
                lstDefaultGroup.ClearSelection();
                lstEmailNotification.ClearSelection();
                lstLineRate.ClearSelection();
                lstProtocol.ClearSelection();
                lstSearchTemplate.ClearSelection();
                lstTimeZone.ClearSelection();
                lstUserRole.ClearSelection();
                lstVideoEquipment.ClearSelection();
                lstIsOutsideNetwork.ClearSelection();
                lstEncryption.ClearSelection();
                lstAddressType.ClearSelection();
                lstSendBoth.ClearSelection();
                //Code added For Date Format by offshore - FB Issue No 1073 - Start
                DrpDateFormat.ClearSelection();
                //Code added For Date Format  by offshore - FB Issue No 1073 - End
                //Code added for AV Switch FB 1426,1429,1425
                DrpEnableAV.ClearSelection();
                DrpEnvPar.ClearSelection();
                lstTimeFormat.ClearSelection();
                lstTimeZoneDisplay.ClearSelection();

                //Ticker Start
                txtTickerBknd.Text = "";
                txtFeedLink.Text = "";
                drpTickerDisplay.ClearSelection();
                drpTickerPosition.ClearSelection();
                drpTickerSpeed.ClearSelection();
                drpTickerStatus.ClearSelection();
                txtTickerBknd1.Text = "";
                txtFeedLink1.Text = "";
                drpTickerDisplay1.ClearSelection();
                drpTickerPosition1.ClearSelection();
                drpTickerSpeed1.ClearSelection();
                drpTickerStatus1.ClearSelection();
                //Ticker End
                if (!txtUserID.Text.Trim().ToLower().Equals("new"))
                {
                    GetUserProfile();
                }
                else
                {
                    //FB 1830 starts
                    obj.GetLanguages(lstLanguage);//FB 1830
                    lstLanguage.Items.FindByValue("1").Selected = true;
                    //FB 1830 Ends

                    //Ticker changes - Start
                    txtFeedLink.Attributes.Add("style", "display:none");
                    feedLink.Attributes.Add("style", "display:none");
                    txtFeedLink1.Attributes.Add("style", "display:none");
                    feedLink1.Attributes.Add("style", "display:none");
                    //FB 1567 -- Start
                    drpTickerStatus.SelectedValue = "1";
                    drpTickerStatus1.SelectedValue = "1";
                    txtTickerBknd.Text = "#660066";
                    txtTickerBknd1.Text = "#99ff99";
                    //FB 1567 -- End

                    //Ticker changes - End
                }
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                errLabel.Text = "BindData: " + ex.StackTrace;
                log.Trace(ex.Message + " : " + ex.StackTrace);
            }
        }

        protected void GetUserProfile()
        {
            //string emailBlock = "";//FB 1860 FB 2027

            try
            {
                Session["multisiloOrganizationID"] = null; //FB 2274
                String inXML = "<login>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID><user><userID>" + txtUserID.Text + "</userID></user></login>";//Organization Module Fixes
                //FB 2027 Start
                String outXML = obj.CallMyVRMServer("GetOldUser", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                //String outXML = obj.CallCOM("GetOldUser", inXML, Application["COM_ConfigPath"].ToString());
                //FB 2027 End
                //Response.Write(obj.Transfer(outXML));
                if (outXML.IndexOf("<error>") < 0)
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);
                    lblHeader.Text = obj.GetTranslatedText("Edit Existing User");//FB 1830 - Translation
                    txtUserFirstName.Text = xmldoc.SelectSingleNode("//oldUser/userName/firstName").InnerText;
                    txtUserLastName.Text = xmldoc.SelectSingleNode("//oldUser/userName/lastName").InnerText;
                    txtUserLogin.Text = xmldoc.SelectSingleNode("//oldUser/login").InnerText;
                    txtPassword1.Attributes.Add("value", xmldoc.SelectSingleNode("//oldUser/password").InnerText);
                    txtPassword2.Attributes.Add("value", xmldoc.SelectSingleNode("//oldUser/password").InnerText);
                    txtPassword1_1.Value = xmldoc.SelectSingleNode("//oldUser/password").InnerText;
                    txtPassword1_2.Value = xmldoc.SelectSingleNode("//oldUser/password").InnerText;
                    txtUserEmail.Text = xmldoc.SelectSingleNode("//oldUser/userEmail").InnerText;
                    txtUserEmail2.Text = xmldoc.SelectSingleNode("//oldUser/alternativeEmail").InnerText;
                    //Code added by Offshore for FB Issue 1073 -- Start
                    // txtAccountExpiry.Text = xmldoc.SelectSingleNode("//oldUser/expiryDate").InnerText;
                    txtAccountExpiry.Text = myVRMNet.NETFunctions.GetFormattedDate(xmldoc.SelectSingleNode("//oldUser/expiryDate").InnerText);
                    //Code added by Offshore for FB Issue 1073 -- end
                    txtTimeRemaining.Text = xmldoc.SelectSingleNode("//oldUser/initialTime").InnerText;
                    txtLevel.Value = xmldoc.SelectSingleNode("//oldUser/status/level").InnerText;
                    //FB 1642-Audio add on -Starts
                    txtWorkPhone.Text = xmldoc.SelectSingleNode("//oldUser/workPhone").InnerText;
                    txtCellPhone.Text = xmldoc.SelectSingleNode("//oldUser/cellPhone").InnerText;
                    txtConfCode.Text = xmldoc.SelectSingleNode("//oldUser/conferenceCode").InnerText;
                    txtLeaderPin.Text = xmldoc.SelectSingleNode("//oldUser/leaderPin").InnerText;
                    if (xmldoc.SelectSingleNode("//oldUser/Audioaddon").InnerText == "1")
                    {
                        chkAudioAddon.Checked = true;
                    }
                    else
                    {
                        chkAudioAddon.Checked = false;
                    }
                    //FB 1642-Audio add on - End
                    //FB 2227 Start
                    txtIntvideonum.Text = xmldoc.SelectSingleNode("//oldUser/IntVideoNum").InnerText;
                    txtExtvideonum.Text = xmldoc.SelectSingleNode("//oldUser/ExtVideoNum").InnerText;
                    //FB 2227 End
                    //FB 1830 - Start
                    XmlNodeList nodes = xmldoc.SelectNodes("//oldUser/languages/language");
                    //LoadLanguage(nodes);
                    obj.GetLanguages(lstLanguage);
                    if (!xmldoc.SelectSingleNode("//oldUser/languageID").InnerText.Equals("0"))
                        lstLanguage.Items.FindByValue(xmldoc.SelectSingleNode("//oldUser/languageID").InnerText).Selected = true;

                    Session.Remove("UsrEmailLangID"); //FB 2283
                    if (xmldoc.SelectSingleNode("//oldUser/EmailLang").InnerText.Trim() != "")
                    {
                        txtEmailLang.Text = xmldoc.SelectSingleNode("//oldUser/EmailLangName").InnerText;
                        Session["UsrEmailLangID"] = xmldoc.SelectSingleNode("//oldUser/EmailLang").InnerText;
                    }
                    //FB 1830 - End

                    //FB 2502 Starts
                    delEmailLang.Visible = false;
                    if (Session["UsrEmailLangID"] != null)
                    {
                        if (Session["UsrEmailLangID"].ToString() != "")
                        {
                            if(Session["UsrEmailLangID"].ToString() == "0")
                                delEmailLang.Visible = false;
                            else
                                delEmailLang.Visible = true;
                        }
                    }
                    //FB 2502 Ends

                    //Code added for AV Switch
                    DrpEnableAV.Enabled = true;
                    if (!txtLevel.Value.Equals("0"))
                    {
                        DrpEnableAV.Enabled = false;
                        DrpEnableAV.SelectedIndex = 0;
                    }
                    else if (xmldoc.SelectSingleNode("//oldUser/enableAV") != null)
                    {
                        if (xmldoc.SelectSingleNode("//oldUser/enableAV").InnerText != "")
                            DrpEnableAV.Items.FindByValue(xmldoc.SelectSingleNode("//oldUser/enableAV").InnerText).Selected = true;
                    }

                    DrpEnvPar.Enabled = true;
                    if (!txtLevel.Value.Equals("0"))
                    {
                        DrpEnvPar.Enabled = false;
                        DrpEnvPar.SelectedIndex = 0;
                    }
                    else if (xmldoc.SelectSingleNode("//oldUser/enableParticipants") != null)
                    {
                        if (xmldoc.SelectSingleNode("//oldUser/enableParticipants").InnerText != "")
                            DrpEnvPar.Items.FindByValue(xmldoc.SelectSingleNode("//oldUser/enableParticipants").InnerText).Selected = true;
                    }

                    txtAddress.Text = xmldoc.SelectSingleNode("//oldUser/IPISDNAddress").InnerText;
                    txtEndpointID.Text = xmldoc.SelectSingleNode("//oldUser/EndpointID").InnerText;
                    try
                    {
                        lstTimeZone.Items.FindByValue(xmldoc.SelectSingleNode("//oldUser/timeZone").InnerText).Selected = true;
                    }
                    catch (Exception ex)
                    {
                        log.Trace(ex.Message + " : " + ex.StackTrace);
                    }
                    try
                    {
                        //lstAddressBook.Items.FindByValue(xmldoc.SelectSingleNode("//oldUser/addressTypeID").InnerText).Selected = true;//FB 765
                        //Code Modified on 21Mar09 For FB 412 - Start
                        //lstAddressBook.Items.FindByValue(xmldoc.SelectSingleNode("//oldUser/addressTypeID").InnerText).Selected = true;//FB 765
                        lstAddressBook.Items.FindByValue(xmldoc.SelectSingleNode("//oldUser/emailClient").InnerText).Selected = true;//FB 765,1361
                        //Code Modified on 21Mar09 For FB 412 - End
                    }
                    catch (Exception ex)
                    {
                        log.Trace(ex.Message + " : " + ex.StackTrace);
                    }
                    try
                    {
                        lstUserRole.Items.FindByValue(xmldoc.SelectSingleNode("//oldUser/roleID").InnerText).Selected = true;
                    }
                    catch (Exception ex)
                    {
                        log.Trace(ex.Message + " : " + ex.StackTrace);
                    }
                    try
                    {
                        //lstEmailNotification.Items.FindByValue(xmldoc.SelectSingleNode("//oldUser/emailClient").InnerText).Selected = true;//FB 765
                        lstEmailNotification.Items.FindByValue(xmldoc.SelectSingleNode("//oldUser/emailMask").InnerText).Selected = true;//FB 1361
                    }
                    catch (Exception ex)
                    {
                        log.Trace(ex.Message + " : " + ex.StackTrace);
                    }
                    try
                    {
                        lstSendBoth.Items.FindByValue(xmldoc.SelectSingleNode("//oldUser/sendBoth").InnerText).Selected = true;
                    }
                    catch (Exception ex)
                    {
                        log.Trace(ex.Message + " : " + ex.StackTrace);
                    }
                    try
                    {
                        lstSearchTemplate.Items.FindByValue(xmldoc.SelectSingleNode("//oldUser/SavedSearch").InnerText).Selected = true;
                    }
                    catch (Exception ex)
                    {
                        log.Trace(ex.Message + " : " + ex.StackTrace);
                    }

                    try   //FB 1598
                    {
                        lstDefaultGroup.Items.FindByValue(xmldoc.SelectSingleNode("//oldUser/defaultAGroups/groupID").InnerText).Selected = true;
                    }
                    catch (Exception ex)
                    {
                        log.Trace(ex.Message + " : " + ex.StackTrace);
                    }

                    //Code Changed For Date Format  by Offshore - FB IssueNo 1073 -Start
                    try
                    {
                        DrpDateFormat.Items.FindByValue(xmldoc.SelectSingleNode("//oldUser/dateFormat").InnerText).Selected = true;
                    }
                    catch (Exception ex)
                    {
                        log.Trace(ex.Message + " : " + ex.StackTrace);
                    }
                    //Code Changed For Date Format  by Offshore - FB IssueNo 1073 - End

                    //Code Changed For Date Format  by Offshore - FB IssueNo 1426,1425
                    try
                    {
                        lstTimeFormat.Items.FindByValue(xmldoc.SelectSingleNode("//oldUser/timeFormat").InnerText).Selected = true;
                    }
                    catch (Exception ex)
                    {
                        log.Trace(ex.Message + " : " + ex.StackTrace);
                    }
                    //Code Added for License modification START
                    try
                    {
                        DrpDom.SelectedValue = xmldoc.SelectSingleNode("//oldUser/dominoUser").InnerText;
                    }
                    catch (Exception ex)
                    {
                        log.Trace(ex.Message + " : " + ex.StackTrace);
                    }
                    try
                    {
                        DrpExc.SelectedValue = xmldoc.SelectSingleNode("//oldUser/exchangeUser").InnerText;
                    }
                    catch (Exception ex)
                    {
                        log.Trace(ex.Message + " : " + ex.StackTrace);
                    }
                    //FB 1979 - Start
                    try
                    {
                        DrpMob.SelectedValue = xmldoc.SelectSingleNode("//oldUser/mobileUser").InnerText;
                    }
                    catch (Exception ex)
                    {
                        log.Trace(ex.Message + ":" + ex.StackTrace);
                    }
                    //FB 1979 - End
                    //FB 2141 - Start
                    try
                    {
                        DrpPIMNotifications.SelectedValue = xmldoc.SelectSingleNode("//oldUser/PIMNotification").InnerText;
                    }
                    catch (Exception ex)
                    {
                        log.Trace(ex.Message + ":" + ex.StackTrace);
                    }
                    //FB 2141 - End
                    //Code Added for License modification END
                    try
                    {
                        lstTimeZoneDisplay.Items.FindByValue(xmldoc.SelectSingleNode("//oldUser/timezoneDisplay").InnerText).Selected = true;
                    }
                    catch (Exception ex)
                    {
                        log.Trace(ex.Message + " : " + ex.StackTrace);
                    }

                    //code added for ticker -- Satart


                    String tickerstatus = "1";    //FB 1567
                    if (xmldoc.SelectSingleNode("//oldUser/tickerStatus") != null)
                    {
                        if (xmldoc.SelectSingleNode("//oldUser/tickerStatus").InnerText != "")
                        {
                            tickerstatus = xmldoc.SelectSingleNode("//oldUser/tickerStatus").InnerText;
                        }
                    }

                    drpTickerStatus.Items.FindByValue(tickerstatus).Selected = true;

                    String tickerspeed = "6";
                    if (xmldoc.SelectSingleNode("//oldUser/tickerSpeed") != null)
                    {
                        if (xmldoc.SelectSingleNode("//oldUser/tickerSpeed").InnerText != "")
                        {
                            tickerspeed = xmldoc.SelectSingleNode("//oldUser/tickerSpeed").InnerText;
                        }
                    }
                    drpTickerSpeed.Items.FindByValue(tickerspeed).Selected = true;

                    string tickerdisplay = "0";
                    if (xmldoc.SelectSingleNode("//oldUser/tickerDisplay") != null)
                    {
                        if (xmldoc.SelectSingleNode("//oldUser/tickerDisplay").InnerText != "")
                        {
                            tickerdisplay = xmldoc.SelectSingleNode("//oldUser/tickerDisplay").InnerText;
                        }
                    }
                    drpTickerDisplay.Items.FindByValue(tickerdisplay).Selected = true;

                    if (tickerdisplay == "0")
                    {
                        txtFeedLink.Attributes.Add("style", "display:none");
                        feedLink.Attributes.Add("style", "display:none");

                    }
                    else
                    {
                        txtFeedLink.Attributes.Add("style", "display:block");
                        feedLink.Attributes.Add("style", "display:block");
                    }

                    String tickerpos = "0";
                    if (xmldoc.SelectSingleNode("//oldUser/tickerPosition") != null)
                    {
                        if (xmldoc.SelectSingleNode("//oldUser/tickerPosition").InnerText != "")
                        {
                            tickerpos = xmldoc.SelectSingleNode("//oldUser/tickerPosition").InnerText;
                        }
                    }
                    drpTickerPosition.Items.FindByValue(tickerpos).Selected = true;

                    if (xmldoc.SelectSingleNode("//oldUser/tickerBackground") != null)
                    {
                        if (xmldoc.SelectSingleNode("//oldUser/tickerBackground").InnerText != "")
                        {
                            txtTickerBknd.Text = xmldoc.SelectSingleNode("//oldUser/tickerBackground").InnerText;
                        }
                        else    //FB 1567
                        {
                            txtTickerBknd.Text = "#660066";
                        }


                    }

                    if (xmldoc.SelectSingleNode("//oldUser/rssFeedLink") != null)
                    {
                        if (xmldoc.SelectSingleNode("//oldUser/rssFeedLink").InnerText != "")
                        {
                            txtFeedLink.Text = xmldoc.SelectSingleNode("//oldUser/rssFeedLink").InnerText;
                        }
                    }

                    String tickerstatus1 = "1";          //Fb 1567
                    if (xmldoc.SelectSingleNode("//oldUser/tickerStatus1") != null)
                    {
                        if (xmldoc.SelectSingleNode("//oldUser/tickerStatus1").InnerText != "")
                        {
                            tickerstatus1 = xmldoc.SelectSingleNode("//oldUser/tickerStatus1").InnerText;
                        }
                    }

                    drpTickerStatus1.Items.FindByValue(tickerstatus1).Selected = true;

                    String tickerspeed1 = "6";
                    if (xmldoc.SelectSingleNode("//oldUser/tickerSpeed1") != null)
                    {
                        if (xmldoc.SelectSingleNode("//oldUser/tickerSpeed1").InnerText != "")
                        {
                            tickerspeed1 = xmldoc.SelectSingleNode("//oldUser/tickerSpeed1").InnerText;
                        }
                    }
                    drpTickerSpeed1.Items.FindByValue(tickerspeed1).Selected = true;

                    string tickerdisplay1 = "0";
                    if (xmldoc.SelectSingleNode("//oldUser/tickerDisplay1") != null)
                    {
                        if (xmldoc.SelectSingleNode("//oldUser/tickerDisplay1").InnerText != "")
                        {
                            tickerdisplay1 = xmldoc.SelectSingleNode("//oldUser/tickerDisplay1").InnerText;
                        }
                    }
                    drpTickerDisplay1.Items.FindByValue(tickerdisplay1).Selected = true;
                    if (tickerdisplay1 == "0")
                    {
                        txtFeedLink1.Attributes.Add("style", "display:none");
                        feedLink1.Attributes.Add("style", "display:none");

                    }
                    else
                    {
                        txtFeedLink1.Attributes.Add("style", "display:block");
                        feedLink1.Attributes.Add("style", "display:block");
                    }
                    String tickerpos1 = "0";
                    if (xmldoc.SelectSingleNode("//oldUser/tickerPosition1") != null)
                    {
                        if (xmldoc.SelectSingleNode("//oldUser/tickerPosition1").InnerText != "")
                        {
                            tickerpos1 = xmldoc.SelectSingleNode("//oldUser/tickerPosition1").InnerText;
                        }
                    }
                    drpTickerPosition1.Items.FindByValue(tickerpos1).Selected = true;

                    if (xmldoc.SelectSingleNode("//oldUser/tickerBackground1") != null)
                    {
                        if (xmldoc.SelectSingleNode("//oldUser/tickerBackground1").InnerText != "")
                        {
                            txtTickerBknd1.Text = xmldoc.SelectSingleNode("//oldUser/tickerBackground1").InnerText;
                        }
                        else    //FB 1567
                        {
                            txtTickerBknd1.Text = "#99ff99";
                        }
                    }

                    if (xmldoc.SelectSingleNode("//oldUser/rssFeedLink1") != null)
                    {
                        if (xmldoc.SelectSingleNode("//oldUser/rssFeedLink1").InnerText != "")
                        {
                            txtFeedLink1.Text = xmldoc.SelectSingleNode("//oldUser/rssFeedLink1").InnerText;
                        }
                    }
                    //code added for ticker - End

                    nodes = xmldoc.SelectNodes("//oldUser/departments/selected"); //FB 1830
                    //Response.Write(Session["admin"].ToString());
                    foreach (ListItem li in lstDepartment.Items)
                    {
                        if (txtLevel.Value.Trim().Equals("2"))
                        {
                            li.Selected = true;
                            lstDepartment.Enabled = false;
                        }
                        foreach (XmlNode node in nodes)
                        {
                            if (li.Value.Trim().Equals(node.InnerText.Trim()))
                                li.Selected = true;
                        }
                    }
                    if (xmldoc.SelectNodes("//oldUser/locationList/selected/level1ID").Count > 0)// code added for room search
                    {
                        //txtLocation.Text = xmldoc.SelectSingleNode("//oldUser/locationList/selected/selectedName").InnerText;

                        if (xmldoc.SelectSingleNode("//oldUser/locationList/selected/level1ID").InnerText != "")
                        {
                            myVRMNet.NETFunctions ntfuncs = new myVRMNet.NETFunctions();
                            locstrname.Value = ntfuncs.RoomDetailsString(xmldoc.SelectSingleNode("//oldUser/locationList/selected/level1ID").InnerText);
                            selectedloc.Value = xmldoc.SelectSingleNode("//oldUser/locationList/selected/level1ID").InnerText;
                            BindRoomToList();
                            ntfuncs = null;
                        }

                        hdnLocation.Value = xmldoc.SelectSingleNode("//oldUser/locationList/selected/level1ID").InnerText;
                    }
                    if (lstAddressBook.SelectedValue.Equals("2"))
                    {
                        txtLotusLoginName.Text = xmldoc.SelectSingleNode("//oldUser/lotus/lnLoginName").InnerText;
                        txtLotusPassword.Attributes.Add("value", xmldoc.SelectSingleNode("//oldUser/lotus/lnLoginPwd").InnerText);
                        txtLotusDBPath.Text = xmldoc.SelectSingleNode("//oldUser/lotus/lnDBPath").InnerText;
                    }
                    //Response.Write(txtEndpointID.Text);
                    if (!txtEndpointID.Text.Equals("0") && !txtEndpointID.Text.Equals("-1"))
                        LoadEndpointDetails();

                    hdntzone.Value = "1";//Code added for  FB 1425 QA Bug 

                    //Commented for FB 2027
                    //FB 1860
                    //emailBlock = obj.CallCommand("GetUserEmailsBlockStatus", inXML);
                    //if (emailBlock.IndexOf("<error>") < 0)
                    //{
                    //    XmlDocument blck = new XmlDocument();
                    //    blck.LoadXml(emailBlock);

                    //    XmlNode node = blck.SelectSingleNode("//login/emailBlockStatus");

                    if (xmldoc.SelectSingleNode("//oldUser/login/emailBlockStatus") != null)
                        {
                            if (xmldoc.SelectSingleNode("//oldUser/login/emailBlockStatus").InnerText == "1")
                            {
                                ChkBlockEmails.Checked = true;
                                 if (isAVEnabled) //FB 1522
                                    BtnBlockEmails.Attributes.Add("style", "display:block");
                            }
                        }
                        
 
                    //}
                    //FB 1860
                    //FB 2268 start
                    if (xmldoc.SelectSingleNode("//oldUser/HelpRequsetorEmail") != null)
                    {
                        hdnHelpReq.Value = xmldoc.SelectSingleNode("//oldUser/HelpRequsetorEmail").InnerText.Trim();
                        String[] HelpReq = xmldoc.SelectSingleNode("//oldUser/HelpRequsetorEmail").InnerText.Trim().Split('�');
                        for (int i = 0; i < HelpReq.Length; i++)
                        {
                            if (HelpReq[i].Trim() != "")
                            {
                                ListItem item = new ListItem();
                                item.Text = HelpReq[i];
                                item.Attributes.Add("title", HelpReq[i]);
                                lstHelpReqEmail.Items.Add(item) ;
                            }
                        }
                    }
                    if (xmldoc.SelectSingleNode("//oldUser/HelpRequsetorPhone") != null)
                        txtHelpReqPhone.Text = xmldoc.SelectSingleNode("//oldUser/HelpRequsetorPhone").InnerText.Trim();
                    
                    //FB 2268 end
                    //FB 2348
                    if (xmldoc.SelectSingleNode("//oldUser/SendSurveyEmail") != null)
                        drpSurveyEmail.SelectedValue = xmldoc.SelectSingleNode("//oldUser/SendSurveyEmail").InnerText.Trim();

                    // FB 2608 Start

                    if (xmldoc.SelectSingleNode("//oldUser/EnableVNOCselection") != null && xmldoc.SelectSingleNode("//oldUser/EnableVNOCselection").InnerText == "1")
                        chkVNOC.Checked = true;
                    else
                        chkVNOC.Checked = false;

                    if (Session["Admin"] != null)
                    {
                        if (Session["Admin"].ToString() == "0")
                            chkVNOC.Enabled = false;
                        else
                            chkVNOC.Enabled = true;
                    }

                    // FB 2608 End
                    
                    //FB 2481
                    if (xmldoc.SelectSingleNode("//oldUser/PrivateVMR") != null)
                        PrivateVMR.Html = xmldoc.SelectSingleNode("//oldUser/PrivateVMR").InnerXml.Trim();
                 
					 //FB 2599 Starts
                    //FB 2262
                    if (xmldoc.SelectSingleNode("//oldUser/VidyoURL") != null)
                        txtVidyoURL.Text = txtPin.Text = xmldoc.SelectSingleNode("//oldUser/VidyoURL").InnerText.Trim();

                    if (xmldoc.SelectSingleNode("//oldUser/Pin") != null)
                        txtConfirmPin.Text = txtPin.Text = xmldoc.SelectSingleNode("//oldUser/Pin").InnerText.Trim();

                    if (xmldoc.SelectSingleNode("//oldUser/Extension") != null)
                        txtExtension.Text = xmldoc.SelectSingleNode("//oldUser/Extension").InnerText.Trim();
                    //FB 2599 End
                    //FB 2594 Starts
                    //whygouserSettings.Attributes.Add("Style", "display:none");
                    whygouserSettings.Visible = false;
                    if (Session["EnablePublicRooms"] != null)
                    {
                        if (Session["EnablePublicRooms"].ToString() == "1")
                        {
                            //whygouserSettings.Attributes.Add("Style", "display:block");
                            whygouserSettings.Visible = true;
                            //FB 2392-Whygo Starts
                            if (xmldoc.SelectSingleNode("//oldUser/ParentReseller") != null)
                                txtParentReseller.Value = xmldoc.SelectSingleNode("//oldUser/ParentReseller").InnerText;
                            if (xmldoc.SelectSingleNode("//oldUser/CorpUser") != null)
                                txtCorpUser.Value = xmldoc.SelectSingleNode("//oldUser/CorpUser").InnerText;
                            if (xmldoc.SelectSingleNode("//oldUser/Region") != null)
                            {
                                if (lstRegion.Items.FindByValue(xmldoc.SelectSingleNode("//oldUser/Region").InnerText.Trim()) != null)
                                    lstRegion.SelectedValue = xmldoc.SelectSingleNode("//oldUser/Region").InnerText.Trim();
                            }
                            if (xmldoc.SelectSingleNode("//oldUser/ESUserID") != null)
                                txtESUserID.Value = xmldoc.SelectSingleNode("//oldUser/ESUserID").InnerText;
                            //FB 2392-Whygo End
                        }
                    }
                    //FB 2594 Ends
                    //FB 2595 Start
                    if (xmldoc.SelectSingleNode("//oldUser/Secured") != null)
                    {
                        if (xmldoc.SelectSingleNode("//oldUser/Secured").InnerText.Trim() == "1")
                            chkSecured.Checked = true;
                        else
                            chkSecured.Checked = false;
                    }
                    //FB 2595 End
                }
                else
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
                if (txtUserID.Text.Equals(Session["userID"].ToString()))
                    SetUserProfile();
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                errLabel.Text = "GetUserProfile: " + ex.Message + " : " + ex.StackTrace;
                log.Trace(ex.StackTrace);
            }
        }

        protected void SetUserProfile()
        {
            try
            {
                txtTimeRemaining.Enabled = false;
                //FB 1318 - Start
                if (txtLevel.Value == "2" && Session["UsrCrossAccess"].ToString() == "1")
                    txtAccountExpiry.Enabled = true;
                else
                    txtAccountExpiry.Enabled = false;
                //FB 1318 - End
                lstDepartment.Enabled = false;
                lstUserRole.Enabled = false;
                DrpEnableAV.SelectedIndex = 0;//For AV Switch
                DrpEnableAV.Enabled = false;//For AV Switch
                DrpEnvPar.SelectedIndex = 0;//For AV Switch
                DrpEnvPar.Enabled = false;//For AV Switch
                lblHeader.Text = obj.GetTranslatedText("My Preferences");//FB 1830 - Translation

                if (Session["organizationID"] != null && Session["UsrCrossAccess"] != null) //FB 1598
                {
                    if (Session["UsrCrossAccess"].ToString().Equals("1") && !Session["organizationID"].ToString().Equals("11"))
                    {
                        lstDefaultGroup.Enabled = false;
                        lstSearchTemplate.Enabled = false;
                        lstBridges.Enabled = false;
                        //roomimage.Attributes.Add("style", "display:none;");
                        roomimage.Visible = false; //FB 1598

                    }
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message + " : " + ex.StackTrace);
            }
        }

        protected void LoadEndpointDetails()
        {
            try
            {
                //Response.Write("EndpointDetaails");
                String inXML = "<EndpointDetails>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "  <UserID>" + txtUserID.Text + "</UserID>";
                inXML += "  <EndpointID>" + txtEndpointID.Text + "</EndpointID>";
                inXML += "  <EntityType>U</EntityType>";
                inXML += "</EndpointDetails>";
                String outXML = obj.CallMyVRMServer("GetEndpointDetails", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                //Response.Write(obj.Transfer(outXML));
                if (outXML.IndexOf("<error>") < 0)
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);

                    txtEndpointName.Text = xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/ProfileName").InnerText;
                    txtEPPassword1.Attributes.Add("value", xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/Password").InnerText);
                    txtEPPassword2.Attributes.Add("value", xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/Password").InnerText);
                    txtAddress.Text = xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/Address").InnerText;
                    txtWebAccessURL.Text = xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/URL").InnerText;
                    txtAssociateMCUAddress.Text = xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/MCUAddress").InnerText;
                    txtExchangeID.Text = xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/ExchangeID").InnerText; //Cisco Telepresence fix
                    txtApiportno.Text = xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/ApiPortno").InnerText;//API Port...
                    // Code changed for FB 1713 - Start
                    txtConfCode.Text = xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/conferenceCode").InnerText;
                    txtLeaderPin.Text = xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/leaderPin").InnerText;
                    int lineRateVal = 0;
                    String txtTemp = xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/LineRate").InnerText;
                    int.TryParse(txtTemp, out lineRateVal);

                    if (lineRateVal < 1)
                        txtTemp = "384";

                    // Code changed for FB 1713 - End
                    try
                    {
                        lstLineRate.Items.FindByValue(txtTemp).Selected = true;
                    }
                    catch (Exception ex)
                    {
                        log.Trace(ex.Message + " : " + ex.StackTrace);
                    }
                    /* Protocol is missing */
                    try
                    {
                        lstProtocol.Items.FindByValue(xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/DefaultProtocol").InnerText).Selected = true;
                    }
                    catch (Exception ex)
                    {
                        log.Trace(ex.Message + " : " + ex.StackTrace);
                    }
                    // Connection Type
                    try
                    {
                        lstConnectionType.Items.FindByValue(xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/ConnectionType").InnerText).Selected = true;
                    }
                    catch (Exception ex)
                    {
                        log.Trace(ex.Message + " : " + ex.StackTrace);
                    }
                    // Address Type
                    try
                    {
                        lstAddressType.Items.FindByValue(xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/AddressType").InnerText).Selected = true;
                    }
                    catch (Exception ex)
                    {
                        log.Trace(ex.Message + " : " + ex.StackTrace);
                    }
                    // Video Equipment
                    try
                    {
                        lstVideoEquipment.Items.FindByValue(xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/VideoEquipment").InnerText).Selected = true;
                    }
                    catch (Exception ex)
                    {
                        log.Trace(ex.Message + " : " + ex.StackTrace);
                    }
                    // Bridge ID
                    try
                    {
                        lstBridges.Items.FindByValue(xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/Bridge").InnerText).Selected = true;
                    }
                    catch (Exception ex)
                    {
                        log.Trace(ex.Message + " : " + ex.StackTrace);
                    }
                    // Is outside Network
                    try
                    {
                        lstIsOutsideNetwork.Items.FindByValue(xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/IsOutside").InnerText).Selected = true;
                    }
                    catch (Exception ex)
                    {
                        log.Trace(ex.Message + " : " + ex.StackTrace);
                    }
                    try
                    {
                        lstMCUAddressType.Items.FindByValue(xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/MCUAddressType").InnerText).Selected = true;
                    }
                    catch (Exception ex)
                    {
                        log.Trace(ex.Message + " : " + ex.StackTrace);
                    }
                    try
                    {
                        lstEncryption.Items.FindByValue(xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/EncryptionPreferred").InnerText).Selected = true;
                    }
                    catch (Exception ex)
                    {
                        log.Trace(ex.Message + " : " + ex.StackTrace);
                    }
                }
                else
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
            }
            catch (Exception ex)
            {
                errLabel.Text = ex.StackTrace;
                errLabel.Visible = true;
            }
        }
        #endregion

        #region SubmitUser
        // FB 2025 Start
        protected void SubmitUser(Object sender, EventArgs e)
        {
            int ChangeVal = 1;
            Submit(ChangeVal);
        }
        protected void SubmitNewUser(object Sender, EventArgs e)
        {
            int ChangeVal = 2;
            Submit(ChangeVal);
        }
        // FB 2025 Ends
        protected void Submit(int ChangeVal)
        {
           // int ChangeVal = ChangeVal;
            try
            {
                if (Page.IsValid)
                {
                    //txtUserLogin.Text = txtUserFirstName.Text; //Org fixes
                    //FB 2027 Start
                    //String inXML = "";
                    StringBuilder inXML = new StringBuilder();
                    inXML.Append("<saveUser>");
                    inXML.Append(obj.OrgXMLElement());//Organization Module Fixes
                    inXML.Append("<login>");
                    inXML.Append("<userID>" + Session["userID"].ToString() + "</userID>");
                    inXML.Append("</login>");
                    inXML.Append("<user>");
                    inXML.Append("<userID>" + txtUserID.Text + "</userID>");
                    inXML.Append("<userName>");
                    inXML.Append("<firstName>" + txtUserFirstName.Text + "</firstName>");
                    inXML.Append("<lastName>" + txtUserLastName.Text + "</lastName>");
                    inXML.Append("</userName>");
                    inXML.Append("<ExchangeID>" + txtEndpointID.Text + "</ExchangeID>"); //Cisco Telepresence fix
                    inXML.Append("<login>" + txtUserLogin.Text.Trim() + "</login>");//FB 1921
                    inXML.Append("<password>" + txtPassword1.Text + "</password>");
                    inXML.Append("<userEmail>" + txtUserEmail.Text + "</userEmail>");
                    inXML.Append("<alternativeEmail>" + txtUserEmail2.Text + "</alternativeEmail>");
                    inXML.Append("<workPhone>" + txtWorkPhone.Text.Trim() + "</workPhone>");//FB 1642-Audio Add on
                    inXML.Append("<cellPhone>" + txtCellPhone.Text.Trim() + "</cellPhone>");//FB 1642-Audio Add on
                    inXML.Append("<sendBoth>" + lstSendBoth.SelectedValue + "</sendBoth>");
                    inXML.Append("<emailClient>" + lstAddressBook.SelectedValue + "</emailClient>");
                    inXML.Append("<SavedSearch>" + lstSearchTemplate.SelectedValue + "</SavedSearch>");
                    if (txtUserID.Text.Equals(Session["userID"].ToString()))
                        Session["SearchID"] = lstSearchTemplate.SelectedValue;
                    inXML.Append("<userInterface>2</userInterface>");
                    inXML.Append("<timeZone>" + lstTimeZone.SelectedValue + "</timeZone>");
                    //inXML += "    <location>" + hdnLocation.Value + "</location>";//Code  added for room search
                    inXML.Append("<location>" + selectedloc.Value + "</location>");//Code  added for room search
                    //Code Added For Date Format by Offshore -  FB IssueNo 1073- Start
                    inXML.Append("<dateFormat>" + DrpDateFormat.SelectedValue + "</dateFormat>");
                    //Code Added For Date Format  by Offshore -  FB IssueNo 1073 -End
                    //Code added for AV Switch FB 1426,1425,1429 -- Start
                    if (isAVEnabled)
                        inXML.Append("<enableAV>" + DrpEnableAV.SelectedValue + "</enableAV>");
                    else
                        inXML.Append("<enableAV>" + enableAV + "</enableAV>");

                    if (isParEnabled)
                        inXML.Append("<enableParticipants>" + DrpEnvPar.SelectedValue + "</enableParticipants>");
                    else
                        inXML.Append("<enableParticipants>" + enablePar + "</enableParticipants>");

                    inXML.Append("<timeFormat>" + lstTimeFormat.SelectedValue + "</timeFormat>");
                    inXML.Append("<timezoneDisplay>" + lstTimeZoneDisplay.SelectedValue + "</timezoneDisplay>");

                    //Code added for AV Switch -- End
                    //code added for ticker -Start
                    inXML.Append("<tickerStatus>" + drpTickerStatus.SelectedValue + "</tickerStatus>");
                    inXML.Append("<tickerPosition>" + drpTickerPosition.SelectedValue + "</tickerPosition>");
                    inXML.Append("<tickerSpeed>" + drpTickerSpeed.SelectedValue + "</tickerSpeed>");
                    inXML.Append("<tickerBackground>" + txtTickerBknd.Text.Trim() + "</tickerBackground>");
                    if (drpTickerStatus.SelectedValue == "0")
                    {
                        if (txtTickerBknd.Text.Trim() == "")
                        {
                            throw new Exception("Please select the ticker background color.");

                        }
                    }
                    inXML.Append("<tickerDisplay>" + drpTickerDisplay.SelectedValue + "</tickerDisplay>");

                    inXML.Append("<rssFeedLink>" + txtFeedLink.Text + "</rssFeedLink>");
                    if (drpTickerDisplay.SelectedValue == "1")
                    {
                        if (txtFeedLink.Text == "")
                        {
                            throw new Exception("Please enter the RSS Feed link.");
                        }
                    }
                    inXML.Append("<tickerStatus1>" + drpTickerStatus1.SelectedValue + "</tickerStatus1>");
                    inXML.Append("<tickerPosition1>" + drpTickerPosition1.SelectedValue + "</tickerPosition1>");
                    inXML.Append("<tickerSpeed1>" + drpTickerSpeed1.SelectedValue + "</tickerSpeed1>");
                    inXML.Append("<tickerBackground1>" + txtTickerBknd1.Text.Trim() + "</tickerBackground1>");
                    if (drpTickerStatus1.SelectedValue == "0")
                    {
                        if (txtTickerBknd1.Text.Trim() == "")
                        {
                            throw new Exception("Please select the ticker background color.");

                        }
                    }
                    inXML.Append("<tickerDisplay1>" + drpTickerDisplay1.SelectedValue + "</tickerDisplay1>");

                    inXML.Append("<rssFeedLink1>" + txtFeedLink1.Text + "</rssFeedLink1>");

                    if (drpTickerDisplay1.SelectedValue == "1")
                    {
                        if (txtFeedLink1.Text == "")
                        {
                            throw new Exception("Please enter the RSS Feed link.");
                        }
                    }

                    if (drpTickerStatus.SelectedValue == "0" || drpTickerStatus1.SelectedValue == "0")
                    {

                        if (txtTickerBknd.Text != "" && txtTickerBknd1.Text != "")
                            if (txtTickerBknd.Text == txtTickerBknd1.Text)
                            {
                                throw new Exception("Both Tickers cannot have the same background color.");
                            }
                    }

                    //code added for Ticker --End
                    string confcode = txtConfCode.Text.Trim(); //FB 1642-Audio add on start
                    string leaderPin = txtLeaderPin.Text.Trim();

                    confcode = confcode.Replace("D", "");
                    confcode = confcode.Replace("+", "");

                    leaderPin = leaderPin.Replace("D", "");
                    leaderPin = leaderPin.Replace("+", ""); //FB 1642-Audio add on end

                    inXML.Append("<lineRateID>" + lstLineRate.SelectedValue + "</lineRateID>");
                    inXML.Append("<videoProtocol>" + lstProtocol.SelectedValue + "</videoProtocol>");
                    inXML.Append("<conferenceCode>" + confcode + "</conferenceCode>");//FB 1642-Audio add on
                    inXML.Append("<leaderPin>" + leaderPin + "</leaderPin>");//FB 1642-Audio add on
                    inXML.Append("<IPISDNAddress>" + txtAddress.Text + "</IPISDNAddress>");
                    inXML.Append("<connectionType>" + lstConnectionType.SelectedValue + "</connectionType>");
                    inXML.Append("<videoEquipmentID>" + lstVideoEquipment.SelectedValue + "</videoEquipmentID>");
                    inXML.Append("<isOutside>" + lstIsOutsideNetwork.SelectedValue + "</isOutside>");
                    inXML.Append("<addressTypeID>" + lstAddressType.SelectedValue + "</addressTypeID>");
                    inXML.Append("<group>" + lstDefaultGroup.SelectedValue + "</group>");
                    inXML.Append("<ccGroup></ccGroup>");
                    inXML.Append("<roleID>" + lstUserRole.SelectedValue + "</roleID>");
                    inXML.Append("<initialTime>" + txtTimeRemaining.Text + "</initialTime>");
                    //Code changed by Offshore for FB Issue 1073 -- Start
                    //inXML += "    <expiryDate>" + txtAccountExpiry.Text + "</expiryDate>";
                    inXML.Append("<expiryDate>" + myVRMNet.NETFunctions.GetDefaultDate(txtAccountExpiry.Text) + "</expiryDate>");
                    //Code changed by Offshore for FB Issue 1073 -- End
                    inXML.Append("<bridgeID>" + lstBridges.SelectedValue + "</bridgeID>");
                    //FB 1830 - Start
                    inXML.Append("<languageID>" + lstLanguage.SelectedValue + " </languageID>");
                    if (Session["UsrEmailLangID"] != null)//FB 2283
                        inXML.Append("<EmailLang>" + Session["UsrEmailLangID"].ToString() + " </EmailLang>");
                    else
                        inXML.Append("<EmailLang></EmailLang>");
                    //FB 1830 - ENd
                    inXML.Append("<departments>");
                    //Response.Write(lstAdmin.Items[lstUserRole.SelectedIndex].Text);
                    if (lstAdmin.Items[lstUserRole.SelectedIndex].Text.Equals("2"))
                        foreach (ListItem li in lstDepartment.Items)
                            inXML.Append("<id>" + li.Value + "</id>");
                    else
                        foreach (ListItem li in lstDepartment.Items)
                        {
                            if (li.Selected.Equals(true))
                                inXML.Append("<id>" + li.Value + "</id>");
                        }
                    inXML.Append("</departments>");
                    inXML.Append("<status>");
                    inXML.Append("<deleted>0</deleted>");
                    inXML.Append("<locked>0</locked>");
                    inXML.Append("</status>");
                    if (lstAddressBook.SelectedValue.Equals("2"))
                    {
                        inXML.Append("<lotus>");
                        inXML.Append("<lnLoginName>" + txtLotusLoginName.Text + "</lnLoginName>");
                        inXML.Append("<lnLoginPwd>" + txtLotusPassword.Text + "</lnLoginPwd>");
                        inXML.Append("<lnDBPath>" + txtLotusDBPath.Text + "</lnDBPath>");
                        inXML.Append("</lotus>");
                    }
                    inXML.Append("<creditCard>0</creditCard>");
                    //FB 2227 Start
                    inXML.Append("<IntVideoNum>" + txtIntvideonum.Text + "</IntVideoNum>");
                    inXML.Append("<ExtVideoNum>" + txtExtvideonum.Text + "</ExtVideoNum>");
                    //FB 2227 End
                    inXML.Append("</user>");
                    inXML.Append("<emailMask>" + lstEmailNotification.SelectedValue + "</emailMask>");
                    //Code Added for License modification START
                    inXML.Append("<exchangeUser>" + DrpExc.SelectedValue + "</exchangeUser>");
                    inXML.Append("<dominoUser>" + DrpDom.SelectedValue + "</dominoUser>");
                    inXML.Append("<mobileUser>" + DrpMob.SelectedValue + "</mobileUser>"); //FB 1979
                    //Code Added for License modification END
                    inXML.Append("<PIMNotification>" + DrpPIMNotifications.SelectedValue + "</PIMNotification>");//FB 2164
                    //Audio Add on..
                    //if (chkAudioAddon.Checked) //FB 2023
                    //    inXML.Append("<audioaddon>1</audioaddon>");
                    //else
                    inXML.Append("<audioaddon>0</audioaddon>");
                    
                    inXML.Append("<HelpRequsetorEmail>" + hdnHelpReq.Value + "</HelpRequsetorEmail>"); //FB 2268
                    inXML.Append("<HelpRequsetorPhone>" + txtHelpReqPhone.Text + "</HelpRequsetorPhone>");//FB 2268
                    inXML.Append("<SendSurveyEmail>" + drpSurveyEmail.SelectedValue + "</SendSurveyEmail>");//FB 2348

                    //FB 2608 Start
                    int EnableVNOCselectionVal = 0;
                    if (Request.QueryString["t"] != null && Request.QueryString["t"].ToString() == "1")
                    {
                        if (lstUserRole.SelectedIndex == 2 || lstUserRole.SelectedIndex == 3)                            
                            EnableVNOCselectionVal = 1;
                        else
                        {
                            if (chkVNOC.Checked == true)
                                EnableVNOCselectionVal = 1;
                            else
                                EnableVNOCselectionVal = 0;
                        }
                    }
                    else
                    {
                        if (chkVNOC.Checked)
                            EnableVNOCselectionVal = 1;
                        else
                            EnableVNOCselectionVal = 0;
                    }
                    inXML.Append("<EnableVNOCselection>" + EnableVNOCselectionVal + "</EnableVNOCselection>");
                    //FB 2608 End
                    //FB 2595 Start
					if(chkSecured.Checked)
                        inXML.Append("<Secured>1</Secured>");
                    else
                        inXML.Append("<Secured>0</Secured>");
                    //FB 2595 End
                    inXML.Append("<PrivateVMR>" + PrivateVMR.Html + "</PrivateVMR>");//FB 2481
                    //FB 2599 Starts
                    //FB 2262
                    inXML.Append("<VidyoURL>" + txtVidyoURL.Text + "</VidyoURL>");
                    inXML.Append("<Pin>" + txtPin.Text + "</Pin>");
                    inXML.Append("<Extension>" + txtExtension.Text + "</Extension>");
                    //FB 2599 End

                    //FB 2594 Starts
                    if (Session["EnablePublicRooms"].ToString() == "1")
                    {
                        //FB 2392-Whygo Starts
                        inXML.Append("<ParentReseller>" + txtParentReseller.Value + "</ParentReseller>");
                        inXML.Append("<CorpUser>" + txtCorpUser.Value + "</CorpUser>");
                        inXML.Append("<Region>" + lstRegion.SelectedValue.Trim() + "</Region>");
                        inXML.Append("<ESUserID>" + txtESUserID.Value.Trim() + "</ESUserID>");
                        //FB 2392-Whygo End
                    }
                    else
                    {
                        inXML.Append("<ParentReseller></ParentReseller>");
                        inXML.Append("<CorpUser></CorpUser>");
                        inXML.Append("<Region></Region>");
                        inXML.Append("<ESUserID></ESUserID>");
                    }
                    //FB 2594 Ends
                    inXML.Append("</saveUser>");
                    log.Trace(inXML.ToString());
                    inXML.Replace("&", "&amp;"); //FB 2262T
                    String outXML = obj.CallMyVRMServer("SetUser", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
                    //String outXML = obj.CallCOM("SetUser", inXML, Application["COM_ConfigPath"].ToString());
                    //FB 2027 End
                    //Response.Write(obj.Transfer(outXML));
                    //Response.End();

                    Session.Remove("UserdeptXML");//Coded added for FB 1597
                    BindRoomToList();//Room Search

                    if (outXML.IndexOf("<error>") >= 0)
                    {
                        errLabel.Text = obj.ShowErrorMessage(outXML);
                        errLabel.Visible = true;

                        //FB 2268 start
                        lstHelpReqEmail.Items.Clear();
                        String[] HelpReq = hdnHelpReq.Value.Split('�');
                        for (int i = 0; i < HelpReq.Length; i++)
                        {
                            if (HelpReq[i].Trim() != "")
                            {
                                ListItem item = new ListItem();
                                item.Text = HelpReq[i];
                                item.Attributes.Add("title", HelpReq[i]);
                                lstHelpReqEmail.Items.Add(item);
                            }
                        }
                        //FB 2268 end
                    }
                    else
                    {
                        if (SetEndpointDetails(outXML))
                            if (Session["userID"].ToString().Trim().Equals(txtUserID.Text.Trim()))
                            {
                                //Code added by offshore for FB Issue 1073 -- start
                                string dt = "0";
                                if (Session["FormatDateType"].ToString() != DrpDateFormat.SelectedValue)
                                    dt = "1";
                                Session.Remove("FormatDateType");
                                Session.Add("FormatDateType", DrpDateFormat.SelectedValue);
                                dt = "0";
                                if (Session["timeFormat"].ToString() != lstTimeFormat.SelectedValue)
                                    dt = "1";
                                Session.Remove("timeFormat");
                                Session.Add("timeFormat", lstTimeFormat.SelectedValue);
                                //Code Modified For FB 1425
                                dt = "0";
                                if (Session["timeZoneDisplay"].ToString() != lstTimeZoneDisplay.SelectedValue)
                                    dt = "1";
                                Session.Remove("timeZoneDisplay");
                                Session.Add("timeZoneDisplay", lstTimeZoneDisplay.SelectedValue);
                                //code for ticker --Start
                                Session.Remove("tickerStatus");
                                Session.Add("tickerStatus", drpTickerStatus.SelectedValue);
                                Session.Remove("tickerSpeed");
                                Session.Add("tickerSpeed", drpTickerSpeed.SelectedValue);
                                Session.Remove("tickerPosition");
                                Session.Add("tickerPosition", drpTickerPosition.SelectedValue);
                                if (Session["tickerDisplay"].ToString() != drpTickerDisplay.SelectedValue)
                                    dt = "1";
                                Session.Remove("tickerDisplay");
                                Session.Add("tickerDisplay", drpTickerDisplay.SelectedValue);
                                Session.Remove("tickerBackground");
                                Session.Add("tickerBackground", txtTickerBknd.Text.Trim());
                                Session.Remove("rssFeedLink");
                                Session.Add("rssFeedLink", txtFeedLink.Text);
                                Session.Remove("tickerStatus1");
                                Session.Add("tickerStatus1", drpTickerStatus1.SelectedValue);
                                Session.Remove("tickerSpeed1");
                                Session.Add("tickerSpeed1", drpTickerSpeed1.SelectedValue);
                                Session.Remove("tickerPosition1");
                                Session.Add("tickerPosition1", drpTickerPosition1.SelectedValue);
                                if (Session["tickerDisplay1"].ToString() != drpTickerDisplay1.SelectedValue)
                                    dt = "1";
                                Session.Remove("tickerDisplay1");
                                Session.Add("tickerDisplay1", drpTickerDisplay1.SelectedValue);
                                Session.Remove("tickerBackground11");
                                Session.Add("tickerBackground1", txtTickerBknd1.Text.Trim());
                                Session.Remove("rssFeedLink1");
                                Session.Add("rssFeedLink1", txtFeedLink1.Text);
                                Session.Remove("timezoneID"); //FB 2468
                                Session.Add("timezoneID", lstTimeZone.SelectedValue);

                                // FB 2608 Start
                                Session.Remove("EnableVNOCselection");
                                if (chkVNOC.Checked)
                                    Session.Add("EnableVNOCselection", 1);
                                else
                                    Session.Add("EnableVNOCselection", 0);
                                // FB 2608 End

                               


                                //FB 1830 Start
                                switch (Session["language"].ToString())
                                {
                                    case "en":
                                        if (lstLanguage.SelectedValue != "1" && lstLanguage.SelectedValue != "2")
                                            dt = "1";
                                        break;
                                    case "sp":
                                        if (lstLanguage.SelectedValue != "3")
                                            dt = "1";
                                        break;
                                    case "ch":
                                        if (lstLanguage.SelectedValue != "4")
                                            dt = "1";
                                        break;
                                    case "fr":
                                        if (lstLanguage.SelectedValue != "5")
                                            dt = "1";
                                        break;
                                   
                                }
                                Session.Remove("CurrencyFormat"); 
                                Session.Remove("NumberFormat"); 
                                if (Session["FormatDateType"].ToString() == "dd/MM/yyyy")
                                {
                                    Session.Add("CurrencyFormat", ns_MyVRMNet.vrmCurrencyFormat.bound);
                                    Session.Add("NumberFormat", ns_MyVRMNet.vrmNumberFormat.european);
                                }
                                else
                                {
                                    Session.Add("CurrencyFormat", ns_MyVRMNet.vrmCurrencyFormat.dollar);
                                    Session.Add("NumberFormat", ns_MyVRMNet.vrmNumberFormat.american);
                                   
                                }
                                //FB 1830 End
                                //code for ticker --end

                                //FB 2025 starts
                                if (ChangeVal == 1) 
                                    Response.Redirect("ManageUserProfile.aspx?m=1&dt=" + dt);
                                else 
                                    Response.Redirect("ManageUserProfile.aspx?m=1&dt=");
                                //FB 2025 Ends
                            }
                            else
                            {
                                string dt = "0";
                                if (Session["FormatDateType"].ToString() != DrpDateFormat.SelectedValue)
                                    dt = "1";
                                //Session.Remove("FormatDateType"); //Commented for FB 1762
                                //Session.Add("FormatDateType", DrpDateFormat.SelectedValue); //Commented for FB 1762
                                dt = "0";
                                if (Session["timeFormat"].ToString() != lstTimeFormat.SelectedValue)
                                    dt = "1";
                                //Session.Remove("timeFormat"); //Commented for FB 1762
                                //Session.Add("timeFormat", lstTimeFormat.SelectedValue); //Commented for FB 1762
                                //Code Modified For FB 1425
                                dt = "0";
                                if (Session["timeZoneDisplay"].ToString() != lstTimeZoneDisplay.SelectedValue)
                                    dt = "1";
                                //Session.Remove("timeZoneDisplay"); //Commented for FB 1762
                                //Session.Add("timeZoneDisplay", lstTimeZoneDisplay.SelectedValue); //Commented for FB 1762

                                Session["UserToEdit"] = "";
                                //Code changed by offshore for FB Issue 1073 -- start
                                //Response.Redirect("ManageUser.aspx?t=1&m=1");
                                //FB 2025 starts
                                if (ChangeVal == 1) 
                                    Response.Redirect("ManageUser.aspx?t=1&m=1&dt=" + dt);
                                else //FB 2025
                                {
                                    Session.Add("UserToEdit", "new"); //FB 2025
                                    Response.Redirect("ManageUserProfile.aspx?t=1"); 
                                }
                                //FB 2025 ends
                                //Code changed by offshore for FB Issue 1073 -- end
                               
                            }
                    }
                }

            }
            catch (System.Threading.ThreadAbortException) { } // FB 1762
            catch (Exception ex)
            {
                errLabel.Visible = true;
                errLabel.Text = ex.Message;
                log.Trace(ex.StackTrace);
            }
        }
        #endregion

        #region UpdateDepartments
        protected void UpdateDepartments(Object sender, EventArgs e)
        {
            try
            {
                if (lstAdmin.Items[lstUserRole.SelectedIndex].Text.Equals("2"))
                {
                    foreach (ListItem li in lstDepartment.Items)
                        li.Selected = true;
                    lstDepartment.Enabled = false;
                }
                else
                    lstDepartment.Enabled = true;

                //Code added for AV switch
                DrpEnableAV.Enabled = true;
                DrpEnvPar.Enabled = true;
                if (!lstAdmin.Items[lstUserRole.SelectedIndex].Text.Equals("0"))
                {
                    DrpEnableAV.Enabled = false;
                    DrpEnableAV.SelectedIndex = 0;

                    DrpEnvPar.Enabled = false;
                    DrpEnvPar.SelectedIndex = 0;
                }



            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        #region SetEndpointDetails
        protected bool SetEndpointDetails(String outXML)
        {
            try
            {
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);
                txtUserID.Text = xmldoc.SelectSingleNode("//SetUser/userID").InnerText;
                String inXML = GenerateInxml();
                //Response.Write(obj.Transfer(inXML));
                //Response.End();
                outXML = obj.CallMyVRMServer("SetEndpoint", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                //Response.Write(obj.Transfer(outXML));
                //Response.End();
                if (outXML.IndexOf("<error>") < 0)
                {

                    //FB 1860
                    inXML = "  <login>";
                    inXML += "    <userID>" + txtUserID.Text + "</userID>";
                    if (ChkBlockEmails.Checked)
                        inXML += "<MailBlocked>1</MailBlocked>";
                    else
                        inXML += "<MailBlocked>0</MailBlocked>";
                    inXML += "  </login>";

                    outXML = obj.CallCommand("SetUserEmailsBlockStatus", inXML);

                    if (outXML.IndexOf("<error>") < 0)
                        return true;

                    //FB 1860

                   


                }
                else
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                    return false;
                }

                return false;
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message + " : " + ex.StackTrace);
                return false;
            }
        }
        #endregion

        #region GenerateInxml
        protected String GenerateInxml()
        {
            try
            {
                // Code changed for FB 1713 - Start
                int lineRate = 0;
                int.TryParse(lstLineRate.SelectedValue, out lineRate);
                string confcode = txtConfCode.Text.Trim(); //FB 1642-Audio add on start
                string leaderPin = txtLeaderPin.Text.Trim();

                confcode = confcode.Replace("D", "");
                confcode = confcode.Replace("+", "");

                leaderPin = leaderPin.Replace("D", "");
                leaderPin = leaderPin.Replace("+", ""); //FB 1642-Audio add on end

                if (lineRate < 1)
                    lineRate = 384;
                // Code changed for FB 1713 - End

                if (txtEndpointID.Text.Trim().Equals(""))
                    txtEndpointID.Text = "new";

                String inXML = "<SetEndpoint>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "      <EndpointID>" + txtEndpointID.Text + "</EndpointID>";
                inXML += "      <EndpointName>" + txtUserFirstName.Text + " " + txtUserLastName.Text + "</EndpointName>";
                inXML += "      <EntityType>U</EntityType>";
                inXML += "      <UserID>" + txtUserID.Text + "</UserID>";
                inXML += "      <Profiles>";
                inXML += "        <Profile>";
                inXML += "          <ProfileID>0</ProfileID>";
                if (txtEndpointName.Text.Trim().Equals(""))
                    txtEndpointName.Text = txtUserFirstName.Text + " " + txtUserLastName.Text;
                inXML += "          <ProfileName>" + txtEndpointName.Text + "</ProfileName>";
                inXML += "          <Default>0</Default>";
                inXML += "          <EncryptionPreferred>" + lstEncryption.SelectedValue + "</EncryptionPreferred>";
                inXML += "          <AddressType>" + lstAddressType.SelectedValue + "</AddressType>";
                inXML += "          <Password>" + txtEPPassword1.Text + "</Password>";
                inXML += "          <Address>" + txtAddress.Text + "</Address>";
                inXML += "          <URL>" + txtWebAccessURL.Text + "</URL>";
                inXML += "          <IsOutside>" + lstIsOutsideNetwork.SelectedValue + "</IsOutside>";
                inXML += "          <ConnectionType>" + lstConnectionType.SelectedValue + "</ConnectionType>";
                inXML += "          <VideoEquipment>" + lstVideoEquipment.SelectedValue + "</VideoEquipment>";
                inXML += "          <LineRate>" + lineRate + "</LineRate>";
                inXML += "          <Bridge>" + lstBridges.SelectedValue + "</Bridge>";
                inXML += "          <MCUAddress>" + txtAssociateMCUAddress.Text + "</MCUAddress>";
                //API Port Starts...
                if (txtApiportno.Text != "")
                    inXML += "          <ApiPortno>" + txtApiportno.Text + "</ApiPortno>";
                else
                    inXML += "          <ApiPortno>23</ApiPortno>";
                //API Port Ends...
                inXML += "          <DefaultProtocol>" + lstProtocol.SelectedValue + "</DefaultProtocol>";
                inXML += "          <MCUAddressType>" + lstMCUAddressType.SelectedValue + "</MCUAddressType>";
                inXML += "          <Deleted>0</Deleted>";
                inXML += "<ExchangeID>" + txtExchangeID.Text + "</ExchangeID>";//Cisco Telepresence fix
                //Code Added For FB 1422 - Start
                inXML += "          <TelnetAPI>0</TelnetAPI>";
                //Code Added For FB 1422 - End
                inXML += "    <conferenceCode>" + confcode + "</conferenceCode>";//FB 1642-Audio add on
                inXML += "    <leaderPin>" + leaderPin + "</leaderPin>";//FB 1642-Audio add on
                //FB 2595 Start
                if (chkSecured.Checked)
                    inXML += "<Secured>1</Secured>";
                else
                    inXML += "<Secured>0</Secured>";
                //FB 2595 End
                inXML += "        </Profile>";
                inXML += "      </Profiles>";
                inXML += "    </SetEndpoint>";
                return inXML;
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                errLabel.Text = ex.StackTrace + " : " + ex.Message;
                return "<error></error>";
            }
        }
        #endregion

        #region ValidateIPAddress
        protected void ValidateIPAddress(Object sender, ServerValidateEventArgs e)
        {
            try
            {
                errLabel.Text = "";
                String patternIP = @"^([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}$";
                String patternISDN = @"[1-9]\d{3,21}"; //[1-9][0-9]\d{3,9}
                Regex checkIP = new Regex(patternIP);
                Regex checkISDN = new Regex(patternISDN);
                bool valid = true;
                if ((lstProtocol.SelectedValue.Equals("1") && (lstAddressType.SelectedValue.Equals("4") || lstAddressType.SelectedValue.Equals("-1"))) || (lstProtocol.SelectedValue.Equals("2") && !lstAddressType.SelectedValue.Equals("4")))
                {
                    valid = false;
                    cusVal1.Text = obj.GetTranslatedText("Address Type and Protocol don't match. Please verify");//FB 1830 - Translation
                }
                else
                {
                    if (lstAddressType.SelectedValue.Equals(ns_MyVRMNet.vrmAddressType.IP) && lstProtocol.SelectedValue.Equals("1"))
                    {
                        valid = checkIP.IsMatch(txtAddress.Text, 0);
                        cusVal1.Text = obj.GetTranslatedText(ns_MyVRMNet.ErrorList.InvalidIPAddress);
                    }
                    else if (lstProtocol.SelectedValue.Equals("2"))
                    {
                        valid = checkISDN.IsMatch(txtAddress.Text, 0);
                        cusVal1.Text = obj.GetTranslatedText(ns_MyVRMNet.ErrorList.InvalidISDNAddress);
                    }
                }
                //Code changed by Offshore for FB Issue 1073 -- Start                
                //if (DateTime.Parse(txtAccountExpiry.Text) <= DateTime.Now)
                if (DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(txtAccountExpiry.Text)) <= DateTime.Now)
                //Code changed by Offshore for FB Issue 1073 -- End
                {
                    valid = false;
                    cusVal1.Text = obj.GetTranslatedText(ns_MyVRMNet.ErrorList.InvalidAccountExpiry);
                    txtAccountExpiry.Focus();
                }
                if (lstAddressType.SelectedValue.Equals(ns_MyVRMNet.vrmAddressType.MPI))
                    if (!lstConnectionType.SelectedValue.Equals(ns_MyVRMNet.vrmConnectionTypes.Direct) || !lstProtocol.SelectedValue.Equals(ns_MyVRMNet.vrmVideoProtocol.MPI) || !lstMCUAddressType.SelectedValue.Equals(ns_MyVRMNet.vrmAddressType.MPI))
                    {
                        valid = false;
                        cusVal1.Text = obj.GetTranslatedText("Invalid selection for Address Type, Protocol and Connection Type.");//FB 1830 - Translation
                        lstAddressType.Focus();
                    }
                if (lstAddressType.SelectedValue.Equals(ns_MyVRMNet.vrmAddressType.MPI) && (lstBridges.SelectedValue.Equals("-1")))
                {
                    valid = false;
                    cusVal1.Text = obj.GetTranslatedText("Please select MCU.");//FB 1830 - Translation
                    lstBridges.Focus();
                }
                //Response.Write(valid);
                //Response.End();
                e.IsValid = valid;
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message + " : " + ex.StackTrace);
            }
        }
        #endregion

        #region ValidateTypes
        protected void ValidateTypes(Object sender, EventArgs e)
        {
            try
            {
                switch (lstAddressType.SelectedValue)
                {
                    case ns_MyVRMNet.vrmAddressType.MPI:
                        lstConnectionType.ClearSelection();
                        lstConnectionType.Items.FindByValue(ns_MyVRMNet.vrmConnectionTypes.Direct).Selected = true;
                        lstProtocol.ClearSelection();
                        lstProtocol.Items.FindByValue(ns_MyVRMNet.vrmVideoProtocol.MPI).Selected = true;
                        reqBridges.Enabled = true;
                        break;
                    case ns_MyVRMNet.vrmAddressType.IP:
                        if (lstConnectionType.SelectedValue.Equals(ns_MyVRMNet.vrmConnectionTypes.Direct))
                        {
                            lstConnectionType.ClearSelection();
                            lstConnectionType.Items.FindByValue(ns_MyVRMNet.vrmConnectionTypes.DialIn).Selected = true;
                        }
                        lstProtocol.ClearSelection();
                        lstProtocol.Items.FindByValue(ns_MyVRMNet.vrmVideoProtocol.IP).Selected = true;
                        if (lstMCUAddressType.SelectedValue.Equals(ns_MyVRMNet.vrmAddressType.ISDN) || lstMCUAddressType.SelectedValue.Equals(ns_MyVRMNet.vrmAddressType.MPI))
                        {
                            lstMCUAddressType.ClearSelection();
                            lstMCUAddressType.Items.FindByValue(ns_MyVRMNet.vrmAddressType.IP).Selected = true;
                        }
                        break;
                    case ns_MyVRMNet.vrmAddressType.ISDN:
                        if (lstConnectionType.SelectedValue.Equals(ns_MyVRMNet.vrmConnectionTypes.Direct))
                        {
                            lstConnectionType.ClearSelection();
                            lstConnectionType.Items.FindByValue(ns_MyVRMNet.vrmConnectionTypes.DialIn).Selected = true;
                        }
                        lstProtocol.ClearSelection();
                        lstProtocol.Items.FindByValue(ns_MyVRMNet.vrmVideoProtocol.ISDN).Selected = true;
                        lstMCUAddressType.ClearSelection();
                        lstMCUAddressType.Items.FindByValue(ns_MyVRMNet.vrmAddressType.ISDN).Selected = true;
                        break;
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        //RSS Feed
        #region RSSFeed
        protected void RSSFeed(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("RSSGenerator.aspx");
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message);
            }
        }
        #endregion

        #region Bind to Rooms

        public void BindRoomToList()
        {
            String[] locsName = null;
            try
            {
                if (locstrname.Value != "")
                {

                    locsName = locstrname.Value.Split('+');
                    RoomList.Items.Clear();
                    foreach (String s in locsName)
                    {
                        if (s != "")
                        {

                            if (s.Split('|').Length > 1)
                                RoomList.Items.Add(new ListItem(s.Split('|')[1], s.Split('|')[0]));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
                Response.Write(ex.Message);
            }
        }
        #endregion

        //FB 1830 - Start
        #region LoadLanguage
        protected void LoadLanguage(XmlNodeList nodes)
        {
            try
            {
                XmlTextReader xtr;
                DataSet ds = new DataSet();
                foreach (XmlNode node in nodes)
                {
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.InferSchema);
                }
                if (ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];
                    lstLanguage.DataSource = dt;
                    lstLanguage.DataBind();
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        #region DefineEmailLanguage

        protected void DefineEmailLanguage(object sender, EventArgs e)
        {
            try
            {
				//FB 2283 Start
                Session.Remove("UsrBaseLang");
                Session["UsrBaseLang"] = lstLanguage.SelectedValue;
				//FB 2283 End
                if (Request.QueryString["t"] != null)
                    if (Request.QueryString["t"].ToString().Equals("1"))
                        Response.Redirect("EmailCustomization.aspx?tp=au");

                Response.Redirect("EmailCustomization.aspx?tp=u");
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message);
            }
        }
        #endregion
        //FB 1830 - End

        //FB 1860
        #region EditBlockEmails

        protected void EditBlockEmails(object sender, EventArgs e)
        {
            try
            {
                if (Request.QueryString["t"] != null)
                    if (Request.QueryString["t"].ToString().Equals("1"))
                        Response.Redirect("ViewBlockedMails.aspx?tp=au");

                Response.Redirect("ViewBlockedMails.aspx?tp=u");
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message);
            }
        }
        #endregion

        //FB 1830 - DeleteEmailLang start
        #region DeleteEmailLangugage
        /// <summary>
        /// DeleteEmailLangugage
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DeleteEmailLangugage(object sender, EventArgs e)
        {
            try
            {
                StringBuilder inXML = new StringBuilder();
                String outXML = "", emailLangID = "";

                if (Session["UsrEmailLangID"] != null)//FB 2283
                    if (Session["UsrEmailLangID"].ToString() != "")
                        emailLangID = Session["UsrEmailLangID"].ToString();

                if (emailLangID.Trim() != "")
                {
                    inXML.Append("<DeleteEmailLang>");
                    inXML.Append(obj.OrgXMLElement());
                    inXML.Append("<userid>" + txtUserID.Text + "</userid>");
                    inXML.Append("<emaillangid>" + emailLangID.ToString() + "</emaillangid>");
                    inXML.Append("<langOrigin>u</langOrigin>");
                    inXML.Append("</DeleteEmailLang>");

                    outXML = obj.CallMyVRMServer("DeleteEmailLanguage", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());

                    if (outXML.IndexOf("<error>") >= 0)
                    {
                        errLabel.Text = obj.ShowErrorMessage(outXML);
                        errLabel.Visible = true;
                    }
                    else
                    {
                        txtEmailLang.Text = "";
                        Session["UsrEmailLangID"] = null; //FB 2283
                        errLabel.Text = obj.GetTranslatedText("Operation Successful!");//FB 1830 - Translation
                        errLabel.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                log.Trace("UserProfile DeleteEmailLangugage :" + ex.Message);
            }
        }
        #endregion
        //FB 1830 - DeleteEmailLang end

        protected void lobbyManage(object sender,EventArgs e)
        {

            try
            {
                String reqstr="?p=1"; // FB 2567
                if (Request.QueryString["t"] != null)
                    if (Request.QueryString["t"].ToString().Equals("1"))
                        reqstr += "&t=au"; // FB 2567
                Response.Redirect("LobbyManagement.aspx" + reqstr);
            }
            catch(Exception ex)
            {
                log.Trace(ex.Message);
            }
        }

        //FB 2481 Start

        #region Private VMR Cancel
        /// <summary>
        /// video guest location Cancel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void fnPrivateVMRCancel(object sender, EventArgs e)
        {
            try
            {
                PrivateVMRPopup.Hide();
            }
            catch (Exception ex)
            {
                log.Trace("fnPrivateVMRCancel" + ex.Message);
            }
        }
        #endregion 

        #region Private VMR Submit
        /// <summary>
        /// Video Guest Location Submit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void fnPrivateVMRSubmit(object sender, EventArgs e)
        {
            try
            {
               
            }
            catch (Exception ex)
            {
                log.Trace("fnPrivateVMRSubmit" + ex.Message);
            }
        }
        #endregion video guest location submit
        //FB 2481 End
    }
}

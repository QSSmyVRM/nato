﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

namespace ns_license
{
    #region Class License
    /// <summary>
    /// Public license Class
    /// </summary>
    public partial class license : System.Web.UI.Page
    {
        #region Data Members
        /// <summary>
        /// All Data Members
        /// </summary>
        protected String init = "";

        #endregion

        #region Page Load
        /// <summary>
        /// Page Load Event Handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["InitPage"] != null)
                init = Session["InitPage"].ToString();
        }
        #endregion
    }
    #endregion
}

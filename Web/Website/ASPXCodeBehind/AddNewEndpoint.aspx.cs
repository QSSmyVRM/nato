﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Text.RegularExpressions;

namespace ns_MyVRM
{
    public partial class en_AddNewEndpoint : System.Web.UI.Page
    {
        # region prviate DataMember

        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnWebAccURL;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnLineRate;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnApiPortNo;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnVideoEquipment;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnMCUServiceAdd;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnExchangeID;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnEndpointID;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnEndpointURL;

        protected System.Web.UI.WebControls.Label errLabel;

        protected System.Web.UI.WebControls.TextBox txtEndpointName;
        protected System.Web.UI.WebControls.TextBox txtAddress;

        protected System.Web.UI.WebControls.CheckBox chkIsOutside;
        protected System.Web.UI.WebControls.CheckBox chkEncryption;
        protected System.Web.UI.WebControls.CheckBox chkListedEndpoint;
        protected System.Web.UI.WebControls.CheckBox chkUnlistedEndpoint;

        protected System.Web.UI.WebControls.DropDownList lstBridges;
        protected System.Web.UI.WebControls.DropDownList lstAddressType;
        protected System.Web.UI.WebControls.DropDownList lstMCUAddressType;
        protected System.Web.UI.WebControls.DropDownList lstConnection;
        protected System.Web.UI.WebControls.DropDownList lstConnectionType;
        protected System.Web.UI.WebControls.DropDownList lstProtocol;
        protected System.Web.UI.WebControls.CheckBox chkMute; //FB 2441
        
        myVRMNet.NETFunctions obj;
        ns_Logger.Logger log;

        private String inXML = "";
        private String outXML = "";
        private String queryStrtp = "ManageConference.aspx?t=";
        
        #endregion

        public en_AddNewEndpoint()
        {
            //
            // TODO: Add constructor logic here
            //
            obj = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger();
        }

        protected void Page_Load(Object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    //FB 2646 Starts
                    string confOrgID = "";
                    if (Request.QueryString["conforgID"] != null)
                    {
                        confOrgID = Request.QueryString["conforgID"].ToString();
                        Session.Add("multisiloOrganizationID", confOrgID);
                    }
                    
                    //FB 2646 Ends
                    obj.BindBridges(lstBridges);
                    obj.BindAddressType(lstAddressType);
                    obj.BindVideoProtocols(lstProtocol); 
                    obj.BindDialingOptions(lstConnectionType);
                    hdnEndpointID.Value = "";
                    if (Session["ProfileID"] == null)
                        Session.Add("ProfileID", "1");
                }
                if (Request.QueryString["tp"] != null)
                    if (Request.QueryString["tp"].Trim().Equals("cc"))
                        queryStrtp = "Dashboard.aspx?";
            }
            catch (Exception ex)
            {
                log.Trace("Page_Load" + ex.Message);
                errLabel.Text = obj.ShowSystemMessage();
            }

        }

        #region BindEndpointData
        /// <summary>
        /// BindEndpointData
        /// </summary>
        protected void BindEndpointData(Object sender,EventArgs e)
        {
            try
            {
                String inXML = "";
                inXML += "<EndpointDetails>";
                inXML += obj.OrgXMLElement();
                inXML += "  <UserID>" + Session["userID"].ToString() + "</UserID>";
                inXML += "  <EndpointID>" + hdnEndpointID.Value + "</EndpointID>";
                inXML += "</EndpointDetails>";
                String outXML = obj.CallMyVRMServer("GetEndpointDetails", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") < 0)
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);
                    XmlNodeList nodes = xmldoc.SelectNodes("//EndpointDetails/Endpoint/Profiles/Profile");
                    
                    if(xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Name") != null)
                        txtEndpointName.Text = xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Name").InnerText;

                    if (xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/DefaultProfileID") != null)
                        Session["ProfileID"]  = xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/DefaultProfileID").InnerText.Trim();

                    hdnEndpointID.Value = "new";
                    for(int i=0;i< nodes.Count;i++)
                    {
                        XmlNode node = nodes[i];
                        // only the Default profile's details will be captured and used 
                        if (node.SelectSingleNode("ProfileID") == null)
                            continue;
                        
                        if (node.SelectSingleNode("ProfileID").InnerText.Equals(Session["ProfileID"].ToString()))
                        {
                            lstConnectionType.ClearSelection();
                            lstAddressType.ClearSelection();
                            lstBridges.ClearSelection();
                            lstProtocol.ClearSelection();
                            lstConnection.ClearSelection();

                            if (node.SelectSingleNode("DefaultProtocol") != null)
                                lstProtocol.Items.FindByValue(node.SelectSingleNode("DefaultProtocol").InnerText.Trim()).Selected = true;

                            if (node.SelectSingleNode("AddressType") != null)
                                lstAddressType.Items.FindByValue(node.SelectSingleNode("AddressType").InnerText.Trim()).Selected = true;

                            if (node.SelectSingleNode("Address") != null)
                                txtAddress.Text = node.SelectSingleNode("Address").InnerText.Trim();

                            if (node.SelectSingleNode("ConnectionType") != null)
                                lstConnectionType.Items.FindByValue(node.SelectSingleNode("ConnectionType").InnerText.Trim()).Selected = true;

                            if (node.SelectSingleNode("Bridge") != null)
                            {
                                lstBridges.Items.FindByValue(node.SelectSingleNode("Bridge").InnerText.Trim()).Selected = true;
                                DisplayBridgeDetails(lstBridges, new EventArgs());
                            }

                            chkEncryption.Checked = false;
                            if (node.SelectSingleNode("EncryptionPreferred") != null)
                                if (node.SelectSingleNode("EncryptionPreferred").InnerText == "1")
                                    chkEncryption.Checked = true;

                            chkIsOutside.Checked = false;
                            if (node.SelectSingleNode("IsOutside") != null)
                                if (node.SelectSingleNode("IsOutside").InnerText.Trim().Equals("1"))
                                    chkIsOutside.Checked = true;

                            //FB 2441 - Starts
                            chkMute.Checked = false;
                            if (node.SelectSingleNode("IsMute") != null)
                                if (node.SelectSingleNode("IsMute").InnerText.Trim().Equals("1"))
                                    chkMute.Checked = true;
                            //FB 2441 - End

                            lstConnection.Items.FindByValue("2").Selected = true;

                            if (node.SelectSingleNode("VideoEquipment") != null)
                                hdnVideoEquipment.Value = node.SelectSingleNode("VideoEquipment").InnerText.Trim();

                            if (node.SelectSingleNode("LineRate") != null)
                                hdnLineRate.Value = node.SelectSingleNode("LineRate").InnerText.Trim();

                            if (node.SelectSingleNode("ExchangeID") != null)
                                hdnExchangeID.Value = node.SelectSingleNode("ExchangeID").InnerText.Trim();

                            if (node.SelectSingleNode("ApiPortno") != null)
                                hdnApiPortNo.Value = node.SelectSingleNode("ApiPortno").InnerText.Trim();

                            if (node.SelectSingleNode("URL") != null)
                                hdnEndpointURL.Value = node.SelectSingleNode("URL").InnerText.Trim();
                            
                        }
                        
                    }
                }

            }
            catch (Exception ex)
            {
                log.Trace("Page_Load" + ex.Message);
                errLabel.Text = obj.ShowSystemMessage();
            }
        }
        #endregion
       
        // if a bridge is associated with an endpoint profile then we get the 
        //details of that bridge and associated IP/ISDN services
        #region DisplayBridgeDetails
        /// <summary>
        /// DisplayBridgeDetails
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DisplayBridgeDetails(Object sender, EventArgs e)
        {
            try
            {
                if (lstBridges.SelectedIndex > 0)
                {
                    inXML = "<login>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID><bridgeID>" + lstBridges.SelectedValue + "</bridgeID></login>";//Organization Module Fixes
                    outXML = obj.CallMyVRMServer("GetOldBridge", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                    if (outXML.IndexOf("<error>") < 0)
                    {
                        XmlDocument xmldoc = new XmlDocument();
                        xmldoc.LoadXml(outXML);
                        if (xmldoc.SelectSingleNode("//bridge/bridgeDetails/controlPortIPAddress") != null)
                            hdnMCUServiceAdd.Value = xmldoc.SelectSingleNode("//bridge/bridgeDetails/controlPortIPAddress").InnerText;
                        if (xmldoc.SelectSingleNode("//bridge/bridgeDetails/portA") != null)
                            hdnMCUServiceAdd.Value = xmldoc.SelectSingleNode("//bridge/bridgeDetails/portA").InnerText;
                        obj.BindAddressType(lstMCUAddressType);
                        lstMCUAddressType.ClearSelection();
                        lstMCUAddressType.Items.FindByValue(lstAddressType.SelectedValue).Selected = true;
                        errLabel.Text = "";
                    }
                    else
                    {
                        errLabel.Text = obj.ShowErrorMessage(outXML);
                        errLabel.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                log.Trace("SubmitEndpoint" + ex.Message);
                errLabel.Text = obj.ShowSystemMessage();
            }
        }
        #endregion

        #region SubmitEndpoint
        /// <summary>
        /// SubmitEndpoint
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SubmitEndpoint(Object sender, EventArgs e)
        {
            String epID = "";//FB 2261
            XmlDocument xmlDoc = null;//FB 2261
            try
            {
                if (!Addressvalidation())
                    return;

                inXML += "<SetConferenceEndpoint>";
                inXML += obj.OrgXMLElement();
                inXML += "<UserID>" + Session["userID"].ToString() + "</UserID>";
                inXML += "<ConfID>" + Session["ConfID"].ToString() + "</ConfID>";
                inXML += "<Endpoint>";
                inXML += "<EndpointID>" + hdnEndpointID.Value + "</EndpointID>"; // As 'new' to Enter into guest data as new party
                inXML += "<EndpointName>" + txtEndpointName.Text + "</EndpointName>";
                inXML += "<EndpointLastName></EndpointLastName>";//FB 2528
                inXML += "<EndpointEmail>admin@myvrm.com</EndpointEmail>";
                inXML += "<ProfileID>" + Session["ProfileID"].ToString() + "</ProfileID>";
                inXML += "<Type>U</Type>";
                inXML += "<ApiPortno>"+ hdnApiPortNo.Value +"</ApiPortno>";
                
                if (chkEncryption.Checked)
                    inXML += "<EncryptionPreferred>1</EncryptionPreferred>";
                else
                    inXML += "<EncryptionPreferred>0</EncryptionPreferred>";
                
                inXML += "<AddressType>" + lstAddressType.Text + "</AddressType>";
                inXML += "<Address>" + txtAddress.Text + "</Address>";
                inXML += "<URL>" + hdnEndpointURL.Value + "</URL>";
                
                if (chkIsOutside.Checked)
                    inXML += "<IsOutside>1</IsOutside>";
                else
                    inXML += "<IsOutside>0</IsOutside>";

                //FB 2441 - Starts
                if (chkMute.Checked)
                    inXML += "<IsMute>1</IsMute>";
                else
                    inXML += "<IsMute>0</IsMute>";
                //FB 2441 - End

                inXML += "<ConnectionType>" + lstConnectionType.SelectedValue + "</ConnectionType>";
                inXML += "<VideoEquipment>" + hdnVideoEquipment.Value + "</VideoEquipment>";
                inXML += "<ExchangeID>" + hdnExchangeID.Value + "</ExchangeID>";
                inXML += "<LineRate>"+ hdnLineRate.Value +"</LineRate>";
                inXML += "<Bridge>" + lstBridges.SelectedValue + "</Bridge>";
                inXML += "<Connection>" + lstConnection.SelectedValue + "</Connection>";
                inXML += "<Protocol>" + lstProtocol.SelectedValue + "</Protocol>";
                inXML += "<BridgeAddress>" + hdnMCUServiceAdd.Value + "</BridgeAddress>";
                inXML += "<BridgeAddressType>" + lstMCUAddressType.SelectedValue + "</BridgeAddressType>";
                inXML += "</Endpoint>";
                inXML += "</SetConferenceEndpoint>";

                outXML = obj.CallMyVRMServer("SetConferenceEndpoint", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                
                if (outXML.IndexOf("<error>") < 0)
                {
                    /*** FB 2261 **/
                    xmlDoc = new XmlDocument();
                    xmlDoc.LoadXml(outXML);

                    if (xmlDoc.SelectSingleNode("/SetConferenceEndpoint/EndpointID") != null)
                        epID = xmlDoc.SelectSingleNode("/SetConferenceEndpoint/EndpointID").InnerText;

                    if (epID != "")
                    {
                        inXML = "<AddConferenceEndpoint>";
                        inXML += obj.OrgXMLElement();
                        inXML += "<UserID>" + Session["userID"].ToString() + "</UserID>";
                        inXML += "<ConfID>" + Session["ConfID"].ToString() + "</ConfID>";
                        inXML += "<EndpointID>" + epID + "</EndpointID>";
                        inXML += "</AddConferenceEndpoint>";

                        outXML = obj.CallCommand("AddConferenceEndpoint", inXML);
                    }

                    /*** FB 2261 **/
                    Button btnCtrl = (System.Web.UI.WebControls.Button)sender;
                    if (btnCtrl.ID.Trim().Equals("btnSubmitAddNew"))
                    {
                        hdnEndpointID.Value = "";
                        errLabel.Text = obj.ShowSuccessMessage();
                        Page_Load(null, null);
                    }
                    else
                        Response.Redirect(queryStrtp+"&m=1");
                }
                else
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
            }
            catch (Exception ex)
            {
                log.Trace("SubmitEndpoint" + ex.Message);
                errLabel.Text = obj.ShowSystemMessage();
            }
        }
        #endregion

        #region AddressValidation
        /// <summary>
        /// Addressvalidation
        /// </summary>
        /// <returns></returns>
        protected bool Addressvalidation()
        {
            try
            {
                string pattern = @"^([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}\:?(\d{4})?$";
                Regex check = new Regex(pattern);
                bool valid = true;
                String errmsg = "",IPToValidate = txtAddress.Text;

                if ((lstProtocol.SelectedValue.Equals(ns_MyVRMNet.vrmVideoProtocol.ISDN) && !lstAddressType.SelectedValue.Equals(ns_MyVRMNet.vrmAddressType.ISDN)) || (!lstProtocol.SelectedValue.Equals(ns_MyVRMNet.vrmVideoProtocol.ISDN) && lstAddressType.SelectedValue.Equals(ns_MyVRMNet.vrmAddressType.ISDN)))
                {
                    errLabel.Text = obj.GetTranslatedText("Protocol does not match with selected Address Type.");//FB 1830 - Translation
                    return false;
                }

                if (lstProtocol.SelectedValue.Equals(ns_MyVRMNet.vrmAddressType.IP) && lstAddressType.SelectedValue.Equals(ns_MyVRMNet.vrmAddressType.IP))
                {
                    if (IPToValidate == "")
                        valid = false;
                    else
                        valid = check.IsMatch(IPToValidate, 0);

                    
                }
                else if (lstProtocol.SelectedValue.Equals("2") && lstAddressType.SelectedValue.Equals(ns_MyVRMNet.vrmAddressType.ISDN))
                    valid = !check.IsMatch(IPToValidate, 0);
                else if (lstAddressType.SelectedValue.Equals(ns_MyVRMNet.vrmAddressType.MPI))
                {
                    valid = !check.IsMatch(IPToValidate, 0);
                    if (!obj.IsValidMCUForMPI(lstBridges.SelectedValue, ref errmsg) && valid)
                    {
                        errLabel.Text = ns_MyVRMNet.ErrorList.InvalidMPIBridge;
                        return false;
                    }
                }
                if (valid.Equals(false))
                {
                    txtAddress.Focus();
                    errLabel.Text = obj.GetTranslatedText("Invalid Address.");//FB 1830 - Translation
                }

                
                return valid;
            }
            catch(Exception ex)
            {
                log.Trace("validation:" + ex.Message);
                return false;
            }
        }

        #endregion

        #region CancelEndpoint
        /// <summary>
        /// CancelEndpoint
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CancelEndpoint(Object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(queryStrtp);
            }
            catch (Exception ex)
            {
                log.Trace("CancelEndpoint"+ex.Message);
            }
        }
        #endregion
    }

}
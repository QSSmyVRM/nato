using System;
using System.Data;
using System.Xml;
using System.Text;
using System.IO;
using System.Collections;
using System.ComponentModel;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.Xml.Schema;

using System.Management;
using System.Management.Instrumentation;

/// <summary>
/// Summary description for SuperAdministrator.
/// </summary>

namespace ns_SuperAdministrator
{
    public partial class SuperAdministrator : System.Web.UI.Page
    {

        #region protected Members

        protected System.Web.UI.WebControls.Label errLabel;
        protected System.Web.UI.WebControls.Label lblLicenseDetails;
        protected System.Web.UI.WebControls.TextBox txtLicenseKey;
        protected System.Web.UI.WebControls.TextBox txtSystemEmail;
        protected System.Web.UI.WebControls.TextBox txtDisplayName;
        protected System.Web.UI.WebControls.TextBox txtServerAddress;
        protected System.Web.UI.WebControls.TextBox txtMailServerLogin;
        protected System.Web.UI.WebControls.TextBox txtMSPassword1;
        protected System.Web.UI.WebControls.TextBox txtServerPort;
        protected System.Web.UI.WebControls.TextBox txtWebsiteURL;
        protected System.Web.UI.WebControls.TextBox txtLDAPServerAddress;
        protected System.Web.UI.WebControls.TextBox txtLDAPAccountLogin;
        protected System.Web.UI.WebControls.TextBox txtLDAPAccountPassword1;
        protected System.Web.UI.WebControls.TextBox txtLDAPAccountPassword2;
        protected System.Web.UI.WebControls.TextBox txtMSPassword2;
        protected System.Web.UI.WebControls.TextBox txtLDAPServerPort;
        protected System.Web.UI.WebControls.TextBox txtLDAPSearchFilter;
        protected System.Web.UI.WebControls.TextBox txtMailMessage;
        protected System.Web.UI.WebControls.TextBox txtLDAPLoginKey;
        protected System.Web.UI.WebControls.TextBox txtLDAPPrefix;
        protected System.Web.UI.WebControls.TextBox txtCompanymessage;
        //site logo...
        protected System.Web.UI.WebControls.TextBox txtRetryCount;//FB 2552

        protected System.Web.UI.WebControls.Label hdnImageId;


        protected System.Web.UI.WebControls.Label hdnUploadMap1;
        protected System.Web.UI.HtmlControls.HtmlInputHidden Map1ImageDt;
        protected System.Web.UI.HtmlControls.HtmlInputFile fleMap1;
        protected System.Web.UI.WebControls.Button btnUploadImages;
        protected System.Web.UI.WebControls.Label hdnCompanymessage;
        protected System.Web.UI.WebControls.Label lblsitelogo;
        //Banner logo...

        protected System.Web.UI.WebControls.Label hdnImageId1;

        protected System.Web.UI.WebControls.Label hdnUploadMap2;
        protected System.Web.UI.HtmlControls.HtmlInputHidden Map2ImageDt;
        protected System.Web.UI.HtmlControls.HtmlInputFile fleMap2;
        protected System.Web.UI.WebControls.Label lblstdres;
        protected System.Web.UI.WebControls.Label lblHighres;



        protected System.Web.UI.WebControls.Label hdnImageId2;

        protected System.Web.UI.WebControls.Label hdnUploadMap3;
        protected System.Web.UI.HtmlControls.HtmlInputHidden Map3ImageDt;
        protected System.Web.UI.HtmlControls.HtmlInputFile fleMap3;




        protected System.Web.UI.WebControls.Button btnSubmit;
        protected System.Web.UI.WebControls.DropDownList lstLDAPConnectionTimeout;
        protected System.Web.UI.WebControls.CheckBoxList chkLstDays;
        protected MetaBuilders.WebControls.ComboBox lstLDAPScheduleTime;
        protected DropDownList PreferTimezone;//Code added for organisation module
        //protected DropDownList DrpOrganization;//Code added for organisation module//Commented for FB 1849


        protected System.Web.UI.WebControls.Label ActStatus;
        protected System.Web.UI.WebControls.Label EncrypTxt;
        protected System.Web.UI.WebControls.Label DeactLbl;
        protected System.Web.UI.HtmlControls.HtmlTableRow ActivationTR;
        protected System.Web.UI.HtmlControls.HtmlTableCell ExportTD;
        protected String dtFormatType = "MM/dd/yyyy";
        //protected System.Web.UI.HtmlControls.HtmlTableRow trSwt; //Added for New Menu design//Commented for FB 1849
        //protected AjaxControlToolkit.ModalPopupExtender RoomPopUp; //Added for New Menu design//Commented for FB 1849
        protected TextBox CompanyInfo;
        protected System.Web.UI.HtmlControls.HtmlInputButton btnOrg; //FB 1662
        protected System.Web.UI.HtmlControls.HtmlInputButton btnuser; //FB 1969
		protected System.Web.UI.WebControls.TextBox txtLaunchBuffer;//FB 2007
        //FB 2501  EM7 Starts
        protected System.Web.UI.WebControls.TextBox txtEM7URI;
        protected System.Web.UI.WebControls.TextBox txtEM7Username;
        protected System.Web.UI.WebControls.TextBox txtEM7ConformPassword;
        protected System.Web.UI.WebControls.TextBox txtEM7Password;
        protected System.Web.UI.WebControls.TextBox txtEM7Port;
        //FB 2501  EM7 Ends

        //FB 2363 - Start
       
        protected System.Web.UI.WebControls.TextBox txtPartnerName;
        protected System.Web.UI.WebControls.TextBox txtPartnerEmail;
        protected System.Web.UI.WebControls.TextBox txtPartnerURL;
        protected System.Web.UI.WebControls.TextBox txtPUserName;
        protected System.Web.UI.WebControls.TextBox txtP1Password;
        protected System.Web.UI.WebControls.TextBox txtP2Password;
        protected System.Web.UI.WebControls.TextBox txtTimeoutValue;//FB 2363

        protected System.Web.UI.WebControls.TextBox txtUsrRptCustmName;
        protected System.Web.UI.WebControls.TextBox txtUsrRptDestination;
        protected System.Web.UI.WebControls.TextBox txtUsrRptStartTime;
        protected System.Web.UI.WebControls.TextBox txtUsrRptSentTime;
        protected System.Web.UI.WebControls.DropDownList lstUsrRptDeliverType;
        protected System.Web.UI.WebControls.DropDownList lstUsrRptFrequencyType;
        protected System.Web.UI.WebControls.DropDownList lstUsrRptFrequencyCount;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnESMailUsrSent;
        protected System.Web.UI.WebControls.DropDownList lstEnableLaunchBufferP2P; //FB 2437
        protected System.Web.UI.WebControls.DropDownList lstStartMode; //FB 2501

        //FB 2598 Starts
        protected System.Web.UI.HtmlControls.HtmlTableRow trEM7Connectivity;
        protected System.Web.UI.HtmlControls.HtmlTableRow trEM7URIUsrName;
        protected System.Web.UI.HtmlControls.HtmlTableRow trEM7Pwd;
        protected System.Web.UI.HtmlControls.HtmlTableRow trEM7Port;
        //FB 2598 Ends
        
        //FB 2363 - End

        //FB 2392 start
        protected System.Web.UI.WebControls.TextBox txtWhyGoURL;
        protected System.Web.UI.WebControls.TextBox txtWhygoUsr;
        protected System.Web.UI.WebControls.TextBox txtWhygoPwd;
        protected System.Web.UI.WebControls.TextBox txtWhygoPwd2;
        //FB 2392 end
        //FB 2594 Starts
        protected System.Web.UI.HtmlControls.HtmlTableRow trWhygo;
        protected System.Web.UI.HtmlControls.HtmlTableRow trWhygoURL;
        protected System.Web.UI.HtmlControls.HtmlTableRow trWhygoPassword;
        protected System.Web.UI.HtmlControls.HtmlTableRow trPoll;
        protected System.Web.UI.WebControls.ImageButton delPubliRoom;
        protected System.Web.UI.WebControls.Button PollWhyGo;
        string WhygoPW = "";
        //FB 2594 Ends

        #endregion

        myVRMNet.NETFunctions obj;
        ns_Logger.Logger log;
        String tformat = "hh:mm tt";
        protected Int32 orgid = 11;
        String fName; //Image Project
        //Site Logo...
        string fileext;
        String pathName;
        myVRMNet.ImageUtil imageUtilObj = null;
        byte[] imgArray = null;
        string companyinfofile = "";
        protected string language = ""; //FB 1830

        #region Constructor
        public SuperAdministrator()
        {
            obj = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger();
        }

        #endregion

        #region Methods Executed on PreRender
        private void Page_PreRender(object sender, System.EventArgs e)
        {
            string js = "<script language=javascript>";
            js += "  function _fixsmartnav(){";
            js += "     if(window.__smartNav!=null){";
            js += "        var a=window.__smartNav.update.toString();";
            js += "        a=a.replace('hdm.appendChild(k);','try{hdm.appendChild(k);}catch(e){}');";
            js += "        eval('window.__smartNav.update='+a);";
            js += "        document.detachEvent('onmousemove', _fixsmartnav);";
            js += "     }";
            js += "  }";
            js += "  document.attachEvent('onmousemove', _fixsmartnav);";
            js += "  document.body.onload=_fixsmartnav;";
            js += "</script>";
            RegisterClientScriptBlock("_CdgMnk_FixSmartNavBug", js);
        }
        #endregion

        #region Methods Executed on Page Load

        private void Page_Load(object sender, System.EventArgs e)
        {
            //            Response.Write(Session["sMenuMask"].ToString());
            try
            {
                imageUtilObj = new myVRMNet.ImageUtil(); //FB 1633
                if (HttpContext.Current.Application["SchemaPath"] != null)
                {
                    companyinfofile = HttpContext.Current.Application["SchemaPath"].ToString() + "\\company.ifo";
                }
                else
                {
                    Response.Redirect("genlogin.asp");
                }
                //Commented for FB 1849
                /*if (!IsPostBack)
                {
                    if (Request.QueryString["c"] != null) // Added for New Menu Design
                    {
                        if (Request.QueryString["c"] != "")
                            if (Request.QueryString["c"].Equals("1"))
                            {
                                trSwt.Attributes.Add("style", "display:none");
                                RoomPopUp.Show();
                            }
                    }


                }*/

                if (Session["FormatDateType"] != null)
                {
                    if (Session["FormatDateType"].ToString() == "")
                        Session["FormatDateType"] = "MM/dd/yyyy";
                    else
                        dtFormatType = Session["FormatDateType"].ToString();
                }

                Session["timeFormat"] = ((Session["timeFormat"] == null) ? "1" : Session["timeFormat"]);
                tformat = (Session["timeFormat"].ToString().Equals("0") ? "HH:mm" : "hh:mm tt");
                lstLDAPScheduleTime.Items.Clear();
                obj.BindTimeToListBox(lstLDAPScheduleTime, false, false);

                //FB 1830- Starts
                if (Session["language"] != null)
                {
                    if (Session["language"].ToString() != "")
                        language = Session["language"].ToString();
                }
                //FB 1830- End

                if (!IsPostBack)
                {
                    //obj.BindOrganizationNames(DrpOrganization);//Commented for FB 1849
                    try
                    {
                        String inXML = "<login>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID></login>";//Organization Module Fixes
                        //Response.Write(Application["COM_ConfigPath"].ToString());
                        String outXML = obj.CallCommand("GetSuperAdmin", inXML);//FB 2027
						//String outXML = obj.CallCOM("GetSuperAdmin", inXML, Application["COM_ConfigPath"].ToString());
                        //Response.Write(obj.Transfer(outXML));
                        //                    Session["outxml"] = outXML;

                        if (Session["organizationID"] != null)
                            Int32.TryParse(Session["organizationID"].ToString(), out orgid);
                        /*Commented for FB 1849
                        DrpOrganization.ClearSelection();

                        if (DrpOrganization.Items.FindByValue(orgid.ToString()) != null)
                              DrpOrganization.Items.FindByValue(orgid.ToString()).Selected = true;
                         */

                        if (outXML.IndexOf("<error>") < 0)
                        {
                            Session.Add("outXML", outXML);
                            BindData();
                            //FB 2594 Starts
                            PollWhyGo.Enabled = true;
                            if ((txtWhyGoURL.Text == "") || (WhygoPW == "") || (txtWhygoUsr.Text == ""))
                            {
                                //errLabel.Text = obj.GetTranslatedText("Please enter the Whygo Integration Settings.");
                                PollWhyGo.Enabled = false;
                            }
                            //FB  2594 Ends
                            //Site Logo...
                            GetImage();
                            if (hdnCompanymessage.Text != "")
                            {
                                txtCompanymessage.Text = hdnCompanymessage.Text;
                            }
                            GetActivation(Application["MyVRMServer_ConfigPath"].ToString());

                        }
                        else
                        {
                            errLabel.Text = obj.ShowErrorMessage(outXML);
                        }
                        if (Request.QueryString["m"] != null)
                        {
                            if (Request.QueryString["m"].ToString().Equals("1"))
                            {
                                errLabel.Text = obj.GetTranslatedText("Operation Successful! Please logout and re-login to see the changes."); //Custom attribute fixes //FB 1830 - Translation
                                errLabel.Visible = true;
                            }
                        }
                        //Code added for FB 1662 start...
                        btnOrg.Disabled = true;
                        if (orgid == myVRMNet.NETFunctions.defaultOrgID)
                        {
                            btnOrg.Disabled = false;
                        }
                        //Code added for FB 1662 end...

                        //Code added for FB 1969 start...
                        btnuser.Disabled = true;
                        if (orgid == myVRMNet.NETFunctions.defaultOrgID)
                        {
                            btnuser.Disabled = false;
                        }
                        //Code added for FB 1969 end...

                    }
                    catch (Exception ex)
                    {
                        log.Trace(ex.StackTrace + "Error in Getting System Administrator Settings: " + ex.Message);
						errLabel.Text  = obj.ShowSystemMessage();//FB 1881
                        //errLabel.Text = "Error in Getting System Administrator Settings. Please contact VRM Administrator. ";
                        errLabel.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                //                Response.Write(ex.Message);
                errLabel.Visible = true;
                errLabel.Text = ex.Message;
            }
        }


        private void BindData()
        {
            try
            {

                XmlDocument xmlDOC = new XmlDocument();
                xmlDOC.LoadXml(Session["outxml"].ToString());
                XmlNode node = (XmlNode)xmlDOC.DocumentElement;
                txtLicenseKey.Text = node.SelectSingleNode("//preference/securityKey").InnerText;
                //FB 1774 - Start
                String license = node.SelectSingleNode("//preference/licenseDetail").InnerText;

                if (Session["FormatDateType"].ToString() == "dd/MM/yyyy")
                {
                    String licDate = license.Split(' ')[3].ToString();                    
                    String formatDate = "";

                    if (licDate != "")
                        formatDate = myVRMNet.NETFunctions.GetFormattedDate(licDate);

                    license = license.Replace(licDate, formatDate);

                    lblLicenseDetails.Text = license;
                }
                else
                    lblLicenseDetails.Text = license;
                //FB 1774 - End

                XmlNodeList nodes = node.SelectNodes("//preference/accountingLogics/logic");
                int length = nodes.Count;
                int index = 0;

                /*** code added for Organisation module ***/
                try
                {
                    String tmpstr = node.SelectSingleNode("/preference/SystemTimeZoneID").InnerText;
                    PreferTimezone.ClearSelection();
                    PreferTimezone.Items.FindByValue(tmpstr).Selected = true;

                    txtLaunchBuffer.Text = node.SelectSingleNode("//preference/launchBuffer").InnerText;//FB 2007

                    lstEnableLaunchBufferP2P.ClearSelection();  //FB 2437
                    if (lstEnableLaunchBufferP2P.Items.FindByValue(node.SelectSingleNode("//preference/EnableLaunchBufferP2P").InnerText) != null)
                        lstEnableLaunchBufferP2P.Items.FindByValue(node.SelectSingleNode("//preference/EnableLaunchBufferP2P").InnerText).Selected = true;
                }
                catch (Exception ex)
                {
                    log.Trace("Server Timezone: " + ex.Message);
                }
                //FB 2501 starts
                lstStartMode.ClearSelection();
                if (lstStartMode.Items.FindByValue(node.SelectSingleNode("//preference/StartMode").InnerText) != null)
                    lstStartMode.Items.FindByValue(node.SelectSingleNode("//preference/StartMode").InnerText).Selected = true;
                //FB 2501 ends

                if (Session["EnablePublicRooms"].ToString() == "0")
                {
                    trWhygo.Visible = false;
                    trWhygoURL.Visible = false;
                    trWhygoPassword.Visible = false;
                    trPoll.Visible = false;
                }
                else
                {
                    trWhygo.Visible = true;
                    trWhygoURL.Visible = true;
                    trWhygoPassword.Visible = true;
                    trPoll.Visible = true;

                    //FB 2392 start
                    if (node.SelectSingleNode("//preference/WhyGo/WhyGoURL") != null)
                        txtWhyGoURL.Text = node.SelectSingleNode("//preference/WhyGo/WhyGoURL").InnerText;

                    if (node.SelectSingleNode("//preference/WhyGo/WhyGoUserName") != null)
                        txtWhygoUsr.Text = node.SelectSingleNode("//preference/WhyGo/WhyGoUserName").InnerText;

                    if (node.SelectSingleNode("//preference/WhyGo/WhyGoPassword") != null)
                    {
                        txtWhygoPwd.Attributes.Add("value", node.SelectSingleNode("//preference/WhyGo/WhyGoPassword").InnerText);
                        txtWhygoPwd2.Attributes.Add("value", node.SelectSingleNode("//preference/WhyGo/WhyGoPassword").InnerText);
                        WhygoPW = node.SelectSingleNode("//preference/WhyGo/WhyGoPassword").InnerText;
                    }
                    //FB 2392 end
                }
                /*** code added for Organisation module ***/

                if (node.SelectSingleNode("//preference/emailSystem").InnerXml != "")  //FB 2027
                {
                    txtSystemEmail.Text = node.SelectSingleNode("//preference/emailSystem/companyEmail").InnerText;
                    txtDisplayName.Text = node.SelectSingleNode("//preference/emailSystem/displayName").InnerText;
                    txtMailServerLogin.Text = node.SelectSingleNode("//preference/emailSystem/accountLogin").InnerText;
                    //txtMSPassword1.Text = node.SelectSingleNode("/preference/emailSystem/accountPwd").InnerText;
                    txtMSPassword1.Attributes.Add("value", node.SelectSingleNode("//preference/emailSystem/accountPwd").InnerText);
                    txtMSPassword2.Attributes.Add("value", node.SelectSingleNode("//preference/emailSystem/accountPwd").InnerText);
                    txtServerAddress.Text = node.SelectSingleNode("//preference/emailSystem/serverAddress").InnerText;
                    txtServerPort.Text = node.SelectSingleNode("//preference/emailSystem/portNo").InnerText;
                    txtMailMessage.Text = node.SelectSingleNode("//preference/emailSystem/messageTemplate").InnerText;
                    txtWebsiteURL.Text = node.SelectSingleNode("//preference/emailSystem/websiteURL").InnerText;
                    txtRetryCount.Text = node.SelectSingleNode("//preference/emailSystem/retrycount").InnerText;//FB 2552
                    
                }

                if (txtSystemEmail.Text.Trim().Equals("")) //FB 2027
                    txtSystemEmail.Text = "support@myvrm.com";

                //for (int i = 0; i < Request.ServerVariables.Count; i++)
                //    Response.Write("<br>" + Request.ServerVariables[i].ToString() + " : " + Request.ServerVariables[i].ToString());
                if (node.SelectSingleNode("//preference/LDAP").InnerXml != "") //FB 2027
                {
                    txtLDAPServerAddress.Text = node.SelectSingleNode("//preference/LDAP/serverAddress").InnerText;
                    txtLDAPServerPort.Text = node.SelectSingleNode("//preference/LDAP/portNo").InnerText;
                    txtLDAPAccountLogin.Text = node.SelectSingleNode("//preference/LDAP/loginName").InnerText;
                    txtLDAPAccountPassword1.Attributes.Add("value", node.SelectSingleNode("//preference/LDAP/loginPassword").InnerText);
                    txtLDAPAccountPassword2.Attributes.Add("value", node.SelectSingleNode("//preference/LDAP/loginPassword").InnerText);
                    txtLDAPLoginKey.Text = node.SelectSingleNode("//preference/LDAP/loginkey").InnerText;
                    txtLDAPSearchFilter.Text = node.SelectSingleNode("//preference/LDAP/searchfilter").InnerText;
                    txtLDAPPrefix.Text = node.SelectSingleNode("//preference/LDAP/LDAPPrefix").InnerText.Trim();
                    String schDays = "";
                    try
                    {
                        schDays = node.SelectSingleNode("//preference/LDAP/scheduler/Days").InnerText;
                    }
                    catch (Exception ex)
                    {
                        log.Trace(ex.Message + " : " + ex.StackTrace);
                    }
                    if (schDays.IndexOf(",") > 0)
                        for (int i = 0; i < schDays.Split(',').Length; i++)
                            chkLstDays.Items.FindByValue(schDays.Split(',')[i].Trim()).Selected = true;
                    else if (schDays.Length.Equals(1))
                        chkLstDays.Items.FindByValue(schDays.Trim()).Selected = true;
                    try
                    {
                        lstLDAPScheduleTime.Text = DateTime.Parse(node.SelectSingleNode("//preference/LDAP/scheduler/Time").InnerText).ToString(tformat);
                    }
                    //Code Modified for Synchronization Time Issues - Start
                    catch (Exception)
                    {
                        if (lstLDAPScheduleTime.Text.Trim().Equals(""))
                            lstLDAPScheduleTime.Text = lstLDAPScheduleTime.Items[0].Text;
                    }
                    //Code Modified for Synchronization Time Issues - End
                    ListItem temp;
                    temp = lstLDAPConnectionTimeout.Items.FindByValue(node.SelectSingleNode("//preference/LDAP/connectionTimeout").InnerText);
                    if (temp != null)
                        temp.Selected = true;
                }

                //FB 2501 EM7 Starts
                if (node.SelectSingleNode("//preference/EM7Connectivity").InnerXml != "") 
                {
                    if (node.SelectSingleNode("//preference/EM7Connectivity/EM7URI") != null)
                         txtEM7URI.Text = node.SelectSingleNode("//preference/EM7Connectivity/EM7URI").InnerText;
                    if (node.SelectSingleNode("//preference/EM7Connectivity/EM7Username") != null)
                        txtEM7Username.Text = node.SelectSingleNode("//preference/EM7Connectivity/EM7Username").InnerText;
                    if (node.SelectSingleNode("//preference/EM7Connectivity//EM7Password")!= null)
                        txtEM7Password.Attributes.Add("value", node.SelectSingleNode("//preference/EM7Connectivity/EM7Password").InnerText);
                    if (node.SelectSingleNode("//preference/EM7Connectivity//EM7Password") != null)
                        txtEM7ConformPassword.Attributes.Add("value", node.SelectSingleNode("//preference/EM7Connectivity/EM7Password").InnerText);
                    if (node.SelectSingleNode("//preference/EM7Connectivity//EM7Port") != null)
                        txtEM7Port.Text = node.SelectSingleNode("//preference/EM7Connectivity/EM7Port").InnerText;
                }
                //FB 2501  EM7 Ends

                if (txtLDAPServerPort.Text.Trim().Equals(""))
                    txtLDAPServerPort.Text = "389";
                if (txtServerPort.Text.Trim().Equals(""))
                    txtServerPort.Text = "25";
                if (txtWebsiteURL.Text.Trim().Equals(""))
                    txtWebsiteURL.Text = Request.ServerVariables["HTTP_REFERER"].ToString().Remove(Request.ServerVariables["HTTP_REFERER"].ToString().IndexOf("/" + language + "/")); //FB 1830
                if (txtSystemEmail.Text.Trim().Equals(""))
                    txtSystemEmail.Text = "support@myvrm.com";

                if (lstLDAPScheduleTime.Text.Trim().Equals(""))//FB 2027
                    lstLDAPScheduleTime.Text = lstLDAPScheduleTime.Items[0].Text;

                //FB 2363 - Start
                if (node.SelectSingleNode("//preference/External").InnerXml != "")
                {
                    if (node.SelectSingleNode("//preference/External/PartnerName") != null)
                    {
                        if (node.SelectSingleNode("//preference/External/PartnerName").InnerText != "")
                            txtPartnerName.Text = node.SelectSingleNode("//preference/External/PartnerName").InnerText;
                    }

                    if (node.SelectSingleNode("//preference/External/PartnerEmail") != null)
                    {
                        if (node.SelectSingleNode("//preference/External/PartnerEmail").InnerText != "")
                            txtPartnerEmail.Text = node.SelectSingleNode("//preference/External/PartnerEmail").InnerText;
                    }

                    if (node.SelectSingleNode("//preference/External/PartnerURL") != null)
                    {
                        if (node.SelectSingleNode("//preference/External/PartnerURL").InnerText != "")
                            txtPartnerURL.Text = node.SelectSingleNode("//preference/External/PartnerURL").InnerText;
                    }

                    if (node.SelectSingleNode("//preference/External/UserName") != null)
                    {
                        if (node.SelectSingleNode("//preference/External/UserName").InnerText != "")
                            txtPUserName.Text = node.SelectSingleNode("//preference/External/UserName").InnerText;
                    }

                    if (node.SelectSingleNode("//preference/External/Password") != null)
                    {
                        if (node.SelectSingleNode("//preference/External/Password").InnerText != "")
                        {
                            txtP1Password.Attributes.Add("value", node.SelectSingleNode("//preference/External/Password").InnerText);
                            txtP2Password.Attributes.Add("value", node.SelectSingleNode("//preference/External/Password").InnerText);
                        }
                    }

                    if (node.SelectSingleNode("//preference/External/TimeoutValue") != null) //FB 2363K
                    {
                        if (node.SelectSingleNode("//preference/External/TimeoutValue").InnerText != "")
                            txtTimeoutValue.Text = node.SelectSingleNode("//preference/External/TimeoutValue").InnerText;
                    }

                    if (txtTimeoutValue.Text.Trim() == "")
                        txtTimeoutValue.Text = "6000";

                    //UserReportSettings
                    if (node.SelectSingleNode("//preference/External/MailusrRptSetting/CustomerName") != null)
                        txtUsrRptCustmName.Text = node.SelectSingleNode("//preference/External/MailusrRptSetting/CustomerName").InnerText.Trim();

                    lstUsrRptDeliverType.ClearSelection();
                    if (node.SelectSingleNode("//preference/External/MailusrRptSetting/DeliveryType") != null)
                        lstUsrRptDeliverType.SelectedValue = node.SelectSingleNode("//preference/External/MailusrRptSetting/DeliveryType").InnerText.Trim();

                   if (node.SelectSingleNode("//preference/External/MailusrRptSetting/RptDestination") != null)
                       txtUsrRptDestination.Text = node.SelectSingleNode("//preference/External/MailusrRptSetting/RptDestination").InnerText.Trim();

                    if ( node.SelectSingleNode("//preference/External/MailusrRptSetting/StartTime") != null)
                        txtUsrRptStartTime.Text = myVRMNet.NETFunctions.GetFormattedDate(node.SelectSingleNode("//preference/External/MailusrRptSetting/StartTime").InnerText.Trim());

                    lstUsrRptFrequencyType.ClearSelection();
                    if (node.SelectSingleNode("//preference/External/MailusrRptSetting/FrequencyType") != null)
                        lstUsrRptFrequencyType.SelectedValue = node.SelectSingleNode("//preference/External/MailusrRptSetting/FrequencyType").InnerText.Trim();

                    lstUsrRptFrequencyCount.ClearSelection();
                    if ( node.SelectSingleNode("//preference/External/MailusrRptSetting/FrequencyCount") != null)
                        lstUsrRptFrequencyCount.SelectedValue = node.SelectSingleNode("//preference/External/MailusrRptSetting/FrequencyCount").InnerText.Trim();

                    if (node.SelectSingleNode("//preference/External/MailusrRptSetting/SentTime") != null)
                        txtUsrRptSentTime.Text = myVRMNet.NETFunctions.GetFormattedDate(node.SelectSingleNode("//preference/External/MailusrRptSetting/SentTime").InnerText.Trim());

                    hdnESMailUsrSent.Value = "0";
                    if (node.SelectSingleNode("//preference/External/MailusrRptSetting/Sent") != null)
                        hdnESMailUsrSent.Value = node.SelectSingleNode("//preference/External/MailusrRptSetting/Sent").InnerText.Trim();

                }
                DateTime startTime = new DateTime(DateTime.Now.Year,DateTime.Now.Month,1); //FB 2563
                if (txtUsrRptStartTime.Text.Trim() == "")
                    txtUsrRptStartTime.Text = startTime.ToString("MM/dd/yyyy");
                //FB 2363 - End


                if (!File.Exists(companyinfofile))
                {
                    FileInfo fs = new FileInfo(companyinfofile);
                    fs.Create(); //.Create(companyinfofile, 2048, FileOptions.Asynchronous);
                }
                else
                {
                    using (StreamReader sr = new StreamReader(companyinfofile))
                        CompanyInfo.Text = sr.ReadToEnd();
                }
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                errLabel.Text = "BindData: " + ex.StackTrace;
            }
        }


        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            InitializeUIComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            //this.Load += new System.EventHandler(this.Page_Load);

        }

        private void InitializeUIComponent()
        {

        }
        #endregion


        #region SyncNow
        /// <summary>
        /// SyncNow
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SyncNow(object sender, EventArgs e)
        {
            string inxml = "<login><userID>" + Session["userID"].ToString() + "</userID></login>";
            string outxml = obj.CallCOM2("SyncLdapNow", inxml, Application["RTC_ConfigPath"].ToString());
            if (outxml.IndexOf("<error>") >= 0)
                Session.Add("errMsg", obj.ShowErrorMessage(outxml));
            else
                //Session.Add("errMsg", "Operation Successful!");
                Response.Redirect("SuperAdministrator.aspx?m=1");
        }
        #endregion

        #region btn1_Click
        protected void btn1_Click(object sender, EventArgs e)
        {
            btnSubmit_Click(sender, e);
        }
        #endregion

        #region btnChangeUIDesign_Click
        protected void btnChangeUIDesign_Click(object sender, EventArgs e)
        {
            Response.Redirect("UISettings.aspx");
        }
        #endregion

        #region TestLDAPConnection

        protected void TestLDAPConnection(Object sender, EventArgs e)
        {
            try
            {
                String inXML = "";
                inXML += "<login>";
                inXML += "<userID>" + Session["userID"].ToString() + "</userID>";
                inXML += "<AccountLogin>" + txtLDAPAccountLogin.Text + "</AccountLogin>";
                inXML += "<AccountPwd>" + txtLDAPAccountPassword1.Text + "</AccountPwd>";
                inXML += "<ServerAddress>" + txtLDAPServerAddress.Text + "</ServerAddress>";
                inXML += "<Port>" + txtLDAPServerPort.Text + "</Port>";
                inXML += "<ConnectionTimeout>" + lstLDAPConnectionTimeout.SelectedValue + "</ConnectionTimeout>";
                inXML += "<LoginKey>" + txtLDAPLoginKey.Text + "</LoginKey>";
                inXML += "<SearchFilter>" + txtLDAPSearchFilter.Text.Replace("&", "&amp;").Trim() + "</SearchFilter>";
                inXML += "<LDAPPrefix>" + txtLDAPPrefix.Text + "</LDAPPrefix>";
                inXML += "<Days>";
                String strTemp = "";
                foreach (ListItem li in chkLstDays.Items)
                    if (li.Selected)
                        strTemp += li.Value + ",";
                if (strTemp.Length > 0)
                    strTemp = strTemp.Substring(0, strTemp.Length - 1);
                inXML += strTemp.Trim();
                inXML += "</Days>";
                inXML += "<SyncTime>" + DateTime.Parse(lstLDAPScheduleTime.Text).ToString("hh:mm tt") + "</SyncTime>";
                inXML += "</login>";
                log.Trace(inXML);
                String outXML = obj.CallCOM2("TestLDAPConnection", inXML, Application["RTC_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
                else
                //    Response.Redirect("SuperAdministrator.aspx?m=1");
                {
                    errLabel.Text = obj.GetTranslatedText("Operation Successful!");//FB 1830 - Translation
                    errLabel.Visible = true;
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        #region TestMailServerConnection
        /// <summary>
        /// TestMailServerConnection
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void TestMailServerConnection(Object sender, EventArgs e) //fogbugz case 393 Saima
        {
            try
            {
                String inXML = "";
                inXML += "<login>";
                inXML += "<userID>" + Session["userID"].ToString() + "</userID>";
                inXML += "<AccountLogin>" + txtMailServerLogin.Text.Trim() + "</AccountLogin>";
                inXML += "<AccountPwd>" + txtMSPassword1.Text.Trim() + "</AccountPwd>";
                inXML += "<ServerAddress>" + txtServerAddress.Text.Trim() + "</ServerAddress>";
                inXML += "<CompanyEmail>" + txtSystemEmail.Text.Trim() + "</CompanyEmail>";
                inXML += "<DisplayName>" + txtDisplayName.Text.Trim() + "</DisplayName>";
                inXML += "<ServerPort>" + txtServerPort.Text.Trim() + "</ServerPort>";
                inXML += "<SiteURL>" + txtWebsiteURL.Text + "</SiteURL>"; // FB Case 269 Saima
                inXML += "</login>";
                log.Trace(inXML);

                String outXML = obj.CallCOM2("TestMailConnection", inXML, Application["RTC_ConfigPath"].ToString());

                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
                else
                //    Response.Redirect("SuperAdministrator.aspx?m=1");
                {
                    errLabel.Text = obj.GetTranslatedText("Operation Successful!");//FB 1830 - Translation
                    errLabel.Visible = true;
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        protected void UpdateTimezones(Object sender, EventArgs e)
        {
            try
            {
                String selTZ = "";
                obj.GetAllTimezones(PreferTimezone, ref selTZ);
            }
            catch (Exception ex)
            {
                log.Trace("UpdateTimezones: " + ex.StackTrace + " : " + ex.Message);
            }
        }

        //Commented for FB 1849
        /*#region btnChgOrg_Click
        protected void btnChgOrg_Click(object sender, EventArgs e)
        {
            try
            {
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();

                if (!Int32.TryParse(DrpOrganization.SelectedValue, out orgid))
                    throw new Exception("Invalid Organization");

                obj.SetOrgSession(orgid);

                if(Session["organizationID"] != null) //FB 1662
                {
                    Int32.TryParse(Session["organizationID"].ToString(), out orgid);

                }
                DrpOrganization.ClearSelection();

                if (DrpOrganization.Items.FindByValue(orgid.ToString()) != null)
                    DrpOrganization.Items.FindByValue(orgid.ToString()).Selected = true;

                //Code changed for FB 1662 - start
                btnOrg.Disabled = true;
                if (orgid == myVRMNet.NETFunctions.defaultOrgID)
                {
                    btnOrg.Disabled = false;
                }
                Session.Remove("EndpointXML"); //FB 1552
                //Code changed for FB 1662 - end

                //Code changed for FB 1969 - start
                btnuser.Disabled = true;
                if (orgid == myVRMNet.NETFunctions.defaultOrgID)
                {
                    btnuser.Disabled = false;
                }
                //Code changed for FB 1969 - end
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                errLabel.Text = ex.Message;
                log.Trace(ex.Message);
            }
        }
        #endregion*/

        #region Bind Processor

        public void BindProcessorDetails()
        {

            ManagementObjectSearcher objCS;
            String cpuID = "";
            String macID = "";
            XmlDocument docs = null;

            try
            {
                objCS = new ManagementObjectSearcher("SELECT * FROM Win32_Processor");

                foreach (ManagementObject objmgmt in objCS.Get())
                {
                    cpuID += "," + objmgmt.Properties["ProcessorID"].Value.ToString();
                }

                objCS = null;

                objCS = new ManagementObjectSearcher("SELECT * FROM Win32_NetworkAdapter");

                foreach (ManagementObject objmgmt in objCS.Get())
                {
                    if (objmgmt.Properties["AdapterTypeID"].Value != null)//Ethernet 802.3
                    {
                        if (objmgmt.Properties["AdapterTypeID"].Value.ToString() == "0")
                        {
                            macID += "," + objmgmt.Properties["MACAddress"].Value.ToString();
                        }
                    }
                }

                String inxmls = "<System><Cipher>" + cpuID + "||" + macID + "</Cipher></System>";

                string outXML = obj.CallMyVRMServer("GetEncrpytedText", inxmls, Application["MyVRMServer_ConfigPath"].ToString());

                if (outXML.IndexOf("<error>") < 0)
                {
                    docs = new XmlDocument();
                    docs.LoadXml(outXML);
                    XmlNode nde = docs.SelectSingleNode("System/Cipher");
                    if (nde != null)
                        EncrypTxt.Text = nde.InnerXml;
                }
                else
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                }

            }
            catch (Exception ex)
            {
                errLabel.Text = ex.StackTrace;
                errLabel.Visible = true;

            }
        }


        #endregion

        #region GetActivation

        public String GetActivation(String configpth)
        {

            ManagementObjectSearcher objCS;
            String cpuID = "";
            String macID = "";
            XmlDocument docs = null;
            String loctxt = "";
            DateTime deactDate = DateTime.MinValue;
            String ret = "";

            try
            {
                if (configpth != "")
                {
                    configpth = configpth.Replace("VRMRTCConfig.xml", "");

                    obj = new myVRMNet.NETFunctions();

                    objCS = new ManagementObjectSearcher("SELECT * FROM Win32_Processor");

                    foreach (ManagementObject objmgmt in objCS.Get())
                    {
                        cpuID += "," + objmgmt.Properties["ProcessorID"].Value.ToString();

                    }

                    objCS = null;

                    objCS = new ManagementObjectSearcher("SELECT * FROM Win32_NetworkAdapter");

                    foreach (ManagementObject objmgmt in objCS.Get())
                    {
                        if (objmgmt.Properties["AdapterTypeID"].Value != null)//Ethernet 802.3
                        {
                            if (objmgmt.Properties["AdapterTypeID"].Value.ToString() == "0")
                            {
                                macID += "," + objmgmt.Properties["MACAddress"].Value.ToString();
                            }
                        }
                    }

                    String inxmls = "<System><CPUID>" + cpuID + "</CPUID><MACID>" + macID + "</MACID></System>";

                    string outXML = obj.CallMyVRMServer("GetActivation", inxmls, configpth);


                    if (outXML.IndexOf("<error>") < 0)
                    {
                        docs = new XmlDocument();
                        docs.LoadXml(outXML);
                        XmlNode nde = docs.SelectSingleNode("System/Locked");
                        if (nde != null)
                            loctxt = nde.InnerXml;

                        nde = docs.SelectSingleNode("System/Activated");
                        if (nde != null)
                        {
                            if (nde.InnerText == "1")
                            {
                                ActStatus.Text = obj.GetTranslatedText("Activated"); //FB 2272
                                ActStatus.ForeColor = System.Drawing.Color.DarkGreen;
                                ExportTD.Attributes.Add("style", "display:none");
                                ActivationTR.Attributes.Add("style", "display:none");

                            }
                            else if (nde.InnerText == obj.GetTranslatedText("Demo")) //FB 2272Doubt
                            {
                                ActStatus.Text = obj.GetTranslatedText("Demo");
                                ActStatus.ForeColor = System.Drawing.Color.Red;
                                ExportTD.Attributes.Add("style", "display:none");
                                ActivationTR.Attributes.Add("style", "display:none");
                            }
                            else
                            {
                                ActStatus.Text = obj.GetTranslatedText("Deactivated"); //FB 2272
                                ActStatus.ForeColor = System.Drawing.Color.Red;
                                ExportTD.Attributes.Add("style", "display:block");
                                ActivationTR.Attributes.Add("style", "display:block");
                            }
                        }

                        nde = docs.SelectSingleNode("System/DeactivateDate");
                        if (nde != null)
                        {
                            DateTime.TryParse(nde.InnerText, out deactDate);

                            if (deactDate != DateTime.MinValue)
                                DeactLbl.Text = deactDate.ToString(dtFormatType) + " " + deactDate.ToString(tformat);
                        }

                    }

                    if (loctxt == "1")
                    {
                        string lckOutXML = obj.CallMyVRMServer("BlockAllUsers", "<Lock>1</Lock>", configpth);

                        if (lckOutXML == "<success>1</success>")
                            ret = "success";
                    }
                    else if (loctxt == "0")
                    {

                        string unlckOutXML = obj.CallMyVRMServer("UnBlockAllUsers", "<Lock>1</Lock>", configpth);

                        if (unlckOutXML == "<success>1</success>")
                            ret = "success";

                    }
                }

            }
            catch (Exception ex)
            {
                ret = ex.ToString();

            }

            return ret;
        }

        #endregion

        #region btnExportTxt_Click

        protected void btnExportTxt_Click(object sender, EventArgs e)
        {
            try
            {
                BindProcessorDetails();

                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=LicenseActivationText.txt");
                Response.Charset = "";

                StringBuilder sb = new StringBuilder(EncrypTxt.Text);
                StringWriter sw = new StringWriter(sb);
                Response.Output.Write(sw.ToString());
                Response.Flush();
                Response.End();
                sw.Close();
                sw.Dispose();
                sb = null;
            }
            catch (Exception ex)
            { }

        }

        #endregion

        //site logo starts here...
        #region UploadImages
        /// <summary>
        /// UploadImages
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected bool UploadImages()
        {
            try
            {
                String fName;
                HttpPostedFile myFile;
                int nFileLen;
                byte[] myData = null;
                String errMsg = "";
                String fPath = "";
                string fileExtn = "";
                fPath = Request.Url.ToString().Substring(0, Request.Url.ToString().IndexOf("/" + language + "/") + 3) + "/Upload/";//FB 1830
                if (!fleMap1.Value.Equals(""))
                {
                    fName = Path.GetFileName(fleMap1.Value);
                    myFile = fleMap1.PostedFile;
                    nFileLen = myFile.ContentLength;
                    fName = "SiteLogo.jpg";
                    if (fName != "")
                        fileExtn = fName.Substring(fName.LastIndexOf(".") + 1);

                    myData = new byte[nFileLen];
                    myFile.InputStream.Read(myData, 0, nFileLen);
                    //if (nFileLen <= 100000) //FB 2407
                    if (nFileLen <= 10000000) //10MB
                    {

                        hdnUploadMap1.Text = fName;
                        imageUtilObj = new myVRMNet.ImageUtil();
                        MemoryStream ms1 = new MemoryStream(myData, 0, myData.Length);
                        ms1.Write(myData, 0, myData.Length);
                        System.Drawing.Image SiteLogo = (System.Drawing.Image)System.Drawing.Image.FromStream(ms1);
                        string SiteWidth = SiteLogo.Width.ToString();
                        string SiteHeight = SiteLogo.Height.ToString();
                        int width = 0;
                        int height = 0;
                        int.TryParse(SiteWidth, out width);
                        int.TryParse(SiteHeight, out height);
                        //if (width > 122 || height > 44) //FB 2407
                        if (width > 244 || height > 88)
                        {
                            errMsg = obj.GetTranslatedText("Site Logo  exceeds the Resolution Limit");
                        }
                        else
                        {
                            Map1ImageDt.Value = "";
                            Map1ImageDt.Value = imageUtilObj.ConvertByteArrToBase64(myData);

                            if (!Directory.Exists(HttpContext.Current.Request.MapPath("..").ToString() + "\\image\\" + "\\company-logo"))//FB 1830 - Translation Menu
                            {
                                Directory.CreateDirectory(HttpContext.Current.Request.MapPath("..").ToString() + "\\image\\" + "\\company-logo");//FB 1830 - Translation Menu
                            }
                            imageUtilObj.WriteToFile(HttpContext.Current.Request.MapPath("..").ToString() + "\\image\\" + "\\company-logo\\" + fName, ref myData);//FB 1830 - Translation Menu
                        }
                    }
                    else
                        errMsg = obj.GetTranslatedText("Site Logo attachment is greater than 10MB. File has not been uploaded.");//FB 2407-100KB

                }

                if (errMsg != "")
                {
                    errLabel.Text = errMsg;
                    errLabel.Visible = true;
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                log.Trace("Error in uploading files." + ex.StackTrace + " : " + ex.Message);
                errLabel.Visible = true;
                return false;
            }
        }
        #endregion

        #region UploadStdBanner
        /// <summary>
        /// UploadImages
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected bool UploadStdBanner()
        {
            try
            {
                String fName;
                HttpPostedFile myFile;
                int nFileLen;
                byte[] myData = null;
                String errMsg = "";
                String fPath = "";
                string fileExtn = "";
                fPath = Request.Url.ToString().Substring(0, Request.Url.ToString().IndexOf("/" + language + "/") + 3) + "/Upload/"; //FB 1830
                if (!fleMap2.Value.Equals(""))
                {
                    fName = Path.GetFileName(fleMap2.Value);
                    myFile = fleMap2.PostedFile;
                    nFileLen = myFile.ContentLength;
                    //fName = "StdBanner.jpg"; //FB 1633
                    
                    if (fName != "")
                        fileExtn = fName.Substring(fName.LastIndexOf(".") + 1);

                    myData = new byte[nFileLen];
                    myFile.InputStream.Read(myData, 0, nFileLen);

                    if (nFileLen <= 100000)
                    {

                        hdnUploadMap2.Text = fName;
                        imageUtilObj = new myVRMNet.ImageUtil();

                        MemoryStream ms1 = new MemoryStream(myData, 0, myData.Length);
                        ms1.Write(myData, 0, myData.Length);
                        System.Drawing.Image Satbanner = (System.Drawing.Image)System.Drawing.Image.FromStream(ms1);
                        string Std = Satbanner.Width.ToString();
                        string shd = Satbanner.Height.ToString();
                        int swidth = 0;
                        int sheight = 0;
                        int.TryParse(Std, out swidth);
                        int.TryParse(shd, out sheight);
                        if (swidth > 1024 || sheight > 72)
                        {
                            errMsg = obj.GetTranslatedText("Standard Resolution Banner exceeds the Resolution Limit");

                        }
                        else
                        {


                            Map2ImageDt.Value = "";
                            Map2ImageDt.Value = imageUtilObj.ConvertByteArrToBase64(myData);
                            Map3ImageDt.Value = Map2ImageDt.Value; //FB 1633
                            if (!Directory.Exists(HttpContext.Current.Request.MapPath("..").ToString() + "\\image\\" + "\\company-logo"))//FB 1830 - Translation Menu
                            {
                                Directory.CreateDirectory(HttpContext.Current.Request.MapPath("..").ToString() + "\\image\\" + "\\company-logo");//FB 1830 - Translation Menu
                            }
                            imageUtilObj.WriteToFile(Session["SiteStdBanner"].ToString(), ref myData);
                            imageUtilObj.WriteToFile(Session["SiteHighBanner"].ToString(), ref myData); //FB 1633
                        }



                    }
                    else
                        errMsg = obj.GetTranslatedText("Standard Resolution Banner attachment is greater than 100kB. File has not been uploaded.");


                }

                if (errMsg != "")
                {
                    errLabel.Text = errMsg;
                    errLabel.Visible = true;
                    return false;

                }
                return true;
            }
            catch (Exception ex)
            {
                log.Trace("Error in uploading files." + ex.StackTrace + " : " + ex.Message);
                errLabel.Visible = true;
                return false;
            }
        }
        #endregion

        #region UploadHighBanner
        /// <summary>
        /// UploadImages
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected bool UploadHighBanner()
        {
            try
            {
                String fName;
                HttpPostedFile myFile;
                int nFileLen;
                byte[] myData = null;
                String errMsg = "";
                String fPath = "";
                string fileExtn = "";
                fPath = Request.Url.ToString().Substring(0, Request.Url.ToString().IndexOf("/" + language + "/") + 3) + "/Upload/"; //FB 1830
                if (!fleMap3.Value.Equals(""))
                {
                    fName = Path.GetFileName(fleMap3.Value);
                    myFile = fleMap3.PostedFile;
                    nFileLen = myFile.ContentLength;
                    fName = "HighBanner.jpg";
                    if (fName != "")
                        fileExtn = fName.Substring(fName.LastIndexOf(".") + 1);

                    myData = new byte[nFileLen];
                    myFile.InputStream.Read(myData, 0, nFileLen);
                    if (nFileLen <= 100000)
                    {

                        hdnUploadMap3.Text = fName;
                        imageUtilObj = new myVRMNet.ImageUtil();
                        MemoryStream ms1 = new MemoryStream(myData, 0, myData.Length);
                        ms1.Write(myData, 0, myData.Length);
                        System.Drawing.Image Highres = (System.Drawing.Image)System.Drawing.Image.FromStream(ms1);
                        string Highwidth = Highres.Width.ToString();
                        string HighHeight = Highres.Height.ToString();
                        int hwidth = 0;
                        int hheight = 0;
                        int.TryParse(Highwidth, out hwidth);
                        int.TryParse(HighHeight, out hheight);
                        if (hwidth > 1600 || hheight > 72)
                        {
                            errMsg = obj.GetTranslatedText("High Resolution Banner exceeds the Resolution Limit");

                        }
                        else
                        {

                            Map3ImageDt.Value = "";
                            Map3ImageDt.Value = imageUtilObj.ConvertByteArrToBase64(myData);
                            if (!Directory.Exists(HttpContext.Current.Request.MapPath(".").ToString() + "\\image\\" + "\\company-logo"))
                            {
                                Directory.CreateDirectory(HttpContext.Current.Request.MapPath(".").ToString() + "\\image\\" + "\\company-logo");
                            }
                            imageUtilObj.WriteToFile(HttpContext.Current.Request.MapPath(".").ToString() + "\\image\\" + "\\company-logo\\" + fName, ref myData);



                        }

                    }
                    else
                        errMsg = obj.GetTranslatedText("Site Logo attachment is greater than 100kB. File has not been uploaded.");
                }

                if (errMsg != "")
                {
                    errLabel.Text = errMsg;
                    errLabel.Visible = true;
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                log.Trace("Error in uploading files." + ex.StackTrace + " : " + ex.Message);
                errLabel.Visible = true;
                return false;
            }
        }
        #endregion

        #region Display Images
        /// <summary>
        /// Display Room Other Images - Added for Image Project Edit Mode
        /// </summary>
        /// <param name="imgNode"></param>
        private void DisplayImages(XmlNode imgNode)
        {
            string imgString = "";
            try
            {
                imgString = "";
                fName = "";

                imageUtilObj = new myVRMNet.ImageUtil();
                if (imgNode != null)
                    imgString = imgNode.InnerText;
                Map1ImageDt.Value = imgString;
                imgArray = imageUtilObj.ConvertBase64ToByteArray(imgString);

                LoadImage();

            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }

        private void LoadImage()
        {
            try
            {

                fileext = "jpg";

                if (imgArray == null)
                {
                    hdnUploadMap1.Text = "";
                    Map1ImageDt.Value = "";

                    fleMap1.Visible = true;

                    btnUploadImages.Visible = true;
                }
                if (imgArray.Length <= 0)
                {
                    hdnUploadMap1.Text = "";
                    Map1ImageDt.Value = "";

                    fleMap1.Visible = true;

                    btnUploadImages.Visible = true;
                }




            }

            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        #region DisplayStdBanner
        public void DisplayStdBanner(XmlNode imgNode)
        {
            string imgString = "";
            try
            {
                imgString = "";
                fName = "";

                imageUtilObj = new myVRMNet.ImageUtil();
                if (imgNode != null)
                    imgString = imgNode.InnerText;
                Map2ImageDt.Value = imgString;
                imgArray = imageUtilObj.ConvertBase64ToByteArray(imgString);

                LoadStdBanner();

            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        private void LoadStdBanner()
        {
            try
            {

                fileext = "jpg";

                if (imgArray == null)
                {
                    hdnUploadMap2.Text = "";
                    Map2ImageDt.Value = "";
                    fleMap2.Visible = true;
                }
                if (imgArray.Length <= 0)
                {
                    hdnUploadMap2.Text = "";
                    Map2ImageDt.Value = "";
                    fleMap2.Visible = true;
                }



            }

            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        #region DisplayHighBanner
        public void DisplayHighBanner(XmlNode imgNode)
        {
            string imgString = "";
            try
            {
                imgString = "";
                fName = "";

                imageUtilObj = new myVRMNet.ImageUtil();
                if (imgNode != null)
                    imgString = imgNode.InnerText;
                Map3ImageDt.Value = imgString;
                imgArray = imageUtilObj.ConvertBase64ToByteArray(imgString);

                LoadHighBanner();

            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        private void LoadHighBanner()
        {
            try
            {

                fileext = "jpg";

                if (imgArray == null)
                {
                    hdnUploadMap3.Text = "";
                    Map3ImageDt.Value = "";

                    fleMap3.Visible = true;
                }
                if (imgArray.Length <= 0)
                {
                    hdnUploadMap3.Text = "";
                    Map3ImageDt.Value = "";

                    fleMap3.Visible = true;
                }



            }

            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        #region RemoveFile
        protected void RemoveFile(Object sender, CommandEventArgs e)
        {
            try
            {
                int imageId = 0;
                Label lblTemp = new Label();
                HtmlInputFile inTemp = new HtmlInputFile();
                Label lblFname = new Label();
                Label lblHdnName = new Label();
                ImageButton btnTemp = new ImageButton();
                HtmlInputHidden tempHdn = new HtmlInputHidden();
                myVRMWebControls.ImageControl tempImg = new myVRMWebControls.ImageControl();
                lblTemp = hdnUploadMap1;
                inTemp = fleMap1;

                lblHdnName = hdnUploadMap1;


                tempHdn = Map1ImageDt;

                tempImg.Dispose();
                tempImg.Visible = false;
                inTemp.Visible = true;
                lblFname.Visible = false;
                lblFname.Text = "";
                lblHdnName.Visible = false;
                lblHdnName.Text = "";
                btnTemp.Visible = false;
                tempHdn.Value = "";
                //  btnUploadImages.Visible = true;
                DeleteImage();
                this.errLabel.Text = obj.GetTranslatedText("Operation Successful! Please logout and re-login to see the changes.");//FB 1830 - Translation
                errLabel.Visible = true;
                //FB 1633 start
                if (Map1ImageDt.Value == "")
                {
                    byte[] myData = null;
                    Map1ImageDt.Value = imageUtilObj.ConvertImageToBase64(Session["BlankLogoPath"].ToString());
                    myData = imageUtilObj.ConvertBase64ToByteArray(Map1ImageDt.Value);
                    imageUtilObj.WriteToFile(Session["SiteLogPath"].ToString(), ref myData);
                    setImages();
                }
                //FB 1633 end
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        #region RemoveStdBanner
        protected void RemoveStdBanner(Object sender, CommandEventArgs e)
        {
            try
            {
                int imageId = 0;
                Label lblTemp = new Label();
                HtmlInputFile inTemp = new HtmlInputFile();
                Label lblFname = new Label();
                Label lblHdnName = new Label();
                ImageButton btnTemp = new ImageButton();
                HtmlInputHidden tempHdn = new HtmlInputHidden();
                myVRMWebControls.ImageControl tempImg = new myVRMWebControls.ImageControl();
                lblTemp = hdnUploadMap2;
                inTemp = fleMap2;
                //lblFname = lblUploadMap2;
                lblHdnName = hdnUploadMap2;
                //btnTemp = btnRemoveMap2;
                // tempImg = Map2ImageCtrl;
                tempHdn = Map2ImageDt;

                tempImg.Dispose();
                tempImg.Visible = false;
                inTemp.Visible = true;
                lblFname.Visible = false;
                lblFname.Text = "";
                lblHdnName.Visible = false;
                lblHdnName.Text = "";
                btnTemp.Visible = false;
                tempHdn.Value = "";
                DeleteImage();
                this.errLabel.Text = obj.GetTranslatedText("Operation Successful! Please logout and re-login to see the changes.");//FB 1830 - Translation
                errLabel.Visible = true;
                //btnUploadImages1.Visible = true;
                //FB 1633 Start
                if (Map2ImageDt.Value == "")
                {
                    byte[] myData = null;
                    Map2ImageDt.Value = imageUtilObj.ConvertImageToBase64(Session["BlankBannerPath"].ToString());
                    Map3ImageDt.Value = Map2ImageDt.Value;
                    myData = imageUtilObj.ConvertBase64ToByteArray(Map2ImageDt.Value.ToString());
                    imageUtilObj.WriteToFile(Session["SiteStdBanner"].ToString(), ref myData);
                    imageUtilObj.WriteToFile(Session["SiteHighBanner"].ToString(), ref myData);
                    setImages();
                }
                //FB 1633 end
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        #region RemoveHighBanner
        protected void RemoveHighBanner(Object sender, CommandEventArgs e)
        {
            try
            {
                int imageId = 0;
                Label lblTemp = new Label();
                HtmlInputFile inTemp = new HtmlInputFile();
                Label lblFname = new Label();
                Label lblHdnName = new Label();
                ImageButton btnTemp = new ImageButton();
                HtmlInputHidden tempHdn = new HtmlInputHidden();
                myVRMWebControls.ImageControl tempImg = new myVRMWebControls.ImageControl();
                lblTemp = hdnUploadMap3;
                inTemp = fleMap3;

                lblHdnName = hdnUploadMap3;


                tempHdn = Map3ImageDt;

                tempImg.Dispose();
                tempImg.Visible = false;
                inTemp.Visible = true;
                lblFname.Visible = false;
                lblFname.Text = "";
                lblHdnName.Visible = false;
                lblHdnName.Text = "";
                btnTemp.Visible = false;
                tempHdn.Value = "";
                DeleteImage();
                this.errLabel.Text = obj.GetTranslatedText("Operation Successful! Please logout and re-login to see the changes.");//FB 1830 - Translation
                errLabel.Visible = true;

            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        #region setImages
        /// <summary>
        /// SetImages
        /// </summary>
        /// <returns></returns>
        protected int setImages()
        {
            int imageId = 0;
            string Companymessage = "";
            if (txtCompanymessage.Text != "")
            {
                Companymessage = txtCompanymessage.Text;
            }
            try
            {
                String inXML = BuildImageINXML(); //FB 2027 - SetSuperAdmin

                log.Trace("SetImagekey Inxml: " + inXML);
                string outxml = obj.CallCommand("SetImagekey", inXML);
                log.Trace("SetImagekey Outxml: " + outxml);
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                errLabel.Text = ex.Message;
                log.Trace(ex.Message);
            }
            return imageId;

        }
        #endregion

        //New method added for FB 2027 - SetSuperAdmin
        #region BuildImageINXML
        /// <summary>
        /// BuildImageINXML
        /// </summary>
        /// <returns></returns>
        private string BuildImageINXML()
        {
            StringBuilder inXML = new StringBuilder();
            int imageId = 0;
            string Companymessage = "";
            if (txtCompanymessage.Text != "")
            {
                Companymessage = txtCompanymessage.Text;
            }
            try
            {
                inXML.Append("<SetImagekey>");
                if (Map1ImageDt.Value != "")
                    inXML.Append("<Image>" + Map1ImageDt.Value + "</Image>");
                else
                    inXML.Append("<Image></Image>");
                if (Map2ImageDt.Value != "")
                    inXML.Append("<StdBanner>" + Map2ImageDt.Value + "</StdBanner>");
                else
                    inXML.Append("<StdBanner></StdBanner>");
                if (Map3ImageDt.Value != "")
                    inXML.Append("<HighBanner>" + Map3ImageDt.Value + "</HighBanner>");
                else
                    inXML.Append("<HighBanner></HighBanner>");
                if (Companymessage != "")
                    inXML.Append("<Companymessage>" + Companymessage + "</Companymessage>");
                else
                    inXML.Append("<Companymessage></Companymessage>");

                inXML.Append("</SetImagekey>");

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return inXML.ToString();
        }
        #endregion

        #region GetImage
        protected void GetImage()
        {
            try
            {
                XmlDocument xd = new XmlDocument();
                string inXML = "";
                XmlNode xnode;
                inXML = "<GetSiteImage>";
                inXML += "<UserId>" + Session["userid"].ToString() + "</UserId>";
                inXML += "</GetSiteImage>";
                log.Trace("GetSiteImage Inxml: " + inXML);
                string outxml = obj.CallCommand("GetSiteImage", inXML);
                log.Trace("GetSiteImage Outxml: " + outxml);
                xd.LoadXml(outxml);
                if (outxml.IndexOf("<error>") >= 0)
                {
                    xnode = xd.SelectSingleNode("<error>");
                    errLabel.Text = xnode.InnerText;
                }

                else
                {
                    XmlNode node;
                    node = xd.SelectSingleNode("//GetSiteImage/Image");
                    if (node != null)
                    {
                        if (node.InnerText.Trim() != "")
                            DisplayImages(node);
                    }
                    node = xd.SelectSingleNode("//GetSiteImage/StdBanner");
                    if (node != null)
                    {
                        if (node.InnerText.Trim() != "")
                            DisplayStdBanner(node);
                    }
                    node = xd.SelectSingleNode("//GetSiteImage/HighBanner");
                    if (node != null)
                    {
                        if (node.InnerText.Trim() != "")
                            DisplayHighBanner(node);
                    }
                    node = xd.SelectSingleNode("//GetSiteImage/ImageId");
                    if (node != null)
                        hdnImageId.Text = node.InnerText;
                    node = xd.SelectSingleNode("//GetSiteImage/Companymessage");
                    if (node != null)
                        hdnCompanymessage.Text = node.InnerText;

                }
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                errLabel.Text = ex.Message;
                log.Trace(ex.Message);
            }


        }
        #endregion

        //Site Logo ends here
        #region DeleteImage
        public void DeleteImage()
        {
            if (Map1ImageDt.Value == "")
            {
                if (Directory.Exists(HttpContext.Current.Request.MapPath(".").ToString() + "\\image\\" + "\\company-logo"))
                {
                    string[] Files = Directory.GetFiles(HttpContext.Current.Request.MapPath(".").ToString() + "\\image\\" + "\\company-logo");
                    for (int i = 0; i < Files.Length; i++)
                    {
                        if (Files[i] == HttpContext.Current.Request.MapPath(".").ToString() + "\\image\\" + "\\company-logo" + "\\SiteLogo.jpg")
                        {
                            File.Delete(HttpContext.Current.Request.MapPath(".").ToString() + "\\image\\" + "\\company-logo" + "\\SiteLogo.jpg");
                        }

                    }
                }
            }
            if (Map3ImageDt.Value == "")
            {
                if (Directory.Exists(HttpContext.Current.Request.MapPath(".").ToString() + "\\image\\" + "\\company-logo"))
                {
                    string[] Files = Directory.GetFiles(HttpContext.Current.Request.MapPath(".").ToString() + "\\image\\" + "\\company-logo");
                    for (int i = 0; i < Files.Length; i++)
                    {
                        if (Files[i] == HttpContext.Current.Request.MapPath(".").ToString() + "\\image\\" + "\\company-logo" + "\\HighBanner.jpg")
                        {
                            File.Delete(HttpContext.Current.Request.MapPath(".").ToString() + "\\image\\" + "\\company-logo" + "\\HighBanner.jpg");
                        }

                    }
                }
            }
            if (Map2ImageDt.Value == "")
            {
                if (Directory.Exists(HttpContext.Current.Request.MapPath(".").ToString() + "\\image\\" + "\\company-logo"))
                {
                    string[] Files = Directory.GetFiles(HttpContext.Current.Request.MapPath(".").ToString() + "\\image\\" + "\\company-logo");
                    for (int i = 0; i < Files.Length; i++)
                    {
                        if (Files[i] == HttpContext.Current.Request.MapPath(".").ToString() + "\\image\\" + "\\company-logo" + "\\StdBanner.jpg")
                        {
                            File.Delete(HttpContext.Current.Request.MapPath(".").ToString() + "\\image\\" + "\\company-logo" + "\\StdBanner.jpg");
                        }

                    }
                }
            }

        }
        #endregion

        #region btnSubmit_Click
        /// <summary>
        /// btnSubmit_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                string imageStr = "";
                //Response.Write("here<p>" + Application["COM_ConfigPath"]);
                //errLabel.Visible = true;
                //errLabel.Text = Page.IsValid.ToString();
                if (UploadImages() && UploadStdBanner()) //FB 1633 ( && UploadHighBanner())
                {
                    try
                    {
                        using (StreamWriter sr = new StreamWriter(companyinfofile))
                        {
                            sr.Write(CompanyInfo.Text.ToString());
                        }
                        //Site Logo...
                        //FB 2027 SetSuperAdmin - start
                        imageStr = BuildImageINXML();
                        //setImages();
                        //FB 2027 SetSuperAdmin - end
                    }
                    catch (Exception ex)
                    {
                        log.Trace("CompanyInfo: " + ex.StackTrace + " : " + ex.Message);
                    }
                    //FB 2027 SetSuperAdmin - start
                    string temp = "0";
                    StringBuilder xmldoc = new StringBuilder();
                    xmldoc.Append("<superAdmin><userID>" + Session["userID"].ToString() + "</userID><preference>"); //FB 2027 SetSuperAdmin
                    xmldoc.Append("<SystemTimeZoneID>" + PreferTimezone.SelectedValue + "</SystemTimeZoneID>");//Code added for organisation module
                    xmldoc.Append("<securityKey>" + txtLicenseKey.Text.ToString() + "</securityKey>");
                    xmldoc.Append("<launchBuffer>" + txtLaunchBuffer.Text.ToString() + "</launchBuffer>");//FB 2007
                    xmldoc.Append("<StartMode>"+lstStartMode.SelectedValue+"</StartMode>"); //FB 2501
                    xmldoc.Append("<EnableLaunchBufferP2P>" + lstEnableLaunchBufferP2P.SelectedValue + "</EnableLaunchBufferP2P>");//FB 2437
                    xmldoc.Append("<emailSystem>");
                    xmldoc.Append("<companyEmail>" + txtSystemEmail.Text.Trim() + "</companyEmail>");
                    xmldoc.Append("<displayName>" + txtDisplayName.Text.Trim() + "</displayName>");
                    xmldoc.Append("<messageTemplate>" + txtMailMessage.Text.Trim() + "</messageTemplate>");
                    xmldoc.Append("<remoteServer>1</remoteServer>");
                    xmldoc.Append("<serverAddress>" + txtServerAddress.Text.Trim() + "</serverAddress>");
                    xmldoc.Append("<accountLogin>" + txtMailServerLogin.Text.Trim() + "</accountLogin>");
                    xmldoc.Append("<accountPwd>" + txtMSPassword1.Text.Trim() + "</accountPwd>");
                    xmldoc.Append("<portNo>" + txtServerPort.Text.Trim() + "</portNo>");
                    xmldoc.Append("<connectionTimeout></connectionTimeout>");
                    xmldoc.Append("<websiteURL>" + txtWebsiteURL.Text.Trim() + "</websiteURL>");
                    xmldoc.Append("<retrycount>" + txtRetryCount.Text.Trim() + "</retrycount>");//FB 2552
                    xmldoc.Append("</emailSystem>");

                    xmldoc.Append("<exchange>");
                    xmldoc.Append("<exchangeURL></exchangeURL>");
                    xmldoc.Append("<exchangeDomain></exchangeDomain>");
                    xmldoc.Append("<exchangeLogin></exchangeLogin>");
                    xmldoc.Append("<exchangePwd></exchangePwd>");
                    xmldoc.Append("</exchange>");

                    xmldoc.Append("<locationList>");
                    xmldoc.Append("<selected></selected>");
                    xmldoc.Append("<level3List></level3List>");
                    xmldoc.Append("</locationList>");

                    xmldoc.Append("<LDAP>");
                    xmldoc.Append("<serverAddress>" + txtLDAPServerAddress.Text.ToString() + "</serverAddress>");
                    xmldoc.Append("<loginName>" + txtLDAPAccountLogin.Text.ToString() + "</loginName>");
                    xmldoc.Append("<loginPassword>" + txtLDAPAccountPassword1.Text.ToString() + "</loginPassword>");
                    xmldoc.Append("<portNo>" + txtLDAPServerPort.Text.ToString() + "</portNo>");
                    xmldoc.Append("<connectionTimeout>" + lstLDAPConnectionTimeout.SelectedValue.ToString() + "</connectionTimeout>");
                    xmldoc.Append("<scheduler>");
                    xmldoc.Append("<Time>" + DateTime.Now.ToShortDateString() + " " + DateTime.Parse(lstLDAPScheduleTime.Text).ToString("hh:mm tt") + "</Time>");
                    xmldoc.Append("<Days>");

                    String strTemp = "";
                    foreach (ListItem li in chkLstDays.Items)
                        if (li.Selected)
                            strTemp += li.Value + ",";
                    if (strTemp.Length > 0)
                        strTemp = strTemp.Substring(0, strTemp.Length - 1);

                    xmldoc.Append(strTemp.Trim());
                    xmldoc.Append("</Days>");
                    xmldoc.Append("</scheduler>");
                    xmldoc.Append("<loginkey>" + txtLDAPLoginKey.Text.ToString() + "</loginkey>");
                    xmldoc.Append("<synctime>" + DateTime.Parse(lstLDAPScheduleTime.Text).ToString("hh:mm tt") + "</synctime>");
                    xmldoc.Append("<searchfilter>" + txtLDAPSearchFilter.Text.ToString().Replace("&", "&amp;") + "</searchfilter>");
                    xmldoc.Append("<LDAPPrefix>" + txtLDAPPrefix.Text.Trim() + "</LDAPPrefix>");
                    xmldoc.Append("</LDAP>");
                    xmldoc.Append(imageStr);

                    //FB 2501    Starts
                    xmldoc.Append("<EM7Connectivity>");
                    xmldoc.Append("<EM7URI>" + txtEM7URI.Text.Trim() + "</EM7URI>");
                    xmldoc.Append("<EM7Username>" + txtEM7Username.Text.Trim() + "</EM7Username>");
                    xmldoc.Append("<EM7Password>" + txtEM7ConformPassword.Text.Trim() + "</EM7Password>"); 
                    if(txtEM7Port.Text.Trim()!="")
                        xmldoc.Append("<EM7Port>"+txtEM7Port.Text.Trim()+"</EM7Port>");
                    else
                        xmldoc.Append("<EM7Port>80</EM7Port>");
                    xmldoc.Append("</EM7Connectivity>");
                    //FB 2501 EM7 Ends

                    //FB 2363 - Start
                    if (Application["External"].ToString() != "")
                    {
                        xmldoc.Append("<external>");
                        xmldoc.Append("<PartnerName>" + txtPartnerName.Text + "</PartnerName>");
                        xmldoc.Append("<PartnerEmail>" + txtPartnerEmail.Text + "</PartnerEmail>");
                        xmldoc.Append("<PartnerURL>" + txtPartnerURL.Text + "</PartnerURL>");
                        xmldoc.Append("<UserName>" + txtPUserName.Text + "</UserName>");
                        xmldoc.Append("<Password>" + txtP1Password.Text + "</Password>");
                        xmldoc.Append("<TimeoutValue>" + txtTimeoutValue.Text + "</TimeoutValue>");//FB 2363K
                        xmldoc.Append("<MailusrRptSetting>"); //shan
                        xmldoc.Append("<CustomerName>" + txtUsrRptCustmName.Text + "</CustomerName>");
                        xmldoc.Append("<DeliveryType>" + lstUsrRptDeliverType.SelectedValue + "</DeliveryType>");
                        xmldoc.Append("<RptDestination>" + txtUsrRptDestination.Text + "</RptDestination>");
                        xmldoc.Append("<StartTime>" +myVRMNet.NETFunctions.GetDefaultDate(txtUsrRptStartTime.Text.Trim()) + "</StartTime>");
                        xmldoc.Append("<SentTime>" + myVRMNet.NETFunctions.GetDefaultDate(txtUsrRptSentTime.Text.Trim()) + "</SentTime>");
                        xmldoc.Append("<FrequencyType>" + lstUsrRptFrequencyType.SelectedValue + "</FrequencyType>");
                        xmldoc.Append("<FrequencyCount>" + lstUsrRptFrequencyCount.SelectedValue + "</FrequencyCount>");
                        xmldoc.Append("<Sent>" + hdnESMailUsrSent.Value + "</Sent>");                        
                        xmldoc.Append("<Type>S</Type>");
                        xmldoc.Append("</MailusrRptSetting>");
                        xmldoc.Append("</external>");
                    }
                    //FB 2363 - End
                    //FB 2392 start //FB 2594 Starts
                    if (Session["EnablePublicRooms"].ToString() == "1") 
                    {
                        xmldoc.Append("<WhyGo>");
                        xmldoc.Append("<WhyGoURL>" + txtWhyGoURL.Text + "</WhyGoURL>");
                        xmldoc.Append("<WhyGoUserName>" + txtWhygoUsr.Text + "</WhyGoUserName>");
                        xmldoc.Append("<WhyGoPassword>" + txtWhygoPwd.Text + "</WhyGoPassword>");
                        xmldoc.Append("</WhyGo>");//FB 2392 end
                    }
                    else
                    {
                        xmldoc.Append("<WhyGo>");
                        xmldoc.Append("<WhyGoURL></WhyGoURL>");
                        xmldoc.Append("<WhyGoUserName></WhyGoUserName>");
                        xmldoc.Append("<WhyGoPassword></WhyGoPassword>");
                        xmldoc.Append("</WhyGo>");
                    }
                    //FB 2594 Ends
                    xmldoc.Append("</preference></superAdmin>");
                    //Response.Write(obj.Transfer(xmldoc));?
                    //string outxml = obj.CallCOM("SetSuperAdmin", xmldoc, Application["COM_ConfigPath"].ToString());
                    string outxml = obj.CallCommand("SetSuperAdmin", xmldoc.ToString());
                    //FB 2027 SetSuperAdmin - end

                    //Response.Write("<BR>" + obj.Transfer(outxml));
                    //Response.End();
                    if (outxml.Length > 0)
                        if (outxml.IndexOf("<error>") >= 0)
                        {
                            errLabel.Visible = true;
                            /* *** Code added for FB 1425 QA Bug -Start *** */

                            if (Application["Client"].ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.MOJ))
                            {
                                errLabel.Text = obj.ShowErrorMessage(outxml);
                                if (errLabel.Text.ToString().ToUpper().Contains("CONFERENCES"))
                                    errLabel.Text = errLabel.Text.ToString().Replace("conferences", "hearings");
                            }
                            else
                                /* *** Code added for FB 1425 QA Bug -End *** */
                                errLabel.Text = obj.ShowErrorMessage(outxml);
                        }
                        else
                        {
                            //Custom attribute fixes
                            //Session.Add("errMsg", "Operation Successful!");
                            Session.Add("StartMode", lstStartMode.SelectedValue);  //FB 2501
                            Response.Redirect("SuperAdministrator.aspx?m=1");
                        }
                }
            }
            catch (Exception ex)
            {
                /* *** Code added for FB 1425 QA Bug -Start *** */

                if (Application["Client"].ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.MOJ))
                {
                    errLabel.Text = ex.StackTrace;
                    if (errLabel.Text.ToString().ToUpper().Contains("CONFERENCES"))
                    {
                        errLabel.Text = errLabel.Text.ToString().Replace("conferences", "hearings");
                        errLabel.Visible = true;
                    }
                }
                else
                {
                    /* *** Code added for FB 1425 QA Bug -End *** */
                    errLabel.Text = ex.StackTrace;
                    errLabel.Visible = true;
                } /* *** Code added for FB 1425 QA Bug **/
            }
            //Response.End();
        }
        #endregion

        //FB 2392
        #region PollWhyGo
        /// <summary>
        /// PollWhyGo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void PollWhyGoNow(object sender, EventArgs e)
        {
            try
            {
                string inXML = "", outXML = "";

                outXML = obj.CallCommand("GetLocationUpdate", inXML);
                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Visible = true;
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                }
                else
                {
                    errLabel.Text = obj.GetTranslatedText("Operation Successful!");
                    String roomxmlPath = Directory.GetParent(HttpContext.Current.Request.MapPath(".").ToString()) + "\\en" + "\\" + Session["RoomXmlPath"].ToString();//FB 2594
                    if (File.Exists(roomxmlPath))
                        File.Delete(roomxmlPath);
                    errLabel.Visible = true;
                }
                
            }
            catch (Exception ex)
            {
                log.Trace("" + ex.StackTrace);
                errLabel.Text = ex.Message;
                errLabel.Visible = true;
            }
        }
        #endregion

        //FB 2594 Starts
        #region DeletePublicRooms
        /// <summary>
        /// DeletePublicRooms
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DeletePublicRooms(object sender, EventArgs e)
        {
            try
            {
                StringBuilder inXML = new StringBuilder();
                String outXML = "";


                inXML.Append("<DeltePublicRoomEP>");
                inXML.Append(obj.OrgXMLElement());
                inXML.Append("<userid>" + Session["userID"].ToString() + "</userid>");
                inXML.Append("</DeltePublicRoomEP>");

                outXML = obj.CallMyVRMServer("DeltePublicRoomEP", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());

                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
                else
                {
                    errLabel.Text = obj.GetTranslatedText("Operation Successful!");
                    String roomxmlPath = Directory.GetParent(HttpContext.Current.Request.MapPath(".").ToString()) + "\\en" + "\\" + Session["RoomXmlPath"].ToString();
                    if (File.Exists(roomxmlPath))
                        File.Delete(roomxmlPath);
                    errLabel.Visible = true;
                }
            }
            catch (Exception ex)
            {
                log.Trace("DeletePublicRooms :" + ex.Message);
            }
        }
        #endregion
        //FB 2594 Ends

    }
}

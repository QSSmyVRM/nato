 using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class en_outlookemaillist2main : System.Web.UI.Page
{
    #region Private and Public variables

    protected string titlealign = "";
    protected string alphabet = "";
    #endregion

    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {

            if (Request.QueryString["outlookEmails"] != null)
                Session["outLookUpEmail"] = Request.QueryString["outlookEmails"];
            else
                Session["outlookEmails"] = Request.Form["outlookEmails"];

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    #endregion
}

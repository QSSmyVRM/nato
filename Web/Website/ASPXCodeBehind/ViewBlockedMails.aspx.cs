using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml;
using System.IO;


namespace ns_MyVRM
{
    public partial class ViewBlockedMails : System.Web.UI.Page
    {
        myVRMNet.NETFunctions obj;

        #region Protected Data Members

        protected System.Web.UI.WebControls.Label errLabel;
        protected System.Web.UI.WebControls.Table tblNoBlockMail;
        protected System.Web.UI.WebControls.DataGrid dgBlockEmail;
        protected System.Web.UI.WebControls.Button btnCreateCusAtt;
        protected System.Web.UI.HtmlControls.HtmlTableCell ConfMessage;
        protected System.Web.UI.WebControls.Button CustomTrigger;
        protected System.Web.UI.WebControls.Table ConfListTbl;        
        protected System.Web.UI.HtmlControls.HtmlInputHidden HdnCustOptID;
        protected System.Web.UI.WebControls.Button BtnEditCA;
        protected System.Web.UI.WebControls.Button BtnDeleteAll;

        protected System.Web.UI.WebControls.ImageButton btnExcel;

        string confListHeader = "";
        XmlNodeList confList = null;
        ns_Logger.Logger log;
        MyVRMNet.Util utilObj;//FB 2236
        
        protected string customAttrID = "";
        ArrayList colNames = null;

        #endregion

        public ViewBlockedMails()
        {
            obj = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger();
            utilObj = new MyVRMNet.Util(); //FB 2236
        }

        #region  Page Load

        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                errLabel.Text = "";
                if (Request.QueryString["m"] != null)
                    if (Request.QueryString["m"].ToString().Equals("1"))
                    {
                        errLabel.Text = obj.GetTranslatedText("Operation Successful!");//FB 1830 - Translation
                        errLabel.Visible = true;
                    }

                if (!IsPostBack)
                    BindData();
               
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                errLabel.Text = "PageLoad: " + ex.StackTrace;
            }
        }
        #endregion

        #region BindData 
        private void BindData()
        {
            String command = "GetUserEmails";
            try
            {
                String inXML = "<login>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID></login>";

                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                
                String outXML = "";

                if (Request.QueryString["tp"] != null)
                {
                    if (Request.QueryString["tp"].ToLower().Equals("o"))
                        command = "GetOrgEmails";

                }

                outXML = obj.CallMyVRMServer(command, inXML, Application["MyVRMServer_ConfigPath"].ToString());                
                
                if (outXML.IndexOf("<error>") < 0)
                {
                    XmlDocument xmldoc = new XmlDocument();
                    outXML = outXML.Replace("&", "&amp;");
                    xmldoc.LoadXml(outXML);

                    CreateDtColumnNames();
                    XmlNodeList nodes = null;
                    DataTable dtable = obj.LoadDataTable(nodes, colNames);

                    nodes = xmldoc.SelectNodes("//Emails/Email");
                    Int32 rowCnt = 0;
                    DataRow dr;
                    foreach (XmlNode node in nodes)
                    {
                        dr = dtable.NewRow();
                        rowCnt = rowCnt + 1;
                        dr["UUID"] = node.SelectSingleNode("uuid").InnerText;
                        dr["RowID"] = rowCnt.ToString();
                        dr["From"] = node.SelectSingleNode("emailFrom").InnerText;
                        dr["To"] = node.SelectSingleNode("emailTo").InnerText;
                        dr["Subject"] = node.SelectSingleNode("Subject").InnerText;
                        dr["Message"] = utilObj.ReplaceOutXMLSpecialCharacters((node.SelectSingleNode("Message").InnerText),1);//FB 2236
                        dr["Iscalendar"] = node.SelectSingleNode("Iscalendar").InnerText;
                        dtable.Rows.Add(dr);
                    }
                    Boolean isItems = false;
                    if (dtable != null)
                    {
                        if (dtable.Rows.Count > 0)
                        {
                            dgBlockEmail.DataSource = dtable;
                            dgBlockEmail.DataBind();

                            dgBlockEmail.Visible = true;
                            tblNoBlockMail.Visible = false;

                            isItems = true;
                        }
                    }
                    if (!isItems)
                    {
                        dgBlockEmail.Visible = false;
                        tblNoBlockMail.Visible = true;
                    }
                    else
                    {
                        for (Int32 dg = 0; dg < dgBlockEmail.Items.Count; dg++)
                        {
                            
                            dgBlockEmail.Items[dg].Cells[7].Text = obj.GetTranslatedText("No");
                            if (dgBlockEmail.Items[dg].Cells[1].Text.Trim() == "1")
                            {
                                dgBlockEmail.Items[dg].Cells[7].Text = obj.GetTranslatedText("Yes");
                                LinkButton BtnEdit = (LinkButton)dgBlockEmail.Items[dg].FindControl("btnEdit");

                                if (BtnEdit != null)
                                    BtnEdit.Enabled = false;
 
                            }
 
                        }
                    }
                }
                else
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                errLabel.Text = "BindData: " + ex.StackTrace;
            }
        }
        #endregion        

        #region Delete Block Mail 
        protected void DeleteBlockMail(object sender, EventArgs e)
        {
            try
            {
                String inXML = "";

                inXML += "<DeleteEmailsList>";
                inXML += obj.OrgXMLElement();

                for (Int32 dg = 0; dg < dgBlockEmail.Items.Count; dg++)
                {
                    CheckBox chkDelete = (CheckBox)dgBlockEmail.Items[dg].FindControl("chkDelete");

                    if (chkDelete != null)
                    {
                        if (chkDelete.Checked)
                        {
                            inXML += "<DeleteEmail>";
                            inXML += "  <uuid>" + dgBlockEmail.Items[dg].Cells[0].Text + "</uuid>";
                            inXML += "</DeleteEmail>";
                        }
                    }
                    // Release email
                    CheckBox chkRelease = (CheckBox)dgBlockEmail.Items[dg].FindControl("chkRelease");

                    if (chkRelease != null)
                    {
                        if (chkRelease.Checked)
                        {
                            inXML += "<ReleaseEmail>";
                            inXML += "  <uuid>" + dgBlockEmail.Items[dg].Cells[0].Text + "</uuid>";
                            inXML += "</ReleaseEmail>";
                        }
                    }

                }

                inXML += "</DeleteEmailsList>";
                                
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();

                String outXML = obj.CallMyVRMServer("DeleteEmails", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") < 0)
                    Response.Redirect("ViewBlockedMails.aspx?m=1&tp=" + Request.QueryString["tp"].ToString());
            }
            catch (Exception ex)
            {
                errLabel.Text = ex.StackTrace;
                errLabel.Visible = true;
            }
        }

        #endregion
        // FB 1860 - Paging
        #region BindBlockEmail
        protected void BindBlockEmail(object sender, DataGridPageChangedEventArgs e)
        {
            try
            {
                dgBlockEmail.CurrentPageIndex = e.NewPageIndex;
                BindData();
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message);
                errLabel.Text = "Error 122: Please contact your VRM Administrator";
                errLabel.Visible = true;
            }
        }
        #endregion

        #region EditBlockEmail
        protected void EditBlockEmail(object sender, DataGridCommandEventArgs e)
        {
            try
            {
                Response.Redirect("EditBlockEmail.aspx?uuid=" + e.Item.Cells[0].Text + "&tp=" + Request.QueryString["tp"].ToString());
            }
            catch (System.Threading.ThreadAbortException) { }
            catch (Exception ex)
            {
                log.Trace(ex.Message);
                errLabel.Text = "Error 122: Please contact your VRM Administrator";
                errLabel.Visible = true;
            }
        }
        #endregion

        #region Create Column Names 
        /// <summary>
        /// CreateDtColumnNames
        /// </summary>
        private void CreateDtColumnNames()
        {
            colNames = new ArrayList();
            colNames.Add("UUID");
            colNames.Add("RowID");
            colNames.Add("From");
            colNames.Add("To");
            colNames.Add("Subject");
            colNames.Add("Message");
            colNames.Add("Iscalendar");
            
        }
        #endregion

        #region Redirect To TargetPage 

        protected void RedirectToTargetPage(Object sender, EventArgs e)
        {
            try
            {
                if (Request.QueryString["tp"] != null)
                {
                    if (Request.QueryString["tp"].ToLower().Equals("o"))
                        Response.Redirect("OrganisationSettings.aspx");
                    else
                    {
                        if (Request.QueryString["tp"].ToLower().Equals("au"))
                            Response.Redirect("ManageUserProfile.aspx?t=1");
                        else
                            Response.Redirect("ManageUserProfile.aspx");
                    }
                }
            }
            catch (Exception ex)
            {
                log.Trace("EmailCustomization:" + ex.Message);
            }
        }
        #endregion

        //Release Emails
        #region Delete All Block Mail
        protected void DeleteAllBlockMail(object sender, EventArgs e)
        {
            try
            {
                
                for (Int32 dg = 0; dg < dgBlockEmail.Items.Count; dg++)
                {
                    CheckBox chkDelete = (CheckBox)dgBlockEmail.Items[dg].FindControl("chkDelete");

                    if (chkDelete != null)
                        chkDelete.Checked = true;

                    CheckBox chkRelease = (CheckBox)dgBlockEmail.Items[dg].FindControl("chkRelease");

                    if (chkRelease != null)
                         chkRelease.Checked = false;

                }

                DeleteBlockMail(null, null);

               
            }
            catch (Exception ex)
            {
                errLabel.Text = ex.StackTrace;
                errLabel.Visible = true;
            }
        }

        #endregion
    }
}

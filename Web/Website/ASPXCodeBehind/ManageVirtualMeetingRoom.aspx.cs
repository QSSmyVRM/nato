﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.IO;
using System.Text;

namespace ns_MyVRM
{
    public partial class en_ManageVirtualMeetingRoom : System.Web.UI.Page
    {
        #region protected Members
        protected System.Web.UI.WebControls.Label lblTitle;
        protected System.Web.UI.WebControls.Label errLabel;
        protected System.Web.UI.WebControls.Label lblMUser;
        protected System.Web.UI.WebControls.Label lblMdate;

        protected System.Web.UI.WebControls.TextBox txtRoomName;
        protected System.Web.UI.WebControls.TextBox Assistant;
        protected System.Web.UI.WebControls.TextBox txtExternalnum;
        protected System.Web.UI.WebControls.TextBox txtInternalnum;

        protected System.Web.UI.WebControls.ListBox DepartmentList;

        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnMultipleDept;
        protected System.Web.UI.HtmlControls.HtmlInputHidden AssistantID;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnRoomID;

        #endregion

        myVRMNet.NETFunctions obj = null;
        ns_Logger.Logger log = null;

        protected String language = "";
        protected String dtFormatType = "MM/dd/yyyy";
        String tformat = "hh:mm tt";

        public en_ManageVirtualMeetingRoom()
        {
            //
            // TODO: Add constructor logic here
            //
            obj = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger(); 
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["FormatDateType"] != null)
            {
                if (Session["FormatDateType"].ToString() == "")
                    Session["FormatDateType"] = "MM/dd/yyyy";
                else
                    dtFormatType = Session["FormatDateType"].ToString();
            }

            if (Session["language"] == null)
                Session["language"] = "en";

            if (Session["language"].ToString() != "")
                language = Session["language"].ToString();

            Session["timeFormat"] = ((Session["timeFormat"] == null) ? "1" : Session["timeFormat"]);
            tformat = (Session["timeFormat"].ToString().Equals("0") ? "HH:mm" : "hh:mm tt");

            if (!IsPostBack) //if page loads for the first time
            {
                if (Request.QueryString["rID"] != null)
                    hdnRoomID.Value = Request.QueryString["rID"].ToString();
                else
                    hdnRoomID.Value = "new";

                if (Request.QueryString["m"] != null)
                    if (Request.QueryString["m"].ToString().Equals("1"))
                    {
                        errLabel.Text = obj.ShowSuccessMessage();
                        errLabel.Visible = true;
                    }
                
                Assistant.Attributes.Add("readonly", "");
                LoadDepartments();
                if (!hdnRoomID.Value.ToLower().Equals("new"))
                    LoadRoomProfile();
            }

            if (hdnRoomID.Value.ToLower().Equals("new"))
            {
                if (!IsPostBack) // FB 2586
                    lblTitle.Text = obj.GetTranslatedText("Create New ") + lblTitle.Text;
            }
            else
                lblTitle.Text = obj.GetTranslatedText("Edit ") + lblTitle.Text;

        }

        #region LoadDepartments
        /// <summary>
        /// LoadDepartments
        /// </summary>
        protected void LoadDepartments()
        {
            try
            {
                StringBuilder inXML = new StringBuilder();
                inXML.Append("<GetDepartmentsForLocation>");
                inXML.Append(obj.OrgXMLElement());//Organization Module Fixes
                inXML.Append("  <UserID>" + Session["userID"].ToString() + "</UserID>");
                inXML.Append("</GetDepartmentsForLocation>");
                log.Trace("GetDepartmentsForLocation Inxml: " + inXML.ToString());

                String outXML = obj.CallMyVRMServer("GetDepartmentsForLocation", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
                log.Trace("GetDepartmentsForLocation outxml: " + outXML);
                if (outXML.IndexOf("<error>") < 0)
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);
                    hdnMultipleDept.Value = xmldoc.SelectSingleNode("//GetDepartmentsForLocation/multiDepartment").InnerText;
                    if (hdnMultipleDept.Value == "1")
                    {
                        XmlNodeList nodes = xmldoc.SelectNodes("//GetDepartmentsForLocation/departments/department");
                        if (nodes.Count > 0)
                            obj.LoadList(DepartmentList, nodes, "id", "name");
                    }
                }
                else
                {
                    if (Application["Client"].ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.MOJ))
                    {
                        errLabel.Text = obj.ShowErrorMessage(outXML);
                        if (errLabel.Text.ToString().ToUpper().Contains("CONFERENCES"))
                            errLabel.Text = errLabel.Text.ToString().Replace("conferences", "hearings");
                    }
                    else
                        errLabel.Text = obj.ShowErrorMessage(outXML);

                    errLabel.Visible = true;
                }
            }
            catch (Exception ex)
            {
                log.Trace("LoadEndpoints: " + ex.Message + " : " + ex.StackTrace);
            }
        }
        #endregion

        #region LoadRoomProfile
        /// <summary>
        /// LoadRoomProfile
        /// </summary>
        protected void LoadRoomProfile()
        {
            DateTime deactDate = DateTime.MinValue;
            try
            {
                String inXML = "<GetRoomProfile>";
                inXML += obj.OrgXMLElement();
                inXML += "  <UserID>" + Session["userID"].ToString() + "</UserID>";
                inXML += "  <RoomID>" + hdnRoomID.Value + "</RoomID>";
                inXML += "</GetRoomProfile>";
                log.Trace("GetRoomProfile inxml: " + inXML);
                String outXML = obj.CallMyVRMServer("GetRoomProfile", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                log.Trace("GetRoomProfile outxml: " + outXML);

                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);

                txtRoomName.Text = xmldoc.SelectSingleNode("//GetRoomProfile/RoomName").InnerText;
                hdnRoomID.Value = xmldoc.SelectSingleNode("//GetRoomProfile/RoomID").InnerText;

                if (xmldoc.SelectSingleNode("//GetRoomProfile/UserStaus") != null)
                {
                    errLabel.Text = "The Assistant-in-charge \"" + xmldoc.SelectSingleNode("//GetRoomProfile/AssistantInchargeName").InnerText + "\" is not found in Active User List.";
                    errLabel.Visible = true;
                }
                else
                {
                    AssistantID.Value = xmldoc.SelectSingleNode("//GetRoomProfile/AssistantInchargeID").InnerText;
                    Assistant.Text = xmldoc.SelectSingleNode("//GetRoomProfile/AssistantInchargeName").InnerText;
                }


                XmlNodeList nodes = xmldoc.SelectNodes("//GetRoomProfile/Departments/Department");
                Int32 c = nodes.Count;
                for (Int32 d = 0; d < nodes.Count; d++)
                {
                    foreach (ListItem item in DepartmentList.Items)
                    {
                        if (c == 0)
                            break;
                        if (item.Value == nodes[d].SelectSingleNode("ID").InnerText)
                        {
                            item.Selected = true;
                            c--;
                            break;
                        }
                    }
                }

                if (xmldoc.SelectSingleNode("//GetRoomProfile/InternalNumber") != null)
                    txtInternalnum.Text = xmldoc.SelectSingleNode("//GetRoomProfile/InternalNumber").InnerText.Trim();

                if (xmldoc.SelectSingleNode("//GetRoomProfile/ExternalNumber") != null)
                    txtExternalnum.Text = xmldoc.SelectSingleNode("//GetRoomProfile/ExternalNumber").InnerText.Trim();

                if (xmldoc.SelectSingleNode("//GetRoomProfile/LastModifiedDate") != null)
                {
                    DateTime.TryParse(xmldoc.SelectSingleNode("//GetRoomProfile/LastModifiedDate").InnerText, out deactDate);

                    if (deactDate != DateTime.MinValue)
                        lblMdate.Text = deactDate.ToString(dtFormatType) + " " + deactDate.ToString(tformat);
                }
                lblMUser.Text = xmldoc.SelectSingleNode("//GetRoomProfile/LastModififeduserName").InnerText;
            }
            catch (Exception ex)
            {
                log.Trace("LoadRoomProfile: " + ex.StackTrace);
            }
        }
        #endregion

        #region SetRoomProfile
        /// <summary>
        /// SetRoomProfile
        /// </summary>
        /// <returns></returns>
        protected void SetRoomProfile(Object sender,EventArgs e)
        {
            XmlDocument createRooms = null;
            XmlDocument loadRooms = null;
            String roomxmlPath = "";//Room search
            XmlNodeList roomList = null;
            string roomId = "";
            XmlNode ndeRoomID = null;
            StringBuilder inXML = new StringBuilder();
            try
            {
                inXML.Append("<SetRoomProfile>");
                inXML.Append(obj.OrgXMLElement());//Organization Module Fixes
                inXML.Append("<UserID>" + Session["userID"].ToString() + "</UserID>");
                inXML.Append("<RoomID>" + hdnRoomID.Value + "</RoomID>");
                inXML.Append("<RoomName>" + txtRoomName.Text + "</RoomName>");
                inXML.Append("<RoomPhoneNumber></RoomPhoneNumber>");
                inXML.Append("<MaximumCapacity>0</MaximumCapacity>");
                inXML.Append("<MaximumConcurrentPhoneCalls>0</MaximumConcurrentPhoneCalls>");
                inXML.Append("<SetupTime>0</SetupTime>");
                inXML.Append("<TeardownTime>0</TeardownTime>");
                inXML.Append("<AssistantInchargeID>" + AssistantID.Value + "</AssistantInchargeID>");
                inXML.Append("<AssistantInchargeName></AssistantInchargeName>");
                inXML.Append("<MultipleAssistantEmails></MultipleAssistantEmails>");
                inXML.Append("<Tier1ID>" + Session["OnflyTopTierID"].ToString() + "</Tier1ID>"); 
                inXML.Append("<Tier2ID>" + Session["OnflyMiddleTierID"].ToString() + "</Tier2ID>");
                inXML.Append("<Floor></Floor>");
                inXML.Append("<RoomNumber></RoomNumber>");
                inXML.Append("<StreetAddress1></StreetAddress1>");
                inXML.Append("<StreetAddress2></StreetAddress2>");
                inXML.Append("<City></City>");
                inXML.Append("<State></State>");
                inXML.Append("<ZipCode></ZipCode>");
                inXML.Append("<Country></Country>");
                inXML.Append("<Handicappedaccess>0</Handicappedaccess>");
                inXML.Append("<isVIP>0</isVIP>");
                inXML.Append("<isTelepresence>0</isTelepresence>");
                inXML.Append("<RoomQueue></RoomQueue>");
                inXML.Append("<MapLink></MapLink>");
                inXML.Append("<ParkingDirections></ParkingDirections>");
                inXML.Append("<AdditionalComments></AdditionalComments>");
                inXML.Append("<TimezoneID>26</TimezoneID>");
                inXML.Append("<Longitude></Longitude>");
                inXML.Append("<Latitude></Latitude>");
                inXML.Append("<Approvers>");
                inXML.Append("  <Approver1ID></Approver1ID>");
                inXML.Append("  <Approver1Name></Approver1Name>");
                inXML.Append("  <Approver2ID></Approver2ID>");
                inXML.Append("  <Approver2Name></Approver2Name>");
                inXML.Append("  <Approver3ID></Approver3ID>");
                inXML.Append("  <Approver3Name></Approver3Name>");
                inXML.Append("</Approvers>");
                inXML.Append("<EndpointID></EndpointID>");
                inXML.Append("<Custom1></Custom1>");
                inXML.Append("<Custom2></Custom2>");
                inXML.Append("<Custom3></Custom3>");
                inXML.Append("<Custom4></Custom4>");
                inXML.Append("<Custom5></Custom5>");
                inXML.Append("<Custom6></Custom6>");
                inXML.Append("<Custom7></Custom7>");
                inXML.Append("<Custom8></Custom8>");
                inXML.Append("<Custom9></Custom9>");
                inXML.Append("<Custom10></Custom10>");
                inXML.Append("<Projector>0</Projector>");
                inXML.Append("<Video>2</Video>"); //Media type - video rooms //FB 2488
                inXML.Append("<DynamicRoomLayout>1</DynamicRoomLayout>");
                inXML.Append("<CatererFacility>0</CatererFacility>");
                inXML.Append("<ServiceType>-1</ServiceType>");
                inXML.Append("<DedicatedVideo>0</DedicatedVideo>");
                inXML.Append("<DedicatedCodec>0</DedicatedCodec>");
                inXML.Append("<IsVMR>1</IsVMR>");
                inXML.Append("<InternalNumber>" + txtInternalnum.Text + "</InternalNumber>");
                inXML.Append("<ExternalNumber>" + txtExternalnum.Text + "</ExternalNumber>");
                inXML.Append("<AVOnsiteSupportEmail></AVOnsiteSupportEmail>");
                inXML.Append("<Departments>");
                
                foreach (ListItem item in DepartmentList.Items)
                {
                    if (item.Selected)
                        inXML.Append("<Department><ID>" + item.Value + "</ID><Name>" + item.Text + "</Name><SecurityKey></SecurityKey></Department>");
                }

                inXML.Append("</Departments>");
                inXML.Append("<RoomImages>"); //Image Project start
                inXML.Append("</RoomImages>");
                inXML.Append("<RoomImageName></RoomImageName>");
                inXML.Append("<Images>");
                inXML.Append("  <Map1></Map1>");
                inXML.Append("  <Map1Image></Map1Image>");
                inXML.Append("  <Map2></Map2>");
                inXML.Append("  <Map2Image></Map2Image>");
                inXML.Append("  <Security1></Security1>");
                inXML.Append("  <Security1ImageId></Security1ImageId>");
                inXML.Append("  <Misc1></Misc1>");
                inXML.Append("  <Misc1Image></Misc1Image>");
                inXML.Append("  <Misc2></Misc2>");
                inXML.Append("  <Misc2Image></Misc2Image>");
                inXML.Append("</Images>");
                inXML.Append("</SetRoomProfile>");

                log.Trace("SetRoomProfile Inxml: " + inXML);
                String outXML = obj.CallMyVRMServer("SetRoomProfile", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
                log.Trace("SetRoomProfile Outxml: " + outXML);

                if (outXML.IndexOf("<error>") >= 0)
                {
                    if (Application["Client"].ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.MOJ))
                    {
                        errLabel.Text = obj.ShowErrorMessage(outXML);
                        if (errLabel.Text.ToString().ToUpper().Contains("CONFERENCES"))
                            errLabel.Text = errLabel.Text.ToString().Replace("conferences", "hearings");
                    }
                    else
                        errLabel.Text = obj.ShowErrorMessage(outXML);

                    errLabel.Visible = true;
                    return;
                }
                else
                {
                    String innerXML = "";
                    Boolean newroom = true;
                    roomxmlPath = Directory.GetParent(HttpContext.Current.Request.MapPath(".").ToString()) + "\\en" + "\\" + Session["RoomXmlPath"].ToString();//FB 1830
                    if (File.Exists(roomxmlPath))
                    {
                        loadRooms = new XmlDocument();
                        loadRooms.LoadXml(outXML);
                        ndeRoomID = loadRooms.SelectSingleNode("Rooms/Room/RoomID");
                        if (ndeRoomID != null)
                            roomId = ndeRoomID.InnerText;
                        if (roomId != "")
                        {
                            createRooms = new XmlDocument();
                            if (!obj.WaitForFile(roomxmlPath))
                            {
                                return;
                            }
                            else
                                createRooms.Load(roomxmlPath);

                            roomList = createRooms.SelectNodes("Rooms/Room");
                            ndeRoomID = loadRooms.SelectSingleNode("Rooms/Room");
                            innerXML = ndeRoomID.InnerXml;
                            if (roomList != null)
                            {
                                for (int a = 0; a < roomList.Count; a++)
                                {
                                    if (roomList[a].SelectSingleNode("RoomID").InnerText.Trim() == roomId.Trim())
                                    {
                                        newroom = false;
                                        if (ndeRoomID != null)
                                            roomList[a].InnerXml = ndeRoomID.InnerXml;
                                    }

                                }

                                if (newroom)
                                {
                                    if (innerXML != "")
                                    {
                                        ndeRoomID = createRooms.CreateElement("Room");
                                        ndeRoomID.InnerXml = innerXML;
                                        createRooms.SelectSingleNode("Rooms").AppendChild(ndeRoomID);
                                    }
                                }

                                if (File.Exists(roomxmlPath))
                                {
                                    if (obj.WaitForFile(roomxmlPath))
                                    {
                                        File.Delete(roomxmlPath);
                                        createRooms.Save(roomxmlPath);
                                    }
                                }
                            }

                        }

                    }
                    
                }

                Button btnCtrl = (System.Web.UI.WebControls.Button)sender;
                if (btnCtrl.ID.Trim().Equals("btnSubmitAddNew"))
                    Response.Redirect("ManageVirtualMeetingRoom.aspx?m=1");
                else
                    Response.Redirect("manageroom.aspx?hf=&m=1&pub=&d=&comp=&f=&frm=");

            }
            catch (Exception ex)
            {
                log.Trace("SetRoomProfile: "+ ex.StackTrace);
                return;
            }
        }
        #endregion

        #region ResetRoomProfile
        /// <summary>
        /// ResetRoomProfile
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ResetRoomProfile(Object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("ManageVirtualMeetingRoom.aspx?rID=" + hdnRoomID.Value);
            }
            catch (Exception ex)
            {
                log.Trace("ResetRoomProfile" + ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion
    }
}

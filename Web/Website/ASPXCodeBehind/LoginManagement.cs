using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Collections.Generic;
using System.Xml;
using System.IO;
//using System.DirectoryServices.Protocols;//New LDAP Design //FB 1820


namespace MyVRMNet
{
    #region Login Management

    public class LoginManagement
    {
        #region Data Members
        /// <summary>
        /// Data Members
        /// </summary>
        public String WHO_USE = "";
        public Boolean wizard_enable = false;
        public Boolean feedback_enable = true;
        public String asp_version = "v14";
        public String db_version = "beta2";
        public String com_version = "beta2";
        public String mcu_version = "beta2";
        public String queyStraVal = "";
        public String qStrPVal = "";
        public string CompanyLogo = "";
        public string OrgBanner1600Path = "";
        public string OrgBanner1024Path = "";
        public string companyTagline = "Saving the planet...one meeting at a time";
        string outXML = "";
        public string errMessage = "";
        public Boolean windwsAuth = false;
        myVRMNet.NETFunctions obj = null;
        ns_Logger.Logger log;
        myVRMNet.ImageUtil imageObj = null;
        //private LdapConnection ldapConn; //FB 1820

        public string mailextn = "";//FB 1943

        #endregion

        # region Constructor
        /// <summary>
        /// Public Constructor 
        /// </summary>
        public LoginManagement()
        {
            obj = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger();
            imageObj = new myVRMNet.ImageUtil();
        }
        #endregion

        #region  readaspconfigASP
        /// <summary>
        /// readaspconfig.asp Converted as method,Code lines currently using in application.
        /// </summary>
        public void readaspconfigASP()
        {
            Int32 TimeoutSecond;
            Boolean FoodPrice = true;
            String getpwdLNK = "enable";
            String getactLNK = "enable";
            String viwpubLNK = "enable";
            Int32 useDuration = 0;
            String roomListView = "level";	// level/list
            Int32 sharedEnv = 0;
            Int32 confRecurrance = 250;
            Int32 CosignEnable = 0;
            Int32 DefaultConferenceType = 2; // 7:Room Conference, 2:Audio/Video, 6:Audio-only, 4: Point-to-point
            Int32 EnableRoomConfType = 1;
            Int32 EnableAudioVideoConfType = 1;
            Int32 EnableAudioOnlyConfType = 1;
            try
            {

                HttpContext.Current.Application["appletChart"] = 2;
                HttpContext.Current.Application["global"] = "disable"; //GlobalMode = "enable" declared only in ASPConfig.asp/ASPConfig_template.asp

                //Settings for Codian - Need to verify where the codian value is being set as yes
                //if (Codian == "yes")
                //    HttpContext.Current.Application["Codian"] = "1";
                //else
                //      HttpContext.Current.Application["Codian"] = "0";


                //HttpContext.Current.Application["Version"] = "1.9.2"; //Edited For FB 1648

                HttpContext.Current.Application["Codian"] = "0";
                HttpContext.Current.Application["FileSizeLimit"] = 10000000; //limit of total three upload files (< 10 MB)
                HttpContext.Current.Application["FRImageSizeLimit"] = 10000000; //limit of each individual upload food/resource images (< 10 MB)
                HttpContext.Current.Application["RmImageSizeLimit"] = 10000000; //limit of each individual upload room images (< 10 MB) fogbugz case 146

                if (HttpContext.Current.Application["Client"] != null)
                {
                    switch (HttpContext.Current.Application["Client"].ToString().ToUpper()) //Code Lines from inc/ASPConfig.asp
                    {
                        case "HKLAW":
                            TimeoutSecond = 28800;	// unit: second
                            //warning pop-up is 1 mintues earlier.
                            FoodPrice = false;
                            break;
                        default:
                            TimeoutSecond = 28800;	// unit: second, eg 20 minutes timeout, please input 1200; 
                            //warning pop-up is 1 mintues earlier.
                            FoodPrice = true;
                            break;
                    }
                    HttpContext.Current.Application["timeoutSecond"] = TimeoutSecond;

                }

                if (FoodPrice)
                    HttpContext.Current.Application["FoodPrice"] = "1";
                else
                    HttpContext.Current.Application["FoodPrice"] = "0";

                if (HttpContext.Current.Application["Client"] != null)
                {
                    if (HttpContext.Current.Application["Client"].ToString().ToUpper().Equals("MOJ"))  //Added for FB 1425 QA Bug -Start
                    {
                        getpwdLNK = "disable";
                        getactLNK = "disable";
                        viwpubLNK = "disable";
                        HttpContext.Current.Application["interval"] = 15;

                    }
                }
                else
                {
                    getpwdLNK = "enable";
                    getactLNK = "enable";
                    viwpubLNK = "enable";
                }
                HttpContext.Current.Application["GetPwdLNK"] = getpwdLNK;
                HttpContext.Current.Application["GetActLNK"] = getactLNK;
                HttpContext.Current.Application["ViwPubLNK"] = viwpubLNK;
                HttpContext.Current.Application["UseDuration"] = useDuration;
                HttpContext.Current.Application["RoomListView"] = roomListView;
                HttpContext.Current.Application["sharedEnv"] = sharedEnv;
                HttpContext.Current.Application["confRecurrence"] = confRecurrance;
                //HttpContext.Current.Application["CosignEnable"] = CosignEnable;  FOr PSU 
                HttpContext.Current.Session["DefaultConferenceType"] = DefaultConferenceType;
                HttpContext.Current.Session["EnableRoomConfType"] = EnableRoomConfType;
                HttpContext.Current.Session["EnableAudioVideoConfType"] = EnableAudioVideoConfType;
                HttpContext.Current.Application["EnableVideoOnlyConfType"] = EnableAudioOnlyConfType;

                //All Config Path in Appliction Variable
                HttpContext.Current.Application.Remove("SchemaPath");
                HttpContext.Current.Application.Remove("COM_ConfigPath");
                HttpContext.Current.Application.Remove("MyVRMServer_ConfigPath");
                HttpContext.Current.Application.Remove("RTC_ConfigPath");
                HttpContext.Current.Application.Add("SchemaPath", "C:\\VRMSchemas_v1.8.3");
                HttpContext.Current.Application.Add("COM_ConfigPath", "C:\\VRMSchemas_v1.8.3\\COMConfig.xml");
                HttpContext.Current.Application.Add("MyVRMServer_ConfigPath", "C:\\VRMSchemas_v1.8.3\\");
                HttpContext.Current.Application.Add("RTC_ConfigPath", "C:\\VRMSchemas_v1.8.3\\VRMRTCConfig.xml");
            }
            catch (Exception ex)
            {
                log.Trace("readaspconfigASP" + ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        #region  HeaderASP
        /// <summary>
        /// Header.asp Converted as method,Code lines currently using in application.
        /// </summary>
        public void HeaderASP()
        {
            Boolean enableBACK = true;
            Boolean enableBLOCK = false;
            Boolean enableOUTLOOK = true;
            String error_page = "";
            String error_page_for_popwin = "";
            String appClient = "";
            WHO_USE = "";

            try
            {
                if (HttpContext.Current.Application["debugMode"] != null)
                    WHO_USE = HttpContext.Current.Application["debugMode"].ToString();

                if (HttpContext.Current.Application["Client"] != null)
                    appClient = HttpContext.Current.Application["Client"].ToString();

                /** PRIVATE PART **/
                HttpContext.Current.Session["WHO_USE"] = WHO_USE;

                switch (WHO_USE)
                {
                    case "ASP":
                        //test = true;			//used in dispatcher2 to give hard-code XML for test.
                        //email_test = true;
                        enableBACK = true;  //Same Variable Used in inc\DisableBack.aspx ,but hard corded over there
                        enableBLOCK = false; //Same Variable Called in gendispatcher.asp
                        enableOUTLOOK = true;	// kapil wanted, 0803
                        error_page = "../showerror.aspx"; // Session Variable
                        error_page_for_popwin = "../showerror.aspx?wintype=popwin"; // Session Variable
                        wizard_enable = true;
                        break;
                    case "COM":
                        //test = false;
                        //email_test = false;
                        enableBACK = false;
                        enableBLOCK = false;
                        enableOUTLOOK = true;
                        error_page = "../underconstruction.aspx";
                        error_page_for_popwin = "../underconstruction.aspx?wintype=popwin";
                        wizard_enable = true;
                        break;
                    case "VRM":
                        //test = false;
                        //email_test = false;
                        enableBACK = false;
                        enableBLOCK = false;
                        enableOUTLOOK = true;
                        error_page = "../underconstruction.aspx";
                        error_page_for_popwin = "../underconstruction.aspx?wintype=popwin";
                        wizard_enable = true;
                        break;
                    default:
                        //test = false;
                        //email_test = false;
                        enableBACK = false;
                        enableBLOCK = false;
                        enableOUTLOOK = true;
                        error_page = "../underconstruction.aspx";
                        error_page_for_popwin = "../underconstruction.aspx?wintype=popwin";
                        wizard_enable = true;
                        break;
                }

                HttpContext.Current.Session.Remove("enableBACK");
                HttpContext.Current.Session.Remove("enableBLOCK");
                HttpContext.Current.Session.Remove("enableOUTLOOK");
                HttpContext.Current.Session.Remove("error_page");
                HttpContext.Current.Session.Remove("wizard_enable");
                HttpContext.Current.Session.Remove("error_page_for_popwin");

                HttpContext.Current.Session.Add("enableBACK", enableBACK);
                HttpContext.Current.Session.Add("enableBLOCK", enableBLOCK);
                HttpContext.Current.Session.Add("enableOUTLOOK", enableOUTLOOK);
                HttpContext.Current.Session.Add("error_page", error_page);
                HttpContext.Current.Session.Add("error_page_for_popwin", error_page_for_popwin);

                if (wizard_enable)
                    HttpContext.Current.Session.Add("wizard_enable", "1");
                else
                    HttpContext.Current.Session.Add("wizard_enable", "0");


                // Image Part START
                HttpContext.Current.Session.Remove("IMAGE_PATH");
                HttpContext.Current.Session.Remove("RESOURCE_ICON_PATH");
                HttpContext.Current.Session.Remove("FOOD_ICON_PATH");
                HttpContext.Current.Session.Remove("ROOM_IMAGE_PATH");
                HttpContext.Current.Session.Remove("SEC_PASS_IMAGE_PATH");
                HttpContext.Current.Session.Remove("MAP_IMAGE_PATH");
                HttpContext.Current.Session.Remove("MISC_ATTACH_IMAGE_PATH");
                HttpContext.Current.Session.Remove("DISPLAY_LAYOUT_IMAGE_PATH");
                HttpContext.Current.Session.Remove("FILEUPLOAD_PATH");
                HttpContext.Current.Session.Remove("vrmtop_img");
                HttpContext.Current.Session.Remove("vrmtopright2_img");

                HttpContext.Current.Session.Add("IMAGE_PATH", "image/");
                HttpContext.Current.Session.Add("RESOURCE_ICON_PATH", "image/resource/");
                HttpContext.Current.Session.Add("FOOD_ICON_PATH", "image/food/");
                HttpContext.Current.Session.Add("ROOM_IMAGE_PATH", "image/room/");
                HttpContext.Current.Session.Add("SEC_PASS_IMAGE_PATH", "image/secpass/");
                HttpContext.Current.Session.Add("MAP_IMAGE_PATH", "image/mapfile/");
                HttpContext.Current.Session.Add("MISC_ATTACH_IMAGE_PATH", "image/miscattach/");
                HttpContext.Current.Session.Add("DISPLAY_LAYOUT_IMAGE_PATH", "image/displaylayout/");

                // FILE UPLOAD Part
                HttpContext.Current.Session.Add("FILEUPLOAD_PATH", "upload");
                HttpContext.Current.Session.Add("vrmtop_img", "image/Lobbytop.jpg");
                HttpContext.Current.Session.Add("vrmtopright2_img", "image/vrmtopright2.gif");
            }
            catch (Exception ex)
            {
                log.Trace("HeaderASP" + ex.StackTrace + " : " + ex.Message);
                
            }
        }
        #endregion

        #region  GetHome Command
        /// <summary>
        /// GetHome Command form ASP to C#.Net
        /// </summary>
        public String GetHomeCommand()
        {
            XmlDocument XmlDoc = null;
            XmlDocument NewXmlDoc = null;
            XmlNode node = null;
            XmlNodeList nodes = null;
            string inxml = "";           
            string localUserList = "";
            string user = "";            
            string approver = "";
            string menuMask = "";
            string uptz = "";
            Int32 startpos = 0;
            Boolean enableBLOCK = false;
            string newuser = "";
            bool isValidemail = true;
			//FB 1830
            string language = "en";            

            //Dictionary<string, string> EmailSeverDict = null; //FB 1820
            try
            {
                if(windwsAuth)  //SSO Mode & Cosign Environment
                {
                    inxml = "<login><userName>" + queyStraVal + "</userName>"+
                        "<userPassword>" + qStrPVal + "</userPassword>"+
                        "<homeURL>" + GenerateHomeURL() + "</homeURL>"+
                        "<userAuthenticated>Yes</userAuthenticated></login>";
                }
                else //Normal Login
                {
                    if (queyStraVal.IndexOf("@") <= 0)
                        isValidemail = false;

                    if (!isValidemail)
                    {
                        //outXML = "<error><errorCode>000</errorCode><message>Invalid Email Address</message></error>";
                        outXML = obj.GetErrorMessage(498);//FB 1881
                        return outXML;
                    }

                    inxml = "<login><emailID>" + queyStraVal + "</emailID>" +
                        "<userPassword>" + qStrPVal + "</userPassword>" +
                        "<homeURL>" + GenerateHomeURL() + "</homeURL>" +
                        "<userAuthenticated>No</userAuthenticated></login>";
                }

                outXML = obj.CallMyVRMServer("GetHome", inxml, HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString()); //FB 2027

                HttpContext.Current.Session.Remove("guestlogin");
                HttpContext.Current.Session.Add("guestlogin", "");
                HttpContext.Current.Session["outXML"] = outXML; //Added New

                //if (outXML.IndexOf("<error>") >= 0 )
                {
                    if (outXML.IndexOf("<organizationID>") < 0) //FB 1820 Login Management
                        return outXML;
                    else
                    {
                        NewXmlDoc = new XmlDocument();
                        NewXmlDoc.LoadXml(outXML);
                        if (NewXmlDoc.DocumentElement.SelectSingleNode("error") != null)
                        {
                            HttpContext.Current.Session["errMsg"] = obj.ShowErrorMessage(NewXmlDoc.DocumentElement.SelectSingleNode("error").OuterXml);
                        }
                    }
                }

                RemoveSessionVariables();

                #region Assign Session Variables - Organization Level

                int orgid = 11;
                int.TryParse(NewXmlDoc.DocumentElement.SelectSingleNode("organizationID").InnerText.Trim(), out orgid);
                HttpContext.Current.Session.Add("organizationID", orgid);

                node = NewXmlDoc.DocumentElement.SelectSingleNode("globalInfo");

                HttpContext.Current.Session.Add("userID", node.SelectSingleNode("userID").InnerText);
                if (HttpContext.Current.Session["userID"].ToString() != "")
                {
                    localUserList = HttpContext.Current.Application["OnlineUserList"].ToString();
                    user = HttpContext.Current.Session["userID"].ToString() + ",";
                    startpos = localUserList.IndexOf(user);
                    if (startpos != 0)
                    {
                        if (enableBLOCK)
                        {
                            HttpContext.Current.Response.Redirect("thankyou3.aspx");
                        }
                    }
                    else
                    {
                        HttpContext.Current.Application.Lock();
                        HttpContext.Current.Application.Set("OnlineUserList", Convert.ToString(HttpContext.Current.Application["OnlineUserList"]) + Convert.ToString(HttpContext.Current.Session["userID"]) + ",");
                        HttpContext.Current.Application.UnLock();
                    }
                }

                LoadOrgHolidays();// FB 1861

                if (node.SelectSingleNode("userName/firstName") != null)
                    HttpContext.Current.Session.Add("userFirstName", node.SelectSingleNode("userName/firstName").InnerText);

                if (node.SelectSingleNode("userName/lastName") != null)
                    HttpContext.Current.Session.Add("userLastName", node.SelectSingleNode("userName/lastName").InnerText);

                if (node.SelectSingleNode("userName/lastName") != null && node.SelectSingleNode("userName/lastName") != null)
                    HttpContext.Current.Session.Add("userName", node.SelectSingleNode("userName/firstName").InnerText + " " + node.SelectSingleNode("userName/lastName").InnerText);

                if (node.SelectSingleNode("userEmail") != null)
                    HttpContext.Current.Session.Add("userEmail", node.SelectSingleNode("userEmail").InnerText);
                //Audio Add On Starts..
                if (node.SelectSingleNode("ConferenceCode") != null)
                {
                    if(node.SelectSingleNode("ConferenceCode").InnerText != "")
                        HttpContext.Current.Session.Add("ConferenceCode", node.SelectSingleNode("ConferenceCode").InnerText);
                    else
                        HttpContext.Current.Session.Add("ConferenceCode", "0");
                    
                }
                if (node.SelectSingleNode("LeaderPin") != null)
                {
                    if (node.SelectSingleNode("LeaderPin").InnerText!= "")
                    HttpContext.Current.Session.Add("LeaderPin", node.SelectSingleNode("LeaderPin").InnerText);
                    else
                        HttpContext.Current.Session.Add("LeaderPin", "0");
                }
                if (node.SelectSingleNode("AdvAvParams") != null)
                {
                    if (node.SelectSingleNode("AdvAvParams").InnerText != "")
                        HttpContext.Current.Session.Add("AdvAvParams", node.SelectSingleNode("AdvAvParams").InnerText);
                    else
                        HttpContext.Current.Session.Add("AdvAvParams", "0");

                }
                if (node.SelectSingleNode("AudioParams") != null)
                {
                    if(node.SelectSingleNode("AudioParams").InnerText != "")                        
                        HttpContext.Current.Session.Add("AudioParams", node.SelectSingleNode("AudioParams").InnerText);
                    else
                        HttpContext.Current.Session.Add("AudioParams", "0");
                }
                //FB 2359 Start
                if (node.SelectSingleNode("EnableRoomParam") != null)
                {
                    if (node.SelectSingleNode("EnableRoomParam").InnerText != "")
                        HttpContext.Current.Session.Add("EnableRoomParam", node.SelectSingleNode("EnableRoomParam").InnerText);
                    else
                        HttpContext.Current.Session.Add("EnableRoomParam", "0");

                }

                //FB 2359 End
                if (node.SelectSingleNode("isVIP") != null)// FB 1864
                {
                    if (node.SelectSingleNode("isVIP").InnerText != "")
                        HttpContext.Current.Session.Add("isVIP", node.SelectSingleNode("isVIP").InnerText);
                    else
                        HttpContext.Current.Session.Add("isVIP", "0");
                }

                if (node.SelectSingleNode("EnableRoomServiceType") != null)// FB 2219
                {
                    if (node.SelectSingleNode("EnableRoomServiceType").InnerText != "")
                        HttpContext.Current.Session.Add("EnableRoomServiceType", node.SelectSingleNode("EnableRoomServiceType").InnerText);
                    else
                        HttpContext.Current.Session.Add("EnableRoomServiceType", "0");
                }

                if (node.SelectSingleNode("EnableImmConf") != null)// FB 2036
                {
                    if (node.SelectSingleNode("EnableImmConf").InnerText != "")
                        HttpContext.Current.Session.Add("EnableImmConf", node.SelectSingleNode("EnableImmConf").InnerText);
                    else
                        HttpContext.Current.Session.Add("EnableImmConf", "0");
                }

                if (node.SelectSingleNode("EnableAudioBridges") != null)// FB 2023
                {
                    if (node.SelectSingleNode("EnableAudioBridges").InnerText != "")
                        HttpContext.Current.Session.Add("EnableAudioBridges", node.SelectSingleNode("EnableAudioBridges").InnerText);
                    else
                        HttpContext.Current.Session.Add("EnableAudioBridges", "0");
                }
                if (node.SelectSingleNode("EnableEM7") != null)// FB 2633
                {
                    if (node.SelectSingleNode("EnableEM7").InnerText != "")
                        HttpContext.Current.Session.Add("EnableEM7", node.SelectSingleNode("EnableEM7").InnerText);
                    else
                        HttpContext.Current.Session.Add("EnableEM7", "0");
                }

                HttpContext.Current.Session.Add("EnableConfPassword", "1");
                if (node.SelectSingleNode("EnableConfPassword") != null)// FB 2359
                    if (node.SelectSingleNode("EnableConfPassword").InnerText != "")
                         HttpContext.Current.Session["EnableConfPassword"] = node.SelectSingleNode("EnableConferencePassword").InnerText;
                
                HttpContext.Current.Session.Add("EnablePublicConf", "1");
                if (node.SelectSingleNode("EnablePublicConf") != null)// FB 2359
                    if (node.SelectSingleNode("EnablePublicConf").InnerText != "")
                        HttpContext.Current.Session["EnablePublicConf"] = node.SelectSingleNode("EnablePublicConference").InnerText;

                // FB VMR Start
                HttpContext.Current.Session.Add("EnableVMR", "1");
                if (node.SelectSingleNode("EnableVMR") != null)
                    if (node.SelectSingleNode("EnableVMR").InnerText != "")
                        HttpContext.Current.Session["EnableVMR"] = node.SelectSingleNode("EnableVMR").InnerText;
                // FB VMR End

                if (node.SelectSingleNode("isAssignedMCU") != null)// FB 1901
                {
                    if (node.SelectSingleNode("isAssignedMCU").InnerText != "")
                        HttpContext.Current.Session.Add("isAssignedMCU", node.SelectSingleNode("isAssignedMCU").InnerText);
                    else
                        HttpContext.Current.Session.Add("isAssignedMCU", "1");
                }
				//FB 1830 - Translation
                if (node.SelectSingleNode("isMultiLingual") != null)// // Translate Text
                {
                    if (node.SelectSingleNode("isMultiLingual").InnerText != "")
                        HttpContext.Current.Session.Add("isMultiLingual", node.SelectSingleNode("isMultiLingual").InnerText);
                    else
                        HttpContext.Current.Session.Add("isMultiLingual", "0");
                }


                if (node.SelectSingleNode("languageID") != null)// // Translate Text
                {
                    if (node.SelectSingleNode("languageID").InnerText != "")
                        HttpContext.Current.Session.Add("languageID", node.SelectSingleNode("languageID").InnerText);
                    else
                        HttpContext.Current.Session.Add("languageID", "1");
                }
                

                if (node.SelectSingleNode("isSpecialRecur") != null)// FB 2052
                {
                    if (node.SelectSingleNode("isSpecialRecur").InnerText != "")
                        HttpContext.Current.Session.Add("isSpecialRecur", node.SelectSingleNode("isSpecialRecur").InnerText);
                    else
                        HttpContext.Current.Session.Add("isSpecialRecur", "0");
                }

                if (node.SelectSingleNode("roleID") != null)
                    HttpContext.Current.Session.Add("roleID", node.SelectSingleNode("roleID").InnerText);

                if (node.SelectSingleNode("admin") != null)
                    HttpContext.Current.Session.Add("admin", node.SelectSingleNode("admin").InnerText);

                if (node.SelectSingleNode("realtimeType") != null)
                    HttpContext.Current.Session.Add("realtimeType", node.SelectSingleNode("realtimeType").InnerText);

                if (node.SelectSingleNode("dialoutEnabled") != null)
                    HttpContext.Current.Session.Add("dialoutEnabled", node.SelectSingleNode("dialoutEnabled").InnerText);

                if (node.SelectSingleNode("defaultPublicEnabled") != null)
                    HttpContext.Current.Session.Add("defaultPublic", node.SelectSingleNode("defaultPublicEnabled").InnerText);
                 //FB 1830
                if (node.SelectSingleNode("language") != null)
                {
                    if(node.SelectSingleNode("language").InnerText != "")
                        language = node.SelectSingleNode("language").InnerText ;
                }
                HttpContext.Current.Session.Add("language", language);

                //FB 1881 - Error Handling start
                int languageid = 1;
                if (node.SelectSingleNode("languageid") != null)
                {
                    int.TryParse(node.SelectSingleNode("languageid").InnerText.Trim(), out languageid);
                }
                if (languageid < 1)
                    languageid = 1; //default

                HttpContext.Current.Session.Add("languageid", languageid);
                HttpContext.Current.Session.Add("SystemError", obj.GetErrorMessage(200));
                //FB 1881 - Error Handling end
                if (node.SelectSingleNode("enableAV") != null)
                    HttpContext.Current.Session.Add("enableAV", node.SelectSingleNode("enableAV").InnerText);
                //For Av Switch
                if (node.SelectSingleNode("enableParticipants") != null)
                    HttpContext.Current.Session.Add("enableParticipants", node.SelectSingleNode("enableParticipants").InnerText);
                //For FB 1429
                if (node.SelectSingleNode("timeFormat") != null)
                    HttpContext.Current.Session.Add("timeFormat", node.SelectSingleNode("timeFormat").InnerText);

                if (node.SelectSingleNode("timeZoneDisplay") != null)
                    HttpContext.Current.Session.Add("timeZoneDisplay", node.SelectSingleNode("timeZoneDisplay").InnerText);
                
                //FB 1779 start
                if (node.SelectSingleNode("userWorkNo") != null)
                    HttpContext.Current.Session.Add("workPhone", node.SelectSingleNode("userWorkNo").InnerText);
                if (node.SelectSingleNode("userCellNo") != null)
                    HttpContext.Current.Session.Add("cellPhone", node.SelectSingleNode("userCellNo").InnerText);
                //FB 1779 end
				// FB 1719  start
                if (node.SelectSingleNode("defaultConfTemplate") == null)
                {
                        HttpContext.Current.Session.Add("defaultConfTemp", "0");
                }
                else if (node.SelectSingleNode("defaultConfTemplate").ToString() == "")
                {
                    HttpContext.Current.Session.Add("defaultConfTemp", "0");
                }
                else
                {
                    HttpContext.Current.Session.Add("defaultConfTemp", node.SelectSingleNode("defaultConfTemplate").InnerText);
                }
                HttpContext.Current.Session.Add("UserToEdit", "");
                // FB 1719 - ConferenceTemplate end

                if (node.SelectSingleNode("tickerStatus") != null)
                    HttpContext.Current.Session.Add("tickerStatus", node.SelectSingleNode("tickerStatus").InnerText);

                if (node.SelectSingleNode("tickerSpeed") != null)
                    HttpContext.Current.Session.Add("tickerSpeed", node.SelectSingleNode("tickerSpeed").InnerText);

                if (node.SelectSingleNode("tickerPosition") != null)
                    HttpContext.Current.Session.Add("tickerPosition", node.SelectSingleNode("tickerPosition").InnerText);

                if (node.SelectSingleNode("tickerDisplay") != null)
                    HttpContext.Current.Session.Add("tickerDisplay", node.SelectSingleNode("tickerDisplay").InnerText);

                if (node.SelectSingleNode("tickerBackground") != null)
                    HttpContext.Current.Session.Add("tickerBackground", node.SelectSingleNode("tickerBackground").InnerText.Trim());   //FB 1630

                if (node.SelectSingleNode("rssFeedLink") != null)
                    HttpContext.Current.Session.Add("rssFeedLink", node.SelectSingleNode("rssFeedLink").InnerText);

                if (node.SelectSingleNode("tickerStatus1") != null)
                    HttpContext.Current.Session.Add("tickerStatus1", node.SelectSingleNode("tickerStatus1").InnerText);

                if (node.SelectSingleNode("tickerSpeed1") != null)
                    HttpContext.Current.Session.Add("tickerSpeed1", node.SelectSingleNode("tickerSpeed1").InnerText);

                if (node.SelectSingleNode("tickerPosition1") != null)
                    HttpContext.Current.Session.Add("tickerPosition1", node.SelectSingleNode("tickerPosition1").InnerText);

                if (node.SelectSingleNode("tickerDisplay1") != null)
                    HttpContext.Current.Session.Add("tickerDisplay1", node.SelectSingleNode("tickerDisplay1").InnerText);

                if (node.SelectSingleNode("tickerBackground1") != null)
                    HttpContext.Current.Session.Add("tickerBackground1", node.SelectSingleNode("tickerBackground1").InnerText.Trim());    //FB 1630

                if (node.SelectSingleNode("rssFeedLink1") != null)
                    HttpContext.Current.Session.Add("rssFeedLink1", node.SelectSingleNode("rssFeedLink1").InnerText);

                if (HttpContext.Current.Application["Client"] != null)
                {
                    if (HttpContext.Current.Application["Client"].ToString().ToUpper().Equals("MOJ"))
                    {
                        //Code added for FB 1425 QA Bug
                        HttpContext.Current.Session.Add("timeZoneDisplay", "0");
                        //Code added for FB 1425 QA Bug
                        HttpContext.Current.Session.Add("enableParticipants", "0");
                        //Code added for MOJ Phase 2 QA Bug
                        HttpContext.Current.Session.Add("enableAV", "0");
                        //Code added for MOJ Phase 2 QA Bug
                    }
                }

                if (node.SelectSingleNode("SystemEndTime") != null)
                    HttpContext.Current.Session.Add("SystemEndTime", node.SelectSingleNode("SystemEndTime").InnerText);

                if (node.SelectSingleNode("SystemStartTime") != null)
                    HttpContext.Current.Session.Add("SystemStartTime", node.SelectSingleNode("SystemStartTime").InnerText);

                if (node.SelectSingleNode("dynamicInviteEnabled") != null)
                    HttpContext.Current.Session.Add("dynInvite", node.SelectSingleNode("dynamicInviteEnabled").InnerText);

                if (node.SelectSingleNode("doubleBookingEnabled") != null)
                    HttpContext.Current.Session.Add("doubleBooking", node.SelectSingleNode("doubleBookingEnabled").InnerText);

                if (node.SelectSingleNode("p2pConfEnabled") != null)
                    HttpContext.Current.Session.Add("P2PEnable", node.SelectSingleNode("p2pConfEnabled").InnerText);

                if (node.SelectSingleNode("enableRecurrance") != null)
                    HttpContext.Current.Session.Add("recurEnable", node.SelectSingleNode("enableRecurrance").InnerText);

                if (node.SelectSingleNode("organizationName") != null)
                    HttpContext.Current.Session.Add("organizationName", node.SelectSingleNode("organizationName").InnerText);
                //New tags added for CSJ requirements FB case 109 and 1070 start here

                if (node.SelectSingleNode("DefaultConferenceType") != null)
                    HttpContext.Current.Session.Add("DefaultConferenceType", node.SelectSingleNode("DefaultConferenceType").InnerText);

                if (node.SelectSingleNode("EnableRoomConference") != null)
                    HttpContext.Current.Session.Add("EnableRoomConfType", node.SelectSingleNode("EnableRoomConference").InnerText);

                if (node.SelectSingleNode("EnableAudioVideoConference") != null)
                    HttpContext.Current.Session.Add("EnableAudioVideoConfType", node.SelectSingleNode("EnableAudioVideoConference").InnerText);

                if (node.SelectSingleNode("EnableAudioOnlyConference") != null)
                    HttpContext.Current.Session.Add("EnableAudioOnlyConfType", node.SelectSingleNode("EnableAudioOnlyConference").InnerText);

                if (node.SelectSingleNode("DefaultCalendarToOfficeHours") != null)
                    HttpContext.Current.Session.Add("DefaultCalendarToOfficeHours", node.SelectSingleNode("DefaultCalendarToOfficeHours").InnerText);

                if (node.SelectSingleNode("RoomTreeExpandLevel") != null)
                    HttpContext.Current.Session.Add("roomExpandLevel", node.SelectSingleNode("RoomTreeExpandLevel").InnerText);

                if (node.SelectSingleNode("RoomLimit") != null)
                    HttpContext.Current.Session.Add("RoomLimit", node.SelectSingleNode("RoomLimit").InnerText);

                if (node.SelectSingleNode("MaxGuestRooms") != null)//FB 2426
                    HttpContext.Current.Session.Add("MaxGuestRooms", node.SelectSingleNode("MaxGuestRooms").InnerText);

                if (node.SelectSingleNode("McuLimit") != null)
                    HttpContext.Current.Session.Add("McuLimit", node.SelectSingleNode("McuLimit").InnerText);

                if (node.SelectSingleNode("MCUEnchancedLimit") != null)//FB 2486
                    HttpContext.Current.Session.Add("MCUEnchancedLimit", node.SelectSingleNode("MCUEnchancedLimit").InnerText);

                if (node.SelectSingleNode("UserLimit") != null)
                    HttpContext.Current.Session.Add("UserLimit", node.SelectSingleNode("UserLimit").InnerText);

                if (node.SelectSingleNode("MaxVideoRooms") != null)
                    HttpContext.Current.Session.Add("VideoRooms", node.SelectSingleNode("MaxVideoRooms").InnerText);

                if (node.SelectSingleNode("MaxNonVideoRooms") != null)
                    HttpContext.Current.Session.Add("NonVideoRooms", node.SelectSingleNode("MaxNonVideoRooms").InnerText);

                if (node.SelectSingleNode("MaxVMRRooms") != null)//FB 2586
                    HttpContext.Current.Session.Add("VMRRooms", node.SelectSingleNode("MaxVMRRooms").InnerText);

                if (node.SelectSingleNode("MaxEndpoints") != null)
                    HttpContext.Current.Session.Add("EndPoints", node.SelectSingleNode("MaxEndpoints").InnerText);

                if (node.SelectSingleNode("ExchangeUserLimit") != null)
                    HttpContext.Current.Session.Add("ExchangeUserLimit", node.SelectSingleNode("ExchangeUserLimit").InnerText);

                if (node.SelectSingleNode("DominoUserLimit") != null)
                    HttpContext.Current.Session.Add("DominoUserLimit", node.SelectSingleNode("DominoUserLimit").InnerText);

                if (node.SelectSingleNode("MobileUserLimit") != null) // FB 1979
                    HttpContext.Current.Session.Add("MobileUserLimit", node.SelectSingleNode("MobileUserLimit").InnerText);

                if (node.SelectSingleNode("EnableCatering") != null)
                    HttpContext.Current.Session.Add("foodModule", node.SelectSingleNode("EnableCatering").InnerText);

                if (node.SelectSingleNode("EnableFacilities") != null)
                    HttpContext.Current.Session.Add("roomModule", node.SelectSingleNode("EnableFacilities").InnerText);

                if (node.SelectSingleNode("EnableHousekeeping") != null)
                    HttpContext.Current.Session.Add("hkModule", node.SelectSingleNode("EnableHousekeeping").InnerText);

                if (node.SelectSingleNode("EnablePC") != null) //FB 2347
                    HttpContext.Current.Session.Add("PCModule", node.SelectSingleNode("EnablePC").InnerText);

                if (node.SelectSingleNode("EnableCloud") != null) //FB 2262 //FB 2599
                    HttpContext.Current.Session.Add("Cloud", node.SelectSingleNode("EnableCloud").InnerText);

                if (node.SelectSingleNode("EnablePublicRoom") != null) //FB 2594
                    HttpContext.Current.Session.Add("EnablePublicRooms", node.SelectSingleNode("EnablePublicRoom").InnerText);

                if (node.SelectSingleNode("EnableEntity") != null)
                    HttpContext.Current.Session.Add("EnableEntity", node.SelectSingleNode("EnableEntity").InnerText);

                if (node.SelectSingleNode("EnableBufferZone") != null)
                    HttpContext.Current.Session.Add("EnableBufferZone", node.SelectSingleNode("EnableBufferZone").InnerText);

                if (node.SelectSingleNode("OrgSetupTime") != null) //FB 2398
                    HttpContext.Current.Session.Add("OrgSetupTime", node.SelectSingleNode("OrgSetupTime").InnerText);

                if (node.SelectSingleNode("OrgTearDownTime") != null)//FB 2398
                    HttpContext.Current.Session.Add("OrgTearDownTime", node.SelectSingleNode("OrgTearDownTime").InnerText);

                if (node.SelectSingleNode("EnableAcceptDecline") != null) //FB 2419
                    HttpContext.Current.Session.Add("EnableAcceptDecline", node.SelectSingleNode("EnableAcceptDecline").InnerText);

                if (node.SelectSingleNode("rolename") != null)
                    HttpContext.Current.Session.Add("UsrRoleName", node.SelectSingleNode("rolename").InnerText);

                if (node.SelectSingleNode("level") != null)
                    HttpContext.Current.Session.Add("UsrLevel", node.SelectSingleNode("level").InnerText);

                nodes = node.SelectNodes("level"); //iphone
                if (nodes.Count > 1)
                {
                    //nodes[1].InnerText;
                    if (nodes[1].InnerText != null)
                    {
                        HttpContext.Current.Session.Add("UsrLevel2", nodes[1].InnerText);
                    }
                }
                if (node.SelectSingleNode("crossaccess") != null)
                    HttpContext.Current.Session.Add("UsrCrossAccess", node.SelectSingleNode("crossaccess").InnerText);

                if (HttpContext.Current.Session["roomExpandLevel"] != null)
                {
                    if (HttpContext.Current.Session["roomExpandLevel"].ToString().ToUpper().Equals("LIST"))
                    {
                        HttpContext.Current.Session.Add("roomListView", "list");
                    }
                    else
                    {
                        HttpContext.Current.Session.Add("roomListView", "level");
                    }
                }

                if (node.SelectSingleNode("approver") != null)
                    approver = node.SelectSingleNode("approver").InnerText;

                if (node.SelectSingleNode("menuMask") != null)
                    menuMask = node.SelectSingleNode("menuMask").InnerText;

                if (menuMask != "")
                {
                    HttpContext.Current.Session.Add("sMenuMask", menuMask);
                    //FB 1779 - Start
                    string[] mary = menuMask.ToString().Split('-');
                    string[] mmary = mary[1].Split('+');
                    string[] ccary = mary[0].Split('*');
                    int usrmenu = Convert.ToInt32(ccary[1]);
                    bool isExpuser = Convert.ToBoolean((((usrmenu & 1) == 1) && ((usrmenu & 2) == 2)) || ((((usrmenu & 2) == 2)  && ((usrmenu & 64) == 64))) );
                    
                    if (isExpuser)
                        HttpContext.Current.Session.Add("isExpressUser", "1");
                    else
                        HttpContext.Current.Session.Add("isExpressUser", "0");

                    bool isExpuserAdv = Convert.ToBoolean(((((usrmenu & 2) == 2) && ((usrmenu & 64) == 64)) && ((usrmenu & 16) == 16)));  //|| (((usrmenu & 2) == 2) && ((usrmenu & 64) == 64)) //FB 2394
                    
                    if (isExpuserAdv)
                        HttpContext.Current.Session.Add("isExpressUserAdv", "1");
                    else
                        HttpContext.Current.Session.Add("isExpressUserAdv", "0");
                    //FB 1779 - end

                    //FB 2429 - Starts
                    bool isExpuserManage = Convert.ToBoolean(((((usrmenu & 2) == 2) && ((usrmenu & 64) == 64))));
                    
                    if (isExpuserManage && isExpuser)
                        HttpContext.Current.Session.Add("isExpressManage", "1");
                    else
                        HttpContext.Current.Session.Add("isExpressManage", "0");
                    //FB 2429 - End

                }

                if (NewXmlDoc.DocumentElement.SelectSingleNode("newUser") != null)
                {
                    newuser = NewXmlDoc.DocumentElement.SelectSingleNode("newUser").InnerText;
                }

                //Commented the below codes during FB 1820 as unused
                /*
                 * 
                if (NewXmlDoc.DocumentElement.SelectSingleNode("globalInfo/emailSystem") != null)
                    node = NewXmlDoc.DocumentElement.SelectSingleNode("globalInfo/emailSystem");
                
                EmailSeverDict = new Dictionary<string, string>();

                if (node.SelectSingleNode("companyEmail") != null)
                    EmailSeverDict.Add("ce", node.SelectSingleNode("companyEmail").InnerText);

                if (node.SelectSingleNode("companyEmail") != null)
                    EmailSeverDict.Add("rs", node.SelectSingleNode("remoteServer").InnerText);
                {
                    if (node.SelectSingleNode("remoteServer").InnerText == "1")
                    {
                        if (node.SelectSingleNode("serverAddress") != null)
                        EmailSeverDict.Add("sa", node.SelectSingleNode("serverAddress").InnerText);
                        if (node.SelectSingleNode("accountLogin") != null)
                        EmailSeverDict.Add("al", node.SelectSingleNode("accountLogin").InnerText);
                        if (node.SelectSingleNode("accountPwd") != null)
                        EmailSeverDict.Add("ap", node.SelectSingleNode("accountPwd").InnerText);
                        if (node.SelectSingleNode("portNo") != null)
                        EmailSeverDict.Add("pn", node.SelectSingleNode("portNo").InnerText);
                        if (node.SelectSingleNode("connectionTimeout") != null)
                        EmailSeverDict.Add("ct", node.SelectSingleNode("connectionTimeout").InnerText);
                    }
                }
                
                if (EmailSeverDict != null)
                    HttpContext.Current.Session.Add("EmailSeverDictioary", EmailSeverDict);
                
                EmailSeverDict = null;
                
                */
                //FB 2501 Starts
                if (NewXmlDoc.DocumentElement.SelectSingleNode("globalInfo/ServerOptions/StartMode") != null)
                    HttpContext.Current.Session.Add("StartMode", NewXmlDoc.DocumentElement.SelectSingleNode("globalInfo/ServerOptions/StartMode").InnerText);
                //FB 2501 Ends

                if (NewXmlDoc.DocumentElement.SelectSingleNode("globalInfo/lotus") != null)
                    node = NewXmlDoc.DocumentElement.SelectSingleNode("globalInfo/lotus");

                if (node.SelectSingleNode("lnLoginName") != null)
                    HttpContext.Current.Session.Add("lnLoginName", node.SelectSingleNode("lnLoginName").InnerText);

                if (node.SelectSingleNode("lnLoginPwd") != null)
                    HttpContext.Current.Session.Add("lnLoginPwd", node.SelectSingleNode("lnLoginPwd").InnerText);

                if (node.SelectSingleNode("lnDBPath") != null)
                    HttpContext.Current.Session.Add("lnDBPath", node.SelectSingleNode("lnDBPath").InnerText);

                if (NewXmlDoc.DocumentElement.SelectSingleNode("globalInfo/contactDetails") != null)
                    node = NewXmlDoc.DocumentElement.SelectSingleNode("globalInfo/contactDetails");

                if (node.SelectSingleNode("name") != null)
                {
                    //FB 1888 start
                    String contactname = node.SelectSingleNode("name").InnerText;
                    contactname = contactname.Replace("\"", "||").Replace("\'", "!!");
                    HttpContext.Current.Session.Add("contactName", contactname);
                    //FB 1888 end
                }

                if (node.SelectSingleNode("email") != null)
                    HttpContext.Current.Session.Add("contactEmail", node.SelectSingleNode("email").InnerText);

                if (node.SelectSingleNode("phone") != null)
                    HttpContext.Current.Session.Add("contactPhone", node.SelectSingleNode("phone").InnerText);

                if (node.SelectSingleNode("additionInfo") != null)
                    HttpContext.Current.Session.Add("contactAddInfo", node.SelectSingleNode("additionInfo").InnerText);

                HttpContext.Current.Session.Add("userInterface", "2");
                //NewXmlDoc.DocumentElement.SelectSingleNode("/user/userInterface").InnerText

                HttpContext.Current.Session.Add("IMTalker", "");

                HttpContext.Current.Session.Add("IMEnabled", "0");
                //NewXmlDoc.DocumentElement.SelectSingleNode("globalInfo/IMTalk/IMEnabled").InnerText

                HttpContext.Current.Session.Add("ImRefreshRate", "1000");
                //NewXmlDoc.DocumentElement.SelectSingleNode("globalInfo/IMTalk/refreshTime").InnerText

                if (NewXmlDoc.DocumentElement.SelectSingleNode("globalInfo/primaryBridge") != null)
                    HttpContext.Current.Session.Add("primaryBridge", NewXmlDoc.DocumentElement.SelectSingleNode("globalInfo/primaryBridge").InnerText);

                NewXmlDoc = null;
                #endregion

                //Get Old User   
                inxml = "<login>"+
                    "<userID>" + HttpContext.Current.Session["userID"].ToString() + "</userID>"+
                    "<user><userID>" + HttpContext.Current.Session["userID"].ToString() + "</userID></user>"+
                    "</login>";
                //FB 2027 Start
                outXML = obj.CallMyVRMServer("GetOldUser", inxml, HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());
                //outXML = obj.CallCOM("GetOldUser", inxml, HttpContext.Current.Application["COM_ConfigPath"].ToString());
                //FB 2027 End
                
                XmlDoc = new XmlDocument();

                #region Assign User Bases Session Variables

                if (outXML.IndexOf("<error>") < 0)
                {
                    String usrEmailLang = "", usrBaseLang = "1", SendSurveyEmail="", Secured = ""; //FB 2283 FB 2348 //FB 2595

                    XmlDoc.LoadXml(outXML);
                    uptz = XmlDoc.DocumentElement.SelectSingleNode("timeZone").InnerText;
                    nodes = XmlDoc.DocumentElement.SelectNodes("timezones/timezone");

                    //FB 2468 - Starts
                    //for (int i = 0; i < nodes.Count; i++)
                    foreach (XmlNode Node in nodes)
                    {
                        if (Node.SelectSingleNode("timezoneID") != null)
                            if (Node.SelectSingleNode("timezoneID").InnerText == uptz)
                            {
                                if (Node.SelectSingleNode("timezoneName") != null)
                                    HttpContext.Current.Session.Add("uptz", Node.SelectSingleNode("timezoneName").InnerText);

                                HttpContext.Current.Session.Add("timezoneID", Node.SelectSingleNode("timezoneID").InnerText);
                                //FB 1201
                            }
                    }
                    //FB 2468 - End

                    // FB 2608 Start
                    if (XmlDoc.SelectSingleNode("//oldUser/EnableVNOCselection") != null)
                        HttpContext.Current.Session.Add("EnableVNOCselection", XmlDoc.SelectSingleNode("//oldUser/EnableVNOCselection").InnerText);
                    // FB 2608 End

                    if (XmlDoc.SelectSingleNode("//oldUser/SavedSearch") != null)
                        HttpContext.Current.Session.Add("SearchID", XmlDoc.SelectSingleNode("//oldUser/SavedSearch").InnerText);

                    if (XmlDoc.SelectSingleNode("//oldUser/emailClient") != null)
                        HttpContext.Current.Session.Add("emailClient", XmlDoc.SelectSingleNode("//oldUser/emailClient").InnerText);
                    //FB Case 526: Saima
                    if (XmlDoc.SelectSingleNode("//oldUser/dateFormat") != null)
                    {
                        if (XmlDoc.SelectSingleNode("//oldUser/dateFormat").InnerText != "")
                        {
                            HttpContext.Current.Session.Add("FormatDateType", XmlDoc.SelectSingleNode("//oldUser/dateFormat").InnerText);
                        }
                        else
                        {
                            HttpContext.Current.Session.Add("FormatDateType", "MM/dd/yyyy");
                        }
                    }

                    //FB 1830
                    if (HttpContext.Current.Session["FormatDateType"].ToString() == "dd/MM/yyyy")
                    {
                        HttpContext.Current.Session["CurrencyFormat"] = ns_MyVRMNet.vrmCurrencyFormat.bound;
                        HttpContext.Current.Session["NumberFormat"] = ns_MyVRMNet.vrmNumberFormat.european;
                    }
                    else
                    {
                        HttpContext.Current.Session["CurrencyFormat"] = ns_MyVRMNet.vrmCurrencyFormat.dollar;
                        HttpContext.Current.Session["NumberFormat"] = ns_MyVRMNet.vrmNumberFormat.american;
                    }

                    if (XmlDoc.SelectSingleNode("//oldUser/Audioaddon") != null)
                    {
                        HttpContext.Current.Session.Add("AudioAddon", XmlDoc.SelectSingleNode("//oldUser/Audioaddon").InnerText);
                    }

                    //FB 2283 start
                    if (XmlDoc.SelectSingleNode("//oldUser/languageID") != null)
                        if (XmlDoc.SelectSingleNode("//oldUser/languageID").InnerText != "")
                            usrBaseLang = XmlDoc.SelectSingleNode("//oldUser/languageID").InnerText.Trim();
                    
                    HttpContext.Current.Session.Add("UsrBaseLang", usrBaseLang);

                    if (XmlDoc.SelectSingleNode("//oldUser/EmailLang") != null)
                        if (XmlDoc.SelectSingleNode("//oldUser/EmailLang").InnerText != "")
                            usrEmailLang = XmlDoc.SelectSingleNode("//oldUser/EmailLang").InnerText.Trim();
                    
                    HttpContext.Current.Session.Add("UsrEmailLangID", usrEmailLang);
                    //FB 2283 end
                    //FB 2348 Start
                    if (XmlDoc.SelectSingleNode("//oldUser/SendSurveyEmail") != null)
                        if (XmlDoc.SelectSingleNode("//oldUser/SendSurveyEmail").InnerText != "")
                            SendSurveyEmail = XmlDoc.SelectSingleNode("//oldUser/SendSurveyEmail").InnerText.Trim();

                    HttpContext.Current.Session.Add("SendSurveyEmail", SendSurveyEmail);
                    //FB 2348

                    //FB 2595 Start
                    if (XmlDoc.SelectSingleNode("//oldUser/Secured") != null)
                        if (XmlDoc.SelectSingleNode("//oldUser/Secured").InnerText != "")
                            Secured = XmlDoc.SelectSingleNode("//oldUser/Secured").InnerText.Trim();

                    HttpContext.Current.Session.Add("Secured", Secured);
                    //FB 2595

                }
                #endregion

                //FB 2283 start
                obj.SetOrgSession(orgid);
                //obj.SetCSSFilePath(); //These two functions are calling from obj.SetOrgSession
                //obj.SetOrgTextChangeXML();
                //FB 2283 end

                //obj.GetSystemDateTime(HttpContext.Current.Application["COM_ConfigPath"].ToString());
                obj.GetSystemDateTime(HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());//FB 2027

                if (HttpContext.Current.Session["CompanyLogo"] != null)
                    CompanyLogo = HttpContext.Current.Session["CompanyLogo"].ToString();

                if (HttpContext.Current.Session["OrgBanner1024Path"] != null)
                    OrgBanner1024Path = HttpContext.Current.Session["OrgBanner1024Path"].ToString();

                if (HttpContext.Current.Session["OrgBanner1600Path"] != null)
                    OrgBanner1600Path = HttpContext.Current.Session["OrgBanner1600Path"].ToString();

                CustomizationUtil.CSSReplacementUtility cssUtil = new CustomizationUtil.CSSReplacementUtility();
                cssUtil.ApplicationPath = HttpContext.Current.Server.MapPath("..") + "//" + HttpContext.Current.Session["language"].ToString(); //FB 1830 - Translation Menu
                
                String orgID = "";
                orgID = "Org_" + orgid;
                cssUtil.FolderName = orgID;
                cssUtil.CreateOrgStyles();
                SetOrganizationThemes();

                //FB 1639 - starts here

                obj.GetSysLicenseInfo();
                HttpContext.Current.Session.Remove("OrganizationsLimit");
                HttpContext.Current.Session.Add("OrganizationsLimit", obj.maxOrganizations.ToString());

                //FB 1639 - ends here

                if(newuser == "1")
                {
                    //FB 1830
                    HttpContext.Current.Response.Redirect("..//" + HttpContext.Current.Session["language"].ToString() + "//LicenseAgreement.aspx?a=" + HttpContext.Current.Session["userID"] + "&p=&u=");
                }
                outXML = "";
                
                return outXML;
            }
            catch (Exception ex)
            {
                log.Trace("GetHomeCommand: " + ex.Message);
                return outXML;
            }
        }

        /// <summary>
        /// RemoveSessionVariables
        /// </summary>
        private void RemoveSessionVariables()
        {
            try
            {
                //HttpContext.Current.Session.Remove("errMsg"); //FB 1820
                HttpContext.Current.Session.Remove("userFirstName");
                HttpContext.Current.Session.Remove("userLastName");
                HttpContext.Current.Session.Remove("userName");
                HttpContext.Current.Session.Remove("userEmail");
                HttpContext.Current.Session.Remove("roleID");
                HttpContext.Current.Session.Remove("admin");
                HttpContext.Current.Session.Remove("realtimeType");
                HttpContext.Current.Session.Remove("dialoutEnabled");
                HttpContext.Current.Session.Remove("defaultPublic");
                HttpContext.Current.Session.Remove("enableAV");
                HttpContext.Current.Session.Remove("enableParticipants");
                HttpContext.Current.Session.Remove("timeFormat");
                HttpContext.Current.Session.Remove("timeZoneDisplay");
                HttpContext.Current.Session.Remove("tickerStatus");
                HttpContext.Current.Session.Remove("tickerSpeed");
                HttpContext.Current.Session.Remove("tickerPosition");
                HttpContext.Current.Session.Remove("tickerDisplay");
                HttpContext.Current.Session.Remove("tickerBackground");
                HttpContext.Current.Session.Remove("rssFeedLink");
                HttpContext.Current.Session.Remove("tickerStatus1");
                HttpContext.Current.Session.Remove("tickerSpeed1");
                HttpContext.Current.Session.Remove("tickerPosition1");
                HttpContext.Current.Session.Remove("tickerDisplay1");
                HttpContext.Current.Session.Remove("tickerBackground1");
                HttpContext.Current.Session.Remove("rssFeedLink1");
                HttpContext.Current.Session.Remove("organizationID");
                HttpContext.Current.Session.Remove("TransferToNet");
                HttpContext.Current.Session.Remove("SystemEndTime");
                HttpContext.Current.Session.Remove("SystemStartTime");
                HttpContext.Current.Session.Remove("dynInvite");
                HttpContext.Current.Session.Remove("doubleBooking");
                HttpContext.Current.Session.Remove("P2PEnable");
                HttpContext.Current.Session.Remove("recurEnable");
                HttpContext.Current.Session.Remove("organizationName");
                HttpContext.Current.Session.Remove("DefaultConferenceType");
                HttpContext.Current.Session.Remove("EnableRoomConfType");
                HttpContext.Current.Session.Remove("EnableAudioVideoConfType");
                HttpContext.Current.Session.Remove("EnableAudioOnlyConfType");
                HttpContext.Current.Session.Remove("DefaultCalendarToOfficeHours");
                HttpContext.Current.Session.Remove("roomExpandLevel");
                HttpContext.Current.Session.Remove("RoomLimit");
                HttpContext.Current.Session.Remove("McuLimit");
                HttpContext.Current.Session.Remove("MCUEnchancedLimit");//FB 2486
                HttpContext.Current.Session.Remove("UserLimit");
                HttpContext.Current.Session.Remove("VideoRooms");
                HttpContext.Current.Session.Remove("NonVideoRooms");
                HttpContext.Current.Session.Remove("EndPoints");
                HttpContext.Current.Session.Remove("ExchangeUserLimit");
                HttpContext.Current.Session.Remove("DominoUserLimit");
                HttpContext.Current.Session.Remove("MobileUserLimit"); //FB 1979
                HttpContext.Current.Session.Remove("foodModule");
                HttpContext.Current.Session.Remove("roomModule");
                HttpContext.Current.Session.Remove("hkModule");
                HttpContext.Current.Session.Remove("EnableEntity");
                HttpContext.Current.Session.Remove("EnableBufferZone");
                HttpContext.Current.Session.Remove("OrgSetupTime"); //FB 2398
                HttpContext.Current.Session.Remove("OrgTearDownTime");//FB 2398
                HttpContext.Current.Session.Remove("UsrRoleName");
                HttpContext.Current.Session.Remove("UsrLevel");
                HttpContext.Current.Session.Remove("UsrLevel2"); //iphone
                HttpContext.Current.Session.Remove("UsrCrossAccess");
                HttpContext.Current.Session.Remove("roomListView");
                HttpContext.Current.Session.Remove("EmailSeverDictioary");
                HttpContext.Current.Session.Remove("lnLoginName");
                HttpContext.Current.Session.Remove("lnLoginPwd");
                HttpContext.Current.Session.Remove("lnDBPath");
                HttpContext.Current.Session.Remove("contactName");
                HttpContext.Current.Session.Remove("contactEmail");
                HttpContext.Current.Session.Remove("contactPhone");
                HttpContext.Current.Session.Remove("contactAddInfo");
                HttpContext.Current.Session.Remove("userInterface");
                HttpContext.Current.Session.Remove("IMTalker");
                HttpContext.Current.Session.Remove("IMEnabled");
                HttpContext.Current.Session.Remove("ImRefreshRate");
                HttpContext.Current.Session.Remove("primaryBridge");
                HttpContext.Current.Session.Remove("SearchID");
                HttpContext.Current.Session.Remove("emailClient");
                HttpContext.Current.Session.Remove("FormatDateType");
                HttpContext.Current.Session.Remove("ConferenceCode");
                HttpContext.Current.Session.Remove("LeaderPin");
                HttpContext.Current.Session.Remove("AdvAvParams");
                HttpContext.Current.Session.Remove("AudioParams");
                //FB 2359 Start
                HttpContext.Current.Session.Remove("EnableRoomParam");
                //FB 2359 End
                HttpContext.Current.Session.Remove("defaultConfTemplate"); //FB 1719
                //FB 1779 Starts
                HttpContext.Current.Session.Remove("workPhone");
                HttpContext.Current.Session.Remove("cellPhone");
                HttpContext.Current.Session.Remove("isExpressUser");
                //FB 1779 End
               //FB 1830
                HttpContext.Current.Session.Remove("language");
                HttpContext.Current.Session.Remove("languageid"); //FB 1881
                HttpContext.Current.Session.Remove("SystemError"); //FB 1881
                HttpContext.Current.Session.Remove("UsrBaseLang"); //FB 2283
                HttpContext.Current.Session.Remove("UsrEmailLangID"); //FB 2283
                HttpContext.Current.Session.Remove("EnableConfPassword");//FB 2359
                HttpContext.Current.Session.Remove("EnablePublicConf");//FB 2359
                HttpContext.Current.Session.Remove("SendSurveyEmail"); //FB 2348
                HttpContext.Current.Session.Remove("EnableAcceptDecline");//FB 2419
                HttpContext.Current.Session.Remove("isExpressUserAdv");//FB 2429
                HttpContext.Current.Session.Remove("isExpressManage");//FB 2429
                HttpContext.Current.Session.Remove("MaxGuestRooms");//FB 2426
                HttpContext.Current.Session.Remove("VMRRooms");//FB 2586
				HttpContext.Current.Session.Remove("EnableVNOCselection");//FB 2608
				HttpContext.Current.Session.Remove("Cloud");//FB 2262 -J //FB 2599
                HttpContext.Current.Session.Remove("EnablePublicRooms");//FB 2594
                HttpContext.Current.Session.Remove("Secured");//FB 2595
            }
            catch (Exception ex)
            {
                log.Trace("RemoveSessionValues: " + ex.Message);
            }
        }
        #endregion

        #region SetOrganizationThemes
        /// <summary>
        /// SetOrganizationThemes
        /// </summary>
        private void SetOrganizationThemes()
        {
            string inXml = "";
            string outxml = "";
            XmlDocument xd = null;
            XmlNode node = null;
            string logoImage;
            string lobytopimage;
            string lobytophighimage;
            byte[] imageData = null;
            try
            {
                inXml = "<GetOrgImages>";
                inXml += obj.OrgXMLElement();
                inXml += "</GetOrgImages>";
                
                outxml = obj.CallCommand("GetOrgImages", inXml);
                if (outxml.IndexOf("<error>") < 0)
                {
                    xd = new XmlDocument();
                    xd.LoadXml(outxml);

                    node = xd.SelectSingleNode("//GetOrgImages/Logo");
                    logoImage = node.InnerText.Trim();
                    imageData = imageObj.ConvertBase64ToByteArray(logoImage);

                    if (imageData != null)
                    {
                        if (File.Exists(CompanyLogo))
                            File.Delete(CompanyLogo);

                        imageObj.WriteToFile(CompanyLogo, ref imageData);
                    }

                    node = xd.SelectSingleNode("//GetOrgImages/LobyTop");
                    lobytopimage = node.InnerText.Trim();
                    //FB 1633 start
                    imageData = null;
                    HttpContext.Current.Session.Remove("orgStdBannerPath");
                    HttpContext.Current.Session.Remove("orgHighBannerPath");
                    //FB 1830 - Translation Menu
                    HttpContext.Current.Session.Add("orgStdBannerPath", HttpContext.Current.Server.MapPath(".").ToString() + "\\Organizations\\Org_" + HttpContext.Current.Session["organizationID"].ToString() + "\\CSS\\Mirror\\Image\\lobbytop1024.jpg");
                    HttpContext.Current.Session.Add("orgHighBannerPath", HttpContext.Current.Server.MapPath(".").ToString() + "\\Organizations\\Org_" + HttpContext.Current.Session["organizationID"].ToString() + "\\CSS\\Mirror\\Image\\lobbytop1600.jpg");
                    OrgBanner1024Path = HttpContext.Current.Session["orgStdBannerPath"].ToString();
                    OrgBanner1600Path = HttpContext.Current.Session["orgHighBannerPath"].ToString();
                    //FB 1633 end
                    imageData = imageObj.ConvertBase64ToByteArray(lobytopimage);
                    if (imageData != null)
                    {
                        if (File.Exists(OrgBanner1024Path))
                            File.Delete(OrgBanner1024Path);

                        imageObj.WriteToFile(OrgBanner1024Path, ref imageData);
                    }
                    else
                        File.Copy(HttpContext.Current.Session["BlankBannerPath"].ToString(), OrgBanner1024Path, true);//FB 1633

                    //Commented For FB 1633 start
                    //node = xd.SelectSingleNode("//GetOrgImages/LobyTopHigh");
                    //lobytophighimage = node.InnerText.Trim();
                    //imageData = null;
                    //imageData = imageObj.ConvertBase64ToByteArray(lobytophighimage);
                    //Commented For FB 1633 end

                    if (imageData != null)
                    {
                        if (File.Exists(OrgBanner1600Path))
                            File.Delete(OrgBanner1600Path);

                        imageObj.WriteToFile(OrgBanner1600Path, ref imageData);
                    }
                    else
                        File.Copy(HttpContext.Current.Session["BlankBannerPath"].ToString(), OrgBanner1600Path, true);//FB 1633

                }
                else
                {
                    outXML = outxml;
                }
            }
            catch (Exception ex)
            {
                log.Trace("SetOrganizationThemes: " + ex.Message);
            }
        }
        #endregion
        
        //FB 1633 - New Method added
        #region SetSitePaths

        public void SetSitePaths()
        {
            try
            {
                String smPath = HttpContext.Current.Server.MapPath(".."); //FB 1830 - Translation Menu
                HttpContext.Current.Session.Remove("SiteLogPath");
                HttpContext.Current.Session.Remove("SiteStdBanner");
                HttpContext.Current.Session.Remove("SiteHighBanner");
                HttpContext.Current.Session.Remove("BlankBannerPath");
                HttpContext.Current.Session.Remove("BlankLogoPath");

                HttpContext.Current.Session.Add("SiteLogPath",smPath + "\\image\\company-logo\\SiteLogo.jpg");
                HttpContext.Current.Session.Add("SiteStdBanner", smPath + "\\image\\company-logo\\StdBanner.jpg");
                HttpContext.Current.Session.Add("SiteHighBanner", smPath + "\\image\\company-logo\\HighBanner.jpg");
                HttpContext.Current.Session.Add("BlankBannerPath", smPath + "\\image\\blank_images\\StdBanner.jpg");
                HttpContext.Current.Session.Add("BlankLogoPath", smPath + "\\image\\blank_images\\SiteLogo.jpg");
            }
            catch(Exception ex)
            {
                log.Trace(ex.Message);
            }
        }
        #endregion

        #region SetSiteThemes
        /// <summary>
        /// SetSiteThemes
        /// </summary>
        public void SetSiteThemes()
        {
            string inXml = "";
            XmlDocument xd = null;
            XmlNode node = null;
            string logoImage;
            string lobytopimage;
            string lobytophighimage;
            byte[] imageData = null;
            try
            {
                inXml = "<GetSiteImage>";
                inXml += "<UserId>11</UserId>";
                inXml += "</GetSiteImage>";

                outXML = obj.CallCommand("GetSiteImage", inXml);
                errMessage = outXML;

                if (outXML.IndexOf("<error>") < 0)
                {
                    xd = new XmlDocument();
                    xd.LoadXml(outXML);

                    //FB 1943
                    node = xd.SelectSingleNode("//GetSiteImage/CompanyMailExtension");
                    if (node != null)
                    {
                        if (node.InnerText.Trim() != "")
                            mailextn = node.InnerText.Trim();
                    }
                    //FB 1943

                    node = xd.SelectSingleNode("//GetSiteImage/Image");
                    logoImage = node.InnerText.Trim();
                    imageData = imageObj.ConvertBase64ToByteArray(logoImage);

                    CompanyLogo = HttpContext.Current.Session["SiteLogPath"].ToString();//FB 1633
                    if (imageData != null)
                    {
                        if (File.Exists(CompanyLogo))
                            File.Delete(CompanyLogo);

                        imageObj.WriteToFile(CompanyLogo, ref imageData);
                    }
                    else
                        File.Copy(HttpContext.Current.Session["BlankLogoPath"].ToString(), CompanyLogo, true);//FB 1633

                    node = xd.SelectSingleNode("//GetSiteImage/StdBanner");
                    lobytopimage = node.InnerText.Trim();

                    imageData = null;
                    imageData = imageObj.ConvertBase64ToByteArray(lobytopimage);

                    OrgBanner1024Path = HttpContext.Current.Session["SiteStdBanner"].ToString(); //FB 1633
                    if (imageData != null)
                    {
                        if (File.Exists(OrgBanner1024Path))
                            File.Delete(OrgBanner1024Path);

                        imageObj.WriteToFile(OrgBanner1024Path, ref imageData);
                    }
                    else
                        File.Copy(HttpContext.Current.Session["BlankBannerPath"].ToString(), OrgBanner1024Path, true);//FB 1633

                    node = xd.SelectSingleNode("//GetSiteImage/HighBanner");
                    lobytophighimage = node.InnerText.Trim();

                    OrgBanner1600Path = HttpContext.Current.Session["SiteHighBanner"].ToString(); //FB 1633
                    imageData = null;
                    imageData = imageObj.ConvertBase64ToByteArray(lobytophighimage);
                    if (imageData != null)
                    {
                        if (File.Exists(OrgBanner1600Path))
                            File.Delete(OrgBanner1600Path);

                        imageObj.WriteToFile(OrgBanner1600Path, ref imageData);
                    }
                    else
                        File.Copy(HttpContext.Current.Session["BlankBannerPath"].ToString(), OrgBanner1600Path, true);//FB 1633

                    node = xd.SelectSingleNode("//GetSiteImage/Companymessage");
                    if(node.InnerText.Trim() != "")
                        companyTagline = node.InnerText.Trim();

                }
            }
            catch (Exception ex)
            {
                log.Trace("SetSiteThemes: " + ex.Message);
            }
        }
        #endregion

        #region GenerateHomeURL
        /// <summary>
        /// Generate HomeURL
        /// </summary>
        /// <returns></returns>
        public string GenerateHomeURL()
        {
            string cururl = "";
            string homeurl = "";
            int pos = 0;
            cururl = "http://" + HttpContext.Current.Request.ServerVariables["HTTP_HOST"] + HttpContext.Current.Request.ServerVariables["URL"];
            homeurl = "";
            pos = 0;
            if (cururl.LastIndexOf("/en/") != 0)
            {
                pos = cururl.LastIndexOf("/en/");
            }
            else if (cururl.LastIndexOf("/fr/") != 0)
            {
                pos = cururl.LastIndexOf("/en/");
            }
            else if (cururl.LastIndexOf("/ch/") != 0)
            {
                pos = cururl.LastIndexOf("/en/");
            }
            if (pos > 4)
            {
                homeurl = cururl.Substring(0, pos + 3);
            }
            return homeurl;
        }
        #endregion

        #region  cookiedectASP
        /// <summary>
        /// cookiedect.asp Converted as method,Code lines currently using in application.
        /// </summary>
        public void cookiedectASP()
        {
            Boolean logined = true;
            System.Collections.Specialized.NameValueCollection CookieValues = null;
            try
            {
                if (HttpContext.Current.Request.Cookies["VRMuser"] == null)
                {
                    logined = false;
                }
                else
                {
                    if (HttpContext.Current.Request.Cookies["VRMuser"].HasKeys)
                    {
                        CookieValues = HttpContext.Current.Request.Cookies["VRMuser"].Values;
                        for (int j = 0; j < CookieValues.Count; j++)
                        {
                            if (CookieValues[j] == "")
                                logined = false;
                        }
                    }
                    else
                    {
                        logined = false;
                    }
                }
                if (logined)
                {
                    //Need to check this
                    HttpContext.Current.Response.Redirect("genlogin.aspx?a=" + HttpContext.Current.Request.Cookies["VRMuser"]["act"] + "&p=" + HttpContext.Current.Request.Cookies["VRMuser"]["pwd"]);
                }

            }
            catch (System.Threading.ThreadAbortException) { }
            catch (Exception ex)
            {
                log.Trace("cookiedectASP" + ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        //FB 1861 //Method changed for FB 2052
        #region Load Org Holidays
        public void LoadOrgHolidays()
        {
            String inxml = "";
            String outXML = "";
            XmlDocument XmlDoc = null;
            String holidays = "";
            String holidayString = "";
            String cssString = "";
            try
            {
                //Changed Cache to Session in this whole method for FB 2052
                if (HttpContext.Current.Session["HolidaysString"] == null)
                    HttpContext.Current.Session["HolidaysString"] = "";

                if (HttpContext.Current.Session["HolidaysCSSString"] == null)
                    HttpContext.Current.Session["HolidaysCSSString"] = "";

                if (HttpContext.Current.Session["Holidays"] != null)
                    HttpContext.Current.Session.Remove("Holidays");

                inxml = "<GetOrgHolidays>" +
                   "<userID>" + HttpContext.Current.Session["userID"].ToString() + "</userID>" +
                   "<organizationID>" + HttpContext.Current.Session["organizationID"].ToString() + "</organizationID>" +
                   "</GetOrgHolidays>";

                outXML = obj.CallCommand("GetOrgHolidays", inxml);

                XmlDoc = new XmlDocument();
                XmlDoc.LoadXml(outXML);
                XmlNode ndeHolidays = XmlDoc.SelectSingleNode("SystemHolidays");
                XmlNodeList ndeHolidayString = XmlDoc.SelectNodes("SystemHolidays/Holidays/Holiday");
                if (ndeHolidays != null)
                {
                    if (HttpContext.Current.Session["Holidays"] == null)
                        HttpContext.Current.Session.Add("Holidays", ndeHolidays.InnerXml);
                    else
                        HttpContext.Current.Session["Holidays"] = ndeHolidays.InnerXml;
                }

                if (ndeHolidayString != null)
                {

                    for (Int32 n = 0; n < ndeHolidayString.Count; n++)
                    {
                        if (holidayString == "")
                            holidayString = ndeHolidayString[n].ChildNodes[0].InnerText + ";holy" + n + ";" + ndeHolidayString[n].ChildNodes[2].InnerText;
                        else
                            holidayString += "|" + ndeHolidayString[n].ChildNodes[0].InnerText + ";holy" + n + ";" + ndeHolidayString[n].ChildNodes[2].InnerText;

                        if (cssString == "")
                            cssString = ".calendar tbody td.holy" + n + " {font-weight: bold;color: " + ndeHolidayString[n].ChildNodes[2].InnerText + ";}";
                        else
                            cssString += " " + ".calendar tbody td.holy" + n + " {font-weight: bold;color: " + ndeHolidayString[n].ChildNodes[2].InnerText + ";}";

                    }
                }


                HttpContext.Current.Session["HolidaysString"] = holidayString;

                if (HttpContext.Current.Session["HolidaysString"] == null)
                    HttpContext.Current.Session.Add("HolidaysString", holidayString);
                else
                    HttpContext.Current.Session["HolidaysString"] = holidayString;

                HttpContext.Current.Session["HolidaysCSSString"] = cssString;

                if (HttpContext.Current.Session["HolidaysCSSString"] == null)
                    HttpContext.Current.Session.Add("HolidaysCSSString", cssString);
                else
                    HttpContext.Current.Session["HolidaysCSSString"] = cssString;
                //}
                //else
                //{
                //    if (HttpContext.Current.Session["Holidays"] == null)
                //        HttpContext.Current.Session.Add("Holidays", HttpContext.Current.Session["Holidays"].ToString());
                //    else
                //        HttpContext.Current.Session["Holidays"] = HttpContext.Current.Session["Holidays"].ToString();

                //    if (HttpContext.Current.Session["HolidaysString"] == null)
                //        HttpContext.Current.Session.Add("HolidaysString", HttpContext.Current.Session["HolidaysString"].ToString());
                //    else
                //        HttpContext.Current.Session["HolidaysString"] = HttpContext.Current.Session["HolidaysString"].ToString();

                //    if (HttpContext.Current.Session["HolidaysCSSString"] == null)
                //        HttpContext.Current.Session.Add("HolidaysCSSString", HttpContext.Current.Session["HolidaysCSSString"].ToString());
                //    else
                //        HttpContext.Current.Session["HolidaysCSSString"] = HttpContext.Current.Session["HolidaysCSSString"].ToString();
                //}
                //FB 1830 - Translation
                inxml = "<login>" + obj.OrgXMLElement() + "<userID>" + HttpContext.Current.Session["userID"].ToString() + "</userID></login>";
                outXML = obj.CallCommand("GetLanguageTexts", inxml);

                if (HttpContext.Current.Session["TranslationText"] == null)
                    HttpContext.Current.Session.Add("TranslationText", outXML);
                else
                    HttpContext.Current.Session["TranslationText"] = outXML;

            }
            catch (Exception ex)
            {

                log.Trace("LoadOrgHolidays: " + ex.Message);
            }
        }
        #endregion

    }
    #endregion
                
}

﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;

namespace ns_MyVRM
{

    public partial class ManageAudioAddOnBridges : System.Web.UI.Page
    {
        #region Private Data Members

        myVRMNet.NETFunctions obj;
        ns_Logger.Logger log;

        #endregion

        #region ProtectDataMember
        protected System.Web.UI.HtmlControls.HtmlTableCell tdLicencesRemaining;
        
        protected System.Web.UI.WebControls.Label lblTotalUsers;
        protected System.Web.UI.WebControls.Label lblLicencesRemaining;
        protected System.Web.UI.WebControls.Label lblHeader;
        protected System.Web.UI.WebControls.Label lblError;
        protected System.Web.UI.WebControls.DataGrid dgAudioBridges;
        protected System.Web.UI.WebControls.Button btnNewAudioBridge;
        protected System.Web.UI.WebControls.Table tblNoAudioBridges;
        protected System.Web.UI.HtmlControls.HtmlTableRow trNewBridge;
        protected System.Web.UI.WebControls.Table tblPage;//FB 2023

        #endregion

        #region ManageAudioAddOnBridges
        /// <summary>
        /// ManageAudioAddOnBridges
        /// </summary>
        public ManageAudioAddOnBridges()
        {
            obj = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger();
            //
            // TODO: Add constructor logic here
            //
        }
        #endregion

        #region Page_Load
        /// <summary>
        /// Page_Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Session["admin"].ToString().Equals("2"))
                    trNewBridge.Visible = false;

                BindAudioBridges();
            }
            catch (Exception ex)
            {
                log.Trace("Page_Load" + ex.Message);
                lblError.Text = obj.ShowSystemMessage();
            }
        }
        #endregion

        #region BindAudioBridges
        /// <summary>
        /// BindAudioBridges
        /// </summary>
        private void BindAudioBridges()
        {
            try
            {
				//FB 2023 - Starts
                string inXML = "", outXML="", pageNo="1",Alpha="ALL",sortBy="1";
                int totalPages = 1;

                if (Request.QueryString["pageNo"] != null)
                    pageNo = Request.QueryString["pageNo"].ToString();

                inXML = "<login>"
                      + obj.OrgXMLElement()
                      + "  <userID>" + Session["userID"].ToString() + "</userID>"
                      + "  <sortBy>" + sortBy + "</sortBy>" //FB 2023 - End
                      + "  <alphabet>" + Alpha + "</alphabet>"
                      + "  <pageNo>" + pageNo + "</pageNo>"
                      + " <audioaddon>1</audioaddon>"
                      + "</login>";

                outXML = obj.CallMyVRMServer("GetManageUser", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") < 0)
                {
                    DataTable dt = new DataTable();
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);

                    XmlNodeList nodes = xmldoc.SelectNodes("//users/user");
                    totalPages = Convert.ToInt32(xmldoc.SelectSingleNode("//users/totalPages").InnerText);
                    
                    lblTotalUsers.Text = xmldoc.SelectSingleNode("//users/totalNumber").InnerText;
                    lblLicencesRemaining.Text = xmldoc.SelectSingleNode("//users/licensesRemain").InnerText;
                    
                    if (nodes.Count > 0)
                    {
                        dgAudioBridges.Visible = true;
                        tblNoAudioBridges.Visible = false;
                        dt = obj.LoadDataTable(nodes, null);
                        if (!dt.Columns.Contains("Status")) dt.Columns.Add("Status");
                        foreach (DataRow dr in dt.Rows)
                        {
                            dr["Status"] = "Active";
                        }

                        dgAudioBridges.DataSource = dt;
                        dgAudioBridges.DataBind();

                        if (totalPages > 1)//FB 2023
                        {
                            //  Request.QueryString.Remove("pageNo");
                            obj.DisplayPaging(totalPages, Int32.Parse(pageNo), tblPage, "ManageAudioAddOnBridges.aspx?");
                        }

                    }
                    else
                    {
                        dgAudioBridges.Visible = false;
                        tblNoAudioBridges.Visible = true;
                    }
                }
                else
                {
                    lblError.Text = obj.ShowErrorMessage(outXML);
                    lblError.Visible = true;
                }

            }
            catch (Exception ex)
            {
                log.Trace("BindAudioBridges" + ex.Message);
                lblError.Text = obj.ShowSystemMessage();
            }
        }
        #endregion

        #region CreateNewAudioAddonBridge
        /// <summary>
        /// CreateNewAudioAddonBridge
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CreateNewAudioAddonBridge(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("AudioAddOnBridge.aspx");
            }
            catch (Exception ex)
            {
                log.Trace("CreateNewAudioAddonBridge" + ex.Message);
            }
        }
        #endregion

        #region EditAudionAddonBridge
        /// <summary>
        /// EditAudionAddonBridge
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void EditAudionAddonBridge(object sender, DataGridCommandEventArgs e)
        {
            try
            {
                log.Trace("AudioBridgeToEdit :"+ e.Item.Cells[0].Text);
                Response.Redirect("AudioAddOnBridge.aspx?t=" + e.Item.Cells[0].Text);
            }
            catch (Exception ex)
            {
                lblError.Text = ex.StackTrace;
            }
        }
        #endregion

        #region DeleteAudionAddonBridge
        /// <summary>
        /// DeleteAudionAddonBridge
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DeleteAudionAddonBridge(object sender, DataGridCommandEventArgs e)
        {
            try
            {
                String inXML = "";
                inXML += "<login>";
                inXML += obj.OrgXMLElement();
                inXML += "  <userID>" + Session["userID"].ToString() + "</userID>";
                inXML += "  <user>";
                inXML += "      <userID>" + e.Item.Cells[0].Text + "</userID>";
                inXML += "      <action>-1</action>";
                inXML += "  </user>";
                inXML += "</login>";
                
                String outXML = obj.CallMyVRMServer("ChangeUserStatus", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                
                if (outXML.IndexOf("<error>") < 0)
                {
                    lblError.Text = obj.GetTranslatedText("Operation Successful!");
                    BindAudioBridges();
                }
                else
                    lblError.Text = obj.ShowErrorMessage(outXML);
            }
            catch (Exception ex)
            {
                lblError.Text = ex.StackTrace;
                log.Trace("EditAudionAddonBridge" + ex.StackTrace);
            }
        }
        #endregion

    }
}

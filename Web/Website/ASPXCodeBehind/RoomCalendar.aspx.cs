/** FB 1850 & FB 1861 Both aspx and cs were restructured for performance **/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.Text;
using System.Threading;
using System.ComponentModel;
using System.Linq;
using System.Xml.Linq;
using DevExpress.Web.ASPxTabControl;

public partial class RoomCalendar : System.Web.UI.Page
{

    protected System.Web.UI.HtmlControls.HtmlGenericControl Html1;
    protected System.Web.UI.HtmlControls.HtmlHead Head1;
    protected System.Web.UI.HtmlControls.HtmlForm frmCalendarviewroom;
    protected System.Web.UI.ScriptManager CalendarScriptManager;
    protected System.Web.UI.HtmlControls.HtmlInputHidden IsMonthChanged;
    protected System.Web.UI.HtmlControls.HtmlInputHidden IsSettingsChange;
    protected System.Web.UI.HtmlControls.HtmlInputHidden IsWeekOverLap;
    protected System.Web.UI.HtmlControls.HtmlInputHidden HdnMonthlyXml;
    protected System.Web.UI.WebControls.Label errLabel;
    protected System.Web.UI.WebControls.Label CalenMenu;
    protected System.Web.UI.UpdatePanel PanelTab;
    protected System.Web.UI.HtmlControls.HtmlInputHidden HdnXml;
    protected System.Web.UI.WebControls.View Daily;
    protected System.Web.UI.WebControls.CheckBox officehrDaily;
    protected System.Web.UI.WebControls.View Weekly;
    protected System.Web.UI.WebControls.CheckBox officehrWeek;
    protected System.Web.UI.WebControls.View Monthly;
    protected System.Web.UI.WebControls.CheckBox officehrMonth;
    protected System.Web.UI.WebControls.Button btnDate;
    protected System.Web.UI.WebControls.TreeView treeRoomSelection;
    protected System.Web.UI.WebControls.TextBox txtType;
    protected System.Web.UI.HtmlControls.HtmlInputHidden txtSelectedDate;
    protected System.Web.UI.HtmlControls.HtmlInputHidden txtSelectedDate1;
    protected System.Web.UI.HtmlControls.HtmlTableCell Tdbuffer1;
    protected System.Web.UI.HtmlControls.HtmlTableCell Tdbuffer2;
    protected System.Web.UI.HtmlControls.HtmlTableCell Tdbuffer3;
    protected System.Web.UI.HtmlControls.HtmlTableCell Tdbuffer4;
    protected AjaxControlToolkit.CollapsiblePanelExtender Dateextender;
    protected System.Web.UI.WebControls.Panel TableCalendar;
    protected System.Web.UI.WebControls.Image Collapse;
    protected System.Web.UI.HtmlControls.HtmlInputButton btnCompare;
    protected System.Web.UI.WebControls.RadioButtonList rdSelView;
    protected System.Web.UI.WebControls.Panel pnlLevelView;
    protected System.Web.UI.WebControls.Panel pnlListView;
    protected System.Web.UI.HtmlControls.HtmlInputCheckBox selectAllCheckBox;
    protected System.Web.UI.WebControls.CheckBoxList lstRoomSelection;
    protected System.Web.UI.WebControls.Panel pnlNoData;
    protected System.Web.UI.WebControls.TextBox txtTemp;
    protected System.Web.UI.WebControls.RequiredFieldValidator RequiredFieldValidator2;
    protected System.Web.UI.WebControls.Label Label4;
    protected System.Web.UI.WebControls.Label Label3;
    protected System.Web.UI.WebControls.Label NRoom;
    protected System.Web.UI.HtmlControls.HtmlInputHidden selectedloc;
    protected System.Web.UI.HtmlControls.HtmlTableCell TdAV1;
    protected System.Web.UI.HtmlControls.HtmlTableCell TdAV;
    protected System.Web.UI.HtmlControls.HtmlTableCell TdA;
    protected System.Web.UI.HtmlControls.HtmlTableCell TdA1;
    protected System.Web.UI.HtmlControls.HtmlTableCell TdP2p1;
    protected System.Web.UI.HtmlControls.HtmlTableCell TdP2p;
    protected System.Web.UI.HtmlControls.HtmlSelect lstCalendar;//FB 2585
    
    protected DayPilot.Web.Ui.DayPilotScheduler schDaypilot;
    protected DayPilot.Web.Ui.DayPilotScheduler schDaypilotMonth;
    protected DayPilot.Web.Ui.DayPilotScheduler schDaypilotweek;
    protected DayPilot.Web.Ui.DayPilotBubble Details;
    protected DayPilot.Web.Ui.DayPilotBubble DetailsMonthly;
    protected DayPilot.Web.Ui.DayPilotBubble DetailsWeekly;
    protected System.Web.UI.HtmlControls.HtmlInputHidden locstrname;
    protected System.Web.UI.HtmlControls.HtmlInputHidden Sellocstrname;
    protected System.Web.UI.HtmlControls.HtmlInputHidden selectedList;

    protected DevExpress.Web.ASPxTabControl.ASPxPageControl CalendarContainer;

    protected Int32 hasApproal;
    protected String dtFormatType = "MM/dd/yyyy";
    protected String CalendarType = "D";
    protected String timeFormat = "1";
    protected String tFormats = "hh:mm tt";
    protected String isPublic = "D", isFuture = "D", isPending = "D", isApproval = "D", isOngoing = "D";
    protected Boolean bypass = false, isAdminRole = false;
    protected DateTime conf = DateTime.Now;
	//FB 2450
    //protected msxml4_Net.DOMDocument40Class XmlDoc = null;

    protected string paramHF;
    protected string paramF;
    protected string selRooms;
    protected String upTZ;
    protected String xmlstr = "";
    protected String enableBufferZone = "";

    myVRMNet.NETFunctions obj = null;
    ns_Logger.Logger log = null;   //Location Issues
    DateTime dtCell = DateTime.Now;//FB 1861
    String cellColor = "";//FB 1861
    String[] strSplitat = new String[1] { "@@" }; //FB 2012
    protected int dayCounter = 0;//1851

    protected HtmlImage ImgPrinter;

    //Organization/CSS Module -- Start
    protected System.Web.UI.HtmlControls.HtmlGenericControl spnAV;
    protected System.Web.UI.HtmlControls.HtmlGenericControl spnRmHrg;
    protected System.Web.UI.HtmlControls.HtmlGenericControl spnAudCon;
    protected System.Web.UI.HtmlControls.HtmlGenericControl spnPpConf;
    protected System.Web.UI.HtmlControls.HtmlGenericControl spnAVW;
    protected System.Web.UI.HtmlControls.HtmlGenericControl spnRmHrgW;
    protected System.Web.UI.HtmlControls.HtmlGenericControl spnAudConW;
    protected System.Web.UI.HtmlControls.HtmlGenericControl spnPpConfW;
    protected System.Web.UI.HtmlControls.HtmlGenericControl spnAVM;
    protected System.Web.UI.HtmlControls.HtmlGenericControl spnRmHrgM;
    protected System.Web.UI.HtmlControls.HtmlGenericControl spnAudConM;
    protected System.Web.UI.HtmlControls.HtmlGenericControl spnPpConfM;
    CustomizationUtil.CSSReplacementUtility cssUtil;
    //Organization/CSS Module -- End

    protected System.Web.UI.WebControls.CheckBox showDeletedConf; //FB 1800
    protected System.Web.UI.HtmlControls.HtmlTableRow trlegend1; //FB 1985
    protected System.Web.UI.HtmlControls.HtmlTableRow trlegend2; //FB 1985
    protected System.Web.UI.HtmlControls.HtmlTableRow trlegend3; //FB 1985

    #region Page Load Event
    /// <summary>
    /// Page Load Event
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        String favRooms = "";
        try
        {


            obj = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger();   //Location Issues


            //Organization/CSS Module - Create folder for UI Settings --- Strat
            String fieldText = "";
            cssUtil = new CustomizationUtil.CSSReplacementUtility();
            if (Session["isMultiLingual"] != null)
            {
                if (Session["isMultiLingual"].ToString() != "1")
                {

                    fieldText = cssUtil.GetUITextForControl("RoomCalendar.aspx", "spnAV");
                    spnAV.InnerText = fieldText;
                    spnAVW.InnerText = fieldText;
                    spnAVM.InnerText = fieldText;

                    fieldText = cssUtil.GetUITextForControl("RoomCalendar.aspx", "spnRmHrg");
                    spnRmHrg.InnerText = fieldText;
                    spnRmHrgW.InnerText = fieldText;
                    spnRmHrgM.InnerText = fieldText;

                    if (Application["client"] != null)
                        if (Application["client"].ToString().ToUpper() != "MOJ")
                        {
                            fieldText = cssUtil.GetUITextForControl("RoomCalendar.aspx", "spnAudCon");
                            spnAudCon.InnerText = fieldText;
                            spnAudConW.InnerText = fieldText;
                            spnAudConM.InnerText = fieldText;

                            fieldText = cssUtil.GetUITextForControl("RoomCalendar.aspx", "spnPpConf");
                            spnPpConf.InnerText = fieldText;
                            spnPpConfW.InnerText = fieldText;
                            spnPpConfM.InnerText = fieldText;
                        }
                    //Organization/CSS Module - Create folder for UI Settings --- End
                }
            }

            

            if (Session["EnableBufferZone"] == null)//Organization Module Fixes
            {
                Session["EnableBufferZone"] = "0";//Organization Module Fixes
            }

            if (Session["FormatDateType"] != null)
            {
                if (Session["FormatDateType"].ToString() == "")
                    Session["FormatDateType"] = "MM/dd/yyyy";
                else
                    dtFormatType = Session["FormatDateType"].ToString();
            }

            enableBufferZone = Session["EnableBufferZone"].ToString();//Organization Module Fixes

            if (Application["Client"].ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.MOJ))//Code added for Phase2
            {
                Tdbuffer1.Attributes.Add("style", "display:none");
                Tdbuffer2.Attributes.Add("style", "display:none");
                Tdbuffer3.Attributes.Add("style", "display:none");
                Tdbuffer4.Attributes.Add("style", "display:none");
                TdA.Attributes.Add("style", "display:none");
                TdA1.Attributes.Add("style", "display:none");
                TdP2p.Attributes.Add("style", "display:none");
                TdP2p1.Attributes.Add("style", "display:none");
                TdAV.Attributes.Add("style", "display:none");
                TdAV1.Attributes.Add("style", "display:none");
                enableBufferZone = "0";
            }

            if (Session["admin"] != null)
            {
                if (Session["admin"].ToString() == "1" || Session["admin"].ToString() == "2")   //buffer zone - Super & conf admin
                    isAdminRole = true;
            }
            if (enableBufferZone == "0")
            {
                schDaypilot.UseEventBoxes = DayPilot.Web.Ui.Enums.UseBoxesEnum.ShortEventsOnly;
                schDaypilotweek.UseEventBoxes = DayPilot.Web.Ui.Enums.UseBoxesEnum.ShortEventsOnly;
                schDaypilotMonth.UseEventBoxes = DayPilot.Web.Ui.Enums.UseBoxesEnum.ShortEventsOnly;
            }


            selRooms = " ,";

            if (!DateTime.TryParse(txtSelectedDate.Value, out conf))
                conf = DateTime.Today;

            timeFormat = ((Session["timeFormat"] != null) ? Session["timeFormat"].ToString() : timeFormat);

            if (Session["timeFormat"].ToString() == "0")
            {
                tFormats = "HH:mm";
                schDaypilot.TimeFormat = DayPilot.Web.Ui.Enums.TimeFormat.Clock24Hours;
                schDaypilotweek.TimeFormat = DayPilot.Web.Ui.Enums.TimeFormat.Clock24Hours;
            }

            if (Session["uptz"] != null)
            {
                upTZ = Session["uptz"].ToString();
            }

            if (Request.QueryString["hf"] != null)
            {
                paramHF = Request.QueryString["hf"].ToString();
            }

            if (Request.QueryString["f"] != null)
            {
                paramF = Request.QueryString["f"].ToString();
            }

            if (Session["Holidays"] != null)
            {
                if (Session["Holidays"].ToString().Trim() != "")
                {
                    schDaypilotweek.BeforeCellRender += new DayPilot.Web.Ui.Events.BeforeCellRenderEventHandler(BeforeCellRenderhandler);
                    
                    schDaypilotMonth.BeforeCellRender += new DayPilot.Web.Ui.Events.BeforeCellRenderEventHandler(BeforeCellRenderhandler);

                }
            }

            //FB 1985 
            if (Application["Client"].ToString().ToUpper() == "DISNEY")
            {
                trlegend1.Style.Add("display", "none");
                trlegend2.Style.Add("display", "none");
                trlegend3.Style.Add("display", "none");
            }
            //IsMonthChanged.Value = "Y";

            if (!IsPostBack)
            {

                officehrDaily.Checked = true;
                officehrWeek.Checked = true;
                officehrMonth.Checked = true;

                if (Session["DefaultCalendarToOfficeHours"] != null)//Organization Module Fixes
                {
                    if (Session["DefaultCalendarToOfficeHours"].ToString() == "0")
                    {
                        officehrDaily.Checked = false;
                        officehrWeek.Checked = false;
                        officehrMonth.Checked = false;
                    }

                }

                if (Request.QueryString["hf"] != null)
                {
                    if (Request.QueryString["hf"].ToString() == "1")
                    {
                        if (Request.QueryString["m"] != null)
                        {
                            selectedList.Value = Request.QueryString["m"].ToString();

                        }

                        if (Request.QueryString["d"] != null)
                        {
                            if (!DateTime.TryParse(myVRMNet.NETFunctions.GetDefaultDate(Request.QueryString["d"].ToString()), out conf))
                                conf = DateTime.Today;
                        }

                    }

                }

                BindData();
                IsMonthChanged.Value = "Y";
                String outXMLDept = "";
                String inXMLDept = "<login>";
                inXMLDept += "  <UserID>" + Session["userID"].ToString() + "</UserID>";
                inXMLDept += "</login>";
                if (Request.QueryString["hf"] != null)
                {
                    if (Request.QueryString["hf"].ToString() != "1")
                    {

                        if (Session["UserdeptXML"] == null)
                        {
                            outXMLDept = obj.CallMyVRMServer("GetRoomsDepts", inXMLDept, Application["MyVRMServer_ConfigPath"].ToString());
                            Session.Add("UserdeptXML", outXMLDept);

                        }
                        else
                            outXMLDept = Session["UserdeptXML"].ToString();

                        if (outXMLDept != "")
                        {
                            XmlDocument deptdoc = new XmlDocument();
                            deptdoc.LoadXml(outXMLDept);

                            XmlNode favlist = deptdoc.SelectSingleNode("Rooms/favourite");
                            if (favlist != null)
                                favRooms = favlist.InnerText;

                            selectedList.Value = favRooms;

                        }
                    }
                }

                if (selectedList.Value != "")
                {
                    rdSelView_SelectedIndexChanged(new object(), new EventArgs());
                    ChangeCalendarDate(null, null);
                }
                
                if (Session["isExpressUser"] != null)//FB 1779
                {
                    if (Session["isExpressUser"].ToString() == "1")
                        schDaypilot.CellSelectColor = System.Drawing.Color.Empty;
                       
                    if (Session["isExpressUser"].ToString() == "1" && Session["isExpressUserAdv"].ToString() == "0")//FB 2585
                    {
                        lstCalendar.Items.Remove(lstCalendar.Items[4]);
                        lstCalendar.Items.Remove(lstCalendar.Items[3]);
                    }
                }
                IsMonthChanged.Value = "";
            }
            
        }
        catch (Exception ex)
        {
            errLabel.Text = ex.StackTrace;
            errLabel.Visible = true;
            log.Trace(ex.Message);  //Location Issues
        }
    }
    #endregion   

    #region rdSelView_SelectedIndexChanged

    protected void rdSelView_SelectedIndexChanged(object sender, EventArgs e)
    {
        selRooms = "";
        // code changed for FB 1319 -- start
        Int32 cnt = 0;
        Int32 mCnt = 0;
        Int32 tCnt = 0;
        if (rdSelView.SelectedValue.Equals("2"))
        {
            pnlListView.Visible = true;
            pnlLevelView.Visible = false;
            lstRoomSelection.ClearSelection();
            HtmlInputCheckBox selectAll = (HtmlInputCheckBox)FindControl("selectAllCheckBox");

            if (selectedList.Value.Trim() != "")
            {
                foreach (ListItem lstItem in lstRoomSelection.Items)
                {
                    for (int i = 0; i < selectedList.Value.Trim().Split(',').Length; i++)
                        if (lstItem.Value.Equals(selectedList.Value.Split(',')[i].Trim()))
                        {
                            lstItem.Selected = true;
                            cnt = cnt + 1;
                        }
                }

                if (selectAll != null)
                {
                    if (cnt == lstRoomSelection.Items.Count)
                        selectAll.Checked = true;
                    else
                        selectAll.Checked = false;
                }
            }
            else
            {
                if (selectAll != null)
                    selectAll.Checked = false;
            }
        }
        else
        {
            pnlLevelView.Visible = true;
            pnlListView.Visible = false;

            foreach (TreeNode tnTop in treeRoomSelection.Nodes[0].ChildNodes)
            {
                tCnt = 0;
                foreach (TreeNode tnMid in tnTop.ChildNodes)
                {
                    mCnt = 0;
                    foreach (TreeNode tn in tnMid.ChildNodes)
                    {
                        tn.Checked = false;
                        if (selectedList.Value.Trim() != "")
                        {
                            for (int i = 0; i < selectedList.Value.Trim().Split(',').Length; i++)
                                if (tn.Depth.Equals(3) && tn.Value.Equals(selectedList.Value.Split(',')[i].Trim()))
                                {
                                    tn.Checked = true;
                                    mCnt++;
                                }
                        }
                    }

                    if (mCnt == tnMid.ChildNodes.Count)
                    {
                        tnMid.Checked = true;
                        tCnt++;
                    }
                    else
                        tnMid.Checked = false;

                }

                if (tCnt == tnTop.ChildNodes.Count)
                {
                    tnTop.Checked = true;
                    treeRoomSelection.Nodes[0].Checked = true;
                }
                else
                {
                    tnTop.Checked = false;
                    treeRoomSelection.Nodes[0].Checked = false;
                }
            }

        }

        if (lstRoomSelection != null)
        {
            if (lstRoomSelection.Items.Count == 0)
            {
                rdSelView.Enabled = false;
                pnlListView.Visible = false;
                pnlLevelView.Visible = false;
                btnCompare.Disabled = true;
                pnlNoData.Visible = true;
            }
           
        }

        // code changed for FB 1319 -- end
    }

    #endregion

    #region Bind Data

    private void BindData()
    {
        obj = new myVRMNet.NETFunctions();

        obj.GetManageConfRoomData(paramF, paramHF, "", "");

        GetCalendarXML();

        if (xmlstr.IndexOf("<error>") < 0)
        {
            XmlDocument xmldoc = new XmlDocument();
            xmldoc.LoadXml(xmlstr);
            XmlNodeList nodes = xmldoc.DocumentElement.SelectNodes("//locationList/level3List/level3");
            lstRoomSelection.Items.Clear();
            treeRoomSelection.Nodes.Clear();
            TreeNode tn = new TreeNode(obj.GetTranslatedText("All"));
            tn.SelectAction = TreeNodeSelectAction.None;
            tn.Expanded = true;
            treeRoomSelection.Nodes.Add(tn);
            GetLocationList(nodes);
        }


        int countLeaf = 0;
        int countMid = 0;
        int countTop = 0;
        if (!Session["roomExpandLevel"].ToString().Equals("list"))//Organization Module Fixes
        {
            foreach (TreeNode tnTop in treeRoomSelection.Nodes[0].ChildNodes)
            {
                countMid = 0;
                if (Session["roomExpandLevel"] != null)//Organization Module Fixes
                {
                    if (Session["roomExpandLevel"].ToString() != "")//Organization Module Fixes
                    {
                        if (Int32.Parse(Session["roomExpandLevel"].ToString()) >= 2)//Organization Module Fixes
                            tnTop.Expanded = true;
                    }
                }
                else
                    tnTop.Expanded = false;

                foreach (TreeNode tnMid in tnTop.ChildNodes)
                {
                    if (Session["roomExpandLevel"] != null)//Organization Module Fixes
                    {
                        if (Session["roomExpandLevel"].ToString() != "")//Organization Module Fixes
                        {
                            if (Int32.Parse(Session["roomExpandLevel"].ToString()) >= 3)//Organization Module Fixes
                                tnMid.Expanded = true; // fogbugz case 277
                        }
                    }
                    else
                        tnMid.Expanded = false;
                    countLeaf = 0;
                    foreach (TreeNode tn in tnMid.ChildNodes)
                    {
                        for (int i = 0; i < selectedList.Value.Trim().Split(',').Length - 1; i++)
                            if (tn.Depth.Equals(3) && tn.Value.Equals(selectedList.Value.Split(',')[i].Trim()))
                            {
                                tn.Checked = true;
                                countLeaf++;
                            }
                    }
                    if (countLeaf.Equals(tnMid.ChildNodes.Count))
                    {
                        tnMid.Checked = true;
                        countMid++;
                    }
                }
                if (countMid.Equals(tnTop.ChildNodes.Count))
                {
                    tnTop.Checked = true;
                    countTop++;
                }
            }
            if (countTop.Equals(treeRoomSelection.Nodes[0].ChildNodes.Count))
                treeRoomSelection.Nodes[0].Checked = true;

            //Code added for Search Room Error - start
            if (lstRoomSelection != null)
            {
                if (lstRoomSelection.Items.Count == 0)
                {
                    rdSelView.Enabled = false;
                    pnlListView.Visible = false;
                    pnlLevelView.Visible = false;
                    btnCompare.Disabled = true;
                    pnlNoData.Visible = true;
                }
                else if (lstRoomSelection.Items.Count > 0)//FB 1481 - Start
                {
                    rdSelView.Enabled = true;
                    rdSelView_SelectedIndexChanged(new object(), new EventArgs());
                    btnCompare.Disabled = false;
                    pnlNoData.Visible = false;
                }//FB 1481 - End
            }
        }

//        XmlDoc = null; //FB 2450
    }
    #endregion

    #region Get calendar XML from the Session

    private void GetCalendarXML()
    {
        int index;
        index = 1;

        string errorMessage = "";

        if (Session["outXML"] != null)
            xmlstr = Session["outXML"].ToString();

        if (paramHF == "1")
        {
            if (Session["calXML"] != null)
                xmlstr = Session["calXML"].ToString();
        }

        if (Session["errMsg"] != null)
        {
            if (Session["errMsg"].ToString() != "")
            {
                if (xmlstr.IndexOf("<error>") > 0)
                {
                    Response.Write("<br><br><p align='center'><font size=4><b>" + xmlstr + "<b></font></p>");
                    Response.End();
                }
            }
        }
    }
    #endregion

    #region GetLocationList

    protected void GetLocationList(XmlNodeList nodes)
    {
        try
        {
            ListItem li = new ListItem(obj.GetTranslatedText("Please select...."), "0");//FB 2272
            int nodes2Count = 0;
            int length = nodes.Count;
            for (int i = 0; i < length; i++)
            {
                TreeNode tn3 = new TreeNode(nodes.Item(i).SelectSingleNode("level3Name").InnerText, nodes.Item(i).SelectSingleNode("level3ID").InnerText);
                tn3.SelectAction = TreeNodeSelectAction.None;
                treeRoomSelection.Nodes[0].ChildNodes.Add(tn3);
                XmlNodeList nodes2 = nodes.Item(i).SelectNodes("level2List/level2");
                int length2 = nodes2.Count;
                if (!Session["roomExpandLevel"].ToString().ToLower().Equals("list"))//Organization Module Fixes
                    if (Session["roomExpandLevel"] != null)//Location Issues//Organization Module Fixes
                    {
                        if (Session["roomExpandLevel"].ToString() != "")//Organization Module Fixes
                        {
                            if (Int32.Parse(Session["roomExpandLevel"].ToString()) >= 2)//Organization Module Fixes
                                tn3.Expanded = true;
                        }
                    }
                    else
                        tn3.Expanded = false;

                int mid = 0; //FB 1802
                for (int j = 0; j < length2; j++)
                {
                    TreeNode tn2 = new TreeNode(nodes2.Item(j).SelectSingleNode("level2Name").InnerText, nodes2.Item(j).SelectSingleNode("level2ID").InnerText);

                    tn2.SelectAction = TreeNodeSelectAction.None;
                    treeRoomSelection.Nodes[0].ChildNodes[i].ChildNodes.Add(tn2);
                    XmlNodeList nodes1 = nodes2.Item(j).SelectNodes("level1List/level1");
                    int length1 = nodes1.Count;
                    tn2.Expanded = true; // fogbugz case 277
                    nodes2Count = 0;
                    for (int k = 0; k < length1; k++)
                    {
                        if (nodes1.Item(k).SelectSingleNode("deleted").InnerText.Trim() == "1") //Room search issue - Deactivated rooms get displayed in Treeview
                            continue;

                        if (nodes1.Item(k).SelectSingleNode("ExternalRoom") != null) //FB 2426
                            if (nodes1.Item(k).SelectSingleNode("ExternalRoom").InnerText.Trim() == "1")
                                continue;

                        TreeNode tn = new TreeNode(nodes1.Item(k).SelectSingleNode("level1Name").InnerText, nodes1.Item(k).SelectSingleNode("level1ID").InnerText);
                        tn.SelectAction = TreeNodeSelectAction.None;
                        tn.ToolTip = nodes1.Item(k).SelectSingleNode("level1Name").InnerText + " " + obj.GetTranslatedText("Click to show room resources");
                        tn.Value = nodes1.Item(k).SelectSingleNode("level1ID").InnerText;
                        treeRoomSelection.Nodes[0].ChildNodes[i].ChildNodes[mid].ChildNodes.Add(tn);//FB 1802
                        //FB 1911
                        //tn.NavigateUrl = @"javascript:chkresource('" + nodes1.Item(k).SelectSingleNode("level1ID").InnerText + "');";
                        //string l1Name = "<a class=\"tabtreeLeafNode\"  title='" + nodes1.Item(k).SelectSingleNode("level1Name").InnerText + " Click to show room Resource'  onclick='javascript:chkresource(\"" + nodes1.Item(k).SelectSingleNode("level1ID").InnerText + "\");'>" + nodes1.Item(k).SelectSingleNode("level1Name").InnerText + "</a>";
                        string l1Name = "<a class=\"tabtreeLeafNode\"  title='" + nodes1.Item(k).SelectSingleNode("level1Name").InnerText + "'>" + nodes1.Item(k).SelectSingleNode("level1Name").InnerText + "</a>";
                        li = new ListItem(l1Name, nodes1.Item(k).SelectSingleNode("level1ID").InnerText);
                        lstRoomSelection.ToolTip = obj.GetTranslatedText("Click to show room resources");
                        lstRoomSelection.Items.Add(li);
                        if (selRooms.IndexOf(" " + nodes1.Item(k).SelectSingleNode("level1ID").InnerText + ",") >= 0)
                        {
                            nodes2Count++;
                            tn.Checked = true;
                            li = new ListItem(tn.Text, tn.Value);
                        }
                    }

                    if (treeRoomSelection.Nodes[0].ChildNodes[i].ChildNodes[mid].ChildNodes.Count == 0) //FB 1802
                        treeRoomSelection.Nodes[0].ChildNodes[i].ChildNodes.Remove(tn2);

                    mid = treeRoomSelection.Nodes[0].ChildNodes[i].ChildNodes.Count; //FB 1802

                    /* Fogbugz case 156 to check the middle tier if all rooms are selected */
                    if (nodes1.Count.Equals(nodes2Count))
                        tn2.Checked = true; // fogbugz case 277
                    if (!Session["roomExpandLevel"].ToString().ToLower().Equals("list"))//Organization Module Fixes
                        if (Session["roomExpandLevel"] != null)//Organization Module Fixes
                        {
                            if (Session["roomExpandLevel"].ToString() != "")//Organization Module Fixes
                            {
                                if (Int32.Parse(Session["roomExpandLevel"].ToString()) >= 3)
                                    tn2.Expanded = true;
                            }
                        }
                        else
                            tn2.Expanded = false;
                }
            }
            //FB Case 1056 - Saima starts here 
            for (int i = 0; i < lstRoomSelection.Items.Count - 1; i++)
                for (int j = i + 1; j < lstRoomSelection.Items.Count; j++)
                    if (String.Compare(lstRoomSelection.Items[i].Text, lstRoomSelection.Items[i].Text.IndexOf(">"), lstRoomSelection.Items[j].Text, lstRoomSelection.Items[j].Text.IndexOf(">"), lstRoomSelection.Items[i].Text.Length, true) > 0)
                    {
                        ListItem liTemp = new ListItem(lstRoomSelection.Items[i].Text, lstRoomSelection.Items[i].Value);
                        lstRoomSelection.Items[i].Value = lstRoomSelection.Items[j].Value;
                        lstRoomSelection.Items[i].Text = lstRoomSelection.Items[j].Text;
                        log.Trace(i + " : " + lstRoomSelection.Items[i].Text.Substring(lstRoomSelection.Items[i].Text.IndexOf(">")) + " : " + lstRoomSelection.Items[i].Value);
                        lstRoomSelection.Items[j].Value = liTemp.Value;
                        lstRoomSelection.Items[j].Text = liTemp.Text;
                    }
            //FB Case 1056 - Saima ends here 

            //FB 1149 --Start
            foreach (ListItem listItem in lstRoomSelection.Items)
            {
                for (int r = 0; r < selRooms.Split(',').Length - 1; r++)
                {

                    if (listItem.Value.Equals(selRooms.Split(',')[r].Trim()))
                    {
                        listItem.Selected = true;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            errLabel.Text = ex.StackTrace;
            errLabel.Visible = true;
            log.Trace(ex.StackTrace);
        }
    }

    #endregion

    #region ChangeCalendarDate

    protected void ChangeCalendarDate(Object sender, EventArgs e)
    {
        

        try
        {
            schDaypilot.Visible = false;
            schDaypilotweek.Visible = false;
            schDaypilotMonth.Visible = false;

             String Roomsxml = HdnMonthlyXml.Value;
            if (IsMonthChanged.Value == "Y" || IsSettingsChange.Value == "Y")
            {
                
                Roomsxml = GetCalendarOutXml();
                schDaypilotMonth.ViewType = DayPilot.Web.Ui.Enums.ViewTypeEnum.Resources;
            }



            if (Roomsxml != "")
                DatatablefromXML(Roomsxml);

            BindMonthly();
            BindDaily();

            if (CalendarContainer.ActiveTabIndex== 1)
            {

                if (IsWeekOverLap.Value == "Y")
                    BindWeeklyXml();
            }

            

        }
        catch (Exception ex)
        {
            errLabel.Text = ex.StackTrace;
            errLabel.Visible = true;
            log.Trace("ChangeCalendarDate: " + ex.StackTrace + " : " + ex.Message);
        }
    }

    #endregion

    #region ChangeCalendarDate

    protected void WeeklyBind(Object sender, EventArgs e)
    {


        try
        {
            BindWeeklyXml();
        }
        catch (Exception ex)
        {
            errLabel.Text = ex.StackTrace;
            errLabel.Visible = true;
            log.Trace("ChangeCalendarDate: " + ex.StackTrace + " : " + ex.Message);
        }
    }

    #endregion

    #region GetCalendarOutXml

    public string GetCalendarOutXml()
    {
        String inXML = "";
        String outXML = "";
        String outXMLMonthly = "";
        String roomsselected = "";
        try
        {
            roomsselected = selectedList.Value;
            selectedList.Value = "";

            int isdeletedconf = 0; //FB 1800
            if (showDeletedConf.Checked)
                isdeletedconf = 1;

            if (rdSelView.SelectedValue.Equals("1"))
            {
                foreach (TreeNode node in treeRoomSelection.CheckedNodes)
                {
                    if (node.Depth == 3)
                    {
                        if (node.Value != "")
                        {
                            selectedList.Value += node.Value + ",";
                            inXML = "<calendarView>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID><date>" + conf.ToString("MM / dd / yyyy") + "</date><room>" + node.Value + "</room><isDeletedConf>"+ isdeletedconf +"</isDeletedConf></calendarView>"; //FB 1800
                            //outXMLDaliy += "!" + node.Value + "&" + node.Text + "&" + obj.CallCOM("GetRoomDailyView", inXML, Application["COM_ConfigPath"].ToString());
                            //outXMLWeekly += "!" + node.Value + "&" + node.Text + "&" + obj.CallCOM("GetRoomWeeklyView", inXML, Application["COM_ConfigPath"].ToString());

                            if (IsMonthChanged.Value != "Y" && IsSettingsChange.Value == "Y")
                            {
                                if (CehckifStringContains(roomsselected.Trim(), ",", node.Value))
                                    continue;
                                if (CehckifStringContains(HdnMonthlyXml.Value, node.Value))
                                    continue;
                            }


                            if (IsMonthChanged.Value == "Y" || IsSettingsChange.Value == "Y")
                            {

                                outXMLMonthly += "||" + node.Value + "@@" + node.Text + "@@" + obj.CallMyVRMServer("GetRoomMonthlyView", inXML, Application["MyVRMServer_ConfigPath"].ToString()); //FB 2012 //FB 2027

                            }

                        }
                    }
                }
            }
            else if (rdSelView.SelectedValue.Equals("2"))
            {
                foreach (ListItem lstItem in lstRoomSelection.Items)
                {
                    if (lstItem.Selected)
                    {
                        if (lstItem.Value != "")
                        {
                            selectedList.Value += lstItem.Value + ",";
                            inXML = "<calendarView>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID><date>" + conf.ToString("MM / dd / yyyy") + "</date><room>" + lstItem.Value + "</room><isDeletedConf>" + isdeletedconf + "</isDeletedConf></calendarView>"; //FB 1800
                            //outXMLDaliy += "!" + lstItem.Value + "&" + lstItem.Text.Trim().Split('>')[1].Split('<')[0] + "&" + obj.CallCOM("GetRoomDailyView", inXML, Application["COM_ConfigPath"].ToString());
                            //outXMLWeekly += "!" + lstItem.Value + "&" + lstItem.Text.Trim().Split('>')[1].Split('<')[0] + "&" + obj.CallCOM("GetRoomWeeklyView", inXML, Application["COM_ConfigPath"].ToString());

                            if (IsMonthChanged.Value != "Y" && IsSettingsChange.Value == "Y")
                            {
                                if (CehckifStringContains(roomsselected.Trim(), ",", lstItem.Value))
                                    continue;

                                if (CehckifStringContains(HdnMonthlyXml.Value, lstItem.Value))
                                    continue;
                            }

                            if (IsMonthChanged.Value == "Y" || IsSettingsChange.Value == "Y")
                            {

                                outXMLMonthly += "||" + lstItem.Value + "@@" + lstItem.Text.Trim().Split('>')[1].Split('<')[0] + "@@" + obj.CallMyVRMServer("GetRoomMonthlyView", inXML, Application["MyVRMServer_ConfigPath"].ToString()); //FB 2012 //FB 2027

                            }

                        }
                    }

                }
            }

            if (IsMonthChanged.Value.Trim() == "" && IsSettingsChange.Value == "Y")
                outXMLMonthly += HdnMonthlyXml.Value;

            if (outXMLMonthly != "")
                HdnMonthlyXml.Value = outXMLMonthly;

            outXML = HdnMonthlyXml.Value;//FB 1779
            IsSettingsChange.Value = "";

            return outXML;
        }
        catch (Exception ex)
        {
            //errLabel.Text = ex.StackTrace;
            errLabel.Text  = obj.ShowSystemMessage();
            errLabel.Visible = true;
            log.Trace(ex.StackTrace + "GetCalendarOutXml: Error Retrieving Data" + ex.Message);
            return obj.ShowSystemMessage();//FB 1881
            //return "<error><level>E</level><message>Error Retrieving Data</message></error>";
        }
    }

    #endregion

    #region BeforeEventRenderhandler

    protected void BeforeEventRenderhandler(object sender, DayPilot.Web.Ui.Events.BeforeEventRenderEventArgs e)
    {
        try
        {

            e.InnerHTML = "";
            switch (e.Tag[0])
            {
                case ns_MyVRMNet.vrmConfType.AudioOnly: e.BackgroundColor = "#EAA2D4"; ; break;
                case ns_MyVRMNet.vrmConfType.AudioVideo: e.BackgroundColor = "#BBB4FF"; ; break;
                case ns_MyVRMNet.vrmConfType.P2P: e.BackgroundColor = "#85EE99"; ; break;
                case ns_MyVRMNet.vrmConfType.RoomOnly: e.BackgroundColor = "#F16855"; ; break;
                case "S": e.BackgroundColor = "#FFCC99"; ; break;
                case "T": e.BackgroundColor = "#CCCC99"; ; break;
                case "9": e.BackgroundColor = "#01DFD7"; ; break;
                case "10": e.BackgroundColor = "#82CAFF"; ; break; //FB 2448

            }
        }
        catch (Exception ex)
        {
            errLabel.Text = ex.StackTrace;
            errLabel.Visible = true;

        }
    }

    #endregion
    //FB 1851 - Start
    #region BeforeTimeHeaderRender
    protected void BeforeTimeHeaderRender(Object sender, DayPilot.Web.Ui.Events.BeforeTimeHeaderRenderEventArgs e)
    {
        try
        {
            string dt, wk, mon;
            string[] date_arr;
            DayPilot.Web.Ui.Events.BeforeTimeHeaderRenderEventArgs DateEvent = (DayPilot.Web.Ui.Events.BeforeTimeHeaderRenderEventArgs)e;

            if (!e.IsColGroup)
            {
                if (dayCounter < 7)
                    DateEvent.InnerHTML = obj.GetTranslatedText(String.Format(e.Start.DayOfWeek.ToString())) + "<br />" + String.Format(e.Start.ToShortDateString());
                else
                    DateEvent.InnerHTML = obj.GetTranslatedText(String.Format(e.Start.DayOfWeek.ToString().Substring(0, 3).ToString())) + "<br />" + String.Format(e.Start.Day.ToString());

                dayCounter += 1;
            }
            else
            {
                dt = DateEvent.InnerHTML;
                date_arr = dt.Split(' ');
                if (date_arr.Length > 2)
                {
                    wk = dt.Split(' ')[0];
                    mon = dt.Split('(')[1].Split(' ')[0];
                    dt = dt.Replace(wk, obj.GetTranslatedText(wk));
                    dt = dt.Replace(mon, obj.GetTranslatedText(mon));
                    DateEvent.InnerHTML = dt;
                    DateEvent.ToolTip = dt;
                }
                else
                {
                    mon = dt.Split(' ')[0];
                    dt = dt.Replace(mon, obj.GetTranslatedText(mon));
                    DateEvent.InnerHTML = dt;
                    DateEvent.ToolTip = dt;
                }
            }
        }
        catch (Exception ex)
        {
            errLabel.Text = ex.StackTrace;
            errLabel.Visible = true;
        }
    }
    #endregion
    //FB 1851 - End

    #region BubbleRenderhandler
    protected void BubbleRenderhandler(object sender, DayPilot.Web.Ui.Events.Bubble.RenderEventArgs e)
    {
        try
        {
            DayPilot.Web.Ui.Events.Bubble.RenderEventBubbleEventArgs re = (DayPilot.Web.Ui.Events.Bubble.RenderEventBubbleEventArgs)e;
            // re.InnerHTML = re.Text;
            re.InnerHTML = re.Tag["CustomDescription"];

        }
        catch (Exception ex)
        {
            errLabel.Text = ex.StackTrace;
            errLabel.Visible = true;
        }
    }
    #endregion

    #region ChangeBusinessHour

    protected void ChangeBusinessHour(Object sender, EventArgs e)
    {


        try
        {


            BindMonthly();
            BindDaily(); 



        }
        catch (Exception ex)
        {
            errLabel.Text = ex.StackTrace;
            errLabel.Visible = true;
            log.Trace("ChangeCalendarDate: " + ex.StackTrace + " : " + ex.Message);
        }
    }

    #endregion

    #region ChangeSettings

    protected void ChangeSettings(Object sender, EventArgs e)
    {
        try
        {
            ChangeCalendarDate(null, null);

        }
        catch (Exception ex)
        {
            errLabel.Text = ex.StackTrace;
            errLabel.Visible = true;
            log.Trace("ChangeCalendarDate: " + ex.StackTrace + " : " + ex.Message);
        }
    }

    #endregion

    #region FromRoomSearch

    protected void FromRoomSearch(Object sender, EventArgs e)
    {
        try
        {
            IsSettingsChange.Value = "Y";
            IsMonthChanged.Value = "";
            rdSelView_SelectedIndexChanged(new object(), new EventArgs());
            PanelTab.Update();
            selectedList.Value = "";
            HdnMonthlyXml.Value = "";
            ChangeCalendarDate(null, null);
        }
        catch (Exception ex)
        {
            errLabel.Text = ex.StackTrace;
            errLabel.Visible = true;
            log.Trace("ChangeCalendarDate: " + ex.StackTrace + " : " + ex.Message);
        }
    }

    #endregion

    #region Datatable from XML

    protected void DatatablefromXML(String Roomsxml)
    {
        String hdr = obj.GetTranslatedText("Conference Details"), startHour = "00", startMin = "00", startSet = "AM", endHour = "23", endMin = "59", endSet = "PM", open24 = "";
        string CongAtt = "", CongAttStatus = obj.GetTranslatedText("No"), DedicatedVNOCOperator = obj.GetTranslatedText("Dedicated VNOC Operator");//FB 2632
        string OnSiteAVSupport = obj.GetTranslatedText("On-Site A/V Support"), MeetandGreet = obj.GetTranslatedText("Meet and Greet"), ConciergeMonitoring = obj.GetTranslatedText("Concierge Monitoring");

        Int32 hrs, mins = 0, cnt = 1;


        String locStr = "";
        String setupTxt = "";
        String trdnTxt = "";
        XmlNode node = null;
        XmlNodeList nodes = null;
        XmlDocument xmldoc = null;
        DataTable dt = null;
        DataTable dtDaily = null;
        StringBuilder m = null;
        XmlNodeList subnotes2 = null;
        XmlNode subnode2 = null;
        Boolean Blnsetup = false, BlnTear = false;
        string partyid = "0"; //FB 1659
        string ownerid = "0"; //FB 1659
        bool isParty = false; //FB 1659
        bool isOwner = false; //FB 1659
        isPublic = "0"; //FB 1659
        try
        {

            if (Application["Client"].ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.MOJ))//Code added for Phase2
                hdr = "Hearing Details";

            String spn = "<span  class=\"eventtext\">";
            if (Roomsxml != "")
                {


                    dt = GetDataTable();
                    dtDaily = GetDataTable();
                    schDaypilot.Resources.Clear();
                    schDaypilotweek.Resources.Clear();
                    schDaypilotMonth.Resources.Clear();

                    //FB 1779
                    String[] strSplit = new String[1] { "||" }; //FB 2012

                    foreach (String xmls in Roomsxml.Split(strSplit, StringSplitOptions.RemoveEmptyEntries))
                    {

                        if (xmls != "")
                        {
                            bypass = true;
                            if (!CehckifStringContains(selectedList.Value.Trim(), ",", xmls.Split(strSplitat, StringSplitOptions.RemoveEmptyEntries)[0])) //FB 2012
                                continue;

                            xmldoc = new XmlDocument();
                            xmldoc.LoadXml(xmls.Split(strSplitat, StringSplitOptions.RemoveEmptyEntries)[2]); //FB 2012
                            if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/open24") != null)
                                open24 = xmldoc.DocumentElement.SelectSingleNode("systemAvail/open24").InnerText;

                            if (open24 == "1")
                            {
                                officehrDaily.Checked = false;
                                officehrDaily.Enabled = false;

                                schDaypilot.BusinessBeginsHour = 0;
                                schDaypilot.BusinessEndsHour = 24;

                                schDaypilotweek.BusinessBeginsHour = 0;
                                schDaypilotweek.BusinessEndsHour = 24;
                            }
                            else
                            {

                                if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/startTime/startHour") != null)
                                    startHour = xmldoc.DocumentElement.SelectSingleNode("systemAvail/startTime/startHour").InnerText;

                                if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/startTime/startMin") != null)
                                    startMin = xmldoc.DocumentElement.SelectSingleNode("systemAvail/startTime/startMin").InnerText;

                                if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/startTime/startSet") != null)
                                    startSet = xmldoc.DocumentElement.SelectSingleNode("systemAvail/startTime/startSet").InnerText;

                                if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/endTime/endHour") != null)
                                    endHour = xmldoc.DocumentElement.SelectSingleNode("systemAvail/endTime/endHour").InnerText;

                                if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/endTime/endMin") != null)
                                    endMin = xmldoc.DocumentElement.SelectSingleNode("systemAvail/endTime/endMin").InnerText;

                                if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/endTime/endSet") != null)
                                    endSet = xmldoc.DocumentElement.SelectSingleNode("systemAvail/endTime/endSet").InnerText;

                                schDaypilot.BusinessBeginsHour = Convert.ToInt32(DateTime.Parse("06/06/2006 " + startHour + ":" + startMin + " " + startSet).ToString("HH"));
                                schDaypilot.BusinessEndsHour = Convert.ToInt32(DateTime.Parse("06/06/2006 " + endHour + ":" + endMin + " " + endSet).ToString("HH"));

                                if (endMin != "00")//FB 2057
                                    schDaypilot.BusinessEndsHour = schDaypilot.BusinessEndsHour + 1;

                                schDaypilotweek.BusinessBeginsHour = Convert.ToInt32(DateTime.Parse("06/06/2006 " + startHour + ":" + startMin + " " + startSet).ToString("HH"));
                                schDaypilotweek.BusinessEndsHour = Convert.ToInt32(DateTime.Parse("06/06/2006 " + endHour + ":" + endMin + " " + endSet).ToString("HH"));


                                if (endMin != "00")//FB 2057
                                    schDaypilotweek.BusinessEndsHour = schDaypilotweek.BusinessEndsHour + 1;
                            }

                            DayPilot.Web.Ui.Resource room = new DayPilot.Web.Ui.Resource(xmls.Split(strSplitat, StringSplitOptions.RemoveEmptyEntries)[1], xmls.Split(strSplitat, StringSplitOptions.RemoveEmptyEntries)[0]); //FB 2012


                            schDaypilot.Resources.Add(room);
                            schDaypilotweek.Resources.Add(room);
                            schDaypilotMonth.Resources.Add(room);

                            nodes = xmldoc.SelectNodes("//days/day/conferences/conference");

                            for (int confnodes = 0; confnodes < nodes.Count; confnodes++)// XmlNode node in nodes)
                            {
                                node = nodes[confnodes];
                                String tempFor = "", confSTime = "";
                                String setupSTime = "", teardownSTime = "", confSDate = "", uniqueID=""; // FB 2002

                                if (node.SelectSingleNode("durationMin") != null)
                                {

                                    if (node.SelectSingleNode("durationMin").InnerText != "")
                                    {
                                        if (!showDeletedConf.Checked)
                                        {
                                            if (node.SelectSingleNode("deleted") != null)
                                            {
                                                if (node.SelectSingleNode("deleted").InnerText == "1")
                                                    continue;
                                            }
                                        }

                                       

                                        if (bypass)
                                        {
                                            DataRow dr = dt.NewRow();
                                            DataRow drDaily = dtDaily.NewRow();

                                            dr["RoomID"] = room.Value;

                                            if (node.SelectSingleNode("party") != null) //FB 1659
                                            {
                                                partyid = node.SelectSingleNode("party").InnerText.Trim();
                                            }
                                            if (node.SelectSingleNode("owner") != null) //FB 1659
                                            {
                                                ownerid = node.SelectSingleNode("owner").InnerText.Trim();
                                            }

                                            if (node.SelectSingleNode("isPublic") != null) //FB 1659
                                            {
                                                isPublic = node.SelectSingleNode("isPublic").InnerText.Trim();
                                            }

                                            if (node.SelectSingleNode("confName") != null)
                                                dr["confName"] = node.SelectSingleNode("confName").InnerText;

                                            if (Session["isVIP"] != null)
                                            {
                                                if (Session["isVIP"].ToString() == "1")
                                                {
                                                    if (node.SelectSingleNode("isVIP") != null) // FB 1864
                                                        if (node.SelectSingleNode("isVIP").InnerText == "1")
                                                            dr["confName"] = dr["confName"].ToString() + "<B> {VIP} </B>";
                                                }
                                            }
                                            

                                            // FB 1659 code starts ...

                                            if (partyid == Session["userID"].ToString())
                                                isParty = true;
                                            else
                                                isParty = false;

                                            if (ownerid == Session["userID"].ToString())
                                                isOwner = true;
                                            else
                                                isOwner = false;

                                            if (isAdminRole || isParty || isOwner || (isPublic == "1"))
                                            {
                                                dr["confName"] = "<a href='#' onclick='javascript: ViewConfDetails(\"" + node.SelectSingleNode("confID").InnerText.Trim() + "\");' title='Click to see more details'><font style='cursor:hand;'>" + dr["confName"].ToString() + "</font></a>";
                                            }
                                            // FB 1659 code ends ..

                                            dr["durationMin"] = node.SelectSingleNode("durationMin").InnerText;
                                            hrs = Convert.ToInt32(dr["durationMin"].ToString()) / 60;
                                            mins = Convert.ToInt32(dr["durationMin"].ToString()) % 60;
                                            if (node.SelectSingleNode("confID") != null)
                                                dr["ConfID"] = node.SelectSingleNode("confID").InnerText;
                                            if (node.SelectSingleNode("ConferenceType") != null)
                                                dr["ConferenceType"] = node.SelectSingleNode("ConferenceType").InnerText;

                                            //FB 2448 Starts
                                            if (node.SelectSingleNode("isVMR") != null)
                                            {
                                                if (node.SelectSingleNode("isVMR").InnerText == "1")
                                                    dr["ConferenceType"] = "10";
                                            }
                                            //FB 2448 Ends
                                            if (showDeletedConf.Checked)
                                            {
                                                if (node.SelectSingleNode("deleted") != null)
                                                {
                                                    if (node.SelectSingleNode("deleted").InnerText == "1")
                                                        dr["ConferenceType"] = "9";
                                                }
                                            }

                                            dr["ID"] = dr["ConfID"].ToString();
                                            // FB 2002 starts
                                            if (node.SelectSingleNode("uniqueID") != null)
                                            {
                                                if (node.SelectSingleNode("uniqueID").InnerText != "")
                                                {
                                                    uniqueID = node.SelectSingleNode("uniqueID").InnerText;
                                                }
                                            }
                                            // FB 2002 ends

                                            if (node.SelectSingleNode("confTime") != null)
                                                confSTime = node.SelectSingleNode("confTime").InnerText;

                                            if (node.SelectSingleNode("setupTime") != null)
                                                setupSTime = node.SelectSingleNode("setupTime").InnerText;

                                            if (node.SelectSingleNode("teardownTime") != null)
                                                teardownSTime = node.SelectSingleNode("teardownTime").InnerText;

                                            if (node.SelectSingleNode("confDate") != null)
                                            {
                                                if (node.SelectSingleNode("confDate").InnerText != "")
                                                {
                                                    confSDate = node.SelectSingleNode("confDate").InnerText;
                                                }
                                            }

                                            int adddays = 0;
                                            if (confSTime.Trim() == "00:00 AM")
                                                adddays = 1;

                                            DateTime start = DateTime.Parse(confSDate + " " + confSTime);
                                            DateTime stUp = DateTime.Parse(confSDate + " " + setupSTime);
                                            DateTime end = start.AddMinutes(Convert.ToDouble(dr["durationMin"]));
                                            DateTime trDn = DateTime.Parse(end.ToString("MM/dd/yyyy") + " " + teardownSTime);

                                            start = start.AddDays(adddays);
                                            stUp = stUp.AddDays(adddays);
                                            end = end.AddDays(adddays);
                                            trDn = trDn.AddDays(adddays);

                                            if (adddays < 1 && stUp.ToString("hh:mm tt") == "12:00 AM" && start.ToString("hh:mm tt") != "12:00 AM") //FB 2398
                                                stUp = stUp.AddDays(1);

                                            dr["start"] = start;
                                            dr["end"] = end;

                                            dr["formatstart"] = myVRMNet.NETFunctions.GetFormattedDate(start.ToString("MM/dd/yyyy")) + " " + start.ToString(tFormats);
                                            dr["formatend"] = myVRMNet.NETFunctions.GetFormattedDate(end.ToString("MM/dd/yyyy")) + " " + end.ToString(tFormats); ;

                                            subnotes2 = node.SelectNodes("mainLocation/location");

                                            if (subnotes2 != null)
                                            {
                                                for (int subnodescnt = 0; subnodescnt < subnotes2.Count; subnodescnt++)
                                                {
                                                    subnode2 = subnotes2[subnodescnt];
                                                    locStr = locStr + subnode2.SelectSingleNode("locationName").InnerText;
                                                    locStr = locStr + "<br>";
                                                }
                                            }

                                            if (end != trDn  && enableBufferZone == "1")
                                                BlnTear = true;

                                            if (start != stUp && enableBufferZone == "1")
                                                Blnsetup = true;

                                            trdnTxt = "";
                                            if (end != trDn)//&& cnt > 1
                                                trdnTxt = "Tear Down :" + trDn.ToString(tFormats) + " - " + end.ToString(tFormats) + "<br>";

                                            setupTxt = "";
                                            if (stUp != start)//&& cnt > 1
                                                setupTxt = "Setup :" + stUp.ToString(tFormats) + " - " + start.ToString(tFormats) + "<br>";

                                            if (Blnsetup)
                                            {
                                                dr["start"] = stUp;
                                                dr["formatstart"] = myVRMNet.NETFunctions.GetFormattedDate(stUp.ToString("MM/dd/yyyy")) + " " + stUp.ToString(tFormats);
                                            }

                                            if (BlnTear)
                                            {
                                                dr["end"] = trDn;
                                                dr["formatend"] = myVRMNet.NETFunctions.GetFormattedDate(trDn.ToString("MM/dd/yyyy")) + " " + trDn.ToString(tFormats);
                                            }

                                            if (enableBufferZone == "0")
                                            {
                                                Blnsetup = false;
                                                BlnTear = false;
                                                trDn = end;
                                                stUp = start;
                                                trdnTxt = "";
                                                setupTxt = "";
                                            }

                                            if (Session["EnableEntity"] == null)//Organization Module Fixes
                                            {
                                                Session["EnableEntity"] = "0";//Organization Module Fixes
                                            }
                                            //FB 2013 start
                                            String customText = "", attriName = "", attriValue = "";
                                            if (Session["EnableEntity"].ToString() != "0")
                                            {
                                                XmlNodeList customnodes = node.SelectNodes("CustomAttributesList/CustomAttribute");
                                                XmlNode customnode = null;
                                                for(int i=0;i < customnodes.Count; i++)
                                                {
                                                    customnode = customnodes[i];

                                                    if (customnode.SelectSingleNode("IncludeInCalendar") != null)
                                                        if (customnode.SelectSingleNode("IncludeInCalendar").InnerText.Trim().Equals("0"))
                                                            continue;

                                                    if (customnode.SelectSingleNode("Status") != null)
                                                    {
                                                        if (customnode.SelectSingleNode("Status").InnerText == "0")
                                                        {
                                                            attriName = "";
                                                            attriValue = "";

                                                            if (customnode.SelectSingleNode("Title") != null)
                                                                attriName = customnode.SelectSingleNode("Title").InnerText;

                                                            if (customnode.SelectSingleNode("Type") != null)
                                                            {
                                                                if (customnode.SelectSingleNode("Type").InnerText == "3")
                                                                {
                                                                    if (customnode.SelectSingleNode("SelectedValue") != null)
                                                                    {
                                                                        if (customnode.SelectSingleNode("SelectedValue").InnerText == "1")
                                                                            attriValue = obj.GetTranslatedText("Yes");
                                                                        else
                                                                            attriValue = obj.GetTranslatedText("No");
                                                                    }
                                                                }
                                                                else if (customnode.SelectSingleNode("Type").InnerText == "2")//FB 2377
                                                                {
                                                                    if (customnode.SelectSingleNode("SelectedValue") != null)
                                                                    {
                                                                        if (customnode.SelectSingleNode("SelectedValue").InnerText == "1")
                                                                            attriValue = obj.GetTranslatedText("Yes");
                                                                        else
                                                                            attriValue = obj.GetTranslatedText("No");
                                                                    }
                                                                }
                                                                else
                                                                {

                                                                    if (customnode.SelectSingleNode("SelectedValue") != null)
                                                                        attriValue = customnode.SelectSingleNode("SelectedValue").InnerText;
                                                                }
                                                            }

                                                            XmlNodeList optNodes = customnode.SelectNodes("OptionList/Option");

                                                            if (customnode.SelectSingleNode("Type").InnerText == "5" || customnode.SelectSingleNode("Type").InnerText == "6") //FB 1718
                                                            {
                                                                if (optNodes != null)
                                                                {
                                                                    XmlNode optNode = null;
                                                                    for(int j=0;j <  optNodes.Count; j++)
                                                                    {
                                                                        optNode = optNodes[j];
                                                                        if (optNode.SelectSingleNode("Selected") != null)
                                                                        {
                                                                            if (optNode.SelectSingleNode("Selected").InnerText == "1")
                                                                            {
                                                                                if (optNode.SelectSingleNode("DisplayCaption") != null)
                                                                                {
                                                                                    if (attriValue == "")
                                                                                        attriValue = optNode.SelectSingleNode("DisplayCaption").InnerText;
                                                                                    else
                                                                                        attriValue += "," + optNode.SelectSingleNode("DisplayCaption").InnerText;
                                                                                }

                                                                            }


                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }

                                                        if (attriValue == "")
                                                            attriValue = obj.GetTranslatedText("N/A");

                                                        customText += attriName + " - " + attriValue + "<br>";
                                                    }
                                                }

                                            }
                                            //FB 2013 end
                                            // FB 2632 Starts
                                            CongAtt = "";
                                            CongAttStatus = obj.GetTranslatedText("No");
                                            if (node.SelectSingleNode("OnSiteAVSupport") != null)
                                            {
                                                if (node.SelectSingleNode("OnSiteAVSupport").InnerText.Equals("1"))
                                                    CongAttStatus = obj.GetTranslatedText("Yes");
                                            }
                                            CongAtt = OnSiteAVSupport + " - " + CongAttStatus;

                                            CongAttStatus = obj.GetTranslatedText("No");
                                            if (node.SelectSingleNode("MeetandGreet") != null)
                                            {
                                                if (node.SelectSingleNode("MeetandGreet").InnerText.Equals("1"))
                                                    CongAttStatus = obj.GetTranslatedText("Yes");
                                            }
                                            CongAtt += "</br>" + MeetandGreet + " - " + CongAttStatus;

                                            CongAttStatus = obj.GetTranslatedText("No");
                                            if (node.SelectSingleNode("ConciergeMonitoring") != null)
                                            {
                                                if (node.SelectSingleNode("ConciergeMonitoring").InnerText.Equals("1"))
                                                    CongAttStatus = obj.GetTranslatedText("Yes");
                                            }
                                            CongAtt += "</br>" + ConciergeMonitoring + " - " + CongAttStatus;

                                            CongAttStatus = obj.GetTranslatedText("No");
                                            if (node.SelectSingleNode("VNOCOperator") != null)
                                            {
                                                if (node.SelectSingleNode("VNOCOperator").InnerText != "")
                                                    CongAttStatus = obj.GetTranslatedText("Yes");
                                            }
                                            CongAtt += "</br>" + DedicatedVNOCOperator + " - " + CongAttStatus;
                                            //FB 2632 Ends
                                            m = new StringBuilder(); ;

                                            m.Append("<table cellspacing='0' cellpadding='0' border='0' width='310px' class='promptbox' bgColor = '#ccccff'>");
                                            m.Append("<tr valign='middle'>");
                                            m.Append("<td width='100%' height='22' style='text-indent:2;' class='titlebar' align='left' colspan='2' bgColor = '#9999ff'>");
                                            m.Append("<img src='image/pen.gif' height='18' width='18'>&nbsp;&nbsp;" + hdr);
                                            m.Append("</td>");
                                            m.Append("</tr>");
                                            m.Append("<tr>");
                                            m.Append("<td valign='top'><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + obj.GetTranslatedText("Name") + " </style></td>");
                                            m.Append("<td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + dr["confName"].ToString() + "</style></td>");
                                            m.Append("</tr>");
                                            m.Append("<tr>");
                                            m.Append("<td valign='top'><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + obj.GetTranslatedText("Unique ID") + ": </style></td>"); // FB 2002
                                            m.Append("<td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + uniqueID.ToString() + "</style></td>");
                                            m.Append("</tr>");
                                            m.Append("<tr>");
                                            if (setupTxt != "")
                                            {
                                                m.Append( "  <tr>");
                                                m.Append( "    <td><span style='font-size: 8pt; font-weight: bold; color: #990033'>SetupTIme: </style></td>");
                                                m.Append( "    <td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + start.ToString(tFormats) + " - " + stUp.ToString(tFormats) + "</style></td>");
                                                m.Append( "  </tr>");
                                            }
                                            m.Append("<td><span style='font-size: 8pt; font-weight: bold; color: #990033'>"+ obj.GetTranslatedText("Start - End") + ": </style></td>");
                                            m.Append("<td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + stUp.ToString(tFormats) + " - " + trDn.ToString(tFormats) + "</style></td>");
                                            m.Append("</tr>");
                                            m.Append("<tr>");
                                            if (trdnTxt != "")
                                            {
                                                m.Append( "  <tr>");
                                                m.Append( "    <td><span style='font-size: 8pt; font-weight: bold; color: #990033'>TearDownTime: </style></td>");
                                                m.Append( "    <td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + trDn.ToString(tFormats) + " - " + end.ToString(tFormats) + "</style></td>");
                                                m.Append( "  </tr>");
                                            }
                                            m.Append("<td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + obj.GetTranslatedText("Duration") + ": </style></td>");
                                            m.Append("<td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + hrs.ToString() + " hr(s) " + mins.ToString() + " min(s)</style></td>");
                                            m.Append("</tr>");
                                            m.Append("<tr>");
                                            m.Append("<td valign='top'><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + obj.GetTranslatedText("Location") + ": </style></td>");
                                            m.Append("<td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + ((locStr == "") ? "N/A" : locStr) + "</style></td>");
                                            m.Append("</tr>");
                                            m.Append("<tr>"); //FB 2632
                                            m.Append("<td valign='top'><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + obj.GetTranslatedText("Concierge Support") + ": </style></td>");
                                            m.Append("<td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + CongAtt + "</style></td>");
                                            m.Append("</tr>");
                                            if (Session["EnableEntity"].ToString() != "0")//FB 2013
                                            {
                                                 m.Append("<tr>");
                                                 m.Append("<td valign='top'><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + obj.GetTranslatedText("Custom Options") + ": </style></td>");
                                                 m.Append("<td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + ((customText == "") ? "N/A" : customText) + "</style></td>");
                                                 m.Append("</tr>");
                                            }
                                            m.Append("</table>");

                                            dr["confDetails"] = spn + dr["confName"].ToString() + " - (UID : " + uniqueID + " )<br>" + hrs.ToString() + " hr(s) " + mins.ToString() + " min(s)<br>" + stUp.ToString(tFormats) + " - " + trDn.ToString(tFormats) + "</span><br>" + spn + setupTxt + trdnTxt + "Locations:<br>" + locStr + "</span><br>";

                                            dr["CustomDescription"] = m.ToString();

                                            

                                            String party = "0";
                                            Boolean isParticipant = false;
                                            if (node.SelectNodes("party") != null)
                                            {
                                                if (node.SelectNodes("party").Count >= 1)
                                                {
                                                    party = node.SelectSingleNode("party").InnerText;
                                                }
                                            }

                                            if (Session["userID"].ToString() == party)
                                                isParticipant = true;

                                            if (isParticipant && !isAdminRole)
                                            {
                                                Blnsetup = false;
                                                BlnTear = false;
                                            }
                                            drDaily["CustomDescription"] = dr["CustomDescription"];
                                            drDaily["confDetails"] = dr["confDetails"];
                                            drDaily["start"] = dr["start"];
                                            drDaily["end"] = dr["end"];
                                            drDaily["formatstart"] = dr["formatstart"];
                                            drDaily["formatend"] = dr["formatend"];
                                            drDaily["ID"] = dr["ID"];
                                            drDaily["ConfID"] = dr["ConfID"];
                                            drDaily["ConferenceType"] = dr["ConferenceType"];
                                            drDaily["RoomID"] = dr["RoomID"];
                                            drDaily["durationMin"] = dr["durationMin"];
                                            drDaily["confName"] = dr["confName"];

                                            dt.Rows.Add(dr);

                                            if (Blnsetup)
                                            {
                                                DataRow setup = dtDaily.NewRow();
                                                setup["RoomID"] = room.Value;
                                                setup["ConfID"] = dr["ConfID"].ToString();
                                                setup["ConferenceType"] = "S";
                                                setup["ID"] = dr["ConfID"].ToString();
                                                setup["start"] = start;
                                                setup["end"] = stUp;
                                                setup["formatstart"] = myVRMNet.NETFunctions.GetFormattedDate(start.ToString("MM/dd/yyyy")) + " " + start.ToString(tFormats);
                                                setup["formatend"] = myVRMNet.NETFunctions.GetFormattedDate(stUp.ToString("MM/dd/yyyy")) + " " + stUp.ToString(tFormats);
                                                setup["confDetails"] = spn + "Setup Time<br>" + start.ToString(tFormats) + " - " + stUp.ToString(tFormats) + "</span>";
                                                setup["CustomDescription"] = m;
                                                dtDaily.Rows.Add(setup);
                                            }
                                            dtDaily.Rows.Add(drDaily);

                                            if (BlnTear)
                                            {
                                                DataRow trDown = dtDaily.NewRow();
                                                trDown["RoomID"] = room.Value;
                                                trDown["ConfID"] = dr["ConfID"].ToString();
                                                trDown["ConferenceType"] = "T";
                                                trDown["ID"] = dr["ConfID"].ToString();
                                                trDown["start"] = trDn;
                                                trDown["end"] = end;
                                                trDown["formatstart"] = myVRMNet.NETFunctions.GetFormattedDate(trDn.ToString("MM/dd/yyyy")) + " " + trDn.ToString(tFormats);
                                                trDown["formatend"] = myVRMNet.NETFunctions.GetFormattedDate(end.ToString("MM/dd/yyyy")) + " " + end.ToString(tFormats);
                                                trDown["confDetails"] = spn + "Tear Down Time<br>" + trDn.ToString(tFormats) + " - " + end.ToString(tFormats) + "</span>";
                                                trDown["CustomDescription"] = m;
                                                dtDaily.Rows.Add(trDown);
                                            }

                                            Blnsetup = false; BlnTear = false;

                                            locStr = "";


                                        }
                                    }
                                }
                            }

                        }
                    }


                    if (Session["RoomCalendar"] != null)
                        Session.Add("RoomCalendar", dt);
                    else
                        Session["RoomCalendar"] = dt;

                    if (Session["RoomDaily"] != null)
                        Session.Add("RoomDaily", dtDaily);
                    else
                        Session["RoomDaily"] = dtDaily;
                    
                       

            }
        }
        catch (Exception ex)
        {
            errLabel.Text = ex.StackTrace;
            errLabel.Visible = true;
            log.Trace("ChangeCalendarDate: " + ex.StackTrace + " : " + ex.Message + ":" + ex.InnerException);
        }

    }

    protected DataTable DatatablereturnfromXML(String Roomsxml)
    {
        String hdr = "Conference Details", startHour = "00", startMin = "00", startSet = "AM", endHour = "23", endMin = "59", endSet = "PM", open24 = "";
        Int32 hrs, mins = 0, cnt = 1;


        String locStr = "";
        String setupTxt = "";
        String trdnTxt = "";
        XmlNode node = null;
        XmlNodeList nodes = null;
        XmlDocument xmldoc = null;
        DataTable dt = null;
        StringBuilder m = null;
        XmlNodeList subnotes2 = null;
        XmlNode subnode2 = null;
        Boolean Blnsetup = false, BlnTear = false;
        string partyid = "0"; //FB 1659
        string ownerid = "0"; //FB 1659
        bool isParty = false; //FB 1659
        bool isOwner = false; //FB 1659
        isPublic = "0"; //FB 1659
        try
        {

            if (Application["Client"].ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.MOJ))//Code added for Phase2
                hdr = "Hearing Details";

            String spn = "<span  class=\"eventtext\">";
            if (Roomsxml != "")
            {


                dt = GetDataTable();
                schDaypilotweek.Resources.Clear();

                //FB 1779
                String[] strSplit = new String[1] { "||" }; //FB 2012
               
                foreach (String xmls in Roomsxml.Split(strSplit, StringSplitOptions.RemoveEmptyEntries))
                {

                    if (xmls != "")
                    {
                        bypass = true;

                        xmldoc = new XmlDocument();
                        xmldoc.LoadXml(xmls.Split(strSplitat, StringSplitOptions.RemoveEmptyEntries)[2]); //FB 2012
                        if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/open24") != null)
                            open24 = xmldoc.DocumentElement.SelectSingleNode("systemAvail/open24").InnerText;

                        if (open24 == "1")
                        {
                           
                            schDaypilotweek.BusinessBeginsHour = 0;
                            schDaypilotweek.BusinessEndsHour = 24;
                        }
                        else
                        {

                            if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/startTime/startHour") != null)
                                startHour = xmldoc.DocumentElement.SelectSingleNode("systemAvail/startTime/startHour").InnerText;

                            if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/startTime/startMin") != null)
                                startMin = xmldoc.DocumentElement.SelectSingleNode("systemAvail/startTime/startMin").InnerText;

                            if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/startTime/startSet") != null)
                                startSet = xmldoc.DocumentElement.SelectSingleNode("systemAvail/startTime/startSet").InnerText;

                            if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/endTime/endHour") != null)
                                endHour = xmldoc.DocumentElement.SelectSingleNode("systemAvail/endTime/endHour").InnerText;

                            if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/endTime/endMin") != null)
                                endMin = xmldoc.DocumentElement.SelectSingleNode("systemAvail/endTime/endMin").InnerText;

                            if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/endTime/endSet") != null)
                                endSet = xmldoc.DocumentElement.SelectSingleNode("systemAvail/endTime/endSet").InnerText;

                           
                            schDaypilotweek.BusinessBeginsHour = Convert.ToInt32(DateTime.Parse("06/06/2006 " + startHour + ":" + startMin + " " + startSet).ToString("HH"));
                            schDaypilotweek.BusinessEndsHour = Convert.ToInt32(DateTime.Parse("06/06/2006 " + endHour + ":" + endMin + " " + endSet).ToString("HH"));

                            if (endMin != "00")//FB 2057
                                schDaypilotweek.BusinessEndsHour = schDaypilotweek.BusinessEndsHour + 1;

                        }

                        DayPilot.Web.Ui.Resource room = new DayPilot.Web.Ui.Resource(xmls.Split(strSplitat, StringSplitOptions.RemoveEmptyEntries)[1], xmls.Split(strSplitat, StringSplitOptions.RemoveEmptyEntries)[0]); //FB 2012


                        
                        schDaypilotweek.Resources.Add(room);

                        nodes = xmldoc.SelectNodes("//days/day/conferences/conference");

                        for (int confnodes = 0; confnodes < nodes.Count; confnodes++)// XmlNode node in nodes)
                        {
                            node = nodes[confnodes];
                            String tempFor = "", confSTime = "";
                            String setupSTime = "", teardownSTime = "", confSDate = "", uniqueID = ""; // FB 2002

                            if (node.SelectSingleNode("durationMin") != null)
                            {

                                if (node.SelectSingleNode("durationMin").InnerText != "")
                                {
                                    if (!showDeletedConf.Checked)
                                    {
                                        if (node.SelectSingleNode("deleted") != null)
                                        {
                                            if (node.SelectSingleNode("deleted").InnerText == "1")
                                                continue;
                                        }
                                    }



                                    if (bypass)
                                    {
                                        DataRow dr = dt.NewRow();

                                        dr["RoomID"] = room.Value;

                                        if (node.SelectSingleNode("party") != null) //FB 1659
                                        {
                                            partyid = node.SelectSingleNode("party").InnerText.Trim();
                                        }
                                        if (node.SelectSingleNode("owner") != null) //FB 1659
                                        {
                                            ownerid = node.SelectSingleNode("owner").InnerText.Trim();
                                        }

                                        if (node.SelectSingleNode("isPublic") != null) //FB 1659
                                        {
                                            isPublic = node.SelectSingleNode("isPublic").InnerText.Trim();
                                        }

                                        if (node.SelectSingleNode("confName") != null)
                                            dr["confName"] = node.SelectSingleNode("confName").InnerText;

                                        if (Session["isVIP"] != null)
                                        {
                                            if (Session["isVIP"].ToString() == "1")
                                            {
                                                if (node.SelectSingleNode("isVIP") != null) // FB 1864
                                                    if (node.SelectSingleNode("isVIP").InnerText == "1")
                                                        dr["confName"] = dr["confName"].ToString() + "<B> {VIP} </B>";
                                            }
                                        }


                                        // FB 1659 code starts ...

                                        if (partyid == Session["userID"].ToString())
                                            isParty = true;
                                        else
                                            isParty = false;

                                        if (ownerid == Session["userID"].ToString())
                                            isOwner = true;
                                        else
                                            isOwner = false;

                                        if (isAdminRole || isParty || isOwner || (isPublic == "1"))
                                        {
                                            dr["confName"] = "<a href='#' onclick='javascript: ViewConfDetails(\"" + node.SelectSingleNode("confID").InnerText.Trim() + "\");' title='Click to see more details'><font style='cursor:hand;'>" + dr["confName"].ToString() + "</font></a>";
                                        }
                                        // FB 1659 code ends ..

                                        dr["durationMin"] = node.SelectSingleNode("durationMin").InnerText;
                                        hrs = Convert.ToInt32(dr["durationMin"].ToString()) / 60;
                                        mins = Convert.ToInt32(dr["durationMin"].ToString()) % 60;
                                        if (node.SelectSingleNode("confID") != null)
                                            dr["ConfID"] = node.SelectSingleNode("confID").InnerText;
                                        if (node.SelectSingleNode("ConferenceType") != null)
                                            dr["ConferenceType"] = node.SelectSingleNode("ConferenceType").InnerText;

                                        if (showDeletedConf.Checked)
                                        {
                                            if (node.SelectSingleNode("deleted") != null)
                                            {
                                                if (node.SelectSingleNode("deleted").InnerText == "1")
                                                    dr["ConferenceType"] = "9";
                                            }
                                        }

                                        dr["ID"] = dr["ConfID"].ToString();
                                        // FB 2002 starts
                                        if (node.SelectSingleNode("uniqueID") != null)
                                        {
                                            if (node.SelectSingleNode("uniqueID").InnerText != "")
                                            {
                                                uniqueID = node.SelectSingleNode("uniqueID").InnerText;
                                            }
                                        }
                                        // FB 2002 ends

                                        if (node.SelectSingleNode("confTime") != null)
                                            confSTime = node.SelectSingleNode("confTime").InnerText;

                                        if (node.SelectSingleNode("setupTime") != null)
                                            setupSTime = node.SelectSingleNode("setupTime").InnerText;

                                        if (node.SelectSingleNode("teardownTime") != null)
                                            teardownSTime = node.SelectSingleNode("teardownTime").InnerText;

                                        if (node.SelectSingleNode("confDate") != null)
                                        {
                                            if (node.SelectSingleNode("confDate").InnerText != "")
                                            {
                                                confSDate = node.SelectSingleNode("confDate").InnerText;
                                            }
                                        }

                                        int adddays = 0;
                                        if (confSTime.Trim() == "00:00 AM")
                                            adddays = 1;

                                        DateTime start = DateTime.Parse(confSDate + " " + confSTime);
                                        DateTime stUp = DateTime.Parse(confSDate + " " + setupSTime);
                                        DateTime end = start.AddMinutes(Convert.ToDouble(dr["durationMin"]));
                                        DateTime trDn = DateTime.Parse(end.ToString("MM/dd/yyyy") + " " + teardownSTime);

                                        start = start.AddDays(adddays);
                                        stUp = stUp.AddDays(adddays);
                                        end = end.AddDays(adddays);
                                        trDn = trDn.AddDays(adddays);



                                        dr["start"] = start;
                                        dr["end"] = end;

                                        dr["formatstart"] = myVRMNet.NETFunctions.GetFormattedDate(start.ToString("MM/dd/yyyy")) + " " + start.ToString(tFormats);
                                        dr["formatend"] = myVRMNet.NETFunctions.GetFormattedDate(end.ToString("MM/dd/yyyy")) + " " + end.ToString(tFormats); ;

                                        subnotes2 = node.SelectNodes("mainLocation/location");

                                        if (subnotes2 != null)
                                        {
                                            for (int subnodescnt = 0; subnodescnt < subnotes2.Count; subnodescnt++)
                                            {
                                                subnode2 = subnotes2[subnodescnt];
                                                locStr = locStr + subnode2.SelectSingleNode("locationName").InnerText;
                                                locStr = locStr + "<br>";
                                            }
                                        }

                                        if (end != trDn && enableBufferZone == "1")
                                            BlnTear = true;

                                        if (start != stUp && enableBufferZone == "1")
                                            Blnsetup = true;

                                        trdnTxt = "";
                                        if (end != trDn)//&& cnt > 1
                                            trdnTxt = "Tear Down :" + trDn.ToString(tFormats) + " - " + end.ToString(tFormats) + "<br>";

                                        setupTxt = "";
                                        if (stUp != start)//&& cnt > 1
                                            setupTxt = "Setup :" + stUp.ToString(tFormats) + " - " + start.ToString(tFormats) + "<br>";

                                        if (Blnsetup)
                                        {
                                            dr["start"] = stUp;
                                            dr["formatstart"] = myVRMNet.NETFunctions.GetFormattedDate(stUp.ToString("MM/dd/yyyy")) + " " + stUp.ToString(tFormats);
                                        }

                                        if (BlnTear)
                                        {
                                            dr["end"] = trDn;
                                            dr["formatend"] = myVRMNet.NETFunctions.GetFormattedDate(trDn.ToString("MM/dd/yyyy")) + " " + trDn.ToString(tFormats);
                                        }

                                        if (enableBufferZone == "0")
                                        {
                                            Blnsetup = false;
                                            BlnTear = false;
                                            trDn = end;
                                            stUp = start;
                                            trdnTxt = "";
                                            setupTxt = "";
                                        }

                                        if (Session["EnableEntity"] == null)//Organization Module Fixes
                                        {
                                            Session["EnableEntity"] = "0";//Organization Module Fixes
                                        }

                                        m = new StringBuilder(); ;

                                        m.Append("<table cellspacing='0' cellpadding='0' border='0' width='310px' class='promptbox' bgColor = '#ccccff'>");
                                        m.Append("<tr valign='middle'>");
                                        m.Append("<td width='100%' height='22' style='text-indent:2;' class='titlebar' align='left' colspan='2' bgColor = '#9999ff'>");
                                        m.Append("<img src='image/pen.gif' height='18' width='18'>&nbsp;&nbsp;" + hdr);
                                        m.Append("</td>");
                                        m.Append("</tr>");
                                        m.Append("<tr>");
                                        m.Append("<td valign='top'><span style='font-size: 8pt; font-weight: bold; color: #990033'>Name: </style></td>");
                                        m.Append("<td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + dr["confName"].ToString() + "</style></td>");
                                        m.Append("</tr>");
                                        m.Append("<tr>");
                                        m.Append("<td valign='top'><span style='font-size: 8pt; font-weight: bold; color: #990033'>Unique ID: </style></td>"); // FB 2002
                                        m.Append("<td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + uniqueID.ToString() + "</style></td>");
                                        m.Append("</tr>");
                                        if (setupTxt != "")
                                        {
                                            m.Append("  <tr>");
                                            m.Append("    <td><span style='font-size: 8pt; font-weight: bold; color: #990033'>SetupTIme: </style></td>");
                                            m.Append("    <td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + start.ToString(tFormats) + " - " + stUp.ToString(tFormats) + "</style></td>");
                                            m.Append("  </tr>");
                                        }
                                        m.Append("<td><span style='font-size: 8pt; font-weight: bold; color: #990033'>Start - End: </style></td>");
                                        m.Append("<td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + stUp.ToString(tFormats) + " - " + trDn.ToString(tFormats) + "</style></td>");
                                        m.Append("</tr>");
                                        m.Append("<tr>");
                                        if (trdnTxt != "")
                                        {
                                            m.Append("  <tr>");
                                            m.Append("    <td><span style='font-size: 8pt; font-weight: bold; color: #990033'>TearDownTime: </style></td>");
                                            m.Append("    <td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + trDn.ToString(tFormats) + " - " + end.ToString(tFormats) + "</style></td>");
                                            m.Append("  </tr>");
                                        }
                                        m.Append("<td><span style='font-size: 8pt; font-weight: bold; color: #990033'>Duration: </style></td>");
                                        m.Append("<td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + hrs.ToString() + " hr(s) " + mins.ToString() + " min(s)</style></td>");
                                        m.Append("</tr>");
                                        m.Append("<tr>");
                                        m.Append("<td valign='top'><span style='font-size: 8pt; font-weight: bold; color: #990033'>Location: </style></td>");
                                        m.Append("<td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + ((locStr == "") ? "N/A" : locStr) + "</style></td>");
                                        m.Append("</tr>");
                                        m.Append("</table>");

                                        dr["confDetails"] = spn + dr["confName"].ToString() + " - (UID : " + uniqueID + " )<br>" + hrs.ToString() + " hr(s) " + mins.ToString() + " min(s)<br>" + stUp.ToString(tFormats) + " - " + trDn.ToString(tFormats) + "</span><br>" + spn + setupTxt + trdnTxt + "Locations:<br>" + locStr + "</span><br>";

                                        dr["CustomDescription"] = m.ToString();



                                        String party = "0";
                                        Boolean isParticipant = false;
                                        if (node.SelectNodes("party") != null)
                                        {
                                            if (node.SelectNodes("party").Count >= 1)
                                            {
                                                party = node.SelectSingleNode("party").InnerText;
                                            }
                                        }

                                        if (Session["userID"].ToString() == party)
                                            isParticipant = true;

                                        if (isParticipant && !isAdminRole)
                                        {
                                            Blnsetup = false;
                                            BlnTear = false;
                                        }
                                        

                                        dt.Rows.Add(dr);

                                       
                                        Blnsetup = false; BlnTear = false;

                                        locStr = "";


                                    }
                                }
                            }
                        }

                    }
                }

            }
        }
        catch (Exception ex)
        {
            errLabel.Text = ex.StackTrace;
            errLabel.Visible = true;
            log.Trace("ChangeCalendarDate: " + ex.StackTrace + " : " + ex.Message + ":" + ex.InnerException);
        }

        return dt;

    }

    #endregion    

    #region Get Data Table

    private DataTable GetDataTable()
    {
        DataTable dt = null;
        try
        {
            dt = new DataTable();

            if (!dt.Columns.Contains("start")) dt.Columns.Add("start");
            if (!dt.Columns.Contains("end")) dt.Columns.Add("end");
            if (!dt.Columns.Contains("formatstart")) dt.Columns.Add("formatstart");
            if (!dt.Columns.Contains("formatend")) dt.Columns.Add("formatend");
            if (!dt.Columns.Contains("confDetails")) dt.Columns.Add("confDetails");
            if (!dt.Columns.Contains("ID")) dt.Columns.Add("ID");
            if (!dt.Columns.Contains("ConfID")) dt.Columns.Add("ConfID");
            if (!dt.Columns.Contains("ConferenceType")) dt.Columns.Add("ConferenceType");
            if (!dt.Columns.Contains("RoomID")) dt.Columns.Add("RoomID");
            if (!dt.Columns.Contains("confName")) dt.Columns.Add("confName");
            if (!dt.Columns.Contains("durationMin")) dt.Columns.Add("durationMin");
            if (!dt.Columns.Contains("CustomDescription")) dt.Columns.Add("CustomDescription");



        }
        catch (Exception ex)
        {

            throw ex;
        }

        return dt;

    }

    #endregion

    #region Cehck if String Contains

    private Boolean CehckifStringContains(String container,String delimiter,String iscontained)
    {
        bool rtrn = false;
        try
        {
            if (container != "" && delimiter != "" && iscontained != "")
            {
                String[] containString = container.Split(delimiter.ToCharArray());

                for(int strcnt = 0;strcnt<containString.Length;strcnt ++)
                {
                    if(containString[strcnt] != "")
                    {
                        if (containString[strcnt] == iscontained.Trim())
                        {
                            rtrn = true; break;
                        }
                    }
                }

            }



        }
        catch (Exception ex)
        {

            throw ex;
        }

        return rtrn;

    }

    #endregion

    #region BindMonthly

    protected void BindMonthly()
    {
        DataTable dt = null;

        try
        {
            if (dt == null)
            {
                if (Session["RoomCalendar"] != null)
                {
                    dt = (DataTable)Session["RoomCalendar"];
                }
            }

            if (dt != null)
            {
                schDaypilotweek.DataSource = dt;
                schDaypilotweek.DataBind();
                if (schDaypilotweek.Resources.Count > 0)
                {
                    schDaypilotweek.Visible = true;
                }
                else
                {
                    schDaypilotweek.Visible = false;
                }
                schDaypilotweek.StartDate = DayPilot.Utils.Week.FirstDayOfWeek(conf);
                schDaypilotweek.Days = 7;


                if (officehrWeek.Checked)
                {
                    schDaypilotweek.ShowNonBusiness = false;
                }
                else
                {
                    schDaypilotweek.ShowNonBusiness = true;
                }

                schDaypilotMonth.DataSource = dt;
                schDaypilotMonth.DataBind();
                if (schDaypilotMonth.Resources.Count > 0)
                {
                    schDaypilotMonth.Visible = true;
                }
                else
                {
                    schDaypilotMonth.Visible = false;
                }
                schDaypilotMonth.StartDate = new DateTime(conf.Year, conf.Month, 1);


                if (officehrMonth.Checked)
                    schDaypilotMonth.ShowNonBusiness = false;
                else
                    schDaypilotMonth.ShowNonBusiness = true; ;

            }
        }
        catch (Exception ex)
        {
            errLabel.Text = ex.StackTrace;
            errLabel.Visible = true;
            log.Trace("ChangeCalendarDate: " + ex.StackTrace + " : " + ex.Message);
        }
    }

    #endregion

    #region BindDaily

    protected void BindDaily()
    {
        DataTable dt = null; ;

        try
        {
            if (dt == null)
            {

                if (Session["RoomDaily"] != null)
                {
                    dt = (DataTable)Session["RoomDaily"];
                }
                else if (Session["RoomCalendar"] != null)
                {
                    dt = (DataTable)Session["RoomCalendar"];
                }
            }

            if (dt != null)
            {
                schDaypilot.DataSource = dt;
                schDaypilot.DataBind();

                if (schDaypilot.Resources.Count > 0)
                {
                    schDaypilot.Visible = true;
                }
                else
                {
                    schDaypilot.Visible = false;
                }
                schDaypilot.StartDate = conf;

                if (officehrDaily.Checked)
                    schDaypilot.ShowNonBusiness = false;
                else
                    schDaypilot.ShowNonBusiness = true;
            }


        }
        catch (Exception ex)
        {
            errLabel.Text = ex.StackTrace;
            errLabel.Visible = true;
            log.Trace("ChangeCalendarDate: " + ex.StackTrace + " : " + ex.Message);
        }
    }

    #endregion

    #region Cehck if String Contains

    private Boolean CehckifStringContains(String Roomsxml,String container)
    {
        bool rtrn = false;
        try
        {
            String[] strSplit = new String[1] { "||" }; //FB 2012

            foreach (String xmls in Roomsxml.Split(strSplit, StringSplitOptions.RemoveEmptyEntries))
            {

                if (xmls != "")
                {
                    bypass = true;
                    if (xmls.Split(strSplitat, StringSplitOptions.RemoveEmptyEntries)[0] == container) //FB 2012
                    {
                        rtrn = true;
                        break;

                    }
                }
            }




        }
        catch (Exception ex)
        {

            throw ex;
        }

        return rtrn;

    }

    #endregion

    #region BeforeCellRenderhandler
    /// <summary>
    /// // FB 1860
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void BeforeCellRenderhandler(object sender, DayPilot.Web.Ui.Events.BeforeCellRenderEventArgs e)
    {
        try
        {
            

            if (Session["Holidays"] != null)
            {
                if (Session["Holidays"].ToString().Trim() != "")
                {
                    dtCell = e.Start;
                    GetdayColour();

                    if (cellColor != "")
                        e.BackgroundColor = cellColor;

                }
            }



            /* DateTime cellTIme = e.Start;
             if (cellTIme.Day % 5 == 0)
                 e.BackgroundColor = "#EAA2D5";
             else if (cellTIme.Day % 7 == 0)
                 e.BackgroundColor = "#85EE98";*/

        }
        catch (Exception ex)
        {
            errLabel.Text = ex.StackTrace;
            errLabel.Visible = true;
        }
    }

    #endregion

    #region Get Day Colour
    /// <summary>
    /// FB 1860
    /// </summary>
    private void GetdayColour()
    {
        try
        {
            if (Session["Holidays"] != null)
            {
                if (Session["Holidays"].ToString().Trim() != "")
                {
                    cellColor = "";


                    XElement root = XElement.Parse(Session["Holidays"].ToString());
                    IEnumerable<XElement> hldys =
                        from hldyelmnts in root.Elements("Holiday")
                        where (string)hldyelmnts.Element("Date") == dtCell.ToString("MM/dd/yyyy") //FB 2052
                        select hldyelmnts;

                    foreach (XElement elmnts in hldys)
                    {
                        cellColor = (string)elmnts.Element("Color");
                        break;
                    }
                }
            }

        }
        catch (Exception ex)
        {

            errLabel.Text = ex.StackTrace;
            log.Trace(ex.StackTrace);
        }
    }

    #endregion

    # region bind weekly

    public void BindWeeklyXml()
    {
        String inXML = "";
        String outXML = "";
        String outXMLMonthly = "";
        String roomsselected = "";
        try
        {
           
            int isdeletedconf = 1;

            if (rdSelView.SelectedValue.Equals("1"))
            {
                foreach (TreeNode node in treeRoomSelection.CheckedNodes)
                {
                    if (node.Depth == 3)
                    {
                        if (node.Value != "")
                        {
                            if(!CehckifStringContains(selectedList.Value,",",node.Value))
                                selectedList.Value += node.Value + ",";
                            inXML = "<calendarView>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID><date>" + conf.ToString("MM / dd / yyyy") + "</date><room>" + node.Value + "</room><isDeletedConf>" + isdeletedconf + "</isDeletedConf></calendarView>";
                            outXMLMonthly += "||" + node.Value + "@@" + node.Text + "@@" + obj.CallMyVRMServer("GetRoomWeeklyView", inXML, Application["MyVRMServer_ConfigPath"].ToString()); //FB 2012 //FB 2027

                        }
                    }
                }
            }
            else if (rdSelView.SelectedValue.Equals("2"))
            {
                foreach (ListItem lstItem in lstRoomSelection.Items)
                {
                    if (lstItem.Selected)
                    {
                        if (lstItem.Value != "")
                        {
                            if(!CehckifStringContains(selectedList.Value,",",lstItem.Value))
                                selectedList.Value += lstItem.Value + ",";
                            inXML = "<calendarView>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID><date>" + conf.ToString("MM / dd / yyyy") + "</date><room>" + lstItem.Value + "</room><isDeletedConf>" + isdeletedconf + "</isDeletedConf></calendarView>";
                            outXMLMonthly += "||" + lstItem.Value + "@@" + lstItem.Text.Trim().Split('>')[1].Split('<')[0] + "@@" + obj.CallMyVRMServer("GetRoomWeeklyView", inXML, Application["MyVRMServer_ConfigPath"].ToString()); //FB 2012 //FB 2027

                            

                        }
                    }

                }
            }

            DataTable dt = DatatablereturnfromXML(outXMLMonthly);


            if (dt != null)
            {
                schDaypilotweek.DataSource = dt;
                schDaypilotweek.DataBind();
                if (schDaypilotweek.Resources.Count > 0)
                {
                    schDaypilotweek.Visible = true;
                }
                else
                {
                    schDaypilotweek.Visible = false;
                }
                schDaypilotweek.StartDate = DayPilot.Utils.Week.FirstDayOfWeek(conf);
                schDaypilotweek.Days = 7;


                if (officehrWeek.Checked)
                {
                    schDaypilotweek.ShowNonBusiness = false;
                }
                else
                {
                    schDaypilotweek.ShowNonBusiness = true;
                }
            }

        }
        catch (Exception ex)
        {
            errLabel.Text = ex.StackTrace;
            errLabel.Visible = true;
        }
    }

#endregion




}

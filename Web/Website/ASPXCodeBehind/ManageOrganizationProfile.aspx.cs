using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml;
using System.IO;

namespace ns_OrgProfile
{
    public partial class ManageOrganizationProfile : System.Web.UI.Page
    {
        #region Private Data Members
        ns_Logger.Logger log;
        myVRMNet.NETFunctions obj;
        CustomizationUtil.CSSReplacementUtility cssUtil; //organization\CSS Module
       
        #endregion

        #region Protected Data Members
        protected System.Web.UI.WebControls.TextBox txtOrgID;
        protected System.Web.UI.WebControls.Label lblTitle;
        protected System.Web.UI.WebControls.Label errLabel;
        protected System.Web.UI.WebControls.TextBox txtOrgName;
        protected System.Web.UI.WebControls.TextBox txtOrgWebsite;
        protected System.Web.UI.WebControls.TextBox txtAddress1;
        protected System.Web.UI.WebControls.TextBox txtFaxNumber;
        protected System.Web.UI.WebControls.TextBox txtAddress2;
        protected System.Web.UI.WebControls.TextBox txtCity;
        protected System.Web.UI.WebControls.TextBox txtPhoneNumber;
        protected System.Web.UI.WebControls.TextBox txtEmailID;
        protected System.Web.UI.WebControls.DropDownList lstCountries;
        protected System.Web.UI.WebControls.DropDownList lstStates;
        protected System.Web.UI.WebControls.TextBox txtZipCode;
        protected System.Web.UI.WebControls.Button btnSubmitAddNew;
        protected System.Web.UI.WebControls.Button btnSubmit;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnOrganizationID;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnRooms;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnUsers;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnMCU;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnMCUEncha;//FB 2486
        protected System.Web.UI.WebControls.TextBox TxtMCU;
        protected System.Web.UI.WebControls.TextBox TxtUsers;
        protected System.Web.UI.WebControls.TextBox TxtExUsers;
        protected System.Web.UI.WebControls.TextBox TxtDomUsers;
        protected System.Web.UI.WebControls.TextBox TxtMobUsers; //FB 1979
        protected System.Web.UI.WebControls.TextBox TxtRooms;
        protected System.Web.UI.WebControls.DropDownList DrpFood;
        protected System.Web.UI.WebControls.DropDownList DrpResource;
        protected System.Web.UI.HtmlControls.HtmlGenericControl SpanActiveRooms;
        protected System.Web.UI.HtmlControls.HtmlGenericControl SpanActiveUsers;
        protected System.Web.UI.HtmlControls.HtmlGenericControl SpanActiveExUsers;
        protected System.Web.UI.HtmlControls.HtmlGenericControl SpanActiveDomUsers;
        protected System.Web.UI.HtmlControls.HtmlGenericControl SpanActiveMobUsers; //FB 1979
        protected System.Web.UI.HtmlControls.HtmlGenericControl SpanActiveMCU;
        protected System.Web.UI.HtmlControls.HtmlGenericControl SpanActiveMCUEnchanced;//FB 2486
        protected System.Web.UI.HtmlControls.HtmlGenericControl SpanActiveVRooms;
        protected System.Web.UI.HtmlControls.HtmlGenericControl SpanActiveNVRooms;
        protected System.Web.UI.HtmlControls.HtmlGenericControl SpanActiveEpts;
        protected System.Web.UI.HtmlControls.HtmlGenericControl SpanActiveFacility;
        protected System.Web.UI.HtmlControls.HtmlGenericControl SpanActiveCat;
        protected System.Web.UI.HtmlControls.HtmlGenericControl SpanActiveHK;
        protected System.Web.UI.HtmlControls.HtmlGenericControl SpanActiveAPI;
        protected System.Web.UI.WebControls.TextBox TxtVRooms;
        protected System.Web.UI.WebControls.TextBox TxtNVRooms;
        protected System.Web.UI.WebControls.TextBox TxtEndPoint;
        protected System.Web.UI.WebControls.CheckBox ChkFacility;
        protected System.Web.UI.WebControls.CheckBox ChkCatering;
        protected System.Web.UI.WebControls.CheckBox ChkHK;
        protected System.Web.UI.WebControls.CheckBox ChkAPI;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnVRooms;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnNVRooms;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnEUsers;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnDUsers;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnMUsers; //FB 1979
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnEndPoint;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnFacility;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnCatering;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnHK;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnAPI;
        //FB 2347 Starts
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnPC; 
        protected System.Web.UI.WebControls.CheckBox ChkPC; 
        protected System.Web.UI.HtmlControls.HtmlGenericControl SpanActivePC; 
        //FB 2347 Ends
        //FB 2599 Start
        protected System.Web.UI.WebControls.CheckBox ChkCloud; //FB 2262 - j 
        protected System.Web.UI.HtmlControls.HtmlTableCell tdcloud;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdChkCloud;
        private int enableCloud = 0; //FB 2262 - J
        //FB 2599 End
        //FB 2426 start
        protected System.Web.UI.WebControls.TextBox TxtExtRooms;
        protected System.Web.UI.WebControls.TextBox TxtGstPerUser;
        protected System.Web.UI.HtmlControls.HtmlGenericControl SpanExternalRooms;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnExtRooms;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnGstPerUser;
        //FB 2426 End
        //FB 2594 Starts
        protected System.Web.UI.WebControls.TextBox TxtMCUEncha;
        protected System.Web.UI.WebControls.CheckBox ChkPublicRoom;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdPublicRoom;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdChkPublicRoom;
        //FB 2594 Ends
        private String companyID = "";
        private int currRooms = 0;
        private int currVRooms = 0;
        private int currNVRooms = 0;
        private int currMCU = 0;
        private int currUsers = 0;
        private int currEUsers = 0;
        private int currDUsers = 0;
        private int currMUsers = 0; //FB 1979
        private int currEndpoint = 0;
        private int currMCUEnchanced = 0;//FB 2486
        private int currVMRRooms = 0;//FB 2586
        //FB 2426 Start
        private int currExtRooms = 0;
        private int currGstPerUser = 0;
        //FB 2426 End
        private int enableFacility = 0;
        private int enableCatering = 0;
        private int enableHK = 0;
        private int enableAPI = 0;
        private int enablePC = 0; //FB 2347
        private int enablePublicRoom = 0; //FB 2594
        //FB 2586 Start
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnVMR;
        protected System.Web.UI.WebControls.TextBox TxtVMR;
        protected System.Web.UI.HtmlControls.HtmlGenericControl SpanVMRRooms;
        //FB 2586 End

        #endregion

        #region Constructor
        public ManageOrganizationProfile()
        {
            log = new ns_Logger.Logger();
            obj = new myVRMNet.NETFunctions();
        }
        #endregion

        #region PageLoad
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if(Session["OrganizationToEdit"]!=null)
                    companyID = Session["OrganizationToEdit"].ToString();

                ChkPublicRoom.Enabled = false;//FB 2594
                ChkCloud.Enabled = false;//FB 2599

                errLabel.Text = "";

                btnSubmitAddNew.Enabled = true; //FB 1639
                if (Session["OrganizationsLimit"] != null) 
                {
                    if (Session["OrganizationsLimit"].ToString().Trim() == "1")
                        btnSubmitAddNew.Enabled = false;
                }

                if (!IsPostBack)
                {
                    if (companyID.Equals("new"))
                        lblTitle.Text = obj.GetTranslatedText("Create New Organization");//FB 1830 - Translation
                    else
                        lblTitle.Text = obj.GetTranslatedText("Edit Organization Profile");//FB 1830 - Translation

                    BindData();

                    if(Request.QueryString["m"] != null)
                        if (Request.QueryString["m"].ToString().Equals("1"))
                        {
                            errLabel.Text = obj.GetTranslatedText("Operation Successful!");//FB 1830 - Translation
                            errLabel.Visible = true;
                        }
                }

                SpanActiveRooms.InnerText = "("+obj.GetTranslatedText("Max Allowed Rooms")+": " + hdnRooms.Value + ")";
                SpanActiveVRooms.InnerText = "("+obj.GetTranslatedText("Max Allowed Video Rooms")+": " + hdnVRooms.Value + ")";
                SpanActiveNVRooms.InnerText = "("+obj.GetTranslatedText("Max Allowed Non-Video Rooms")+": " + hdnNVRooms.Value + ")";
                SpanActiveMCU.InnerText = "("+obj.GetTranslatedText("Max Allowed Standard MCU")+": " + hdnMCU.Value + ")";
                SpanActiveUsers.InnerText = "("+obj.GetTranslatedText("Max Allowed Users")+": " + hdnUsers.Value + ")";
                SpanActiveExUsers.InnerText = "("+obj.GetTranslatedText("Max Allowed Outlook Users")+": " + hdnEUsers.Value + ")"; //FB 2098
                    SpanActiveDomUsers.InnerText = "("+obj.GetTranslatedText("Max Allowed Notes Users")+": " + hdnDUsers.Value + ")";   //FB 2098
                SpanActiveMobUsers.InnerText = "("+obj.GetTranslatedText("Max Allowed Mobile Users")+": " + hdnMUsers.Value + ")"; //FB 1979
                SpanActiveEpts.InnerText = "("+obj.GetTranslatedText("Max Allowed Endpoints")+": " + hdnEndPoint.Value + ")";
                SpanExternalRooms.InnerText = "(" + obj.GetTranslatedText("Max Allowed Guest Rooms") + ": " + hdnExtRooms.Value + ")"; //FB 2426

                SpanActiveFacility.InnerText = "(" + obj.GetTranslatedText("Max Allowed Audiovisual") + ": " + hdnFacility.Value + ")"; // FB 2570
                SpanActiveCat.InnerText = "("+obj.GetTranslatedText("Max Allowed Catering")+": " + hdnCatering.Value + ")";
                SpanActiveHK.InnerText = "("+obj.GetTranslatedText("Max Allowed Facility Services")+": " + hdnHK.Value + ")"; //Edited for FB 1706  FB 2570
                SpanActiveAPI.InnerText = "(" + obj.GetTranslatedText("Max Allowed APIs") + ": " + hdnAPI.Value + ")";
                SpanActivePC.InnerText = "(" + obj.GetTranslatedText("Max Allowed PC") + ": " + hdnPC.Value + ")"; //FB 2347
                SpanActiveMCUEnchanced.InnerText = "(" + obj.GetTranslatedText("Max Allowed Enhanced MCU")+ ": " + hdnMCUEncha.Value + ")"; //FB 2486
                SpanVMRRooms.InnerText = "(" + obj.GetTranslatedText("Max Allowed VMR Rooms") + ": " + hdnVMR.Value + ")"; //FB 2586
            }
            catch (Exception ex)
            {
                log.Trace("PageLoad: " + ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        #region BindData
        /// <summary>
        /// BindData
        /// </summary>
        protected void BindData()
        {
            try
            {
                if (Session["OrganizationToEdit"].ToString().Equals("new"))
                {
                    txtOrgName.Text = "";
                    txtAddress1.Text = "";
                    txtAddress2.Text = "";
                    txtCity.Text = "";
                    obj.GetCountryCodes(lstCountries);
                    try
                    {
                        if (companyID.Equals("new"))
                        {
                            lstCountries.Items.FindByValue("225").Selected = true;
                            UpdateStates(null, null);
                        }
                        else
                            lstCountries.Items.FindByValue("1").Selected = true;
                    }
                    catch (Exception ex1) { }

                    lstStates.Items.Clear();
                    obj.GetCountryStates(lstStates, lstCountries.SelectedValue);

                    if (companyID.Equals("new"))
                        lstStates.Items.FindByValue("34").Selected = true;

                    txtZipCode.Text = "";
                    txtPhoneNumber.Text = "";
                    txtFaxNumber.Text = "";
                    txtEmailID.Text = "";
                    txtOrgWebsite.Text = "";

                    TxtRooms.Text = "0";
                    TxtVRooms.Text = "0";
                    TxtNVRooms.Text = "0";
                    TxtUsers.Text = "0";
                    TxtExUsers.Text = "0";
                    TxtDomUsers.Text = "0";
                    TxtMobUsers.Text = "0"; //FB 1979
                    TxtMCU.Text = "0";
                    TxtEndPoint.Text = "0";
                    TxtMCUEncha.Text = "0";//FB 2486
                    //FB 2426 Start
                    TxtExtRooms.Text = "0";
                    TxtGstPerUser.Text = "0";
                    //FB 2426 End
                    TxtVMR.Text = "0";//FB 2586
                    ChkFacility.Checked = false;
                    ChkHK.Checked = false;
                    ChkCatering.Checked = false;
                    ChkAPI.Checked = false;
                    ChkPC.Checked = false; //FB 2347
                    ChkCloud.Enabled = false; //FB 2599
                    ChkPublicRoom.Checked = false; //FB 2594
                    ChkPublicRoom.Enabled = false; //FB 2594
                }
                else
                {
                    String inXML = "<GetOrganizationProfile>";
                    inXML += "  <UserID>" + Session["userID"].ToString() + "</UserID>";
                    inXML += "  <OrgId>" + Session["OrganizationToEdit"].ToString() + "</OrgId>";
                    inXML += "  </GetOrganizationProfile>";
                    log.Trace("GetOrganizationProfile inxml: " + inXML);
                    String outXML = obj.CallMyVRMServer("GetOrganizationProfile", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                    log.Trace("GetOrganizationProfile outxml: " + outXML);

                    if (outXML.IndexOf("<error>") < 0)
                    {
                        XmlDocument xmldoc = new XmlDocument();
                        xmldoc.LoadXml(outXML);

                        txtOrgName.Text = xmldoc.SelectSingleNode("//OrganizationProfile/OrganizationName").InnerText;
                        //txtOrgName.Enabled = false;
                        txtAddress1.Text = xmldoc.SelectSingleNode("//OrganizationProfile/Address1").InnerText;
                        txtAddress2.Text = xmldoc.SelectSingleNode("//OrganizationProfile/Address2").InnerText;
                        txtCity.Text = xmldoc.SelectSingleNode("//OrganizationProfile/City").InnerText;
                        txtZipCode.Text = xmldoc.SelectSingleNode("//OrganizationProfile/ZipCode").InnerText;
                        txtPhoneNumber.Text = xmldoc.SelectSingleNode("//OrganizationProfile/Phone").InnerText;
                        txtFaxNumber.Text = xmldoc.SelectSingleNode("//OrganizationProfile/Fax").InnerText;
                        txtEmailID.Text = xmldoc.SelectSingleNode("//OrganizationProfile/EmailID").InnerText;
                        txtOrgWebsite.Text = xmldoc.SelectSingleNode("//OrganizationProfile/Website").InnerText;


                        TxtRooms.Text = xmldoc.SelectSingleNode("//OrganizationProfile/Rooms").InnerText;
                        TxtVRooms.Text = xmldoc.SelectSingleNode("//OrganizationProfile/VideoRooms").InnerText;
                        TxtNVRooms.Text = xmldoc.SelectSingleNode("//OrganizationProfile/NonVideoRooms").InnerText;
                        TxtUsers.Text = xmldoc.SelectSingleNode("//OrganizationProfile/Users").InnerText;
                        TxtExUsers.Text = xmldoc.SelectSingleNode("//OrganizationProfile/ExchangeUsers").InnerText;
                        TxtDomUsers.Text = xmldoc.SelectSingleNode("//OrganizationProfile/DominoUsers").InnerText;
                        TxtMobUsers.Text = xmldoc.SelectSingleNode("//OrganizationProfile/MobileUsers").InnerText; //FB 1979
                        TxtMCU.Text = xmldoc.SelectSingleNode("//OrganizationProfile/MCU").InnerText;
                        TxtEndPoint.Text = xmldoc.SelectSingleNode("//OrganizationProfile/EndPoints").InnerText;
                        TxtMCUEncha.Text = xmldoc.SelectSingleNode("//OrganizationProfile/MCUEnchanced").InnerText;//FB 2486
                        //FB 2426 Start
                        TxtExtRooms.Text = xmldoc.SelectSingleNode("//OrganizationProfile/GuestRooms").InnerText; 
                        TxtGstPerUser.Text = xmldoc.SelectSingleNode("//OrganizationProfile/GuestRoomPerUser").InnerText;
                        //FB 2426 End
                        TxtVMR.Text = xmldoc.SelectSingleNode("//OrganizationProfile/VMRRooms").InnerText;//FB 2586
                        string enabAV = xmldoc.SelectSingleNode("//OrganizationProfile/EnableFacilites").InnerText;
                        string enabCat = xmldoc.SelectSingleNode("//OrganizationProfile/EnableCatering").InnerText;
                        string enabHK = xmldoc.SelectSingleNode("//OrganizationProfile/EnableHouseKeeping").InnerText;
                        string enabAPI = xmldoc.SelectSingleNode("//OrganizationProfile/EnableAPIs").InnerText;
                        string enabPC = xmldoc.SelectSingleNode("//OrganizationProfile/EnablePC").InnerText; //FB 2347

                        if (xmldoc.SelectSingleNode("//OrganizationProfile/EnableCloud") != null) //FB 2599 feb 22
                             Int32.TryParse(xmldoc.SelectSingleNode("//OrganizationProfile/EnableCloud").InnerText,out enableCloud); //FB 2262  
                        //FB 2594 Starts
                        if (xmldoc.SelectSingleNode("//OrganizationProfile/EnablePublicRoom") != null)
                            Int32.TryParse(xmldoc.SelectSingleNode("//OrganizationProfile/EnablePublicRoom").InnerText, out enablePublicRoom);
                        //FB 2594 Ends

                        Int32.TryParse(TxtRooms.Text.Trim(), out currRooms);
                        Int32.TryParse(TxtVRooms.Text.Trim(), out currVRooms);
                        Int32.TryParse(TxtNVRooms.Text.Trim(), out currNVRooms);
                        Int32.TryParse(TxtMCU.Text.Trim(), out currMCU);
                        Int32.TryParse(TxtUsers.Text.Trim(), out currUsers);
                        Int32.TryParse(TxtExUsers.Text.Trim(), out currEUsers);
                        Int32.TryParse(TxtDomUsers.Text.Trim(), out currDUsers);
                        Int32.TryParse(TxtMobUsers.Text.Trim(), out currMUsers); //FB 1979
                        Int32.TryParse(TxtEndPoint.Text.Trim(), out currEndpoint);
                        Int32.TryParse(TxtMCUEncha.Text.Trim(), out currMCUEnchanced);//FB 2486
                        //FB 2426 Start
                        Int32.TryParse(TxtExtRooms.Text.Trim(), out currExtRooms);
                        Int32.TryParse(TxtGstPerUser.Text.Trim(), out currGstPerUser);
                        //FB 2426 End
                        Int32.TryParse(TxtVMR.Text.Trim(), out currVMRRooms);//FB 2586
                        Int32.TryParse(enabAV, out enableFacility);
                        Int32.TryParse(enabCat, out enableCatering);
                        Int32.TryParse(enabHK, out enableHK);
                        Int32.TryParse(enabAPI, out enableAPI);
                        Int32.TryParse(enabPC, out enablePC); //FB 2347

                        if (enableFacility == 1)
                            ChkFacility.Checked = true;
                        if (enableCatering == 1)
                            ChkCatering.Checked = true;
                        if (enableHK == 1)
                            ChkHK.Checked = true;
                        if (enableAPI == 1)
                            ChkAPI.Checked = true;
                        if (enablePC == 1)//FB 2347
                            ChkPC.Checked = true;

                        //FB 2599 //FB 2645
                        if (enableCloud == 1)
                            ChkCloud.Checked = true;
                        else
                            ChkCloud.Checked = false;

                        //FB 2594 //FB 2645
                        if (enablePublicRoom == 1)
                            ChkPublicRoom.Checked = true;
                        else
                            ChkPublicRoom.Checked = false;

                        obj.GetCountryCodes(lstCountries);
                        //Get Countries and States
                        try
                        {
                            lstCountries.ClearSelection();
                            lstCountries.Items.FindByValue(xmldoc.SelectSingleNode("//OrganizationProfile/Country").InnerText).Selected = true;
                            if ((lstCountries.Items.Count > 1) && (lstCountries.SelectedIndex > 0))
                            {
                                lstStates.Items.Clear();
                                obj.GetCountryStates(lstStates, lstCountries.SelectedValue);
                                if (lstStates.Items.Count > 0)
                                {
                                    lstStates.ClearSelection();
                                    lstStates.Items.FindByValue(xmldoc.SelectSingleNode("//OrganizationProfile/State").InnerText).Selected = true;
                                }
                            }
                        }
                        catch (Exception ex1) { }
                    }
                }

                obj.GetSysLicenseInfo();
                
                hdnRooms.Value = (obj.remainingRooms + currRooms).ToString();
                hdnVRooms.Value = (obj.remainingVRooms + currVRooms).ToString();
                hdnNVRooms.Value = (obj.remainingNVRooms + currNVRooms).ToString();
                hdnMCU.Value = (obj.remainingMCUs + currMCU).ToString();
                hdnUsers.Value = (obj.remainingUsers + currUsers).ToString();
                hdnEUsers.Value = (obj.remExchangeUsers + currEUsers).ToString();
                hdnDUsers.Value = (obj.remDominoUsers + currDUsers).ToString();
                hdnMUsers.Value = (obj.remMobileUsers + currMUsers).ToString(); //FB 1979
                hdnEndPoint.Value = (obj.remainingEndPoints + currEndpoint).ToString();
                hdnMCUEncha.Value = (obj.remainingEnchancedMCUs + currMCUEnchanced).ToString();//FB 2486
                //FB 2426 Start
                hdnExtRooms.Value = (obj.remainingExtRooms + currExtRooms).ToString(); 
                hdnGstPerUser.Value = (obj.remainingGstRoomPerUser + currGstPerUser).ToString();
                //FB 2426 End
                hdnVMR.Value = (obj.remainingVMRRooms + currVMRRooms).ToString();//FB 2586
                hdnFacility.Value = (obj.remainingFacilities + enableFacility).ToString();
                hdnCatering.Value = (obj.remainingCatering + enableCatering).ToString();
                hdnHK.Value = (obj.remainingHouseKeeping + enableHK).ToString();
                hdnAPI.Value = (obj.remainingAPI + enableAPI).ToString();
                hdnPC.Value = (obj.remainingPC + enablePC).ToString(); //FB 2347
                //FB 2599 Start FB 2262
                if (obj.enableCloud == 1)
                {
                    tdcloud.Visible = true;
                    tdChkCloud.Visible = true;
                    ChkCloud.Enabled = false;
                }
                else
                {
                    tdChkCloud.Visible = false;
                    tdcloud.Visible = false;
                    ChkCloud.Checked = false;
                }
                //FB 2599 End

                //FB 2645 Start
                if (obj.enablePublicRooms == 1)
                {
                    tdPublicRoom.Visible = true;
                    tdChkPublicRoom.Visible = true;
                    ChkPublicRoom.Enabled = false;
                }
                else
                {
                    tdPublicRoom.Visible = false;
                    tdChkPublicRoom.Visible = false;
                    ChkPublicRoom.Checked = false;
                }
                //FB 2645 End
            }
            catch (Exception ex)
            {
                log.Trace("BindData: " + ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        #region UpdateStates
        protected void UpdateStates(Object sender, EventArgs e)
        {
            try
            {
                lstStates.Items.Clear();
                if (!lstCountries.SelectedValue.Equals("-1"))
                    obj.GetCountryStates(lstStates, lstCountries.SelectedValue);
            }
            catch (Exception ex)
            {
                log.Trace("UpdateStates: " + ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        #region ResetOrganizationProfile
        protected void ResetOrganizationProfile(Object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("ManageOrganizationProfile.aspx");
            }
            catch (System.Threading.ThreadAbortException) { }
            catch (Exception ex)
            {
                log.Trace("ResetOrganizationProfile: " + ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        #region SubmitOrganizationProfile
        protected void SubmitOrganizationProfile(Object sender, EventArgs e)
        {
            try
            {
                if(SetOrganizationProfile())
                 Response.Redirect("ManageOrganization.aspx?m=1");
                
            }
            catch (System.Threading.ThreadAbortException) { }
            catch (Exception ex)
            {
                log.Trace("SubmitOrganizationProfile: " + ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        #region SubmitAddNewOrganizationProfile
        protected void SubmitAddNewOrganizationProfile(Object sender,EventArgs e)
        {
            try
            {
                
                if (SetOrganizationProfile())
                {
                    Session["OrganizationToEdit"] = "new";
                    Response.Redirect("ManageOrganizationProfile.aspx");
                }
            }
            catch (System.Threading.ThreadAbortException) { }
            catch (Exception ex)
            {
                log.Trace("SubmitAddNewOrganizationProfile: " + ex.StackTrace + " : " + ex.Message);
            }
        }

        #endregion

        #region SetOrganizationProfile
        /// <summary>
        /// SetOrganizationProfile
        /// </summary>
        /// <returns></returns>
        protected bool SetOrganizationProfile()
        {
            try
            {
                if(! validatelicense())
                {
                    return false;
                }

                enableFacility = 0;
                enableCatering = 0;
                enableHK = 0;
                enableAPI = 0;
                enablePC = 0;
                enableCloud = 0;//FB 2262//FB 2599
                enablePublicRoom = 0; //FB 2594

                if (ChkFacility.Checked == true)
                    enableFacility = 1;
                if (ChkCatering.Checked == true)
                    enableCatering = 1;
                if (ChkHK.Checked == true)
                    enableHK = 1;
                if (ChkAPI.Checked == true)
                    enableAPI = 1;
                if (ChkPC.Checked == true) //FB 2347
                    enablePC = 1;
                if (ChkCloud.Checked == true) //FB 2262 - J  //FB 2599
                    enableCloud = 1;
                if (ChkPublicRoom.Checked == true) //FB 2594
                    enablePublicRoom = 1;

                String inXML = "";
                inXML += "<SetOrganizationProfile>";
                inXML += "<Organization>";
                inXML += "<UserID>" + Session["userID"].ToString() + "</UserID>";
                inXML += "<OrgId>" + Session["OrganizationToEdit"].ToString() + "</OrgId>";
                inXML += "<OrgName>" + txtOrgName.Text + "</OrgName>";
                inXML += "<Address1>" + txtAddress1.Text + "</Address1>";
                inXML += "<Address2>" + txtAddress2.Text + "</Address2>";
                inXML += "<City>" + txtCity.Text + "</City>";
                inXML += "<State>" + lstStates.SelectedValue + "</State>";
                inXML += "<Country>" + lstCountries.SelectedValue + "</Country>";
                inXML += "<ZipCode>" + txtZipCode.Text + "</ZipCode>";
                inXML += "<Phone>" + txtPhoneNumber.Text + "</Phone>";
                inXML += "<Fax>" + txtFaxNumber.Text + "</Fax>";
                inXML += "<EmailID>" + txtEmailID.Text + "</EmailID>";
                inXML += "<Website>" + txtOrgWebsite.Text + "</Website>";
                inXML += "<roomlimit>" + TxtRooms.Text + "</roomlimit>";
                inXML += "<videoroomlimit>" + TxtVRooms.Text + "</videoroomlimit>";
                inXML += "<nonvideoroomlimit>" + TxtNVRooms.Text + "</nonvideoroomlimit>";
                inXML += "<userlimit>" + TxtUsers.Text + "</userlimit>";
                inXML += "<exchangeusers>" + TxtExUsers.Text + "</exchangeusers>";
                inXML += "<dominousers>" + TxtDomUsers.Text + "</dominousers>";
                inXML += "<mobileusers>" + TxtMobUsers.Text + "</mobileusers>"; //FB 1979
                inXML += "<mculimit>" + TxtMCU.Text + "</mculimit>";
                inXML += "<endpointlimit>" + TxtEndPoint.Text + "</endpointlimit>";
                inXML += "<mcuenchancedlimit>" + TxtMCUEncha.Text + "</mcuenchancedlimit>";//FB 2486
                inXML += "<vmrroomlimit>" + TxtVMR.Text + "</vmrroomlimit>";//FB 2586
                
                //FB 2426 Start
                inXML += "<GuestRooms>" + TxtExtRooms.Text + "</GuestRooms>"; 
                inXML += "<GuestRoomPerUser>" + TxtGstPerUser.Text + "</GuestRoomPerUser>";
                Session["GuestRooms"] = TxtExtRooms.Text;
                //FB 2426 End
                inXML += "<enablefacilities>" + enableFacility + "</enablefacilities>";
                inXML += "<enablecatering>" + enableCatering + "</enablecatering>";
                inXML += "<enablehousekeeping>" + enableHK + "</enablehousekeeping>";
                inXML += "<enableAPI>" + enableAPI + "</enableAPI>";
                inXML += "<enablePC>" + enablePC + "</enablePC>"; //FB 2347
                inXML += "<enableCloud>" + enableCloud + "</enableCloud>"; //FB 2262 - J  //FB 2599
                inXML += "<EnablePublicRoom>" + enablePublicRoom + "</EnablePublicRoom>"; //FB 2594

                inXML += "</Organization>";
                inXML += "</SetOrganizationProfile>";

                String outXML = obj.CallMyVRMServer("SetOrganizationProfile", inXML, Application["MyVRMServer_ConfigPath"].ToString());

                if (outXML.IndexOf("<error>") < 0)
                {
                     //Organization\CSS Module - Create folder for UI Settings --- Strat
                    if(Session["OrganizationToEdit"] != null)
                        if (Session["OrganizationToEdit"].ToString() == "new")
                        {
                            XmlDocument xd = new XmlDocument();
                            xd.LoadXml(outXML);

                            String orgID = xd.SelectSingleNode("//success/organizationID").InnerText.Trim();

                            cssUtil = new CustomizationUtil.CSSReplacementUtility();
                            cssUtil.ApplicationPath = Server.MapPath(".."); //FB 1830
                            orgID = "Org_" + orgID;
                            cssUtil.FolderName = orgID;
                            cssUtil.CreateOrgStyles();
                        }
                    //Organization\CSS Module -- End
                    errLabel.Text = "";
                    return true;
                }
                else
                {
                    //FB 1881 start
                    /*XmlDocument xd = new XmlDocument();
                    xd.LoadXml(outXML);
                    XmlNode nd = null;
                    nd = xd.SelectSingleNode("error");
                    errLabel.Text = nd.InnerText;*/
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    //FB 1881 end
                    errLabel.Visible = true;
                    return false;
                }
            }
            catch (Exception ex)
            {
                log.Trace("SetOrganizationProfile: " + ex.StackTrace + " : " + ex.Message);
                return false;
            }
        }
        #endregion

        #region ValidateLicense

        private Boolean validatelicense()
        {
            Int32 actMcus = 0, actUsers = 0, actEUsers = 0, actDUsers = 0, actMUsers = 0; //FB 1979
            Int32 actVRms = 0, actNVRms = 0, actEndpts = 0, actExtRooms = 0, actGstPerUser = 0; //FB 2426
            Int32 extVRms = 0, extNVRms = 0, extMcus = 0, extUsers = 0, extEUsers = 0, extMUsers = 0; //FB 1979
            Int32 extDUsers = 0, extEndpts = 0, extGuestRooms = 0;//FB 2426
            Int32 actMCUEnchanced = 0;//FB 2486
            Int32 extMCUEnchanced = 0;//FB 2486
            Int32 actVMRRooms = 0;//FB 2586
            Int32 extVMR = 0;//FB 2586
            try
            {
                Int32.TryParse(TxtMCU.Text, out actMcus);
                Int32.TryParse(TxtUsers.Text, out actUsers);
                Int32.TryParse(TxtExUsers.Text, out actEUsers);
                Int32.TryParse(TxtDomUsers.Text, out actDUsers);
                Int32.TryParse(TxtMobUsers.Text, out actMUsers); //FB 1979
                Int32.TryParse(TxtVRooms.Text, out actVRms);
                Int32.TryParse(TxtNVRooms.Text, out actNVRms);
                Int32.TryParse(TxtEndPoint.Text, out actEndpts);
                Int32.TryParse(TxtMCUEncha.Text, out actMCUEnchanced);//FB 2486
                //FB 2426 Start
                Int32.TryParse(TxtExtRooms.Text, out actExtRooms); 
                Int32.TryParse(TxtGstPerUser.Text, out actGstPerUser);
                //FB 2426 End
                Int32.TryParse(TxtVMR.Text, out actVMRRooms);//FB 2586
                Int32.TryParse(hdnUsers.Value, out currUsers);
                Int32.TryParse(hdnEUsers.Value, out currEUsers);
                Int32.TryParse(hdnDUsers.Value, out currDUsers);
                Int32.TryParse(hdnMUsers.Value, out currMUsers); //FB 1979
                Int32.TryParse(hdnMCU.Value, out currMCU);
                Int32.TryParse(hdnVRooms.Value, out currVRooms);
                Int32.TryParse(hdnNVRooms.Value, out currNVRooms);
                Int32.TryParse(hdnEndPoint.Value, out currEndpoint);
                Int32.TryParse(hdnMCUEncha.Value, out currMCUEnchanced);//FB 2486
                //FB 2426 Start
                Int32.TryParse(hdnExtRooms.Value, out currExtRooms); 
                Int32.TryParse(hdnGstPerUser.Value, out currGstPerUser);
                //FB 2426 End
                Int32.TryParse(hdnVMR.Value, out currVMRRooms);//FB 2586
                //FB 1881 start                
                if (actVRms > currVRooms)
                    throw new Exception(obj.GetErrorMessage(455)); //"Video rooms limit exceeds VRM license."

                if (actNVRms > currNVRooms)
                    throw new Exception(obj.GetErrorMessage(456)); //"Non-Video rooms limit exceeds VRM license.");

                if (actMcus > currMCU)
                    throw new Exception(obj.GetErrorMessage(457)); //"MCU limit exceeds VRM license.");

                if (actMCUEnchanced > currMCUEnchanced)
                    throw new Exception(obj.GetErrorMessage(624)); //"MCU Enchanced limit exceeds VRM license.");

                if (actMCUEnchanced > actMcus)//FB 2486 
                    throw new Exception(obj.GetErrorMessage(626)); //"MCU Enchanced limit exceeds active Standard MCU.");

                if (actEndpts > currEndpoint)
                    throw new Exception(obj.GetErrorMessage(458)); //"Endpoints limit exceeds VRM license.");

                if (actUsers > currUsers)
                    throw new Exception(obj.GetErrorMessage(459)); //"User limit exceeds VRM license.");

                if (actEUsers > currEUsers)
                    throw new Exception(obj.GetErrorMessage(460)); //"Exchange user limit exceeds VRM license.");

                if (actDUsers > currDUsers)
                    throw new Exception(obj.GetErrorMessage(461)); //"Domino user limit exceeds VRM license.");

                if (actMUsers > currMUsers) //FB 1979
                    throw new Exception(obj.GetErrorMessage(526)); //"Mobile user limit exceeds VRM license.");

                if ((actEUsers + actDUsers + actMUsers) > actUsers) //FB 1979
                    throw new Exception(obj.GetErrorMessage(462)); //"Active users limit should be inclusive of both domino and exchange users.");

                //FB 2426 Start
                if (actExtRooms > currExtRooms) 
                    throw new Exception(obj.GetErrorMessage(618)); //"Guest Room limit exceeds VRM license.");

                if (actGstPerUser > actExtRooms)
                    throw new Exception(obj.GetErrorMessage(619)); //"Guest Room limit per user exceeds Guest Room limit.");
                //FB 2426 End

                if (!Session["OrganizationToEdit"].ToString().ToLower().Equals("new"))
                {
                    String inXML = "<GetActiveOrgDetails>";
                    inXML += "  <UserID>" + Session["userID"].ToString() + "</UserID>";
                    inXML += "  <organizationID>" + Session["OrganizationToEdit"].ToString() + "</organizationID>";
                    inXML += "  </GetActiveOrgDetails>";
                    String outXML = obj.CallMyVRMServer("GetActiveOrgDetails", inXML, Application["MyVRMServer_ConfigPath"].ToString());

                    if (outXML.IndexOf("<error>") >= 0)
                        throw new Exception(obj.ShowErrorMessage(outXML));

                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);

                    Int32.TryParse(xmldoc.SelectSingleNode("//GetActiveOrgDetails/ActiveVideoRoom").InnerText, out extVRms);
                    Int32.TryParse(xmldoc.SelectSingleNode("//GetActiveOrgDetails/ActiveNonVideoRoom").InnerText, out extNVRms);
                    Int32.TryParse(xmldoc.SelectSingleNode("//GetActiveOrgDetails/ActiveMCU").InnerText, out extMcus);
                    Int32.TryParse(xmldoc.SelectSingleNode("//GetActiveOrgDetails/ActiveEndPoints").InnerText, out extEndpts);
                    Int32.TryParse(xmldoc.SelectSingleNode("//GetActiveOrgDetails/ActiveUsers").InnerText, out extUsers);
                    Int32.TryParse(xmldoc.SelectSingleNode("//GetActiveOrgDetails/ActiveExchangeUsers").InnerText, out extEUsers);
                    Int32.TryParse(xmldoc.SelectSingleNode("//GetActiveOrgDetails/ActiveDominoUsers").InnerText, out extDUsers);
                    Int32.TryParse(xmldoc.SelectSingleNode("//GetActiveOrgDetails/ActiveMobileUsers").InnerText, out extMUsers); // FB 1979
                    Int32.TryParse(xmldoc.SelectSingleNode("//GetActiveOrgDetails/ActiveGuestRooms").InnerText, out extGuestRooms); // FB 2426
                    Int32.TryParse(xmldoc.SelectSingleNode("//GetActiveOrgDetails/ActiveMCUEnchanced").InnerText, out extMCUEnchanced);//FB 2486
                    Int32.TryParse(xmldoc.SelectSingleNode("//GetActiveOrgDetails/ActiveVMRRooms").InnerText, out extVMR);//FB 2586

                    if (actVRms < extVRms)
                        throw new Exception(obj.GetErrorMessage(463)); //"Please deactivate video rooms to reduce the count.As there are more active video rooms.");
                    if (actNVRms < extNVRms)
                        throw new Exception(obj.GetErrorMessage(464)); //"Please deactivate non-video rooms to reduce the count.As there are more active non-video rooms.");
                    if (actMcus < extMcus)
                        throw new Exception(obj.GetErrorMessage(465)); //"Please delete mcu to reduce the count.As there are more active mcu.");
                    if (actEndpts < extEndpts)
                        throw new Exception(obj.GetErrorMessage(466)); //"Please delete endpoints to reduce the count.As there are more active endpoints.");
                    if (actUsers < extUsers)
                        throw new Exception(obj.GetErrorMessage(467)); //"Please delete users to reduce the count.As there are more active users.");
                    if (actEUsers < extEUsers)
                        throw new Exception(obj.GetErrorMessage(482)); //"Please delete exchange users to reduce the count.As there are more active exchange users.");
                    if (actDUsers < extDUsers)
                        throw new Exception(obj.GetErrorMessage(494));//"Please delete domino users to reduce the count.As there are more active domino users."
                    if (actMUsers < extMUsers) // FB 1979
                        throw new Exception(obj.GetErrorMessage(527));//"Please delete mobile users to reduce the count.As there are more active mobile users."
                    //FB 1881 end
                    if (actExtRooms < extGuestRooms) // FB 2426
                        throw new Exception(obj.GetErrorMessage(615));//"Please delete External Rooms to reduce the count.As there are more active External Rooms."

                    if (actMCUEnchanced < extMCUEnchanced) //FB 2537
                        throw new Exception(obj.GetErrorMessage(628));//"Please delete MCU Enhanced to reduce the count.As there are more active MCU Enhanced.");

                    if (actVMRRooms < extVMR) //FB 2586
                        throw new Exception(obj.GetErrorMessage(657));//"Please deactivate VMR rooms to reduce the count.As there are more active VMR rooms.");


                }
            }
            catch (Exception ex)
            {
                errLabel.Text = ex.Message;
                errLabel.Visible = true;
                log.Trace("SetOrganizationProfile: " + ex.StackTrace + " : " + ex.Message);
                return false;
            }
            return true;
        }

        #endregion
    }
}

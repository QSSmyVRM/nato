using System;
using System.Data;
using System.Xml;
using System.Web.UI.WebControls;
using System.Text;

/// <summary>
/// Summary description for ConferenceOrders
/// </summary>
/// 
namespace ns_Bridges
{
    public partial class Bridges : System.Web.UI.Page
    {
        myVRMNet.NETFunctions obj;
        
        protected System.Web.UI.WebControls.Label lblHeader;
        protected System.Web.UI.WebControls.Label errLabel;
        protected System.Web.UI.WebControls.DropDownList lstBridgeType;
        protected System.Web.UI.WebControls.DropDownList lstBridgeStatus;
        protected System.Web.UI.WebControls.Table tblNoMCUs;
        protected System.Web.UI.WebControls.DataGrid dgMCUs;
        protected System.Web.UI.WebControls.TextBox txtBridges;
        protected System.Web.UI.WebControls.Label lblMCUmsg; // FB 1920
        protected ns_Logger.Logger log = null;//FB 2027

        private Int32 mculmt = 0;
        private Int32 existingMculmt = 0;//organization module
        //FB 2599 Start
        protected System.Web.UI.WebControls.Button btnNewMCU;//FB 2262
        protected int Cloud = 0; //Fb 2262
        //FB 2599 End
        public Bridges()
        {
            obj = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger();//FB 2027
            //
            // TODO: Add constructor logic here
            //
        }
        #region Methods Executed on Page Load

        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                //FB 1920 Starts
                if (Session["organizationID"].ToString() == "11")
                    lblMCUmsg.Attributes.Add("style", "display:none");
                //FB 1920 Ends
                if (Session["McuLimit"] != null)
                {
                    if (Session["McuLimit"].ToString() != "")
                        Int32.TryParse(Session["McuLimit"].ToString(), out mculmt);
                }

                lblHeader.Text = obj.GetTranslatedText("Manage MCUs");//FB 1830 - Translation
                errLabel.Text = "";
                if (Request.QueryString["m"] != null)
                    if (Request.QueryString["m"].ToString().Equals("1"))
                    {
                        errLabel.Text = obj.GetTranslatedText("Operation Successful!");//FB 1830 - Translation
                        errLabel.Visible = true;
                    }
                //FB 2599 Start
                if (Session["Cloud"] != null) //FB 2262
                {
                    if (Session["Cloud"].ToString() != "")
                        Int32.TryParse(Session["Cloud"].ToString(), out Cloud);
                }

                if (Cloud == 1) //FB 2262
                {
                    btnNewMCU.Visible = false;
                }
                //FB 2599 End
                if (!IsPostBack)
                    BindData();
                else
                    if (Request.Params.Get("__EVENTTARGET").ToString().ToUpper().Equals("MANAGEORDER"))
                        ManageOrder();
            }
            catch (Exception ex)
            {
                //                Response.Write(ex.Message);
                errLabel.Visible = true;
                errLabel.Text = "PageLoad: " + ex.StackTrace;
            }

        }

        private void BindData()
        {
            try
            {
                Session["multisiloOrganizationID"] = null; //FB 2274
                String inXML = "";
                inXML += "<login>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "  <userID>" + Session["userID"].ToString() + "</userID>";
                inXML += "</login>";
                String outXML = obj.CallMyVRMServer("GetBridgeList", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                //String outXML = obj.CallCOM("GetBridgeList", inXML, Application["COM_ConfigPath"].ToString());
                //String outXML = "<bridgeInfo><bridgeTypes><type><ID>1</ID><name>Polycom MGC 25</name><interfaceType>1</interfaceType></type><type><ID>2</ID><name>Polycom MGC 50</name><interfaceType>1</interfaceType></type><type><ID>3</ID><name>Polycom MGC 100</name><interfaceType>1</interfaceType></type><type><ID>4</ID><name>Codian MCU 4200</name><interfaceType>3</interfaceType></type><type><ID>5</ID><name>Codian MCU 4500</name><interfaceType>3</interfaceType></type><type><ID>6</ID><name> MSE 8000 Series</name><interfaceType>3</interfaceType></type><type><ID>7</ID><name>Tandberg MPS 800 Series</name><interfaceType>4</interfaceType></type></bridgeTypes><bridgeStatuses><status><ID>1</ID><name>Active</name></status><status><ID>2</ID><name>Maintenance</name></status><status><ID>3</ID><name>Disabled</name></status></bridgeStatuses><bridges><bridge><ID>1</ID><name>Test Bridge</name><interfaceType>3</interfaceType><administrator>VRM Administrator</administrator><exist>1</exist><status>1</status><order></order></bridge></bridges><totalNumber>1</totalNumber><licensesRemain>9</licensesRemain></bridgeInfo>";
                //Response.Write("<br>" + obj.Transfer(outXML));
                if (outXML.IndexOf("<error>") < 0)
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);
                    XmlNodeList nodes = xmldoc.SelectNodes("//bridgeInfo/bridges/bridge");
                    if (nodes.Count > 0)
                    {
                        XmlNodeList nodesTemp = xmldoc.SelectNodes("//bridgeInfo/bridgeTypes/type");
                        LoadList(lstBridgeType, nodesTemp);
                        nodesTemp = xmldoc.SelectNodes("//bridgeInfo/bridgeStatuses/status");
                        //1``Test Bridge||4``Test 4||
                        LoadList(lstBridgeStatus, nodesTemp);
                        LoadMCUGrid(nodes);
                        Label lblTemp = new Label();
                        DataGridItem dgFooter = (DataGridItem)dgMCUs.Controls[0].Controls[dgMCUs.Controls[0].Controls.Count - 1];
                        lblTemp = (Label)dgFooter.FindControl("lblTotalRecords");
                        lblTemp.Text = xmldoc.SelectSingleNode("//bridgeInfo/totalNumber").InnerText;


                        Int32.TryParse(lblTemp.Text, out existingMculmt);

                        lblTemp = (Label)dgFooter.FindControl("lblRemaining");
                        //lblTemp.Text = xmldoc.SelectSingleNode("//bridgeInfo/licensesRemain").InnerText;

                        lblTemp.Text = (mculmt - existingMculmt).ToString();//Organization Module

                        dgMCUs.Visible = true;
                        tblNoMCUs.Visible = false;
                        foreach (XmlNode node in nodes)
                            txtBridges.Text += node.SelectSingleNode("ID").InnerText + "``" + node.SelectSingleNode("name").InnerText + "||";

                    }
                    else
                    {
                        dgMCUs.Visible = false;
                        tblNoMCUs.Visible = true;
                    }
                }
                else
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                errLabel.Text = "BindData: " + ex.StackTrace;
            }
        }

        protected void LoadList(DropDownList lstTemp, XmlNodeList nodes)
        {
            try
            {
                XmlTextReader xtr;
                DataSet ds = new DataSet();

                foreach (XmlNode node in nodes)
                {
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.InferSchema);
                }
                DataTable dt = new DataTable();

                if (ds.Tables.Count > 0)
                    dt = ds.Tables[0];
                lstTemp.DataSource = dt;
                lstTemp.DataBind();
            }
            catch (Exception ex)
            {
                errLabel.Text = ex.StackTrace;
                errLabel.Visible = true;
            }
        }

        protected void LoadMCUGrid(XmlNodeList nodes)
        {
            try
            {
                XmlTextReader xtr;
                DataSet ds = new DataSet();

                foreach (XmlNode node in nodes)
                {
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.InferSchema);
                }
                DataTable dt = new DataTable();

                if (ds.Tables.Count > 0)
                    dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    if (dr["exist"].ToString().Equals("1"))
                        dr["exist"] = obj.GetTranslatedText("Existing"); //FB 2272
                    else
                        dr["exist"] = obj.GetTranslatedText("Virtual");//FB 2272
                    dr["status"] = obj.GetTranslatedText(lstBridgeStatus.Items.FindByValue(dr["status"].ToString()).Text);//FB 2272
                    dr["interfaceType"] = lstBridgeType.Items.FindByValue(dr["interfaceType"].ToString()).Text;
                    
                    //FB 1462 start
                    if (dr["userstate"].ToString() == "I")
                    {
                        dr["administrator"] = "<font color=red>"+ dr["administrator"].ToString() + " (In-Active)</font>";
                    }
                    else if (dr["userstate"].ToString() == "D")
                    {
                        dr["administrator"] = "<font color=red>" + dr["administrator"].ToString() + "</font>";
                    }
                    //FB 1462 end
                }

                dgMCUs.DataSource = dt;
                dgMCUs.DataBind();
            }
            catch (Exception ex)
            {
                errLabel.Text = ex.StackTrace;
                errLabel.Visible = true;
            }
        }

        #endregion
        protected void DeleteMCU(object sender, DataGridCommandEventArgs e)
        {
            try
            {   //FB 2027 - Starts
                StringBuilder inXML = new StringBuilder();
                inXML.Append("<login>");
                inXML.Append(obj.OrgXMLElement());//Organization Module Fixes
                inXML.Append("<userID>" + Session["userID"].ToString() + "</userID>");
                inXML.Append("<bridge>");
                inXML.Append("<bridgeID>" + e.Item.Cells[0].Text + "</bridgeID>");
                inXML.Append("</bridge>");
                inXML.Append("</login>");
                log.Trace(inXML.ToString());
                //Response.Write(obj.Transfer(inXML));
                //String outXML = obj.CallCOM("DeleteBridge", inXML, Application["COM_ConfigPath"].ToString());
                String outXML = obj.CallMyVRMServer("DeleteBridge", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
                //FB 2027 - End
                //Response.Write(obj.Transfer(outXML));
                //Response.End();
                if (outXML.IndexOf("<error>") < 0)
                {
                    Response.Redirect("ManageBridge.aspx?m=1");
                }
                else
                {
                    errLabel.Visible = true;
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                }
            }
            catch (Exception ex)
            {
                errLabel.Text = ex.StackTrace;
                errLabel.Visible = true;
            }
        }
        protected void EditMCU(object sender, DataGridCommandEventArgs e)
        {
            try
            {
                Session.Add("BridgeID", e.Item.Cells[0].Text);
                Response.Redirect("BridgeDetails.aspx");
            }
            catch (Exception ex)
            {
                errLabel.Text = ex.StackTrace;
                errLabel.Visible = true;
            }
        }
        protected void BindRowsDeleteMessage(Object sender, DataGridItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType.Equals(ListItemType.Item) || e.Item.ItemType.Equals(ListItemType.AlternatingItem))
                {
                    LinkButton btnTemp = (LinkButton)e.Item.FindControl("btnDelete");
                    // FB 1920 Starts
                    DataRowView row = e.Item.DataItem as DataRowView;
                    if (Session["organizationID"].ToString() != "11")
                    {
                        if (row["isPublic"].ToString() != "1" && row["Orgid"].ToString() != "11")
                            btnTemp.Attributes.Add("onclick", "return confirm('"+obj.GetTranslatedText("Are you sure you want to delete this MCU?")+"')");//FB japnese
                    }
                    // FB 1920 Ends
                }
            }
            catch (Exception ex)
            {
                errLabel.Text = ex.StackTrace;
                errLabel.Visible = true;
            }
        }
        protected void CreateNewMCU(Object sender, EventArgs e)
        {
            Session.Add("BridgeID", "new");
            Response.Redirect("BridgeDetails.aspx");
        }
        protected void GenerateReport(Object sender, EventArgs e)
        {
            Response.Redirect("MCUResourceReport.aspx");
        }
        protected void ManageOrder()
        {
            try
            {
                String BridgeOrder = Request.Params["Bridges"].ToString();
                String inXML = "<login>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "  <userID>" + Session["userID"].ToString() + "</userID>";
                inXML += "  <bridgeOrder>";
                for (int i = 0; i < BridgeOrder.Split(';').Length - 1; i++)
                {
                    inXML += "      <bridge>";
                    inXML += "          <order>" + (i + 1).ToString() + "</order>";
                    inXML += "          <bridgeID>" + BridgeOrder.Split(';')[i].ToString() + "</bridgeID>";
                    inXML += "      </bridge>";
                }
                inXML += "  </bridgeOrder>";
                inXML += "</login>";
                //Response.Write(obj.Transfer(inXML));
                //Response.End();
                //String outXML = obj.CallMyVRMServer("SetBridge", inXML, Application["MyVRMServer_ConfigPath"].ToString());//Wrong Command used
                //String outXML = obj.CallCOM("SetBridgeList", inXML, Application["COM_ConfigPath"].ToString());
                String outXML = obj.CallMyVRMServer("SetBridgeList", inXML, Application["MyVRMServer_ConfigPath"].ToString());//FB 2027
                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
                else
                    Response.Redirect("ManageBridge.aspx?m=1");
            }
            catch (Exception ex)
            {
                errLabel.Text = ex.StackTrace;
                errLabel.Visible = true;
            }
        }
        // Method Added for FB 1920  
        #region BindRowsToGrid
        protected void BindRowsToGrid(Object sender, DataGridItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType.Equals(ListItemType.Item) || e.Item.ItemType.Equals(ListItemType.AlternatingItem))
                {
                    DataRowView row = e.Item.DataItem as DataRowView;
					//FB 2599 Start
                    LinkButton btnTemp = (LinkButton)e.Item.FindControl("btnDelete");
                    LinkButton btnTemp2 = (LinkButton)e.Item.FindControl("btnEdit");
					//FB 2599 End
                   // string MCUispublic = node.SelectSingleNode("isPublic").InnerText;
                    //FB 2599 Start
                    if (Cloud == 1) //FB 2262
                    {
                        btnTemp.Enabled = false;
                        btnTemp2.Enabled = false;
                    }

                    if (Session["organizationID"].ToString() != "11")
                    {
                        if (row["isPublic"].ToString() != "0" && row["Orgid"].ToString() == "11")
                        {
                            //LinkButton btnTemp = (LinkButton)e.Item.FindControl("btnDelete");//FB 2262
                            //LinkButton btnTemp2 = (LinkButton)e.Item.FindControl("btnEdit");//FB 2262
                            btnTemp.Enabled = false;
                            btnTemp2.Enabled = false;
                        }
                    }
                    //FB 2599 End
                }
            }
            catch (Exception ex)
            {
                errLabel.Text = ex.Message;
                errLabel.Visible = true;
            }
        }
        #endregion

        //FB 1920
        protected void ManageMCUOrder(Object sender, EventArgs e)
        {
            try
            {
                ManageOrder();

            }
            catch (Exception ex)
            {
                
                 errLabel.Text = ex.StackTrace;
                errLabel.Visible = true;
            }
        }
        
    }
}

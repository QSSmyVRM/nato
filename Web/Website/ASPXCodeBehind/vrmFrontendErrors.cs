using System;
using System.Collections.Generic;
using System.Text;

namespace ns_MyVRMNet
{
    public class ErrorList
    {
        public const String InvalidDuration = "Please enter a valid Duration.";
        public const String InvalidIPAddress = "Invalid IP Address.";
        public const String InvalidISDNAddress = "Invalid ISDN Address.";
        public const String InvalidMPIAddress = "Invalid MPI Address - Alphanumeric characters only.";
        public const String InvalidAccountExpiry = "Invalid Account Expiration Date.";
        public const String InvalidMPIBridge = "Please select a bridge with at least one associated MPI Service.";
    }
}

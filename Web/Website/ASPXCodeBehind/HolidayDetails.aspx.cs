using System;
using System.Data;
using System.Xml;
using System.Web.UI.WebControls;
using System.Drawing;

/// <summary>
/// Summary description for ConferenceOrders
/// </summary>
/// 
namespace ns_HolidayDetails
{
    public partial class HolidayDetails : System.Web.UI.Page
    {
        #region Protected Data Members

        myVRMNet.NETFunctions obj;
        ns_Logger.Logger log;
        MyVRMNet.LoginManagement objLogin;

        protected System.Web.UI.WebControls.Label lblHeader;
        protected System.Web.UI.WebControls.Label errLabel;
        protected System.Web.UI.WebControls.TextBox txtMultiDepartment;
        protected System.Web.UI.WebControls.TextBox txtNewDepartmentName;
        protected System.Web.UI.WebControls.Table tblNoDetails;
        protected System.Web.UI.WebControls.DataGrid dgHolidayDetails;
        protected System.Web.UI.WebControls.Button btnManageOrder;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnValue;
        protected System.Web.UI.HtmlControls.HtmlInputHidden Bridges;
        
        #endregion

        #region HolidayDetails 

        public HolidayDetails()
        {
            obj = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger();
            objLogin = new MyVRMNet.LoginManagement();
        }

        #endregion

        #region Page Load 

        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                lblHeader.Text = obj.GetTranslatedText("Manage Day Color"); //FB 2272
                errLabel.Text = "";
                if (Request.QueryString["m"] != null)
                    if (Request.QueryString["m"].ToString().Equals("1"))
                    {
                        errLabel.Text = obj.GetTranslatedText("Operation Successful!");
                        errLabel.Visible = true;
                    }

                if (!IsPostBack)
                    BindData();

            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                errLabel.Text = "PageLoad: " + ex.StackTrace;
            }
        }
        #endregion

        #region BindData 

        private void BindData()
        {
            try
            {
                String inXML = "";
                inXML += "<login>";
                inXML += "  <userID>" + Session["userID"].ToString() + "</userID>";
                inXML += obj.OrgXMLElement();
                inXML += "</login>";
                String outXML = obj.CallMyVRMServer("GetHolidayType", inXML, Application["MyVRMServer_ConfigPath"].ToString());                
                if (outXML.IndexOf("<error>") < 0)
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);
                    XmlNodeList nodes = xmldoc.SelectNodes("//SystemHolidayType/HolidayTypes/HolidayType");
                    LoadDetailsGrid(nodes);


                    foreach (XmlNode node in nodes)
                    {
                        if(hdnValue.Value == "")
                            hdnValue.Value = node.SelectSingleNode("id").InnerText + "``" + node.SelectSingleNode("HolidayDescription").InnerText + "||";
                        else
                            hdnValue.Value += node.SelectSingleNode("id").InnerText + "``" + node.SelectSingleNode("HolidayDescription").InnerText + "||";
                    }

                    btnManageOrder.Attributes.Add("style", "display:none");

                    if (dgHolidayDetails.Items.Count > 0)
                        btnManageOrder.Attributes.Add("style", "display:block");                    
                }
                else
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible=true;
                }
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                errLabel.Text = "BindData: " + ex.StackTrace;
            }
        }

        #endregion

        #region LoadDetailsGrid 
        protected void LoadDetailsGrid(XmlNodeList nodes)
        {
            try
            {
                XmlTextReader xtr;
                DataSet ds = new DataSet();

                foreach (XmlNode node in nodes)
                {
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.InferSchema);
                }
                DataView dv;
                DataTable dt;
                if (ds.Tables.Count > 0)
                {
                    dv = new DataView(ds.Tables[0]);
                    dt = dv.Table;

                    if (!dt.Columns.Contains("RowUID"))
                        dt.Columns.Add("RowUID");

                    for (Int32 i = 0; i < dt.Rows.Count; i++)
                    {
                        dt.Rows[i]["RowUID"] = (i + 1).ToString();
                    }

                    dgHolidayDetails.DataSource = dt;
                    dgHolidayDetails.DataBind();
                    //if (Session["Holidays"] != null)
                    //    Session["Holidays"] = nodes;
                    //else
                    //    Session.Add("Holidays", nodes);
                }
                else
                    tblNoDetails.Visible = true;
            }
            catch (Exception ex)
            {
                errLabel.Text = ex.StackTrace;
                errLabel.Visible = true;
            }
        }
        #endregion

        #region dgHolidayDetails_ItemDataBound
        protected void dgHolidayDetails_ItemDataBound(Object sender, DataGridItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType.Equals(ListItemType.Item) || e.Item.ItemType.Equals(ListItemType.AlternatingItem))
                {
                    TextBox txtColor = (TextBox)e.Item.FindControl("txtColor");

                    if (txtColor != null)
                    {
                        if (txtColor.Text != "")
                            txtColor.ForeColor = ColorTranslator.FromHtml(txtColor.Text);
                    }
                }
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        #region EditDetails
        protected void EditDetails(object sender, DataGridCommandEventArgs e)
        {
            String inXML = "";
            String outXML = "";
            try
            {
                Response.Redirect("EditHolidayDetails.aspx?TypeID=" + e.Item.Cells[0].Text);
            }
            catch (System.Threading.ThreadAbortException) { }
            catch (Exception ex)
            {
                log.Trace(ex.Message);
                errLabel.Text = obj.ShowSystemMessage();
                errLabel.Visible = true;
            }
        }
        #endregion

        #region DeleteHolidayDetails
        protected void DeleteHolidayDetails(object sender, DataGridCommandEventArgs e)
        {
            try
            {
                String inXML = ""; 
                inXML += "<login>";
                inXML += "  <userID>" + Session["userID"].ToString() + "</userID>";
                inXML += obj.OrgXMLElement();
                inXML += "  <delete>";
                inXML += "      <typeID>" + e.Item.Cells[0].Text + "</typeID>";
                inXML += "  </delete>";
                inXML += "</login>";

                String outXML = obj.CallMyVRMServer("DeleteHolidayDetails", inXML, Application["MyVRMServer_ConfigPath"].ToString());

                if (outXML.IndexOf("<error>") < 0)
                {
                    objLogin.LoadOrgHolidays();
                    Response.Redirect("HolidayDetails.aspx?m=1");
                }
                else
                {
                    errLabel.Visible = true;
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                }
            }
            catch (Exception ex)
            {
                errLabel.Text = ex.StackTrace;
                errLabel.Visible = true;
            }
        }
        #endregion

        #region DeleteHolidayTypeMsg 

        protected void DeleteHolidayTypeMsg(Object sender, DataGridItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType.Equals(ListItemType.Item) || e.Item.ItemType.Equals(ListItemType.AlternatingItem))
                {
                    LinkButton btnTemp = (LinkButton)e.Item.FindControl("btnDelete");
                    btnTemp.Attributes.Add("onclick", "return confirm('" + obj.GetTranslatedText("Are you sure you want to delete this Day Color?")+"')"); //FB 2272
                }
            }
            catch (Exception ex)
            {
                errLabel.Text = ex.StackTrace;
                errLabel.Visible = true;
            }
        }
        #endregion

        #region ManageTypeOrder
        protected void ManageTypeOrder(Object sender, EventArgs e)
        {
            try
            {
                String DayOrder = Request.Params["Bridges"].ToString();
                String inXML = "<login>";
                inXML += obj.OrgXMLElement();
                inXML += "  <userID>" + Session["userID"].ToString() + "</userID>";
                inXML += "  <DayOrder>";
                for (int i = 0; i < DayOrder.Split(';').Length - 1; i++)
                {
                    inXML += "      <Day>";
                    inXML += "          <Order>" + (i + 1).ToString() + "</Order>";
                    inXML += "          <DayID>" + DayOrder.Split(';')[i].ToString() + "</DayID>";
                    inXML += "      </Day>";
                }
                inXML += "  </DayOrder>";
                inXML += "</login>";

                String outXML = obj.CallMyVRMServer("SetDayOrderList", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
                else
                {
                    objLogin.LoadOrgHolidays();
                    Response.Redirect("HolidayDetails.aspx?m=1");
                }
            }
            catch (Exception ex)
            {
                errLabel.Text = ex.StackTrace;
                errLabel.Visible = true;
            }
        }
        #endregion

        #region CreateNewHolidayDetails 
        protected void CreateNewHolidayDetails(Object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("EditHolidayDetails.aspx");
            }
            catch (System.Threading.ThreadAbortException) { }
            catch (Exception ex)
            {
                errLabel.Text = ex.StackTrace;
                errLabel.Visible = true;
            }
        }

        #endregion

    }
}

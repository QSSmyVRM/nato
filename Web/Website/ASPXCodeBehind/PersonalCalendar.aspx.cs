/** FB 1850 Both aspx and cs were restructured for performance **/

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.Text;
using System.Threading;
using System.ComponentModel;
using System.Linq;
using System.Xml.Linq;
using DevExpress.Web.ASPxTabControl;

public partial class PersonalCalendar : System.Web.UI.Page
{


    protected System.Web.UI.HtmlControls.HtmlGenericControl Html1;
    protected System.Web.UI.HtmlControls.HtmlHead Head1;
    protected System.Web.UI.HtmlControls.HtmlForm frmPersonalCalendar;
    protected System.Web.UI.ScriptManager CalendarScriptManager;
    protected System.Web.UI.HtmlControls.HtmlInputHidden IsMonthChanged;
    protected System.Web.UI.HtmlControls.HtmlInputHidden IsSettingsChange;
    protected System.Web.UI.HtmlControls.HtmlInputHidden IsWeekChanged;
    protected System.Web.UI.HtmlControls.HtmlInputHidden IsWeekOverLap;
    protected System.Web.UI.HtmlControls.HtmlInputHidden HdnMonthlyXml;
    protected System.Web.UI.WebControls.Label errLabel;
    protected System.Web.UI.WebControls.Label CalenMenu;
    protected System.Web.UI.UpdatePanel PanelTab;
    protected System.Web.UI.HtmlControls.HtmlInputHidden HdnXml;
    protected System.Web.UI.WebControls.CheckBox officehrDaily;
    protected System.Web.UI.WebControls.CheckBox officehrWeek;
    protected System.Web.UI.WebControls.CheckBox officehrMonth;
    protected System.Web.UI.WebControls.Button btnDate;
    protected System.Web.UI.WebControls.TreeView treeRoomSelection;
    protected System.Web.UI.WebControls.TextBox txtType;
    protected System.Web.UI.HtmlControls.HtmlInputHidden txtSelectedDate;
    protected System.Web.UI.HtmlControls.HtmlInputHidden txtSelectedDate1;
    protected System.Web.UI.HtmlControls.HtmlTableCell Tdbuffer1;
    protected System.Web.UI.HtmlControls.HtmlTableCell Tdbuffer2;
    protected System.Web.UI.HtmlControls.HtmlTableCell Tdbuffer3;
    protected System.Web.UI.HtmlControls.HtmlTableCell Tdbuffer4;
    protected System.Web.UI.HtmlControls.HtmlTableCell ActionsTD;
    protected System.Web.UI.HtmlControls.HtmlTableCell TdAV1;
    protected System.Web.UI.HtmlControls.HtmlTableCell TdAV;
    protected System.Web.UI.HtmlControls.HtmlTableCell TdA;
    protected System.Web.UI.HtmlControls.HtmlTableCell TdA1;
    protected System.Web.UI.HtmlControls.HtmlTableCell TdP2p1;
    protected System.Web.UI.HtmlControls.HtmlTableCell TdP2p;
    protected System.Web.UI.HtmlControls.HtmlSelect lstCalendar;//FB 2585

    protected DayPilot.Web.Ui.DayPilotCalendar schDaypilot;
    protected DayPilot.Web.Ui.DayPilotMonth schDaypilotMonth;
    protected DayPilot.Web.Ui.DayPilotCalendar schDaypilotweek;
    protected DayPilot.Web.Ui.DayPilotBubble Details;
    protected DayPilot.Web.Ui.DayPilotBubble DetailsMonthly;
    protected DayPilot.Web.Ui.DayPilotBubble DetailsWeekly;

    protected DevExpress.Web.ASPxTabControl.ASPxPageControl CalendarContainer;

    protected Int32 hasApproal;
    protected string paramHF = "";
    protected string dtFormatType = "MM/dd/yyyy";
    protected string CalendarType = "D";
    protected string timeFormat = "1";
    protected string tFormats = "hh:mm tt";
    protected string isPublic = "D", isFuture = "D", isPending = "D", isApproval = "D", isOngoing = "D";
    protected Boolean bypass = false, isAdminRole = false;
    protected DateTime conf = DateTime.Now;
    myVRMNet.NETFunctions obj;
    ns_Logger.Logger log;
    protected string enableBufferZone = "";
    //Organization/CSS Module -- Start
    protected System.Web.UI.HtmlControls.HtmlGenericControl spnAV;
    protected System.Web.UI.HtmlControls.HtmlGenericControl spnRmHrg;
    protected System.Web.UI.HtmlControls.HtmlGenericControl spnAudCon;
    protected System.Web.UI.HtmlControls.HtmlGenericControl spnPpConf;
    protected System.Web.UI.HtmlControls.HtmlGenericControl spnAVW;
    protected System.Web.UI.HtmlControls.HtmlGenericControl spnRmHrgW;
    protected System.Web.UI.HtmlControls.HtmlGenericControl spnAudConW;
    protected System.Web.UI.HtmlControls.HtmlGenericControl spnPpConfW;
    protected System.Web.UI.HtmlControls.HtmlGenericControl spnAVM;
    protected System.Web.UI.HtmlControls.HtmlGenericControl spnRmHrgM;
    protected System.Web.UI.HtmlControls.HtmlGenericControl spnAudConM;
    protected System.Web.UI.HtmlControls.HtmlGenericControl spnPpConfM;
    CustomizationUtil.CSSReplacementUtility cssUtil;
    //Organization/CSS Module -- End

    private delegate void GetCalendar();

    protected System.Web.UI.WebControls.CheckBox showDeletedConf; //FB 1800
    
    DateTime dtCell = DateTime.Now;//FB 1861
    string cellColor = "";//FB 1861
    protected System.Web.UI.HtmlControls.HtmlTableRow trlegend1; //FB 1985
    protected System.Web.UI.HtmlControls.HtmlTableRow trlegend2; //FB 1985
    protected System.Web.UI.HtmlControls.HtmlTableRow trlegend3; //FB 1985


    #region Page_Load

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            log = new ns_Logger.Logger();   //Location Issues
            obj = new myVRMNet.NETFunctions();

            //Organization/CSS Module - Create folder for UI Settings --- Strat

            if (Session["isMultiLingual"] != null)
            {
                if (Session["isMultiLingual"].ToString() != "1")
                {
                    string fieldText = "";
                    cssUtil = new CustomizationUtil.CSSReplacementUtility();

                    fieldText = cssUtil.GetUITextForControl("PersonalCalendar.aspx", "spnAV");
                    spnAV.InnerText = obj.GetTranslatedText(fieldText);
                    spnAVW.InnerText = obj.GetTranslatedText(fieldText);
                    spnAVM.InnerText = obj.GetTranslatedText(fieldText);

                    fieldText = cssUtil.GetUITextForControl("PersonalCalendar.aspx", "spnRmHrg");
                    spnRmHrg.InnerText = obj.GetTranslatedText(fieldText);
                    spnRmHrgW.InnerText = obj.GetTranslatedText(fieldText);
                    spnRmHrgM.InnerText = obj.GetTranslatedText(fieldText);

                    if (Application["client"] != null)
                        if (Application["client"].ToString().ToUpper() != "MOJ")
                        {
                            fieldText = cssUtil.GetUITextForControl("PersonalCalendar.aspx", "spnAudCon");
                            spnAudCon.InnerText = obj.GetTranslatedText(fieldText);
                            spnAudConW.InnerText = obj.GetTranslatedText(fieldText);
                            spnAudConM.InnerText = obj.GetTranslatedText(fieldText);

                            fieldText = cssUtil.GetUITextForControl("PersonalCalendar.aspx", "spnPpConf");
                            spnPpConf.InnerText = obj.GetTranslatedText(fieldText);
                            spnPpConfW.InnerText = obj.GetTranslatedText(fieldText);
                            spnPpConfM.InnerText = obj.GetTranslatedText(fieldText);
                        }
                    //Organization/CSS Module - Create folder for UI Settings --- End
                }
            }

            if (Session["EnableBufferZone"] == null)//Organization Module Fixes
            {
                Session["EnableBufferZone"] = "0";//Organization Module Fixes
            }

            enableBufferZone = Session["EnableBufferZone"].ToString();//Organization Module Fixes

            if (Application["Client"].ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.MOJ))//Code added for Phase2
            {
                Tdbuffer1.Attributes.Add("style", "display:none");
                Tdbuffer2.Attributes.Add("style", "display:none");
                Tdbuffer3.Attributes.Add("style", "display:none");
                Tdbuffer4.Attributes.Add("style", "display:none");
                TdA.Attributes.Add("style", "display:none");
                TdA1.Attributes.Add("style", "display:none");
                TdP2p.Attributes.Add("style", "display:none");
                TdP2p1.Attributes.Add("style", "display:none");
                TdAV.Attributes.Add("style", "display:none");
                TdAV1.Attributes.Add("style", "display:none");
                
                enableBufferZone = "0";
            }

            if (enableBufferZone == "0")
            {
                schDaypilot.UseEventBoxes = DayPilot.Web.Ui.Enums.UseBoxesEnum.ShortEventsOnly;
                schDaypilotweek.UseEventBoxes = DayPilot.Web.Ui.Enums.UseBoxesEnum.ShortEventsOnly;
            }

            if (Session["admin"] != null)
            {
                if (Session["admin"].ToString() == "1" || Session["admin"].ToString() == "2")   //buffer zone - Super & conf admin
                    isAdminRole = true;
            }

            if (!DateTime.TryParse(txtSelectedDate.Value, out conf))
                conf = DateTime.Today;

            if (Request.QueryString["hf"] != null)
            {
                paramHF = Request.QueryString["hf"].ToString();
            }

            if (Session["FormatDateType"] != null)
            {
                if (Session["FormatDateType"].ToString() == "")
                    Session["FormatDateType"] = "MM/dd/yyyy";
                else
                    dtFormatType = Session["FormatDateType"].ToString();
            }

            schDaypilot.HeaderDateFormat = "d MMMM yyyy";
            schDaypilotweek.HeaderDateFormat = dtFormatType;

            timeFormat = ((Session["timeFormat"] != null) ? Session["timeFormat"].ToString() : timeFormat);

            if (Session["timeFormat"].ToString() == "0")
            {
                tFormats = "HH:mm";
                schDaypilot.TimeFormat = DayPilot.Web.Ui.Enums.TimeFormat.Clock24Hours;
                schDaypilotweek.TimeFormat = DayPilot.Web.Ui.Enums.TimeFormat.Clock24Hours;
            }

            if (Session["Holidays"] != null)
            {
                if (Session["Holidays"].ToString().Trim() != "")
                {
                    schDaypilotweek.BeforeCellRender += new DayPilot.Web.Ui.Events.BeforeCellRenderEventHandler(BeforeCellRenderhandler);
                    
                    schDaypilotMonth.BeforeCellRender +=new DayPilot.Web.Ui.Events.Month.BeforeCellRenderEventHandler(BeforeMonthRenderhandler);

                }
            }
			//FB 1985 
            if (Application["Client"].ToString().ToUpper() == "DISNEY")
            {
                trlegend1.Style.Add("display", "none");
                trlegend2.Style.Add("display", "none");
                trlegend3.Style.Add("display", "none");
            }
            if (!IsPostBack)
            {
                //if (Session["CalendarMonthly"] == null) //FB 2027
                {
					//FB 1675 start
                    //GetCalendar monthly = new GetCalendar(GetMonthlyThread);
                    //monthly.BeginInvoke(null, null);
                    GetMonthlyThread();
					//FB 1675 end
                }

                

                officehrDaily.Checked = true;
                officehrWeek.Checked = true;
                officehrMonth.Checked = true;

                if (Session["DefaultCalendarToOfficeHours"] != null)//Organization Module Fixes
                {
                    if (Session["DefaultCalendarToOfficeHours"].ToString() == "0")
                    {
                        officehrDaily.Checked = false;
                        officehrWeek.Checked = false;
                        officehrMonth.Checked = false;
                    }
                    
                }


                treeRoomSelection.Nodes[0].Checked = true;

                foreach (TreeNode nds in treeRoomSelection.Nodes[0].ChildNodes)
                    nds.Checked = true;

                if (Request.QueryString["hf"].ToString() == "1")
                {
                    if (Request.QueryString["d"] != null)
                    {
                        if (!DateTime.TryParse(myVRMNet.NETFunctions.GetDefaultDate(Request.QueryString["d"].ToString()), out conf))
                            conf = DateTime.Today;
                    }

                }

                if (Session["CalendarMonthly"] != null)
                {
                    HdnMonthlyXml.Value = Session["CalendarMonthly"].ToString();
                    ViewState["HdnMonthlyXml"] = Session["CalendarMonthly"].ToString();
 
                }
                
                ChangeCalendarDate(null, null);

            }
            if (Session["isExpressUser"] != null)//FB 1779
            {
                if (Session["isExpressUser"].ToString() == "1")
                    schDaypilot.CellSelectColor = System.Drawing.Color.Empty;                   
                
                if (Session["isExpressUser"].ToString() == "1" && Session["isExpressUserAdv"].ToString() == "0")//FB 2585
                {
                    lstCalendar.Items.Remove(lstCalendar.Items[4]);
                    lstCalendar.Items.Remove(lstCalendar.Items[3]);
                }
            }
        }
        catch (Exception ex)
        {
            errLabel.Text = ex.StackTrace;
            errLabel.Visible = true;
        }

    }

    #endregion

    #region ChangeCalendarDate

    protected void ChangeCalendarDate(Object sender, EventArgs e)
    {
      
  
        try
        {
            switch (CalendarContainer.ActiveTabIndex)
            {
                case 0:
                    DatatablefromXML(GetCalendarOutXml(), 1);
                    break;
                case 1:
                    Int32 addsessn = 1;
                    if (IsWeekOverLap.Value == "Y")
                        addsessn = 0;
                    else
                        Session.Remove("PersonalWeekly");
                    DatatablefromXML(GetWeeklyCalendar(), addsessn);
                    break;
                case 2:
                    DatatablefromXML(GetMonthlyCalendar(), 1);
                    break;
            }
            BindWeekly();
            BindDaily();
            
        }
        catch (Exception ex)
        {
            errLabel.Text = ex.StackTrace;
            errLabel.Visible = true;
            log.Trace("ChangeCalendarDate: " + ex.StackTrace + " : " + ex.Message);
        }
    }

    #endregion

    #region GetCalendarOutXml
    public string GetCalendarOutXml()
    {
        string inXML = "";
        string outXMLDly = "";
        try
        {
            if (ViewState["HdnMonthlyXml"] != null)
            {
                if (ViewState["HdnMonthlyXml"].ToString() != "")
                    HdnMonthlyXml.Value = ViewState["HdnMonthlyXml"].ToString();
            }

            outXMLDly = HdnMonthlyXml.Value;

            if ((IsMonthChanged.Value == "Y") || outXMLDly == "")
            {

                inXML = "<calendarView>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID><date>" + conf.ToString("MM / dd / yyyy") + "</date><room></room><isDeletedConf>1</isDeletedConf></calendarView>";//Organization Module Fixes //FB 1800
                outXMLDly = obj.CallMyVRMServer("GetRoomDailyView", inXML, Application["MyVRMServer_ConfigPath"].ToString()); //FB 2027
               
            }


            return outXMLDly;
        }
        catch (Exception ex)
        {
            errLabel.Text  = obj.ShowSystemMessage();
            errLabel.Visible = true;
            log.Trace(ex.StackTrace + "GetCalendarOutXml: Error Retrieving Data" + ex.Message);
            return obj.ShowSystemMessage();//FB 1881
            //return "<error><level>E</level><message>Error Retrieving Data</message></error>";//FB 1881
        }
    }
    #endregion

    #region GetMonthlyCalendar
    public string GetMonthlyCalendar()
    {
        string inXML = "";
        string outXmlMn = "";
        try
        {
            if (ViewState["HdnMonthlyXml"] == null) //FB 1675
                Thread.Sleep(100);
          
            
            inXML = "<calendarView>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID><date>" + conf.ToString("MM / dd / yyyy") + "</date><room></room><isDeletedConf>1</isDeletedConf></calendarView>";//Organization Module Fixes //FB 1800

            if (ViewState["HdnMonthlyXml"] != null)
            {
                if (ViewState["HdnMonthlyXml"].ToString() != "")
                {
                    HdnMonthlyXml.Value = ViewState["HdnMonthlyXml"].ToString();
                    
                }
            }

            outXmlMn = HdnMonthlyXml.Value;

            if ((IsMonthChanged.Value == "Y" || HdnMonthlyXml.Value == ""))
            {
                outXmlMn = obj.CallMyVRMServer("GetRoomMonthlyView", inXML, Application["MyVRMServer_ConfigPath"].ToString()); //FB 2027
                HdnMonthlyXml.Value = outXmlMn;
                ViewState["HdnMonthlyXml"] = outXmlMn;
            }

            return outXmlMn;
        }
        catch (Exception ex)
        {
            errLabel.Text  = obj.ShowSystemMessage();
            errLabel.Visible = true;
            log.Trace(ex.StackTrace + "GetMonthlyCalendar: Error Retrieving Data" + ex.Message);
            return obj.ShowSystemMessage();//FB 1881
            //return "<error><level>E</level><message>Error Retrieving Data</message></error>";
        }
    }

    public void GetMonthlyThread()
    {
        string inXML = "";
        string outXmlMn = "";
        try
        {
            inXML = "<calendarView>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID><date>" + conf.ToString("MM / dd / yyyy") + "</date><room></room><isDeletedConf>1</isDeletedConf></calendarView>";//Organization Module Fixes //FB 1800
            outXmlMn = obj.CallMyVRMServer("GetRoomMonthlyView", inXML, Application["MyVRMServer_ConfigPath"].ToString()); //FB 2027
            outXmlMn = outXmlMn.Replace("\t", "").Replace("\n", "").Replace("\r", ""); //FB 2012
            ViewState["HdnMonthlyXml"] = outXmlMn;            
            HdnMonthlyXml.Value = outXmlMn;
            Session.Remove("CalendarMonthly");
            Session.Add("CalendarMonthly", outXmlMn);

        }
        catch (Exception ex)
        {
            errLabel.Text = ex.StackTrace;
            errLabel.Visible = true;

        }
        
    }
    #endregion

    #region GetWeeklyCalendar
    public string GetWeeklyCalendar()
    {
        string inXML = "";
        string outXMLWkly = "";
        try
        {
            if (ViewState["HdnMonthlyXml"] == null) //FB 1675
                Thread.Sleep(100); 

            if (ViewState["HdnMonthlyXml"] != null)
            {
                if (ViewState["HdnMonthlyXml"].ToString() != "")
                {
                    HdnMonthlyXml.Value = ViewState["HdnMonthlyXml"].ToString();
                    ViewState["HdnMonthlyXml"] = "";
                }
            }

            outXMLWkly = HdnMonthlyXml.Value;
                      
            if (IsMonthChanged.Value == "Y"  || IsWeekOverLap.Value == "Y")
            {

                inXML = "<calendarView>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID><date>" + conf.ToString("MM / dd / yyyy") + "</date><room></room><isDeletedConf>1</isDeletedConf></calendarView>";//Organization Module Fixes //FB 1800
                outXMLWkly = obj.CallMyVRMServer("GetRoomWeeklyView", inXML, Application["MyVRMServer_ConfigPath"].ToString()); //FB 2027
                //HdnMonthlyXml.Value = outXMLWkly;
            }
            

            return outXMLWkly;
        }
        catch (Exception ex)
        {
            //errLabel.Text = ex.StackTrace;
            errLabel.Text  = obj.ShowSystemMessage();
            errLabel.Visible = true;
            log.Trace(ex.StackTrace + "GetWeeklyCalendar: Error Retrieving Data" + ex.Message);
            return obj.ShowSystemMessage();//FB 1881
            //return "<error><level>E</level><message>Error Retrieving Data</message></error>";
        }
    }


    public void GetWeekly()
    {
        string inXML = "";
        string outXMLWkly = "";
        try
        {
            inXML = "<calendarView>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID><date>" + conf.ToString("MM / dd / yyyy") + "</date><room></room><isDeletedConf>1</isDeletedConf></calendarView>";//Organization Module Fixes //FB 1800
            outXMLWkly = obj.CallMyVRMServer("GetRoomWeeklyView", inXML, Application["MyVRMServer_ConfigPath"].ToString()); //FB 2027
            ViewState["HdnWeeklyXml"] = outXMLWkly;
        }
        catch (Exception ex)
        {
            errLabel.Text = ex.StackTrace;
            errLabel.Visible = true;
        }
    }

    #endregion

    #region BeforeEventRenderhandler

    protected void BeforeEventRenderhandler(object sender, DayPilot.Web.Ui.Events.BeforeEventRenderEventArgs e)
    {
        try
        {

            e.InnerHTML = e.Text;
            switch (e.Tag[0])
            {
                case ns_MyVRMNet.vrmConfType.AudioOnly: e.BackgroundColor = "#EAA2D4"; ; break;
                case ns_MyVRMNet.vrmConfType.AudioVideo: e.BackgroundColor = "#BBB4FF"; ; break;
                case ns_MyVRMNet.vrmConfType.P2P: e.BackgroundColor = "#85EE99"; ; break;
                case ns_MyVRMNet.vrmConfType.RoomOnly: e.BackgroundColor = "#F16855"; ; break;
                case "S": e.BackgroundColor = "#FFCC99"; ; break;
                case "T": e.BackgroundColor = "#CCCC99"; ; break;
                case "9": e.BackgroundColor = "#01DFD7"; ; break;
                case "10": e.BackgroundColor = "#82CAFF"; ; break; //FB 2448

            }
        }
        catch (Exception ex)
        {
            errLabel.Text = ex.StackTrace;
            errLabel.Visible = true;
        }
    }

    #endregion

    #region BeforeEventRenderhandler

    protected void BeforeEventRenderhandler(object sender, DayPilot.Web.Ui.Events.Month.BeforeEventRenderEventArgs e )
    {
        try
        {

            e.InnerHTML = e.Text;
            switch (e.Tag[0])
            {
                case ns_MyVRMNet.vrmConfType.AudioOnly: e.BackgroundColor = "#EAA2D4"; ; break;
                case ns_MyVRMNet.vrmConfType.AudioVideo: e.BackgroundColor = "#BBB4FF"; ; break;
                case ns_MyVRMNet.vrmConfType.P2P: e.BackgroundColor = "#85EE99"; ; break;
                case ns_MyVRMNet.vrmConfType.RoomOnly: e.BackgroundColor = "#F16855"; ; break;
                case "9": e.BackgroundColor = "#01DFD7"; ; break;
                case "10": e.BackgroundColor = "#82CAFF"; ; break; //FB 2448


            }
        }
        catch (Exception ex)
        {
            errLabel.Text = ex.StackTrace;
            errLabel.Visible = true;
        }
    }
    #endregion
    //2272 - Start

    #region BeforeTimeHeaderRender
    protected void BeforeTimeHeaderRender(Object sender, DayPilot.Web.Ui.Events.BeforeHeaderRenderEventArgs e)
    {
        try
        {
            string dt, mon;            
            DayPilot.Web.Ui.Events.BeforeHeaderRenderEventArgs DateEvent = (DayPilot.Web.Ui.Events.BeforeHeaderRenderEventArgs)e;            
            dt = DateEvent.InnerHTML;
            mon = dt.Split(' ')[1];
            dt = dt.Replace(mon, obj.GetTranslatedText(mon));
            DateEvent.InnerHTML = dt;
            DateEvent.ToolTip = dt;            
        }
        catch (Exception ex)
        {
            errLabel.Text = ex.StackTrace;
            errLabel.Visible = true;
        }
    }
    #endregion

    #region BeforeTimeHeaderRender
    protected void BeforeTimeHeaderRender(Object sender, DayPilot.Web.Ui.Events.Month.BeforeHeaderRenderEventArgs e)
    {
        try
        {
            DayPilot.Web.Ui.Events.Month.BeforeHeaderRenderEventArgs DateEvent = (DayPilot.Web.Ui.Events.Month.BeforeHeaderRenderEventArgs)e;

            DateEvent.InnerHTML = obj.GetTranslatedText(e.DayOfWeek.ToString());

        }
        catch (Exception ex)
        {
            errLabel.Text = ex.StackTrace;
            errLabel.Visible = true;
        }
    }
    #endregion

    //2272 - End

    #region BubbleRenderhandler
    protected void BubbleRenderhandler(object sender, DayPilot.Web.Ui.Events.Bubble.RenderEventArgs e)
    {
        try
        {
            DayPilot.Web.Ui.Events.Bubble.RenderEventBubbleEventArgs re = (DayPilot.Web.Ui.Events.Bubble.RenderEventBubbleEventArgs)e;
            re.InnerHTML = re.Tag["CustomDescription"];

        }
        catch (Exception ex)
        {
            errLabel.Text = ex.StackTrace;
            errLabel.Visible = true;
        }
    }
    #endregion    

    #region BeforeCellRenderhandler
    /// <summary>
    /// // FB 1860
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void BeforeCellRenderhandler(object sender, DayPilot.Web.Ui.Events.BeforeCellRenderEventArgs e)
    {
        try
        {
            if (Session["Holidays"] != null)
            {
                if (Session["Holidays"].ToString().Trim() != "")
                {
                    dtCell = e.Start;
                    GetdayColour();

                    if (cellColor != "")
                        e.BackgroundColor = cellColor;

                }
            }

            

           /* DateTime cellTIme = e.Start;
            if (cellTIme.Day % 5 == 0)
                e.BackgroundColor = "#EAA2D5";
            else if (cellTIme.Day % 7 == 0)
                e.BackgroundColor = "#85EE98";*/
            
        }
        catch (Exception ex)
        {
            errLabel.Text = ex.StackTrace;
            errLabel.Visible = true;
        }
    }

    #endregion

    #region BeforeCellRenderhandler
    /// <summary>
    /// // FB 1860
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void BeforeMonthRenderhandler(object sender, DayPilot.Web.Ui.Events.Month.BeforeCellRenderEventArgs e)
    {
        try
        {
            if (Session["Holidays"] != null)
            {
                if (Session["Holidays"].ToString().Trim() != "")
                {
                    dtCell = e.Start;
                    GetdayColour();

                    if (cellColor != "")
                        e.BackgroundColor = cellColor;

                }
            }
            
            /* DateTime cellTIme = e.Start;
             if (cellTIme.Day % 5 == 0)
                 e.BackgroundColor = "#EAA2D5";
             else if(cellTIme.Day % 7 == 0)
                 e.BackgroundColor = "#85EE98";*/
        }
        catch (Exception ex)
        {
            errLabel.Text = ex.StackTrace;
            errLabel.Visible = true;
        }
    }

    

    #endregion

    #region Get Data Table

    private DataTable GetDataTable()
    {
        DataTable dt = null;
        try
        {
            dt = new DataTable();

            if (!dt.Columns.Contains("start")) dt.Columns.Add("start");
            if (!dt.Columns.Contains("end")) dt.Columns.Add("end");
            if (!dt.Columns.Contains("formatstart")) dt.Columns.Add("formatstart");
            if (!dt.Columns.Contains("formatend")) dt.Columns.Add("formatend");
            if (!dt.Columns.Contains("confDetails")) dt.Columns.Add("confDetails");
            if (!dt.Columns.Contains("ID")) dt.Columns.Add("ID");
            if (!dt.Columns.Contains("ConfID")) dt.Columns.Add("ConfID");
            if (!dt.Columns.Contains("ConferenceType")) dt.Columns.Add("ConferenceType");
            if (!dt.Columns.Contains("RoomID")) dt.Columns.Add("RoomID");
            if (!dt.Columns.Contains("confName")) dt.Columns.Add("confName");
            if (!dt.Columns.Contains("durationMin")) dt.Columns.Add("durationMin");
            if (!dt.Columns.Contains("CustomDescription")) dt.Columns.Add("CustomDescription");


        }
        catch (Exception ex)
        {

            throw ex;
        }

        return dt;

    }

    #endregion

    #region changeDate

    protected void changeDate(Object sender, EventArgs e)
    {
        
        
        try
        {
            BindDaily();

            if (IsWeekOverLap.Value != "Y")
                Session.Remove("PersonalWeekly");

            if ((IsWeekOverLap.Value == "Y" && Session["PersonalWeekly"] == null) || (IsWeekOverLap.Value == "Y" && IsWeekChanged.Value == "Y"))
            {
                int addsessn = 1;
                if (IsWeekOverLap.Value == "Y")
                    addsessn = 0;
                
                DatatablefromXML(GetWeeklyCalendar(), addsessn);
            }

            BindWeekly();

        }
        catch (Exception ex)
        {
            errLabel.Text = ex.StackTrace;
            errLabel.Visible = true;
            log.Trace("ChangeCalendarDate: " + ex.StackTrace + " : " + ex.Message);
        }
    }

    #endregion

    #region Datatable from XML

    protected void DatatablefromXML(string xmls, int addtoSession)
    {
        string hdr = obj.GetTranslatedText("Conference Details"), startHour = "00", startMin = "00", startSet = "AM", endHour = "23", endMin = "59", endSet = "PM", open24 = "";
        int hrs, mins = 0;
        string ConfPassword = "N/A", Password = obj.GetTranslatedText("Password"); //FB 2622
        string CongAtt = "", CongAttStatus = obj.GetTranslatedText("No"), DedicatedVNOCOperator = obj.GetTranslatedText("Dedicated VNOC Operator");//FB 2632
        string OnSiteAVSupport = obj.GetTranslatedText("On-Site A/V Support"), MeetandGreet = obj.GetTranslatedText("Meet and Greet"), ConciergeMonitoring = obj.GetTranslatedText("Concierge Monitoring");

        string locStr = "";
        string setupTxt = "";
        string trdnTxt = "";
        XmlNode node = null;
        XmlNodeList nodes = null;
        XmlDocument xmldoc = null;
        DataTable dt = null;
        StringBuilder m = null;
        XmlNodeList subnotes2 = null;
        XmlNode subnode2 = null;
        try
        {

            if (Application["Client"].ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.MOJ))//Code added for Phase2
                hdr = "Hearing Details";

            String spn = "<span  class=\"eventtext\">";

            if (xmls != "")
            {
                bypass = false;

                if (treeRoomSelection.Nodes[0].Checked)
                    bypass = true;

                if (!bypass)
                {

                    foreach (TreeNode tr in treeRoomSelection.Nodes[0].ChildNodes)
                    {
                        if (tr.Checked)
                        {
                            switch (tr.Value)
                            {

                                case "OG":
                                    isOngoing = "1";
                                    break;
                                case "FC":
                                    isFuture = "1";
                                    break;
                                case "PC":
                                    isPublic = "1";
                                    break;
                                case "PNC":
                                    isPending = "1";
                                    break;
                                case "AC":
                                    isApproval = "1";
                                    break;

                            }
                        }
                    }
                }


                xmldoc = new XmlDocument();
                xmldoc.LoadXml(xmls);

                dt = GetDataTable();

                if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/open24") != null)
                    open24 = xmldoc.DocumentElement.SelectSingleNode("systemAvail/open24").InnerText;

                if (open24 == "1")
                {
                    officehrDaily.Checked = false;
                    officehrDaily.Enabled = false;

                    schDaypilot.BusinessBeginsHour = 0;
                    schDaypilot.BusinessEndsHour = 24;

                    schDaypilotweek.BusinessBeginsHour = 0;
                    schDaypilotweek.BusinessEndsHour = 24;
                }
                else
                {

                    if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/startTime/startHour") != null)
                        startHour = xmldoc.DocumentElement.SelectSingleNode("systemAvail/startTime/startHour").InnerText;

                    if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/startTime/startMin") != null)
                        startMin = xmldoc.DocumentElement.SelectSingleNode("systemAvail/startTime/startMin").InnerText;

                    if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/startTime/startSet") != null)
                        startSet = xmldoc.DocumentElement.SelectSingleNode("systemAvail/startTime/startSet").InnerText;

                    if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/endTime/endHour") != null)
                        endHour = xmldoc.DocumentElement.SelectSingleNode("systemAvail/endTime/endHour").InnerText;

                    if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/endTime/endMin") != null)
                        endMin = xmldoc.DocumentElement.SelectSingleNode("systemAvail/endTime/endMin").InnerText;

                    if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/endTime/endSet") != null)
                        endSet = xmldoc.DocumentElement.SelectSingleNode("systemAvail/endTime/endSet").InnerText;

                    schDaypilot.BusinessBeginsHour = Convert.ToInt32(DateTime.Parse("06/06/2006 " + startHour + ":" + startMin + " " + startSet).ToString("HH"));
                    schDaypilot.BusinessEndsHour = Convert.ToInt32(DateTime.Parse("06/06/2006 " + endHour + ":" + endMin + " " + endSet).ToString("HH"));

                    if (endMin != "00") //FB 2057
                        schDaypilot.BusinessEndsHour = schDaypilot.BusinessEndsHour + 1;

                    schDaypilotweek.BusinessBeginsHour = Convert.ToInt32(DateTime.Parse("06/06/2006 " + startHour + ":" + startMin + " " + startSet).ToString("HH"));
                    schDaypilotweek.BusinessEndsHour = Convert.ToInt32(DateTime.Parse("06/06/2006 " + endHour + ":" + endMin + " " + endSet).ToString("HH"));

                    if (endMin != "00")// FB 2057
                        schDaypilotweek.BusinessEndsHour = schDaypilotweek.BusinessEndsHour + 1;
                }


                nodes = xmldoc.SelectNodes("//days/day/conferences/conference");

                for (int confnodes = 0; confnodes < nodes.Count; confnodes++)// XmlNode node in nodes)
                {
                    node = nodes[confnodes];
                    string confSTime = "";
                    string setupSTime = "", teardownSTime = "", confSDate = "", uniqueID = ""; // FB 2002

                    if (node.SelectSingleNode("durationMin") != null)
                    {

                        if (node.SelectSingleNode("durationMin").InnerText != "")
                        {
                            if (!showDeletedConf.Checked)
                            {
                                if (node.SelectSingleNode("deleted") != null)
                                {
                                    if (node.SelectSingleNode("deleted").InnerText == "1")
                                        continue;
                                }
                            }

                            if (node.SelectSingleNode("isImmediate").InnerText == "0" && node.SelectSingleNode("isFuture").InnerText == "0" && node.SelectSingleNode("isPublic").InnerText == "0" && node.SelectSingleNode("isPending").InnerText == "0" && node.SelectSingleNode("isApproval").InnerText == "0")
                                bypass = true;
                            else if (!treeRoomSelection.Nodes[0].Checked)
                                bypass = false;

                            if (bypass || (node.SelectSingleNode("isImmediate").InnerText == isOngoing || node.SelectSingleNode("isFuture").InnerText == isFuture || node.SelectSingleNode("isPublic").InnerText == isPublic || node.SelectSingleNode("isPending").InnerText == isPending || node.SelectSingleNode("isApproval").InnerText == isApproval))
                            {
                                DataRow dr = dt.NewRow();

                                if (node.SelectSingleNode("confName") != null)
                                    dr["confName"] = node.SelectSingleNode("confName").InnerText;

                                if (Session["isVIP"] != null)
                                {
                                    if (Session["isVIP"].ToString() == "1")
                                    {
                                        if (node.SelectSingleNode("isVIP") != null) // FB 1864
                                            if (node.SelectSingleNode("isVIP").InnerText == "1")
                                                dr["confName"] = dr["confName"].ToString() + "<B> {VIP} </B>";
                                    }
                                }
                                            
                                
                                dr["durationMin"] = node.SelectSingleNode("durationMin").InnerText;
                                hrs = Convert.ToInt32(dr["durationMin"].ToString()) / 60;
                                mins = Convert.ToInt32(dr["durationMin"].ToString()) % 60;
                                if (node.SelectSingleNode("confID") != null)
                                    dr["ConfID"] = node.SelectSingleNode("confID").InnerText;
                                if (node.SelectSingleNode("ConferenceType") != null)
                                    dr["ConferenceType"] = node.SelectSingleNode("ConferenceType").InnerText;

                                //FB 2448 Starts
                                Password = obj.GetTranslatedText("Password"); //FB 2622
                                if (node.SelectSingleNode("isVMR") != null)
                                {
                                    if (node.SelectSingleNode("isVMR").InnerText == "1")
                                    {
                                        dr["ConferenceType"] = "10";
                                        Password = obj.GetTranslatedText("VMR Pin"); //FB 2622
                                    }
                                }
                                //FB 2448 Ends
                                //FB 2622 Starts
                                ConfPassword = obj.GetTranslatedText("N/A");
                                if (node.SelectSingleNode("confPassword") != null)
                                {
                                    if (node.SelectSingleNode("confPassword").InnerText.Trim() != "")
                                        ConfPassword = node.SelectSingleNode("confPassword").InnerText.Trim();
                                }
                                //FB 2622 Ends
                                if (showDeletedConf.Checked)
                                {
                                    if (node.SelectSingleNode("deleted") != null)
                                    {
                                        if (node.SelectSingleNode("deleted").InnerText == "1")
                                            dr["ConferenceType"] = "9";
                                    }
                                }

                                dr["ID"] = dr["ConfID"].ToString();

                                if (node.SelectSingleNode("confTime") != null)
                                    confSTime = node.SelectSingleNode("confTime").InnerText;

                                if (node.SelectSingleNode("setupTime") != null)
                                    setupSTime = node.SelectSingleNode("setupTime").InnerText;

                                if (node.SelectSingleNode("teardownTime") != null)
                                    teardownSTime = node.SelectSingleNode("teardownTime").InnerText;

                                if (node.SelectSingleNode("confDate") != null)
                                {
                                    if (node.SelectSingleNode("confDate").InnerText != "")
                                    {
                                        confSDate = node.SelectSingleNode("confDate").InnerText;
                                    }
                                }
                                // FB 2002 starts
                                if (node.SelectSingleNode("uniqueID") != null)
                                {
                                    if (node.SelectSingleNode("uniqueID").InnerText != "")
                                    {
                                        uniqueID = node.SelectSingleNode("uniqueID").InnerText;
                                    }
                                }
                                // FB 2002 ends

                                int adddays = 0;
                                if (confSTime.Trim() == "00:00 AM")
                                    adddays = 1;

                                DateTime start = DateTime.Parse(confSDate + " " + confSTime);
                                DateTime stUp = DateTime.Parse(confSDate + " " + setupSTime);
                                DateTime end = start.AddMinutes(Convert.ToDouble(dr["durationMin"]));
                                DateTime trDn = DateTime.Parse(end.ToString("MM/dd/yyyy") + " " + teardownSTime);

                                start = start.AddDays(adddays);
                                stUp = stUp.AddDays(adddays);
                                end = end.AddDays(adddays);
                                trDn = trDn.AddDays(adddays);



                                dr["start"] = start;
                                dr["end"] = end;

                                dr["formatstart"] = myVRMNet.NETFunctions.GetFormattedDate(start.ToString("MM/dd/yyyy")) + " " + start.ToString(tFormats);
                                dr["formatend"] = myVRMNet.NETFunctions.GetFormattedDate(end.ToString("MM/dd/yyyy")) + " " + end.ToString(tFormats); ;

                                subnotes2 = node.SelectNodes("mainLocation/location");

                                if (subnotes2 != null)
                                {
                                    for (int subnodescnt = 0; subnodescnt < subnotes2.Count; subnodescnt++)
                                    {
                                        subnode2 = subnotes2[subnodescnt];
                                        locStr = locStr + subnode2.SelectSingleNode("locationName").InnerText;
                                        locStr = locStr + "<br>";
                                    }
                                }


                                trdnTxt = "";

                                if (Session["EnableEntity"] == null)//Organization Module Fixes
                                {
                                    Session["EnableEntity"] = "0";//Organization Module Fixes
                                }
                                //FB 2013 start
                                string customText = "";    
                               if (Session["EnableEntity"].ToString() != "0")//Organization Module Fixes
                                {
                                    XmlNodeList customnodes = node.SelectNodes("CustomAttributesList/CustomAttribute");
                                    for (int custnodescnt = 0; custnodescnt < customnodes.Count; custnodescnt++)
                                    {
                                        XmlNode customnode = customnodes[custnodescnt];
                                        string attriName = "", attriValue = "";

                                        if (customnode.SelectSingleNode("IncludeInCalendar") != null)
                                            if (customnode.SelectSingleNode("IncludeInCalendar").InnerText.Trim().Equals("0"))
                                                continue;
                                        
                                        if (customnode.SelectSingleNode("Status") != null)
                                        {
                                            if (customnode.SelectSingleNode("Status").InnerText == "0")
                                            {
                                                //customText = "<b>Custom Attrributes</b>";
                                                if (customnode.SelectSingleNode("Title") != null)
                                                    attriName = customnode.SelectSingleNode("Title").InnerText;

                                                if (customnode.SelectSingleNode("Type") != null)
                                                {
                                                    if (customnode.SelectSingleNode("Type").InnerText == "3")
                                                    {
                                                        if (customnode.SelectSingleNode("SelectedValue") != null)
                                                        {
                                                            if (customnode.SelectSingleNode("SelectedValue").InnerText == "1")
                                                                attriValue = "Yes";
                                                            else
                                                                attriValue = "No";
                                                        }
                                                    }
                                                    else if (customnode.SelectSingleNode("Type").InnerText == "2")//FB 2377
                                                    {
                                                        if (customnode.SelectSingleNode("SelectedValue") != null)
                                                        {
                                                            if (customnode.SelectSingleNode("SelectedValue").InnerText == "1")
                                                                attriValue = "Yes";
                                                            else
                                                                attriValue = "No";
                                                        }
                                                    }
                                                    else
                                                    {

                                                        if (customnode.SelectSingleNode("SelectedValue") != null)
                                                            attriValue = customnode.SelectSingleNode("SelectedValue").InnerText;
                                                    }
                                                }

                                                XmlNodeList optNodes = customnode.SelectNodes("OptionList/Option");

                                                if (customnode.SelectSingleNode("Type").InnerText.Trim() == "5" || customnode.SelectSingleNode("Type").InnerText.Trim() == "6") //FB 1718
                                                {
                                                    if (optNodes != null)
                                                    {
                                                        for (int optnodescnt = 0; optnodescnt < optNodes.Count; optnodescnt++)
                                                        {
                                                            XmlNode optNode = optNodes[optnodescnt];
                                                            if (optNode.SelectSingleNode("Selected") != null)
                                                            {
                                                                if (optNode.SelectSingleNode("Selected").InnerText == "1")
                                                                {
                                                                    if (optNode.SelectSingleNode("DisplayCaption") != null)
                                                                    {
                                                                        if (attriValue == "")
                                                                            attriValue = optNode.SelectSingleNode("DisplayCaption").InnerText;
                                                                        else
                                                                            attriValue += "," + optNode.SelectSingleNode("DisplayCaption").InnerText;
                                                                    }

                                                                }


                                                            }
                                                        }
                                                    }
                                                }

                                            }

                                            if (attriValue == "")
                                                attriValue = "N/A";

                                            customText += attriName + " - " + attriValue + "<br>";
                                        }
                                    }

                                }
                               //FB 2013 end
                               //FB 2632 Starts
                               CongAtt = "";
                               CongAttStatus = obj.GetTranslatedText("No");
                               if (node.SelectSingleNode("OnSiteAVSupport") != null)
                               {
                                   if (node.SelectSingleNode("OnSiteAVSupport").InnerText.Equals("1"))
                                       CongAttStatus = obj.GetTranslatedText("Yes");
                               }
                               CongAtt = OnSiteAVSupport + " - " + CongAttStatus;

                               CongAttStatus = obj.GetTranslatedText("No");
                               if (node.SelectSingleNode("MeetandGreet") != null)
                               {
                                   if (node.SelectSingleNode("MeetandGreet").InnerText.Equals("1"))
                                       CongAttStatus = obj.GetTranslatedText("Yes");
                               }
                               CongAtt += "</br>" + MeetandGreet + " - " + CongAttStatus;

                               CongAttStatus = obj.GetTranslatedText("No");
                               if (node.SelectSingleNode("ConciergeMonitoring") != null)
                               {
                                   if (node.SelectSingleNode("ConciergeMonitoring").InnerText.Equals("1"))
                                       CongAttStatus = obj.GetTranslatedText("Yes");
                               }
                               CongAtt += "</br>" + ConciergeMonitoring + " - " + CongAttStatus;

                               CongAttStatus = obj.GetTranslatedText("No");
                               if (node.SelectSingleNode("VNOCOperator") != null)
                               {
                                   if (node.SelectSingleNode("VNOCOperator").InnerText != "")
                                       CongAttStatus = obj.GetTranslatedText("Yes");
                               }
                               CongAtt += "</br>" + DedicatedVNOCOperator + " - " + CongAttStatus;
                                //FB 2632 Ends
                                m = new StringBuilder(); ;

                                m.Append("<table cellspacing='0' cellpadding='0' border='0' width='310px' class='promptbox' bgColor = '#ccccff'>");
                                m.Append("<tr valign='middle'>");
                                m.Append("<td width='100%' height='22' style='text-indent:2;' class='titlebar' align='left' colspan='2' bgColor = '#9999ff'>");
                                m.Append("<img src='image/pen.gif' height='18' width='18'>&nbsp;&nbsp;" + hdr);
                                m.Append("</td>");
                                m.Append("</tr>");
                                m.Append("<tr>");
                                m.Append("<td valign='top'><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + obj.GetTranslatedText("Name") + ": </style></td>");
                                m.Append("<td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + dr["confName"].ToString() + "</style></td>");
                                m.Append("</tr>");
                                m.Append("<tr>");
                                m.Append("<td valign='top'><span style='font-size: 8pt; font-weight: bold; color: #990033'> " + obj.GetTranslatedText("Unique ID") + ": </style></td>"); // FB 2002
                                m.Append("<td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + uniqueID.ToString() + "</style></td>");
                                m.Append("</tr>");
                                m.Append("<tr>");
                                m.Append("<td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + obj.GetTranslatedText("Start - End") + ": </style></td>");
                                m.Append("<td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + stUp.ToString(tFormats) + " - " + trDn.ToString(tFormats) + "</style></td>");
                                m.Append("</tr>");
                                m.Append("<tr>");
                                m.Append("<td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + obj.GetTranslatedText("Duration") + ": </style></td>");
                                m.Append("<td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + hrs.ToString() + " hr(s) " + mins.ToString() + " min(s)</style></td>");
                                m.Append("</tr>");
                                m.Append("<tr>");//FB 2622 Starts
                                m.Append("<td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + Password + ": </style></td>"); 
                                m.Append("<td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + ConfPassword + "</style></td>");
                                m.Append("</tr>");//FB 2622 Ends
                                m.Append("<tr>");
                                m.Append("<td valign='top'><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + obj.GetTranslatedText("Location") + ": </style></td>");
                                m.Append("<td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + ((locStr == "") ? "N/A" : locStr) + "</style></td>");
                                m.Append("</tr>");
                                m.Append("<tr>"); //FB 2632
                                m.Append("<td valign='top'><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + obj.GetTranslatedText("Concierge Support") + ": </style></td>");
                                m.Append("<td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + CongAtt + "</style></td>");
                                m.Append("</tr>");
                                if (Session["EnableEntity"].ToString() != "0")//FB 2013
                                {
                                    m.Append("<tr>");
                                    m.Append("<td valign='top'><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + obj.GetTranslatedText("Custom Options") + ": </style></td>");
                                    m.Append("<td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + ((customText == "") ? "N/A" : customText) + "</style></td>");
                                    m.Append("</tr>");
                                }
                                m.Append("</table>");

                                dr["confDetails"] = spn + dr["confName"].ToString() + " - (UID : " + uniqueID + " )<br>" + hrs.ToString() + " hr(s) " + mins.ToString() + " min(s)<br>" + stUp.ToString(tFormats) + " - " + trDn.ToString(tFormats) + "</span><br>" + spn + setupTxt + trdnTxt + Password + " : " + ConfPassword + "<br>" + obj.GetTranslatedText("Locations") + ":<br>" + locStr + "</span><br>"; // FB 2002 //FB 2622

                                dr["CustomDescription"] = m.ToString();

                                dt.Rows.Add(dr);
                                locStr = "";


                            }
                        }
                    }
                }



                if (addtoSession > 0)
                {
                    if (Session["PersonalCalendar"] != null)
                        Session.Add("PersonalCalendar", dt);
                    else
                        Session["PersonalCalendar"] = dt;
                }
                else
                {
                    if (Session["PersonalWeekly"] != null)
                        Session.Add("PersonalWeekly", dt);
                    else
                        Session["PersonalWeekly"] = dt;
                }


            }
        }
        catch (Exception ex)
        {
            errLabel.Text = ex.StackTrace;
            errLabel.Visible = true;
            log.Trace("ChangeCalendarDate: " + ex.StackTrace + " : " + ex.Message + ":"+ ex.InnerException);
        }

    }

    #endregion

    #region BindDaily

    protected void BindDaily()
    {
        DataTable dt = null;

        try
        {
            SetOfficeHours(GetCalendarOutXml());// FB 2057

            

            if (dt == null)
            {
                if (Session["PersonalCalendar"] != null)
                {
                    dt = (DataTable)Session["PersonalCalendar"];
                }
            }

            if (dt != null)
            {
                schDaypilot.StartDate = conf;
                schDaypilot.DataSource = dt;
                schDaypilot.DataBind();
                if (officehrDaily.Checked)
                    schDaypilot.HeightSpec = DayPilot.Web.Ui.Enums.HeightSpecEnum.BusinessHoursNoScroll;
                else
                    schDaypilot.HeightSpec = DayPilot.Web.Ui.Enums.HeightSpecEnum.BusinessHours;
                schDaypilot.Visible = true;

                schDaypilotMonth.StartDate = conf;
                schDaypilotMonth.DataSource = dt;
                schDaypilotMonth.DataBind();
                if (officehrMonth.Checked)
                    schDaypilotMonth.ShowWeekend = false;
                else
                    schDaypilotMonth.ShowWeekend = true; ;
                schDaypilotMonth.Visible = true;
               
            }
        }
        catch (Exception ex)
        {
            errLabel.Text = ex.StackTrace;
            errLabel.Visible = true;
            log.Trace("ChangeCalendarDate: " + ex.StackTrace + " : " + ex.Message);
        }
    }

    #endregion

    #region BindWeekly

    protected void BindWeekly()
    {
        DataTable dt = null; ; 

        try
        {
            SetOfficeHours(GetWeeklyCalendar());// FB 2057

            if (dt == null)
            {

                if (Session["PersonalWeekly"] != null)
                {
                    dt = (DataTable)Session["PersonalWeekly"];
                }
                else if(Session["PersonalCalendar"] != null)
                {
                    dt = (DataTable)Session["PersonalCalendar"];
                }
            }

            if (dt != null)
            {
                schDaypilotweek.StartDate = DayPilot.Utils.Week.FirstWorkingDayOfWeek(conf);
                if (officehrWeek.Checked)
                {
                    schDaypilotweek.Days = 5;
                    schDaypilotweek.HeightSpec = DayPilot.Web.Ui.Enums.HeightSpecEnum.BusinessHoursNoScroll;
                }
                else
                {
                    schDaypilotweek.Days = 7;
                    schDaypilotweek.HeightSpec = DayPilot.Web.Ui.Enums.HeightSpecEnum.BusinessHours;
                }
                schDaypilotweek.DataSource = dt;
                schDaypilotweek.DataBind();
                schDaypilotweek.Visible = true;
               
            }
        }
        catch (Exception ex)
        {
            errLabel.Text = ex.StackTrace;
            errLabel.Visible = true;
            log.Trace("ChangeCalendarDate: " + ex.StackTrace + " : " + ex.Message);
        }
    }

    #endregion
   
    #region Page_UnLoad
    /*
    protected void Page_UnLoad(object sender, EventArgs e)
    {
        try
        {
            
        }
        catch (Exception ex)
        {
            errLabel.Text = ex.StackTrace;
            errLabel.Visible = true;
        }

    }
     */
    #endregion

    #region LoadXmlsAsync

    public void LoadXmlsAsync(Object sender, EventArgs e)
    {
        try
        {
            GetMonthlyThread();
        }
        catch (Exception ex)
        {
            errLabel.Text = ex.StackTrace;
            errLabel.Visible = true;
        }

    }

    #endregion 

    #region Get Day Colour
    /// <summary>
    /// FB 1860
    /// </summary>
    private void GetdayColour()
    {
        try
        {
            if (Session["Holidays"] != null)
            {
                if (Session["Holidays"].ToString().Trim() != "")
                {
                    cellColor = "";


                    XElement root = XElement.Parse(Session["Holidays"].ToString());
                    IEnumerable<XElement> hldys =
                        from hldyelmnts in root.Elements("Holiday")
                        where (string)hldyelmnts.Element("Date") == dtCell.ToString("MM/dd/yyyy") //FB 2052
                        select hldyelmnts;

                    foreach (XElement elmnts in hldys)
                    {
                        cellColor = (string)elmnts.Element("Color");
                        break;
                    }
                }
            }

        }
        catch (Exception ex)
        {

            errLabel.Text = ex.StackTrace;
            log.Trace(ex.StackTrace);
        }
    }

    #endregion

    // FB 2057

    private void SetOfficeHours(string xmls)
    {
        string hdr = obj.GetTranslatedText("Conference Details"), startHour = "00", startMin = "00", startSet = "AM", endHour = "23", endMin = "59", endSet = "PM", open24 = "";
        
        XmlDocument xmldoc = null;
        DataTable dt = null;
        try
        {
            xmldoc = new XmlDocument();
            xmldoc.LoadXml(xmls);

            dt = GetDataTable();

            if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/open24") != null)
                open24 = xmldoc.DocumentElement.SelectSingleNode("systemAvail/open24").InnerText;

            if (open24 == "1")
            {
                officehrDaily.Checked = false;
                officehrDaily.Enabled = false;

                schDaypilot.BusinessBeginsHour = 0;
                schDaypilot.BusinessEndsHour = 24;

                schDaypilotweek.BusinessBeginsHour = 0;
                schDaypilotweek.BusinessEndsHour = 24;
            }
            else
            {

                if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/startTime/startHour") != null)
                    startHour = xmldoc.DocumentElement.SelectSingleNode("systemAvail/startTime/startHour").InnerText;

                if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/startTime/startMin") != null)
                    startMin = xmldoc.DocumentElement.SelectSingleNode("systemAvail/startTime/startMin").InnerText;

                if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/startTime/startSet") != null)
                    startSet = xmldoc.DocumentElement.SelectSingleNode("systemAvail/startTime/startSet").InnerText;

                if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/endTime/endHour") != null)
                    endHour = xmldoc.DocumentElement.SelectSingleNode("systemAvail/endTime/endHour").InnerText;

                if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/endTime/endMin") != null)
                    endMin = xmldoc.DocumentElement.SelectSingleNode("systemAvail/endTime/endMin").InnerText;

                if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/endTime/endSet") != null)
                    endSet = xmldoc.DocumentElement.SelectSingleNode("systemAvail/endTime/endSet").InnerText;

                schDaypilot.BusinessBeginsHour = Convert.ToInt32(DateTime.Parse("06/06/2006 " + startHour + ":" + startMin + " " + startSet).ToString("HH"));
                schDaypilot.BusinessEndsHour = Convert.ToInt32(DateTime.Parse("06/06/2006 " + endHour + ":" + endMin + " " + endSet).ToString("HH"));

                if (endMin != "00")
                    schDaypilot.BusinessEndsHour = schDaypilot.BusinessEndsHour + 1;

                schDaypilotweek.BusinessBeginsHour = Convert.ToInt32(DateTime.Parse("06/06/2006 " + startHour + ":" + startMin + " " + startSet).ToString("HH"));
                schDaypilotweek.BusinessEndsHour = Convert.ToInt32(DateTime.Parse("06/06/2006 " + endHour + ":" + endMin + " " + endSet).ToString("HH"));

                if (endMin != "00")
                    schDaypilotweek.BusinessEndsHour = schDaypilotweek.BusinessEndsHour + 1;
            }


        }
        catch (Exception ex)
        {

            errLabel.Text = ex.StackTrace;
            errLabel.Visible = true;
            log.Trace("ChangeCalendarDate: " + ex.StackTrace + " : " + ex.Message);
        }
    }


}


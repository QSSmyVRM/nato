using System;
using System.Collections.Generic;
using System.Text;

namespace ns_MyVRMNet
{
    public class vrmConfStatus
    {
        public const String Scheduled = "0";
        public const String Pending = "1";
        public const String Terminated = "3";
        public const String Ongoing = "5";
        public const String OnMCU = "6";
        public const String Completed = "7";
        public const String Deleted = "9";
    }
    public class vrmConfType
    {
        public const String Video = "1";
        public const String AudioVideo = "2";
        public const String Immediate = "3";
        public const String P2P = "4"; // Point to point
        public const String Template = "5";
        public const String AudioOnly = "6";
        public const String RoomOnly = "7";
        public const String Phantom = "11";
    }
    public class vrmConfUserType
    {
        public const String External = "1";
        public const String Room = "2";
        public const String CC = "3";
    }
    public class vrmConfUserStatus
    {
        public const String Undecided = "0";
        public const String Accepted = "1";
        public const String Rejectetd = "2";
        public const String Reschedule = "3";
    }
    public class vrmConfEndpointType
    {
        public const String User = "U";
        public const String Room = "R";
        public const String Cascade = "4";
        public const String Guest = "U";
    }
    public class vrmWorkorderType
    {
        public const String AV = "1";
        public const String CAT = "2";
        public const String HK = "3";
    }
    public class vrmConnectionTypes
    {
        public const String DialOutOld = "0";
        public const String DialIn = "1";
        public const String DialOut = "2";
        public const String Direct = "3";
    }
    public class vrmAddressType
    {
        public const String IP = "1";
        public const String H323 = "2";
        public const String E164 = "3";
        public const String ISDN = "4";
        public const String MPI = "5";
    }
    public class vrmVideoProtocol
    {
        public const String IP = "1";
        public const String ISDN = "2";
        public const String SPI = "3";
        public const String MPI = "4";
    }
    public class vrmMCUInterfaceType
    {
        public const String Polycom = "1";
        public const String Codian = "3";
        public const String Tandberg = "4";
    }
    public class vrmClient
    {
        public const String Wustl = "WASHU";
        public const String NGC = "NGC";
        public const String PennState = "PSU";
        public const String LHRIC = "LHRIC";
        public const String MOJ = "MOJ";//Added Client MOJ
        public const String Mayo = "MAYO";//Added Client mayo 1911
    }
    //Code Added For FB 1371 and FB 1367
    public class vrmEndPointConnectionSatus
    {
        public const String disconnect = "0";
        public const String Connecting = "1";//Edited for Blue Status Project
        public const String Connected = "2"; //Edited for Blue Status Project
        public const String Online = "3";//Blue Status Project
        //As per Blue Status Project Constants ; 0=disconnect,1=connecting,2=connected,3=online,-1=Unreachable
    }
    public class vrmImageFormattype  // Image Project
    {
        public const String jpg = "1";
        public const String jpeg = "2";
        public const String gif = "3";
        public const String bmp = "4";
        public const String png = "5";
    }
    public class vrmAttributeType  // Image Project
    {
        public const String room = "1";
        public const String roommap1 = "2";
        public const String roommap2 = "3";
        public const String roomsec1 = "4";
        public const String roomsec2 = "5";
        public const String roommisc1 = "6";
        public const String roommisc2 = "7";
        public const String av = "8";
        public const String catering = "9";
        public const String hk = "10";
        public const String banner = "11";  
        public const String highresbanner = "12"; //Organization/CSS Module
        public const String companylogo = "13";
        public const String emaillogo = "14";
        public const String sitelogo = "15";
        public const String MirrorCssXml = "16";
        public const String ArtifactsXml = "17";
        public const String TextChangeXml = "18";
        public const String DefaultCssXML = "19";
        public const String sitebanner = "20";
        public const String sitehighresbanner = "21";
        public const String footerimage = "22"; // FB 1710
      }

    //FB 1830
    public class vrmCurrencyFormat
    {
        public const String dollar = "$";
        public const String bound = "€";
    }

    public class vrmNumberFormat
    {
        public const String american = "en-US";
        public const String european = "fr-FR";
    }

    public class vrmSpecialChar
    {
        public const String LessThan = "õ"; //Alt 0245
        public const String GreaterThan = "æ"; //Alt 145
        public const String ampersand = "σ"; //Alt 229
    }
}

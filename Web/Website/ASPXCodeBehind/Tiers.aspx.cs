using System;
using System.Data;
using System.Xml;
using System.Web.UI.WebControls;

/// <summary>
/// Summary description for ConferenceOrders
/// </summary>
/// 
namespace ns_Tiers
{
    public partial class Tiers : System.Web.UI.Page
    {
        myVRMNet.NETFunctions obj;
        
        protected System.Web.UI.WebControls.Label lblHeader;
        protected System.Web.UI.WebControls.Label errLabel;
        protected System.Web.UI.WebControls.TextBox txtNewTier1Name;
        protected System.Web.UI.WebControls.Table tblNoTier1s;
        protected System.Web.UI.WebControls.DataGrid dgTier1s;
        ns_Logger.Logger log;
        public Tiers()
        {
            obj = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger();
            //
            // TODO: Add constructor logic here
            //
        }
        #region Methods Executed on Page Load

        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                //reqDepartmentName.Enabled = true;
                //                DateTime startTime = DateTime.Now;
                //                Response.Write(("<br>Start time:" + startTime.ToString()));
                lblHeader.Text = obj.GetTranslatedText("Manage Tiers");//FB 1830 - Translation
                errLabel.Text = "";
                txtNewTier1Name.Focus();//FB 2094
                //tblTier2.Visible = false;
                if (Request.QueryString["m"] != null)
                    if (Request.QueryString["m"].ToString().Equals("1"))
                    {
                        errLabel.Text = obj.GetTranslatedText("Operation Successful!");//FB 1830 - Translation
                        errLabel.Visible = true;
                    }
                if (!IsPostBack)
                    BindData();
            }
            catch (Exception ex)
            {
                //                Response.Write(ex.Message);
                errLabel.Visible = true;
                errLabel.Text = "PageLoad: " + ex.StackTrace;
            }

        }

        private void BindData()
        {
            try
            {
                String inXML = "";
                inXML += "<GetLocations>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "  <UserID>" + Session["userID"].ToString() + "</UserID>";
                inXML += "</GetLocations>";
                log.Trace("getLocations InXML: " + inXML);
                String outXML = obj.CallMyVRMServer("GetLocations", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                log.Trace("getLocations OutXML: " + outXML);
                if (outXML.IndexOf("<error>") < 0)
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);
                    XmlNodeList nodes = xmldoc.SelectNodes("//GetLocations/Location");
                    LoadTier1Grid(nodes);
                    Label lblTemp = new Label();
                    DataGridItem dgFooter = (DataGridItem)dgTier1s.Controls[0].Controls[dgTier1s.Controls[0].Controls.Count - 1];
                    lblTemp = (Label)dgFooter.FindControl("lblTotalRecords");
                    lblTemp.Text = nodes.Count.ToString();
                }
                else
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible=true;
                }
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                errLabel.Text = "BindData: " + ex.StackTrace;
            }
        }

        protected void LoadTier1Grid(XmlNodeList nodes)
        {
            try
            {
                XmlTextReader xtr;
                DataSet ds = new DataSet();

                foreach (XmlNode node in nodes)
                {
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.InferSchema);
                }
                DataView dv;
                DataTable dt;

                if (ds.Tables.Count > 0)
                {
                    dv = new DataView(ds.Tables[0]);
                    dt = dv.Table;
                    Session.Add("Tier1DS", dt);
                }
                else
                {
                    //tblNoTier1s.Visible = true;
                    dv = new DataView();
                    dt = new DataTable();
                    dt.Columns.Add("tier1ID");
                    dt.Columns.Add("tier1Name");
                    DataRow dr = dt.NewRow();
                    dr["tier1ID"] = "";
                    dr["tier1Name"] = "No Tiers found.";
                }
                    dgTier1s.DataSource = dt;
                    dgTier1s.DataBind();
            }
            catch (Exception ex)
            {
                errLabel.Text = ex.StackTrace;
                errLabel.Visible = true;
            }
        }

        #endregion
        protected void DeleteTier1(object sender, DataGridCommandEventArgs e)
        {
            try
            {
                String inXML = ""; // <login><userID>11</userID><edit/><delete><departmentID>1</departmentID></delete></login>
                inXML += "<DeleteTier1>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "  <UserID>" + Session["userID"].ToString() + "</UserID>";
                inXML += "  <ID>" + e.Item.Cells[0].Text + "</ID>";
                inXML += "</DeleteTier1>";
                //Response.Write(obj.Transfer(inXML));
                log.Trace("DeleteTier1 Inxml: " + inXML);
                String outXML = obj.CallMyVRMServer("DeleteTier1", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                log.Trace("DeleteTier1 Outxml: " + outXML);
                if (outXML.IndexOf("<error>") < 0)
                {
                    Response.Redirect("ManageTiers.aspx?m=1");
                }
                else
                {
                    errLabel.Visible = true;
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                }
            }
            catch (Exception ex)
            {
                errLabel.Text = ex.StackTrace;
                errLabel.Visible = true;
            }
        }
        protected void EditTier1(object sender, DataGridCommandEventArgs e)
        {
            try
            {
                //reqDepartmentName.Enabled = false;
                DataTable dtTemp = (DataTable)Session["Tier1DS"];
                dgTier1s.EditItemIndex = e.Item.ItemIndex;
                Session.Add("Tier1ID", e.Item.Cells[0].Text);
                //Response.Write(dtTemp.Rows.Count);
                dgTier1s.DataSource = dtTemp;
                dgTier1s.DataBind();
                foreach (DataGridItem dgi in dgTier1s.Items)
                    if (dgi.ItemIndex.Equals(e.Item.ItemIndex))
                    {
                        LinkButton btnTemp = (LinkButton)dgi.FindControl("btnUpdate");
                        btnTemp.Visible = true;
                        btnTemp = (LinkButton)dgi.FindControl("btnDelete");
                        btnTemp.Visible = false;
                        btnTemp = (LinkButton)dgi.FindControl("btnCancel");
                        btnTemp.Visible = true;
                        btnTemp = (LinkButton)dgi.FindControl("btnEdit");
                        btnTemp.Visible = false;
                    }//Response.Write(":in edit");
                Label lblTemp = new Label();
                DataGridItem dgFooter = (DataGridItem)dgTier1s.Controls[0].Controls[dgTier1s.Controls[0].Controls.Count - 1];
                lblTemp = (Label)dgFooter.FindControl("lblTotalRecords");
                lblTemp.Text = dgTier1s.Items.Count.ToString();
            }
            catch (Exception ex)
            {
                errLabel.Text = ex.StackTrace;
                errLabel.Visible = true;
            }
        }
        protected void CancelTier1(object sender, DataGridCommandEventArgs e)
        {
            try
            {
                Response.Redirect("ManageTiers.aspx");
            }
            catch (Exception ex)
            {
                errLabel.Text = ex.StackTrace;
                errLabel.Visible = true;
            }
        }
        protected void UpdateTier1(object sender, DataGridCommandEventArgs e)
        {
            try
            {
                TextBox tbTemp = (TextBox)e.Item.FindControl("txtTier1Name");
                String outXML = UpdateManageTier1(tbTemp);
                if (outXML.IndexOf("<error>") < 0)
                {
                    Response.Redirect("ManageTiers.aspx?m=1");
                }
                else
                {
                    errLabel.Visible = true;
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                }
            }
            catch (Exception ex)
            {
                errLabel.Text = ex.StackTrace;
                errLabel.Visible = true;
            }
        }
        protected String UpdateManageTier1(TextBox tbTemp)
        {
            try
            {
                String inXML = ""; // <login><userID>11</userID><edit/><delete><departmentID>1</departmentID></delete></login>
                inXML += "<SetTier1>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "  <UserID>" + Session["userID"].ToString() + "</UserID>";
                inXML += "  <ID>" + Session["Tier1ID"].ToString() + "</ID>";
                inXML += "  <Name>" + tbTemp.Text + "</Name>";
                inXML += "</SetTier1>";
                log.Trace("SetTier1 Inxml: " + inXML);
                String outXML = obj.CallMyVRMServer("SetTier1", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                log.Trace("SetTier1 Outxml: " + outXML);
                return outXML;
            }
            catch (Exception ex)
            {
                //return "Error in Setting Tier1";
                log.Trace("UpdateManageTier1 :Error in Setting Tier1" + ex.Message);
                return obj.ShowSystemMessage();//FB 1881
            }
        }

        protected void BindRowsDeleteMessage(Object sender, DataGridItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType.Equals(ListItemType.Item) || e.Item.ItemType.Equals(ListItemType.AlternatingItem))
                {
                    LinkButton btnTemp = (LinkButton)e.Item.FindControl("btnDelete");
                    btnTemp.Attributes.Add("onclick", "return confirm('" + obj.GetTranslatedText("Are you sure you want to delete this tier ?") + "')"); //FB japnese
                }
            }
            catch (Exception ex)
            {
                errLabel.Text = ex.StackTrace;
                errLabel.Visible = true;
            }
        }
        protected void CreateNewTier1(Object sender, EventArgs e)
        {
            try
            {
                //reqDepartmentName.Enabled = true;
                Session.Add("Tier1ID", "new");
                String outXML = UpdateManageTier1(txtNewTier1Name);
                if (outXML.IndexOf("<error>") < 0)
                {
                    Response.Redirect("ManageTiers.aspx?m=1");
                }
                else
                {
                    errLabel.Visible = true;
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                }
            }
            catch (Exception ex)
            {
                errLabel.Text = ex.StackTrace;
                errLabel.Visible = true;
            }
        }

        protected void ManageTier2(Object sender, CommandEventArgs e)
        {
            //Response.Write(e.CommandArgument);
            Session.Add("Tier1ID", e.CommandArgument.ToString().Split('^')[0]);
            Session.Add("Tier1Name", e.CommandArgument.ToString().Split('^')[1]);
            Response.Redirect("ManageTier2.aspx");
        }
    }
}

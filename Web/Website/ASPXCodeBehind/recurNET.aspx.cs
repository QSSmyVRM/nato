using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.Drawing;
using System.Collections.Generic;


public partial class en_recurNET : System.Web.UI.Page
{

    #region Protected Data Members
    
    protected System.Web.UI.WebControls.TextBox RecurDurationhr;
    protected System.Web.UI.WebControls.TextBox RecurDurationmi;
    protected System.Web.UI.WebControls.TextBox EndText;
    protected System.Web.UI.WebControls.TextBox DurText;
    protected System.Web.UI.WebControls.TextBox StartDate;
    protected System.Web.UI.WebControls.RadioButton EndType;
    protected System.Web.UI.WebControls.RadioButton REndAfter;
    protected System.Web.UI.WebControls.TextBox Occurrence;
    protected System.Web.UI.WebControls.RadioButton REndBy;
    protected System.Web.UI.WebControls.TextBox EndDate;
    protected System.Web.UI.WebControls.Button Cancel;
    protected System.Web.UI.WebControls.Button Reset;
    protected System.Web.UI.WebControls.Button RecurSubmit;
    protected System.Web.UI.WebControls.Button RecurSubmit1;
    protected System.Web.UI.HtmlControls.HtmlTableRow RangeRow;
    protected MetaBuilders.WebControls.ComboBox confStartTime;
    protected MetaBuilders.WebControls.ComboBox SetupTime;
    protected MetaBuilders.WebControls.ComboBox TeardownTime;
    protected System.Web.UI.WebControls.RequiredFieldValidator reqStartTime;
    protected System.Web.UI.WebControls.RequiredFieldValidator reqStartDate;
    protected System.Web.UI.WebControls.RegularExpressionValidator regStartTime;
    protected System.Web.UI.WebControls.RegularExpressionValidator regEndTime;
    protected System.Web.UI.WebControls.RegularExpressionValidator regTime;
    protected System.Web.UI.WebControls.RegularExpressionValidator regTearDownStartTime;
    protected System.Web.UI.WebControls.RegularExpressionValidator regSetupStartTime;
    protected System.Web.UI.WebControls.RadioButtonList rdlstDayColor; //FB 2052
    protected System.Web.UI.WebControls.Label lblNoneDayColor;//FB 2052
    #endregion

    #region Private Data Members 

    protected Int32 CustomSelectedLimit = 100;
    protected String format = "MM/dd/yyyy";
    protected String timeZone = "0";
    protected String tformat = "hh:mm tt";
    myVRMNet.NETFunctions obj = null;
    ns_Logger.Logger log;
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            String outXML = "", inXML = "";//FB 2052
            Int32 CustomSelectedLimit = 100;
            Int32 timeFmrt = 12; ;

            Session["timeFormat"] = ((Session["timeFormat"] == null) ? "1" : Session["timeFormat"]);
            Session["FormatDateType"] = ((Session["FormatDateType"] == null) ? "1" : Session["FormatDateType"]);
            Session["timeZoneDisplay"] = ((Session["timeZoneDisplay"] == null) ? "1" : Session["timeZoneDisplay"]);
            Application["interval"] = ((Application["interval"] == null) ? "60" : Application["interval"]);
            format = Session["FormatDateType"].ToString();
            tformat = (Session["timeFormat"].ToString().Equals("0") ? "HH:mm" : "hh:mm tt");
            timeZone = Session["timeZoneDisplay"].ToString();

            if (!IsPostBack)
            {
                obj = new myVRMNet.NETFunctions();
                log = new ns_Logger.Logger();

                String tmpInterval = "";
                if (Application["interval"] != null)
                    if (Application["interval"].ToString() != "")
                        tmpInterval = Application["interval"].ToString();
                Application["interval"] = "1";
                Application["interval"] = tmpInterval;

                confStartTime.Items.Clear();
                SetupTime.Items.Clear();
                TeardownTime.Items.Clear();
                obj.BindTimeToListBox(SetupTime, true, true);
                obj.BindTimeToListBox(TeardownTime, true, true);
                obj.BindTimeToListBox(confStartTime, true, true);
                if (Session["timeFormat"] != null)
                    if (Session["timeFormat"].ToString().Equals("0"))
                    {
                        regStartTime.ValidationExpression = "[0-2][0-9]:[0-5][0-9]";
                        regStartTime.ErrorMessage = obj.GetTranslatedText("Invalid Time (HH:mm)");
                        regSetupStartTime.ValidationExpression = "[0-2][0-9]:[0-5][0-9]";
                        regSetupStartTime.ErrorMessage = obj.GetTranslatedText("Invalid Time (HH:mm)");
                        regTearDownStartTime.ValidationExpression = "[0-2][0-9]:[0-5][0-9]";
                        regTearDownStartTime.ErrorMessage = obj.GetTranslatedText("Invalid Time (HH:mm)");
                    }

                //FB 2052 Start
                inXML = "<login>" 
                      + "  <userID>" + Session["userID"].ToString() + "</userID>"
                      + obj.OrgXMLElement()
                      + "</login>";

                outXML = obj.CallMyVRMServer("GetHolidayType", inXML, Application["MyVRMServer_ConfigPath"].ToString());

                lblNoneDayColor.Visible = true;
                if (outXML.IndexOf("<error>") < 0)
                {

                    XmlDocument xd = new XmlDocument();
                    xd.LoadXml(outXML);
                    XmlNodeList nodes = xd.SelectNodes("//HolidayTypes/HolidayType");

                    for (int i = 0; i < nodes.Count; i++)
                    {
                        if (nodes[i].SelectSingleNode("id") != null)
                            if (nodes[i].SelectSingleNode("id").InnerText.Trim() != "")
                            {
                                if (nodes[i].SelectSingleNode("HolidayDescription") != null)
                                    if (nodes[i].SelectSingleNode("HolidayDescription").InnerText.Trim() != "")
                                    {
                                        rdlstDayColor.Items.Add(nodes[i].SelectSingleNode("HolidayDescription").InnerText.Trim());

                                        rdlstDayColor.Items[i].Value = nodes[i].SelectSingleNode("id").InnerText.Trim();
                                    }
                            }

                    }

                    if (rdlstDayColor.Items.Count > 0)
                    {
                        lblNoneDayColor.Visible = false;
                        rdlstDayColor.SelectedIndex = 0;
                    }
                }
                //FB 2052 End
            }
        }
        catch (Exception ex)
        {
            if(log == null)
                log = new ns_Logger.Logger();

            log.Trace("Page_Load" + ex.StackTrace);
        }
    }
}

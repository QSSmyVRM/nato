var mmm_num, mma_num, mms_num=new Array(14);
var mmm_int, mma_int, mms_int=new Array(14);
var version;
var copyrightsDur; //FB 1648
mm_str = document.frmMenu.menumask.value;

al_int = document.frmMenu.adminlevel.value;
if_str = document.frmMenu.userinterface.value;
rf_int = parseInt(document.frmMenu.roomfood.value, 10);
p2p_int = parseInt(document.frmMenu.p2p.value, 10);
sso_int = ((document.frmMenu.ssoMode.value).toLowerCase() == "yes");

mm_ary = mm_str.split("-");

if (mm_ary.length!=3) {
	alert ("Menu mask is not correct.\n\n- It may be caused by the page idle too long. And for security reason, your session is time out.\n  Please try to login again.\n- If it still happens, please contact us. Thank you.\n")
	window.location.href = '<%=Application("loginPage")%>';
}
mmm_str = mm_ary[0];	mms_str = mm_ary[1];	mma_str = mm_ary[2];

mmm_ary = mmm_str.split("*");
if (mmm_ary.length!=2) {
	alert ("Menumask is not correct. Please check the database or developer.")
}
mmm_num = parseInt(mmm_ary[0], 10);
mmm_int = parseInt(mmm_ary[1], 10);


mms_ary = mms_str.split("+");
for (i=0; i<mms_ary.length; i++) {
	mms_ary[i] = mms_ary[i].split("*");
	if (mms_ary[i].length!=2) {
		alert ("Menumask is not correct. Please check the database or developer.")
	}
	
	mms_num[(i+1)] = parseInt(mms_ary[i][0], 10);	
	mms_int[(i+1)] = parseInt(mms_ary[i][1], 10);
}


mma_ary = mma_str.split("*");
if (mma_ary.length!=2) {
	alert ("Menumask is not correct. Please check the database or developer.")
}
mma_num = parseInt(mma_ary[0], 10);	
mma_int = parseInt(mma_ary[1], 10);



_d = document;


function OpenIntelligentHelp(tp)
{
    var url = "help/WebHelp/mVRM_User_Guide.htm#" + tp; //"help/Washu_Help/mVRM_User_Guide.htm#Basic_Details";

	var w = 700, h = 350; // default sizes
	if (window.screen) {
		w = window.screen.availWidth * 70 / 100;
		h = window.screen.availHeight * 60 / 100;
	}
	window.open(url,'vrmhelp','resizable=yes,scrollbars=yes,toolbar=yes, width='+w+',height='+h);
}

function openhelp()
{
    var url = "help/WebHelp/myVRM_User_and_Administrator_Help.htm"; //fogbugz case 186
    if (document.getElementById("txtClient").value.toUpperCase() == "WASHU")
        url = "help/WUSTLHelp/myVRM_User_and_Administrator_Help.htm";
        
	if (window.screen) {
		w = window.screen.availWidth * 70 / 100;
		h = window.screen.availHeight * 60 / 100;
	}
	window.open(url,'vrmhelp','resizable=yes,scrollbars=yes,toolbar=yes, width='+w+',height='+h);
/*
    	if (document.getElementById("helpPage"))
	        RH_ShowHelp(0, "help/WebHelp/mVRM_User_Guide.htm>WithNavPane", HH_HELP_CONTEXT, document.getElementById("helpPage").value);
        else
	        RH_ShowHelp(0, "help/WebHelp/mVRM_User_Guide.htm>WithNavPane", HH_HELP_CONTEXT, "43");

	
	if (document.getElementById("helpPage"))
	    url = url + "#" + document.getElementById("helpPage").value;
//	var url = "help/WebHelp/Events_Manager.htm";
//	var url = "help/WebHelp/myvrm-documentation_ver2.0.htm#Creating_a_Future_Video_Conference.htm";

	var w = 700, h = 350; // default sizes
	if (window.screen) {
		w = window.screen.availWidth * 70 / 100;
		h = window.screen.availHeight * 60 / 100;
	}
	window.open(url,'vrmhelp','resizable=yes,scrollbars=yes,toolbar=yes, width='+w+',height='+h);
	*/
}


var imchktrlot = (imtime1 + 5) * 1000;
var imTimer1 = 0;

function IMerrchk (teststr)
{
	if ( (teststr.indexOf("</body>") != -1) || (teststr.indexOf("error") != -1) || (teststr == "stop") ) {
		if (teststr != "stop")
			alert(EN_207);
			
		if (document.getElementById("imuserprompt"))
			document.getElementsByTagName("body")[0].removeChild(document.getElementById("imuserprompt"));
		initlisten (0);
		return true;
	} else {
		return false;
	}
}


function listenchg()
{
	if (document.getElementById("imuserprompt"))
		document.getElementsByTagName("body")[0].removeChild(document.getElementById("imuserprompt"));

	cb = document.getElementById("IMlistenIMG")

	if ((cb.src).indexOf("unsim.gif") != -1)
		initlisten (1);
	else
		initlisten (0);
}


function initlisten (ison)
{
	cb1 = document.getElementById("IMlistenIMG");
	cb2 = document.getElementById("IMdropdownIMG");
	
	if (parseInt(ison)) {
		cb1.src = "image/sim.gif";
		cb1.title = "listener is on, click to turn off"
		cb2.src = "image/downarrow.gif";
		ifrmListen.window.location.href = "ifrmlisten.asp?m=1&y=1";

		chkIMTalker();
	} else {
		cb1.src = "image/unsim.gif";
		cb1.title = "listener is off, click to turn on"
		cb2.src = "image/transparent.gif";
		ifrmListen.window.location.href = "ifrmlisten.asp?m=1&y=0";
		
		if (imTimer1) {
			clearTimeout(imTimer1);
			imTimer1 = 0;
		}

		shutdowntalker();
	}
}


function IMuserClick()
{
	if ((document.getElementById("IMdropdownIMG").src).indexOf("downarrow.gif") != -1)
		ajaxGetXML (0, "inc/imuserlistinc.asp", "showIMUser");
}


function showIMUser(outXML)
{
	if (IMerrchk(outXML)) {
		return false;
	}
	
	show_imuser('image/pen.gif','myVRM SIM', outXML);
}

function chkIMTalker()
{
	// mod: 0-no talker id; 1-have talker id
	ajaxGetXML (0, "inc/imusertalkerinc.asp", "getIMtalker");
	imTimer1 = setTimeout('chkIMTalker()',imchktrlot);
}


var openWind = new Array();
var windcounter = 0;

function getIMtalker(strtalkers)
{
	var isfound, arytalks, arycurtalks, aryall, arytalkinfo, arycurtalk;

	if (IMerrchk(strtalkers)) {
		return false;
	}
	
	aryall = strtalkers.split(";;");
	arytalkers = aryall[1].split("^^");

	curtalker = aryall[0];
	arycurtalkers = curtalker.split("^");
//alert("curtalker=" + curtalker + ": new=" + arytalkers);
	for (var i = 0; i < arytalkers.length - 1; i++) {
		arytalkerinfo = arytalkers[i].split("!!");
		isfound = false;
		
		for (var j = 0; j < arycurtalkers.length - 1; j++) {
			if (arycurtalkers[j] == arytalkerinfo[0]) {
				isfound = true;
				break;
			}
		}
		
//alert("no found, pop new window")
		if (!isfound) {
			createtalk (arytalkerinfo[0], arytalkerinfo[1]);
		}
	}
	
	ifrmListen.window.location.href = "ifrmlisten.asp?m=2";
}	


function createtalk (talkerid, talker)
{
	var url = "imtalking.asp?d=" + talkerid + "&n=" + talker;
	
	var w = 320, h = 220; // default sizes
	if (window.screen) {
		w = window.screen.availWidth * 40 / 100;
		h = window.screen.availHeight * 40 / 100;
	}
	
	openWind[windcounter] = window.open(url, 'imtalking' + windcounter, 'resizable=no,scrollbars=no,toolbar=no, width='+w+',height='+h);
	windcounter++;
}



function shutdowntalker()
{
	ajaxGetXML (0, "inc/closetalkinc.asp", "shutdownwin", "");
	windcounter = 0;
}


function shutdownwin()
{
	for (i=0; i<windcounter; i++) {
	  if (openWind[i] && !openWind[i].closed) 
		openWind[i].close();
	}
}

function goToCal2(frm)
{
	roomcalendarview2(frm);
}
function ShowAlerts()
{
//    alert(window.screen.width);
//    alert(document.getElementById("tblAlerts").style.width);
    document.getElementById("tblAlerts").style.position = 'absolute';
    document.getElementById("tblAlerts").style.left = window.screen.width/2 + 200;
    document.getElementById("tblAlerts").style.display = "";
}
function HideAlerts()
{
    document.getElementById("tblAlerts").style.display = "none";
}
function OpenAlerts()
{
    alert(document.getElementById("lblAlertCount").innerText);
}
function roomcalendarview2(frm)
{

	url = "dispatcher/admindispatcher.asp?cmd=ManageConfRoom&f=v&hf=";
	window.location.href = url;
}
// !!! listen (gif) should be controlled by session later, 
// session vlue transferred by param of function
function showMenu(flag, contactname, contactemail, contactphone, contactaddinfo, imenabled, islisten)
{	
	var mt = "";
	switch (flag) {
		case 0:
			mt += "<table class='btprint' width='100%' border='0' cellpadding='0' cellspacing='0' bgcolor='" + ((if_str=="2") ? "" : "") + "'>";
			break;
		case 1:
			mt += "<table class='btprint' width='100%' border='0' cellpadding='0' cellspacing='0'>";
			break;
	}

	mt += "<tr valign='bottom'>";

    
	if (flag == 1) {
	    mt += "<td width='90%' height='20'>";
	    mt += "<table class='btprint' border='0' width='100%' align='right'><tr>";
	    mt += "<td align='left'>";
	    mt += "<a href='#top'>Haut de page</a>";
	    mt += "</td>";
	    mt += "<td align='right' ";
	    if (isExpressUser != 1)//FB 1779
	    {
	        mt += "><a href='SearchConferenceInputParameters.aspx'>Cherche</a></td>";
	        mt += "<td valign='bottom' width='5'><IMG alt='' src='image/blustar.gif' width=5 border=0></td>";
	    }
	    else
	        mt += " colspan='2'>"; 
	    
	    mt += "</tr></table>";
	    mt += "</td>";
	    mt += "<td width='28%' valign='bottom' align='left'>";
	}
	else {
	    mt += "<td width='90%' height='20' align='right'>";
	    mt += "<table class='btprint' border='0' width='100%' align='right'";

	    if (isExpressUser == 1)//FB 1779
	    {
	        mt += " height='38px'><tr>";
	        mt += "<td align='right' colspan='2'>";
	        mt += "<a href='ManageUserProfile.aspx'>Mes Preferences</a></td>"; //FB 2374
	    }
	    else 
	    {
	        mt += " ><tr>";
	        mt += "<td align='right'><a href='SearchConferenceInputParameters.aspx'>Cherche</a>&nbsp;</td>";
	        mt += "<td valign='bottom' width='5'><img src='image/blustar.gif' width='5'/></td>";
	    }
	   
	    mt += "</tr></table>";
	    mt += "</td>";
	    mt += "<td width='28%' valign='bottom' align='left'>";
	}
	
	mt += "<table class='btprint'>";
	mt += "<tr>";
    // flag == 0 means it is the top menu
	addmenu_star = "<TD valign='bottom'><IMG height=16 alt='' src='image/blustar.gif' width=5 border=0></TD>";
    //alert(document.location.href);
//	addmenu0_1 = "<TD noWrap valign='bottom' id='CTD' name='CTD'><A class=" + ((flag == 0) ? "s" : "sb2") + " href='javascript: goToCal2(\"1\")'>Calendar</A></TD>" + addmenu_star;
	addmenu1_1 = "<TD noWrap valign='bottom'><A href='javascript: OpenAlerts()' onmouseover='javascript:ShowAlerts()' onmouseout='javascript:HideAlerts()'>Alerts(<asp:Label ID='txtAlerts' CssClass='lblError' title='' runat='server' ></asp:Label>)</A></TD>";
	addmenu2_1 = "<TD noWrap valign='bottom'><A href='about_us.asp'>About Us</A></TD>";
	addmenu0_1 = "<TD noWrap valign='bottom'><A href='javascript: openhelp();'>Aide</A></TD>";
	addmenu3_1 = "<TD noWrap valign='bottom'><A href='javascript: openFeedback()'>Remarque</A></TD>";
	
	addmenu4_1 = "<TD noWrap valign='bottom'><A href='privacy.asp'>Privacy</A></TD>";
	if(document.location.href.indexOf(".aspx") < 0)
	{
//    	addmenu5_1 = "<TD noWrap valign='bottom'><A class=" + ((flag == 0) ? "sb3" : "sb3") + " href='thankyou.aspx'>Logoff</A></TD>";
        addmenu5_1 = "";
    	//document.getElementById("txtAlerts") = "<%= Session("Alerts") %>";
    }
    else
    {
//    	addmenu5_1 = "<TD noWrap valign='bottom'><A class=" + ((flag == 0) ? "sb3" : "sb3") + " href='thankyou.aspx'>Logoff</A></TD>";
        addmenu5_1 = "";
    	//document.getElementById("txtAlerts").title = <%= Session['Alerts'] %>;
	}
	addmenu6_1 = "<TD noWrap valign='bottom'><img id='IMlistenIMG' border='0' src='image/unsim.gif' width='30' height='15' align='middle' onclick='listenchg();' title='listener is off'></TD>";
	addmenu6_1+= "<TD noWrap valign='bottom' id='showIMuserDIV'><img id='IMdropdownIMG' border='0' src='image/transparent.gif' width='12' height='12' align='middle' onmouseover='IMuserClick(this);'></TD>";
	
/*
	for (i=2; i<=5; i++) {
		mt += (mma_int & (1<<(mma_num-i))) ? (eval("addmenu" + i + "_1")) + ( (mma_int & ((1<<(mma_num-i))-2) ) ? addmenu_star : "" ) : "";
	}
*/
    if (flag == 0 && document.location.href.toLowerCase().indexOf("settingselect2.aspx") > 0 && document.getElementById("txtClient").value.toUpperCase() == "NGC")
    {
	    i = 1 ;
	    //mt += (mma_int & (1<<(mma_num-i))) ? (eval("addmenu" + i + "_1")) + ( (mma_int & ((1<<(mma_num-i))-2) ) ? addmenu_star : "" ) : "";
	    mt += (eval("addmenu" + i + "_1")) +  addmenu_star;
	}
	//alert(document.frmMenu.feedback_enable.value);
	if ((document.frmMenu.feedback_enable.value).toLowerCase() == "true")
	    mt +=  addmenu0_1 + ((mma_int & "001010") ? addmenu_star : "");
	if ((document.frmMenu.help_enable.value).toLowerCase() == "true")
		mt += addmenu3_1 + ( (mma_int & "001010" ) ? "" : "" );
//	i = 3 ;
//	alert(mma_int);
//	mt += (mma_int & (1<<(mma_num-i))) ? (eval("addmenu" + i + "_1")) + ( (mma_int & ((1<<(mma_num-i))-2) ) ? addmenu_star : "" ) : "";
//	alert(mt);
	i = 5 ;
	mt += (mma_int & (1<<(mma_num-i))) ? (eval("addmenu" + i + "_1")) + ( (mma_int & ((1<<(mma_num-i))-2) ) ? addmenu_star : "" ) : "";

	i = 6 ;
	mt += (parseInt(imenabled)) ? (addmenu_star + eval("addmenu" + i + "_1")) : "";
	

	mt += "</tr>"
	mt += "</table>"
	mt += "</td>"
	mt += "<td width='2%'></td>"
	mt += "</tr>"
	mt += "<tr><td height='1px'></td></tr>" //FB 1836
	mt += "<tr valign='top'>"
	//FB 1836
	//mt += "<td colspan='3' bgcolor='#000080'><IMG height='1px' alt='' src='image/space.gif' width='100%'></td>"
	mt += "<td colspan='3' bgcolor='#000080' height='1px'><IMG  alt='' height='1px' src='image/space.gif' width='100%'></td>"
	mt += "</tr>"
	mt += "</table>"
    
	if (flag == 1) {	
		mt += "<center><table border='0' cellpadding='2' cellspacing='2' class='btprint'>";
		mt += "<tr valign='bottom'>";
		mt += "<td>";
		mt += "<span class=srcstext2>Contacter les equipes de Support : </span><span class=contacttext>" + contactname.replace("||", "\"").replace("!!", "\'") + "</span>"; //FB 1888
		mt += "</td>";
		mt += "<td width=10></td>";
		mt += "<td>";
		mt += "<span class=srcstext2>Email des equipes de Support : </span><span class=contacttext><a  href='mailto:" + contactemail + "'>" + contactemail + "</a></span>";
		mt += "</td>";
		mt += "<td width=10></td>";
		mt += "<td>";
		mt += "<span class=srcstext2>Numero de telephone des equipes de support : </span><span class=contacttext>" + contactphone + "</span>";
		mt += "</td>";
		mt += "</tr>";
		mt += "<td colspan=5 align=center>";
		mt += "<span class=srcstext2>myVRM Version "+ version +" (c), Copyright "+copyrightsDur+" <a href='http://www.myvrm.com' target='_blank'>myVRM.com</a>.  All Rights Reserved.</span>"; //fb 1172
		mt += "</td>";
		mt += "</tr>";
/*
		if (contactaddinfo != "") {
			mt += "<tr>";
			mt += "<td colspan=5>";
			mt += "<span class=srcstext2>Additional Information : </span><span class=contacttext>" + contactaddinfo + "</span>";
			mt += "</td>";
			mt += "</tr>";
		}
*/
		mt += "</table></center>";
	}
	if (parseInt(islisten)) {
		initlisten (parseInt(islisten));
	}
	if (document.getElementById("txtClient").value.toUpperCase() == "NGC")
	{
	    mt += "<table border='1'><tr><td id='tblAlerts' style='display:none'>";
	    mt += "<table bgcolor='lightgrey'>";
	    //mt += "<tr><td class='tableHeader'>Conference Name</td><td class='tableHeader'>Start Date/Time</td><td class='tableHeader'>Duration</td><td class='tableHeader'>Status</td></tr>";
	    mt += "<tr><td class='tableHeader'>Alerts</td></tr>";
	    mt += "<tr><td><label id='lblAlertCountP' style='color:red'></td></tr>";
	    mt += "<tr><td><label id='lblAlertCountA' style='color:red'></td></tr>";
	    //mt += "<tr><tr><td>Conference 2</td><td>01/28/2008</td><td>1 hr 30 min</td><td>Pending</td></tr>";
	    mt += "</table>";
	    mt += "</td></tr></table>";
	}
        //window.setTimeout("_d.write(" + mt + ")", 300);
        _d.write(mt);
        if (document.location.href.toLowerCase().indexOf("settingselect2.aspx") > 0 && flag=="0" && document.getElementById("txtClient").value.toUpperCase() == "NGC")
        {
            //alert(document.getElementById("tblAlerts").innerHTML);
            document.getElementById("txtAlerts").innerText = document.getElementById("txtSAlerts").value;
            if (document.getElementById("txtSAlerts").value == "0")
                document.getElementById("lblAlertCountP").outerText = "No Alerts found.";
            else
            {
                //alert("Pending: " + parseInt(document.getElementById("txtSAlertsP").value,10) + " Approval: " + parseInt(document.getElementById("txtSAlertsA").value,10));
                //document.getElementById("lblAlertCount").innerText = "";
                if (parseInt(document.getElementById("txtSAlertsP").value) > 0)
                    document.getElementById("lblAlertCountP").innerText = document.getElementById("txtSAlertsP").value + " Conferences Pending";
                else
                    document.getElementById("lblAlertCountP").outerText = "No pending conferences found.";
                if (parseInt(document.getElementById("txtSAlertsA").value,10) > 0)
                    document.getElementById("lblAlertCountA").innerText = document.getElementById("txtSAlertsA").value + " Conferences Waiting for Approval";
                else
                    document.getElementById("lblAlertCountA").outerText = "No conferences waiting for your approval";
            }
        }
}


//Added for New Menu Design
function showTopMenu(flag, contactname, contactemail, contactphone, contactaddinfo, imenabled, islisten)
{	
	var mt = "";
	switch (flag) {
		case 0:
			mt += "<table class='btprint' width='100%' border='0' cellpadding='0' cellspacing='0' bgcolor='" + ((if_str=="2") ? "" : "") + "'>";
			break;
		case 1:
			mt += "<table class='btprint' width='100%' border='0' cellpadding='0' cellspacing='0'>";
			break;
	}

	mt += "<tr valign='bottom'>";
   	mt += "<td width='70%' height='20'>";

    
	if (flag == 1) {
		mt += "<a class=sb1 href='#top'>Haut de page</a>";
	}

	mt += "</td>";
	mt += "<td width='28%' valign='bottom' align='right'>";
	mt += "<table class='btprint'>";
	mt += "<tr>";
    // flag == 0 means it is the top menu
	addmenu_star = "<TD valign='bottom'><IMG height=16 alt='' src='image/blustar.gif' width=5 border=0></TD>";
    //alert(document.location.href);
//	addmenu0_1 = "<TD noWrap valign='bottom' id='CTD' name='CTD'><A class=" + ((flag == 0) ? "s" : "sb2") + " href='javascript: goToCal2(\"1\")'>Calendar</A></TD>" + addmenu_star;
	
	addmenu1_1 = "<TD noWrap valign='bottom'><A class=" + ((flag == 0) ? "sb3" : "sb3") + " href='javascript: OpenAlerts()' onmouseover='javascript:ShowAlerts()' onmouseout='javascript:HideAlerts()'>Alerts(<asp:Label ID='txtAlerts' CssClass='lblError' title='' runat='server' ></asp:Label>)</A></TD>";
	addmenu2_1 = "<TD noWrap valign='bottom'><A class=" + ((flag == 0) ? "sb3" : "sb3") + " href='about_us.asp'>About Us</A></TD>";
	addmenu0_1 = "<TD noWrap valign='bottom'><A class=" + ((flag == 0) ? "sb3" : "sb3") + " href='javascript: openhelp();'>Aide</A></TD>";
	addmenu3_1 = "<TD noWrap valign='bottom'><A class=" + ((flag == 0) ? "sb3" : "sb3") + " href='javascript: openFeedback()'>Remarque</A></TD>";
	addmenu4_1 = "<TD noWrap valign='bottom'><A class=" + ((flag == 0) ? "sb3" : "sb3") + " href='privacy.asp'>Privacy</A></TD>";
	if(document.location.href.indexOf(".aspx") < 0)
	{
    	//addmenu5_1 = "<TD noWrap valign='bottom'><A class=" + ((flag == 0) ? "sb3" : "sb3") + " href='thankyou.aspx'>Logoff</A></TD>";
    	addmenu5_1 = "";
    	//document.getElementById("txtAlerts") = "<%= Session("Alerts") %>";
    }
    else
    {
//    	addmenu5_1 = "<TD noWrap valign='bottom'><A class=" + ((flag == 0) ? "sb3" : "sb3") + " href='thankyou.aspx'>Logoff</A></TD>";
        addmenu5_1 = "";
    	//document.getElementById("txtAlerts").title = <%= Session['Alerts'] %>;
	}
	addmenu6_1 = "<TD noWrap valign='bottom'><img id='IMlistenIMG' border='0' src='image/unsim.gif' width='30' height='15' align='middle' onclick='listenchg();' title='listener is off'></TD>";
	addmenu6_1+= "<TD noWrap valign='bottom' id='showIMuserDIV'><img id='IMdropdownIMG' border='0' src='image/transparent.gif' width='12' height='12' align='middle' onmouseover='IMuserClick(this);'></TD>";
	
/*
	for (i=2; i<=5; i++) {
		mt += (mma_int & (1<<(mma_num-i))) ? (eval("addmenu" + i + "_1")) + ( (mma_int & ((1<<(mma_num-i))-2) ) ? addmenu_star : "" ) : "";
	}
*/
    if (flag == 0 && document.location.href.toLowerCase().indexOf("settingselect2.aspx") > 0 && document.getElementById("txtClient").value.toUpperCase() == "NGC")
    {
	    i = 1 ;
	    //mt += (mma_int & (1<<(mma_num-i))) ? (eval("addmenu" + i + "_1")) + ( (mma_int & ((1<<(mma_num-i))-2) ) ? addmenu_star : "" ) : "";
	    mt += (eval("addmenu" + i + "_1")) +  addmenu_star;
	}
	//alert(document.frmMenu.feedback_enable.value);
	if ((document.frmMenu.feedback_enable.value).toLowerCase() == "true")
		mt += addmenu0_1 + ( (mma_int & "001010" ) ? addmenu_star : "" );
	if ((document.frmMenu.help_enable.value).toLowerCase() == "true")
		mt += addmenu3_1 + ( (mma_int & "001010" ) ? addmenu_star : "" );
//	i = 3 ;
//	alert(mma_int);
//	mt += (mma_int & (1<<(mma_num-i))) ? (eval("addmenu" + i + "_1")) + ( (mma_int & ((1<<(mma_num-i))-2) ) ? addmenu_star : "" ) : "";
//	alert(mt);
	i = 5 ;
	mt += (mma_int & (1<<(mma_num-i))) ? (eval("addmenu" + i + "_1")) + ( (mma_int & ((1<<(mma_num-i))-2) ) ? addmenu_star : "" ) : "";

	i = 6 ;
	mt += (parseInt(imenabled)) ? (addmenu_star + eval("addmenu" + i + "_1")) : "";
	

	mt += "</tr>"
	mt += "</table>"
	mt += "</td>"
	mt += "<td width='2%'></td>"
	mt += "</tr>"
//	mt += "<tr valign='top'>"
//	mt += "<td colspan='3' bgcolor='#000080'><IMG height='1' alt='' src='image/space.gif' width='100%'></td>"
//	mt += "</tr>"
	mt += "</table>"
    
	if (flag == 1) {	
		mt += "<center><table border='0' cellpadding='2' cellspacing='2' class='btprint'>";
		mt += "<tr valign='bottom'>";
		mt += "<td>";
		mt += "<span class=srcstext2>Contacter les equipes de Support : </span><span class=contacttext>" + contactname.replace("||", "\"").replace("!!", "\'") + "</span>"; //FB 1888
		mt += "</td>";
		mt += "<td width=10></td>";
		mt += "<td>";
		mt += "<span class=srcstext2>Email des equipes de Support : </span><span class=contacttext><a  href='mailto:" + contactemail + "'>" + contactemail + "</a></span>";
		mt += "</td>";
		mt += "<td width=10></td>";
		mt += "<td>";
		mt += "<span class=srcstext2>Numero de telephone des equipes de support : </span><span class=contacttext>" + contactphone + "</span>";
		mt += "</td>";
		mt += "</tr>";
		mt += "<td colspan=5 align=center>";
		mt += "<span class=srcstext2>myVRM Version "+ version +" (c), Copyright "+ copyrightsDur+" <a href='http://www.myvrm.com' target='_blank'>myVRM.com</a>.  All Rights Reserved.</span>"; //fb 1172
		mt += "</td>";
		mt += "</tr>";
/*
		if (contactaddinfo != "") {
			mt += "<tr>";
			mt += "<td colspan=5>";
			mt += "<span class=srcstext2>Additional Information : </span><span class=contacttext>" + contactaddinfo + "</span>";
			mt += "</td>";
			mt += "</tr>";
		}
*/
		mt += "</table></center>";
	}
	if (parseInt(islisten)) {
		initlisten (parseInt(islisten));
	}
	if (document.getElementById("txtClient").value.toUpperCase() == "NGC")
	{
	    mt += "<table border='1'><tr><td id='tblAlerts' style='display:none'>";
	    mt += "<table bgcolor='lightgrey'>";
	    //mt += "<tr><td class='tableHeader'>Conference Name</td><td class='tableHeader'>Start Date/Time</td><td class='tableHeader'>Duration</td><td class='tableHeader'>Status</td></tr>";
	    mt += "<tr><td class='tableHeader'>Alerts</td></tr>";
	    mt += "<tr><td><label id='lblAlertCountP' style='color:red'></td></tr>";
	    mt += "<tr><td><label id='lblAlertCountA' style='color:red'></td></tr>";
	    //mt += "<tr><tr><td>Conference 2</td><td>01/28/2008</td><td>1 hr 30 min</td><td>Pending</td></tr>";
	    mt += "</table>";
	    mt += "</td></tr></table>";
	}
        //window.setTimeout("_d.write(" + mt + ")", 300);
        _d.write(mt);
        if (document.location.href.toLowerCase().indexOf("settingselect2.aspx") > 0 && flag=="0" && document.getElementById("txtClient").value.toUpperCase() == "NGC")
        {
            //alert(document.getElementById("tblAlerts").innerHTML);
            document.getElementById("txtAlerts").innerText = document.getElementById("txtSAlerts").value;
            if (document.getElementById("txtSAlerts").value == "0")
                document.getElementById("lblAlertCountP").outerText = "No Alerts found.";
            else
            {
                //alert("Pending: " + parseInt(document.getElementById("txtSAlertsP").value,10) + " Approval: " + parseInt(document.getElementById("txtSAlertsA").value,10));
                //document.getElementById("lblAlertCount").innerText = "";
                if (parseInt(document.getElementById("txtSAlertsP").value) > 0)
                    document.getElementById("lblAlertCountP").innerText = document.getElementById("txtSAlertsP").value + " Conferences Pending";
                else
                    document.getElementById("lblAlertCountP").outerText = "No pending conferences found.";
                if (parseInt(document.getElementById("txtSAlertsA").value,10) > 0)
                    document.getElementById("lblAlertCountA").innerText = document.getElementById("txtSAlertsA").value + " Conferences Waiting for Approval";
                else
                    document.getElementById("lblAlertCountA").outerText = "No conferences waiting for your approval";
            }
        }
}


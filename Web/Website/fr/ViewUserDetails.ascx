﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="ns_MyVRM.en_ViewUserDetails" %>
<script language="javascript" type="text/javascript">
    function ClosePopUp()
    {   
        parent.document.getElementById("viewHostDetails").style.display = 'none';
        return false;
    }
</script>

<table cellpadding="0" cellspacing="0" border="0" align="center" width="100%">
	<tr>
		<td align="center">
            <table cellpadding="2" cellspacing="1" style="border-color:Black;border-width:1px;border-style:Solid;"  class="tableBody" align="center" width="50%">
              <tr>
                <td class="subtitleblueblodtext" align="center" colspan="2">
                    User Details<br />
                </td>            
              </tr>
              <tr>
               <td align="right" style="width:40%" class="blackblodtext"><b>Name :</b></td> 
               <td align="left">
                   <asp:Label ID="lblUsrName" runat="server" Text="N/A"></asp:Label>
               </td>
              </tr>
              <tr>
               <td align="right" class="blackblodtext"><b>Email ID :</b></td> 
               <td align="left">
                   <asp:Label ID="lblUsrEmail" runat="server"></asp:Label>
               </td>
              </tr>
              <tr>
               <td align="right" class="blackblodtext"><b>AD/LDAP Login :</b></td> 
               <td align="left">
                   <asp:Label ID="lblUsrLogin" runat="server" Text="N/A"></asp:Label>
               </td>
              </tr>
              <% if(Session["timezoneDisplay"].ToString() == "1") { %> 
              <tr>
               <td align="right" class="blackblodtext"><b>Time Zone :</b></td> 
               <td align="left">
                   <asp:Label ID="lblUsrTimeZone" runat="server"></asp:Label>
               </td>
              </tr>
              <% } else {%>
              <tr>
               <td align="right" class="blackblodtext"><b>Time Zone Display :</b></td> 
               <td align="left">Off</td>
              </tr>
              <% }%>
              <tr>
               <td align="right" class="blackblodtext"><b>Preferred Language :</b></td> 
               <td align="left">
                   <asp:Label ID="lblUsrLang" runat="server"></asp:Label>
               </td>
              </tr>
              <tr>
               <td align="right" class="blackblodtext"><b>Email Language :</b></td> 
               <td align="left">
                   <asp:Label ID="lblUsrEmailLang" runat="server" Text="N/A"></asp:Label>
               </td>
              </tr>
              <tr>
               <td align="right" class="blackblodtext"><b>Block Emails :</b></td> 
               <td align="left">
                   <asp:Label ID="lblUsrBlockedEmail" runat="server"></asp:Label>
               </td>
              </tr>
              <tr>
               <td align="right" class="blackblodtext"><b>Work :</b></td> 
               <td align="left">
                   <asp:Label ID="lblUsrWork" runat="server" Text="N/A"></asp:Label>
               </td>
              </tr>
              <tr>
               <td align="right" class="blackblodtext"><b>Cell :</b></td> 
               <td align="left">
                   <asp:Label ID="lblUsrCell" runat="server" Text="N/A"></asp:Label>
               </td>
              </tr>
              <tr>
               <td align="center" colspan="2"><br />
                  <asp:Button ID="BtnUsrDetailClose" Text="Close" CssClass="altShortBlueButtonFormat" runat="server" OnClientClick="javascript:return ClosePopUp()"></asp:Button>
               </td>
              </tr>
           </table>
		</td>
	</tr>
</table>

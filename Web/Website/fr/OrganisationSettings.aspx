<%@ Page Language="C#" AutoEventWireup="true" Inherits="OrganisationSettings" %>

<%@ Register TagPrefix="cc1" Namespace="myVRMWebControls" Assembly="myVRMWebControls" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register Assembly="MetaBuilders.WebControls.ComboBox" Namespace="MetaBuilders.WebControls"
    TagPrefix="mbcbb" %>
<%if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("MSIE"))
  {%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="X-UA-Compatible" content="IE=7" />
<!-- FB 2050 -->
<%}
  else
  {%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%} %>
<head>
    <%--<meta http-equiv="Cache-Control" content="no-cache">--%>
</head>
<%--added for FB 1710 start--%>
<%@ Register Assembly="DevExpress.SpellChecker.v10.2.Core, Version=10.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraSpellChecker" TagPrefix="dxXSC" %>
<%@ Register Assembly="DevExpress.Web.ASPxSpellChecker.v10.2, Version=10.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxSpellChecker" TagPrefix="dxSC" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v10.2, Version=10.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dxHE" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v10.2, Version=10.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxE" %>
<%@ Register Assembly="DevExpress.Web.v10.2, Version=10.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dxP" %>
<%@ Register Assembly="DevExpress.Web.v10.2, Version=10.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxRoundPanel" TagPrefix="dxRP" %>
<%--added for FB 1710 end--%>
<!--window Dressing-->
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->

<script runat="server">
</script>

<script language="JavaScript" src="inc\functions.js">
</script>

<script type="text/javascript" src="script/calview.js"></script>

<script type="text/javascript" language="javascript" src='script/lib.js'></script>

<script type="text/javascript" src="inc/functions.js"></script>

<script language="JavaScript">
<!--
//FB Case 807 starts here
function deleteApprover(id)
{
	eval("document.getElementById('hdnApprover" + (id+1) + "')").value = "";
	eval("document.getElementById('txtApprover" + (id+1) + "')").value = "";
}
//FB 2599 Start
function PreservePassword()// FB 2262
{
    document.getElementById("hdnVidyoPassword").value = document.getElementById("txtvidyoPassword1").value; //FB 2363
}
//FB 2599 End
function getYourOwnEmailList (i)
{
    if (i == -2)//Login Management
    {
//        url = "dispatcher/conferencedispatcher.asp?frm=roomassist&frmname=frmMainroom&cmd=GetEmailList&emailListPage=1&wintype=pop";
      if(queryField("sb") > 0 )
            url = "emaillist2.aspx?t=e&frm=approverNET&wintype=ifr&fn=frmMainsuperadministrator&n=";
            else
            url = "emaillist2main.aspx?t=e&frm=approverNET&fn=frmMainsuperadministrator&n=";
    }
    else
    {
//        url = "dispatcher/conferencedispatcher.asp?frm=approver&frmname=frmMainroom&no=" + i + "&cmd=GetEmailList&emailListPage=1&wintype=pop";
        url = "emaillist2main.aspx?t=e&frm=approverNET&fn=frmMainsuperadministrator&n=" + i;
	}
	//url = "dispatcher/conferencedispatcher.asp?frm=approverNET&frmname=frmMainsuperadministrator&no=" + i + "&cmd=GetEmailList&emailListPage=1&wintype=pop";
	if (!window.winrtc) {	// has not yet been defined
		winrtc = window.open(url, "", "width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
		winrtc.focus();
	} else // has been defined
	    if (!winrtc.closed) {     // still open
	    	winrtc.close();
	        winrtc = window.open(url, "", "width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
			winrtc.focus();
		} else {
	        winrtc = window.open(url, "", "width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
	        winrtc.focus();
		}
				
}


 //code added for custom attribute fixes

function OpenEntityCode()
{
    window.location.replace("ViewCustomAttributes.aspx");
}
// Added for FB 1758
function fnReset()
    {
        document.getElementById("txtTestEmailId").innerText = "";
        document.getElementById("reqvalid").innerText = "";
        document.getElementById("regTestemail").innerText = "";
        document.getElementById("reqvalid2").innerText = "";
    }
//FB 1849    
function fnChangeOrganization()
{
    var btnchng = document.getElementById("BtnChangeOrganization");
    var drporg = document.getElementById("DrpOrganization");
    var cnfrm = confirm("Ci-apr�s toutes les transactions effectu�es dans le syst�me sera s�lectionn� pour la organization.Do vous souhaitez poursuivre?");
            
    if(cnfrm)
        return true;
    else
        return false;
   
}

//FB 2052
function OpenDayColor()
{
    window.location.replace("HolidayDetails.aspx");
}

//-->

//FB 2343
function WorkingDayDetails()
{
    window.location.replace("WorkingDays.aspx");
}

//FB 2486
function OpenManageMsg()
{
    window.location.replace("ManageMessages.aspx");
}

</script>

<script type="text/javascript" src="script\approverdetails.js">

</script>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf8" />
    <title></title>
</head>
<body>
    <%--UI Changes for FB 1849--%>
    <form name="frmOrgSettings" id="frmOrgSettings" method="Post" action="OrganisationSettings.aspx"
    language="JavaScript" runat="server">
    <asp:ScriptManager ID="CalendarScriptManager" runat="server" AsyncPostBackTimeout="600">
    </asp:ScriptManager>
    <%--FB 1849--%>
    <center>
        <input type="hidden" id="helpPage" value="92">
        <input type="hidden" id="hdnVidyoPassword" runat="server" /> <%--FB 2262 //FB 2599--%>
        <input type="hidden" id="hdnMailServer" runat="server" />
        <input type="hidden" id="hdnLDAPPassword" runat="server" />
        <h3>
            Organisation Arrangements</h3>
        <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError"></asp:Label><br />
    </center>
    <%--FB 1710 Alignment change start--%>
    <tr>
        <td>
            <%--FB 1849--%>
            <%--FB 1982--%>
            <table width="100%" border="0">
                <tr>
                    <td>
                        <table width="100%">
                            <%--FB 1849--%>
                            <tr>
                                <td>
                                    <table width="100%" border="0">
                                        <tr valign="top" id="trSwt" runat="server">
                                            <td colspan="2" align="right" valign="top" style="display: none">
                                                <a id="ChgOrg" runat="server" href="#" class="blueblodtext">Switch Organization
                                                </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="40%" valign="top">
                                                <table width="100%">
                                                    <%--FB 1982--%>
                                                    <%--TD Width Updated for FB 2050--%>
                                                    <tr>
                                                        <td align="left" class="subtitleblueblodtext" colspan="5" style="height: 21; font-weight: bold">
                                                            ressources d'organisation
                                                        </td>
                                                        <%--CSS Project--%>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" height="21" style="font-weight: bold; width: 5%">
                                                        </td>
                                                        <td style="width: 45%" align="left" valign="top" class="blackblodtext">
                                                            <b>Salles de conf�rence avec video</b>
                                                        </td>
                                                        <!-- FB 2050 -->
                                                        <td style="width: 50%" align="left" valign="top" colspan="3">
                                                            <!-- FB 2050 -->
                                                            <asp:Label ID="LblActRooms" runat="server" ForeColor="DarkGray" BorderColor="#cccccc"
                                                                BorderStyle="Solid" BorderWidth="1px" Width="40px" Style="text-align: right"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" height="21" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" valign="top" class="blackblodtext">
                                                            <b>Non-Vid�o chambres</b>
                                                        </td>
                                                        <td align="left" valign="top" colspan="3">
                                                            <asp:Label ID="LblNonVidRooms" runat="server" ForeColor="DarkGray" BorderColor="#cccccc"
                                                                BorderStyle="Solid" BorderWidth="1px" Width="40px" Style="text-align: right"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <%--FB 2586 START--%>
                                                     <tr>
                                                        <td align="right" height="21" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" valign="top" class="blackblodtext">
                                                            <b>VMR Rooms</b>
                                                        </td>
                                                        <td align="left" valign="top" colspan="3">
                                                            <asp:Label ID="LblVMRRooms" runat="server" ForeColor="DarkGray" BorderColor="#cccccc"
                                                                BorderStyle="Solid" BorderWidth="1px" Width="40px" Style="text-align: right"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <%--FB 2586 END--%>
                                                    <tr>
                                                        <td align="right" height="21" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" valign="top" class="blackblodtext">
                                                            <b>Utilisateur</b>
                                                        </td>
                                                        <td align="left" class="altblackblodttext" valign="top" colspan="3">
                                                            <asp:Label ID="LblActUsers" runat="server" ForeColor="DarkGray" BorderColor="#cccccc"
                                                                BorderStyle="Solid" BorderWidth="1px" Width="40px" Style="text-align: right"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" height="21" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" valign="top" class="blackblodtext">
                                                            <b>Utilizateur de Domino</b>
                                                        </td>
                                                        <td align="left" class="altblackblodttext" valign="top" colspan="3">
                                                            <asp:Label ID="LblDuser" runat="server" ForeColor="DarkGray" BorderColor="#cccccc"
                                                                BorderStyle="Solid" BorderWidth="1px" Width="40px" Style="text-align: right"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" height="21" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" valign="top" class="blackblodtext">
                                                            <b>�change d'utilisateur</b>
                                                        </td>
                                                        <td align="left" class="altblackblodttext" valign="top" colspan="3">
                                                            <asp:Label ID="LblExchUser" runat="server" ForeColor="DarkGray" BorderColor="#cccccc"
                                                                BorderStyle="Solid" BorderWidth="1px" Width="40px" Style="text-align: right"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <%--FB 1979--%>
                                                        <td align="right" height="21" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" valign="top" class="blackblodtext">
                                                            <b>Utilisateur mobiles</b>
                                                        </td>
                                                        <td align="left" class="altblackblodttext" valign="top" colspan="3">
                                                            <asp:Label ID="LblMobUser" runat="server" ForeColor="DarkGray" BorderColor="#cccccc"
                                                                BorderStyle="Solid" BorderWidth="1px" Width="40px" Style="text-align: right"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" height="21" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" valign="top" class="blackblodtext">
                                                            <b>Standard MCU</b>
                                                        </td>
                                                        <td align="left" class="altblackblodttext" valign="top" colspan="3">
                                                            <asp:Label ID="LblActMCU" runat="server" ForeColor="DarkGray" BorderColor="#cccccc"
                                                                BorderStyle="Solid" BorderWidth="1px" Width="40px" Style="text-align: right"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <%--FB 2486 Start--%>
                                                        <tr>
                                                        <td align="right" height="21" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" valign="top" class="blackblodtext">
                                                            <b>Am�lioration de MCU</b>
                                                        </td>
                                                        <td align="left" class="altblackblodttext" valign="top" colspan="3">
                                                            <asp:Label ID="LblEnchancedMCU" runat="server" ForeColor="DarkGray" BorderColor="#cccccc"
                                                                BorderStyle="Solid" BorderWidth="1px" Width="40px" Style="text-align: right"></asp:Label>
                                                        </td>
                                                        </tr>
                                                        <%--FB 2486 End--%>
                                                    <tr>
                                                        <td align="right" height="21" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" valign="top" class="blackblodtext">
                                                            <b>Points de connexion</b>
                                                        </td>
                                                        <td align="left" class="altblackblodttext" valign="top" colspan="3">
                                                            <asp:Label ID="LblEpts" runat="server" ForeColor="DarkGray" BorderColor="#cccccc"
                                                                BorderStyle="Solid" BorderWidth="1px" Width="40px" Style="text-align: right"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <%--FB 2426 Start--%>
                                                    <tr>
                                                        <td align="right" height="21" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" valign="top" class="blackblodtext">
                                                            <b>Guest Rooms</b>
                                                        </td>
                                                        <td align="left" class="altblackblodttext" valign="top" colspan="3">
                                                            <asp:Label ID="LblGstRooms" runat="server" ForeColor="DarkGray" BorderColor="#cccccc"
                                                                BorderStyle="Solid" BorderWidth="1px" Width="40px" Style="text-align: right"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" height="21" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" valign="top" class="blackblodtext">
                                                            <b>Guest Rooms Per User</b>
                                                        </td>
                                                        <td align="left" class="altblackblodttext" valign="top" colspan="3">
                                                            <asp:Label ID="LblGstRoomsPerUser" runat="server" ForeColor="DarkGray" BorderColor="#cccccc"
                                                                BorderStyle="Solid" BorderWidth="1px" Width="40px" Style="text-align: right"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <%--FB 2426 End--%>
                                                    <tr>
                                                        <td align="right" height="21" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" valign="top" class="blackblodtext">
                                                            <b>Module de nourriture</b>
                                                        </td>
                                                        <td align="left" class="altblackblodttext" valign="top" colspan="3">
                                                            <asp:Label ID="LblFdMod" runat="server" Text="d�sactiv�"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" height="21" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" valign="top" class="blackblodtext">
                                                            <b>Module de Manutention</b>
                                                        </td>
                                                        <td align="left" class="altblackblodttext" valign="top" colspan="3">
                                                            <asp:Label ID="LblRsMod" runat="server" Text="d�sactiv�"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" height="21" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" valign="top" class="blackblodtext">
                                                            <b>Modules de commodit�s</b>
                                                        </td>
                                                        <td align="left" class="altblackblodttext" valign="top" colspan="3">
                                                            <asp:Label ID="LblFacility" runat="server" Text="d�sactiv�"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" height="21" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" valign="top" class="blackblodtext">
                                                            <b>Module API</b>
                                                        </td>
                                                        <td align="left" class="altblackblodttext" valign="top" colspan="3">
                                                            <asp:Label ID="LblApi" runat="server" Text="d�sactiv�"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <%--FB 2347--%>
                                                        <td align="right" height="21" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" valign="top" class="blackblodtext">
                                                            <b>Module PC</b>
                                                        </td>
                                                        <td align="left" class="altblackblodttext" valign="top" colspan="3">
                                                            <asp:Label ID="LblPC" runat="server" Text="d�sactiv�"></asp:Label>
                                                        </td>
                                                    </tr>
                                                     <%--FB 2262 //FB 2599 FB 2645 Starts--%>
                                                     <%if (Session["Cloud"] != null && Session["Cloud"].ToString().Equals("1"))
                                                       {%> 
                                                    <tr>
                                                        <td align="right" height="21" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" valign="top" class="blackblodtext">
                                                            <b>Cloud</b>
                                                        </td>
                                                        <td align="left" class="altblackblodttext" valign="top" colspan="3">
                                                            <asp:Label ID="LblCloud" runat="server" Text="Disabled"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <%} %>
                                                    
                                                    <%--FB 2262 //FB 2599 Ends--%>
                                                    <%--FB 2594 FB 2645 Starts--%>
                                                    <% if (Session["EnablePublicRooms"] != null && Session["EnablePublicRooms"].ToString().Equals("1"))
                                                       { %>
                                                    <tr>
                                                        <td align="right" height="21" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" valign="top" class="blackblodtext">
                                                            <b>Public Room Service</b>
                                                        </td>
                                                        <td align="left" class="altblackblodttext" valign="top" colspan="3">
                                                            <asp:Label ID="lblPublicRoom" runat="server" Text="Disabled"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <% } %>
                                                    <%--FB 2594 Ends--%>
                                                    <tr>
                                                        <td align="left" class="subtitleblueblodtext" colspan="5">
                                                            Approbateur (niveau systeme)
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" height="21" style="font-weight: bold">
                                                        </td>
                                                        <td align="right" style="font-weight: bold" class="blackblodtext">
                                                        </td>
                                                        <%--FB 1982 --%>
                                                        <td align="left" style="font-weight: bold; width: 35%" class="style3">
                                                            Nom de l'approuveur
                                                        </td>
                                                       <td id="tdlblAction" height="21" style="font-weight: bold; width: 10%" class="blackblodtext" runat="server"> <%--FB 2594--%>
                                                            Action
                                                        </td>
                                                        <%--FB 1982 --%>
                                                        <td style="width: 5%">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" height="21" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" height="21" style="font-weight: bold" class="blackblodtext">
                                                            <%--FB 1982--%>
                                                            Approbateur principal
                                                        </td>
                                                        <td style="text-align: left" class="style4">
                                                            <asp:TextBox ID="txtApprover1" runat="server" CssClass="altText" Enabled="False"
                                                                Style="width: 95%"></asp:TextBox>
                                                        </td>
                                                        <%--FB 1982 --%>
                                                        <td align="left" id="tdAction" runat="server"> <%--FB 2594--%>
                                                            <a href="javascript: getYourOwnEmailList(0);" onmouseover="window.status='';return true;">
                                                                <img id="Img1" border="0" src="image/edit.gif" alt="" /></a>
                                                            <%--FB 1982--%>
                                                            <a href="javascript: deleteApprover(0);" onmouseover="window.status='';return true;">
                                                                <img border="0" src="image/btn_delete.gif" alt="Supprimer" alt="" width="16" height="16"></a>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="hdnApprover1" runat="server" Width="0px" BackColor="White" BorderColor="White"
                                                                BorderStyle="None" Style="display: none"></asp:TextBox>
                                                            <%--FB 1982 --%>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left">
                                                        </td>
                                                        <td align="left" style="font-weight: bold" class="blackblodtext">
                                                            Approbateur secondaire
                                                        </td>
                                                        <td style="text-align: left;" class="blackblodtext">
                                                            <asp:TextBox ID="txtApprover2" runat="server" CssClass="altText" Enabled="False"
                                                                Style="width: 95%"></asp:TextBox>
                                                        </td>
                                                        <%--FB 1982 --%>
                                                        <td align="left" id="tdAction2" runat="server"> <%--FB 2594--%>
                                                            <a href="javascript: getYourOwnEmailList(1);" onmouseover="window.status='';return true;">
                                                                <img id="Img2" border="0" src="image/edit.gif" /></a>
                                                            <%--FB 1982--%>
                                                            <a href="javascript: deleteApprover(1);" onmouseover="window.status='';return true;">
                                                                <img border="0" src="image/btn_delete.gif" alt="Supprimer" width="16" height="16"></a>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="hdnApprover2" runat="server" Width="0px" BackColor="White" BorderColor="White"
                                                                BorderStyle="None" Style="display: none"></asp:TextBox>
                                                        </td>
                                                        <%--FB 1982 --%>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" height="21%" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" height="21" style="font-weight: bold" class="blackblodtext">
                                                            Approbateur secondaire
                                                        </td>
                                                        <td style="text-align: left;" class="style5">
                                                            <asp:TextBox ID="txtApprover3" runat="server" CssClass="altText" Enabled="False"
                                                                Style="width: 95%"></asp:TextBox>
                                                        </td>
                                                        <%--FB 1982 --%>
                                                        <td align="left" id="tdAction3" runat="server"> <%--FB 2594--%>
                                                            <a href="javascript: getYourOwnEmailList(2);" onmouseover="window.status='';return true;">
                                                                <img id="Img3" border="0" src="image/edit.gif" /></a>
                                                            <%--FB 1982--%>
                                                            <a href="javascript: deleteApprover(2);" onmouseover="window.status='';return true;">
                                                                <img border="0" src="image/btn_delete.gif" alt="Supprimer" width="16" height="16"></a>
                                                        </td>
                                                        <td style="height: 21px;">
                                                            <asp:TextBox ID="hdnApprover3" runat="server" Width="0px" BackColor="White" BorderColor="White"
                                                                BorderStyle="None" Style="display: none"></asp:TextBox>
                                                        </td>
                                                        <%--FB 1982 --%>
                                                    </tr>
                                                    
                                                    <tr>
                                                        <td align="left" class="subtitleblueblodtext" colspan="5">
                                                            Rapport d'utilisation Chambre param�tres
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                         <td align="right" height="21" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" class="blackblodtext">
                                                            Comptable jours ouvrables
                                                        </td>
                                                        <td style="height: 21px;" colspan="3">
                                                            <input type="button" name="btnManageDayColor" value="configurer" class="altShortBlueButtonFormat"
                                                                onclick="javascript:WorkingDayDetails();" />&nbsp;
                                                        </td>                                                      
                                                    </tr>
                                                    <tr>
                                                         <td align="right" height="21" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" class="blackblodtext">
                                                            Nombre de comptabilit� des heures de travail
                                                        </td>
                                                        <td style="height: 21px;" colspan="3">
                                                            <asp:DropDownList ID="lstWorkingHours" runat="server" CssClass="altSelectFormat"
                                                                Style="width: 30%;">
                                                                <asp:ListItem Value="1">1</asp:ListItem>
                                                                <asp:ListItem Value="2">2</asp:ListItem>
                                                                <asp:ListItem Value="3">3</asp:ListItem>
                                                                <asp:ListItem Value="4">4</asp:ListItem>
                                                                <asp:ListItem Value="5">5</asp:ListItem>
                                                                <asp:ListItem Value="6">6</asp:ListItem>
                                                                <asp:ListItem Value="7">7</asp:ListItem>
                                                                <asp:ListItem Selected="True" Value="8">8</asp:ListItem>
                                                                <asp:ListItem Value="9">9</asp:ListItem>
                                                                <asp:ListItem Value="10">10</asp:ListItem>
                                                                <asp:ListItem Value="11">11</asp:ListItem>
                                                                <asp:ListItem Value="12">12</asp:ListItem>
                                                                <asp:ListItem Value="13">13</asp:ListItem>
                                                                <asp:ListItem Value="14">14</asp:ListItem>
                                                                <asp:ListItem Value="15">15</asp:ListItem>
                                                                <asp:ListItem Value="16">16</asp:ListItem>
                                                                <asp:ListItem Value="17">17</asp:ListItem>
                                                                <asp:ListItem Value="18">18</asp:ListItem>
                                                                <asp:ListItem Value="19">19</asp:ListItem>
                                                                <asp:ListItem Value="20">20</asp:ListItem>
                                                                <asp:ListItem Value="21">21</asp:ListItem>
                                                                <asp:ListItem Value="22">22</asp:ListItem>
                                                                <asp:ListItem Value="23">23</asp:ListItem>
                                                                <asp:ListItem Value="24">24</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                   <%-- FB 2501 EM7 Starts--%>
                                                   <%-- FB 2598 EnableEM7 Starts tr-id --%>
                                                    <tr id="trEM7OrgSetting" runat="server">
                                                    
                                                        <td align="left" class="subtitleblueblodtext" colspan="5">
                                                           EM7 organisation de v�rification des pouvoirs
                                                         </td>
                                                    </tr>
                                                    <tr id="trEM7Organization" runat="server">
                                                     <td align="right" height="21" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" class="blackblodtext">
                                                            EM7 Organization
                                                        </td>
                                                        <td>
                                                                <asp:DropDownList ID="lstEM7Orgsilo" runat="server" CssClass="altSelectFormat" Style="width: 95%">
                                                                <asp:ListItem Value="-1">Pas de Articles</asp:ListItem>
                                                                </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <%-- FB 2598 EnableEM7 tr-id Ends--%>
                                                    <%-- FB 2501 EM7 Ends--%>
                                                </table>
                                            </td>
                                            <td width="40%" valign="top">
                                                <%--FB 1982--%>
                                                <table width="90%" border="0" cellpadding="2" cellspacing="0">
                                                    <%--FB 1982--%>
                                                    <tr>
                                                        <td align="left" class="subtitleblueblodtext" colspan="5" style="height: 21; font-weight: bold">
                                                            Arrangements d'interface de l'utilisateur
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="2%">
                                                        </td>
                                                        <td style="font-weight: bold; width: 15%" class="blackblodtext" colspan="2">
                                                            Arrangements de conception de UI &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        </td>
                                                        <td align="left" width="20%">
                                                            <asp:Button ID="Button1" runat="server" Style="width: 275px;" CssClass="altLongBlueButtonFormat"
                                                                OnClick="btnChangeUIDesign_Click" Text="Changer la conception de la presentation" />
                                                        </td>
                                                        <td width="1%">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="2%">
                                                        </td>
                                                        <td style="font-weight: bold" class="blackblodtext" colspan="2">
                                                            Arrangements de text UI &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        </td>
                                                        <td align="left">
                                                            <input type="button" id="Button2" style="width: 275px;" value="Modifier UI Text"
                                                                class="altLongBlueButtonFormat" onclick="fnTransferPage()" />
                                                        </td>
                                                        <td width="1%">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <%--FB 2154--%>
                                                        <td align="right" height="21" style="font-weight: bold" width="2%">
                                                        </td>
                                                        <td colspan="2" valign="top" style="font-weight: bold" class="blackblodtext">
                                                            domaine e-mail
                                                        </td>
                                                        <td valign="top">
                                                            <asp:Button ID="btnEmailDomain" runat="server" Style="width: 275px;" Text="G�rer un domaine e-mail"
                                                                OnClick="EditEmaiDomain" class="altLongBlueButtonFormat" />
                                                        </td>
                                                    </tr>
                                                    <tr style="height: 10px">
                                                        <td colspan="5">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 15%" align="left" class="subtitleblueblodtext" valign="top" colspan="5">
                                                            options de facturation
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="2%">
                                                        </td>
                                                        <td align="left" height="21" style="font-weight: bold; width: 32%" class="blackblodtext"
                                                            colspan="2">
                                                            Principe de Facturation
                                                        </td>
                                                        <td style="height: 21px;">
                                                            <asp:DropDownList ID="lstBillingScheme" runat="server" CssClass="altSelectFormat">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td width="1%">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="2%">
                                                        </td>
                                                        <td align="left" height="21" style="font-weight: bold; width: 30%" class="blackblodtext"
                                                            colspan="2">
                                                            Autoriser sur allocation
                                                        </td>
                                                        <td style="height: 21px;">
                                                            <asp:CheckBox ID="chkAllowOver" runat="server" />
                                                        </td>
                                                        <td width="1%">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="2%">
                                                        </td>
                                                        <td align="left" height="21" style="font-weight: bold; width: 45%" class="blackblodtext"
                                                            colspan="2">
                                                            <%if (Application["Client"].ToString().ToUpper() == "MOJ")
                              {%>Allow Billing on Point-<br />
                                                            to-point Hearing<%}
                              else
                              { %>Permettre la facturation sur Point-to-point Conf�rences<%}%><%--added for FB 1428 Start--%>
                                                        </td>
                                                        <td style="height: 21px;">
                                                            <asp:CheckBox ID="chkP2P" runat="server" />
                                                        </td>
                                                        <td width="1%">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <%--FB 2045 Start--%>
                                                        <td width="2%">
                                                        </td>
                                                        <td align="left" height="21" style="font-weight: bold; width: 30%" class="blackblodtext"
                                                            colspan="2">
                                                            code de l'entit�
                                                        </td>
                                                        <td style="height: 21px;">
                                                            <asp:Button ID="btnEntityCode" runat="server" Style="width: 270px;" Text="G�rer code de l'entit�"
                                                                class="altLongBlueButtonFormat" OnClick="bntEntityCode" />
                                                        </td>
                                                        <td width="1%">
                                                        </td>
                                                    </tr>
                                                    <%--FB 2045 End--%>
                                                    <%--FB 1830 starts--%>
                                                    <tr>
                                                        <td style="width: 15%" align="left" class="subtitleblueblodtext" valign="top" colspan="5">
                                                            Les param�tres de langue
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="2%">
                                                        </td>
                                                        <td align="left" height="21" style="font-weight: bold; width: 32%" class="blackblodtext"
                                                            colspan="2">
                                                            Langue pr�f�r�e
                                                        </td>
                                                        <td style="height: 21px;">
                                                            <asp:DropDownList ID="drporglang" runat="server" CssClass="altSelectFormat" DataTextField="name"
                                                                DataValueField="ID">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td width="1%">
                                                        </td>
                                                    </tr>
                                                    <%--FB 1830 ends--%>
                                                    <tr>
                                                        <td align="left" class="subtitleblueblodtext" colspan="5">
                                                            Options Personalis�es
                                                        </td>
                                                    </tr>
                                                    <%if (!(Application["Client"].ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.MOJ)))
                      {%>
                                                    <%--Added for FB 1425 MOJ--%>
                                                    <tr>
                                                        <td width="2%">
                                                        </td>
                                                        <td align="left" height="21" style="font-weight: bold; width: 32%;" class="blackblodtext"
                                                            colspan="2">
                                                            Permettre les options personalis�es ?
                                                        </td>
                                                        <td style="height: 21px;">
                                                            <asp:DropDownList ID="CustomAttributeDrop" runat="server" Style="width: 30%;" CssClass="altSelectFormat">
                                                                <asp:ListItem Value="0">Non</asp:ListItem>
                                                                <asp:ListItem Selected="True" Value="1">Oui</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td width="1%">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <%if (!(Session["EnableEntity"].ToString().Equals("0")))
                      {%>
                                                        <td colspan="3">
                                                        </td>
                                                        <td style="text-align: left" width="12%" colspan="2">
                                                            <br />
                                                            <input type="button" name="btnCustomAttribute" style="width: 270px;" value="G�rer les options de personalisation"
                                                                class="altLongBlueButtonFormat" onclick="javascript:OpenEntityCode();" />&nbsp;
                                                        </td>
                                                        <%} %>
                                                        <%-- Code added for 1718 --%>
                                                        <%else
{ %>
                                                        <td style="text-align: left;" colspan="5">
                                                        </td>
                                                        <%} %>
                                                    </tr>
                                                    <%--code added for custom attribute end --%>
                                                    <%} %><%--Added for FB 1425 MOJ--%>
                                                    <%-- FB 2486 Start--%>
                                                    <tr>
                                                        <td align="left" class="subtitleblueblodtext" colspan="3" >
                                                            Message Delivery active
                                                        </td>
                                                        <td style="height: 21px;" valign="bottom">
                                                            <input type="button" name="btnManageMsg" value="G�rer les messages" class="altLongBlueButtonFormat"
                                                                onclick="javascript:OpenManageMsg();" />&nbsp;
                                                        </td>
                                                    </tr>
                                                    <%-- FB 2486 End--%>
                                                    <%--FB 2052--%>
                                                    <%if (!(Session["isSpecialRecur"].ToString().Equals("0")))
                      {%>
                                                    <%--FB 2343 Start--%>
                                                    <tr>
                                                        <td align="left" class="subtitleblueblodtext" colspan="3">
                                                            Journ�e de couleur
                                                        </td>
                                                        <td style="height: 21px;">
                                                            <input type="button" name="btnManageDayColor" style="width: 270px;" value="g�rer les couleurs�jours"
                                                                class="altLongBlueButtonFormat" onclick="javascript:OpenDayColor();" />&nbsp;
                                                        </td>
                                                    </tr>
                                                    <%} %>
                                                    
                                                    <%--FB 2343--%>
                                                    <%--FB 2262 //FB 2599 Starts--%>
                                                    <tr id="trCloud" runat="server" visible="false">
                                                        <td align="left" class="subtitleblueblodtext" colspan="3">
                                                            Cloud
                                                        </td>
                                                        <td style="height: 21px;">
                                                        <asp:Button ID="btnCloudImport" runat="server" Text="Import" CssClass="altShortBlueButtonFormat" OnClick="CloudImport" /> <%--FB 2262T--%>
                                                            <%--<input type="button" name="btnCloudImport" runat="server" value="Import" class="altLongBlueButtonFormat" onclick="CloudImport()" />--%>
                                                        </td>
                                                    </tr>
                                                    <%--FB 2262 //FB 2599 Ends--%>  
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="100%" border="0">
                            <tr style="display: none;">
                                <td align="left" class="subtitleblueblodtext" colspan="3">
                                    Arrangements d'interface de l'utilisateur
                                </td>
                            </tr>
                            <tr style="display: none;">
                                <%--Window Dressing--%>
                                <td align="left" height="21" style="font-weight: bold; width: auto" class="blackblodtext">
                                    Arrangements de conception de UI
                                </td>
                                <td style="height: 21px; font-weight: bold" width="30%" colspan="3">
                                    <asp:Button ID="btnChangeUIDesign" runat="server" Style="width: 270px;" CssClass="altLongBlueButtonFormat"
                                        OnClick="btnChangeUIDesign_Click" Text="Changer la conception de la presentation" />
                                </td>
                                <td align="right" style="height: 21px; width: 15%;">
                                </td>
                                <td style="height: 21px;" width="35%">
                                </td>
                            </tr>
                            <%-- Code Added for FB 1428--%>
                            <tr style="display: none;">
                                <%--Window Dressing--%>
                                <td align="left" height="21" style="font-weight: bold" class="blackblodtext" style="width: 18%">
                                    Arrangements de text UI
                                </td>
                                <td style="height: 21px; font-weight: bold" width="30%">
                                    <input type="button" id="btnUITextChange" style="width: 300px;" value="Changer le texte du module to presentation"
                                        class="altLongBlueButtonFormat" onclick="fnTransferPage()" />
                                </td>
                                <td align="right" style="height: 21px; width: 15%;">
                                </td>
                                <td style="height: 21px;" width="35%">
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <%--Mail Logo and Footer message--%>
                        <table width="100%" style="height: 21%" border="0">
                            <%--FB 1982 --%>
                            <%--TD Width Updated for FB 2050--%>
                            <tr>
                                <td width="100%" align="left" style="font-weight: bold" valign="top" class="subtitleblueblodtext"
                                    colspan="5">
                                    arrangement de e-mail
                                </td>
                                <%--FB 1982 --%>
                            </tr>
                            <tr>
                                <td style="width: 3%">
                                </td>
                                <td style="font-weight: bold; width: 22%" align="left" class="blackblodtext">
                                    logo de e-mail
                                </td>
                                <%--FB 1982--%>
                                <td style="width: 75%" colspan="5" valign="top" align="left">
                                    <%--FB 1982 --%>
                                    <table style="width: 100%" border="0">
                                        <tr>
                                            <td>
                                                <input type="file" id="fleMap1" contenteditable="false" enableviewstate="true" size="50"
                                                    class="altText" runat="server" />
                                                <asp:Button ID="btnUploadImages" OnClick="UploadMailLogoImages" runat="server" Text="Chargement des images"
                                                    CssClass="altLongBlueButtonFormat" />
                                                <cc1:ImageControl ID="Map1ImageCtrl" Width="30" Height="30" Visible="false" runat="server">
                                                </cc1:ImageControl>
                                                <asp:Label ID="lblUploadMap1" Text="" Visible="false" runat="server"></asp:Label>
                                                <asp:Button ID="btnRemoveMap1" CssClass="altShortBlueButtonFormat" Text="Retirer"
                                                    Visible="false" runat="server" OnCommand="RemoveFile" CommandArgument="1" />
                                                <asp:Label ID="hdnUploadMap1" Text="" Visible="false" runat="server"></asp:Label>
                                                <input type="hidden" id="Map1ImageDt" name="Map1ImageDt" runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <%--code added for FB 1710 start--%>
                            <tr>
                                <td align="left" colspan="5" height="21">
                                </td>
                            </tr>
                            <tr>
                                <td height="21%">
                                </td>
                                <td align="left" valign="top" class="blackblodtext">
                                    Note de bas de Page
                                </td>
                                <%--FB 1982 --%>
                                <td colspan="3" height="21" align="left" valign="top" class="blackblodtext">
                                    <dxHE:ASPxHtmlEditor ID="dxHTMLEditor" runat="server" Height="200px">
                                        <SettingsImageUpload UploadImageFolder="~/image/maillogo/">
                                            <ValidationSettings MaxFileSize="100000" MaxFileSizeErrorText="Footer Image attachment is greater than 100KB. File has not been uploaded" />
                                        </SettingsImageUpload>
                                    </dxHE:ASPxHtmlEditor>
                                    <input type="file" id="fmMap" contenteditable="false" size="50" class="altText" runat="server"
                                        visible="false" />
                                    <input type="hidden" id="fmMapImage" name="Map1ImageDt" runat="server" height="21%"
                                        style="display: none" /><%--FB 1982 --%>
                                </td>
                            </tr>
                            <%--code added for FB 1710 end--%>
                            <%--FB 1758 Starts--%>
                            <tr>
                                <table  width="100%" border="0">
                                    <%--TD Width Updated for FB 2050--%>
                                    <tr>
                                        <%--FB 1982--%>
                                        <td colspan="1">
                                            <table border="0" width="100%">
                                                <td colspan="1" style="height: 15">
                                                    <%--FB 1982--%>
                                                </td>
                                            </table>
                                        </td>
                                    </tr>
                                    <%--FB 1982--%>
                                    <tr>
                                        <td height="18%" style="width: 3%">
                                        </td>
                                        <%--//FB 1830 Language  --%>
                                        <td align="left" style="width: 22%" class="blackblodtext">
                                            Test Identit� pour votre email
                                        </td>
                                        <%--//FB 1830 Language--%>
                                        <%--FB 1982--%>
                                        <td  align="left" colspan="2" class="style1">
                                            <asp:TextBox ID="txtTestEmailId" runat="server" CssClass="altText" Width="200px"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="reqvalid" runat="server" ControlToValidate="txtTestEmailId"
                                                ValidationGroup="TestEmail" ErrorMessage="Requis." Display="dynamic"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="regTestemail" runat="server" ControlToValidate="txtTestEmailId"
                                                ErrorMessage="Adresse email invalide" Display="dynamic" ValidationExpression="^[a-zA-Z()^][()^\w\.-]*[a-zA-Z0-9()^]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"></asp:RegularExpressionValidator>
                                            <asp:RegularExpressionValidator ID="reqvalid2" ControlToValidate="txtTestEmailId"
                                                Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ; ? | = ! ` , [ ] { } : # $ ~ e &#34; sont des characteres invalides."
                                                ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=:#$%&'~]*$"></asp:RegularExpressionValidator>
                                            <asp:Button ID="btntestmail" Text="Test votre e-mail" ValidationGroup="TestEmail"
                                                CssClass="altLongBlueButtonFormat" runat="server" OnClick="TestEmailConnection" />
                                        </td>
                                        <td ></td>
                                    </tr>
                                    <%--FB 1830 Starts--%>
                                    <tr>
                                        <td align="left" height="15" style="font-weight: bold" style="width: 2%">
                                            <%--//FB 1830 Language--%>
                                            <%--FB 1982--%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" height="21" style="font-weight: bold;width:3%;">
                                        </td>
                                        <%--FB 1982--%>
                                        <td align="left" class="blackblodtext" style="width: 22%">
                                            Langue des Emails
                                        </td>
                                        <%--FB 1982--%>
                                        <td align="left" height="21" tyle="width: 28%">
                                            <%--FB 1860 start--%>
                                            <%--FB 1982--%>
                                            <asp:Button ID="btnDefine" runat="server" Text="Personalisation" OnClick="DefineEmailLanguage"
                                                class="altShortBlueButtonFormat" />
                                            <%--FB 1982 FB 2104--%>
                                            <asp:TextBox ID="txtEmailLang" runat="server" ReadOnly="true" CssClass="altText"
                                                Visible="false"></asp:TextBox>&nbsp;<%--FB 1830 - DeleteEmailLang 2104--%>
                                            <asp:ImageButton ImageUrl="image/btn_delete.gif" runat="server" ID="delEmailLang"
                                                ToolTip="Supprimer�la langue�email" OnClick="DeleteEmailLangugage" OnClientClick="javascript:return fnDelEmailLan()" />
                                            <%--FB 1830 - DeleteEmailLang--%>
                                        </td>
                                        <td align="left" class="blackblodtext" style="width: 18%">
                                            <%-- FB 2050 --%>
                                            <asp:Label ID="LblBlockEmails" runat="server" Text="Bloquage des Emails" nowrap></asp:Label> &nbsp;                                        
                                            <%--FB 1982--%>
                                            <asp:CheckBox ID="ChkBlockEmails" runat="server" />
                                        </td>
                                        <td align="left" width="29%" valign="top">
                                            <%-- FB 2050 --%>
                                            <asp:Button ID="BtnBlockEmails" Style="display: none;" runat="server" Text="Editer"
                                                OnClick="EditBlockEmails" class="altShortBlueButtonFormat" />
                                            <%--FB 1982--%><%--FB 2164--%>
                                        </td>
                                    </tr>
                                </table>
                                <%--FB 1982--%>
                    </td>
                    <%--FB 1860 end--%>
                </tr>
            </table>
            <%--FB 1982 end--%>
            <%--FB 1830 Ends--%>
            <%--FB 1758 Ends--%>
            <%--Window Dressing end--%>
            <tr>
                <%--FB 2337--%>
                <td colspan="5">
                    <table width="100%" style="height: 21%" border="0" cellpadding="0" cellspacing="0"> <%--FB 2555--%>
                        <tr>
                            <td align="left" height="21" style="font-weight: bold; width: 3%">
                            </td>
                            <td align="left" class="blackblodtext" style="width: 22%">
                                Contrat de License
                            </td>
                            <td align="left" height="21" width="28%"> <%--FB 2555--%>
                                <asp:Button ID="btnCustLicAgrmnt" runat="server" Text="Personalisation" class="altShortBlueButtonFormat"
                                    OnClick="CustomizeLicenseAgreement" />
                            </td>
                            <%--FB 2555 Starts--%>
                            <td align="left" class="blackblodtext" style="width: 18%">Format de date Email
                            </td>
                            <td align="left" height="21" style="width: 29%">
                            <asp:DropDownList ID="drpEmailDateFormat" runat="server" CssClass="altSelectFormat" style="width: 55%">
                                <asp:ListItem Value="0">pr�f�rences de l'utilisateur</asp:ListItem>
                                <asp:ListItem Value="1">Europ�enne (JJ MMM AAAA)</asp:ListItem></asp:DropDownList>
                            </td> 
                            <%--FB 2555 Ends--%> 
                        </tr>
                    </table>
                </td>
            </tr>
            <%if ((Application["External"].ToString() != ""))
              {%>
            <tr>
                <td>
                    <table width="100%" style="height: 21%" border="0">
                        <tr>
                            <td width="100%" align="left" style="font-weight: bold" valign="top" class="subtitleblueblodtext"
                                colspan="5">
                                Les param�tres de planification externes
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 3%">
                            </td>
                            <td class="blackblodtext" style="width: 22%">
                                Nom du client
                            </td>
                            <td style="width: 28%">
                                <asp:TextBox ID="txtCustomerName" runat="server" CssClass="altText" Width="200px"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator12" ControlToValidate="txtCustomerName"
                                    ValidationGroup="submit" Display="dynamic" runat="server" SetFocusOnError="true"
                                    ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ et &#34; sont des characteres invalides."
                                    ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()']*$"></asp:RegularExpressionValidator>
                            </td>
                            <td class="blackblodtext" style="width: 18%">
                                Num�ro de client
                            </td>
                            <td>
                                <asp:TextBox ID="txtCustomerID" runat="server" CssClass="altText" Width="200px"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator13" ControlToValidate="txtCustomerID"
                                    ValidationGroup="submit" Display="dynamic" runat="server" SetFocusOnError="true"
                                    ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ et &#34; sont des characteres invalides."
                                    ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()']*$"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <%} %>
            <%--FB 2262 //FB 2599 start--%>
            <tr id="trCloudDetails" runat="server" visible="false">
                <td>
                    <table width="100%" style="height: 21%" border="0">
                        <tr>
                            <td width="100%" align="left" style="font-weight: bold" valign="top" class="subtitleblueblodtext"
                                colspan="5">
                                Cloud
                            </td>
                        </tr>     
                        <tr>
                            <td style="width: 3%"></td>
                            <td class="blackblodtext" style="width: 22%">
                                Vidyo URL
                            </td>
                            <td style="width: 28%">
                                <asp:TextBox ID="txtvidyoURL" runat="server" CssClass="altText" TextMode="MultiLine" MaxLength="150"
                                    Width="200px"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator11" ControlToValidate="txtvidyoURL"
                                    ValidationGroup="submit" Display="dynamic" runat="server" SetFocusOnError="true"
                                    ErrorMessage="<br>& < > ' + % \ ( ) ; ? | ^ = ! ` , [ ] { } # $ @ ~ and &#34; are invalid characters."
                                    ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>+;?|!`,\[\]{}\x22;=^@#$%&()']*$"></asp:RegularExpressionValidator>
                            </td>
                            <td class="blackblodtext" style="width: 18%">
                                Login
                            </td>
                            <td>
                                <asp:TextBox ID="txtvidyoLogin" runat="server" CssClass="altText" Rows="3" MaxLength="30"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator16" ControlToValidate="txtvidyoLogin"
                                    ValidationGroup="submit" Display="dynamic" runat="server" SetFocusOnError="true"
                                    ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ and &#34; are invalid characters."
                                    ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()']*$"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 3%"></td>
                            <td style="width: 22%" class="blackblodtext">
                                Password
                            </td>
                            <td style="width: 28%">
                                <asp:TextBox ID="txtvidyoPassword1" onchange="javascript:PreservePassword()" runat="server" MaxLength="30"
                                    CssClass="altText" TextMode="Password"></asp:TextBox><asp:CompareValidator ID="CompareValidator2"
                                    runat="server" ControlToValidate="txtvidyoPassword1" ControlToCompare="txtvidyoPassword2"
                                    ErrorMessage="Passwords do not match." Font-Names="Verdana" Font-Size="X-Small"
                                    Font-Bold="False" Display="Dynamic" />
                            </td>
                            <td style="width: 18%" class="blackblodtext">
                                Retype Password
                            </td>
                            <td style="height: 37px;" width="35%">
                                <asp:TextBox ID="txtvidyoPassword2" runat="server" onchange="javascript:PreservePassword()" MaxLength="30"
                                    CssClass="altText" TextMode="Password"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 3%"></td>
                            <td class="blackblodtext" style="width: 22%">
                                Proxy Address
                            </td>
                            <td style="width: 28%">
                                <asp:TextBox ID="txtproxyAdd" runat="server" CssClass="altText"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="regTimeoutVlaue" runat="server" ControlToValidate="txtproxyAdd" ValidationGroup="submit" Display="Dynamic" ErrorMessage="Invalid IP Address" ValidationExpression="^[^&<>+'dD][0-9'.]*$"></asp:RegularExpressionValidator>
                            </td>
                            <td class="blackblodtext" style="width: 18%">
                                Port
                            </td>
                            <td>
                                <asp:TextBox ID="txtvidyoPort" runat="server" CssClass="altText" Rows="3" ></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txtvidyoPort"
                                    ValidationGroup="submit" Display="dynamic" runat="server" SetFocusOnError="true"
                                    ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ and &#34; are invalid characters."
                                    ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()']*$"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <%--<tr>
                            <td style="width: 3%"></td>
                            <td class="blackblodtext" style="width: 22%">
                                Poll
                            </td>
                            <td style="width: 28%">
                            <%--<input type="button" id="btnvidyoPoll" runat="server" name="btnvidyoPoll" value="Now" class="altShortBlueButtonFormat" >--%>
                                <%--<asp:Button ID="btnvidyoPoll"  runat="server" Text="Now" class="altShortBlueButtonFormat" />
                            </td>
                            <td colspan="2" ></td>
                        </tr>--%>   
                    </table>
                </td>
            </tr>
            <%--FB 2262 //FB 2599 End--%>
            <%--<tr>  Commented this, because not deliver for this Phase II delivery
                <td>
                    <table width="100%" style="height: 21%" border="0">
                        <tr>
                            <td width="100%" align="left" style="font-weight: bold" valign="top" class="subtitleblueblodtext"
                                colspan="5">
                                <span class="subtitleblueblodtext">Param�tres de diffusion de rapport d'erreur</span>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 3%">
                            </td>                            
                            <td class="blackblodtext" style="width: 22%">
                                Email du destinataire 
                            </td>
                            <td style="width: 28%">
                                <asp:TextBox ID="txtUsrRptDestination" runat="server" CssClass="altText" Width="200px"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegtxtUsrRptDestination" ControlToValidate="txtUsrRptDestination"
                                    ValidationGroup="submit" Display="dynamic" runat="server" SetFocusOnError="true"
                                    ErrorMessage="<br>Adresse email invalide." ValidationExpression="[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"></asp:RegularExpressionValidator>
                            </td>                        
                            <td class="blackblodtext" style="width: 18%">
                                fr�quence
                            </td>
                            <td >
                                <asp:TextBox ID="lstUsrRptFrequencyCount" runat="server" CssClass="altText" Width="70px"></asp:TextBox>
                                (Mins)
                            </td>                            
                        </tr>                        
                    </table>
                </td>
            </tr>--%>
            <tr>
                <td align="center" colspan="5" style="font-weight: bold; font-size: small; color: black;
                    font-family: verdana; height: 21%;">
                    <input id="btnReset" type="reset" value="Remise � zero" class="altShortBlueButtonFormat"
                        onclick="javascript:fnReset();" />&nbsp;
                    <asp:Button runat="server" ID="btnSubmit" CssClass="altShortBlueButtonFormat" OnClick="btnSubmit_Click"
                        Text="Soumettre"></asp:Button>
                </td>
            </tr>
            <input align="center" type="hidden" name="formname" id="formname" value="frmMainsuperadministrator" />
            <img src="keepalive.asp" name="myPic" width="1" height="1">
            <tr>
                <%--FB 1849 Start--%>
                <td colspan="5" align="center">
                    <ajax:ModalPopupExtender ID="RoomPopUp" runat="server" TargetControlID="ChgOrg" BackgroundCssClass="modalBackground"
                        PopupControlID="switchOrgPnl" DropShadow="false" Drag="true" CancelControlID="ClosePUp">
                    </ajax:ModalPopupExtender>
                    <asp:Panel ID="switchOrgPnl" runat="server" HorizontalAlign="Center" Width="30%"
                        CssClass="treeSelectedNode">
                        <table width="100%" align="center" border="0">
                            <tr>
                                <td align="center" class="blackblodtext">
                                    <span class="subtitleblueblodtext">Switch Organization</span><br />
                                    <p>
                                        Toutes les transactions effectu�es seront pour l'organisation ci-dessous s�lectionn�
                                        apr�s l'interrupteur.
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:DropDownList ID="DrpOrganization" DataTextField="OrganizationName" DataValueField="OrgId"
                                        runat="server" CssClass="altLong0SelectFormat" Width="200px">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button runat="server" ID="BtnChangeOrganization" CssClass="altShortBlueButtonFormat"
                                        Text="Soumettre" OnClientClick="javascript:return fnChangeOrganization();" OnClick="btnChgOrg_Click">
                                    </asp:Button>
                                    <input align="middle" type="button" runat="server" style="width: 100px; height: 21px;"
                                        id="ClosePUp" value=" Ferm�e " class="altButtonFormat" onclick="javascript:fnClearOrg();" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
            <%--FB 1849 End--%>
        </td>
    </tr>
    <%--FB 1982--%>

    <script language="javascript" type="text/javascript">

// FB 1710 Alignment change  Ends
//Code added fro FB 1428 - CSS Project
    function fnTransferPage()
    {
        window.location.replace("UITextChange.aspx");
    }
//FB 1830 - DeleteEmailLang start
  function fnDelEmailLan()
  {
    if (document.getElementById("txtEmailLang").value == "")
        return false;
    else
        return true;
  }
//FB 1830 - DeleteEmailLang end
//FB 1849
    function fnClearOrg()
    {
        var obj1 = document.getElementById('DrpOrganization');
        if(obj1)
        {
            obj1.value = '<%=orgId%>';
        }        
    }
    
    </script>

    </form>
</body>
</html>
<%--code added for Soft Edge button--%>

<script type="text/javascript" src="inc/softedge.js"></script>

<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
<!-- FB 2050 Start -->

<script type="text/javascript">

function scrollWindow()
{
    document.getElementById("Button1").focus();
    refreshStyle();
    window.scrollTo(0,0);
}

function refreshImage()
{
  //setTimeout("scrollWindow()",500);
  var obj = document.getElementById("mainTop");
  if(obj != null)
  {
      var src = obj.src;
      var pos = src.indexOf('?');
      if (pos >= 0) {
         src = src.substr(0, pos);
      }
      var date = new Date();
      obj.src = src + '?v=' + date.getTime();
      
      if(obj.width > 804)
      obj.setAttribute('width','804');
  }
  //refreshStyle();
  setMarqueeWidth();
  return false;
}

function refreshStyle()
{
	var i,a,s;
	a=document.getElementsByTagName('link');
	for(i=0;i<a.length;i++) {
		s=a[i];
		if(s.rel.toLowerCase().indexOf('stylesheet')>=0&&s.href) {
			var h=s.href.replace(/(&|\\?)forceReload=d /,'');
			s.href=h+(h.indexOf('?')>=0?'&':'?')+'forceReload='+(new Date().valueOf());
		}
	}
}

function setMarqueeWidth()
{
    var screenWidth = screen.width - 25;
    if(document.getElementById('martickerDiv')!=null)
        document.getElementById('martickerDiv').style.width = screenWidth + 'px';
        
    if(document.getElementById('marticDiv')!=null)
        document.getElementById('marticDiv').style.width = screenWidth + 'px';
    
    if(document.getElementById('marticker2Div')!=null)
        document.getElementById('marticker2Div').style.width = (screenWidth-15) + 'px';
    
    if(document.getElementById('martic2Div')!=null)
        document.getElementById('martic2Div').style.width = (screenWidth-15) + 'px';
}

window.onload = refreshImage;


</script>

<!-- FB 2050 End -->

<%@ Page Language="C#" Inherits="ns_MyVRM.Users" Buffer="true" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <!-- FB 2050 -->
<meta http-equiv="X-UA-Compatible" content="IE=7" /> <!-- FB 2050 -->

<!--window Dressing-->
<!-- #INCLUDE FILE="inc/maintopNET.aspx" --> 

<script runat="server">

</script>
<script type="text/javascript" src="script/mousepos.js"></script>
<script type="text/javascript" src="script/managemcuorder.js"></script>

<script language="javascript">

	function ManageOrder ()
	{
		change_mcu_order_prompt('image/pen.gif', 'G�rer lordre des MCUs', document.frmManagebridge.Bridges.value); //FB 2164
		
//		frmsubmit('Sauver', '');
	}
	
	function frmsubmit()
	{
	    document.getElementById("__EVENTTARGET").value="ManageOrder";
	    document.frmManagebridge.submit();
	}
	//Added for FB 1405 -Start 
	
	function fnSearch()
	{

	    var obj = document.getElementById("hdnAction");
	    obj.value = 'Y';
	    return true;
   
	}
	
	//Added for FB 1405 -End 
</script>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf8" />
    <title>G�rer Users</title>
    <script type="text/javascript" src="inc/functions.js"></script>

</head>
<body>
    <form id="frmManagebridge" runat="server" method="post" onsubmit="return true">
    <div>
      <input type="hidden" id="helpPage" value="65">
        <input type="hidden" id="hdnAction" runat="server"> <%--Added for FB 1405--%>
        <table width="100%">
            <tr>
                <td align="center">
                    <h3>
                        <asp:Label ID="lblHeader" runat="server"></asp:Label>
                    </h3>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="Left">
                    <table width="100%">
                        <tr>
                            <td >&nbsp;</td>
                            <td>
                                <SPAN class=subtitleblueblodtext>Utilisateur existant</SPAN>
                            </td>
                        </tr>
                        <tr>
                            <td width="20"></td>
                            <td align="center">
                                <table width="90%">
                                    <tr>
                                        <td align="right">
                                            <asp:DropDownList ID="lstSortBy" CssClass="alt2SelectFormat" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ChangeAlphabets">
                                                <asp:ListItem Value="1" Text="Pr�nom"></asp:ListItem>
                                                <asp:ListItem Value="2" Text="Dernier Nom"></asp:ListItem>
                                                <asp:ListItem Value="3" Text="Ouverture de session"></asp:ListItem>
                                                <asp:ListItem Value="4" Text="Email"></asp:ListItem>
                                            </asp:DropDownList> 
                                            <%--Window Dressing--%>
                                            <span class="blackblodtext"> Commencer avec:</span>
                                        </td>
                                        <td align="left">
                                             <asp:Table runat="server"  ID="tblAlphabet" CellPadding="2" CellSpacing="5"></asp:Table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:DataGrid ID="dgUsers" runat="server" AutoGenerateColumns="False" CellPadding="4" GridLines="None"
                         BorderColor="blue" BorderStyle="solid" BorderWidth="1" ShowFooter="false" OnItemDataBound="BindRowsDeleteMessage"
                        OnDeleteCommand="DeleteUser" OnEditCommand="EditUser" OnCancelCommand="LockUser" OnUpdateCommand="RestoreUser" Width="90%" Visible="true" style="border-collapse:separate"> <%--Edited for FF--%>
                        <%--Window Dressing - Start--%>                        
                        <SelectedItemStyle CssClass="tableBody" />
                        <EditItemStyle CssClass="tableBody" />
                        <AlternatingItemStyle CssClass="tableBody" />
                        <ItemStyle CssClass="tableBody" />
                         <%--Window Dressing - End--%> 
                        <HeaderStyle CssClass="tableHeader" HorizontalAlign="left" />
                        <Columns>
                            <asp:BoundColumn DataField="userID" Visible="false" ><HeaderStyle CssClass="tableHeader" HorizontalAlign="left" /></asp:BoundColumn>
                            <asp:BoundColumn DataField="level" Visible="false" ><HeaderStyle CssClass="tableHeader" HorizontalAlign="left" /></asp:BoundColumn>
                            <asp:BoundColumn DataField="deleted" Visible="false" ><HeaderStyle CssClass="tableHeader" HorizontalAlign="left" /></asp:BoundColumn>
                            <asp:BoundColumn DataField="locked" Visible="false" ><HeaderStyle CssClass="tableHeader" HorizontalAlign="left" /></asp:BoundColumn>
                            <asp:BoundColumn DataField="firstName" ItemStyle-CssClass="tableBody"  HeaderStyle-CssClass="tableHeader" ItemStyle-HorizontalAlign="Left" HeaderText="Pr�nom"></asp:BoundColumn>
                            <asp:BoundColumn DataField="lastName" ItemStyle-CssClass="tableBody"  HeaderStyle-CssClass="tableHeader" ItemStyle-HorizontalAlign="Left" HeaderText="Surnom"></asp:BoundColumn>
                            <asp:BoundColumn DataField="login"  ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" ItemStyle-HorizontalAlign="Left" HeaderText="Ouverture de session"></asp:BoundColumn>
                            <asp:BoundColumn DataField="email" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" ItemStyle-HorizontalAlign="Left" HeaderText="Email"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="r�le" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <asp:Image ID="imgRole2" Width="25" Height="25" ImageUrl="image/superadmin.gif" Visible='<%# (DataBinder.Eval(Container, "DataItem.crossaccess").ToString().Trim().Equals("1") && DataBinder.Eval(Container, "DataItem.level").ToString().Trim().Equals("2"))%>' runat="server" />
                                    <asp:Image ID="imgRole1" Width="25" Height="25" ImageUrl="image/admin.gif" Visible='<%# (DataBinder.Eval(Container, "DataItem.crossaccess").ToString().Trim().Equals("0") && DataBinder.Eval(Container, "DataItem.level").ToString().Trim().Equals("2"))%>' runat="server" />
                                    <asp:Image ID="Image1" Width="25" Height="25" ImageUrl="image/admin.gif" Visible='<%# (DataBinder.Eval(Container, "DataItem.crossaccess").ToString().Trim().Equals("0") && DataBinder.Eval(Container, "DataItem.level").ToString().Trim().Equals("1"))%>' runat="server" />
                                    <asp:Image ID="imgRole0" Width="25" Height="25" ImageUrl="image/user.gif" Visible='<%# (DataBinder.Eval(Container, "DataItem.level").ToString().Trim().Equals("0") && DataBinder.Eval(Container, "DataItem.crossaccess").ToString().Trim().Equals("0"))%>' runat="server" />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Actions" ItemStyle-CssClass="tableBody" ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Left">
                                <HeaderStyle CssClass="tableHeader" HorizontalAlign="left" />
                                <ItemTemplate>
                                    <asp:LinkButton runat="server" Text="Editer" Visible='<%# Session["admin"].ToString().Trim().Equals("2") && txtType.Text.Equals("1") %>' Enabled='<%# !DataBinder.Eval(Container, "DataItem.userID").ToString().Trim().Equals("11") && !DataBinder.Eval(Container, "DataItem.userID").ToString().Trim().Equals(Session["userID"].ToString()) && (Session["UsrCrossAccess"].ToString().Trim().Equals("1") || !DataBinder.Eval(Container, "DataItem.crossaccess").ToString().Trim().Equals("1")) %>' ID="btnEdit" CommandName="Edit"></asp:LinkButton>
                                    <asp:LinkButton runat="server" Text="Supprimer" Visible='<%# (Session["admin"].ToString().Trim().Equals("2")) && (!txtType.Text.Equals("3")) %>' Enabled='<%# !DataBinder.Eval(Container, "DataItem.userID").ToString().Trim().Equals("11") && !DataBinder.Eval(Container, "DataItem.userID").ToString().Trim().Equals(Session["userID"].ToString()) && (Session["UsrCrossAccess"].ToString().Trim().Equals("1") || !DataBinder.Eval(Container, "DataItem.crossaccess").ToString().Trim().Equals("1")) %>' ID="btnDelete" CommandName="Delete"></asp:LinkButton>
                                    <asp:LinkButton runat="server" Text="Bloquer" Visible='<%# (Session["admin"].ToString().Trim().Equals("2") || Session["admin"].ToString().Trim().Equals("1"))  && txtType.Text.Equals("1") %>' Enabled='<%# !DataBinder.Eval(Container, "DataItem.userID").ToString().Trim().Equals("11") && !DataBinder.Eval(Container, "DataItem.userID").ToString().Trim().Equals(Session["userID"].ToString()) && (Session["UsrCrossAccess"].ToString().Trim().Equals("1") || !DataBinder.Eval(Container, "DataItem.crossaccess").ToString().Trim().Equals("1"))%>' ID="btnLock" CommandName="Cancel"></asp:LinkButton>
                                    <asp:LinkButton runat="server" Text="Supprimer" Visible='<%# (txtType.Text.Equals("3") && (Application["ssoMode"].ToString().ToUpper().Equals("NO"))) %>' CommandName="Update" CommandArgument="1" ID="btnDeleteInactive"></asp:LinkButton>
<%--                                <asp:LinkButton runat="server" Text="Supprimer" Visible='<%# (txtType.Text.Equals("2")) %>' CommandName="Mise � jours" CommandArgument="9" ID="btnDeleteGuest"></asp:LinkButton>
--%>                                <asp:LinkButton runat="server" Text="Restaurer" Visible='<%# txtType.Text.Equals("3") %>' CommandName="Update" CommandArgument="2" ID="btnReset"></asp:LinkButton> <%--FB 2164--%>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="crossaccess" Visible="false" ><HeaderStyle CssClass="tableHeader" HorizontalAlign="left" /></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:Table runat="server" ID="tblNoUsers" Visible="false" Width="90%">
                        <asp:TableRow CssClass="lblError">
                            <%--Windows Dressing--%>
                            <asp:TableCell CssClass="lblError" HorizontalAlign="center">
                                Aucun utilisateur trouv�.  <%--Edited for FB 1405--%>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>                    
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table width="90%" cellspacing="0" cellpadding="0" border="0">
                        <tr>
                            <%--Windows Dressing--%>
                            <td width="50%" valign="middle" id="tdLegends" runat="server" class="blackblodtext">
                                <b>L�gende:</b>
                                <font size="1" color="blue">
                                    <asp:Image ID="imgRole2" Width="25" Height="25" ImageUrl="image/superadmin.gif" runat="server" /> Administrateur du site
                                    <asp:Image ID="imgRole1" Width="25" Height="25" ImageUrl="image/admin.gif" runat="server" /> Administrateur
                                    <asp:Image ID="imgRole0" Width="25" Height="25" ImageUrl="image/user.gif" runat="server" /> Utilisateur
                                </font>
                            </td>
                            <td align="right" width="25%">
                                <asp:Button ID="btnDeleteAllGuest" Text="Supprimer tous mes clients" runat="server" OnClick="DeleteAllGuest" CssClass="altLongBlueButtonFormat" />
                        <%--Windows Dressing--%>
                                <b class="blackblodtext">Nombre total d'utilisateurs: </b><b><asp:Label ID="lblTotalUsers" runat="server" Text=""></asp:Label> </b>
                            </td>
                            <td  align="right" width="25%" runat="server" id="tdLicencesRemaining">
                        <%--Windows Dressing--%>
				                <b class="blackblodtext">Licence disponible: </b><b><asp:Label ID="lblLicencesRemaining" runat="server" Text=""></asp:Label> </b>                                
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Table ID="tblPage" Visible="false" runat="server">
                        <asp:TableRow ID="TableRow1" runat="server">
                            <asp:TableCell ID="TableCell1" Font-Bold="True" Font-Names="Verdana" Font-Size="Small" ForeColor="Blue" runat="server"><span class="blackblodtext"> Pages:</span> </asp:TableCell>
                            <asp:TableCell ID="TableCell2" runat="server"></asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>

                </td>
            </tr>
            <tr id="trSearchH" runat="server">
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <span class=subtitleblueblodtext>Recherche d'utilisateurs</span>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trSearch" runat="server">
                <td align="center">
                    <table width="80%" cellpadding="2" cellspacing="5">
                        <tr>
                             <%--Window Dressing --%>                        
                            <td align="right" style="font-weight:bold" class="blackblodtext">Pr�nom</td>
                            <td align="left">
                                <asp:TextBox ID="txtFirstName" CssClass="altText" runat="server"></asp:TextBox>
                                 <asp:RegularExpressionValidator ID="regTemplateName" ControlToValidate="txtFirstName" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > + % \ / ? | ^ = ! `[ ] { } # $ et ~ sont des characteres invalides." ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>^+?|!`\[\]{}\=@#$%&~]*$"></asp:RegularExpressionValidator><%--FB 1888--%>
                            </td>
                            <%--Window Dressing --%>                        
                            <td align="right" style="font-weight:bold" class="blackblodtext">Dernier Nom</td>
                            <td align="left">
                                <asp:TextBox ID="txtLastName" CssClass="altText" runat="server"></asp:TextBox>
                                 <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txtLastName" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > + % \ / ? | ^ = ! `[ ] { } # $ et ~ sont des characteres invalides." ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>^+?|!`\[\]{}\=@#$%&~]*$"></asp:RegularExpressionValidator><%--FB 1888--%>
                            </td>
                            <%--Window Dressing --%>                        
                            <td align="right" style="font-weight:bold" class="blackblodtext">Adresse e-mail</td>
                            <td align="left">
                                <asp:TextBox ID="txtEmailAddress" CssClass="altText" runat="server"></asp:TextBox>
                                 <asp:RegularExpressionValidator ID="RegularExpressionValidator2" ControlToValidate="txtEmailAddress" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ ~ et &#34; sont des characteres invalides." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:#$%&()'~]*$"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trSearchB" runat="server">
                <td align="center">
                    <table width="90%">
                        <tr>
                            <td width="50%">&nbsp;</td>
                            <td>
                                <asp:Button ID="btnSearchUser" OnClientClick="javascript:return fnSearch();" OnClick="SearchUser" runat="server" CssClass="altLongBlueButtonFormat" Text="Soumettre" /> <%--Edited for FB 1405--%>
                            </td>
                            
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trNewH" runat="server">
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <span class=subtitleblueblodtext>Cr�er un nouveau utilisateur</span>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trNew" runat="server">
                <td align="center">
                    <table width="90%">
                        <tr>
                            <td width="50%">&nbsp;</td>
                            <td>
                                <asp:Button ID="btnNewMCU" OnClick="CreateNewUser" runat="server" CssClass="altLongBlueButtonFormat" Text="Soumettre" />
                            </td>
                            
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
<asp:TextBox ID="txtBridges" runat="server" Width="0" Height="0" ForeColor="transparent" BackColor="transparent" BorderColor="transparent"></asp:TextBox>
  <input type="hidden" name="Bridges" width="200">
  <asp:TextBox ID="txtType" Visible="false" CssClass="altText" runat="server" />

<img src="keepalive.asp" name="myPic" width="1px" height="1px">
    </form>
    <%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
    <!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
</body>
</html>



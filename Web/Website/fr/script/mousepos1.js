var mousedownX = 0;
var mousedownY = 0;
// If it is not IE, we assume that the browser is NS.
var IE = document.all ? true : false
// NS
if (!IE) document.captureEvents(Event.MOUSEMOVE);
// Set-up to use getMouseXY function onMouseMove
document.onMouseMove  = getMouseXY;
function getMouseXY(e) 
{
	// Set-up to use getMouseXY function onMouseMove
	var IE = document.all ? true : false;
	if (IE) {		// IE
		mousedownX = event.clientX + document.body.scrollLeft;
		mousedownY = event.clientY + document.body.scrollTop;
	} else {		// NS
		mousedownX = e.pageX
		mousedownY = e.pageY
	}  
	// catch possible negative values in NS4
	mousedownX = ( (mousedownX < 0) ? 0 : mousedownX );
	mousedownY = ( (mousedownY < 0) ? 0 : mousedownY );
	return true
}


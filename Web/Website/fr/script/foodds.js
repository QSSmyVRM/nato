// script for food data structure
function FoodItem(fid, name, imgSrc)
{
	this.fid = fid;
	this.name = name;
	this.icon = new Image();
	this.icon.src = imgSrc;
	this.ddobj = null;
	this.toString = foodToString;
}
function foodToString()
{
	return this.name;
}
function compareName(a, b)
{
	var aname = a.name.toUpperCase();
	var bname = b.name.toUpperCase();
	if (aname > bname) 
		return 1;
	else if (aname < bname)
		return -1;
	else 
		return 0;
}


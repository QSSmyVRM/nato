// Operations for Select.Options
var NS4 = (navigator.appName == "Netscape" && parseInt(navigator.appVersion, 10) < 5);
var NSX = (navigator.appName == "Netscape");
var IE4 = (document.all) ? true : false;
function addOption(select, optionText, optionValue)
{
	if (NSX)
	{
		addOptionNS(select, optionText, optionValue);
	}
	else if (IE4)
	{
		addOptionIE(select, optionText, optionValue);
	}
}
function addOptionNS(select, optionText, optionValue)
{
	var newOpt  = new Option(optionText, optionValue);
	var selLength = select.length;
	select.options[selLength] = newOpt;
	if (NS4) history.go(0);
}
function addOptionIE(select, optionText, optionValue)
{
	var newOpt = document.createElement("OPTION");
	newOpt.text=optionText;
	newOpt.value=optionValue;
	select.options.add(newOpt);
}
function emptyOptions(select)
{	
	if (NSX)
	{
		deleteOptionNS(select);
	}
	else if (IE4)
	{
		deleteOptionIE(select);
	}	
}
function emptyOptionsNS(select)
{
	var selLength = select.length;
	for (var i=0; i<selLength; i++) {
		select.options[selLength-1]=null;
		if (NS4) history.go(0);
	}
}
function emptyOptionsIE(select)
{
	var selLength = select.length;
	for (var i=0; i<selLength; i++)
	{
		select.remove(selLength-1);
	}
}


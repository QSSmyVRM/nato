var xmlHttp
function ajaxGetXML()
{
	var argv = ajaxGetXML.arguments;
	var argc = argv.length;
	var url = "";
	  
	if (argc < 2)
		return 0;
	else
		url = argv[0] + "?cmd=" + argv[1] + "&pno=" + (argc-2);
		
	  
	for (var i = 2; i < argc; i++)
		url += "&p" + (i-1) + "=" + argv[i];
	xmlHttp = GetXmlHttpObject(stateChanged0);
	xmlHttp.open("GET", url , true);
	xmlHttp.send(null);
} 
function stateChanged0() 
{
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete") {
		if (ajaxSetXML)
			ajaxSetXML (xmlHttp.responseText)
	} 
} 
function GetXmlHttpObject(handler)
{ 
	var objXmlHttp=null
	if (navigator.userAgent.indexOf("Opera") >= 0) {
		alert("This example doesn't work in Opera") 
		return; 
	}
	
	if (navigator.userAgent.indexOf("MSIE")>=0) {
		var strName="Msxml2.XMLHTTP"
		if (navigator.appVersion.indexOf("MSIE 5.5")>=0) {
			strName="Microsoft.XMLHTTP"
		} 
		try { 
			objXmlHttp=new ActiveXObject(strName)
			objXmlHttp.onreadystatechange=handler 
			return objXmlHttp
		} catch(e) { 
			alert("Error. Scripting for ActiveX might be disabled") 
			return 
		} 
	} 
	
	if (navigator.userAgent.indexOf("Mozilla") >= 0) {
		objXmlHttp=new XMLHttpRequest()
		objXmlHttp.onload=handler
		objXmlHttp.onerror=handler 
		return objXmlHttp
	}
} 


using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Xml;

namespace ns_DataImport
{
    public partial class DataImport : System.Web.UI.Page
    {
        String schemaPath = "C:\\VRMSchemas_V1.8.3\\";
        static DataTable dtMaster;
        //code added for BCS - start
        DataView dvZone;
        DataTable dtZone;
        myVRMNet.NETFunctions obj;
        DataSet dsUsers = new DataSet();
        DataView dvUsers;
        DataTable dtUsers = new DataTable();

        protected void Page_Load(object sender, EventArgs e)
        {

//            if (IsPostBack)
//                //tblDataLoading.Visible = false;
        }

        protected void ImportTier1s(object sender, EventArgs e)
        {
            try
            {
                DataTable dtTemp = new DataTable();
                dtTemp = dtMaster.DefaultView.ToTable(true, dtMaster.Columns["Tier1"].ColumnName);
                Tier1 objTier1 = new Tier1(1, dtTemp, Application["COM_ConfigPath"].ToString());
//                //tblDataLoading.Visible = true;
                int cnt = 0;
                objTier1.Process(ref cnt);
                //tblDataLoading.Visible = false;
                errLabel.Text = "Top Tier(s) are imported successfully!";
                btnImportTier1.Enabled = false;
            }
            catch (Exception ex)
            {
                errLabel.Text = ex.StackTrace + "<br>" + ex.Message;
            }
        }
        protected void GenerateDataTable(Object sender, EventArgs e)
        {
            try
            {
                dtMaster = CreateDataTableFromCSV();
                if (dtMaster != null)
                    errLabel.Text = "Data Table has been generated successfully!";
            }
            catch (Exception ex)
            {
                errLabel.Text = ex.StackTrace + "<br>" + ex.Message;
            }
        }
        protected void ImportTier2s(object sender, EventArgs e)
        {
            try
            {
                DataTable dtTemp = new DataTable();
                dtTemp = dtMaster.DefaultView.ToTable(true, new string[] { dtMaster.Columns["Tier1"].ColumnName, dtMaster.Columns["Tier2"].ColumnName } );
                Tier2 objTier2 = new Tier2(1, dtTemp, Application["COM_ConfigPath"].ToString());
                //tblDataLoading.Visible = true;
                int cnt = 0;
                objTier2.Process(ref cnt);
                //tblDataLoading.Visible = false;
                errLabel.Text = cnt.ToString() + " Middle Tier(s) are imported successfully!";
                btnImportTier2.Enabled = false;
            }
            catch (Exception ex)
            {
                errLabel.Text = ex.StackTrace + "<br>" + ex.Message;
            }
        }
        protected void ImportDepartments(object sender, EventArgs e)
        {
            try
            {
                DataTable dtTemp = new DataTable();
                dtTemp = dtMaster.DefaultView.ToTable(true, new string[] { dtMaster.Columns["DepartmentID"].ColumnName, dtMaster.Columns["DepartmentName"].ColumnName });
                Department objDepartment = new Department(1, dtTemp, Application["COM_ConfigPath"].ToString());
                //tblDataLoading.Visible = true;
                int cnt = 0;
                objDepartment.Process(ref cnt);
                //tblDataLoading.Visible = false;
                errLabel.Text = cnt.ToString() + " Department(s) are imported successfully!";
                btnImportDepartment.Enabled = false;
            }
            catch (Exception ex)
            {
                errLabel.Text = ex.StackTrace + "<br>" + ex.Message;
            }
        }
        protected void ImportRooms(object sender, EventArgs e)
        {
            try
            {

                DataTable dtTemp = new DataTable();
                dtTemp = dtMaster.DefaultView.ToTable(true, new string[] { dtMaster.Columns["Room"].ColumnName, dtMaster.Columns["Tier1"].ColumnName, dtMaster.Columns["Tier2"].ColumnName, dtMaster.Columns["DepartmentName"].ColumnName,dtMaster.Columns["TimeZone"].ColumnName });
                Room objRoom = new Room(1, dtTemp, Application["COM_ConfigPath"].ToString());
                int cnt = 0;
                objRoom.Process(ref cnt);
                //tblDataLoading.Visible = false;
                errLabel.Text = cnt.ToString() + " Room(s) are imported successfully!";
                btnImportRooms.Enabled = false;
            }
            catch (Exception ex)
            {
                errLabel.Text = ex.StackTrace + "<br>" + ex.Message;
            }
        }
        protected void ImportUsers(object sender, EventArgs e)
        {
            try
            {
                DataTable dtTemp = new DataTable();
                dtMaster.DefaultView.Sort = "UserEmail ASC";
                dtTemp = dtMaster.DefaultView.ToTable(true, new string[] { dtMaster.Columns["DepartmentName"].ColumnName, dtMaster.Columns["UserEmail"].ColumnName, dtMaster.Columns["UserFirstName"].ColumnName, dtMaster.Columns["UserLastName"].ColumnName });
                User objUser = new User(1, dtTemp, Application["COM_ConfigPath"].ToString());
                
                //int i = 0;
                //foreach (DataRow dr in dtTemp.Rows)
                //{
                //    i++;
                //    Response.Write("<br>" + i + dr["UserEmail"] + " : " + dr["DepartmentName"]);
                //}
                //tblDataLoading.Visible = true;
                int cnt = 0;
                objUser.Process(ref cnt);
                //tblDataLoading.Visible = false;
                errLabel.Text = cnt.ToString() + " User(s) are imported successfully!";
                btnImportUsers.Enabled = false;
            }
            catch (Exception ex)
            {
                errLabel.Text = ex.StackTrace + "<br>" + ex.Message;
            }
        }
        protected void ImportEndpoints(object sender, EventArgs e)
        {
            try
            {
                DataTable dtTemp = new DataTable();
                dtTemp = dtMaster.DefaultView.ToTable(true, new string[] { dtMaster.Columns["Room"].ColumnName }); //, dtMaster.Columns["Tier1"].ColumnName, dtMaster.Columns["Tier2"].ColumnName });
                Endpoint objEndpoint = new Endpoint(1, dtTemp, Application["MyVRMServer_ConfigPath"].ToString());
                int cnt = 0;
                objEndpoint.Process(ref cnt);
                //tblDataLoading.Visible = false;
                errLabel.Text = cnt.ToString() + " Endpoint(s) are imported successfully!";
                btnImportEndpoints.Enabled = false;
            }
            catch (Exception ex)
            {
                errLabel.Text = ex.StackTrace + "<br>" + ex.Message;
            }
        }
        protected void ImportConferences(object sender, EventArgs e)
        {
            try
            {
                DataTable dtTemp = new DataTable();
                dtMaster.DefaultView.Sort = "ConfirmationNumber, StartDateTime, Duration, Room";
                dtTemp = dtMaster.DefaultView.ToTable(); //true, new string[] { dtMaster.Columns["ConferenceName"].ColumnName, dtMaster.Columns["StartDateTime"].ColumnName, dtMaster.Columns["Duration"].ColumnName, dtMaster.Columns["Tier1"].ColumnName, dtMaster.Columns["Tier2"].ColumnName, dtMaster.Columns["Room"].ColumnName, dtMaster.Columns["UserFirstName"].ColumnName, dtMaster.Columns["UserLastName"].ColumnName, dtMaster.Columns["ConferenceType"].ColumnName, dtMaster.Columns["Description"].ColumnName, dtMaster.Columns["TimeZone"].ColumnName, dtMaster.Columns["Remarks"].ColumnName });
                dtTemp.DefaultView.Sort = "ConfirmationNumber, StartDateTime, Duration, Room";
                //dtTemp = dtMaster.DefaultView.ToTable();
                //Response.Write(dtTemp.Rows.Count);
                Conference objConference = new Conference(1, dtTemp, Application["COM_ConfigPath"].ToString());
                
                //int i = 0;
                //foreach (DataRow dr in dtTemp.Rows)
                //{
                //    i++;
                //    Response.Write("<br>" + dr["Room"].ToString() + " : " + dr["ConferenceName"].ToString() + " : " + dr["StartDateTime"].ToString() + " : " + dr["ConfirmationNumber"].ToString());
                //}
                //tblDataLoading.Visible = true;
                int cnt = 0;
                objConference.Process(ref cnt);
                //tblDataLoading.Visible = false;
                errLabel.Text = cnt.ToString() + " Conference(s) are imported successfully!";
                btnImportConferences.Enabled = false;
            }
            catch (Exception ex)
            {
                errLabel.Text = ex.StackTrace + "<br>" + ex.Message;
            }
        }

        protected DataTable CreateDataTableFromCSV()
        {
            StreamReader sr = null;
            try
            {
                ns_Logger.Logger log = new ns_Logger.Logger();
                DataTable dt = new DataTable();
                String ExternalFileName = fleMasterCSV.PostedFile.FileName;
                GenerateTableSchema(ref dt);
                //Response.Write(ExternalFileName);
                sr = File.OpenText(ExternalFileName);
                string strRecord = sr.ReadLine();
                //Response.Write
                string[] strTemp = strRecord.Split(',');
                int len = strTemp.Length;
                //Response.Write(dt.Columns.Count);
                while (sr.Peek() != -1)
                {
                    strRecord = sr.ReadLine();
                    DataRow dr = dt.NewRow();
                    strTemp = strRecord.Split(',');
                    //Response.Write("<br>" + strRecord);
                    for (int i = 0; i < len; i++)
                    {
                        dr[i] = strTemp[i].ToString().Trim();
                        dr[i] = dr[i].ToString().Replace("'", "");
                        //dr[i] = dr[i].ToString().Replace("\\", "");
                        //dr[i] = dr[i].ToString().Replace("/", "");
                        dr[i] = dr[i].ToString().Replace("\"", "");
                        dr[i] = dr[i].ToString().Replace("&", "&amp;");
                        dr[i] = dr[i].ToString().Replace("(", "");
                        dr[i] = dr[i].ToString().Replace(")", "");
                        dr[i] = dr[i].ToString().Replace("<", "");
                        dr[i] = dr[i].ToString().Replace(">", "");
                        //dr[i] = dr[i].ToString().Replace("+", "");
                        dr[i] = dr[i].ToString().Replace("%", "");

                        //Response.Write(dr[i]);
                    }
                    dr["UserEmail"] = dr["UserFirstName"] + "." + dr["UserLastName"] + "@Valleyair.com";
                    //log.Trace(dr["UserEmail"].ToString());
                    switch (dr["ConferenceType"].ToString().ToUpper())
                    {
                             /* Fogbugz case 138  */
                        case "MULTI POINT":
                            dr["ConferenceType"] = ns_MyVRMNet.vrmConfType.AudioVideo;
                            break;
                        case "POINT TO POINT":
                            dr["ConferenceType"] = ns_MyVRMNet.vrmConfType.AudioVideo;
                            break;
                        default: dr["ConferenceType"] = ns_MyVRMNet.vrmConfType.RoomOnly;
                            break;
                    }

                    //Code Changed for BCS --Start

                    switch (dr["TimeZone"].ToString())
                    {
                        case "CST":
                        case "CT":
                            dr["TimeZone"] = "14";
                            break;
                        case "EST":
                        case "ET":
                            dr["TimeZone"] = "26";
                            break;
                        case "GMT":
                            dr["TimeZone"] = "31";
                            break;
                        case "HST":
                            dr["TimeZone"] = "35";
                            break;
                        case "MST":
                            dr["TimeZone"] = "42";
                            break;
                        case "PST":
                        case "PT":
                            dr["TimeZone"] = "51";
                            break;
                        default:
                            dr["TimeZone"] = GetConferenceTimeZoneID(dr["TimeZone"].ToString());
                            break;
                    }

                    dr["UserID"] = GetUsers(dr["UserFirstName"].ToString(), dr["UserLastName"].ToString());
                    //Code changed for BCS --End

                    dt.Rows.Add(dr);
                    //log.Trace("StartDateTime: " + dr["StartDateTime"].ToString());
                }
                //sr.Close();
                btnImportConferences.Enabled = true;
                return dt;
            }
            catch (Exception ex)
            {
                errLabel.Text = ex.StackTrace + "<br>" + ex.Message;
                return null;
            }
            finally
            {
                if (sr != null)
                    sr.Close();
                sr = null;
            }
        }

        protected void GenerateTableSchema(ref DataTable dt)
        {
            try
            {
                if (!dt.Columns.Contains("Tier1")) dt.Columns.Add("Tier1");
                if (!dt.Columns.Contains("Tier2")) dt.Columns.Add("Tier2");
                if (!dt.Columns.Contains("Room")) dt.Columns.Add("Room");
                if (!dt.Columns.Contains("DepartmentID")) dt.Columns.Add("DepartmentID");
                if (!dt.Columns.Contains("DepartmentName")) dt.Columns.Add("DepartmentName");
                if (!dt.Columns.Contains("ConferenceName")) dt.Columns.Add("ConferenceName");
                if (!dt.Columns.Contains("ConferenceType")) dt.Columns.Add("ConferenceType");
                if (!dt.Columns.Contains("Description")) dt.Columns.Add("Description");
                if (!dt.Columns.Contains("ConferenceStatus")) dt.Columns.Add("ConferenceStatus");
                if (!dt.Columns.Contains("UserLastName")) dt.Columns.Add("UserLastName");
                if (!dt.Columns.Contains("UserFirstName")) dt.Columns.Add("UserFirstName");
                if (!dt.Columns.Contains("Telephone")) dt.Columns.Add("Telephone");
                if (!dt.Columns.Contains("StartDateTime")) dt.Columns.Add("StartDateTime");
                if (!dt.Columns.Contains("EndDateTime")) dt.Columns.Add("EndDateTime");
                if (!dt.Columns.Contains("TimeZone")) dt.Columns.Add("TimeZone");
                if (!dt.Columns.Contains("DaylightYesNo")) dt.Columns.Add("DaylightYesNo");
                if (!dt.Columns.Contains("Duration")) dt.Columns.Add("Duration");
                if (!dt.Columns.Contains("ConfirmationNumber")) dt.Columns.Add("ConfirmationNumber");
                if (!dt.Columns.Contains("Remarks")) dt.Columns.Add("Remarks");
                if (!dt.Columns.Contains("UserEmail")) dt.Columns.Add("UserEmail");
                if (!dt.Columns.Contains("UserID")) dt.Columns.Add("UserID");//veni

            }
            catch (Exception ex)
            {
                errLabel.Text = ex.StackTrace + "<br>" + ex.Message;
            }
        }

        //code added for BCS - start
        protected String GetConferenceTimeZoneID(String tZoneStr)
        {
            
            DataSet dsZone = new DataSet();
            myVRMNet.NETFunctions obj;
            String tZoneID = "33";
            String tZone = "";
            String[] tZoneArr = null;
             String[] tZoneGMTArr = null;
            try
            {
                //code added for BCS - start
                //Time Zone

                String zoneInXML = "<GetTimezones><UserID>11</UserID></GetTimezones>";
                String zoneOutXML;

                tZone = tZoneStr;

                if (dtZone == null)
                {
                    if (dsZone.Tables.Count == 0)
                    {
                        obj = new myVRMNet.NETFunctions();

                        zoneOutXML = obj.CallMyVRMServer("GetTimezones", zoneInXML, HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());
                        //HttpContext.Current.Response.Write(obj.Transfer(zoneOutXML));
                        //HttpContext.Current.Response.End();

                        obj = null;

                        XmlDocument xmldoc = new XmlDocument();
                        xmldoc.LoadXml(zoneOutXML);
                        String sel = xmldoc.SelectSingleNode("//Timezones/selected").InnerText;
                        XmlNodeList nodes = xmldoc.SelectNodes("//Timezones/timezones/timezone");
                        if (nodes.Count > 0)
                        {
                            XmlTextReader xtr;

                            foreach (XmlNode node in nodes)
                            {
                                xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                                dsZone.ReadXml(xtr, XmlReadMode.InferSchema);
                            }

                            if (dsZone.Tables.Count > 0)
                            {
                                dvZone = new DataView(dsZone.Tables[0]);
                                dtZone = dvZone.Table;
                            }
                        }
                        //code added for BCS - start
                    }
                }

                if (tZone != "")
                {

                    tZoneArr = tZone.Split(' ');

                    foreach (String s in tZoneArr)
                    {
                        if (s.ToString().ToUpper() == "GMT")
                        {
                            tZoneID = "31";
                            break;
                        }
                        else if (s.Contains("GMT"))
                        {
                            if (dtZone.Rows.Count > 0)
                            {
                                foreach (DataRow row in dtZone.Rows)
                                {
                                    if (row["timezoneName"].ToString().Contains(s))
                                    {
                                        tZoneID = row["timezoneID"].ToString();
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                errLabel.Text = ex.StackTrace + "<br>" + ex.Message;
            }

            return tZoneID;
        }

        protected String GetUsers(String firstName, String lastName)
        {

            string userid = "11";

            if (dsUsers.Tables.Count == 0)
            {
                obj = new myVRMNet.NETFunctions();

                String inXML = "";
                inXML += "<login>";
                inXML += "  <userID>11</userID>";
                inXML += "  <user>";
                inXML += "      <firstName>" + firstName + "</firstName>";
                inXML += "      <lastName>" + lastName + "</lastName>";
                inXML += "      <email></email>";
                inXML += "  </user>";
                inXML += "</login>";
                String outXML = obj.CallCOM("SearchUser", inXML, Application["COM_ConfigPath"].ToString());

                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);
                //String userName = xmldoc.SelectSingleNode("/oldUser/userName/firstName").InnerText + " " + xmldoc.SelectSingleNode("/oldUser/userName/lastName").InnerText;

                XmlNodeList nodes = xmldoc.SelectNodes("//users/user");
                if (nodes.Count > 0)
                {
                    XmlTextReader xtr;

                    foreach (XmlNode node in nodes)
                    {
                        xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                        dsUsers.ReadXml(xtr, XmlReadMode.InferSchema);
                    }

                    if (dsUsers.Tables.Count > 0)
                    {
                        dvUsers = new DataView(dsUsers.Tables[0]);
                        dtUsers = dvUsers.Table;
                    }
                }
            }

            if (dtUsers.Rows.Count > 0)
            {
                foreach (DataRow row in dtUsers.Rows)
                {
                    //String name = row["UserFirstName"].ToString() + "," + row["UserLastName"].ToString();
                    String name = row["FirstName"].ToString() + "," + row["LastName"].ToString();
                    if (name.ToLower() == firstName.ToLower() + "," + lastName.ToLower())
                    {
                        userid = row["userID"].ToString();
                        break;
                    }
                }
            }

            return userid;

        }
        //code added for BCS - end

        //Code added for Organization/CSS Module  -- Start
        #region ImportDefaultCSSXML
        protected void ImportDefaultCSSXML(object sender, EventArgs e)
        {
            try
            {
                String inXML = "";
                inXML = "<SetDefaultCSSXML>";
                myVRMNet.NETFunctions obj = new myVRMNet.NETFunctions();
                inXML += obj.OrgXMLElement();//Organization Module 
                myVRMNet.ImageUtil imageUtil = new myVRMNet.ImageUtil();
                string textXML = "";
                if (cssXMLFileUpload.PostedFile.FileName != "")
                    textXML = imageUtil.ConvertImageToBase64(cssXMLFileUpload.PostedFile.FileName);
                else
                {
                    errLabel.Text = "Please Upload the File";
                    errLabel.Visible = true;
                }

                inXML += "<DefaultCSSXml>" + textXML + "</DefaultCSSXml>";
                inXML += "</SetDefaultCSSXML>";

                String outXML = obj.CallMyVRMServer("SetDefaultCSSXML", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") >= 0)
                {
                    this.errLabel.Text = obj.ShowErrorMessage(outXML);
                    this.errLabel.Visible = true;
                }
                else
                {
                    errLabel.Text =" Default CSS XML has been imported successfully!";
                   // btnImportDefaultCSSXML.Enabled = false;
                }
                
            }
            catch (Exception ex)
            {
                errLabel.Text = ex.StackTrace + "<br>" + ex.Message;
            }
        }
        #endregion
        //Code added for Organization/CSS Module -- End
    }
    
}
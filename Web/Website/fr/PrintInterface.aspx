<%@ Page Language="C#" AutoEventWireup="true" Inherits="en_PrintInterface" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf8" />
    <title>Report</title>
    <link rel="stylesheet" title="Expedite base styles" type="text/css" href="<%=Session["OrgCSSPath"]%>" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table cellpadding="0" cellspacing="0" border="0" align="center" width="98%">
			<tr bgcolor="white">
				<td><br/>
					<asp:Label ID="ErrorLabel" CssClass="lblError" Runat="server" Visible="True"></asp:Label>
				</td>
			</tr>
			<tr>
				<td align="center" bgcolor="white" width="100%">
					<asp:PlaceHolder ID="PrintHolder" Runat="server"></asp:PlaceHolder>
				</td>
			</tr>
		</table>
    </div>
    </form>
</body>
</html>
<script language="javascript">window.print()</script>


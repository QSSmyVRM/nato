<%@ Page Language="C#" Inherits="ns_ConferenceList.ConferenceList" ValidateRequest="false"  EnableEventValidation="false"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <!-- FB 2050 -->
<meta http-equiv="X-UA-Compatible" content="IE=7" /> <!-- FB 2050 -->

<% 
if (Request.QueryString["hf"] == null) 
{ 
%>
    <!-- #INCLUDE FILE="inc/maintopNET.aspx" --> 
<% 
} 
else 
{ 
    if (Request.QueryString["hf"].ToString().Equals("1")) 
    {
        if (Session["userID"] == null)
        {
            Session.Add("userID", "11");
            Application.Add("COM_ConfigPath", "C:\\VRMSchemas_v1.8.3\\ComConfig.xml");
            Application.Add("MyVRMServer_ConfigPath", "C:\\VRMSchemas_v1.8.3\\");
        }
        %>
        <!-- #INCLUDE FILE="inc/maintopNET4.aspx" --> <%--Login Management--%>
        <%  
    }
    if (Request.QueryString["hf"].ToString().Equals("1") && Request.QueryString["t"].ToString().Equals("4")) 
    {
        lstCalendar.Attributes.Add("Style", "Display:None"); 
    } 
} %>
<script type="text/javascript">
  var servertoday = new Date();

//FB Case 680 Saima this function is introduced � avoid Approver conference getting
// called if Non conference has been selected.
function CountChecked() 
{
    var count = 0;
    var elements = document.getElementsByTagName('input'); 
    for (i=0;i<elements.length;i++)
    if ( (elements.item(i).type == "radio") && (elements.item(i).id.indexOf("Tout") < 0) && (elements.item(i).checked) ) 
        count++;
    if (count <= 0)
    {
        //added for FB 1428 Start
            
		if('<%=Application["Client"]%>' == "MOJ")
		    alert("Veuillez selectioner au moins une conference.");
		else
		//added for FB 1428 End	`
            alert("Veuillez selectioner au moins une conference.");
            
    }
    document.getElementById("txtSelectionCount").value = count;
}

//FB 2448 - Starts
function viewconfMCUinfo(cid) {
    url = "ConfMCUInfo.aspx?t=hf&confid=" + cid;
    confMCUdetail = window.open(url, "viewconfMCUinfo", "status=no,width=700,height=400,scrollbars=yes,resizable=yes");
    confMCUdetail.focus();
}
//FB 2448 - End

function goToCal()
{
        if(document.getElementById("lstCalendar") != null)
        {
		    if (document.getElementById("lstCalendar").value == "1"){
			    window.location.href = "PersonalCalendar.aspx?v=1&r=1&hf=&d=" ; //code changed for calendar conversion FB 412
		    }
		    if (document.getElementById("lstCalendar").value == "3"){
			    window.location.href = "roomcalendar.aspx?v=1&r=1&hf=&d=&pub=&m=&comp=" ; //code changed for calendar conversion FB 412
			}
			//FB 2501 Call Monitoring
			if (document.getElementById("lstCalendar").value == "4") {
			    window.location.href = "MonitorMCU.aspx";
			}
			//FB 2501 P2P Call Monitoring
			if (document.getElementById("lstCalendar").value == "5") {
			    window.location.href = "point2point.aspx";
			}
		}
        
		
}
function DataLoading(val)
{
//alert(val);
    if (val=="1")
        document.getElementById("dataLoadingDIV").innerHTML="<img border='0' src='image/wait1.gif'>";
    else
        document.getElementById("dataLoadingDIV").innerHTML="";                   
}

function roomcalendarview()
{
	window.location.href = "dispatcher/admindispatcher.asp?cmd=ManageConfRoom&f=v";
}

function personalcalendarview()
{
	//window.location.href = "calendarpersonaldaily.aspx?v=1&r=1&hf=&d=" ; //code changed for calendar conversion FB 412
                    window.location.href = "PersonalCalendar.aspx?v=1&r=1&hf=&d=" ; //code changed for calendar conversion FB 412
}

function UndecideAll(obj)
{
  if (obj.tagName == "INPUT" && obj.type == "radio" && obj.checked) 
    {
        var elements = document.getElementsByTagName('input'); 
        for (i=0;i<elements.length;i++)
        if ( (elements.item(i).type == "radio") && (elements.item(i).id.indexOf("rdUndecided") >= 0) ) 
        {
            if(elements.item(i).id!=obj.id) 
            {
                elements.item(i).checked= true; 
            }
        } 
    }
}
function ApproveAll(obj)
{
  if (obj.tagName == "INPUT" && obj.type == "radio" && obj.checked) 
    {
        var elements = document.getElementsByTagName('input'); 
        for (i=0;i<elements.length;i++)
        if ( (elements.item(i).type == "radio") && (elements.item(i).id.indexOf("rdApprove") >= 0) ) 
        {
            if(elements.item(i).id!=obj.id) 
            {
                elements.item(i).checked= true; 
            }
        } 
    }
}
function DenyAll(obj)
{
//alert(obj.checked);
  if (obj.tagName == "INPUT" && obj.type == "radio" && obj.checked) 
    {
        var elements = document.getElementsByTagName('input'); 
        for (i=0;i<elements.length;i++)
        if ( (elements.item(i).type == "radio") && (elements.item(i).id.indexOf("rdDeny") >= 0) ) 
        {
            if(elements.item(i).id!=obj.id) 
            {
                elements.item(i).checked= true; 
            }
        } 
    }
}
function viewconf(cid)
{
	url = "ManageConference.aspx?t=hf&confid=" + cid;
	confdetail = window.open(url, "viewconference", "width=1,height=1,resizable=yes,scrollbars=yes,status=no");
	confdetail.focus();
}

function viewapprovalstatus(cid, m, d)
{
	//code added for FB 412 - Start
	//url = "dispatcher/gendispatcher.asp?cmd=GetApprovalStatus&cid=" + cid + "&m==" + m + "&d=" + d;
	m = m.replace("�","'"); //FB 2321
	url = "approvalstatus.aspx?confid=" + cid + "&m=" + m + "&d=" + d;
	//code added for FB 412 - End
	approvalwin = window.open(url, "approvalstatus", "status=no,width=700,height=400,scrollbars=yes,resizable=yes");
	approvalwin.focus()
}

//Code added for FB 1391 -- Start

function CustomEditAlert()
{

    DataLoading('1');
  
    var args = CustomEditAlert.arguments;
    
    if(args != null)
    { 
            
        var msg = "Certaines des conferences de cetter serie commencent a des heures qui leurs sont uniques. Executer un changement global forcera un realignement de toutes les heures. Choisisser OK pour continuer, ou ANNULER pour sauvegarder lunicite des conferences."
        var act;
        
        if (args[0] == "Y" )
        {
            act = confirm(msg);
            
            if(!act)
                DataLoading('0')
                
           return act;
        }
    }  
    
    return true;
}
//Code added for FB 1391 -- End
//FB 2382
function fnCheck()
{
    var chkAllSilo = document.getElementById("chkAllSilo");  
    var hdnChkSilo = document.getElementById("hdnChkSilo");  
    
    if(chkAllSilo)
    {
        if(chkAllSilo.checked == true)
            hdnChkSilo.value = "1";
        else
            hdnChkSilo.value = "0";
    }    
}


</script>



<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf8" />
    <title>Conference List</title>
</head>
<body>
    <form id="frmSearchConference" runat="server" method="post">
    <input type="hidden" id="hdnJoinID" runat="server" /> <%--FB 1934--%>
    <input type="hidden" id="hdnChkSilo" runat="server" />  <%--FB 2382--%>
        <center><table border="0" width="98%" cellpadding="2" cellspacing="2">
            <tr>
                <td align="center">
                    <h3><asp:Label ID="lblHeader" runat="server" Text="Liste des conf�rences"></asp:Label>
	                        <select id="lstCalendar" name="lstCalendar" class="altText" size="1" onchange="goToCal()" runat="server">
	                                <option value="2">Liste s�quentielle</option>          		                    
          		                    <option value="1">Calendrier personnel Visualisation</option>
          		                    <option value="3">Calendrier des Salles de Conf�rence Visualisation</option>
          		                    <option value="4">moniteur d'appel</option><%--FB 2501 Call Monitoring--%>
          		                    <option value="5">Moniteur d'appel (P2P)</option><%--FB 2501 P2P Call Monitoring--%>
                                </select>
                      
                    </h3>
                    <asp:Label ID="lblTimezone" runat="server" Visible="false" CssClass="subtitleblueblodtext"></asp:Label>
                    <br />
                    <asp:Label ID="errLabel" runat="server" CssClass="lblError"></asp:Label>
                    <div id="dataLoadingDIV"></div>
                    <asp:Button ID="btnRefreshList" runat="server" Visible="false" OnClick="Updatelist" />
                    <asp:CheckBox ID="Refreshchk" runat="server" style="display:none" />
                </td>
                
            </tr>
            <tr><%--rss changes--%>
                <td  align="right" runat="server" id="PublicRSS"> 
                      <asp:ImageButton ID="ImageButton1"  src="image/rss.gif"  runat="server"  ToolTip="Publique Flux RSS" OnClick="RSSFeed" />
                </td>
            </tr>
            <%--Added for Org - Start--%>
            <tr id="OrgRow" runat="server" style="display:none;">
                <td> 
                    <table width="95%" align="center" border="0">
                        <tr align="left"> <!-- FB 2050 -->
                            <td align="left" class="subtitleblueblodtext" width="10%"><asp:Label ID="lblOrgNames" runat="server" Text="organisation"></asp:Label> <!-- FB 2050 -->
                            </td>
                            <td valign="bottom" align="left"> <!-- FB 2050 -->
                                 <asp:DropDownList CssClass="altSelectFormat" ID="drpOrgs" runat="server" DataTextField="OrganizationName" OnSelectedIndexChanged="OrgIndexChanged" DataValueField="OrgID" AutoPostBack="true"></asp:DropDownList> <%--Edited for FF--%>
                            </td>
                        </tr>
                     </table>
                 </td>
            </tr>
            <%--Added for Org - End--%>
            <tr>
                <td>
                    <table width="90%" runat="server" id="tblLists">
                        <tr>
                            <%--Window Dressing--%>
                            <td width="50%" align="left" class="blackblodtext"><b><asp:Label runat="server" ID="lblConfTypeLabel" Text="Filtre de S�lection" ></asp:Label></b>
                                <asp:DropDownList ID="lstListType" runat="server" CssClass="altSelectFormat" OnSelectedIndexChanged="ChangeListType" AutoPostBack="true" onchange="javascript:DataLoading('1');">
                                    <asp:ListItem Selected="True" Text="Visualisation En cours" Value="2"></asp:ListItem>
                                    <asp:ListItem Selected="False" Text="Visualisation R�servations" Value="3"></asp:ListItem>
                                    <asp:ListItem Selected="False" Text="Visualisation Publique (dans le Monde myVRM)" Value="4"></asp:ListItem>
                                    <asp:ListItem Selected="False" Text="Visualisation En cours" Value="5"></asp:ListItem>
                                    <asp:ListItem Selected="False" Text="Visualisation Approval En cours" Value="6"></asp:ListItem>
                                    <asp:ListItem Selected="False" Text="View VNOC Conference" Value="7"></asp:ListItem> <%--FB 2501 VNOC--%>
                                </asp:DropDownList>        
                            </td>
                            <td class="blackblodtext" width="11%" id="tdchkSilo">Afficher tout�Silo</td><%--FB 2274--%>
                            <td id="td2ChkSilo">
                                <asp:CheckBox ID="chkAllSilo" runat="server" OnClick="fnCheck()" OnCheckedChanged="CheckAllSilo"  AutoPostBack="true"/>  <%--FB 2382--%>                             
                            </td>
                            <td  align="right" runat="server" id="PrivateRSS"> <%--rss changes--%>
                              <asp:ImageButton ID="ImageButton3"  src="image/rss.gif"  runat="server"  ToolTip="Priv� Flux RSS" OnClick="PrivateRSSFeed" />
                            </td>
                        </tr>
                    </table>
                    
                    
                </td>
            </tr>
            <tr>
                <td align="center" colspan="2">
                <asp:DataGrid ID="dgConferenceList" AllowSorting="true" runat="server" AutoGenerateColumns="False" Font-Names="Verdana" Font-Size="Small" ShowFooter="true"
                 Width="100%"  style="border-collapse:separate" BorderStyle="None" BorderWidth="0px" GridLines="None" OnItemDataBound="InterpretRole" OnItemCreated="BindRowsDeleteMessage"
                 OnEditCommand="EditConference" OnCancelCommand="DeleteConference" OnDeleteCommand="ManageConference" OnUpdateCommand="CloneConference"><%--Edited For FF--%><%--FB 1982--%>
                    <ItemStyle CssClass="tableBody" Height="15" VerticalAlign="Top" />
                    <AlternatingItemStyle CssClass="tableBody" Height="15" />
                    <HeaderStyle CssClass="tableHeader" Height ="30"/>
                    <SelectedItemStyle CssClass="tableBody" />
                    <Columns>
                        <asp:BoundColumn DataField="ConferenceID" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="IsHost" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="IsParticipant" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="ConferenceDuration" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="ConferenceStatus" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="IsRecur" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="OpenForRegistration" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="Conference_Id" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="ConferenceDateTime" Visible="false"></asp:BoundColumn>
						<asp:BoundColumn DataField="organizationName" Visible="false"></asp:BoundColumn>
                        <asp:TemplateColumn ItemStyle-Width="80%" HeaderStyle-Width="80%" FooterStyle-HorizontalAlign="Right">
                            <HeaderTemplate>
                                <table width="100%">
                                    <tr class="tableHeader" runat="server" visible='<%# Request.QueryString["t"] == "6" %>'>
                                        <td class="tableHeader">&nbsp;</td>
                                        <td class="tableHeader">&nbsp;</td>
                                        <td class="tableHeader">&nbsp;</td>
                                        <td class="tableHeader">&nbsp;</td>
                                        <td class="tableHeader">&nbsp;</td>
                                        <td class="tableHeader">&nbsp;</td><%--FB 2274--%>
                                        <td align="left">
                                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td width="30%">&nbsp;</td>
                                                    <td width="35%">
                                                        <table width="100%" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td class="tableHeader" align="left">Ind�cis</td>
                                                                <td class="tableHeader" align="left">Approver</td>
                                                                <td class="tableHeader" align="left">Rejeter</td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr class="tableHeader">
                                    <%--Window Dressing--%>
                                        <td width="5%" runat="server" id="tdID" align="left" class="tableHeader"><%--FB 2274--%>
                                            ID unique<asp:LinkButton  ID="btnSortID" runat="server" CommandArgument="1" OnCommand="SortGrid" OnClientClick="javascript:DataLoading('1');"><%if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("MSIE"))%> <span style="font-family:Wingdings; font-weight:bold;">&#233;</span><%else %><span  style="font-family:Wingdings; font-weight:bolder; font-size:x-large;">&#8593;</span></asp:LinkButton> <%--Edited for FF--%>
                                        </td>
                                        <%--Added for FB 1428 Start--%>
                                                <%if (Application["Client"] == "MOJ")
                                                  { %>
                                                  <td width="20%" runat="server" id="td5" align="left" class="tableHeader"> <%--Window Dressing--%>
                                           Hearing Nom<asp:LinkButton ID="btnMOJSortName" runat="server" CommandArgument="2" OnCommand="SortGrid" OnClientClick="javascript:DataLoading('1');"><%if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("MSIE"))%> <span style="font-family:Wingdings; font-weight:bold;">&#233;</span><%else %><span style="font-family:Wingdings; font-weight:bolder; font-size:x-large">&#8593;</span></asp:LinkButton> <%--Edited for FF--%>
                                        </td>
                                        <%}
                                                  else
                                                  {%>
                                        <td width="20%" runat="server" id="tdName" align="left" class="tableHeader"> <%--Window Dressing--%>
                                           Nom de Conf�rence<asp:LinkButton ID="btnSortName" runat="server" CommandArgument="2" OnCommand="SortGrid" OnClientClick="javascript:DataLoading('1');"><%if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("MSIE"))%> <span style="font-family:Wingdings; font-weight:bold;">&#233;</span><%else %><span style="font-family:Wingdings; font-weight:bolder; font-size:x-large">&#8593;</span></asp:LinkButton> <%--Edited for FF--%>
                                        </td>
                                         <%}%>
                                         <%--Added for FB 1428 End--%>
										<td width="10%" runat="server" id="tdOrgName" align="left" class="tableHeader"> <%--Window Dressing--%>
                                           Nom Silo<asp:LinkButton ID="btnSortorgName" runat="server" CommandArgument="2" OnCommand="SortGrid" OnClientClick="javascript:DataLoading('1');"><%if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("MSIE"))%> <span style="font-family:Wingdings; font-weight:bold;">&#233;</span><%else %><span style="font-family:Wingdings; font-weight:bolder; font-size:x-large">&#8593;</span></asp:LinkButton> <%--Edited for FF--%>
                                        </td>
                                                <%--Added for FB 1428 Start--%>
                                                <%if (Application["Client"] == "MOJ")
                                                  { %>
                                                  <td width="10%" runat="server" id="td6" style="text-decoration:underline" align="left" class="tableHeader"> <%--Window Dressing--%>
                                           Hearing Date/Time<asp:LinkButton ID="btnMOJSortDateTime" runat="server" CommandArgument="3" OnCommand="SortGrid" OnClientClick="javascript:DataLoading('1');"><%if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("MSIE"))%> <span style="font-family:Wingdings; font-weight:bold;">&#233;</span><%else %><span style="font-family:Wingdings; font-weight:bolder; font-size:x-large">&#8593;</span></asp:LinkButton> <%--Edited for FF--%>
                                        </td>
                                        <%}
                                                  else
                                                  {%>
                                        <td width="10%" runat="server" id="tdDateTime" style="text-decoration:underline" align="left" class="tableHeader"> <%--Window Dressing--%>
                                           <asp:Label ID="lblDtTimeHeader" runat="server" Text="Date / Heure"></asp:Label> <%--FB 1607--%>
                                           <asp:LinkButton ID="btnSortDateTime" runat="server" CommandArgument="3" OnCommand="SortGrid" OnClientClick="javascript:DataLoading('1');"><%if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("MSIE"))%> <span style="font-family:Wingdings; font-weight:bold;">&#233;</span><%else %><span style="font-family:Wingdings; font-weight:bolder; font-size:x-large">&#8593;</span></asp:LinkButton> <%--Edited for FF--%>
                                        </td>
                                        <%}%>
                                                <%--Added for FB 1428 End--%>
                                        <td width="10%" class="tableHeader" align="left">Genre</td>
                                        
                                        <%--FB 2501 - Start--%>
                                        <td width="5%" runat="server" id="td9" align="left" class="tableHeader">
                                           mode de conf<asp:LinkButton ID="btnSortConfMode" runat="server" CommandArgument="4" OnCommand="SortGrid" OnClientClick="javascript:DataLoading('1');"><%if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("MSIE"))%> <span style="font-family:Wingdings; font-weight:bold;">&#233;</span><%else %><span style="font-family:Wingdings; font-weight:bolder; font-size:x-large">&#8593;</span></asp:LinkButton>
                                        </td>
                                        <%--FB 2501 - End--%>
                                        
                                        <td width="5%" class="tableHeader" align="left" >Dur�e</td><%--FB 2274--%>
                                        <td width="5%" class="tableHeader" align="left" runat="server" visible='<%# (Request.QueryString["t"] != "6") %>' >Propri�taire</td>
                                        <td width="5%" class="tableHeader" align="left" runat="server" visible='<%# (Request.QueryString["t"] != "6") %>' >Invit�</td>
                                        <td width="30%" class="tableHeader" align="center" runat="server" visible='<%# (Request.QueryString["t"] != "6") %>'>Actions</td>
                                        <td width="40%" class="tableHeader" align="center" runat="server" visible='<%# (Request.QueryString["t"] == "6") %>'>
                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td width="30%">&nbsp;</td>
                                                    <td width="35%">
                                                        <table width="100%" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td align="center">
                                                                    <asp:RadioButton ID="rdUndecideAll" GroupName="Decision" onclick="javascript:UndecideAll(this);" Checked="true" runat="server" />
                                                                </td>
                                                                <td align="center">
                                                                    <asp:RadioButton ID="rdApproveAll" GroupName="Decision" runat="server" onclick="javascript:ApproveAll(this);"  />
                                                                </td>
                                                                <td align="center">
                                                                    <asp:RadioButton ID="rdDenyAll" GroupName="Decision" runat="server" onclick="javascript:DenyAll(this);" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <table width="100%" class="tableBody">
                                    <tr style='background-color: <%#((Request.QueryString["hf"] == "1") && DataBinder.Eval(Container, "DataItem.isVMR").ToString().Equals("1") && DataBinder.Eval(Container, "DataItem.OpenForRegistration").ToString().Equals("1") && !(DataBinder.Eval(Container, "DataItem.RemPublicVMRCount").ToString().Equals("0")))?"#F0E68C":"Transparent"%>'> <%--FB 2550--%>
                                        <td width="5%" align="left" valign="top" ><%--FB 2274--%>
                                            <asp:Label ID="lblUniqueID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ConferenceUniqueID") %>'></asp:Label>
                                        </td>
                                        <td width="20%" align="left" valign="top">
                                            <asp:Image ID="imgRecur" Height="20" Width="20" ImageUrl="image/recurring.gif" runat="server" ToolTip="conf�rence r�current" Visible='<%# DataBinder.Eval(Container, "DataItem.IsRecur").ToString().Trim().Equals("1") %>' />
                                            <asp:Label ID="lblConfName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ConferenceName") %>'></asp:Label>
                                        </td>
										<td width="10%" align="left" valign="top" >
                                            <asp:Label ID="lblOrgName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.organizationName") %>'></asp:Label>
                                        </td>
                                        <td width="10%" align="left" valign="top" style="display:none"><%--FB 2448--%>
                                            <asp:Label ID="lblisVMR" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.isVMR") %>'></asp:Label>
                                        </td>
                                        <%--FB 2550 Starts--%>
                                        <td width="10%" align="left" valign="top" style="display:none">
                                            <asp:Label ID="lblPublicVMRCount" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.RemPublicVMRCount") %>'></asp:Label>
                                        </td>
                                        <td width="10%" align="left" valign="top" style="display:none">
                                            <asp:Label ID="Label3" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.isVMRJoin") %>'></asp:Label>
                                        </td>
                                        <%--FB 2550 Ends--%>
                                        <td width="10%" align="left" valign="top">
                                            <asp:Label ID="lblDateTime" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ConferenceDateTime")%>' Visible='<%# !DataBinder.Eval(Container, "DataItem.IsRecur").ToString().Trim().Equals("1") %>'></asp:Label>
                                            <asp:LinkButton ID="btnGetInstances" Text="Visualiser toutes les Instances" runat="server" CommandName="Edit" Visible='<%# DataBinder.Eval(Container, "DataItem.IsRecur").ToString().Trim().Equals("1") %>' OnClientClick="javascript:DataLoading('1');"></asp:LinkButton>
                                        </td>
                                        <td width="10%" align="left" valign="top">
                                            <asp:Label ID="lblConferenceType" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ConferenceTypeDescription")%>'></asp:Label>
                                        </td>
                                        <td width="10%" align="left" valign="top"><%--FB 2501--%>
                                            <asp:Label ID="lblConferenceMode" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.StartMode") %>'></asp:Label>
                                        </td>
                                        
                                        <td width="5%" align="left" valign="top"><%--FB 2274--%>
                                            <asp:Label ID="lblDuration" runat="server"></asp:Label>
                                        </td>
                                        <td width="5%" align="left" valign="top" runat="server" visible='<%# (Request.QueryString["t"] != "6") %>' >
                                            <asp:Label ID="lblOwner" runat="server"></asp:Label>
                                        </td>
                                        <td width="5%" align="left" valign="top" runat="server" visible='<%# (Request.QueryString["t"] != "6") %>' >
                                            <asp:Label ID="lblAttendee" runat="server"></asp:Label>
                                        </td>
                                        <td id="Td7" width="30%" runat="server" valign="top" visible='<%# (Request.QueryString["t"] != "6") %>'>
                                                <table width="100%" cellspacing="3">
                                                    <tr>
                                                        <td align="left" ><asp:LinkButton ID="btnViewDetails" runat="server" Text="Voir" Visible='<%# (Request.QueryString["hf"] != "1") %>'></asp:LinkButton></td>
                                                        <td align="left" visible='<%# (Request.QueryString["hf"] != "1") %>'><asp:LinkButton Text="D�doubler" runat="server" ID="btnClone" CommandName="Update" Visible='<%# ( (Request.QueryString["hf"] != "1") && (DataBinder.Eval(Container, "DataItem.IsHost").ToString().Equals("1") || (Int32.Parse(Session["admin"].ToString()) > 0))) %>' OnClientClick="javascript:DataLoading('1');"></asp:LinkButton></td>
                                                        <td align="left" visible='<%# (Request.QueryString["hf"] != "1") %>'><asp:LinkButton Text="Supprimer" runat="server" ID="btnDelete" CommandName="Cancel" Visible='<%# ( (Request.QueryString["hf"] != "1")) && (DataBinder.Eval(Container, "DataItem.IsHost").ToString().Equals("1") || (Int32.Parse(Session["admin"].ToString()) >= 1)) %>' OnClientClick="javascript:DataLoading('1');"></asp:LinkButton></td>
                                                        <td align="left" visible='<%# (Request.QueryString["hf"] != "1") %>'><asp:LinkButton Text="Editer" runat="server" ID="btnEdit" CommandName="Edit" Visible='<%# ((Request.QueryString["hf"] != "1")) && (DataBinder.Eval(Container, "DataItem.IsHost").ToString().Equals("1") || (Int32.Parse(Session["admin"].ToString()) >= 1)) %>' OnClientClick="javascript:DataLoading('1');" ></asp:LinkButton></td>
                                                        <td align="left" visible='<%# (Request.QueryString["hf"] != "1") %>'><asp:LinkButton Text="G�rer" runat="server" ID="btnManage" CommandArgument="1" CommandName="Delete" Visible='<%# (Request.QueryString["hf"] != "1") && (DataBinder.Eval(Container, "DataItem.IsHost").ToString().Equals("1") || Int32.Parse(Session["admin"].ToString()) >= 1) %>' OnClientClick="javascript:DataLoading('1');"></asp:LinkButton></td>
                                                        <td align="left" ><asp:LinkButton ID="btnMCUDetails" runat="server" Text="MCU infos" Visible="false"></asp:LinkButton></td> <%--FB 2448--%>                                                        
                                                        <td align="left" visible='<%# ( (Request.QueryString["hf"] == "1") && DataBinder.Eval(Container, "DataItem.OpenForRegistration").ToString().Equals("1") && (DataBinder.Eval(Container, "DataItem.isVMRJoin").ToString().Equals("1"))) %>'><asp:LinkButton Text="Join" runat="server" ID="btnJoin" CommandArgument="2" CommandName="Delete" Visible='<%# ( (Request.QueryString["hf"] == "1") && DataBinder.Eval(Container, "DataItem.OpenForRegistration").ToString().Equals("1") && (DataBinder.Eval(Container, "DataItem.isVMRJoin").ToString().Equals("1"))) %>' OnClientClick="javascript:DataLoading('1');"></asp:LinkButton></td> <%--FB 2550--%>
                                                        <td align="left" visible='<%# (Request.QueryString["hf"] != "1") %>'><asp:LinkButton Text="Condition" runat="server" ID="btnApprovalStatus" Visible='<%# (Request.QueryString["t"] == "5")%>'></asp:LinkButton></td>
<%--                                                        <td align="left" ><asp:LinkButton ID="btnViewDetails" runat=server Text="View" Visible='<%# (Request.QueryString["hf"] != "1") %>'></asp:LinkButton></td>
                                                        !DataBinder.Eval(Container, "DataItem.ConferenceStatus").ToString().Equals("7") &&<td align="left" visible='<%# (Request.QueryString["hf"] != "1") %>'><asp:LinkButton Text="D�doubler" runat="server" ID="btnClone" CommandName="Update" Visible='<%# ( (Request.QueryString["hf"] != "1") && (DataBinder.Eval(Container, "DataItem.IsHost").ToString().Equals("1") || (Int32.Parse(Session["admin"].ToString()) > 0))) %>' OnClientClick="javascript:DataLoading('1');"></asp:LinkButton></td>
                                                        <td align="left" visible='<%# (Request.QueryString["hf"] != "1") %>'><asp:LinkButton Text="Supprimer" runat="server" ID="btnDelete" CommandName="Cancel" Visible='<%# ( (Request.QueryString["hf"] != "1") && !DataBinder.Eval(Container, "DataItem.ConferenceStatus").ToString().Equals("7")) && (DataBinder.Eval(Container, "DataItem.IsHost").ToString().Equals("1") || (Int32.Parse(Session["admin"].ToString()) >= 1)) %>' OnClientClick="javascript:DataLoading('1');"></asp:LinkButton></td>
                                                        <td align="left" visible='<%# (Request.QueryString["hf"] != "1") %>'><asp:LinkButton Text="Edit" runat="server" ID="btnEdit" CommandName="Edit" Visible='<%# ((Request.QueryString["hf"] != "1") && !DataBinder.Eval(Container, "DataItem.ConferenceStatus").ToString().Equals("7")) && (DataBinder.Eval(Container, "DataItem.IsHost").ToString().Equals("1") || (Int32.Parse(Session["admin"].ToString()) >= 1)) %>' OnClientClick="javascript:DataLoading('1');"></asp:LinkButton></td>
                                                        <td align="left" visible='<%# (Request.QueryString["hf"] != "1") %>'><asp:LinkButton Text="G�rer" runat="server" ID="btnManage" CommandArgument="1" CommandName="Delete" Visible='<%# (Request.QueryString["hf"] != "1") && (DataBinder.Eval(Container, "DataItem.IsHost").ToString().Equals("1") || Int32.Parse(Session["admin"].ToString()) >= 1) %>' OnClientClick="javascript:DataLoading('1');"></asp:LinkButton></td>
                                                        <td align="left" visible='<%# ( (Request.QueryString["hf"] == "1") && DataBinder.Eval(Container, "DataItem.OpenForRegistration").ToString().Equals("1")) %>'><asp:LinkButton Text="Join" runat="server" ID="btnJoin" CommandArgument="2" CommandName="Delete" Visible='<%# ( (Request.QueryString["hf"] == "1") && DataBinder.Eval(Container, "DataItem.OpenForRegistration").ToString().Equals("1")) %>' OnClientClick="javascript:DataLoading('1');"></asp:LinkButton></td>
                                                        <td align="left" visible='<%# (Request.QueryString["hf"] != "1") %>'><asp:LinkButton Text="Condition" runat="server" ID="btnApprovalStatus" Visible='<%# (Request.QueryString["t"] == "5")%>'></asp:LinkButton></td>--%>
                                                    </tr>
                                                </table>
                                        </td>
                                        <td width="40%" valign="top" runat="server" visible='<%# (Request.QueryString["t"] == "6") %>'>
                                            <asp:DataGrid runat="server" ShowHeader="false" BorderWidth="0"   ID="dgResponse" Width="100%"
                                             AutoGenerateColumns="false" CellPadding="0" CellSpacing="0" style="border-collapse:separate"> <%--Edited for FF--%>
                                            <Columns>
                                                <asp:BoundColumn DataField="ID" Visible="false"></asp:BoundColumn>
                                                <asp:TemplateColumn HeaderText="R�ponse pour" HeaderStyle-CssClass="tableHeader" ItemStyle-Width="30%" ItemStyle-HorizontalAlign="Left">
                                                    <ItemStyle HorizontalAlign="left" />
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblEntityTypeID" Text='<%# DataBinder.Eval(Container, "DataItem.EntityTypeID") %>' Visible="false"></asp:Label>
                                                        <asp:Label runat="server" CssClass="blackblodtext" ID="lblEntityTypeName" Text='<%# DataBinder.Eval(Container, "DataItem.EntityTypeName")  + ": " %>' Visible="true" Font-Bold="true"></asp:Label>
                                                        <asp:Label runat="server" ID="lblEntityID" Text='<%# DataBinder.Eval(Container, "DataItem.ID") %>' Visible="false"></asp:Label>
                                                        <asp:Label runat="server" ID="lblEntityName" Text='<%# DataBinder.Eval(Container, "DataItem.Name") %>' Visible="true"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" ItemStyle-Width="35%">
                                                    <ItemTemplate>
                                                        <table width="100%" border="0">
                                                            <tr>
                                                                <td align="center"><asp:RadioButton ID="rdUndecided" GroupName="Decision" Checked="true" runat="server" /></td>
                                                                <td align="center"><asp:RadioButton ID="rdApprove" GroupName="Decision" runat="server" /></td>
                                                                <td align="center"><asp:RadioButton ID="rdDeny" GroupName="Decision" runat="server" /></td>
                                                            </tr>
                                                        </table>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" HeaderText="Message" ItemStyle-Width="35%" ItemStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtMessage" runat="server" CssClass="altText" TextMode="MultiLine" Rows="2" Width="150"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                            </Columns>
                                            </asp:DataGrid>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="10"><%--FB 2274 2501--%>
                                          <asp:Table runat="server" ID="tblInstances" Visible="false" BorderColor="black" Width="100%" CellPadding="0" CellSpacing="0" BorderWidth="1" BorderStyle="Inset">
                                            <asp:TableRow>
                                                <asp:TableCell>                                                
                                                    <asp:DataGrid ID="dgInstanceList" runat="server" AutoGenerateColumns="False" Font-Names="Verdana" Font-Size="Small"
                                                     Width="100%" BorderColor="blue" BorderStyle="Solid" BorderWidth="0px" OnItemCreated="BindRowsDeleteMessage" Visible="false"
                                                     OnEditCommand="EditConference" ShowHeader="false" OnCancelCommand="DeleteConference" OnDeleteCommand="ManageConference" OnUpdateCommand="CloneConference" style="border-collapse:separate" GridLines="None"> <%-- Edited for FF and FB 2050 --%>
                                                        <HeaderStyle CssClass="tableHeader" Height ="30"/>
                                                        <Columns>
                                                            <asp:BoundColumn DataField="ConferenceID" Visible="false"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="IsHost" Visible="false"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="IsParticipant" Visible="false"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="ConferenceDuration" Visible="false"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="ConferenceStatus" Visible="false"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="IsRecur" Visible="false"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="OpenForRegistration" Visible="false"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="Conference_Id" Visible="false"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="ConferenceDateTime" Visible="false"></asp:BoundColumn>
															<asp:BoundColumn DataField="organizationName" Visible="false"></asp:BoundColumn>
                                                            <asp:TemplateColumn ItemStyle-Width="80%" HeaderStyle-Width="80%" FooterStyle-HorizontalAlign="Right">
                                                                <ItemTemplate>
                                                                    <table width="100%">
                                                                        <tr style='background-color: <%#((Request.QueryString["hf"] == "1") && DataBinder.Eval(Container, "DataItem.isVMR").ToString().Equals("1") && DataBinder.Eval(Container, "DataItem.OpenForRegistration").ToString().Equals("1") && !(DataBinder.Eval(Container, "DataItem.RemPublicVMRCount").ToString().Equals("0")))?"#F0E68C":"Transparent"%>'> <%--FB 2550--%>
                                                                            <td width="5%" align="left" valign="top" ><%--FB 2274--%>
                                                                                <asp:Label ID="lblUniqueID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ConferenceUniqueID") %>'></asp:Label>
                                                                            </td>
                                                                            <td width="20%" align="left" valign="top">
                                                                                <asp:Image ID="imgRecur" Height="20" Width="20" ImageUrl="image/recurring.gif" runat="server" ToolTip="conf�rence r�current" Visible='<%# DataBinder.Eval(Container, "DataItem.IsRecur").ToString().Trim().Equals("1") %>' />
                                                                                <asp:Label ID="lblConfName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ConferenceName") %>'></asp:Label>
                                                                            </td>
																			<td width="10%" align="left" valign="top" >
                                                                                <asp:Label ID="lblOrgName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.organizationName") %>'></asp:Label>
                                                                            </td>
                                                                            <td width="10%" align="left" valign="top" style="display:none"><%--FB 2448--%>
                                                                                <asp:Label ID="lblisVMR" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.isVMR") %>'></asp:Label>
                                                                            </td>
                                                                            <%--FB 2550 Starts--%>
                                                                            <td width="10%" align="left" valign="top" style="display:none">
                                                                                   <asp:Label ID="lblPublicVMRCount" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.RemPublicVMRCount") %>'></asp:Label>
                                                                            </td>
                                                                            <td width="10%" align="left" valign="top" style="display:none">
                                                                                <asp:Label ID="Label3" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.isVMRJoin") %>'></asp:Label>
                                                                            </td>
                                                                            <%--FB 2550 Ends--%>
                                                                            <td width="10%" align="left" valign="top">
                                                                                <asp:Label ID="lblDateTime" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ConferenceDateTime")%>' Visible='<%# !DataBinder.Eval(Container, "DataItem.IsRecur").ToString().Trim().Equals("1") %>'></asp:Label>
                                                                                <asp:LinkButton ID="btnGetInstances" Text="Visualiser toutes les Instances" runat="server" CommandName="Edit" Visible='<%# DataBinder.Eval(Container, "DataItem.IsRecur").ToString().Trim().Equals("1") %>' OnClientClick="javascript:DataLoading('1');"></asp:LinkButton>
                                                                            </td>
                                                                            <td width="10%" align="left" valign="top">
                                                                                <asp:Label ID="lblConferenceType" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ConferenceTypeDescription")%>'></asp:Label>
                                                                            </td>
                                                                            <td width="10%" align="left" valign="top"><%--FB 2501--%>
                                                                                <asp:Label ID="lblConferenceMode" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.StartMode")%>'></asp:Label>
                                                                            </td>
	                                                                            <td width="5%" align="left" valign="top"><%--FB 2274--%>
                                                                                <asp:Label ID="lblDuration" runat="server"></asp:Label>
                                                                            </td>
                                                                            <td id="Td2" width="5%" align="left" valign="top" runat="server" visible='<%# (Request.QueryString["t"] != "6") %>' >
                                                                                <asp:Label ID="lblOwner"  runat="server"></asp:Label>
                                                                            </td>
                                                                            <td id="Td3" width="5%" align="left" valign="top" runat="server" visible='<%# (Request.QueryString["t"] != "6") %>' >
                                                                                <asp:Label ID="lblAttendee" runat="server"></asp:Label>
                                                                            </td>
                                                                            <td id="Td1" width="30%" runat="server" valign="top" visible='<%# (Request.QueryString["t"] != "6") %>'>
                                                                                    <table width="100%" cellspacing="3">
                                                                                        <tr>
                                                                                            <td align="left" valign="top" ><asp:LinkButton ID="btnViewDetails" runat=server Text="Visualisation" Visible='<%# (Request.QueryString["hf"] != "1") %>'></asp:LinkButton></td><%--FB 2501--%>
                                                                                            <td align="left" visible='<%# (Request.QueryString["hf"] != "1") %>'><asp:LinkButton Text="D�doubler" runat="server" ID="btnClone" CommandName="Update" Visible='<%# ( (Request.QueryString["hf"] != "1") && (DataBinder.Eval(Container, "DataItem.IsHost").ToString().Equals("1") || (Int32.Parse(Session["admin"].ToString()) > 0))) %>' OnClientClick="javascript:DataLoading('1');"></asp:LinkButton></td>
                                                                                            <td align="left" visible='<%# (Request.QueryString["hf"] != "1") %>'><asp:LinkButton Text="Supprimer" runat="server" ID="btnDelete" CommandName="Cancel" Visible='<%# ( (Request.QueryString["hf"] != "1") && !DataBinder.Eval(Container, "DataItem.ConferenceStatus").ToString().Equals("7")) && (DataBinder.Eval(Container, "DataItem.IsHost").ToString().Equals("1") || (Int32.Parse(Session["admin"].ToString()) >= 1)) %>' OnClientClick="javascript:DataLoading('1');"></asp:LinkButton></td>
                                                                                            <td align="left" visible='<%# (Request.QueryString["hf"] != "1") %>'><asp:LinkButton Text="Editer" runat="server" ID="btnEdit" CommandName="Edit" Visible='<%# ((Request.QueryString["hf"] != "1") && !DataBinder.Eval(Container, "DataItem.ConferenceStatus").ToString().Equals("7")) && (DataBinder.Eval(Container, "DataItem.IsHost").ToString().Equals("1") || (Int32.Parse(Session["admin"].ToString()) >= 1)) %>' OnClientClick="javascript:DataLoading('1');"></asp:LinkButton></td>
                                                                                            <td align="left" valign="top" visible='<%# (Request.QueryString["hf"] != "1") %>'><asp:LinkButton Text="G�rer" runat="server" ID="btnManage" CommandArgument="1" CommandName="Delete" Visible='<%# (Request.QueryString["hf"] != "1") && (DataBinder.Eval(Container, "DataItem.IsHost").ToString().Equals("1") || Int32.Parse(Session["admin"].ToString()) >= 1) %>' OnClientClick="javascript:DataLoading('1');"></asp:LinkButton></td><%--FB 2501--%>
                                                                                            <td align="left" visible='<%# ( (Request.QueryString["hf"] == "1") && DataBinder.Eval(Container, "DataItem.OpenForRegistration").ToString().Equals("1") && (DataBinder.Eval(Container, "DataItem.isVMRJoin").ToString().Equals("1"))) %>'><asp:LinkButton Text="Join" runat="server" ID="btnJoin" CommandArgument="2" CommandName="Delete" Visible='<%# ( (Request.QueryString["hf"] == "1") && DataBinder.Eval(Container, "DataItem.OpenForRegistration").ToString().Equals("1") && (DataBinder.Eval(Container, "DataItem.isVMRJoin").ToString().Equals("1"))) %>' OnClientClick="javascript:DataLoading('1');"></asp:LinkButton></td> <%--FB 2550--%>
                                                                                            <td align="left" visible='<%# (Request.QueryString["hf"] != "1") %>'><asp:LinkButton Text="Condition" runat="server" ID="btnApprovalStatus" Visible='<%# (Request.QueryString["t"] == "5")%>'></asp:LinkButton></td>
                                    <%--                                                        <td align="left" ><asp:LinkButton ID="btnViewDetails" runat=server Text="View" Visible='<%# (Request.QueryString["hf"] != "1") %>'></asp:LinkButton></td>
                                                                                            <td align="left" visible='<%# (Request.QueryString["hf"] != "1") %>'><asp:LinkButton Text="D�doubler" runat="server" ID="btnClone" CommandName="Update" Visible='<%# ( (Request.QueryString["hf"] != "1") && (DataBinder.Eval(Container, "DataItem.IsHost").ToString().Equals("1") || (Int32.Parse(Session["admin"].ToString()) > 0))) %>' OnClientClick="javascript:DataLoading('1');"></asp:LinkButton></td>
                                                                                            <td align="left" visible='<%# (Request.QueryString["hf"] != "1") %>'><asp:LinkButton Text="Supprimer" runat="server" ID="btnDelete" CommandName="Cancel" Visible='<%# ( (Request.QueryString["hf"] != "1") && !DataBinder.Eval(Container, "DataItem.ConferenceStatus").ToString().Equals("7")) && (DataBinder.Eval(Container, "DataItem.IsHost").ToString().Equals("1") || (Int32.Parse(Session["admin"].ToString()) >= 1)) %>' OnClientClick="javascript:DataLoading('1');"></asp:LinkButton></td>
                                                                                            <td align="left" visible='<%# (Request.QueryString["hf"] != "1") %>'><asp:LinkButton Text="Edit" runat="server" ID="btnEdit" CommandName="Edit" Visible='<%# ((Request.QueryString["hf"] != "1") && !DataBinder.Eval(Container, "DataItem.ConferenceStatus").ToString().Equals("7")) && (DataBinder.Eval(Container, "DataItem.IsHost").ToString().Equals("1") || (Int32.Parse(Session["admin"].ToString()) >= 1)) %>' OnClientClick="javascript:DataLoading('1');"></asp:LinkButton></td>
                                                                                            <td align="left" visible='<%# (Request.QueryString["hf"] != "1") %>'><asp:LinkButton Text="G�rer" runat="server" ID="btnManage" CommandArgument="1" CommandName="Delete" Visible='<%# (Request.QueryString["hf"] != "1") && (DataBinder.Eval(Container, "DataItem.IsHost").ToString().Equals("1") || Int32.Parse(Session["admin"].ToString()) >= 1) %>' OnClientClick="javascript:DataLoading('1');"></asp:LinkButton></td>
                                                                                            <td align="left" visible='<%# ( (Request.QueryString["hf"] == "1") && DataBinder.Eval(Container, "DataItem.OpenForRegistration").ToString().Equals("1")) %>'><asp:LinkButton Text="Join" runat="server" ID="btnJoin" CommandArgument="2" CommandName="Delete" Visible='<%# ( (Request.QueryString["hf"] == "1") && DataBinder.Eval(Container, "DataItem.OpenForRegistration").ToString().Equals("1")) %>' OnClientClick="javascript:DataLoading('1');"></asp:LinkButton></td>
                                                                                            <td align="left" visible='<%# (Request.QueryString["hf"] != "1") %>'><asp:LinkButton Text="Condition" runat="server" ID="btnApprovalStatus" Visible='<%# (Request.QueryString["t"] == "5")%>'></asp:LinkButton></td>--%>
                                                                                        </tr>
                                                                                    </table>
                                                                            </td>
                                                                            <td id="Td4" width="40%" valign="top" runat="server" visible='<%# (Request.QueryString["t"] == "6") %>'>
                                                                                <asp:DataGrid runat="server" ShowHeader="false" BorderWidth="0" BorderColor="black" BorderStyle="solid" ID="dgResponse" Width="100%"
                                                                                 AutoGenerateColumns="false" CellPadding="0" CellSpacing="0" style="border-collapse:separate"> <%--Edited for FF--%>
                                                                                <Columns>
                                                                                    <asp:BoundColumn DataField="ID" Visible="false"></asp:BoundColumn>
                                                                                    <asp:TemplateColumn HeaderText="R�ponse�pour" HeaderStyle-CssClass="tableHeader" ItemStyle-Width="30%" ItemStyle-HorizontalAlign="Left">
                                                                                        <ItemStyle HorizontalAlign="left" />
                                                                                        <ItemTemplate>
                                                                                            <asp:Label runat="server" ID="lblEntityTypeID" Text='<%# DataBinder.Eval(Container, "DataItem.EntityTypeID") %>' Visible="false"></asp:Label>
                                                                                            <asp:Label runat="server" CssClass="blackblodtext" ID="lblEntityTypeName" Text='<%# DataBinder.Eval(Container, "DataItem.EntityTypeName")  + ": " %>' Visible="true" Font-Bold="true"></asp:Label>
                                                                                            <asp:Label runat="server" CssClass="tableBody" ID="lblEntityID" Text='<%# DataBinder.Eval(Container, "DataItem.ID") %>' Visible="false"></asp:Label>
                                                                                            <asp:Label runat="server" CssClass="tableBody" ID="lblEntityName" Text='<%# DataBinder.Eval(Container, "DataItem.Name") %>' Visible="true"></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                    <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" ItemStyle-Width="35%">
                                                                                        <ItemTemplate>
                                                                                            <table width="100%" border="0">
                                                                                                <tr>
                                                                                                    <td align="center"><asp:RadioButton ID="rdUndecided" GroupName="Decision" Checked="true" runat="server" /></td>
                                                                                                    <td align="center"><asp:RadioButton ID="rdApprove" GroupName="Decision" runat="server" /></td>
                                                                                                    <td align="center"><asp:RadioButton ID="rdDeny" GroupName="Decision" runat="server" /></td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                    <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Message" ItemStyle-Width="35%" ItemStyle-HorizontalAlign="Left">
                                                                                        <ItemTemplate>
                                                                                            <asp:TextBox ID="txtMessage" runat="server" CssClass="altText" TextMode="MultiLine" Rows="2" Width="150"></asp:TextBox>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                </Columns>
                                                                                
                                                                                </asp:DataGrid>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:BoundColumn DataField="StartMode" Visible="false"></asp:BoundColumn><%--FB 2501--%>
                                                            <asp:BoundColumn DataField="isVMR" Visible="false"></asp:BoundColumn> <%--FB 2550--%>
                                                            <asp:BoundColumn DataField="VMRType" Visible="false"></asp:BoundColumn> <%--FB 2550--%>
                                                        </Columns>
                                                    </asp:DataGrid>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                          </asp:Table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="9" align="center">
                                            <asp:Table ID="tblJoin" runat="server" Width="90%" Visible="false" BackColor="lemonchiffon">
                                                <asp:TableRow>
                                                    <asp:TableCell>
                                                        <table width="100%">
                                                            <tr>
                                                                <%--Window Dressing--%>
                                                                <td align="left" class="blackblodtext" colspan="6"><asp:Label ID="lblHdTxt" runat="server" Text="" ></asp:Label></td> <%--FB 2550--%>
                                                            </tr>
                                                            <tr>
                                                                <%--Window Dressing--%>
                                                                <td align="right" class="blackblodtext">Pr�nom<span class="reqfldText">*</span></td>
                                                                <td align="left">
                                                                    <asp:TextBox ID="txtJFirstName" runat="server" CssClass="altText"></asp:TextBox>
                                                                     <asp:RegularExpressionValidator ID="RegularExpressionValidator2" ControlToValidate="txtJFirstName" Display="dynamic" runat="server" 
                                                                        ErrorMessage="& < > + % \ / ? | = ! ` [ ] { } # $ @ et ~ sont des characteres invalides." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+?|!`\[\]{}\=@#$%&~]*$"></asp:RegularExpressionValidator><%--FB 1888--%>
                                                                    <asp:RequiredFieldValidator ID="regFN" ValidationGroup="Submit" ErrorMessage="Requis" runat="server" Display="dynamic" ControlToValidate="txtJFirstName" ></asp:RequiredFieldValidator>
                                                                </td>
                                                                <%--Window Dressing--%>
                                                                <td align="right" class="blackblodtext">Dernier Nom<span class="reqfldText">*</span></td>
                                                                <td align="left">
                                                                    <asp:TextBox ID="txtJLastName" runat="server" CssClass="altText"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ID="reqLN" ValidationGroup="Submit" ErrorMessage="Requis" runat="server" Display="dynamic" ControlToValidate="txtJLastName" ></asp:RequiredFieldValidator>
                                                                     <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txtJLastName" Display="dynamic" runat="server" 
                                                                        ErrorMessage="& < > + % \ / ? | = ! ` [ ] { } # $ @ et ~ sont des characteres invalides." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+?|!`\[\]{}\=@#$%&~]*$"></asp:RegularExpressionValidator><%--FB 1888 ([A-Za-z0-9\\.\\@_\~#!`$^/ \-]+)--%>
                                                                </td>
                                                                <%--Window Dressing--%>
                                                                <td align="right" class="blackblodtext">Email<span class="reqfldText">*</span></td>
                                                                <td align="left">
                                                                    <asp:TextBox ID="txtJEmailAddress" runat="server" CssClass="altText"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ID="reqEmail" ValidationGroup="Submit" ErrorMessage="Requis" runat="server" Display="dynamic" ControlToValidate="txtJEmailAddress" ></asp:RequiredFieldValidator>
                                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator5" ValidationGroup="Submit" ControlToValidate="txtJEmailAddress" Display="dynamic" runat="server" 
                                                                        ErrorMessage="<br>Invalid email address." ValidationExpression="^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"></asp:RegularExpressionValidator><%--fogbugz case 389--%>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <%--Window Dressing--%>
                                                                <td align="left" colspan="6" class="blackblodtext"><asp:Label ID="lblRooms" runat="server" Text="salle de conf�rences" Visible="false"></asp:Label> </td> <%--FB 2550--%>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                                <td colspan="5" align="left">
                                                                        <!--[Viek] Code changed as a fix for Issue 295 -->
                                                                        <asp:RadioButtonList ID="rdJRooms" RepeatDirection="Vertical" RepeatLayout="table" runat="server" DataSource='<%# GetJLocations((String)DataBinder.Eval(Container.DataItem, "Conference_Id").ToString(),(String)DataBinder.Eval(Container.DataItem, "ConferenceType").ToString()) %>' DataTextField="Name" DataValueField="ID" OnSelectedIndexChanged="ChangeJoinOption" AutoPostBack="true"></asp:RadioButtonList>
                                                                        <asp:Label ID="lblJNoRooms" Text="Aucune chambre n'est disponible pour cette conf�rence" CssClass="lblError" Visible="false" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="6">  
                                                                    <asp:Table ID="tblEA" runat="server" Width="100%" Visible="false">
                                                                        <asp:TableRow>
                                                                            <asp:TableCell HorizontalAlign="center">
                                                                                <table width="80%" border="0" cellpadding="2" cellspacing="5" bgcolor="lightgrey">
                                                                                    <tr>
                                                                                         <%--Window Dressing--%>
                                                                                        <td align="left" colspan="4" class="blackblodtext" >Please provide the connection renseignements below</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                         <%--Window Dressing--%>
                                                                                        <td align="right" class="blackblodtext">Connection Genre</td>
                                                                                        <td align="left">
                                                                                            <asp:DropDownList ID="lstJConnectionType" runat="server" CssClass="altLongSelectFormat">
                                                                                                <asp:ListItem Selected="True" Value="1" Text="Dial-In to MCU"></asp:ListItem>
                                                                                                <asp:ListItem Value="0" Text="Dial-Out from MCU"></asp:ListItem>
                                                                                            </asp:DropDownList>
                                                                                        </td>
                                                                                         <%--Window Dressing--%>
                                                                                        <td align="right" class="blackblodtext">Reseau ext�rieur</td>
                                                                                        <td align="left">
                                                                                            <asp:CheckBox ID="chkJIsOutSide" runat="server" Checked="true" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                         <%--Window Dressing--%>
                                                                                        <td align="right" class="blackblodtext">Genre d'interface</td>
                                                                                        <td align="left">
                                                                                            <asp:DropDownList ID="lstJProtocol" runat="server" CssClass="altLongSelectFormat">
                                                                                                <asp:ListItem Selected="True" Value="IP" Text="IP"></asp:ListItem>
                                                                                                <asp:ListItem Value="ISDN" Text="ISDN"></asp:ListItem>
                                                                                            </asp:DropDownList>
                                                                                        </td>
                                                                                         <%--Window Dressing--%>
                                                                                        <td align="right" class="blackblodtext">Address<span class="reqfldText">*</span></td>
                                                                                        <td align="left">
                                                                                            <asp:TextBox ID="txtJAddress" CssClass="altText" runat="server"></asp:TextBox>
                                                                                            <asp:RequiredFieldValidator ID="reqJAddress" Display="dynamic" ErrorMessage="Requis" runat="server" ControlToValidate="txtJAddress"></asp:RequiredFieldValidator>
                                                                                             <%--FB 1972--%>
                                                                                             <asp:RegularExpressionValidator ID="regConfName" ControlToValidate="txtJAddress" Display="dynamic" runat="server" 
                                                                                                ErrorMessage=" <br> + - & < > = ( ) ? [ ] { } % et * sont des characteres invalides." ValidationExpression="[A-Za-z0-9\\.\\@',:;\x22;_\~#!`$^/ \-]+"></asp:RegularExpressionValidator>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </asp:TableCell>
                                                                        </asp:TableRow>
                                                                    </asp:Table>
                                                                    <asp:Table ID="tblRA" runat="server" Visible="false" Width="100%">
                                                                        <asp:TableRow>
                                                                            <asp:TableCell HorizontalAlign="center">
                                                                                <%--Window Dressing--%>
                                                                                <table width="80%" cellpadding="2" cellspacing="5" class="tableBody">
                                                                                    <tr>
                                                                                        <td align="left">Please choose one of the following salle de conf�rences � attend.</td>
                                                                                    </tr>
                                                                                </table>
                                                                            </asp:TableCell>
                                                                        </asp:TableRow>
                                                                    </asp:Table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right" colspan="3">
                                                                    <asp:Button ID="btnJCancel" Text="Annuler" CssClass="altShortBlueButtonFormat" OnClick="CancelJoin" runat="server" />
                                                                </td>
                                                                <td align="left" colspan="3">
                                                                    <asp:Button ID="btnJSubmit" ValidationGroup="Submit" Text="Soumettre" CssClass="altShortBlueButtonFormat" CommandArgument="3" CommandName="Delete" runat="server" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                            </asp:Table>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                            <FooterTemplate>
                                <br /><br />
                                <asp:Button ID="btnApproveConference" CssClass="altLongBlueButtonFormat" Visible='<%# Request.QueryString["t"].ToString().Equals("6") %>' OnClick="ApproveConference" OnClientClick="javascript:CountChecked();" runat="server" Text="Soumettre" />
                                <br />
                            <%--Window Dressing--%>
                                <asp:Label ID="Label1" Text="Nombre total d'enregistrements:" runat="server" Font-Bold="true" CssClass="subtitleblueblodtext"></asp:Label>
                                <asp:Label runat="server" ID="lblTotalRecords" Text='<%# DataBinder.Eval(Container, "DataItem.TotalRecords")%>'></asp:Label>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn Visible="false" ItemStyle-Width="20px" ItemStyle-HorizontalAlign="Center" HeaderText="�tat de la connexion"></asp:BoundColumn>
                        <asp:BoundColumn DataField="StartMode" Visible="false"></asp:BoundColumn><%--FB 2501--%>
                        <asp:BoundColumn DataField="isVMR" Visible="false"></asp:BoundColumn> <%--FB 2550--%>
                        <asp:BoundColumn DataField="VMRType" Visible="false"></asp:BoundColumn> <%--FB 2550--%>
                    </Columns>
                    <ItemStyle Height="20px" />
                
                </asp:DataGrid>
                    <asp:Label ID="lblNoConferences" runat="server" Text="Aucune Conf�rence" Visible="False" CssClass="lblError"></asp:Label>&nbsp;
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Table ID="tblPage" Visible="false" runat="server">
                        <asp:TableRow ID="TableRow1" runat="server">
                            <%--Window Dressing--%>
                            <asp:TableCell ID="tc1" Font-Bold="True" Font-Names="Verdana" Font-Size="Small" ForeColor="Blue" CssClass="subtitleblueblodtext" runat="server">Pages: </asp:TableCell>
                            <asp:TableCell ID="tc2" runat="server"></asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>

                </td>
            </tr>            
            <tr runat="server" id="trGoToLobby" visible="false" align="right"> <%--Fogbugz case 158--%>
                <td colspan="3" align="right">
                    <table cellpadding="2" cellspacing="2" border="0">
                        <tr>
                            <td>
                                <asp:Button id="btnGoBack" CssClass="altShortBlueButtonFormat" Text="Retourner" runat="server" OnClick="GoToLobby" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
</center>
                <input type="hidden" id="helpPage" value="107" runat="server" />
                <input type="hidden" id="txtSelectionCount" runat="server" runat="server" />
                <input type="hidden" id="txtSearchType" runat="server" />
                <input type="hidden" id="txtSortBy" runat="server" />
                <input type="hidden" id="txtPublic" runat="server" />
                <input type="hidden" id="txtConferenceSearchType" runat="server" />
                <input type="hidden" id="txtCOMConfigPath" runat="server" value="C:\VRMSchemas_v1.8.3\COMConfig.xml" />
                <input type="hidden" id="txtASPILConfigPath" runat="server" value="C:\VRMSchemas_v1.8.3\" />
<script language="javascript">
    //FB Case 723 - Retirer Calendrier dropdown from Approval screen
    if (document.getElementById("lstListType") != null)
    {
        if(document.getElementById("lstListType").value == 6)
            document.getElementById("lstCalendar").style.display="none";
        else
            document.getElementById("lstCalendar").style.display="";  
            
      if(document.getElementById("lstListType").value == 2 || document.getElementById("lstListType").value == 3)
       {
            var tim = '15000';
            
            var chk = document.getElementById("Refreshchk");
            
            if(chk)
            {
                if(chk.checked == true)
                    tim = '45000'
            }
            
            setTimeout("DataLoading(1);__doPostBack('btnRefreshList', '')", tim);            
       }
       //FB 1985 - Starts
       if('<%=Application["Client"]%>'.toUpperCase() == "DISNEY")
        {
         if(document.getElementById("lstListType").value == 4 || document.getElementById("lstListType").value == 5 || document.getElementById("lstListType").value == 6) 
           {
             var tim = '15000';
             var chk = document.getElementById("Refreshchk");
         
             if(chk)
              {
                if(chk.checked == true)
                  tim = '45000'
              }
                setTimeout("DataLoading(1);__doPostBack('btnRefreshList', '')", tim); 
           }
        }  
        //FB 1985 - End          
    }
   
   if(document.getElementById("ImageButton3") != null)
   {
     if (isExpressUser == 1) //FB 1779
         document.getElementById("ImageButton3").style.display = "none";
     else
       document.getElementById("ImageButton3").style.display = "block";

}

//FB 2487 - Start USSIE - Success, ERREUR - Error
var obj = document.getElementById("errLabel");
if (obj != null) {
    var strInput = obj.innerHTML.toUpperCase();

    if (((strInput.indexOf("SUCC") > -1) || (strInput.indexOf("USSIE") > -1)) && !(strInput.indexOf("UNSUCC") > -1) && !(strInput.indexOf("ERREUR") > -1)) {
        obj.setAttribute("class", "lblMessage");
        obj.setAttribute("className", "lblMessage");
    }
    else {
        obj.setAttribute("class", "lblError");
        obj.setAttribute("className", "lblError");
    }
}
//FB 2487 - End  

//FB 2598 Starts

    function removeOption()
    {
        if("<%=Session["EnableCallmonitor"]%>" == "0")
        {
        
        var x=document.getElementById("lstCalendar");
        x.remove(4);
        x.remove(3);
        }
       
    }

    removeOption();
    
//FB 2598 Ends

</script>
                
</form>
</body>
</html>
<%--code added for Soft Edge button--%>
<%--FB 1822--%>
<%--<script type="text/javascript" src="inc/softedge.js"></script>--%>
<% if (Request.QueryString["hf"] == null) { %>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" --><% } else { 
    if (!Request.QueryString["hf"].ToString().Equals("1")) {    
%>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" --> 
<%}
} %>

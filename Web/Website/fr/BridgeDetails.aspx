<%@ Page Language="C#" Inherits="ns_Bridges.BridgeDetails" Buffer="true" %>
<%@ Register Assembly="MetaBuilders.WebControls.ComboBox" Namespace="MetaBuilders.WebControls" TagPrefix="mbcbb" %> <%--FB 1938--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">  <!-- FB 2050 -->
<meta http-equiv="X-UA-Compatible" content="IE=6" /> <!-- FB 2050 -->

<!--Window Dressing-->
<% if (Request.QueryString["hf"] != null && Request.QueryString["hf"].ToString().Equals("1"))
   {
%>
        <!-- #INCLUDE FILE="inc/maintop4.aspx" --> 
<% }
else
    {
%>
        <!-- #INCLUDE FILE="inc/maintopNET.aspx" --> 
<%
    }
%>

<script type="text/javascript">

  var servertoday = new Date(); //FB 1938

</script>
<script language="javascript">
//FB 1766 - Start
function disableservname()
{
    var enable = document.getElementById("chkEnableIVR");
    if((enable != null) && (enable.checked != true) )
    {
      reqServName.innerText = "";
      document.getElementById("txtIVRName").value = "";
      document.getElementById("txtIVRName").disabled = true;
    }
    else
    {
      document.getElementById("txtIVRName").disabled = false;
    }
}
function RequireIVRName()
{   
    var txtIVRName = document.getElementById("txtIVRName");
    var enable = document.getElementById("chkEnableIVR");
    if (enable != null)
    {
        if (enable.checked && txtIVRName.value == '')
        {
        reqServName.style.display = 'block';
        reqServName.innerText = 'Please specify IVR Service Name';
        document.getElementById("txtIVRName").focus();
        return false;
        }
    }
}
//FB 1766 - End
function DeleteApprover(id)
{
    document.getElementById("hdnApprover" + id).value="";
    document.getElementById("txtApprover" + id).value="";
    document.getElementById("hdnApprover" + id + "_1").value="";
    document.getElementById("txtApprover" + id + "_1").value="";
}

function SavePassword()
{
    document.getElementById("<%=confPassword.ClientID %>").value = document.getElementById("<%=txtPassword1.ClientID %>").value;
    for(i=1;i<5;i++)
    {
        document.getElementById("txtApprover" + i).value = document.getElementById("txtApprover" + i + "_1").value;
        document.getElementById("hdnApprover" + i).value = document.getElementById("hdnApprover" + i + "_1").value;
    }
}

function ShowISDN(obj)
{
    if (obj.checked)
        document.getElementById("trISDN").style.display="";
    else
        document.getElementById("trISDN").style.display="none";
}

function getYourOwnEmailList (i)	// -1, 0, 1, 2
{
	//url = "dispatcher/conferencedispatcher.asp?frm=approverNET&frmname=frmBridgesetting&no=" + i + "&cmd=GetEmailList&emailListPage=1&wintype=pop"; //Login Management	
	url = "emaillist2main.aspx?t=e&frm=approverNET&fn=frmBridgesetting&n=" + i; //Login Management
	
	if (!window.winrtc) {	// has not yet been defined
		winrtc = window.open(url, "", "width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
		winrtc.focus();
	} else // has been defined
	    if (!winrtc.closed) {     // still open
	    	winrtc.close();
	        winrtc = window.open(url, "", "width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
			winrtc.focus();
		} else {
	        winrtc = window.open(url, "", "width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
	        winrtc.focus();
		}
}
//FB 1937 - Start
function ShowHideRow(frm)
{
    var trow1 = document.getElementById("trMPIServices");
    var trow2 = document.getElementById("trIPServices");
    var trow3 = document.getElementById("trMCUCards");
    var trow4 = document.getElementById("trISDN1");
    var trow5 = document.getElementById("trE164Services"); //FB 2636
    var chkExpand = document.getElementById("chkExpandCollapse")
    var frmCheck, frmVal
    frmVal = 0;
    if(frm)
        frmVal = frm;
    
    if(chkExpand)
    {
        if(frmVal == 1)
            chkExpand.checked = true;
        
        frmCheck = chkExpand.checked;
        
        if (frmCheck == true)
        {   
            if(trow1 != null)
                trow1.style.display = "none";
            if(trow2 != null)
                trow2.style.display = "none";
            if(trow3 != null)
                trow3.style.display = "none";
            if(trow4 != null)
                trow4.style.display = "none"; 
            if(trow5 != null) //FB 2636
                trow5.style.display = "none";                                        
        }
        else
        {
            if(trow1 != null)
                trow1.style.display = "block";
            if(trow2 != null)
                trow2.style.display = "block";
            if(trow3 != null)
                trow3.style.display = "block";
            if(trow4 != null)
                trow4.style.display = "block"; 
            if(trow5 != null) //FB 2636
                trow5.style.display = "block";   
        }
    }
    
    if(frmVal == 1)
        return true;
}


function fnShowHide(arg)
{
    var varg
    
    if(arg == 2)
    {
        varg = 2;
        arg = 1;
    }
    
    if (arg == '1')    
      document.getElementById("UsageReportDiv").style.display = 'block';
    else if (arg == '0')
    {
      document.getElementById("UsageReportDiv").style.display = 'none';
      return false;
    }
    
    if(varg == 2)
        ShowHideRow(1);
    
    return true; 
}

function fnCheck()
{
    var rptTime = document.getElementById("ReportTime_Text");
    if(rptTime)
    {
        if(rptTime.value == "")
        {
            document.getElementById("reqStartTime").style.display = 'block';
            return false;
        }
        else if('<%=Session["timeFormat"]%>' == "0")
        {
            if(rptTime.value.search(/[0-2][0-9]:[0-5][0-9]/)==-1 ) 
            {
                document.getElementById("regStartTime").style.display = 'block';
                rptTime.focus();
                return false;
            }
        }
        else if (rptTime.value.search(/[0|1][0-9]:[0-5][0-9] ?[a|A|p|P][M|m]/)==-1)
        {
            document.getElementById("regStartTime").style.display = 'block';
            rptTime.focus();
            return false;
        }   
    }
    
    var rptDate = document.getElementById("ReportDate");
    if(rptDate)
    {
        if(rptDate.value == "")
        {
            document.getElementById("reqStartData").style.display = 'block';
            return false;
        }
        else if (rptDate.value.search(/(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d|(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d/)==-1) 
        {
            document.getElementById("regStartDate").style.display = 'block';
            rptTime.focus();
            return false;
        }
    }
    

    return true;
}  
//FB 1937 - End

</script>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf8" />
    <title>MCU D�tails</title>
    <script type="text/javascript" src="inc/functions.js"></script>
	<%--FB 1938--%>
    <script type="text/javascript" src="script/cal-flat.js"></script>
    <script type="text/javascript" src="lang/calendar-en.js"></script>
    <script type="text/javascript" src="script/calendar-setup.js"></script>
    <script type="text/javascript" src="script/calendar-flat-setup.js"></script>
    <link rel="stylesheet" type="text/css" media="all" href="css/calendar-blue.css" /> <%--FB 1938--%> <%--FB 1982--%>
    
</head>
<body>
    <form id="frmMCUManagement" runat="server" method="post" onsubmit="return true">
    <input id="confPassword" runat="server" type="hidden" />
    <input id="txtApprover1_1" runat="server" type="hidden" />
    <input id="txtApprover2_1" runat="server" type="hidden" />
    <input id="txtApprover3_1" runat="server" type="hidden" />
    <input id="txtApprover4_1" runat="server" type="hidden" />
    <input id="hdnApprover1_1" runat="server" type="hidden" />
    <input id="hdnApprover2_1" runat="server" type="hidden" />
    <input id="hdnApprover3_1" runat="server" type="hidden" />
    <input id="hdnApprover4_1" runat="server" type="hidden" />
    <%--Code changed for FB 1425 QA Bug -Start--%>
      <input type="hidden" id="hdntzone" runat="server"/>
      <%--Code changed for FB 1425 QA Bug -End--%>
      <input type="hidden" id="hdnMcuCount" runat="server"/> <%--FB 2486 --%>
      <%--FB 1938 - Start --%>
     <div id="UsageReportDiv"  runat="server" align="center" style="top: 170px;left:105px; POSITION: absolute; WIDTH:92%; HEIGHT: 350px;VISIBILITY: visible; Z-INDEX: 3; display:none"> 
      <table width="75%" border="1" style="border-color:Blue;" cellpadding="0" cellspacing="0">
        <tr>
            <td>
              <table cellpadding="2" cellspacing="1"  width="100%" class="tableBody" align="center">
                 <tr>
                    <td  class="subtitleblueblodtext" align="center" colspan="2">
                        MCU utilisation des ports
                    </td>            
                 </tr>
                 <tr>
                    <td width="27%"  style="font-weight:bold" class="blackblodtext" align="left"> <!-- FB 2050 -->
                        Total Audio / vid�o utilis�
                    </td> 
                    <td style="font-weight:bold" class="blackblodtext" align="left">
                      :&nbsp;<asp:Label ID="LblVideoused" runat="server" ></asp:Label>
                   </td>           
                 </tr>
                 <tr>
                    <td width="20%" style="font-weight:bold" class="blackblodtext" align="left"> <!-- FB 2050 -->
                        Total Audio / vid�o disponible 
                    </td> 
                    <td style="font-weight:bold" class="blackblodtext" align="left">
                      :&nbsp;<asp:Label ID="LblVideoavail" runat="server" ></asp:Label>
                   </td>            
                 </tr>
                 <tr>
                    <td width="20%" style="font-weight:bold" class="blackblodtext" align="left"> <!-- FB 2050 -->
                        Audio totale utilis�e uniquement 
                    </td>
                    <td style="font-weight:bold" class="blackblodtext" align="left">
                      :&nbsp;<asp:Label ID="LblAudioused" runat="server" ></asp:Label>
                   </td>             
                 </tr>
                 <tr>
                    <td width="20%" style="font-weight:bold" class="blackblodtext" align="left"> <!-- FB 2050 -->
                         Audio totale disponible uniquement
                    </td>
                    <td style="font-weight:bold" class="blackblodtext" align="left">
                      :&nbsp;<asp:Label ID="LblAudioavail" runat="server" ></asp:Label>
                   </td>             
                 </tr>
                 <tr>
                    <td  class="subtitleblueblodtext" align="center" colspan="2">
                        Conf�rences
                    </td>            
                 </tr>
                 <tr>
                    <td align="center" colspan="2">
                       <div  style="width: 95%;height: 350px;overflow: auto;" id="dd" runat="server"  >
                            <asp:DataGrid ID="dgUsageReport" GridLines="Horizontal" BorderStyle="Solid" BorderColor="blue" Width="100%" AutoGenerateColumns="false" ShowFooter="false" runat="server" style="border-collapse:separate">
                                 <HeaderStyle HorizontalAlign="Left" />
                                <Columns> <%-- FB 2050 Starts --%>
                                    <asp:BoundColumn HeaderText="Nom <br> Conf�rence" DataField="ConfName"  ItemStyle-CssClass="tableBody"  HeaderStyle-CssClass="tableHeader" HeaderStyle-Width="20%" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ></asp:BoundColumn>
                                    <asp:BoundColumn HeaderText="Conf #" DataField="ConfID" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn HeaderText="Date de d�but" DataField="StartDate" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn HeaderText="Date de fin" DataField="EndDate" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ></asp:BoundColumn>
                                    <asp:BoundColumn HeaderText="Ports audio <br> utilis�" DataField="AudioOnly" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn HeaderText="Vid�o Port <br> utilis�" DataField="AudioVideo" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                </Columns> <%-- FB 2050 Ends --%>
                            </asp:DataGrid>
                            <asp:Label ID="lblDetails" Text="Aucun d�tail trouv�e" CssClass="lblError" runat="server"></asp:Label>
                        </div>
                    </td>
                  </tr>
                  <tr>
                   <td align="center" colspan="2">
                      <asp:Button ID="BtnClose" Text="Close" CssClass="altShortBlueButtonFormat" Runat="server" OnClientClick="javascript:return fnShowHide('0')"></asp:Button>
                   </td>
                  </tr>
                </table>
            </td>
        </tr>
    </table>
    </div>
      <%--FB 1938 - End --%>
    
    <div>
      <input type="hidden" id="helpPage" value="65">
        
        <table width="100%">
            <tr>
                <td align="center">
                    <h3>
                        <asp:Label ID="lblHeader" runat="server"></asp:Label>
                    </h3>
                </td>
            </tr>
            <tr>
                <td align="right" style="font-weight:bold">
                  <asp:Label ID="lblRequired" ForeColor="red" Font-Size="XX-Small" Text="* Obligatoire" runat="server" Font-Italic="true"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError"></asp:Label>
                    <asp:CustomValidator id="customvalidation" runat="server" display="dynamic" OnServerValidate="ValidateIP" />           
                </td>
            </tr>
            <tr>
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <SPAN class=subtitleblueblodtext>Configuration de base</SPAN>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table width="90%" cellspacing="3" cellpadding="2">
                        <tr>
                            <td width="20%" align="right" style="font-weight:bold" class="blackblodtext">MCU Nom<span class="reqfldstarText">*</span></td> <%--window dressing--%>
                            <td width="30%" align="left">
                                <asp:TextBox ID="txtMCUID" Visible="false" runat="server"></asp:TextBox>
                                <asp:TextBox ID="txtMCUName" CssClass="altText" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="txtMCUName" ValidationGroup="Submit" Display="dynamic" ErrorMessage="Obligatoire"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="regMCUName" ControlToValidate="txtMCUName" ValidationGroup="Submit" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ et &#34; sont des characteres invalides." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                            </td>
                            <td width="20%" align="right" style="font-weight:bold" class="blackblodtext">MCU Ouverture de session<span class="reqfldstarText">*</span></td> <%--window dressing--%>
                            <td width="30%" align="left">
                                <asp:TextBox CssClass="altText" ID="txtMCULogin" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="txtMCULogin" ValidationGroup="Submit" Display="dynamic" ErrorMessage="Obligatoire"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator4" ValidationGroup="Submit" ControlToValidate="txtMCULogin" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ et &#34; sont des characteres invalides." ValidationExpression="^(a-z|A-Z|0-9)*[^\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()'~]*$"></asp:RegularExpressionValidator> <%--Polycom CMA--%>
                            </td>
                        </tr>
                        <tr>
                            <td width="20%" align="right" style="font-weight:bold" class="blackblodtext">MCU Mot de Passe<%--<span class="reqfldstarText">*</span>FB 1408--%></td> <%--window dressing--%>
                            <td width="30%" align="left">
                                <asp:TextBox ID="txtPassword1" TextMode="Password" CssClass="altText" runat="server"></asp:TextBox>
                                <%--<asp:RequiredFieldValidator ControlToValidate="txtPassword1" ValidationGroup="Submit" Display="dynamic" ErrorMessage="Requis" runat="server"></asp:RequiredFieldValidator>FB 1408--%>
                                <%--<asp:RegularExpressionValidator ID="reg1" ValidationExpression="{1,128}" ControlToValidate="txtPassword1" Display="Dynamic" ErrorMessage="<br>Only numeric passwords between 1000-9999 are allowed." runat="server"></asp:RegularExpressionValidator>
                                --%><asp:CompareValidator ID="cmpValPassword1" runat="server" ValidationGroup="Submit" ControlToCompare="txtPassword2"
                                    ControlToValidate="txtPassword1" Display="Dynamic" ErrorMessage="<br>Re-enter password."></asp:CompareValidator> <%--FB 2232--%>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator5" ValidationGroup="Submit" ControlToValidate="txtPassword1" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > + \ ( ) ; | ^ = ` , [ ] { } : @ ~ et &#34; sont des characteres invalides." ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>+;|`,\[\]{}\x22;=^:@&()~]*$"></asp:RegularExpressionValidator>
                            </td>
                            <td width="20%" align="right" style="font-weight:bold" class="blackblodtext">Retaper le mot de passe<%--<span class="reqfldstarText">*</span>FB 1408--%></td> <%--window dressing--%>
                            <td width="30%" align="left">
                                <asp:TextBox CssClass="altText" ID="txtPassword2" TextMode="Password" runat="server"></asp:TextBox>
                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator7" ValidationGroup="Submit" ControlToValidate="txtPassword2" Display="dynamic" ErrorMessage="Requis" runat="server"></asp:RequiredFieldValidator>FB 1408--%>
                                <asp:CompareValidator ID="cmp2" runat="server" ControlToCompare="txtPassword1"
                                    ControlToValidate="txtPassword2" Display="Dynamic" ValidationGroup="Submit" ErrorMessage="<br>Passwords do not match."></asp:CompareValidator> <%--FB 2232--%>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator6" ValidationGroup="Submit" ControlToValidate="txtPassword2" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > + \ ( ) ; | ^ = ` , [ ] { } : @ ~ et &#34; sont des characteres invalides." ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>+;|`,\[\]{}\x22;=^:@&()~]*$"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <%--Code changed for FB 1425 QA Bug -Start--%>
                            <td width="20%" align="right" style="font-weight:bold" id ="TzTD1" runat="server" class="blackblodtext">MCU Fuseau horaire</td>
                            <td width="30%" align="left" id ="TzTD2" runat="server">
                                <asp:DropDownList ID="lstTimezone" CssClass="altSelectFormat" DataTextField="timezoneName" DataValueField="timezoneID" runat="server"></asp:DropDownList>
                            </td>
                            <%--Code changed for FB 1425 QA Bug -End--%>
                            <td width="20%" align="right" style="font-weight:bold" class="blackblodtext">MCU Genre</td> <%--window dressing--%>
                            <td width="30%" align="left">
                                <asp:DropDownList ID="lstMCUType" CssClass="altSelectFormat" OnSelectedIndexChanged="ChangeFirmwareVersion" AutoPostBack="true" DataTextField="name" DataValueField="ID" runat="server"></asp:DropDownList>
                                <asp:DropDownList ID="lstInterfaceType" Visible="false" DataTextField="interfaceType" DataValueField="ID" runat="server"></asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td width="20%" align="right" style="font-weight:bold" class="blackblodtext">�tat MCU</td> <%--window dressing--%>
                            <td width="30%" align="left">
                                <asp:DropDownList ID="lstStatus" CssClass="altSelectFormat" runat="server" DataTextField="name" DataValueField="ID"></asp:DropDownList>
                            </td>
                            <td width="20%" align="right" style="font-weight:bold" class="blackblodtext">virtuelle MCU</td> <%--window dressing--%>
                            <td width="30%" align="left">
                                <asp:CheckBox ID="chkIsVirtual" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td width="20%" align="right" style="font-weight:bold" class="blackblodtext">Administrateur<span class="reqfldstarText">*</span></td> <%--window dressing--%>
                            <td width="30%" align="left">
                                <asp:TextBox ID="txtApprover4" ReadOnly="true" EnableViewState="true" CssClass="altText" runat="server"></asp:TextBox>
                                <a href="javascript: getYourOwnEmailList(3);" onmouseover="window.status='';return true;"><img border="0" src="image/edit.gif" alt="edit" WIDTH="17" HEIGHT="15"></a>                                
                                <asp:TextBox ID="hdnApprover4" runat="server" Width="0" Height="0" ForeColor="transparent" BorderColor="transparent"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="Submit" ErrorMessage="Requis" ControlToValidate="hdnApprover4" Display="dynamic" runat="server"></asp:RequiredFieldValidator>
                            </td>
                            <td width="20%" align="right" style="font-weight:bold" class="blackblodtext">version du micrologiciel</td> <%--window dressing--%>
                            <td width="30%" align="left">
                                <asp:DropDownList ID="lstFirmwareVersion" CssClass="altSelectFormat" DataTextField="Name" DataValueField="ID" runat="server"></asp:DropDownList>
                            </td>
                        </tr>
                        
                        <%--Api port starts--%>
                        <tr>
                        <td width="20%" align="right" style="font-weight:bold" class="blackblodtext">Prifice API</td>
                        <td width="30%" align="left">
                        <asp:TextBox ID="txtapiportno" CssClass="altText" runat="server" MaxLength="5"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator13" ValidationGroup="Submit" ControlToValidate="txtapiportno" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="Les valeurs num�riques�seulement." ValidationExpression="\d+"></asp:RegularExpressionValidator>
                        </td>
                        <%--FB 1920 - Starts--%>
                        <td width="20%" align="right" style="font-weight:bold" class="blackblodtext">
                            <asp:Label ID="lblIsPublic" runat="server">Publique</asp:Label>
                        </td>
                        <td width="30%" align="left">
                            <asp:CheckBox ID="chkbxIsPublic" runat="server" />
                        </td>
                        <%--FB 1920 - Ends--%>
                        </tr>
                        <%--Api port ends--%>
                        <%--FB 1937--%>
                        <tr>
                            <td width="20%" align="right" style="font-weight:bold" class="blackblodtext">Total audio / vid�o les ports</td> <%--window dressing FB 1937--%>
                            <td width="30%" align="left">
                                <asp:TextBox CssClass="altText" ID="txtMaxVideoCalls" runat="server"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator8" ValidationGroup="Submit" ControlToValidate="txtMaxVideoCalls" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>Numreic values only." ValidationExpression="\d+"></asp:RegularExpressionValidator>
                            </td>
                            <%--FB 2501 CallMonitor Favourite Starts--%>
                            <td width="20%" align="right" style="font-weight:bold;display:none;" class="blackblodtext">D�finir favori</td> <%--FB 2591--%>
                            <td width="30%" align="left">
                                <asp:CheckBox Visible="false" ID="chkSetFavourite" runat="server" /> <%--FB 2591--%>
                            </td>
                            <%--FB 2501 Ends--%>
                        </tr>
                        <tr id="trAudio" runat="server">
                            <td  width="20%" align="right" style="font-weight:bold" class="blackblodtext">Total des seuls ports audio</td> <%--window dressing FB 1937--%>
                            <td  width="30" align="left"> 
                                <asp:TextBox ID="txtMaxAudioCalls" CssClass="altText" runat="server"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator7" ValidationGroup="Submit" ControlToValidate="txtMaxAudioCalls" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>Numreic values only." ValidationExpression="\d+"></asp:RegularExpressionValidator>
                            </td>
                            <td width="20%">&nbsp;</td>
                            <td width="30%">&nbsp;</td>              
                        </tr>                         
                    </table>
                </td>
            </tr>
            <tr>
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <SPAN class=subtitleblueblodtext>MCU Approbateurs</SPAN>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table width="90%" cellspacing="3" cellpadding="2">
                        <tr>
                            <td width="20%" align="right" style="font-weight:bold" class="blackblodtext">Approbateur principal</td><%--window dressing--%>
                            <td width="800px" align="left"><%--Edited For FF--%>
                                <asp:TextBox ID="txtApprover1" ReadOnly="true" EnableViewState="true" CssClass="altText" runat="server"></asp:TextBox>
                                <a href="javascript: getYourOwnEmailList(0);" onmouseover="window.status='';return true;"><img border="0" src="image/edit.gif" style="vertical-align:top" alt="edit" WIDTH="17" HEIGHT="15"></a><%--Edited For FF--%>
                                <img border="0" src="image/btn_delete.gif" style="vertical-align:top" alt="delete" WIDTH="16" HEIGHT="16" onclick="javascript:DeleteApprover(1)"><%--Edited For FF--%>
                            </td>
                            <td width="50%" align="left">
                                <asp:TextBox ID="hdnApprover1" Width="0" Height="0" ForeColor="transparent" BackColor="transparent" BorderWidth="0" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td width="20%" align="right" style="font-weight:bold" class="blackblodtext">Approbateur secondaire 1</td> <%--window dressing--%>
                            <td width="30%" align="left">
                                <asp:TextBox ID="txtApprover2" ReadOnly="true" CssClass="altText" runat="server"></asp:TextBox>
                                <a href="javascript: getYourOwnEmailList(1);" onmouseover="window.status='';return true;"><img border="0" src="image/edit.gif" style="vertical-align:top" alt="edit" WIDTH="17" HEIGHT="15"></a><%--Edited For FF--%>
                                <img border="0" src="image/btn_delete.gif" alt="delete" WIDTH="16" HEIGHT="16" style="vertical-align:top" onclick="javascript:DeleteApprover(2)"><%--Edited For FF--%>
                            </td>
                            <td width="50%" align="left">
                                <asp:TextBox ID="hdnApprover2" Width="0" Height="0" ForeColor="transparent" BackColor="transparent" BorderWidth="0" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td width="20%" align="right" style="font-weight:bold" class="blackblodtext">Approbateur secondaire 2</td> <%--window dressing--%>
                            <td width="30%" align="left">
                                <asp:TextBox ID="txtApprover3" ReadOnly="true" CssClass="altText" runat="server"></asp:TextBox>
                                <a href="javascript: getYourOwnEmailList(2);" onmouseover="window.status='';return true;"><img border="0" src="image/edit.gif" style="vertical-align:top" alt="edit" WIDTH="17" HEIGHT="15"></a> <%--Edited For FF--%>                           
                                <img border="0" src="image/btn_delete.gif" alt="delete" WIDTH="16" HEIGHT="16" style="vertical-align:top" onclick="javascript:DeleteApprover(3)"><%--Edited For FF--%>
                            </td>
                            <td width="50%" align="left">
                                <asp:TextBox ID="hdnApprover3" Width="0" Height="0" ForeColor="transparent" BackColor="transparent" BorderWidth="0" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                    </table>                 
                </td>
            </tr>
            <tr>
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <asp:Label CssClass="subtitleblueblodtext" ID="lblHeader1" runat="server" Text="MGC Accord MCU Configuration" ></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr> <%-- Split tr1 row as tr1, tr2 and tr5 for FB 1937--%>
            <tr id="tr3" runat="server">
                <td align="center" >
                    <table width="90%">
                        <tr>
                            <td width="20%" align="right" style="font-weight:bold" class="blackblodtext">Port A</td><%--window dressing--%>
                            <td width="30%" align="left">
                                <asp:TextBox ID="txtPortA" CssClass="altText" runat="server"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" Enabled="true" ValidationGroup="Submit" ControlToValidate="txtPortA" ValidationExpression="^([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}$" ErrorMessage="Invalid IP Address" Display="dynamic" runat="server"></asp:RegularExpressionValidator>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" Enabled="true" ValidationGroup="Submit" ControlToValidate="txtPortA" ErrorMessage="Requis" Display="dynamic" runat="server"></asp:RequiredFieldValidator>
                            </td>
                            <td width="20%" align="right" style="font-weight:bold" class="blackblodtext">Port B</td><%--window dressing--%>
                            <td width="30%" align="left">
                                <asp:TextBox ID="txtPortB" CssClass="altText" runat="server"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" Enabled="true" ValidationGroup="Submit" ControlToValidate="txtPortB" ValidationExpression="^([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}$" ErrorMessage="Invalid IP Address" Display="dynamic" runat="server"></asp:RegularExpressionValidator>
<%--                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" Enabled="true" ControlToValidate="txtPortB" ErrorMessage="Requis" Display="dynamic" runat="server"></asp:RequiredFieldValidator>
--%>                            </td>
                        </tr>

                        <%--FB 2003 - Start--%>
                        <tr>
                            <td width="20%" align="right" style="font-weight:bold" class="blackblodtext">RNIS pr�fixe audio</td>
                            <td width="30%" align="left">
                               <asp:TextBox CssClass="altText" ID="txtISDNAudioPref" MaxLength="4" runat="server"></asp:TextBox>
			                   <asp:RegularExpressionValidator ID="regISDNAudioPref" ControlToValidate="txtISDNAudioPref" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="Les valeurs num�riques�seulement." ValidationGroup="Submit" ValidationExpression="^(\(|\d| |-|\))*$"></asp:RegularExpressionValidator>
			                </td>
                            <td width="20%" align="right" style="font-weight:bold" class="blackblodtext">Pr�fixe vid�o RNIS</td>
                            <td width="30%" align="left">
                               <asp:TextBox CssClass="altText" ID="txtISDNVideoPref" MaxLength="4" runat="server"></asp:TextBox>
			                   <asp:RegularExpressionValidator ID="regISDNVideoPref" ControlToValidate="txtISDNVideoPref" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="Les valeurs num�riques�seulement." ValidationGroup="Submit" ValidationExpression="^(\(|\d| |-|\))*$"></asp:RegularExpressionValidator>
			                </td>
                        </tr>
                        <%--FB 2003 - End--%>
						<tr id="tr6" runat="server">
                            <td  width="20%" align="right" style="font-weight:bold" class="blackblodtext">RNIS passerelle</td> 
                            <td  width="30" align="left"> 
                                <asp:TextBox ID="txtISDNGateway" CssClass="altText" runat="server"></asp:TextBox>
                                
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator14" Enabled="true" ValidationGroup="Submit" ControlToValidate="txtISDNGateway" ValidationExpression="^([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}$" ErrorMessage="Invalid IP Address" Display="dynamic" runat="server"></asp:RegularExpressionValidator>
                            </td>
                            <td width="20%">&nbsp;</td>
                            <td width="30%">&nbsp;</td>
                  
                         
                         </tr>
                          <%--FB 2610 starts--%>
                         <tr id="trTempExtNumber" runat="server" visible="true">
                            <td width="20%" align="right" valign="top" style="font-weight: bold" class="blackblodtext">
                                Ext #
                            </td>
                            <td  width="30%" align="left">
                                <asp:TextBox ID="txtTempExtNumber" MaxLength="50" CssClass="altText" runat="server"></asp:TextBox>
                            </td>
                           <%--FB 2636 starts--%>
                            <td width="20%" align="right" valign="top" style="font-weight: bold" class="blackblodtext">
                                <asp:Label runat="server" ID="lblE164">Use E.164 Dialing</asp:Label> 
                            </td>
                            <td>
                                <asp:CheckBox ID="chkE164" runat="server" />
                            </td>
                        </tr>
                        <tr>
                         <td width="20%" align="right" valign="top" style="font-weight: bold" class="blackblodtext">
                                <asp:Label runat="server" ID="lblEH323">Use H.323 Dialing</asp:Label> 
                            </td>
                            <td>
                                <asp:CheckBox ID="chkH323" runat="server" />
                            </td>
                        </tr>
                        <%--FB 2636 End --%>
                        <%--FB 2610 Ends--%>
                         
                         
                    </table>
                </td>
            </tr>
            <tr id="tr1" runat="server">
                <td align="center" >
                    <table width="90%" >
                        <tr>
                            <td width="20%" align="right" style="font-weight:bold" class="blackblodtext">Adresse IP du port de contr�le<span class="reqfldstarText">*</span></td><%--window dressing--%>
                            <td width="30%" align="left">
                                <asp:TextBox ID="txtPortP" CssClass="altText" runat="server"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="regPortP" Enabled="true" ValidationGroup="Submit" ControlToValidate="txtPortP" ValidationExpression="^([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}$" ErrorMessage="Invalid IP Address" Display="dynamic" runat="server"></asp:RegularExpressionValidator>
                                <asp:RequiredFieldValidator ID="reqPortP" Enabled="true" ValidationGroup="Submit" ControlToValidate="txtPortP" ErrorMessage="Requis" Display="dynamic" runat="server"></asp:RequiredFieldValidator>
                            </td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <%--FB 2016 Starts--%>
                        <tr width="100%" id="trConfServiceID" runat="server">
                            <td width="20%" align="right" style="font-weight:bold" class="blackblodtext">Conf�rence service d'identification<span class="reqfldstarText">*</span></td><%--window dressing--%>
                            <td width="30%" align="left">
                                <asp:TextBox ID="txtConfServiceID" CssClass="altText" runat="server"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RngValidConfServiceID" Enabled="true" ValidationGroup="Submit" ControlToValidate="txtConfServiceID" ValidationExpression="^\d{0,9}$" ErrorMessage="Invalid Service ID" Display="dynamic" runat="server"></asp:RegularExpressionValidator>
                                <%--<asp:rangevalidator id="RngValidConfServiceID" runat="server"
                                    display="dynamic" controltovalidate="txtConfServiceID" errormessage="Invalid Conference Service ID"
                                   minimumvalue="0" maximumvalue="1000"  type="Integer"></asp:rangevalidator>--%>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" Enabled="true" ValidationGroup="Submit" ControlToValidate="txtConfServiceID" ErrorMessage="Requis" Display="dynamic" runat="server"></asp:RequiredFieldValidator>
                            </td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <%--FB 2016 Ends--%>
                         <%--FB 2591 Starts--%>
                        <%--FB 2427 Starts--%>
                        <tr width="100%" id="trProfileID" runat="server">
                            <td width="20%" align="right" style="font-weight:bold" class="blackblodtext">Num�ro du profil</td>
                            <td width="30%" align="left">
                                <%--<asp:TextBox ID="txtProfileID" CssClass="altText" runat="server"></asp:TextBox>--%>
                                <asp:DropDownList Width="37%" ID="txtProfileID" CssClass="alt2SelectFormat" runat="server" DataValueField="Id" DataTextField="Name" >
									<asp:ListItem Selected="True" Text="None" Value="0"></asp:ListItem>
                                </asp:DropDownList>
                                <%--<asp:RegularExpressionValidator ID="RegtxtProfileID" Enabled="true" ValidationGroup="Submit" ControlToValidate="txtProfileID" ValidationExpression="^\d{0,9}$" ErrorMessage="Invalid Profile ID" Display="dynamic" runat="server"></asp:RegularExpressionValidator>--%>
                                <asp:Button ID="btnGetProfiles" runat="server" CssClass="altShortBlueButtonFormat" Text="Get Profiles" OnClick="GetProfiles" />
                            </td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <%--FB 2427 Ends--%>
                        <%--FB 2591 Ends--%>
                        <%--FB 1907 - Start--%>
                        <tr width="100%" id="trEnableIVR" runat="server">
                            <td width="20%" align="right" style="font-weight: bold" class="blackblodtext">
                                Permis IVR
                            </td>
                            <td width="15%" align="left">
                                <asp:CheckBox ID="chkEnableIVR" runat="server" onclick="javascript:disableservname();" />
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr id="trIVRName" runat="server">
                            <td width="20%" align="right" valign="top" style="font-weight: bold" class="blackblodtext">
                                Nom du service IVR
                            </td>
                            <td align="left" width="55%">
                                <asp:TextBox ID="txtIVRName" MaxLength="99" CssClass="altText" runat="server" Width="128px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reqServName" runat="server" ControlToValidate="lstIVR"
                                    Display="dynamic" SetFocusOnError="true" Text="" ValidationGroup="Submit"></asp:RequiredFieldValidator>
                            </td>
                            <td>
                            
                            </td>
                            <td style="width: 20%; display: none" align="right" valign="top">
                                <asp:DropDownList ID="lstIVR" runat="server" CssClass="altSelectFormat" DataTextField="Name"
                                    DataValueField="ID">
                                    <asp:ListItem Text="Please Select..." Value="-1"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr id="trEnableRecord" runat="server">
                            <td width="20%" align="right" valign="top" style="font-weight: bold" class="blackblodtext">
                                Permis Recording
                            </td>
                            <td align="left">
                                <asp:CheckBox ID="chkEnableRecord" runat="server" />
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr id="trEnableLPR" runat="server">
                            <td width="20%" align="right" valign="top" style="font-weight: bold" class="blackblodtext">
                                Enable LPR
                            </td>
                            <td align="left">
                                <asp:CheckBox ID="chkLPR" runat="server" />
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr id="trEnableMessage" runat="server"><%--FB 2486--%>
                            <td width="20%" align="right" valign="top" style="font-weight: bold" class="blackblodtext">
                                Activer la messagerie
                            </td>
                            <td align="left">
                                <asp:CheckBox ID="chkMessage" runat="server" />
                            </td>
                            <td>
                            </td>
                        </tr>
                         <%--FB 2610 starts--%>
                         <tr id="trExtensionNumber" runat="server" visible="true">
                            <td width="20%" align="right" valign="top" style="font-weight: bold" class="blackblodtext">
                                Ext #
                            </td>
                            <td  width="30%" align="left">
                                <asp:TextBox ID="txtExtNumber" MaxLength="50" CssClass="altText" runat="server"></asp:TextBox>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <%--FB 2610 Ends--%>
                        
                     </table>
                </td>
            </tr><%-- FB 1937--%>
             <tr id="tr5" runat="server">
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td>&nbsp;</td>
                            <td align="right" valign="top" style="font-weight:bold" id="tdExpandCollapse"  class="subtitleblueblodtext">
                                Recapitulation<input id="chkExpandCollapse" type="checkbox" onclick="javascript:ShowHideRow()" runat="server" class="btprint" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>             
            <tr id="tr2" runat="server">
                <td align="center" >
                    <table width="90%" >
                        <%--FB 1907 - End--%>
                        <tr id="trMCUCards" runat="server" align="center"><%--window dressing--%>
                            <td colspan="4">
                             <%--FB 1766 - Start
                             <table width="100%">
                                <tr>
                                   <td width="20%" align="right" style="font-weight:bold" class="blackblodtext">Permis IVR</td>
                                   <td width="15%" align="left">
                                      <asp:CheckBox ID="chkEnableIVR" runat="server" onclick="javascript:disableservname();" />
                                   </td>
                                   <td>&nbsp;</td>
                                   <td>&nbsp;</td>                                
                                </tr>
                                <tr>
                                   <td width="20%" align="right" valign="top" style="font-weight:bold" class="blackblodtext">IVR Service Nom</td>
                                   <td align="left">
                                      <asp:TextBox ID="txtIVRName" MaxLength="99" CssClass="altText" runat="server"></asp:TextBox>
                                   </td>
                                   <td width="25%">
                                      <asp:RequiredFieldValidator ID="reqServName" runat="server" ControlToValidate="lstIVR" Display="dynamic" SetFocusOnError="true" Text="" ValidationGroup="Submit"></asp:RequiredFieldValidator>
                                   </td>
                                   <td style="width:20%;display:none" align="right" valign="top"> 
                                      <asp:DropDownList ID="lstIVR" runat="server"  CssClass="altSelectFormat" DataTextField="Name" DataValueField="ID">
                                      <asp:ListItem Text="Please Select..." Value="-1"></asp:ListItem>
                                      </asp:DropDownList>
                                   </td>
                                </tr>
                              </table>
                              FB 1766 - End--%>
                              <h5 class="subtitleblueblodtext">MCU Cartes</h5>
                              <asp:DataGrid ID="dgMCUCards" GridLines="Horizontal" BorderStyle="Solid" BorderColor="blue" Width="75%" AutoGenerateColumns="false" ShowFooter="true" runat="server" style="border-collapse:separate"> <%--Edited for FF--%>
                                 <HeaderStyle CssClass="tableHeader" Height="30" HorizontalAlign="Left" VerticalAlign="Middle" />
                                 <ItemStyle HorizontalAlign="left" />
                                 <Columns>
                                    <asp:BoundColumn DataField="ID" Visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody" DataField="Name" HeaderText="Nom de la carte" ItemStyle-Width="40%" HeaderStyle-Width="50%"></asp:BoundColumn><%--window dressing--%>
                                    <asp:TemplateColumn HeaderText="Nombre maximal de connexions @ 384 Kbps" ItemStyle-CssClass="tableBody"  HeaderStyle-CssClass="tableHeader" FooterStyle-BorderStyle="NotSet"><%--window dressing--%>
                                       <ItemTemplate>
                                          <asp:TextBox ID="txtMaxCalls" runat="server" CssClass="altText" Text='<%#DataBinder.Eval(Container, "DataItem.MaximumCalls") %>'></asp:TextBox>
                                          <asp:RangeValidator runat="server" ID="rangeMaxCalls" ValidationGroup="Submit" ControlToValidate="txtMaxCalls" ErrorMessage="please enter an integer value from 0 to 100" MinimumValue="0" MaximumValue="100" Type="integer" SetFocusOnError="true"></asp:RangeValidator> 
                                       </ItemTemplate>
                                    </asp:TemplateColumn>
                                 </Columns>
                              </asp:DataGrid>
                            </td>
                        </tr>
                        <tr id="trIPServices" runat="server" align="center"><%--window dressing--%>
                            <td colspan="4">
                                <h5 class="subtitleblueblodtext">les services IP</h5>
                                <asp:DataGrid ID="dgIPServices" GridLines="Horizontal" BorderStyle="Solid" BorderColor="blue" Width="75%" AutoGenerateColumns="false" ShowFooter="true" runat="server">
                                    <HeaderStyle CssClass="tableHeader" Height="30" />
                                    <FooterStyle Height="30" />
                                    <Columns>
<%--                                        <asp:BoundColumn DataField="ID" Visible="false"></asp:BoundColumn>
--%>                                        <asp:TemplateColumn  ItemStyle-CssClass="tableBody" >
                                            <ItemTemplate>
                                            <%--Window Dressing--%>
                                                <asp:DropDownList ID="lstOrder" CssClass="altText" EnableViewState="true" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ChangeIPOrder" SelectedValue='<%#DataBinder.Eval(Container, "DataItem.SortID") %>'>
                                                    <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                                    <asp:ListItem Text="4" Value="4"></asp:ListItem>
                                                    <asp:ListItem Text="5" Value="5"></asp:ListItem>
                                                    <asp:ListItem Text="6" Value="6"></asp:ListItem>
                                                    <asp:ListItem Text="7" Value="7"></asp:ListItem>
                                                    <asp:ListItem Text="8" Value="8"></asp:ListItem>
                                                    <asp:ListItem Text="9" Value="9"></asp:ListItem>
                                                    <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Nom" ItemStyle-CssClass="tableBody" >
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtName" CssClass="altText" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.name") %>'></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="Submit" ControlToValidate="txtName" Display="dynamic" ErrorMessage="Requis" runat="server" ></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="regIPName" ValidationGroup="Submit" ControlToValidate="txtName" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ et &#34 sont des characteres invalides; ." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Genre d'adresse" ItemStyle-CssClass="tableBody" >
                                            <ItemTemplate>
                                                <asp:DropDownList ID="lstAddressType" DataTextField="Name" DataValueField="ID" CssClass="altText" runat="server" OnInit="BindAddressType" SelectedValue='<%#DataBinder.Eval(Container, "DataItem.addressType") %>'></asp:DropDownList>  
                                                <asp:RequiredFieldValidator ControlToValidate="lstAddressType" ValidationGroup="Submit" Display="dynamic" ErrorMessage="Requis" runat="server" InitialValue="-1" ></asp:RequiredFieldValidator>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Adresse" ItemStyle-CssClass="tableBody" >
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtAddress" CssClass="altText" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.address") %>'></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="Submit" ControlToValidate="txtAddress" Display="dynamic" ErrorMessage="Requis" runat="server" ></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="regIPAddress" ValidationGroup="Submit" ControlToValidate="txtAddress" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ` , [ ] { } : $ @ - ~ et &#34; sont des characteres invalides." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|`,\[\]{}\x22;=^:@$%&()\-'~]*$"></asp:RegularExpressionValidator> <%--FB 2267--%>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="L'acc�s au r�seau" ItemStyle-CssClass="tableBody" >
                                            <ItemTemplate>
                                                <asp:DropDownList ID="lstNetworkAccess" CssClass="altText" runat="server" SelectedValue='<%#DataBinder.Eval(Container, "DataItem.networkAccess") %>'>
                                                    <asp:ListItem Text="Publique (dans le Monde myVRM)" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="Private" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="Les deux" Value="3"></asp:ListItem>
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Usage" ItemStyle-CssClass="tableBody" >
                                            <ItemTemplate>
                                                <asp:DropDownList ID="lstUsage" CssClass="altText" runat="server" SelectedValue='<%#DataBinder.Eval(Container, "DataItem.usage") %>'>
                                                    <asp:ListItem Text="audioconf�rence" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="La vid�oconf�rence" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="Les deux" Value="3"></asp:ListItem>
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                            </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Supprimer" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody" >
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkDelete" runat="server" />
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Button Text="Ajouter" OnClick="AddNewIPService" CssClass="altShortBlueButtonFormat" runat="server" />
                                            </FooterTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                                <asp:Label ID="lblNoIPServices" Text="No IP Services defini" CssClass="lblError" runat="server" Visible="false"></asp:Label>
                                <asp:Button ID="btnAddNewIPService" Text="Ajouter" OnClick="AddNewIPService" CssClass="altShortBlueButtonFormat" runat="server" />
                                </td>
                            </tr>
                        <tr align="center" id="trISDN1" runat="server"><%--window dressing--%> <%--FB 2636  --%>
                                <td colspan="4">
                                
                                <h5 class="subtitleblueblodtext">ISDN Services</h5>
                                <asp:DataGrid ID="dgISDNServices" GridLines="Horizontal" BorderStyle="Solid" BorderColor="blue" Width="75%" AutoGenerateColumns="false" ShowFooter="true" runat="server">
                                    <HeaderStyle CssClass="tableHeader" Height="30" />
                                    <FooterStyle Height="30" />
                                    <Columns>
<%--                                        <asp:BoundColumn DataField="ID" Visible="false"></asp:BoundColumn>
--%>                                        <asp:TemplateColumn ItemStyle-CssClass="tableBody" >
                                            <ItemTemplate>
                                            <%--Window Dressing--%>
                                                <asp:DropDownList ID="lstOrder" CssClass="altText" EnableViewState="true" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ChangeISDNOrder" SelectedValue='<%#DataBinder.Eval(Container, "DataItem.SortID") %>'>
                                                    <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                                    <asp:ListItem Text="4" Value="4"></asp:ListItem>
                                                    <asp:ListItem Text="5" Value="5"></asp:ListItem>
                                                    <asp:ListItem Text="6" Value="6"></asp:ListItem>
                                                    <asp:ListItem Text="7" Value="7"></asp:ListItem>
                                                    <asp:ListItem Text="8" Value="8"></asp:ListItem>
                                                    <asp:ListItem Text="9" Value="9"></asp:ListItem>
                                                    <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Nom" ItemStyle-CssClass="tableBody" >
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtName" CssClass="altText" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.name") %>'></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="reqISDNName" runat="server" ValidationGroup="Submit" Display="dynamic" ControlToValidate="txtName" ErrorMessage="Requis" CssClass="lblError"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="regISDNName" ValidationGroup="Submit" ControlToValidate="txtName" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ et &#34; sont des characteres invalides." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Prefix" ItemStyle-CssClass="tableBody" >
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtPrefix" CssClass="altText" Width="50" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.prefix") %>'></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="reqISDNPrefix" runat="server" ValidationGroup="Submit" Display="dynamic" ControlToValidate="txtPrefix" ErrorMessage="Requis" CssClass="lblError"></asp:RequiredFieldValidator>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Start Range" ItemStyle-CssClass="tableBody" >
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtStartRange" CssClass="altText" Width="80" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.startRange") %>'></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="reqISDNStartRange" ValidationGroup="Submit" runat="server" Display="dynamic" ControlToValidate="txtStartRange" ErrorMessage="Requis" CssClass="lblError"></asp:RequiredFieldValidator>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="End Range" ItemStyle-CssClass="tableBody" >
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtEndRange" CssClass="altText" Width="80" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.endRange") %>'></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="reqISDNEndRange" runat="server" ValidationGroup="Submit" Display="dynamic" ControlToValidate="txtEndRange" ErrorMessage="Requis" CssClass="lblError"></asp:RequiredFieldValidator>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Range Sort Order" ItemStyle-CssClass="tableBody" >
                                            <ItemTemplate>
                                                <asp:DropDownList ID="lstRangeSortOrder" CssClass="altText" runat="server" SelectedValue='<%#DataBinder.Eval(Container, "DataItem.RangeSortOrder") %>'>
                                                    <asp:ListItem Text="Start � End" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="End � Start" Value="1"></asp:ListItem>
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Network access" ItemStyle-CssClass="tableBody" >
                                            <ItemTemplate>
                                                <asp:DropDownList ID="lstNetworkAccess" CssClass="altText" runat="server" SelectedValue='<%#DataBinder.Eval(Container, "DataItem.networkAccess") %>'>
                                                    <asp:ListItem Text="Publique (dans le Monde myVRM)" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="Priv�e" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="Les deux" Value="3"></asp:ListItem>
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Usage" ItemStyle-CssClass="tableBody" >
                                            <ItemTemplate>
                                                <asp:DropDownList ID="lstUsage" CssClass="altText" runat="server" SelectedValue='<%#DataBinder.Eval(Container, "DataItem.usage") %>'>
                                                    <asp:ListItem Text="audioconf�rence" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="La vid�oconf�rence" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="Les deux" Value="3"></asp:ListItem>
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                            </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Supprimer" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody" >
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkDelete" runat="server" />
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Button ID="btnAddISDNService" Text="Ajouter" OnClick="AddNewISDNService" ValidationGroup="New" CssClass="altShortBlueButtonFormat" runat="server" />
                                            </FooterTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                                <asp:Label ID="lblNoISDNServices" Text="No ISDN Services defini" CssClass="lblError" runat="server" Visible="false"></asp:Label>
                                <asp:Button ID="btnAddISDNService" Text="Ajouter" OnClick="AddNewISDNService" ValidationGroup="New" CssClass="altShortBlueButtonFormat" runat="server" />
                                </td>
                            </tr>
                        <tr id="trMPIServices" runat="server" align="center"><%--window dressing--%>
                            <td colspan="4">
                                <h5 class="subtitleblueblodtext">MPI Services</h5>
                                <asp:DataGrid ID="dgMPIServices" GridLines="Horizontal" BorderStyle="Solid" BorderColor="blue" Width="75%" AutoGenerateColumns="false" ShowFooter="true" runat="server">
                                    <HeaderStyle CssClass="tableHeader" Height="30" />
                                    <FooterStyle Height="30" />
                                    <Columns>
<%--                                        <asp:BoundColumn DataField="ID" Visible="false"></asp:BoundColumn>
--%>                                        <asp:TemplateColumn ItemStyle-CssClass="tableBody" >
                                            <ItemTemplate>
                                            <%--Window Dressing--%>
                                                <asp:DropDownList ID="lstOrder" CssClass="altText" EnableViewState="true" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ChangeIPOrder" SelectedValue='<%#DataBinder.Eval(Container, "DataItem.SortID") %>'>
                                                    <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                                    <asp:ListItem Text="4" Value="4"></asp:ListItem>
                                                    <asp:ListItem Text="5" Value="5"></asp:ListItem>
                                                    <asp:ListItem Text="6" Value="6"></asp:ListItem>
                                                    <asp:ListItem Text="7" Value="7"></asp:ListItem>
                                                    <asp:ListItem Text="8" Value="8"></asp:ListItem>
                                                    <asp:ListItem Text="9" Value="9"></asp:ListItem>
                                                    <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Nom" ItemStyle-CssClass="tableBody" >
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtName" CssClass="altText" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.name") %>'></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="Submit" ControlToValidate="txtName" Display="dynamic" ErrorMessage="Requis" runat="server" ></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="regMPIName" ControlToValidate="txtName" ValidationGroup="Submit" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ et &#34; sont des characteres invalides." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Genre d'adresse" ItemStyle-CssClass="tableBody" >
                                            <ItemTemplate>
                                                <asp:DropDownList ID="lstAddressType" DataTextField="Name" DataValueField="ID" CssClass="altText" runat="server" OnInit="BindAddressType" SelectedValue='<%#DataBinder.Eval(Container, "DataItem.addressType") %>'></asp:DropDownList>  
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ControlToValidate="lstAddressType" ValidationGroup="Submit" Display="dynamic" ErrorMessage="Requis" runat="server" InitialValue="-1" ></asp:RequiredFieldValidator>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Adresse" ItemStyle-CssClass="tableBody" >
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtAddress" CssClass="altText" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.address") %>'></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtAddress" ValidationGroup="Submit" Display="dynamic" ErrorMessage="Requis" runat="server" ></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator is="regMPIName" ControlToValidate="txtAddress" ValidationGroup="Submit" Display="dynamic" ErrorMessage="Alphanumeric characters only." ValidationExpression="\w+" runat="server"  ></asp:RegularExpressionValidator>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="L'acc�s au r�seau" ItemStyle-CssClass="tableBody" >
                                            <ItemTemplate>
                                                <asp:DropDownList ID="lstNetworkAccess" CssClass="altText" runat="server" SelectedValue='<%#DataBinder.Eval(Container, "DataItem.networkAccess") %>'>
                                                    <asp:ListItem Text="Publique (dans le Monde myVRM)" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="priv�" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="Les deux" Value="3"></asp:ListItem>
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Usage"  ItemStyle-CssClass="tableBody" > 
                                            <ItemTemplate>
                                                <asp:DropDownList ID="lstUsage" CssClass="altText" runat="server" SelectedValue='<%#DataBinder.Eval(Container, "DataItem.usage") %>'>
                                                    <asp:ListItem Text="audioconf�rence" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="La vid�oconf�rence" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="Les deux" Value="3"></asp:ListItem>
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                            </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Supprimer" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody" >
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkDelete" runat="server" />
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Button ID="btnAddNewMPIService" Text="Ajouter" OnClick="AddNewMPIService" CssClass="altShortBlueButtonFormat" runat="server" />
                                            </FooterTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                                <asp:Label ID="lblNoMPIServices" Text="No MPI Services defini" CssClass="lblError" runat="server" Visible="false"></asp:Label>
                                <asp:Button ID="btnAddNewMPIService" Text="Ajouter" OnClick="AddNewMPIService" CssClass="altShortBlueButtonFormat" runat="server" />
                                </td>
                            </tr>
                             <%--FB 2636 Starts --%>
                        <tr id="trE164Services" runat="server" align="center">
                        <td colspan="4">
                            <h5 class="subtitleblueblodtext">E.164 Services</h5>
                            <asp:DataGrid ID="dgE164Services" GridLines="Horizontal" BorderStyle="Solid" BorderColor="blue" Width="75%" AutoGenerateColumns="false" ShowFooter="true" runat="server">
                                <HeaderStyle CssClass="tableHeader" Height="30" />
                                <FooterStyle Height="30" />
                                <Columns>
                                   <asp:TemplateColumn ItemStyle-CssClass="tableBody" >
                                        <ItemTemplate>
                                        <asp:TextBox ID="lstOrder" CssClass="altText" Width="20px" Enabled="false" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.SortID") %>'></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Start Range" ItemStyle-CssClass="tableBody" >
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtStartRange" CssClass="altText" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.StartRange") %>'></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="ReqFieldValidator2" ValidationGroup="Submit" ControlToValidate="txtStartRange" Display="dynamic" ErrorMessage="Required" runat="server" ></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="regE164StartRange2" ControlToValidate="txtStartRange" ValidationGroup="Submit" Display="dynamic" runat="server"  SetFocusOnError="true" ErrorMessage="Numeric values only." ValidationExpression="\d+"></asp:RegularExpressionValidator>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="End Range" ItemStyle-CssClass="tableBody" >
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtEndRange" CssClass="altText" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.EndRange") %>'></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="ReqFieldValidator3" ValidationGroup="Submit" ControlToValidate="txtEndRange" Display="dynamic" ErrorMessage="Required" runat="server" ></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="regE164StartRange3" ControlToValidate="txtEndRange" ValidationGroup="Submit" Display="dynamic" runat="server"  SetFocusOnError="true" ErrorMessage="Numeric values only." ValidationExpression="\d+"></asp:RegularExpressionValidator>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Delete" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody" >
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkDelete" runat="server" />
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Button ID="btnAddNewE164Service" Text="Add" OnClick="AddNewE164Service" CssClass="altShortBlueButtonFormat" runat="server" />
                                        </FooterTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                            <asp:Label ID="lblNoE164Services" Text="No E.164 Services found" CssClass="lblError" runat="server" Visible="false"></asp:Label>
                            <asp:Button ID="btnAddNewE164Service" Text="Add" OnClick="AddNewE164Service" CssClass="altShortBlueButtonFormat" runat="server" />
                            </td>
                        </tr>
                        <%-- FB 2636 Ends --%>
                    </table>
                </td>
            </tr>
            
            <tr id="tr4" runat="server">
                <td align="center" >
                    <table width="90%">
                        <tr>
                            <td width="20%" align="right" style="font-weight:bold">Port A</td>
                            <td width="30%" align="left">
                                <asp:TextBox ID="txtPortT" CssClass="altText" runat="server"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" Enabled="true" ValidationGroup="Submit" ControlToValidate="txtPortT" ValidationExpression="^([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}$" ErrorMessage="Invalid IP Address" Display="dynamic" runat="server"></asp:RegularExpressionValidator>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" Enabled="true" ValidationGroup="Submit" ControlToValidate="txtPortT" ErrorMessage="Requis" Display="dynamic" runat="server"></asp:RequiredFieldValidator>
                            </td>
                            <td width="20%" align="right" style="font-weight:bold">&nbsp;</td>
                            <td width="30%" align="left">&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
             <tr>
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <SPAN class=subtitleblueblodtext>Alertes</SPAN>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center" >
                    <table width="90%">
                        <tr>
                            <td width="20%" align="right" class="blackblodtext">ISDN Alertes de seuil</td>
                            <td width="30%" align="left">
                                <asp:CheckBox ID="chkISDNThresholdAlert" onclick="javascript:ShowISDN(this)" runat="server" />
                            </td>
                            <td width="20%" align="right" class="blackblodtext">Alertes Dysfonctionnement</td>
                            <td width="30%" align="left">
                                <asp:CheckBox ID="chkMalfunctionAlert" runat="server" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trISDN" style="display:none" runat="server">
                <td align="center" >
                    <table width="90%">
                        <tr>
                            <td width="20%" align="right" class="blackblodtext">MCU ISDN Port Charge</td>
                            <td width="30%" align="left">
                                <asp:TextBox ID="txtMCUISDNPortCharge" CssClass="altText" runat="server"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator9" ValidationGroup="Submit" ControlToValidate="txtMCUISDNPortCharge" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>Invalid format." ValidationExpression="^\d+(?:\.\d{0,2})?$"></asp:RegularExpressionValidator>
                            </td>
                            <td width="20%" align="right" class="blackblodtext">ISDN Line Cost</td>
                            <td width="30%" align="left">
                                <asp:TextBox ID="txtISDNLineCost" CssClass="altText" runat="server"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator10" ValidationGroup="Submit" ControlToValidate="txtISDNLineCost" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>Invalid format." ValidationExpression="^\d+(?:\.\d{0,2})?$"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td width="20%" align="right" class="blackblodtext">RNIS max co�t</td>
                            <td width="30%" align="left">
                                <asp:TextBox ID="txtISDNMaxCost" CssClass="altText" runat="server"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator11" ValidationGroup="Submit" ControlToValidate="txtISDNMaxCost" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>Invalid format." ValidationExpression="^\d+(?:\.\d{0,2})?$"></asp:RegularExpressionValidator>
                            </td>
                            <td width="20%" align="right" class="blackblodtext">RNIS D�lai Seuil</td>
                            <td width="30%" align="left">
                                <asp:RadioButtonList ID="rdISDNThresholdTimeframe" runat="server" CssClass="blackblodtext">
                                    <asp:ListItem Text="Monthly" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="Yearly" Value="2"></asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                         <tr>
                            <td width="20%" align="right" class="blackblodtext">Seuil exprim� en pourcentage RNIS</td>
                            <td width="30%" align="left">
                                <asp:TextBox ID="txtISDNThresholdPercentage" CssClass="altText" runat="server"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator12" ValidationGroup="Submit" ControlToValidate="txtISDNThresholdPercentage" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>Invalid format." ValidationExpression="^\d{0,2}(?:\.\d{0,2})?$"></asp:RegularExpressionValidator>
                            </td>
                            <td width="20%" align="right" style="font-weight:bold">&nbsp;</td>
                            <td width="30%" align="left">&nbsp;</td>
                        </tr>
                   </table>
                </td>
            </tr>
            <%--FB 1642 - DTMF Start--%>
             <tr>
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <SPAN class=subtitleblueblodtext>arrangements DTMF</SPAN>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">                
                    <table cellpadding="3" cellspacing="0" width="90%" border="0">
                         <tr>
                            <td align="right" height="21" style="font-weight:bold" class="blackblodtext" width="20%">
                                Code de la conf�rence pr�paratoire
                            </td>
                            <td style="text-align: left;" width="30%">
                                <asp:TextBox ID="PreConfCode" runat="server" CssClass="altText"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="regPreConfCode" ControlToValidate="PreConfCode" Display="dynamic" runat="server" ValidationGroup="Submit" SetFocusOnError="true" 
                                ErrorMessage="<br> & < > ' + % \ ; ? | ^ = ! ` [ ] { } :  $ @ ~ et &#34; sont des characteres invalides." ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>^+;?|!`\[\]{}\x22;=:@$%&'~]*$"></asp:RegularExpressionValidator>
                            </td>
                            <td align="right" height="21" style="font-weight:bold" class="blackblodtext" width="20%">
                                Amorce Pr�
                            </td>
                            <td style="text-align: left;" width="30%">
                                <asp:TextBox ID="PreLeaderPin" runat="server" CssClass="altText"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="regPreLeaderPin" ControlToValidate="PreLeaderPin" Display="dynamic" runat="server" ValidationGroup="Submit" SetFocusOnError="true" 
                                ErrorMessage="<br> & < > ' + % \ ; ? | ^ = ! ` [ ] { } :  $ @ ~ et &#34; sont des characteres invalides." ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>^+;?|!`\[\]{}\x22;=:@$%&'~]*$"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" height="21" style="font-weight:bold" class="blackblodtext" width="20%">
                                Poster amorce
                            </td>
                            <td style="text-align: left;" width="30%">
                                <asp:TextBox ID="PostLeaderPin" runat="server" CssClass="altText"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="regPostLeaderPin" ControlToValidate="PostLeaderPin" Display="dynamic" runat="server" ValidationGroup="Submit" SetFocusOnError="true" 
                                ErrorMessage="<br> & < > ' + % \ ; ? | ^ = ! ` [ ] { } :  $ @ ~ et &#34; sont des characteres invalides." ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>^+;?|!`\[\]{}\x22;=:@$%&'~]*$"></asp:RegularExpressionValidator>
                            </td>
                            <td colspan="2"></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <%--FB 1938 - Start--%>
            <tr>
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <SPAN class=subtitleblueblodtext>les rapports d'utilisation</SPAN>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="ReportRow" runat="server">
                <td align="center">                
                    <table cellpadding="3" cellspacing="0" width="90%" border="0">
                        <tr>
                            <td width="30%" align="left"> <%-- FB 2050 --%>
                                 <asp:TextBox ID="ReportDate" runat="server" CssClass="altText" Width="35%" ></asp:TextBox>
                                 <img src="image/calendar.gif" border="0" width="20" height="20" id="cal_triggerd" style="cursor: pointer;vertical-align:middle;" title="Date selector" onclick="return showCalendar('<%=ReportDate.ClientID %>', 'cal_triggerd', 1, '<%=format%>');" /> 
                                 <span class="blackblodtext">@</span> 
                                 <mbcbb:combobox id="ReportTime" runat="server" CssClass="altText" Rows="10"  style="width:75px" CausesValidation="True"><%--FB 1982--%>
                                 </mbcbb:combobox>
                                <asp:RequiredFieldValidator ID="reqStartTime" runat="server" ControlToValidate="ReportTime" ValidationGroup="Submit1" Display="Dynamic" SetFocusOnError="true" ErrorMessage="Heure est obligatoire"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="regStartTime" runat="server"  ControlToValidate="ReportTime" ValidationGroup="Submit1" Display="Dynamic"  ErrorMessage="Format temporaire invalid (HH:MM AM/PM)" ValidationExpression="[0|1][0-9]:[0-5][0-9] ?[a|A|p|P][M|m]"></asp:RegularExpressionValidator> 
                                <asp:RequiredFieldValidator ID="reqStartData" runat="server" ControlToValidate="ReportDate" ValidationGroup="Submit1" Display="Dynamic" ErrorMessage="Date est obligatoire"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="regStartDate" runat="server" ControlToValidate="ReportDate" ValidationGroup="Submit1" Display="Dynamic" SetFocusOnError="true"  ErrorMessage="Date Invalide " ValidationExpression="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d|(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d"></asp:RegularExpressionValidator>
                            </td>
                            <td align="left"> <!-- FB 2050 -->
                                <asp:Button runat="server" ID="UsageReport" Width="175px" Text="Afficher le rapport" CssClass="altShortBlueButtonFormat" ValidationGroup="Submit1" 
                                CausesValidation="True" OnClientClick="javascript: return fnCheck();"  OnClick="MCUusageReport"/>  
                            </td>
                        </tr>                      
                    </table>
                </td>
            </tr>
            <%--FB 1938 - End--%>
            <tr>
                <td align="center">
                    <table width="90%">
                        <tr>
                            <td align="left"> <%-- FB 2050 --%>
                                <asp:Button runat="server" ID="btnTestConnection" ValidationGroup="TestConnection" OnClick="TestConnection" Text="Test de la correspondance" CssClass="altLongBlueButtonFormat" />  <%--OnClientClick="javascript:testConnection();return false;"--%>
                            </td>
                            <td align="left"> <%-- FB 2050 --%>
                                <asp:Button ID="btnSubmit" OnClick="SubmitMCU" runat="server" ValidationGroup="Submit" CssClass="altLongBlueButtonFormat" Text="Soumettre" OnClientClick="javascript:return RequireIVRName();" /><%--FB 1766--%>
                            </td>
                            
                        </tr>
                        <tr id="trCodianMessage" runat="server"> <%--FB Case 698--%>
                            <td class="cmmttext" align="left"> <%-- FB 2050 --%>
                                Management API should be Activ� for successful connection.<br />The code can be entered via the MCU website in the Feature Management Section<br />(Settings > Upgrade).
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:DropDownList CssClass="altLong0SelectFormat" ID="lstAddressType" Visible="false" runat="server" DataTextField="Name" DataValueField="ID"></asp:DropDownList>
                    <asp:DropDownList runat="server" ID="lstNetworkAccess" Visible="false" CssClass="altLong0SelectFormat">
                        <asp:ListItem Text="Publique (dans le Monde myVRM)" Value="1"></asp:ListItem>
                        <asp:ListItem Text="priv�" Value="2"></asp:ListItem>
                        <asp:ListItem Text="Les deux" Value="3"></asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
        </table>
    </div>
<script language="javascript">
    SavePassword();
    //FB 1937
    document.getElementById("reqStartTime").controltovalidate = "ReportTime_Text";
    
</script>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
<img src="keepalive.asp" name="myPic" width="1px" height="1px">
    </form>
<% if (Request.QueryString["hf"] != null)
   {
       if (!Request.QueryString["hf"].ToString().Equals("1"))
       {
        %>
 
<% }
}%>
   
</body>
</html>


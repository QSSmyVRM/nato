<%@ Page Language="C#" AutoEventWireup="true" Inherits="ns_license.license" %>

<!-- #INCLUDE FILE="inc/maintop3.aspx" -->
<script language="javascript" type="text/javascript">

function declineLicense(){
	var msg = "\n Are you sure you don't want \n " +
				"à continue the myVRM demo?";
				
	if (confirm(msg)){
		window.location='<%=Application["loginPage"]%>';
	}

}

</script>

  <table width="100%" border="0" cellpadding="0" cellspacing="0">
    <tr valign="top">
      <td style="background:image/vrmtop.gif" width="100%" height="72">
      </td>
    </tr>
  </table>


<center>

<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr valign="top">
    <td width="20%" valign="top" align="right">
	  <br/><br/><br/><br/><br/><br/><br/><br/>
      <img src="<%=Application["company_logo"].ToString() %>">
    </td>

    <td width="80%">
      <br/><br/><br/>

<%if(init == "1") {%>
      <form method="post" action="reservationOption.aspx" name="frmLicense"/>
<%}else {%>
	<form method="post" action="demoInit.aspx" name="frmLicense">
<% } %>
		<table width="100%" cellpadding="6" border="0">
			<tr><td align="middle" colspan="3">Please read the following Licence Agreement and choose à accept ou decline it.</td></tr>
		  <tr>
		  <td align="middle" colspan=3>
		  <input type="hidden" name="start" value=0/>
		  <input type="hidden" name="cmd" value="GetHome"/>
		  <%--FB 1882 EULA - address change--%> 
		  <textarea wrap="soft" style="WIDTH: 700px; HEIGHT: 350px; padding-left:40px; padding-right:20px;" name=agreement rows=80 readOnly>	
				LICENSE AGREEMENT

THIS Licence AGREEMENT by and between Interactive Ideas, LLC., a Delaware Limited Liability Corporation d/b/a myVRM having an office at 325 Duffy Ave., Hicksville, NY 11801 ("myVRM"), and the party that has indicated its acceptance à this agreement (the "Licensee").

WHEREAS, myVRM is the sole and exclusive Propri�taire of certain video conferencing software known as Versatile Resource Management ("myVRM") and the Licensee desires à utilize the myVRM software in its business; and

WHEREAS, myVRM has agreed à Licence the myVRM software � the Licensee upon the terms and conditions set forth in this Agreement.

Maintenant, THEREFORE, in consideration of the mutual promises, agreements and covenants set forth herein, the parties, each intending à be legally bound hereby, do hereby agree as follows:

1. Licence Grant.  
myVRM hereby grants à the Licensee a fully paid non-exclusive and non-transferable, non-sublicenseable license (the "License" ) � use the myVRM software modules listed on Schedule A hereto (the "Software") at only the lieux identified on Schedule A hereto to G�rer the Licensee's video conferencing as provided herein.

2. Licence Term.  
The term of the Licence shall be perpetual, commencing upon myVRM's receipt of payment of the Licence fee ( as hereinafter defined) , Sujet à termination as provided herein (the "License Term").  

3. Licence Fees.  
The Licensee shall pay à myVRM a one-time license fee in the amount set forth on Schedule B hereto (the "License Fee").  The License Fee shall cover the license to use the Software as provided herein, for an indefinite time, and maintenance of the Software during the Thirty Day Period. After the Thirty Day Period, continued usage of the Software as provided herein shall not require further payment.  

4. Provision of Services.  
During the thirty day period from the date the Software is delivered à the Licensee (the "Thirty Day Period"), myVRM shall provide such installation, training and maintenance Services � the Licensee as provided on Schedule C hereto.  After the Thirty Day Period, myVRM shall provide such Maintenance Services to the Licensee as mutually determined between myVRM and the Licensee.

5. Maintenance Services. 
If the Licensee desires à utilize myVRM's Maintenance Services after the Thirty Day Period, the Licensee shall pay � myVRM an annual maintenance fee (the "Maintenance Fee") in an amount equal to myVRM's then prevailing rate within 10 days of the date of execution of a Maintenance Services Agreement. The Maintenance Services Agreement shall automatically renew for a successive period at myVRM's then prevailing Maintenance Fee Sujet to termination as set forth in this agreement.  If the Licensee fails to timely pay the applicable Maintenance Fee, myVRM shall be under Non obligation to provide maintenance services or provide necessary updates or patches to the Licensee.  If the Licensee desires to reinstate maintenance thereafter, the Licensee shall pay to myVRM a fee equal to 150% of myVRM's then prevailing Maintenance Fee for each year for which the Maintenance Fee has not been paid.  All fees shall be due and paid by the Licensee upon invoicing.  N'importe quel fees that are not paid within 30 days of invoicing shall accrue a late fee of 1.5% per annum until paid in full.

6. Software Updates.  
During the Licence Term, so long as the myVRM is providing Maintenance Services and the Licensee continues à pay the Maintenance Fee � myVRM, myVRM shall provide to the Licensee all updates and patches to the Software that myVRM deems necessary without additional charge.  If the Licensee desires N'importe quel updates, patches or software modules to the Software in addition to those that myVRM deems necessary, the Licensee Mai request such additional updates, patches or software modules from myVRM in writing, and if myVRM agrees, in its discretion to provide such updates, patches or software modules, it shall do so and the Licensee shall pay myVRM's then standard fees therefore upon receipt of an invoice.  The term "Software" shall include all updates and patches to the Software and N'importe quel additional software modules licensed by the Licensee pursuant to this Agreement.

7. Ownership of Software.  
It is understood and agreed that myVRM owns, and shall retain, all right, title and interest in and to the Software and N'importe quel patent, patent application, trademark, registered or unregistered, copyright, trade secret, good will and other intellectual and proprietary rights therein (collectively, the "Intellectual Property Rights").  The Licensee shall not challenge myVRM's ownership of the Software or the Intellectual Property Rights, and the Licensee shall not obtain N'importe quel rights thereto except the License expressly provided herein, Sujet to all laws protecting trade secrets, know-how and the like. The Licensee shall not be entitled to make any modifications, changes, enhancements or improvements to the Software without the prior written consent of myVRM.  It is understood and agreed that myVRM shall retain all right, title and interest in and to any modifications, changes, enhancements, or improvements made to the Software by the Licensee.  The Licensee shall not decompile, disassemble, reverse assemble or otherwise reverse engineer or attempt to reconstruct or deconstruct any software code or underlying ideas or algorithms of or relating to the Software.  The Licensee shall not copy, distribute, sublicense or use for the benefit of third parties the Software, except as expressly stated in this Agreement.

8. Confidentiality.  
a) The Licensee, its employees, agents, officers, directors and affiliates shall 
hold in confidence and not disclose ou use, directly ou indirectly, for its or their benefit
(except as expressly provided in this Agreement) N'importe quel confidential renseignements of, or 
received from, myVRM.  Confidential renseignements shall include, without limitation, all 
data, Rapports, designs, programs, trade secrets, proprietary renseignements, interpretations, 
forecasts and records containing or otherwise reflecting renseignements concerning myVRM, 
the Software and the installation, and the maintenance thereof, training of personnel with 
respect à the use and operation of the Software and/or the terms of this Agreement (the 
"Confidential renseignements").  Confidential renseignements shall not include  (i) information 
already known à the Licensee when received, (ii) renseignements in the Publique (dans le Monde myVRM) Domaine when 
received ou thereafter in the Publique (dans le Monde myVRM) Domaine through sources other than myVRM, ou 
myVRM's employees, officers, directors, consultants, contractors, as its agents or 
representatives ("Representatives"), ou (iii) renseignements lawfully obtained from a third 
party not Sujet à a confidentiality obligation � myVRM. 
b) The Licensee agrees à monitor the activities  of all its Representatives 
who receive ou otherwise gain access à Confidential renseignements for the purpose of 
ensuring compliance with this Agreement.
c) In the �v�nement that the Licensee ou its representatives are requested in N'importe quel 
judicial ou administrative proceeding à disclose N'importe quel Confidential renseignements, the
Licencee will give prompt notice of such request à myVRM so that myVRM Mai seek 
an appropriate protective order.  If, in the absence of a protective order (or other 
protective remedy), the Licensee ou its Representatives are nonetheless compelled à 
disclose Review Material, the Licensee ou its Representatives Mai disclose such 
renseignements without liability hereunder, provided, however, (i) that the Licensee give
written notice of the renseignements à be disclosed as far in advance of its disclosure as is
practable and, upon myVRM's request, use its best efforts à obtain assurances that
confidential treatment will be accorded à such Confidential Information; and (ii) only
that portion of the Confidential renseignements which the Licensee is advised in writing by 
its counsel is legally Requis à be disclosed will be disclosed.  
d) The Licensee agrees that money damages would not be a sufficient remedy 
for N'importe quel breach of this agreement by it or its Representatives, and that, without the
necessity of proving damages and in addition à all other remedies, myVRM shall be 
entitled à specific performance and injunctive or other equitable relief as a remedy for 
N'importe quel such breach, and the Licensee further agrees à waive and � use its best efforts to 
cause it Representatives à waive, N'importe quel requirements for securing or posting of any
bond in connection with such remedy.

9. Système Maintenance and Back-up.  
a) Prior à the installation of the Software and during all times that (i) the 
Licencee uses the Software and (ii) the Licensee uses myVRM's Maintenance Services, 
the Licensee shall be responsible for ensuring that its computer Mat�riel informatique and operating 
Système is sufficient à install and operate the Software.
b) The Licensee shall back up all of the data, configurations and Arrangements 
on all of its MCUs, networks and servers not Suivant than 24 hours prior à the installation 
of N'importe quel Software or the provisions of the Maintenance Services.

10. Warranty.  
a) Provided that the Licensee is in compliance with Section 9 hereof and the 
Software is properly installed in accordance with Schedule B hereof, myVRM warrants 
à the Licensee that the Software will operate substantially in accordance with the 
specifications set forth in the Utilisateur guide for the Software as in effect on the date the 
Software is installed for a period of 30 days from the date the Software is installed (the 
"Warranty") .  In order à invoke the foregoing Warranty, the Licensee must notify
myVRM in writing within thirty (30)  days following the installation of the Software to
the Licensee of N'importe quel defect in the Software.  In the �v�nement that the Licensee determines that the Software does not operate substantially in accordance with its specifications and 
Rapports such defect à myVRM in writing during such 30-day period, myVRM shall use 
reasonable efforts, consistent with industry standards, à cure such defect.  The 
Licencee's sole remedy for breach of the Warranty shall be repair of the 
Software, replacement of the Software ou refund of the Licence Fee paid hereunder, at 
myVRM's sole option.  If the Licensee makes N'importe quel modifications, changes, 
enhancements, ou improvements à the Software during such 30-day period, the Warranty 
shall be of Non force and effect.  myVRM shall not be liable under the Warranty if its 
testing and examination disclose that the alleged defect ou malfunction in the Software 
does not exist ou was caused by the Licensee's ou N'importe quel third party's misuse, negligence, 
improper installation ou test (other than installation ou testing by employees or agents of
myVRM), unauthorized attempts à open, repair ,modify, decompile, disassemble, 
reverse engineer ou reconstruct the Software, use of the Software with Software as on 
Mat�riel informatique not provided or approved in writing by myVRM, use of Software outside of the 
scope of the Licence, ou N'importe quel other cause beyond the range of intended use, 
use contrary à myVRM's specifications ou instructions, damage caused by computer 
'viruses,' ou by accident, disaster, fire, lightning, other hazards, failure ou defect of 
electrical power, external circuitry, ou air conditioning ou humidity, moisture,  acts of 
terrorism or acts of God or the Licensee's non-compliance with Section 9 hereof
myVRM makes Non warranty in respect of the Software after expiration of the Warranty.
b) Correction for difficulties or defects traceable matters that are outside the 
scope of the Warranty, that are requested by the Licensee shall be billed by myVRM à 
the Licensee at myVRM's standard time and material charges.   If the Licensee Rapports 
N'importe quel defects à myVRM after the expiration of the Warranty, myVRM shall use 
commercially reasonable efforts à incorporate a cure for such defects into a patch that 
Mai be released à its customers as it deems appropriate, in myVRM's sole discretion.

11. Disclaimers; Limitation of Liability.    
à THE FULLEST EXTENT ALLOWED BY LAW, THE WARRANTY AND REMEDIES SET FORTH IN SECTION 9 HEREOF ARE EXCLUSIVE AND IN LIEU OF ALL OTHER WARRANTIES OR CONDITIONS, EXPRESS OR IMPLIED, EITHER IN FACT OR BY OPERATION OF LAW, STATUTORY OR OTHERWISE, INCLUDING WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, SATISFACTORY QUALITY, CORRESPONDENCE WITH Description OR ARISING FROM A COURSE OF DEALING OR USAGE OF TRADE AND N'importe quel WARRANTIES OF NONINFRINGEMENT, ALL OF WHICH ARE EXPRESSLY DISCLAIMED.  THE WARRANTY CONTAINED IN SECTION 9 HEREOF RUNS ONLY TO THE LICENSEE AND IS NOT EXTENDED TO N'importe quel THIRD PARTIES.  IN NO �v�nement WILL myVRM BE LIABLE TO THE LICENSEE OR ANY OTHER PARTY FOR ANY CLAIM OF LOSS, INCLUDING TIME, MONEY, GOODWILL OR CONSEQUENTIAL, EXEMPLARY, SPECIAL OR INCIDENTAL DAMAGES, WHICH Mai ARISE FROM THE USE, OPERATION, OR MODIFICATION OF THE SOFTWARE.  IN THE �v�nement THAT THE ABOVE LIABILITY LIMITATION IS defini TO BE INVALID UNDER APPLICABLE LAW, THEN myVRM'S LIABILITY FOR ANY SUCH CLAIM SHALL BE LIMITED TO THE LICENSE FEE ACTUALLY RECEIVED BY myVRM UNDER THIS AGREEMENT.  myVRM SHALL HAVE NO LIABILITY TO THE LICENSEE IN RESPECT OF FAILED CALLS, LOST CALLS, MISSED CALLS, FAILURE OF THE SOFTWARE TO DISENGAGE A CALL OR LAUNCH UNWANTED CALLS OR LOSS OF DATA, CONFIGURATIONS OR SETTINGS. THE LIABILITY OF myVRM UNDER THIS AGREEMENT, WHETHER CONTRACTUAL OR IN TORT, SHALL BE LIMITED AS SET FORTH ABOVE AND SUCH LIMITATION SHALL APPLY NOTWITHSTANDING THE ELECTION OF THE LICENSEE, IF SO ENTITLED, TO TERMINATE OR BE DISCHARGED FROM THIS AGREEMENT.

12. Marketing and References.  
The Licensee shall permit myVRM à use the Licensee's Nom in connection with the 
promotion and marketing of the Software, myVRM and its software and video conferencing 
software solutions and shall permit myVRM à use the Licensee's Nom as a positive 
reference in connection with myVRM's efforts à sell or Licence its products � 
customers.

13. Termination. 
a) Right of Termination.  myVRM shall have the right à immediately 
terminate this Agreement, including the Licence granted herein, by giving written notice
à the Licensee in the �v�nement that the Licensee breaches ou threatens � breach Section 6 ou 
7 of this Agreement. 
b) Post-Termination Rights.  Except as otherwise provided herein, upon the 
termination of this Agreement pursuant à Section 12(a) above, all of the rights of the 
Licencee under this Agreement shall forthwith terminate and immediately revert à 
myVRM and the Licensee shall immediately discontinue all use of the Software at Non 
cost whatsoever à myVRM.  Upon such termination, the Licensee shall immediately 
return à myVRM all material relating � the Software, at Non cost whatsoever to myVRM.  
c) Survival of Provisions.  The provisions of Sections 6, 7, 10, 12(b) and (c), 
13, 14, 15 and 17 through 24 shall survive the expiration ou termination of this Agreement.
d) Maintenance Termination.  myVRM shall have the right à discontinue the
provision of Maintenance Services à the Licensee if (i) the Licensee fails � make N'importe quel payment of Maintenance Fees hereunder within ten (10) days following its due date. (ii) the Licensee breaches N'importe quel other provision of this Agreement and fails to cure such breach after fifteen (15) days prior written notice thereof from myVRM to the Licensee, or (iii) myVRM sells or assigns its rights, title and interest in the Software to a third party, upon thirty (30) days prior written notice thereof given by myVRM to the Licensee.  The termination of Maintenance Services shall have no effect on the License granted herein.

14. Taxes.  
The Licensee shall, in addition à other amounts payable under this Agreement, pay all 
sales, use and other taxes, federal, R�gion, ou otherwise, however designated, which are 
levied ou imposed by reason of the transactions contemplated by this Agreement.

15. Independent Contractors. 
The relationship established by this Agreement is that of independent contractors. 
Nonnnnthing contained herein shall constitute this arrangement à be employment, a joint 
venture ou a partnership.

16. Notices.
All notices, consents, demands, requests, approvals and other communications which are
Requis ou Mai be given hereunder shall be in writing and shall be deemed à have been
duly given (a) when delivered personally, (b) the second day following the day on which
the same has been delivered prepaid à a reputable national overnight courier service, ou 
(c) if sent by mail, three business days following deposit in the mail as registered or
certified, postage prepaid in each case.  All such  notices shall be addressed à myVRM, 
at its address set forth in the header à this Agreement and � the Licensee at its address
set forth on the signature Page à this Agreement, or � such other person or persons at
such address ou addresses as Mai be designated by written notice hereunder.

17. Assignment.  
The Licensee Mai not assign this Agreement and its rights and obligations hereunder 
without the prior written consent of myVRM.  myVRM Mai assign this Agreement and 
its rights and obligations hereunder upon five (5) days' prior notice à the Licensee.  N'importe quel 
attempted assignment in violation of this Section 15 shall be null and void and of Non 
effect whatsoever.

18. Headings.  
Headings in this Agreement are for convenience of reference only and shall not be 
deemed à have N'importe quel substantive effect.

19. Severability. 
In the �v�nement that N'importe quel part of this Agreement shall be held à be invalid or unenforceable, 
the remaining parts thereof shall nevertheless continue à be valid and enforceable as 
though the invalid portions were not a part hereof.

20. Construction.  
Whenever used herein, the singular number shall include the plural, the plural the
singular and the use of the masculine, feminine ou neuter gender shall include all genders.

21. Binding Effect; Benefit.  
This Agreement shall inure à the benefit of and be binding upon the parties hereto and
their respective successors and permitted assigns; provided, however, that nothing in this 
Agreement, expressed ou implied, is intended à confer on N'importe quel person other than the 
parties hereto ou their respective successors and assigns, N'importe quel rights, remedies, obligations 
ou liabilities under ou by reason of this Agreement.

22. Waiver.  
Nonnnn delay ou failure of N'importe quel party hereto to exercise N'importe quel right, remedy or power hereunder 
shall impair the same ou be construed as a waiver thereof.  The waiver by either party 
icito of a breach of N'importe quel provision of this Agreement shall not operate or be construed as 
a waiver of N'importe quel subsequent breach.

23. Entire Agreement; Amendment.  
This Agreement (including the Schedules hereto which constitute an integral part hereof) 
embodies the entire agreement and understanding of the parties hereto with respect to the 
Sujet matter hereof and supersedes N'importe quel prior agreement or understanding between the 
parties.  This Agreement Mai be modified ou amended only by a writing signed by each 
of the parties hereto.

24. Governing Law and Jurisdiction.  
This Agreement shall be governed by, construed, interpreted and enforced in accordance 
with the laws of the R�gion of New York, without reference à principles of conflicts of 
law.  Each of the parties hereto hereby irrevocably agrees that all Actions ou proceedings 
in N'importe quel way, manner or respect, arising out of or from or related à this Agreement or the 
transactions referenced herein shall be litigated only in courts having situs within Nassau 
County, New York.  In connection therewith, each of the parties hereto hereby consents 
and submits à the jurisdiction of N'importe quel local, R�gion or federal court located within said 
county and R�gion and hereby waives N'importe quel rights such party Mai have à transfer or Modifier 
the venue of N'importe quel such litigation.

25. Representation by Counsel; Interpretation. 
The Licensee acknowledges that it has been represented by counsel ou has had the
opportunity à be represented by counsel in connection with this Agreement and the 
transactions contemplated by this Agreement.  Accordingly, N'importe quel rule or law or N'importe quel legal 
decision that would require interpretation of N'importe quel claimed ambiguities in this Agreement
against N'importe quel party that drafted it has Non application and is expressly waived by such 
parties.  The provisions of this Agreement shall be interpreted in a reasonable manner à 
effect the intent of the parties hereto.

26. Counterparts.  
This Agreement Mai be executed in one ou Suivant counterparts, each of which shall be 
deemed an original and all of which, when taken together, shall constitute one and the 
same instrument.

27. Legal Fees. 
In the �v�nement that myVRM brings an action against the Licensee à enforce N'importe quel provision 
of this Agreement and substantially prevails, Licensee shall be Requis all legal fees, 
costs and disbursements.

			</TEXTAREA>
          </td>
        </tr>
        
        <tr>
			<td height="27" align="right"> 
				<input type="Soumettre" name="Soumettre" value="Accept" class="altShortBlueButtonFormat"/>
			</td>
            <td width="30"> 
			</td>
            <td height="27"> 
				<input type="Remise à zero" name="Remise � zero" value="Decline" class="altShortBlueButtonFormat" onclick="javascript:declineLicense()"/>
			</td>
        </tr>
      </table>
	</form>
    </td>
  </tr>
</table>

</center>

<script language="JavaScript" type="text/javascript">
<!--

	document.body.style.background = "<%=bgimg%>";
	document.body.style.margin = "0px";
	document.body.style.bgcolor = "#fff7e7";

//-->
</script>

</body>
</html>



﻿<%@ Page Language="C#" ValidateRequest="false" Inherits="ns_ConfMCUInfo.ConfMCUInfo" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ConfMCUInfo</title>
    <link title="Expedite base styles" href="<%=Session["OrgCSSPath"]%>" type="text/css"
        rel="stylesheet" />

    <script type="text/javascript" src="script/errorList.js"></script>

</head>
<body>
    <form id="frmconfMCUinfo" runat="server">
    <div>
        <br />
        <center>
            <h3>
                <asp:Label ID="lblMCUInfo" runat="server" Font-Bold="true" Text="Informations MCU fuseau horaire"></asp:Label></h3>
            <asp:Label ID="lblError" runat="server" CssClass="lblError"></asp:Label>
        </center>
        <table id="tblConfMCUinfo1" runat="server" border="0" align="left">
            <tr>
                <td align="left" nowrap="nowrap">
                    <b>Nom de Conférence</b>:
                    <asp:Label ID="lblConfName" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="left" nowrap="nowrap">
                    <b>Date &amp; temps</b>:
                    <asp:Label ID="lblStartTime" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="blackblodtext">
                    <br />
                    informations MCU:
                </td>
            </tr>
            <tr>
                <td>
                    <table id="tblConfMCUinfo" runat="server" width="65%" border="0">
                    </table>
                    <asp:Label runat="server" ID="tdNoMCU" Visible="false" Text="Pas trouvé MCU."></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="left" style="width: 50%" class="blackblodtext" nowrap="nowrap">
                    <br />
                    Date du serveur Web / heure:
                    <asp:Label ID="lblWebServerTime" runat="server"></asp:Label>
                </td>
            </tr>
            <tr id="trDatabaseServerTime" runat="server">
                <td align="left" style="width: 50%" class="blackblodtext" nowrap="nowrap">
                    Date du serveur de base de données / heure:
                    <asp:Label ID="lblDatabaseServerTime" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>

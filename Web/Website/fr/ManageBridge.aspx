<%@ Page Language="C#" Inherits="ns_Bridges.Bridges" Buffer="true" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <!-- FB 2050 -->
<meta http-equiv="X-UA-Compatible" content="IE=7" /> <!-- FB 2050 -->

<!-- #INCLUDE FILE="inc/maintopNET.aspx" --> 

<script runat="server">

</script>
<script type="text/javascript" src="script/mousepos.js"></script>
<script type="text/javascript" src="script/managemcuorder.js"></script>

<script language="javascript">

	function ManageOrder ()
	{
	
		change_mcu_order_prompt('image/pen.gif', 'G�rer lordre des MCUs', document.getElementById('Bridges').value, "MCUs");//Edited For FF...
		
//		frmsubmit('Sauver', '');
	}
	
	function frmsubmit()
	{
	
	
	    if(document.getElementById("btnManage")!= null)//FB 1920
	    {
	        document.getElementById("btnManage").click();
	    }
	    /* Commented for FB 1920
	
	    if(document.getElementById("__EVENTTARGET")!= null)//FB 1763
	    {
	    document.getElementById("__EVENTTARGET").value="ManageOrder";
	    document.frmManagebridge.submit();
	    }*/
	}
</script>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf8" />
    <title>G�rer MCUs</title>
    <script type="text/javascript" src="inc/functions.js"></script>

</head>
<body>
    <form id="frmManagebridge" runat="server" method="post" onsubmit="return true">
    <div>
      <input type="hidden" id="helpPage" value="65">

        <table width="100%">
            <tr>
                <td align="center">
                    <h3>
                        <asp:Label ID="lblHeader" runat="server"></asp:Label>
                    </h3>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="Left">
                    <table cellspacing="5" width="100%"> <%-- FB 2050 --%>
                        <tr>
                            <td>&nbsp;</td>
                            <td width = "50%">
                                <SPAN class=subtitleblueblodtext>MCU existant</SPAN>
                            </td>
                            <%--FB 1920 Starts--%>
                            <td width = "100%" style="color: Red;" align="right">
                                <asp:Label  ID = "lblMCUmsg" runat="server">
                                    <b>(*)</b> indicates Publique (dans le Monde myVRM) MCUs of "Super Tenant"
                                </asp:Label>
                            </td>
                            <%--FB 1920Ends--%>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:DataGrid ID="dgMCUs" runat="server" AutoGenerateColumns="False" CellPadding="4" GridLines="None"
                         BorderColor="blue" style="border-collapse:separate" BorderStyle="solid" BorderWidth="1" ShowFooter="true" OnItemCreated="BindRowsDeleteMessage"
                         OnItemDataBound="BindRowsToGrid" OnDeleteCommand="DeleteMCU" OnEditCommand="EditMCU" Width="90%" Visible="true" > <%--Edited for FF--%> <%--FB 1920--%>
                        <%--Window Dressing start--%>
                        <SelectedItemStyle CssClass="tableBody" Font-Bold="True" />
                        <EditItemStyle CssClass="tableBody" />
                        <AlternatingItemStyle CssClass="tableBody"/>
                        <ItemStyle CssClass="tableBody" />
                        <FooterStyle CssClass="tableBody"/>
                        <%--Window Dressing end--%>
                        <HeaderStyle CssClass="tableHeader" />
                        <Columns><%--window dressing start--%>
                            <asp:BoundColumn DataField="ID" Visible="false"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>
                            <asp:BoundColumn DataField="name" HeaderStyle-CssClass="tableHeader" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderText="Nom" ItemStyle-CssClass="tableBody"></asp:BoundColumn> <%-- FB 2050 --%> 
                            <asp:BoundColumn DataField="interfaceType" HeaderStyle-CssClass="tableHeader" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderText="Type d'interface" ItemStyle-CssClass="tableBody"></asp:BoundColumn> <%-- FB 2050 --%> 
                            <asp:BoundColumn DataField="administrator" HeaderStyle-CssClass="tableHeader" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderText="Administrateur" ItemStyle-CssClass="tableBody"></asp:BoundColumn> <%-- FB 2050 --%> 
                            <asp:BoundColumn DataField="exist" HeaderStyle-CssClass="tableHeader" HeaderText="Existant/virtuel" ItemStyle-CssClass="tableBody"></asp:BoundColumn>
                            <asp:BoundColumn DataField="status" HeaderStyle-CssClass="tableHeader" HeaderText="Condition" ItemStyle-CssClass="tableBody"></asp:BoundColumn>
                            <asp:BoundColumn DataField="order" Visible="false"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="Actions" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="15%"> <%-- FB 2050 --%>
                                <HeaderStyle CssClass="tableHeader" />
                                <ItemTemplate>
                                    <asp:LinkButton runat="server" Text="Editer" ID="btnEdit" CommandName="Edit"></asp:LinkButton>
                                    <asp:LinkButton runat="server" Text="Supprimer" ID="btnDelete" CommandName="Delete"></asp:LinkButton>
                                </ItemTemplate>
                                <FooterTemplate>
                                <div style="float:left; text-align:left; width:150px"> <%-- FB 2050 --%>
                                    <%--Window Dressing--%>                                
                                    <b><span class="blackblodtext">MCU total&nbsp;</span><asp:Label  ID="lblTotalRecords" runat="server" Text=""></asp:Label> </b>
				    <br>
				     <%--Window Dressing--%>                                
				    <b><span class="blackblodtext">Licence disponible&nbsp;</span><asp:Label ID="lblRemaining" runat="server" Text=""></asp:Label> </b>
				                </div> <%-- FB 2050 --%>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                    <%--window dressing End--%>
                    <asp:Table runat="server" ID="tblNoMCUs" Visible="false" Width="90%">
                        <asp:TableRow CssClass="lblError">
                            <asp:TableCell CssClass="lblError">
                                Non MCUs found.
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>                    
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table width="90%">
                        <tr>
                            <td width="50%">&nbsp;</td>
                            <td>
                                <asp:Button ID="btnManageOrder" runat="server" OnClientClick="javascript:ManageOrder();return false;" Text="G�rer l'ordre des MCUs" CssClass="altLongBlueButtonFormat" />
                                <asp:Button ID="btnManage" OnClick="ManageMCUOrder" runat="server" style="display:none;" Width="220px"  /> <%--FB 1920 FB 2050--%>
                            </td>
                            
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <%--<SPAN class=subtitleblueblodtext>Rapport d'utilization des MCUs</SPAN>--%><%--Commented for FB 2094--%>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table width="90%">
                        <tr>
                            <td width="50%">&nbsp;</td>
                            <td>
                                <asp:Button ID="btnMCUAllocationReport" OnClick="GenerateReport" runat="server" CssClass="altLongBlueButtonFormat" Text="Rapport d'utilization des MCUs" width="220px" /><%--FB 2094 FB 2050--%>
                            </td>
                            
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <%--<SPAN class=subtitleblueblodtext>Cr�er un nouveau MCU</SPAN>--%><%--Commented for FB 2094--%>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table width="90%">
                        <tr>
                            <td width="50%">&nbsp;</td>
                            <td>
                                <asp:Button ID="btnNewMCU" OnClick="CreateNewMCU" runat="server" CssClass="altLongBlueButtonFormat" Text="Cr�er un nouveau MCU" Width="220px" /><%--FB 2094 FB2050--%>
                            </td>
                            
                        </tr>
                    </table>
                </td>
            </tr>
<%--            <tr>
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td width="20" class="tableHeader" align="center">3</td>
                            <td>
                                <SPAN class=subtitleblueblodtext>Check Resource Availability</SPAN>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table width="90%">
                        <tr>
                            <td width="50%">&nbsp;</td>
                            <td>
                                <asp:Button ID="Button1" OnClick="GenerateReport" runat="server" CssClass="altLongBlueButtonFormat" Text="Soumettre" />
                            </td>
                            
                        </tr>
                    </table>
                </td>
            </tr>--%>
            <tr style="display:none">
                <td align="center">
                    <asp:DropDownList ID="lstBridgeType" DataTextField="name" DataValueField="ID" runat="server"></asp:DropDownList>
                    <asp:DropDownList ID="lstBridgeStatus" DataTextField="name" DataValueField="ID" runat="server"></asp:DropDownList>
                </td>
            </tr>
        </table>
    </div>
<asp:TextBox ID="txtBridges" runat="server" Width="0" Height="0" ForeColor="transparent" BackColor="transparent" BorderColor="transparent"></asp:TextBox>
  <input type="hidden" id="Bridges" name="Bridges" width="200">

<img src="keepalive.asp" name="myPic" width="1px" height="1px">
    </form>
<%--FB 1491 Start--%>
<script language="javascript">
document.getElementById("Bridges").value=document.getElementById("<%=txtBridges.ClientID %>").value;
</script>
<%--FB 1491 End--%>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
    <!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
</body>
</html>



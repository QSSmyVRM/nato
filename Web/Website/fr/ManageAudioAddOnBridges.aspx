﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="ns_MyVRM.ManageAudioAddOnBridges" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="X-UA-Compatible" content="IE=7" />
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Gérer les ponts audio</title>

    <script type="text/javascript" src="script/mousepos.js"></script>

    <script type="text/javascript" src="script/managemcuorder.js"></script>

    <script type="text/javascript" src="inc/functions.js"></script>

    <link rel="StyleSheet" href="css/divtable.css" type="text/css" />
    <link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/border-table.css" />
    <link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/main-table.css" />
    <link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/main.css" />
    <link rel="stylesheet" title="Expedite base styles" type="text/css" href="<%=Session["OrgCSSPath"]%>" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <input type="hidden" id="helpPage" value="65" />
        <table width="100%">
            <tr>
                <td align="center">
                    <h3>
                        <asp:Label ID="lblHeader" runat="server" Text="Gérer les ponts audio"></asp:Label>
                    </h3>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="lblError" runat="server" Text="" CssClass="lblError"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table width="40%" cellspacing="0" cellpadding="0" border="0">
                        <tr>
                            <td align="left">
                                <span class="subtitleblueblodtext">Existants ponts audio</span>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:DataGrid ID="dgAudioBridges" runat="server" AutoGenerateColumns="False" CellPadding="4"
                        GridLines="None" BorderColor="blue" BorderStyle="solid" BorderWidth="1" ShowFooter="false"
                        OnEditCommand="EditAudionAddonBridge" OnDeleteCommand="DeleteAudionAddonBridge"
                        Width="40%" Visible="true" Style="border-collapse: separate">
                        <SelectedItemStyle CssClass="tableBody" />
                        <EditItemStyle CssClass="tableBody" />
                        <AlternatingItemStyle CssClass="tableBody" />
                        <ItemStyle CssClass="tableBody" />
                        <HeaderStyle CssClass="tableHeader" HorizontalAlign="left" />
                        <Columns>
                            <asp:BoundColumn DataField="userID" Visible="false">
                                <HeaderStyle CssClass="tableHeader" HorizontalAlign="left" />
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="firstName" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader"
                                ItemStyle-HorizontalAlign="Left" HeaderText="Nom"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Status" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader"
                                ItemStyle-HorizontalAlign="Left" HeaderText="Condition" Visible="false"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="Actions" ItemStyle-CssClass="tableBody" ItemStyle-Width="28%"
                                ItemStyle-HorizontalAlign="Left">
                                <HeaderStyle CssClass="tableHeader" HorizontalAlign="left" />
                                <ItemTemplate>
                                    <asp:LinkButton runat="server" Text="Editer" ID="btnEdit" Visible='<%# Session["admin"].ToString().Trim().Equals("2")%>' CommandName="Edit" Width="28%"></asp:LinkButton>&nbsp;&nbsp;
                                    <asp:LinkButton runat="server" Text="Supprimer" ID="btnDelete" Visible='<%# Session["admin"].ToString().Trim().Equals("2")%>' CommandName="Delete"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:Table runat="server" ID="tblNoAudioBridges" Visible="false" Width="90%">
                        <asp:TableRow CssClass="lblError">
                            <asp:TableCell CssClass="lblError">
                                Pas de ponts audio trouvés.
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table width="40%" cellspacing="0" cellpadding="0" border="0">
                        <tr>
                            <td align="right" width="20%">
                                <b class="blackblodtext">Total des ponts audio: </b><b><asp:Label ID="lblTotalUsers" runat="server" Text=""></asp:Label> </b>
                            </td>
                            <td  align="right" width="20%" runat="server" id="tdLicencesRemaining">
				                <b class="blackblodtext">licences restantes: </b><b><asp:Label ID="lblLicencesRemaining" runat="server" Text=""></asp:Label> </b>                                
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
			<tr><%--FB 2023--%>
                <td align="center">
                    <asp:Table ID="tblPage" Visible="false" runat="server">
                        <asp:TableRow ID="TableRow1" runat="server">
                            <asp:TableCell ID="TableCell1" Font-Bold="True" Font-Names="Verdana" Font-Size="Small" ForeColor="Blue" runat="server"><span class="blackblodtext"> Pages:</span> </asp:TableCell>
                            <asp:TableCell ID="TableCell2" runat="server"></asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>

                </td>
            </tr>
            <tr>
                <td align="left">
                </td>
            </tr>
            <tr id="trNewBridge" runat="server">
                <td align="center">
                    <table width="40%" border="0">
                        <tr>
                            <td width="50%">
                                &nbsp;
                            </td>
                            <td align="right">
                                <asp:Button ID="btnNewAudioBridge" runat="server" CssClass="altShortBlueButtonFormat"
                                    Text="Créer un nouveau pont audio" Width="200px" OnClick="CreateNewAudioAddonBridge" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->

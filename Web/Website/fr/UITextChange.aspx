<%@ Page Language="C#" AutoEventWireup="true"  Inherits="en_UITextChange" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <!-- FB 2050 -->
<meta http-equiv="X-UA-Compatible" content="IE=7" /> <!-- FB 2050 -->

	<!-- #INCLUDE FILE="inc/maintopNET.aspx" --> 
<html xmlns="http://www.w3.org/1999/xhtml" >

  <link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/border-table.css">
  <link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/main-table.css">
  <link rel="stylesheet" title="Expedite base styles" type="text/css" href="<%=Session["OrgCSSPath"]%>">  
  <script language="javascript" src="script/RoboHelp_CSH.js"></script>

<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf8" />
<script>
    function fnOpen()
    {
        frmUIText.reset();           
    }
    
    function fnCancel()
	{
	
		window.location.replace('OrganisationSettings.aspx');  //CSS Project
	}
	
	function fnValidation()
    {
        
        var args = fnValidation.arguments
        var recCount = 2;	    
	    var gridName = document.getElementById("dgTxtChange");
	    
	    for(var i=2; i<=args[0]+1; i++)
		{				
			statusValue = "";
			if(i < 10)
			    i = "0" + i;
			 
			var newText = document.getElementById("dgTxtChange_ctl"+ i + "_NewText");
			var rqName = document.getElementById("dgTxtChange_ctl"+ i + "_reqText");
			var rgName = document.getElementById("dgTxtChange_ctl"+ i + "_regText");
			if(newText)
			{
				if(newText.value == "")
                {        
                    rqName.style.display = 'block';
                    newText.focus();
                    return false;
                }
                // Edited for FB 1428 , � allow character '/'
                else if (newText.value.search(/^(a-z|A-Z|0-9)*[^\\<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$/)==-1)
                {        
                    rgName.style.display = 'block';
                    newText.focus();
                    return false;
                }   
			}														
			
	    }
	    
	    return true;	    
    }
    //FB 1975 - Start
    function suppressBackspace(evt) 
    {
         evt = evt || window.event;
         var target = evt.target || evt.srcElement; 
         if (evt.keyCode == 8 && !/input|textarea/i.test(target.nodeName)) 
         { 
            window.location.replace('OrganisationSettings.aspx');        
            return false;
         } 
    } 
    document.onkeydown = suppressBackspace;
    document.onkeypress = suppressBackspace; 
    //FB 1975 - End
    
</script>
    <title>UI Text Modifier</title>
</head>
<body>
    <form id="frmUIText" method="post" runat="server">
    <div>
    <input type="hidden" id="hdnValue" runat="server" />
    <center>
		<H3>Utilisateur Interface Text Arrangements</H3>
		<asp:Label id="errLabel" Runat="server" CssClass="lblError"></asp:Label>
	</center>	
        <table cellSpacing="0" cellpadding="3" border="0" width="100%">	<!-- FB 2050 -->
            <tr>
                <td>
                    <asp:DataGrid ID="dgTxtChange" runat="server" AutoGenerateColumns="False" CellPadding="1" GridLines="None" AllowSorting="true" 
                         BorderColor="blue" BorderStyle="solid" BorderWidth="1" ShowFooter="true" Width="100%">
                         <SelectedItemStyle  CssClass="tableBody"/>
                         <AlternatingItemStyle CssClass="tableBody" />
                         <ItemStyle CssClass="tableBody"  />
                        <HeaderStyle CssClass="tableHeader" Height="30px" />
                        <EditItemStyle CssClass="tableBody" />                    
                        <FooterStyle CssClass="tableBody" />
                        <Columns>    
                          <asp:BoundColumn DataField="Id" Visible="false"></asp:BoundColumn>
                            <%--Window Dressing--%>
                          <asp:TemplateColumn HeaderText="Description" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass ="tableHeader" ItemStyle-Width="15%" >
                                <ItemTemplate>
                                    <asp:Label ID="LblDesc" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Name")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>                       
                            <%--Window Dressing--%>
                             <asp:TemplateColumn HeaderText="Current Text" ItemStyle-CssClass="tableBody"   HeaderStyle-CssClass ="tableHeader" ItemStyle-Wrap="true" ItemStyle-Width="40%">
                                <ItemTemplate>
                                    <asp:Label ID="CurText" runat="server"  Text='<%# DataBinder.Eval(Container, "DataItem.CurrentText")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>                      
                            <%--Window Dressing--%>
                              <asp:TemplateColumn HeaderText="New Text" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass ="tableHeader">
                                <ItemTemplate>
                                    <asp:TextBox ID="NewText" runat="server" CssClass="altText"  Text='<%# DataBinder.Eval(Container, "DataItem.NewText")%>' Width="320"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="reqText" runat="server" ControlToValidate="NewText" Display="dynamic"  SetFocusOnError="true" Text="Required" ValidationGroup="Submit"></asp:RequiredFieldValidator>
                                     <asp:RegularExpressionValidator ID="regText" ControlToValidate="NewText" Display="dynamic" runat="server" ValidationGroup="Submit" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ et &#34; sont des characteres invalides." ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:RegularExpressionValidator> <%--Edited for FB 1428 , To allow character '/'--%>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </td>
            </tr>
            <tr>
                <td align="center">
					<asp:button id="Original" runat="server" OnClick="Original_Click" CssClass="altShort0BlueButtonFormat" Text="Default Configuration"></asp:button>
                    <asp:Button ID="Reset" runat="server" CssClass="altShort0BlueButtonFormat" Text="Remise � zero" OnClientClick="fnOpen()" />
                    <input type="button" id="Close" class="altShort0BlueButtonFormat" value="Annuler" onclick="fnCancel()" />
                    <asp:Button ID="btnSubmit" runat="server" CssClass="altShort0BlueButtonFormat" OnClick="Submit_Click" Text="Soumettre" />
                </td>
            </tr>
        </table>	         
    </div>
    </form>
</body>
</html>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->



<%@ Page Language="C#" AutoEventWireup="true" Inherits="ns_MyVRM.Room" %>

<%@ Register TagPrefix="cc1" Namespace="myVRMWebControls" Assembly="myVRMWebControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- FB 2050 -->
<meta http-equiv="X-UA-Compatible" content="IE=7" />
<!-- FB 2050 -->
<!--window Dressing start-->
<% 
    if (Request.QueryString["cal"] == "2")
    { 
%>
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
<% 
    } 
%>
<!--window Dressing end-->

<script type="text/javascript" src="inc/functions.js"></script>

<script type="text/javascript" src="script/errorList.js"></script>

<script type="text/javascript" src="extract.js"></script>

<%--Login Management--%>

<script type="text/javascript" language="javascript">



function ViewEndpointDetails()
{
    val = document.getElementById("lstEndpoint").value;
	
    if (val == "" || val == "-1") {
        alert("Selectionner s'il vous pla�t an Endpoint from the list first.");
    } else {
        url = "dispatcher/admindispatcher.asp?eid=" + val + "&cmd=GetEndpoint&ed=1&wintype=pop";

        if (!window.winrtc) {	// has not yet been defined
            winrtc = window.open(url, "", "width=400,height=310,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
            winrtc.focus();
        } else { // has been defined
            if (!winrtc.closed) {     // still open
                winrtc.close();
                winrtc = window.open(url, "", "width=400,height=310,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
                winrtc.focus();
            } else {
                winrtc = window.open(url, "", "width=400,height=310,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
                winrtc.focus();
            }
        }
    }
}

function getYourOwnEmailList (i)
{
    if (i == -2)//Login Management
    {
//        url = "dispatcher/conferencedispatcher.asp?frm=roomassist&frmname=frmMainroom&cmd=GetEmailList&emailListPage=1&wintype=pop";
      if(queryField("sb") > 0 )
            url = "emaillist2.aspx?t=e&frm=roomassist&wintype=ifr&fn=frmMainroom&n=";
            else
            url = "emaillist2main.aspx?t=e&frm=roomassist&fn=frmMainroom&n=";
    }
    else
    {
//        url = "dispatcher/conferencedispatcher.asp?frm=approver&frmname=frmMainroom&no=" + i + "&cmd=GetEmailList&emailListPage=1&wintype=pop";
        url = "emaillist2main.aspx?t=e&frm=approver&fn=frmMainroom&n=" + i;
	}
    if (!window.winrtc) {	// has not yet been defined
        winrtc = window.open(url, "", "width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
        winrtc.focus();
    } else // has been defined
        if (!winrtc.closed) {     // still open
            winrtc.close();
            winrtc = window.open(url, "", "width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
            winrtc.focus();
        } else {
            winrtc = window.open(url, "", "width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
            winrtc.focus();
        }
}


function deleteApprover(id)
{
    eval("document.frmMainroom.Approver" + id + "ID").value = "";
    eval("document.frmMainroom.Approver" + id).value = "";
}

//Endpoint Fix
function deleteEndpoint()
{
    eval("document.frmMainroom.hdnEPID").value = "";
    eval("document.frmMainroom.txtEndpoint").value = "";
}

function deleteAssistant()
{
    eval("document.frmMainroom.AssistantID").value = "";
    eval("document.frmMainroom.Assistant").value = "";
}


function frmMainroom_Validator()
{
    //fB 2415 Start
    if (!Page_ClientValidate())
	        return Page_IsValid; 
	//FB 2415 End
    //for DQA Commentaire - start
    var txtroomname = document.getElementById('<%=txtRoomName.ClientID%>');
    if(txtroomname.value == "")
    {        
        reqName.style.display = 'block';
        txtroomname.focus();
        return false;
    } // FB 1640
    else if (txtroomname.value.search(/^(a-z|A-Z|0-9)*[^\\<>^+?|!`\[\]{}\=@$%&~]*$/)==-1) //FB 1888
    {        
        regRoomName.style.display = 'block';
        txtroomname.focus();
        return false;
    }    
    
//Coomented for FB 2594
//    var txtroomphone = document.getElementById('<%=txtRoomPhone.ClientID%>');
//    if (txtroomphone.value != '' && txtroomphone.value.search(/^(\(|\d| |-|\))*$/)==-1)
//    {        
//        regRoomPhone.style.display = 'block';
//        regRoomPhone.innerText = 'Les valeurs num�riques�seulement';
//        errLabel.innerText = 'Please Enter - Valid salle de conf�rence Num�ro de T�l�phone';//Added for FB 1459
//        txtroomphone.focus();
//        return false;
//    }    
//    
    var txtmaximumcapacity = document.getElementById('<%=txtMaximumCapacity.ClientID%>')
    if(txtmaximumcapacity.value != '')
    {
        var maxCapVal = parseInt(txtmaximumcapacity.value);    
        if( !isFinite(maxCapVal) || maxCapVal < 0 || maxCapVal > 10000)
        {
            CapacityValidator.style.display = 'block';
            txtmaximumcapacity.focus();
            return false;
        }
    }   
    
    var txtmaxconcurrentCalls = document.getElementById('<%=txtMaxConcurrentCalls.ClientID%>');
    if (txtmaxconcurrentCalls.value != '' && txtmaxconcurrentCalls.value.search(/^(\(|\d|\))*$/)==-1)
    {        
        regMaxCall.style.display = 'block';
        txtmaxconcurrentCalls.focus();
        return false;
    }    
    
    var txtsetuptime = document.getElementById('<%=txtSetupTime.ClientID%>');
    if (txtsetuptime.value != '' && txtsetuptime.value.search(/^(\(|\d|\))*$/)==-1)
    {        
        regMaxCall.style.display = 'block';
        regMaxCall.focus();
        return false;
    }    

    var txtteardowntime = document.getElementById('<%=txtTeardownTime.ClientID%>');
    if (txtteardowntime.value != '' && txtteardowntime.value.search(/^(\(|\d|\))*$/)==-1)
    {        
        regTeardownTime.style.display = 'block';
        txtteardowntime.focus();
        return false;
    }    
  
    
    var assistant = document.getElementById('<%=Assistant.ClientID%>');
    var editHref = document.getElementById("EditHref");
    
    if(assistant.value == "")
    {
        //alert("Selectionner s'il vous pla�t the Administrateur en charge");        
        AssistantValidator.style.display = 'block';
        editHref.focus();
        return false;
    }
    //for DQA Commentaire - end
    
//    var lstvideo = document.getElementById('<%=lstVideo.ClientID%>');
//    if (lstvideo.value == "1" || lstvideo.value == "3") {//Edited for FB 1459
//    errLabel.innerText = ""; //Added for FB 1459
//        if (txtroomphone.value == "") {
//            //alert(EN_150);
//            regRoomPhone.style.display = 'block'; 
//            regRoomPhone.innerText = 'Requis';
//            errLabel.innerText = 'Please Enter - salle de conf�rence Phone Number';//Added for FB 1459
//            txtroomphone.focus();
//            return (false);
//        }
//    }   
   
    var cb = document.getElementById('<%=txtMultipleAssistant.ClientID%>');
    if(cb.value != '')
    {
        if(cb.value.search(/^(a-z|A-Z|0-9)*[^\\/<>^+?|!`,\[\]{}\x22=:#$%&()'~]*$/)==-1)
        {   
            regMultipleAssistant.style.display = 'block';
            cb.focus();
            return false;
        }
    }
    
    var emailsary = (cb.value).split(/,| |:|;/g);
    var newval = "";	var num = 0;
    for (i = 0; i<emailsary.length; i++) {
        if (Trim(emailsary[i]) != "") {
            newval += Trim(emailsary[i]) + ";"
            num ++;
        }
    }
    newval = Trim(newval);
    while ( (newval.length > 0) && (newval.charAt(newval.length-1) == ";") )
        newval = newval.substring(0, newval.length-1);

    cb.value = newval;
    if (num > 10) {
        alert(EN_192);
        cb.focus();
        return false;
    }
    
    var lsttoptier = document.getElementById('<%=lstTopTier.ClientID%>');
    var lstmiddletier = document.getElementById('<%=lstMiddleTier.ClientID%>');
    
    if(lsttoptier.value == "-1")
    {
        //alert("Selectionner s'il vous pla�t the Top Tier");
        reqTopTier.style.display = 'block';
        lsttoptier.focus();
        return false;
    }

    if(lstmiddletier.value == "-1")
    {
        //alert("Selectionner s'il vous pla�t the Middle Tier");
        reqMiddleTier.style.display = 'block';
        lstmiddletier.focus();
        return false;
    }

    var txtzipcode = document.getElementById('<%=txtZipCode.ClientID%>');
    //FB 2222
    if (txtzipcode.value != '' && txtzipcode.value.search(/^(a-z|A-Z|0-9)*[^\\/<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$/)==-1) 
    {        
        regZipCode.style.display = 'block';
        txtzipcode.focus();
        return false;
    }  
    
    var txtparkingdirections = document.getElementById('<%=txtParkingDirections.ClientID%>');
    if (txtparkingdirections.value != '' && txtparkingdirections.value.search(/[^`']*/)==-1)
    {        
        regParkDirections.style.display = 'block';
        txtparkingdirections.focus();
        return false;
   }     
    
    var txtaddcomments = document.getElementById('<%=txtAdditionalComments.ClientID%>');
    if (txtaddcomments.value != '' && txtaddcomments.value.search(/^(a-z|A-Z|0-9)*[^\\/<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$/)==-1)
    {        
        regAddComments.style.display = 'block';
        txtaddcomments.focus();
        return false;
    }  
    
    var txtmaplink = document.getElementById('<%=txtMapLink.ClientID%>');
    if (txtmaplink.value != '' && txtmaplink.value.search(/[^`']*/)==-1)
    {        
        regMapLink.style.display = 'block';
        txtmaplink.focus();
        return false;
    }  
    
    var lsttimezone = document.getElementById('<%=lstTimezone.ClientID%>');
    if(lsttimezone.value == "-1")
    {    
        //alert("Selectionner s'il vous pla�t the Timezone");
        regTimeZone.style.display = 'block';
        lsttimezone.focus();
        return false;
    }   
    
   /* var txtlongitude = document.getElementById('<%=txtLongitude.ClientID%>');
    if (txtlongitude.value != '' && txtlongitude.value.search(/^(\(|\d| |-|\))*$/)==-1)
    {        
        regLongitude.style.display = 'block';
        txtlongitude.focus();
        return false;
    }  
   
    var txtlatitude = document.getElementById('<%=txtLatitude.ClientID%>');
    if (txtlatitude.value != '' && txtlatitude.value.search(/^(\(|\d| |-|\))*$/)==-1)
    {        
        regLatitude.style.display = 'block';
        txtlatitude.focus();
        return false;
    }  */
    // dept
    // FB 2342 starts
    //FB 2342 stopped validation
//    var txtroomemail1 = document.getElementById('<%=txtRoomEmail.ClientID%>');
//    if(txtroomemail1.value == "")
//    {        
//        reqRoomEmail.style.display = 'block';
//        txtroomemail1.focus();
//        return false;
//    }
//    else 
//    {
//         var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
//         if(reg.test (txtroomemail1 .value)==false)
//         {
//                reqroomEmail1_1.style.display='block';
//                txtroomemail1.focus();
//                return false;
//         }
//    } 
    // FB 2342 end
    var cb1 = document.getElementById('<%=lstVideo.ClientID%>');
    var cb2 = document.getElementById('<%=txtEndpoint.ClientID%>');
    if ( (cb1.selectedIndex != 0) && (cb2.value == "") ){
        //alert(EN_205);
        regEndPoint.style.display = 'block';
        regEndPoint.innerText = 'Please add a endpoint, because you select a Genre of media.';
        cb2.focus();
        return false;
    }
     if ( (cb1.selectedIndex <= 0) && (cb2.value != "") ) {
        //alert(EN_204);
        regVideo.style.display = 'block';
        regVideo.innerText = 'When selecting endpoint, Selectionner sil vous pla�t M�dia for it.';
        cb1.focus();
        return false;
    }
    
    var hdnmultipledept = document.getElementById('<%=hdnMultipleDept.ClientID%>');
    var departmentlist = document.getElementById('<%=DepartmentList.ClientID%>');

    if (hdnmultipledept.value == 1) 
    {
        if ((departmentlist.value == "") && (departmentlist.length > 0) ) 
        {
            isConfirm = confirm("Are you sure you want � set up this salle de conf�rence with Non department(s) assigned?\n")
            if (isConfirm == false)
            {
                return(false);
            }
        }
    }
    
    //FB 2400 Starts
    if(cb1.value != "0")
    {
        var RoomTele = document.getElementById('<%=DrpisTelepresence.ClientID%>')
        var EPTele = document.getElementById('<%=hdnisEPTelePresence.ClientID%>');
        if(EPTele.value != RoomTele.value)
        {
              alert("S'il vous pla�t v�rifiez le type de t�l�pr�sence du point de terminaison s�lectionn� pour la chambre.")
              return(false);
        }
    }
    //FB 2400 Ends
    

    

    return(true);
}

function fnClose()
{
//    window.location.replace('dispatcher/admindispatcher.asp?cmd=ManageConfRoom'); //Login management
    window.location.replace("manageroom.aspx?hf=&m=&pub=&d=&comp=&f=&frm=");//Login management
    
    return true;
}


function fnOpen()
{
   //var url = "manageimage.asp?p=image/room/";
   var url = "manageimage.aspx";// This code is added for ManageImage.aspx Conversion
   window.open(url, "", "width=700,height=500,top=0,left=0,resizable=yes,scrollbars=no,status=no");// This code is edited for ManageImage.aspx Conversion
   return false;
}

function AddImage(imgname)
{
	addopt(document.getElementById('<%=lstRoomImage.ClientID%>'), imgname, imgname, true, true);
}

function DelImage(imgname)
{
	deloptbyval(document.getElementById('<%=lstRoomImage.ClientID%>'), imgname);
}

function chgEndpoint()
{
    var endPt = document.getElementById('<%=lstEndpoint.ClientID%>');
	document.getElementById('<%=btnEndpointDetails.ClientID%>').disabled = (endPt.value == "-1") ? "Rien" : "";
}

//Added for FB 1459  Start
//function fnRmIsEmpty()
//{
//    var ltvideo = document.getElementById('<%=lstVideo.ClientID%>');
//    if (ltvideo.value == "1" || ltvideo.value == "3") {
//        if(document.getElementById("txtroomphone").value == " ") {
//            errLabel.innerText = 'Please Enter - Valid salle de conf�rence Phone Number';
//            document.getElementById("txtroomphone").focus();
//            return false;
//        }
//    }
//}

//Added for FB 1459  End

//added for Endpoint Recherches - Start
function OpenEndpointSearch()
{

        if(OpenEndpointSearch.arguments != null)
        {
            var rmargs = OpenEndpointSearch.arguments;
            var prnt = rmargs[0];
            var isTele = document.getElementById('<%=DrpisTelepresence.ClientID%>') //FB 2400
            var url = "";
            url = "EndpointSearch.aspx?frm="+prnt+"&isRoomTel="+isTele.value ; //FB 2400
            window.open(url, "EndpointSearch", "width="+ screen. availWidth +",height=666px,resizable=no,scrollbars=yes,status=no,top=0,left=0");
        }
       
    
}
function AddEndpoint()
{
  
   
}

//added for Endpoint Recherches - End

function toggleDiv(id,flagit) 
{
    if (flagit=="1")
    {
        if (document.layers) document.layers[''+id+''].visibility = "Montrer"
        else if (document.all) document.all[''+id+''].style.visibility = "visible"
        else if (document.getElementById) document.getElementById(''+id+'').style.visibility = "visible"
    }
    else
        if (flagit=="0")
        {
            if (document.layers) document.layers[''+id+''].visibility = "Cach�"
            else if (document.all) document.all[''+id+''].style.visibility = "hidden"
            else if (document.getElementById) document.getElementById(''+id+'').style.visibility = "hidden"
        }
}
// FB 2136 Start
function fnOpenSecurityBadge()
{

var e = document.getElementById("drpSecImgList");
var selOption = e.options[e.selectedIndex].text;
var optionArray = selOption.split(".");
var url = "ManageSecurityBadge.aspx?drpSelOption=" + optionArray[0] + "_" + "<%=Session["organizationID"]%>" + ".jpg" ;
window.open(url, "", "width=675,height=500,top=0,left=0,resizable=no,scrollbars=no,status=no");// This code is edited for ManageImage.aspx Conversion
return false;

}

// >> Please refer updateImage() in ManageSecurityBadge.aspx <<
function fnUpdateHdnField(selValue)
{
alert('auto' + selValue.substring(0, selValue.length-4));
document.getElementById('hdnSelecOption').value = selValue.substring(0, selValue.length-4);
}

// FB 2136 End

function fnSecImgSelection()
{
    var e = document.getElementById("drpSecImgList");
    var selOption = e.options[e.selectedIndex].value;
    document.getElementById('hdnSecSelection').value = selOption;
}

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf8" />
    <title>myVRM</title>
    <link rel="stylesheet" title="Expedite base styles" type="text/css" href="<%=Session["OrgCSSPath"]%>">
</head>
<body>
    <form id="frmMainroom" runat="server">
    <asp:ScriptManager ID="RoomImageScript" runat="server">
    </asp:ScriptManager>
    <input type="hidden" runat="server" id="hdnMultipleDept" />
    <input type="hidden" runat="server" id="hdnRoomID" />
    <input type="hidden" runat="server" id="hdnSelecOption" />
    <input type="hidden" runat="server" id="hdnSecSelection" />
    <input type="hidden" runat="server" id="hdnisEPTelePresence" /> <%--FB 2400--%>
    <%--FB 2136--%>
    <input name="hdnEPID" type="hidden" id="hdnEPID" runat="server" />
    <%--Endpoint Search--%>
    <%--Code changed for FB 1425 QA Bug -Start--%>
    <input type="hidden" id="hdntzone" runat="server" />
    <%--Code changed for FB 1425 QA Bug -End--%>
    <div>
        <center>
            <div id="dataLoadingDIV" style="z-index: 1">
            </div>
            <h3>
                <asp:Label ID="lblTitle" runat="server" CssClass="h3" Text="salle de conf�rence"></asp:Label>
            </h3>
            <asp:Label ID="errLabel" runat="server" CssClass="lblError"></asp:Label>
        </center>
        <table cellpadding="0" cellspacing="0" border="0" style="width: 100%">
            <%--table 1 starts here--%>
            <tr>
                <%--Basic Configuration--%>
                <td align="center">
                    <table id="tblBasicConfiguration" cellpadding="2" cellspacing="2" border="0" style="width: 95%">
                        <tr>
                            <td colspan="2" align="left">
                                <table id="Table4" cellpadding="2" cellspacing="2" border="0" style="width: 100%">
                                    <tr align="left">
                                        <td style="width: 10%" align="left" valign="top" class="blackblodtext" nowrap>
                                            Dernier Modified by :
                                        </td>
                                        <td style="width: 85%" align="left" valign="top">
                                            <asp:Label ID="lblMUser" runat="server" CssClass="active"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 10%" align="left" valign="top" class="blackblodtext" nowrap>
                                            Dernier Modified at :
                                        </td>
                                        <td style="width: 85%" align="left" valign="top">
                                            <asp:Label ID="lblMdate" runat="server" CssClass="active"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <span class="subtitleblueblodtext">Configuration de base</span>
                            </td>
                            <td class="reqfldText" align="center">
                                * Element Obligatoire
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <table id="tblBasicConfigurationDetials" cellpadding="2" cellspacing="2" border="0"
                                    style="width: 95%">
                                    <%-- Configuration de base Param�tres starts ici --%>
                                    <tr>
                                        <%--Window Dressing--%>
                                        <td style="width: 15%" align="right" valign="top" class="blackblodtext">
                                            <b>salle de conf�rence Nom</b> <span class="reqfldText">*</span>
                                        </td>
                                        <td style="width: 35%" align="left" valign="top">
                                            <%--Edited For FF & FB 2050--%>
                                            <asp:TextBox ID="txtRoomName" runat="server" CssClass="altText"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="reqName" runat="server" ControlToValidate="txtRoomName"
                                                Display="dynamic" CssClass="lblError" SetFocusOnError="true" Text="Required"
                                                ValidationGroup="Submit"></asp:RequiredFieldValidator>
                                            <%-- Code Added for FB 1640--%>
                                            <asp:RegularExpressionValidator ID="regRoomName" ControlToValidate="txtRoomName"
                                                Display="dynamic" runat="server" ValidationGroup="Submit" SetFocusOnError="true"
                                                ErrorMessage="<br> & < > + % \ ? | ^ = ! ` [ ] { } $ @  et ~ sont des characteres invalides."
                                                ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>^+?|!`\[\]{}\=@$%&~]*$"></asp:RegularExpressionValidator>
                                            <%--FB 1888--%>
                                        </td>
                                        <%--Window Dressing--%>
                                        <td style="width: 20%" align="right" valign="top" class="blackblodtext">
                                            <b>salle de conf�rence Num�ro de T�l�phone</b>
                                        </td>
                                        <%--Edited For FF--%>
                                        <td style="width: 30%" align="left" valign="top">
                                            <%--Edited For FF--%>
                                            <asp:TextBox ID="txtRoomPhone" runat="server" CssClass="altText"></asp:TextBox>
                                            <%--Edited for FB 1459--%><%--FB 2594--%>
                                            <asp:RegularExpressionValidator ID="regRoomPhone" ControlToValidate="txtRoomPhone"
                                                Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="& < > ' % \ ; ? | ^ = ! ` [ ] { } : # $  ~ and &#34; are invalid characters."
                                                ValidationGroup="Submit" ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>^;?|!`\[\]{}\x22;=:#$%&'~]*$"></asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <%--Window Dressing--%>
                                        <td style="width: 15%" align="right" valign="top" class="blackblodtext">
                                            <b>Capacit� maximum</b>
                                        </td>
                                        <td style="width: 30%" align="left" valign="top">
                                            <%--Edited For FF--%>
                                            <asp:TextBox ID="txtMaximumCapacity" runat="server" CssClass="altText"></asp:TextBox>
                                            <asp:RangeValidator ID="CapacityValidator" runat="server" ControlToValidate="txtMaximumCapacity"
                                                SetFocusOnError="true" Type="integer" MinimumValue="0" MaximumValue="10000" CssClass="lblError"
                                                Text="Capacit� maximum between 0 and 10,000. "></asp:RangeValidator>
                                        </td>
                                        <%--Window Dressing--%>
                                        <td style="width: 20%" align="right" valign="top" class="blackblodtext">
                                            <b>Nomber maximum de coups de t�l�phon simultan�s</b>
                                        </td>
                                        <%--Edited For FF--%>
                                        <td style="width: 30%" align="left" valign="top">
                                            <%--Edited For FF--%>
                                            <asp:TextBox ID="txtMaxConcurrentCalls" runat="server" CssClass="altText"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="regMaxCall" ControlToValidate="txtMaxConcurrentCalls"
                                                Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="Entrez uniquement des chiffres."
                                                ValidationGroup="Submit" ValidationExpression="^(\(|\d|\))*$"></asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <%--Window Dressing--%>
                                        <td style="width: 15%" align="right" valign="top" class="blackblodtext">
                                            <b>Temps de pr�paration Buffer (minutes)</b>
                                        </td>
                                        <td style="width: 30%" align="left" valign="top">
                                            <%--Edited For FF--%>
                                            <asp:TextBox ID="txtSetupTime" runat="server" CssClass="altText"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="regSetupTime" ControlToValidate="txtSetupTime"
                                                Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="Entrez uniquement des chiffres."
                                                ValidationGroup="Submit" ValidationExpression="^(\(|\d|\))*$"></asp:RegularExpressionValidator>
                                        </td>
                                        <%--Window Dressing--%>
                                        <td style="width: 20%" align="right" valign="top" class="blackblodtext">
                                            <b>Temp de D�montage Buffer (minutes)</b>
                                        </td>
                                        <%--Edited For FF--%>
                                        <td style="width: 30%" align="left" valign="top">
                                            <%--Edited For FF--%>
                                            <asp:TextBox ID="txtTeardownTime" runat="server" CssClass="altText"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="regTeardownTime" ControlToValidate="txtTeardownTime"
                                                Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="Entrez uniquement des chiffres."
                                                ValidationGroup="Submit" ValidationExpression="^(\(|\d|\))*$"></asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <%--Window Dressing--%>
                                        <td align="right" valign="top" class="blackblodtext">
                                            <b>Projecteur disponible?</b>
                                        </td>
                                        <td align="left" valign="top">
                                            <asp:DropDownList ID="lstProjector" runat="server" CssClass="SelectFormat">
                                                <asp:ListItem Text="Oui" Value="1" Selected="True" />
                                                <asp:ListItem Text="Non" Value="0" />
                                            </asp:DropDownList>
                                        </td>
                                        <%--Window Dressing--%>
                                        <td align="right" valign="top" class="blackblodtext">
                                            <b>M�dia</b>
                                        </td>
                                        <td align="left" valign="top">
                                            <asp:DropDownList ID="lstVideo" runat="server" CssClass="SelectFormat">
                                                <asp:ListItem Text="[Rien]" Value="0" Selected="True" />
                                                <asp:ListItem Text="Audio-only" Value="1" />
                                                <asp:ListItem Text="Audio, Video" Value="2" />
                                                <%--FB 1744--%>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="regVideo" runat="server" ControlToValidate="lstVideo"
                                                Display="dynamic" CssClass="lblError" SetFocusOnError="true" Text="Requis"
                                                ValidationGroup="Submit"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <%--Window Dressing--%>
                                        <td style="width: 15%" align="right" valign="top" class="blackblodtext">
                                            <b>Administrateur en charge</b><span class="reqfldText">*</span>
                                        </td>
                                        <td style="width: 30%" align="left" valign="top">
                                            <%--Edited For FF--%>
                                            <asp:TextBox ID="Assistant" runat="server" CssClass="altText"></asp:TextBox>
                                            <a id="EditHref" href="javascript: getYourOwnEmailList(-2);" onmouseover="window.status='';return true;">
                                                <img border="0" src="image/edit.gif" alt="edit" width="17" height="15"></a>
                                            <a href="javascript: deleteAssistant();" onmouseover="window.status='';return true;">
                                                <img border="0" src="image/btn_delete.gif" alt="delete" width="16" height="16"></a>
                                            <asp:RequiredFieldValidator ID="AssistantValidator" runat="server" ControlToValidate="Assistant"
                                                Display="dynamic" CssClass="lblError" SetFocusOnError="true" Text="Requis"
                                                ValidationGroup="Submit"></asp:RequiredFieldValidator>
                                        </td>
                                        <%--Window Dressing--%>
                                        <td style="width: 20%" align="right" valign="top" class="blackblodtext">
                                            <b>Email adresses de l'assitant</b><br />
                                            (point-virgule de s�paration)
                                        </td>
                                        <%--Edited For FF--%>
                                        <td style="width: 30%" align="left" valign="top">
                                            <%--Edited For FF--%>
                                            <asp:TextBox ID="txtMultipleAssistant" Rows="2" TextMode="MultiLine" Width="275px"
                                                runat="server" CssClass="altText"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="regMultipleAssistant" ControlToValidate="txtMultipleAssistant"
                                                Display="dynamic" runat="server" ValidationGroup="Submit" SetFocusOnError="true"
                                                ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ ~ et &#34 sont des characteres invalides."
                                                ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>^+?|!`,\[\]{}\x22=:#$%&()'~]*$"></asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                    <tr id="trVIP">
                                        <%--FB 1982--%>
                                        <%--Window Dressing--%>
                                        <td align="right" valign="top" class="blackblodtext">
                                            <b>VIP salle de conf�rence</b>
                                        </td>
                                        <td align="left">
                                            <asp:DropDownList ID="DrpisVIP" runat="server" CssClass="SelectFormat">
                                                <asp:ListItem Text="Oui" Value="1" />
                                                <asp:ListItem Text="Non" Value="0" Selected="True" />
                                            </asp:DropDownList>
                                        </td>
                                        <td align="right" valign="top" class="blackblodtext">
                                            &nbsp;
                                        </td>
                                        <td align="left">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <%--Window Dressing--%>
                                        <td align="right" valign="top" class="blackblodtext">
                                            <b>Restauration</b>
                                        </td>
                                        <%-- FB 2050 --%>
                                        <td align="left">
                                            <%-- FB 2050 --%>
                                            <asp:DropDownList ID="CatererList" runat="server" CssClass="SelectFormat">
                                                <asp:ListItem Text="Oui" Value="1" Selected="True" />
                                                <asp:ListItem Text="Non" Value="0" />
                                            </asp:DropDownList>
                                        </td>
                                        <td align="right" valign="top" class="blackblodtext">
                                            <b>Acc�s handicap�</b>
                                        </td>
                                        <%-- FB 2050 --%>
                                        <td align="left">
                                            <%-- FB 2050 --%>
                                            <asp:DropDownList ID="DrpHandi" runat="server" CssClass="SelectFormat">
                                                <asp:ListItem Text="Oui" Value="1" />
                                                <asp:ListItem Text="Non" Value="0" Selected="True" />
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" valign="top" class="blackblodtext">
                                            <b>salle de t�l�pr�sence</b>
                                        </td>
                                        <td align="left">
                                            <asp:DropDownList ID="DrpisTelepresence" runat="server" CssClass="SelectFormat">
                                                <asp:ListItem Text="Yes" Value="1" />
                                                <asp:ListItem Text="No" Value="0" Selected="True" />
                                            </asp:DropDownList>
                                        </td>
                                        <td align="right" valign="Middle" class="blackblodtext">
                                            <b>Type de service</b>
                                        </td>
                                        <%--FB 2219--%>
                                        <td align="left">
                                            <asp:DropDownList ID="DrpServiceType" CssClass="SelectFormat" DataTextField="Name"
                                                DataValueField="ID" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr> <%--FB 2334--%>
                                    <td align="right" height="38"><span class="blackblodtext">D�di� vid�o</span></td>
                                        <td align="left">
                                            <input type="checkbox" id="Chkdedicatedvideo" runat="server" />
                                        </td>
                                        <%--FB 2390--%>
                                        <td align="right" height="38"><span class="blackblodtext">Codec Pr�sentation d�di�</span></td>
                                        <td align="left">
                                            <input type="checkbox" id="Chkdedpresentcodec" runat="server" />
                                        </td>
                                        </tr>
                                         <%--FB 2415 Start--%>
                                        <tr> 
                                        <td align="right" height="38"><span class="blackblodtext">AV email support sur site</span></td>
                                        <td align="left">
                                            <asp:TextBox ID="txtAVOnsiteSupportEmail" runat="server" CssClass="altText"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="regEmail1_1" ControlToValidate="txtAVOnsiteSupportEmail" Display="dynamic" runat="server" 
                                                ErrorMessage="<br>Adresse email invalide." ValidationExpression="[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"></asp:RegularExpressionValidator>
                                            <asp:RegularExpressionValidator ID="regEmail1_2" ControlToValidate="txtAVOnsiteSupportEmail" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ /(); ? | ^= ! ` , [ ] { } : # $ ~ and &#34; des caract�res non valides." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=:#$%&'~]*$"></asp:RegularExpressionValidator>
                                        </td>
                                        <td colspan="2"></td>
                                        </tr>
                                        <%--FB 2415 End--%>
                                </table>
                                <%-- Configuration de base Param�tres end ici --%>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <%--Location--%>
                <td align="center">
                    <table id="tblLocation" cellpadding="2" cellspacing="2" border="0" style="width: 95%">
                        <tr>
                            <%--Window Dressing--%>
                            <td align="left">
                                <span class="subtitleblueblodtext">Salle de Conf�rence</span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table id="tblLocationDetails" cellpadding="2" cellspacing="2" border="0" style="width: 95%">
                                    <%-- Configuration de base Param�tres starts ici --%>
                                    <tr>
                                        <%--Window Dressing--%>
                                        <td style="width: 15%" align="right" valign="top" class="blackblodtext">
                                            <b>Haut niveau</b> <span class="reqfldText">*</span>
                                        </td>
                                        <td style="width: 35%" align="left" valign="top">
                                            <%--Edited For FF & FB 2050 --%>
                                            <asp:DropDownList ID="lstTopTier" DataTextField="Name" DataValueField="ID" runat="server"
                                                CssClass="altSelectFormat" OnSelectedIndexChanged="UpdateMiddleTiers" AutoPostBack="true"
                                                onchange="javascript:DataLoading(1)">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="reqTopTier" runat="server" ControlToValidate="lstTopTier"
                                                Display="dynamic" CssClass="lblError" SetFocusOnError="true" Text="Requis"
                                                ValidationGroup="Submit"></asp:RequiredFieldValidator>
                                        </td>
                                        <%--Window Dressing--%>
                                        <td style="width: 20%" align="right" valign="top" class="blackblodtext">
                                            <b>Moyen niveau</b><%--Edited For FF--%>
                                            <span class="reqfldText">*</span>
                                        </td>
                                        <td style="width: 30%" align="left" valign="top">
                                            <%--Edited For FF--%>
                                            <asp:DropDownList ID="lstMiddleTier" DataTextField="Name" DataValueField="ID" runat="server"
                                                CssClass="altSelectFormat">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="reqMiddleTier" runat="server" ControlToValidate="lstMiddleTier"
                                                Display="dynamic" CssClass="lblError" SetFocusOnError="true" Text="Requis"
                                                ValidationGroup="Submit"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <%--Window Dressing--%>
                                        <td style="width: 15%" align="right" valign="top" class="blackblodtext">
                                            <b>Etage</b>
                                        </td>
                                        <td style="width: 30%" align="left" valign="top">
                                            <%--Edited For FF--%>
                                            <asp:TextBox ID="txtFloor" runat="server" CssClass="altText"></asp:TextBox>
                                        </td>
                                        <%--Window Dressing--%>
                                        <td style="width: 20%" align="right" valign="top" class="blackblodtext">
                                            <b>salle de conf�rence #</b>
                                        </td>
                                        <%--Edited For FF--%>
                                        <td style="width: 30%" align="left" valign="top">
                                            <%--Edited For FF--%>
                                            <asp:TextBox ID="txtRoomNumber" runat="server" CssClass="altText"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <%--Window Dressing--%>
                                        <td style="width: 15%" align="right" valign="top" class="blackblodtext">
                                            <b>adresse postale 1</b>
                                        </td>
                                        <td style="width: 30%" align="left" valign="top">
                                            <%--Edited For FF--%>
                                            <asp:TextBox ID="txtStreetAddress1" runat="server" CssClass="altText"></asp:TextBox>
                                        </td>
                                        <%--Window Dressing--%>
                                        <td style="width: 20%" align="right" valign="top" class="blackblodtext">
                                            <b>adresse postale 2</b>
                                        </td>
                                        <%--Edited For FF--%>
                                        <td style="width: 30%" align="left" valign="top">
                                            <%--Edited For FF--%>
                                            <asp:TextBox ID="txtStreetAddress2" runat="server" CssClass="altText"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <%--Window Dressing--%>
                                        <td style="width: 15%" align="right" valign="top" class="blackblodtext">
                                            <b>Ville</b>
                                        </td>
                                        <td style="width: 30%" align="left" valign="top">
                                            <%--Edited For FF--%>
                                            <asp:TextBox ID="txtCity" runat="server" CssClass="altText"></asp:TextBox>
                                        </td>
                                        <%-- code added for FB 146--%>
                                        <td style="width: 20%" align="right" valign="top">
                                            <%--Edited For FF--%>
                                            <table>
                                                <tr>
                                                    <%--Window Dressing--%>
                                                    <td class="blackblodtext" align="right">
                                                        <b>Pays </b>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <%--Window Dressing--%>
                                                    <td class="blackblodtext">
                                                        <b>R�gion / Code postal</b>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <%--FB 146--%>
                                        <td style="width: 30%" align="left" valign="top">
                                            <%--Edited For FF--%>
                                            <asp:DropDownList ID="lstCountries" CssClass="altText" runat="server" DataTextField="Name"
                                                DataValueField="ID" OnSelectedIndexChanged="UpdateStates" AutoPostBack="true" Width="275px" > <%-- FB 2050 --%>
                                            </asp:DropDownList>
                                            <br />
                                            <asp:DropDownList ID="lstStates" CssClass="altText" Width="50" runat="server" DataTextField="Code"
                                                DataValueField="ID">
                                            </asp:DropDownList>
                                            <asp:TextBox ID="txtZipCode" Width="50" runat="server" CssClass="altText"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="regZipCode" ControlToValidate="txtZipCode" Display="dynamic"
                                                runat="server" SetFocusOnError="true" ErrorMessage="<br> & < > ' + % \ ; ? | ^ = ! ` [ ] { } : # $ @ ~ et &#34; sont des characteres invalides."
                                                ValidationGroup="Submit" ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>^+;?|!`\[\]{}\x22;=:@#$%&'~]*$"></asp:RegularExpressionValidator><%--FB 2222--%>
                                        </td>
                                    </tr>
                                    <%--FB 2342 starts--%>
                                    <tr >
                                    <td style="width: 15%" align="right" valign="top" class="blackblodtext"><b>Queue Email Chambre</b>
                                    </td><td align="left" valign="top" height="10" style ="width :50">
                                     <asp:TextBox ID ="txtRoomEmail" runat ="server" CssClass="altText"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="reqRoomEmail" ControlToValidate="txtRoomEmail" Display="dynamic" ErrorMessage="<B>Requis" runat="server" Enabled="false" CssClass="lblError"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="reqroomEmail1_1"  ValidationGroup ="Submit" ControlToValidate="txtRoomEmail" Display="dynamic" runat="server"  Enabled="false" ErrorMessage ="<B>Invalid Email Id" ValidationExpression="[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$" SetFocusOnError ="true"  CssClass="lblError"></asp:RegularExpressionValidator>
                                    <asp:RegularExpressionValidator ID="reqroomEmail1_2" ControlToValidate="txtRoomEmail"  ValidationGroup ="Submit" Display="dynamic" runat="server"  Enabled="false" SetFocusOnError="true"  ErrorMessage="<br>& < > ' + % \ /(); ? | ^= ! ` , [ ] { } : # $ ~ et &#34; sont des characteres invalides." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=:#$%&'~]*$" CssClass="lblError"></asp:RegularExpressionValidator>
                                    </td>
                                    </tr>
                                    <%--FB 2342 end--%>
                                    <tr>
                                        <%--Window Dressing--%>
                                        <td style="width: 15%" align="right" valign="top" class="blackblodtext">
                                            <b>Direction du garage</b>
                                        </td>
                                        <td style="width: 30%" align="left" valign="top">
                                            <%--Edited For FF--%>
                                            <asp:TextBox ID="txtParkingDirections" TextMode="MultiLine" Rows="2" Width="275"
                                                runat="server" CssClass="altText"></asp:TextBox>
                                            <%--FB 2050--%>
                                            <asp:RegularExpressionValidator ID="regParkDirections" ControlToValidate="txtParkingDirections"
                                                Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="' et `  c'est le caract�re non valide."
                                                ValidationGroup="Submit" ValidationExpression="[^`']*"></asp:RegularExpressionValidator>
                                        </td>
                                        <%--Window Dressing--%>
                                        <td style="width: 20%" align="right" valign="top" class="blackblodtext">
                                            <b>Additional Commentaire</b>
                                        </td>
                                        <%--Edited For FF--%>
                                        <td style="width: 30%" align="left" valign="top">
                                            <%--Edited For FF--%>
                                            <asp:TextBox ID="txtAdditionalComments" TextMode="MultiLine" Rows="2" Width="275"
                                                runat="server" CssClass="altText"></asp:TextBox>
                                            <%--FB 2050--%>
                                            <asp:RegularExpressionValidator ID="regAddComments" ControlToValidate="txtAdditionalComments"
                                                Display="dynamic" runat="server" ValidationGroup="Submit" SetFocusOnError="true"
                                                ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ et &#34; sont des characteres invalides."
                                                ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <%--Window Dressing--%>
                                        <td style="width: 15%" align="right" valign="top" class="blackblodtext">
                                            <b>Link Carte</b>
                                        </td>
                                        <td style="width: 30%" align="left" valign="top">
                                            <%--Edited For FF--%>
                                            <asp:TextBox ID="txtMapLink" TextMode="MultiLine" Rows="2" Width="275" runat="server"
                                                CssClass="altText"></asp:TextBox>
                                            <%--FB 2050--%>
                                            <asp:RegularExpressionValidator ID="regMapLink" ControlToValidate="txtMapLink" Display="dynamic"
                                                runat="server" SetFocusOnError="true" ErrorMessage="' et ` sont des characteres invalides."
                                                ValidationGroup="Submit" ValidationExpression="[^`']*"></asp:RegularExpressionValidator>
                                        </td>
                                        <%--Window Dressing--%>
                                        <%--Code changed for FB 1425 QA Bug -Start--%>
                                        <td style="width: 20%" align="right" valign="top" id="TzTD1" runat="server" class="blackblodtext">
                                            <b>Fuseau horaire</b><%--Edited For FF--%>
                                            <span class="reqfldText">*</span>
                                        </td>
                                        <td style="width: 30%" align="left" valign="top" id="TzTD2" runat="server">
                                            <%--Edited For FF--%>
                                            <asp:DropDownList ID="lstTimezone" runat="server" CssClass="altSelectFormat" DataTextField="timezoneName"
                                                DataValueField="timezoneID">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="regTimeZone" runat="server" ControlToValidate="lstTimezone"
                                                Display="dynamic" CssClass="lblError" SetFocusOnError="true" Text="Requis"
                                                ValidationGroup="Submit"></asp:RequiredFieldValidator>
                                        </td>
                                        <%--Code changed for FB 1425 QA Bug -End--%>
                                    </tr>
                                    <tr>
                                        <%--Window Dressing--%>
                                        <td style="width: 15%" align="right" valign="top" class="blackblodtext">
                                            <b>latitude</b>
                                        </td>
                                        <%--Edited For FF and FB 2050 --%>
                                        <td style="width: 30%" align="left" valign="top">
                                            <%--Edited For FF--%>
                                            <asp:TextBox ID="txtLatitude" runat="server" CssClass="altText"></asp:TextBox>
                                        </td>
                                        <%--Window Dressing--%>
                                        <td style="width: 20%" align="right" valign="top" class="blackblodtext">
                                            <b>Longitude</b>
                                        </td>
                                        <%-- FB 2050 --%>
                                        <td style="width: 30%" align="left" valign="top">
                                            <%--Edited For FF--%>
                                            <asp:TextBox ID="txtLongitude" runat="server" CssClass="altText"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                                <%-- Salle de Conf�rence Param�tres end ici --%>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <%--Images--%>
                <td align="center">
                    <table id="tblImages" cellpadding="2" cellspacing="2" border="0" style="width: 90%">
                        <tr>
                            <td align="left" colspan="4">
                                <span class="subtitleblueblodtext">Images</span>
                            </td>
                        </tr>
                        <tr style="display: none">
                            <%--Window Dressing--%>
                            <%--Window Dressing--%>
                            <td align="right" valign="top" class="blackblodtext" style="display: none">
                                <b>salle de conf�rence Image</b>
                                <br />
                                <i style="font-size: xx-small">Utilisez la touche CTRL enfonc�e pour s�lectionner plusieurs images.</i>
                            </td>
                            <td align="left" valign="top" style="display: none">
                                <asp:ListBox ID="lstRoomImage" runat="server" CssClass="SelectFormat" SelectionMode="multiple">
                                </asp:ListBox>
                                <%--code added for Soft Edge button--%>
                                <input type="button" name="Editer" class="altShortBlueButtonFormat" onclick="javascript:fnOpen()"
                                    style="width: 30pt" value="Editer" />
                            </td>
                        </tr>
                        <tr>
                            <td align="left" colspan="4">
                                <%-- <asp:UpdatePanel ID="RoomImgUpdatePanel" runat="server"  UpdateMode="Always" RenderMode="Inline" >
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="BtnUploadRmImg"  />
                                    </Triggers>
                                    <ContentTemplate>
                                        <asp:Panel  id="RoomImagePanel" runat="server">--%>
                                <table align="left" cellpadding="2" cellspacing="0" border="0" style="width:95%">
                                    <!-- FB 2050 -->
                                    <tr>
                                        <td align="right" style="width: 25%" valign="top" class="blackblodtext">
                                            <b>Dynamic salle de conf�rence Layout</b>
                                        </td>
                                        <!-- FB 2050 -->
                                        <td align="left" style="width: 75%" valign="top">
                                            <!-- FB 2050 -->
                                            <asp:DropDownList ID="lstDynamicRoomLayout" runat="server" CssClass="SelectFormat" Width="175px"> <%-- FB 2050 --%>
                                                <asp:ListItem Text="Permis" Value="1" Selected="True" />
                                                <asp:ListItem Text="disable" Value="0" />
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr><%--FB 2050--%>
                                        <%--FB 2136 Start--%>
                                        <td align="right" class="blackblodtext" valign="top">
                                            <b>Image pour Badge de securit�</b>
                                            </td><td colspan="3"><%--FB 2050--%>
                                            <asp:DropDownList ID="drpSecImgList" runat="server" DataTextField="badgename" DataValueField="badgeid"
                                                CssClass="SelectFormat" Width="175px" EnableViewState="true" onchange="javascript:fnSecImgSelection();">
                                            </asp:DropDownList>
                                            <input id="btnMngSecImg" type="button" value="G�rer" class="altShortBlueButtonFormat"
                                                onclick="javascript:return fnOpenSecurityBadge()" />
                                            <br />
                                            <br />
                                        </td>
                                        <%--FB 2136 End--%>
                                    </tr>
                                    <tr>
                                        <td align="right" valign="top" class="blackblodtext">
                                            <b>salle de conf�rence Image</b>
                                        </td>
                                        <td colspan="3" align="left" class="blackblodtext">
                                            <input type="file" id="roomfileimage" contenteditable="false" enableviewstate="true"
                                                size="45" class="altText" runat="server" />
                                            <asp:Button ID="BtnUploadRmImg" Width="250px" CssClass="altLongBlueButtonFormat"
                                                runat="server" Text="Envoyer salle de conf�rence Image" OnClick="UploadRoomImage" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <%-- FB 2136 Starts --%>
                                        <td align="left" colspan="3">
                                            <div style="overflow-y: hidden; overflow-x: auto; height: auto; width: 600px;">
                                                <asp:DataGrid BorderColor="blue" BorderStyle="solid" BorderWidth="1" ID="dgItems"
                                                    AutoGenerateColumns="false" OnItemCreated="BindRowsDeleteMessage" OnDeleteCommand="RemoveImage"
                                                    runat="server" Width="70%" GridLines="None" Visible="false" Style="border-collapse: separate">
                                                    <HeaderStyle Height="30" CssClass="tableHeader" HorizontalAlign="Center" />
                                                    <AlternatingItemStyle CssClass="tableBody" />
                                                    <ItemStyle CssClass="tableBody" />
                                                    <FooterStyle CssClass="tableBody" />
                                                    <Columns>
                                                        <asp:BoundColumn DataField="ImageName" Visible="true" HeaderText="Nom" HeaderStyle-CssClass="tableHeader"
                                                            ItemStyle-Width="20%"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="Image" Visible="false"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="Imagetype" Visible="false"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="ImagePath" Visible="false"></asp:BoundColumn>
                                                        <asp:TemplateColumn HeaderText="Image" HeaderStyle-CssClass="tableHeader" ItemStyle-Width="40%"
                                                            ItemStyle-HorizontalAlign="center">
                                                            <ItemTemplate>
                                                                <asp:Image ID="itemImage" ImageUrl='<%# DataBinder.Eval(Container, "DataItem.ImagePath") %>'
                                                                    Width="30" Height="30" runat="server" />
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="Actions" HeaderStyle-CssClass="tableHeader" ItemStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="btnDelete" Text="Retirer" CommandName="Delete" runat="server"></asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                    </Columns>
                                                </asp:DataGrid><br />
                                            </div>
                                            <%-- FB 2136 Ends --%>
                                        </td>
                                    </tr>
                                </table>
                                <%-- </asp:Panel>
                                     </ContentTemplate>
                                </asp:UpdatePanel>--%>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" colspan="4">
                                <table align="left" id="tblImagesDetails" cellpadding="2" cellspacing="0" border="0"
                                    style="width: 95%"><%-- FB 2050 --%>
                                    <%--FB 2136--%>
                                    <%-- Images Parameters starts here --%>
                                    <tr>
                                        <%--Window Dressing--%>
                                        <td style="width: 15%" align="right" valign="top" class="blackblodtext">
                                            <b>Carte 1</b>
                                        </td>
                                        <td style="width: 85%" align="left" valign="top">
                                            <table width="100%">
                                                <tr>
                                                    <td>
                                                        <input type="file" id="fleMap1" contenteditable="false" enableviewstate="true" size="50"
                                                            class="altText" runat="server" />
                                                        <cc1:ImageControl ID="Map1ImageCtrl" Width="30" Height="30" Visible="false" runat="server">
                                                        </cc1:ImageControl>
                                                        <asp:Label ID="lblUploadMap1" Text="" Visible="false" runat="server"></asp:Label>
                                                        <asp:Button ID="btnRemoveMap1" CssClass="altShortBlueButtonFormat" Text="Retirer"
                                                            Visible="false" runat="server" OnCommand="RemoveFile" CommandArgument="1" />
                                                        <asp:Label ID="hdnUploadMap1" Text="" Visible="false" runat="server"></asp:Label>
                                                        <input type="hidden" id="Map1ImageDt" name="Map1ImageDt" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <%--Window Dressing--%>
                                        <td style="width: 25%" align="right" valign="top" class="blackblodtext">
                                            <b>Carte 2</b>
                                        </td>
                                        <td style="width: 75%" align="left" valign="top">
                                            <table width="100%">
                                                <tr>
                                                    <td>
                                                        <input type="file" id="fleMap2" contenteditable="false" enableviewstate="true" size="50"
                                                            class="altText" runat="server" />
                                                        <cc1:ImageControl ID="Map2ImageCtrl" Width="30" Height="30" Visible="false" runat="server">
                                                        </cc1:ImageControl>
                                                        <asp:Label ID="lblUploadMap2" Text="" Visible="false" runat="server"></asp:Label>
                                                        <asp:Button ID="btnRemoveMap2" CssClass="altShortBlueButtonFormat" Text="Retirer"
                                                            Visible="false" runat="server" OnCommand="RemoveFile" CommandArgument="2" />
                                                        <asp:Label ID="hdnUploadMap2" Text="" Visible="false" runat="server"></asp:Label>
                                                        <input type="hidden" id="Map2ImageDt" name="Map2ImageDt" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr style="display: none">
                                        <%--FB 2136--%>
                                        <%--Window Dressing--%>
                                        <td style="width: 25%" align="right" valign="top" class="blackblodtext">
                                            <b>S�curit� 1</b>
                                        </td>
                                        <td style="width: 75%" align="left" valign="top">
                                            <table width="100%">
                                                <tr>
                                                    <td>
                                                        <input type="file" id="fleSecurity1" contenteditable="false" enableviewstate="true"
                                                            size="50" class="altText" runat="server" />
                                                        <cc1:ImageControl ID="Sec1ImageCtrl" Width="30" Height="30" Visible="false" runat="server">
                                                        </cc1:ImageControl>
                                                        <asp:Label ID="lblUploadSecurity1" Text="" Visible="false" runat="server"></asp:Label>
                                                        <asp:Button ID="btnRemoveSecurity1" CssClass="altShortBlueButtonFormat" Text="Retirer"
                                                            Visible="false" runat="server" OnCommand="RemoveFile" CommandArgument="3" />
                                                        <asp:Label ID="hdnUploadSecurity1" Text="" Visible="false" runat="server"></asp:Label>
                                                        <input type="hidden" id="Sec1ImageDt" name="Sec1ImageDt" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr style="display: none">
                                        <%--FB 2136--%>
                                        <%--Window Dressing--%>
                                        <td style="width: 25%" align="right" valign="top" class="blackblodtext">
                                            <b>S�curit� 2</b>
                                        </td>
                                        <td style="width: 75%" align="left" valign="top">
                                            <table width="100%">
                                                <tr>
                                                    <td>
                                                        <input type="file" id="fleSecurity2" contenteditable="false" enableviewstate="true"
                                                            size="50" class="altText" runat="server" />
                                                        <cc1:ImageControl ID="Sec2ImageCtrl" Width="30" Height="30" Visible="false" runat="server">
                                                        </cc1:ImageControl>
                                                        <asp:Label ID="lblUploadSecurity2" Text="" Visible="false" runat="server"></asp:Label>
                                                        <asp:Button ID="btnRemoveSecurity2" CssClass="altShortBlueButtonFormat" Text="Retirer"
                                                            Visible="false" runat="server" OnCommand="RemoveFile" CommandArgument="4" />
                                                        <asp:Label ID="hdnUploadSecurity2" Text="" Visible="false" runat="server"></asp:Label>
                                                        <input type="hidden" id="Sec2ImageDt" name="Sec2ImageDt" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <%--Window Dressing--%>
                                        <td style="width: 25%" align="right" valign="top" class="blackblodtext">
                                            <b>Divers 1</b>
                                        </td>
                                        <td style="width: 75%" align="left" valign="top">
                                            <table width="100%">
                                                <tr>
                                                    <td>
                                                        <input type="file" id="fleMisc1" contenteditable="false" enableviewstate="true" size="50"
                                                            class="altText" runat="server" />
                                                        <cc1:ImageControl ID="Misc1ImageCtrl" Width="30" Height="30" Visible="false" runat="server">
                                                        </cc1:ImageControl>
                                                        <asp:Label ID="lblUploadMisc1" Text="" Visible="false" runat="server"></asp:Label>
                                                        <asp:Button ID="btnRemoveMisc1" CssClass="altShortBlueButtonFormat" Text="Retirer"
                                                            Visible="false" runat="server" OnCommand="RemoveFile" CommandArgument="5" />
                                                        <asp:Label ID="hdnUploadMisc1" Text="" Visible="false" runat="server"></asp:Label>
                                                        <input type="hidden" id="Misc1ImageDt" name="Misc1ImageDt" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <%--Window Dressing--%>
                                        <td style="width: 25%" align="right" valign="top" class="blackblodtext">
                                            <b>Divers 2</b>
                                        </td>
                                        <td style="width: 75%" align="left" valign="top">
                                            <table width="100%">
                                                <tr>
                                                    <td>
                                                        <input type="file" id="fleMisc2" contenteditable="false" enableviewstate="true" size="50"
                                                            class="altText" runat="server" />
                                                        <cc1:ImageControl ID="Misc2ImageCtrl" Width="30" Height="30" Visible="false" runat="server">
                                                        </cc1:ImageControl>
                                                        <asp:Label ID="lblUploadMisc2" Text="" Visible="false" runat="server"></asp:Label>
                                                        <asp:Button ID="btnRemoveMisc2" CssClass="altShortBlueButtonFormat" Text="Retirer"
                                                            Visible="false" runat="server" OnCommand="RemoveFile" CommandArgument="6" />
                                                        <asp:Label ID="hdnUploadMisc2" Text="" Visible="false" runat="server"></asp:Label>
                                                        <input type="hidden" id="Misc2ImageDt" name="Misc2ImageDt" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                                <%-- Images Param�tres ends ici --%>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="4">
                                <%--<asp:Button ID="btnUploadFiles" OnClick="UploadFiles" runat="server" Text="Chargement des images" CssClass="altLongBlueButtonFormat" />--%>
                                <asp:Button ID="btnUploadImages" OnClick="UploadOtherImages" runat="server" Text="Chargement des images"
                                    CssClass="altLongBlueButtonFormat" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <%--Approvers--%>
                <td align="center">
                    <table id="tblApprovers" cellpadding="2" cellspacing="2" border="0" style="width: 90%">
                        <tr>
                            <td align="left">
                                <span class="subtitleblueblodtext" runat="server" id="spnConf">Conference salle de conf�rence
                                    Approvers</span>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <table id="Table1" cellpadding="2" cellspacing="2" border="0" style="width: 95%">
                                    <%-- Images Param�tres starts ici --%>
                                    <tr>
                                        <%--Window Dressing--%>
                                        <td style="width: 25%" align="right" valign="top" class="blackblodtext">
                                            <b>Approbateur principal</b>
                                        </td>
                                        <td style="width: 75%" align="left" valign="top">
                                            <asp:TextBox ID="Approver0" CssClass="altText" runat="server" />
                                            <a href="javascript: getYourOwnEmailList(0);" onmouseover="window.status='';return true;">
                                                <img border="0" src="image/edit.gif" alt="edit" width="17" height="15"></a>
                                            <a href="javascript: deleteApprover(0);" onmouseover="window.status='';return true;">
                                                <img border="0" src="image/btn_delete.gif" alt="delete" width="16" height="16"></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <%--Window Dressing--%>
                                        <td style="width: 25%" align="right" valign="top" class="blackblodtext">
                                            <b>Approbateur secondaire 1</b>
                                        </td>
                                        <td style="width: 75%" align="left" valign="top">
                                            <asp:TextBox ID="Approver1" CssClass="altText" runat="server" />
                                            <!-- Code Modified by Offshore FB # 412 Start(Changed getYourOwnEmailList(0) and deleteApprover() � getYourOwnEmailList(1))  -->
                                            <a href="javascript: getYourOwnEmailList(1);" onmouseover="window.status='';return true;">
                                                <img border="0" src="image/edit.gif" alt="edit" width="17" height="15"></a>
                                            <a href="javascript: deleteApprover(1);" onmouseover="window.status='';return true;">
                                                <img border="0" src="image/btn_delete.gif" alt="delete" width="16" height="16"></a>
                                            <!-- Code Modified by Offshore FB # 412 End  -->
                                        </td>
                                    </tr>
                                    <tr>
                                        <%--Window Dressing--%>
                                        <td style="width: 25%" align="right" valign="top" class="blackblodtext">
                                            <b>Approbateur secondaire 2</b>
                                        </td>
                                        <td style="width: 75%" align="left" valign="top">
                                            <asp:TextBox ID="Approver2" CssClass="altText" runat="server" />
                                            <!-- Code Modified by Offshore FB # 412 Start(Changed getYourOwnEmailList(0) � getYourOwnEmailList(2))  -->
                                            <a href="javascript: getYourOwnEmailList(2);" onmouseover="window.status='';return true;">
                                                <img border="0" src="image/edit.gif" alt="edit" width="17" height="15"></a>
                                            <a href="javascript: deleteApprover(2);" onmouseover="window.status='';return true;">
                                                <img border="0" src="image/btn_delete.gif" alt="delete" width="16" height="16"></a>
                                            <!-- Code Modified by Offshore FB # 412 Start(Changed getYourOwnEmailList(0) � getYourOwnEmailList(2))  -->
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <%--Endpoint--%>
                <td align="center">
                    <table id="tblEndpoint" cellpadding="2" cellspacing="2" border="0" style="width: 90%">
                        <tr>
                            <td align="left">
                                <span class="subtitleblueblodtext">cession Endpoint</span>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <table id="Table2" cellpadding="2" cellspacing="2" border="0" style="width: 95%">
                                    <%-- Images Param�tres starts ici --%>
                                    <tr>
                                        <%--Window Dressing--%>
                                        <%--Endpoint Search--%>
                                        <td style="width: 25%" align="right" valign="top" class="blackblodtext">
                                            <b>Endpoint</b>
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" ID="txtEndpoint" CssClass="altText" Width="40%" ReadOnly="true"></asp:TextBox>
                                            <a href="javascript: deleteEndpoint();" onmouseover="window.status='';return true;">
                                                <img border="0" src="image/btn_delete.gif" alt="delete" width="16" height="16"></a>
                                            <asp:Button Text="Ajouter Endpoint" Width="150px" class="altShortBlueButtonFormat"
                                                OnClientClick="javascript:OpenEndpointSearch('frmMainroom');" OnClick="BindEndpoint"
                                                runat="server" ID="addEndpoint" />
                                            <input name="opnEndpoint" type="button" id="opnEndpoint" onclick="javascript:OpenEndpointSearch('frmMainroom');"
                                                value="Add Endpoint" class="altShortBlueButtonFormat" style="display: none;" />
                                            <input name="addEndpoint" type="button" id="addEndpoint1" onclick="javascript:AddEndpoint();"
                                                style="display: none;" /><br />
                                            <asp:RequiredFieldValidator ID="regEndPoint" runat="server" ControlToValidate="lstEndpoint"
                                                Display="dynamic" CssClass="lblError" SetFocusOnError="true" Text="Requis"
                                                ValidationGroup="Submit"></asp:RequiredFieldValidator>
                                        </td>
                                        <td style="width: 75%; display: none" align="left" valign="top">
                                            <%--Edited for FF--%>
                                            <asp:DropDownList ID="lstEndpoint" runat="server" CssClass="altSelectFormat" DataTextField="Name"
                                                DataValueField="ID" onChange="chgEndpoint();">
                                                <asp:ListItem Text="Please Select..." Value="-1"></asp:ListItem>
                                                <%--Code added for FB 1257--%>
                                            </asp:DropDownList>
                                            <asp:Button ID="btnEndpointDetails" CssClass="altShortBlueButtonFormat" Text="Voir les d�tails"
                                                runat="server" OnClientClick="ViewEndpointDetails();return false;" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <%if (hdnMultipleDept.Value == "1")
              {
            %>
            <tr>
                <td align="center">
                    <table cellpadding="2" cellspacing="2" border="0" style="width: 90%">
                        <tr>
                            <td align="left">
                                <span class="subtitleblueblodtext">d�partement</span>
                            </td>
                        </tr>
                        <tr align="left">
                            <td align="left">
                                <table id="Table3" cellpadding="2" cellspacing="2" border="0" style="width: 95%">
                                    <%-- Images Param�tres starts ici --%>
                                    <tr>
                                        <%--Window Dressing--%>
                                        <td width="25%" align="right" valign="top" class="blackblodtext">
                                            <b>salle de conf�rence's D�partements</b>
                                        </td>
                                        <td style="width: 75%" align="left" valign="top">
                                            <asp:ListBox ID="DepartmentList" runat="server" CssClass="altText" DataTextField="Name"
                                                DataValueField="ID" SelectionMode="multiple"></asp:ListBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <%} %>
            <tr>
                <td style="height: 60px">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table id="tblButtons" cellpadding="2" cellspacing="2" border="0" style="width: 90%">
                        <tr>
                            <td align="center" style="width: 33%">
                                <asp:Button ID="btnReset" runat="server" Text="Remise � zero" CssClass="altLongBlueButtonFormat"
                                    OnClick="ResetRoomProfile" />
                            </td>
                            <td align="center" style="width: 33%">
                                <%--code added for Soft Edge button--%>
                                <input name="Go" type="button" class="altLongBlueButtonFormat" onclick="javascript:fnClose();"
                                    value=" Retourner " />
                            </td>
                            <td align="center" style="width: 33%">
                                <asp:Button ID="btnSubmitAddNew" Width="300px" runat="server" ValidationGroup="Submit"
                                    Text="Soumettre / Nouveau salle de conf�rence" CssClass="altLongBlueButtonFormat"
                                    OnClick="SubmitAddNewRoomProfile" OnClientClick="javascript:return frmMainroom_Validator()" />
                            </td>
                            <td align="center" style="width: 33%">
                                <asp:Button ID="btnSubmit" ValidationGroup="Submit" runat="server" Text="Soumettre"
                                    CssClass="altLongBlueButtonFormat" OnClick="SubmitRoomProfile" OnClientClick="javascript:return frmMainroom_Validator()" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <%--table 1 ends here--%>
        <input type="hidden" id="Approver0ID" runat="server" />
        <input type="hidden" id="Approver1ID" runat="server" />
        <input type="hidden" id="Approver2ID" runat="server" />
        <input type="hidden" id="AssistantID" runat="server" />
        <input type="hidden" id="AssistantName" runat="server" />
    </div>
    </form>
</body>
</html>
<%--code added for Soft Edge button--%>

<script type="text/javascript">
if("<%=Session["isVIP"]%>" == "0")//FB 1982
{
    document.getElementById("trVIP").style.display = "none"; 
}
</script>

<script type="text/javascript" src="inc/softedge.js"></script>

<% 
    if (Request.QueryString["cal"] == "2")
    { 
%>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
<%} %>

<script>chgEndpoint();</script>


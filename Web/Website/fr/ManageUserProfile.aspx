<%@ Page Language="C#" Inherits="ns_MyVRM.UserProfile" Buffer="true" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %><!-- FB 2481 -->
<meta http-equiv="X-UA-Compatible" content="IE=7" /> <!-- FB 2050 -->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <!-- FB 2050 -->
<!--Window Dressing-->
<!-- #INCLUDE FILE="inc/maintopNET.aspx" --> 
<%--FB 2481 start--%>
<%@ Register Assembly="DevExpress.SpellChecker.v10.2.Core, Version=10.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraSpellChecker" TagPrefix="dxXSC" %>
<%@ Register Assembly="DevExpress.Web.ASPxSpellChecker.v10.2, Version=10.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxSpellChecker" TagPrefix="dxSC" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v10.2, Version=10.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dxHE" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v10.2, Version=10.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxE" %>
<%@ Register Assembly="DevExpress.Web.v10.2, Version=10.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dxP" %>
<%@ Register Assembly="DevExpress.Web.v10.2, Version=10.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxRoundPanel" TagPrefix="dxRP" %>
    <%@ Register Assembly="DevExpress.Web.v10.2, Version=10.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGlobalEvents" TagPrefix="dx" %>
<%--FB 2481 end--%>
<script type="text/javascript">
  var servertoday = new Date();
</script>
<script type="text/javascript" src="inc/functions.js"></script>
<%--FB 1861--%>
<%--<script type="text/javascript" src="script/cal.js"></script>--%>
<script type="text/javascript" src="script/cal-flat.js"></script>
<script type="text/javascript" src="lang/calendar-en.js"></script>
<script type="text/javascript" src="script/calendar-setup.js"></script>
<script type="text/javascript" src="script/calendar-flat-setup.js"></script>

<script type="text/javascript" src="script/mytreeNET.js"></script>
<%--//FB 1830 - Translation Menu--%>
<script language="javascript" src="Organizations/Original/Javascript/RGBColorPalette.js"> </script>
<script type="text/javascript" src="script/RoomSearch.js"></script>
<link rel="stylesheet" type="text/css" media="all" href="css/calendar-blue.css" /> <%--FB 1861--%> <%--FB 1982--%>

<style type="text/css">
#lstDepartment option /* FB 2611 */
{
	color:Black;
}
</style>

<script runat="server">

</script>
<script language="javascript">

function DisplayWarning(obj)
{
    if ("<%=Session["userID"] %>" == document.getElementById("txtUserID").value)
        if (obj.value != "-1")
        {
            alert("Default Recherches results will display in lobby upon submit.\r\nPlease log off and log back in for changes � take affect.");
        }
}
function CheckDate(obj)
{
    //alert("in CheckDate" + Date.parse(obj.value) + " : " + Date.parse(new Date()) );
    if (Date.parse(obj.value) < Date.parse(new Date()))
    {
        alert("Invalid Date");
        //obj.focus(); FOGBUGZ CASE 100 - case was misexplained it is not for conferencesetup.
    }   
}

function CheckSecondaryEmail()
{
    var objL = document.getElementById("lstSendBoth");
    var objT = document.getElementById("txtUserEmail2");
    var objemail = document.getElementById("txtUserEmail");//FB 289
    if ( (objL.value == "1") && (objT.value == "") )
        {
            alert("Please enter a valid Email secondaire address");
            objL.value = "0";
            return false;
        }
/* FB 289 Starts */        
    if((objT.value != "") && (objemail.value != ""))
    {
        if(objT.value.toUpperCase() == objemail.value.toUpperCase())
        {
            alert("Email secondaire should not be same as Utilisateur email.");
            objT.focus();
            return false;
        } 
    }
 /* FB 289 Ends */
}
//FB 2339 Start

function validatePassword (pw, options) 
{
	// default options (allows any password)
	var o = {
		lower:    0,
		upper:    0,
		alpha:    0, /* lower + upper */
		numeric:  0,
		special:  0,
		length:   [0, Infinity],
		custom:   [ /* regexes and/or functions */ ],
		badWords: [],
		badSequenceLength: 0,
		noQwertySequences: false,
		noSequential:      false
	};

	for (var property in options)
		o[property] = options[property];

	var	re = {
			lower:   /[a-z]/g,
			upper:   /[A-Z]/g,
			alpha:   /[A-Z]/gi,
			numeric: /[0-9]/g,
			special: /[\W_]/g
		},
		rule, i;

	// enforce min/max length
	if (pw.length < o.length[0] || pw.length > o.length[1])
		return false;

	// enforce lower/upper/alpha/numeric/special rules
	for (rule in re) {
		if ((pw.match(re[rule]) || []).length < o[rule])
			return false;
	}

	// enforce word ban (case insensitive)
	for (i = 0; i < o.badWords.length; i++) {
		if (pw.toLowerCase().indexOf(o.badWords[i].toLowerCase()) > -1)
			return false;
	}

	// enforce the no sequential, identical characters rule
	if (o.noSequential && /([\S\s])\1/.test(pw))
		return false;

	// enforce alphanumeric/qwerty sequence ban rules
	if (o.badSequenceLength) {
		var	lower   = "abcdefghijklmnopqrstuvwxyz",
			upper   = lower.toUpperCase(),
			numbers = "0123456789",
			qwerty  = "qwertyuiopasdfghjklzxcvbnm",
			start   = o.badSequenceLength - 1,
			seq     = "_" + pw.slice(0, start);
		for (i = start; i < pw.length; i++) {
			seq = seq.slice(1) + pw.charAt(i);
			if (
				lower.indexOf(seq)   > -1 ||
				upper.indexOf(seq)   > -1 ||
				numbers.indexOf(seq) > -1 ||
				(o.noQwertySequences && qwerty.indexOf(seq) > -1)
			) {
				return false;
			}
		}
	}

	// enforce custom regex/function rules
	for (i = 0; i < o.custom.length; i++) {
		rule = o.custom[i];
		if (rule instanceof RegExp) {
			if (!rule.test(pw))
				return false;
		} else if (rule instanceof Function) {
			if (!rule(pw))
				return false;
		}
	}
	return true;
}

function PreservePassword()
{
        document.getElementById("txtPassword1_1").value = document.getElementById("txtPassword1").value;
        document.getElementById("txtPassword1_2").value = document.getElementById("txtPassword2").value;
        var password = document.getElementById("txtPassword1").value;
        if("<%=Session["EnablePasswordRule"]%>" == "1")
        {
            var passed = validatePassword(password, {
	                length:   [6, Infinity],
	                lower:    1,
	                upper:    1,
	                numeric:  1,
	                special:  0,
	                badWords: [],
	                badSequenceLength: 0
                    });
            if(passed == false)
            {
                document.getElementById("cmpValPassword1").style.visibility = 'visible';
            }
            else
            {
                document.getElementById("cmpValPassword1").style.visibility = 'hidden';
            }
        }
}
function RulePassword()
{
        var password = document.getElementById("txtPassword1").value;
        if("<%=Session["EnablePasswordRule"]%>" == "1")
        {
            var passed = validatePassword(password, {
	                length:   [6, Infinity],
	                lower:    1,
	                upper:    1,
	                numeric:  1,
	                special:  0,
	                badWords: [],
	                badSequenceLength: 0
                    });
            if(passed == false)
            {
               document.getElementById("cmpValPassword1").innerHTML = "Weak Password";  
                document.getElementById("txtPassword1").focus();
                document.getElementById("cmpValPassword1").style.display = "block";
                return false;          
            }
        }
        IsNumeric();
}
//FB 2339 End

function SavePassword()
{
        document.getElementById("txtPassword1").value = document.getElementById("txtPassword1_1").value;
        document.getElementById("txtPassword2").value = document.getElementById("txtPassword1_2").value;
}

function GetLocations()
{
    url = "LocationList.aspx?roomID=" + document.getElementById("hdnLocation").value;
	if (!window.winrtc) {	// has not yet been defined
		winrtc = window.open(url, "", "width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
		winrtc.focus();
	} else // has been defined
	    if (!winrtc.closed) {     // still open
	    	winrtc.close();
	        winrtc = window.open(url, "", "width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
			winrtc.focus();
		} else {
	        winrtc = window.open(url, "", "width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
	        winrtc.focus();
		}

}
//Code added for Ticker -Start

function fnShowFeed()
 {
    var tickerDisplay = document.getElementById("drpTickerDisplay");
    var tickerDisplay1 = document.getElementById("drpTickerDisplay1");
    
   
    if(tickerDisplay1.selectedIndex == "1")
    {
         document.getElementById("feedLink1").style.display = "Block";
         document.getElementById("txtFeedLink1").style.display = "Block";
    }
    else
    {
       document.getElementById("feedLink1").style.display = "None";
       document.getElementById("txtFeedLink1").style.display = "None";
    }
    
    if(tickerDisplay.selectedIndex == "1")
    {
         document.getElementById("feedLink").style.display = "Block";
         document.getElementById("txtFeedLink").style.display = "Block";
    }
    else
    {
       document.getElementById("feedLink").style.display = "None";
       document.getElementById("txtFeedLink").style.display = "None";
    }
 }
//Code changed for Ticker End
//API Port Starts...
function IsNumeric()

{
   
   var ValidChars = "0123456789";
   var IsNumber=true;
   var Char;
   var error = document.getElementById('lblapierror');
   var sTexttemp = document.getElementById("txtApiportno").value;
   for (i = 0; i < sTexttemp.length && IsNumber == true; i++) 
      { 
      Char = sTexttemp.charAt(i); 
      if (ValidChars.indexOf(Char) == -1) 
         {
         IsNumber = false;
         }
      }
   if(IsNumber == false)
    {
        error.style.display = 'block';
        error.innerHTML = "Les valeurs num�riques�seulement.";
        
    }
   if(IsNumber == true)
    {
        error.style.display = 'none';
        error.innerHTML = "";
        
    } 
   return IsNumber;
   
   }
//API Port Ends...
//FB 2594 Starts
    function isNumberKey(evt)
      {
         var charCode = (evt.which) ? evt.which : event.keyCode
         if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

         return true;
      }
      //FB 2594 Ends
</script>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf8" />
    <title>Utilisateur Profile</title>
    <script type="text/javascript" src="inc/functions.js"></script>

</head>
<body>
    <form id="frmUserProfile" runat="server" method="post" onsubmit="return true">
    <div id="hideScreen" style="position:absolute; background-color:white; top:0; left:0; width:1500px; height:2000px; z-index:1000; display:block"></div><%--FB 2491--%>
            <asp:ScriptManager ID="PrivateVMRScriptManager" runat="server" AsyncPostBackTimeout="600">
                 </asp:ScriptManager><!--Fb 2481-->
    <input id="txtPassword1_1" runat="server" type="hidden" />
    <input id="txtPassword1_2" runat="server" type="hidden" />
    <input name="selectedloc" type="hidden" id="selectedloc" runat="server"  /> <!--Added salle de conf�rence search-->
     <input name="locstrname" type="hidden" id="locstrname" runat="server"  /> <!--Added salle de conf�rence search-->
    <input id="txtLevel" runat="server" type="hidden" />
    <%--Code changed for FB 1425 QA Bug -Start--%>
      <input type="hidden" id="hdntzone" runat="server"/>
      <%--Code changed for FB 1425 QA Bug -End--%>
      <input type="hidden" name="hdnAVParamState" id="hdnAVParamState" runat="server" /> <%--FB 1985--%>
      <input type="hidden" name="hdnHelpReq" id="hdnHelpReq" runat="server" /> <%--FB 2268 --%>     
    <div>
      <input type="hidden" id="helpPage" value="65" />
      <asp:TextBox ID="txtUserID" style="width:0" Height="0" runat="server" BorderStyle="none" BorderWidth="0"></asp:TextBox>

        <table style="width:100%;" align="center" border="0" ><%--FB 2611--%>
            <tr>
                <td align="center">
                    <h3>
                        <asp:Label ID="lblHeader" runat="server"></asp:Label>
                    </h3>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError"></asp:Label>
                    <asp:CustomValidator runat="server" Display="dynamic" ID="cusVal1" OnServerValidate="ValidateIPAddress" CssClass="lblError"></asp:CustomValidator>
                </td>
            </tr>
            <tr>
                <td align="Left">
                    <table cellspacing="5" style="width:100%;"> <%--FB 2611--%>
                        <tr>
                            <td style="width:20">&nbsp;</td>
                            <td>
                                <span class="subtitleblueblodtext">S�lectionner vos options personnelles</span>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table style="width:100%" cellspacing="3" cellpadding="2" border="0"> <%--FB 2611--%>
                        <tr>
                            <td style="width:20%" align="right" class="blackblodtext">Pr�nom<span class="reqfldstarText">*</span></td><%-- FB 1773 --%>
                            <td style="width:30%" align="left">
                            <table border="0" cellspacing="0" cellpadding="1" align="left"><tr><td valign="bottom" align="left"> <%--Edited for FF--%>
                                <asp:TextBox ID="txtUserFirstName" Enabled='<%# Application["ssoMode"].ToString().ToUpper().Equals("No") %>' CssClass="altText" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reqFirstName" runat="server" ControlToValidate="txtUserFirstName" Display="dynamic" ErrorMessage="Requis"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator20" ControlToValidate="txtUserFirstName" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > + % \ / ? | = ! ` [ ] { } # $ @ et ~ sont des characteres invalides." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+?|!`\[\]{}\=@#$%&~]*$"></asp:RegularExpressionValidator> <%--FB 1888--%>
                                </td></tr>
                             </table><%-- FB 1773 --%>
                                <%--<tr><td valign="top" align="left">
                                <asp:TextBox ID="txtUserLastName" CssClass="altText" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reqLastName" runat="server" ControlToValidate="txtUserLastName" Display="dynamic" ErrorMessage="Requis"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txtUserLastName" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ; ? | = ! ` , [ ] { } : # $ @ ~ and &#34; are invalid characters." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=:@#$%&'~]*$"></asp:RegularExpressionValidator>
                                </td></tr>--%><%--Edited for FF--%>
                                </td>
                            <td style="width:20%" align="right" class="blackblodtext">Dernier Nom<span class="reqfldstarText">*</span></td>
                            <td valign="top" align="left">
                                <asp:TextBox ID="txtUserLastName" CssClass="altText" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reqLastName" runat="server" ControlToValidate="txtUserLastName" Display="dynamic" ErrorMessage="Requis"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator7" ControlToValidate="txtUserLastName" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > + % \ / ? | = ! ` [ ] { } # $ @ et ~ sont des characteres invalides." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+?|!`\[\]{}\=@#$%&~]*$"></asp:RegularExpressionValidator><%--FB 1888--%>
                             </td>
                            <%--<td style="width:30%" align="left">
                                <asp:TextBox CssClass="altText" ID="txtUserLogin" runat="server"></asp:TextBox>
                                
                            </td>--%><%-- FB 1773 --%>
                         </tr>   

                        <tr>
                            <td style="width:20%" align="right" class="blackblodtext">Mot de Passe<span class="reqfldstarText">*</span></td>
                            <td style="width:30%" align="left">
                                <asp:TextBox ID="txtPassword1" onchange="javascript:PreservePassword()" TextMode="Password" CssClass="altText" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reqPassword1" ControlToValidate="txtPassword1" Display="dynamic" ErrorMessage="Requis" runat="server"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="regPassword1" ControlToValidate="txtPassword1" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br> & < et > sont des characteres invalides." ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator>
                                <asp:CompareValidator ID="cmpValPassword1" runat="server" ControlToCompare="txtPassword2"
                                    ControlToValidate="txtPassword1" Display="Dynamic" ErrorMessage="<br>Re-enter password."></asp:CompareValidator>
                            </td>
                            <td style="width:20%" align="right" class="blackblodtext">Re-taper votre mot de passe<span class="reqfldstarText">*</span></td>
                            <td style="width:30%" align="left">
                                <asp:TextBox CssClass="altText" ID="txtPassword2" onchange="javascript:PreservePassword()" TextMode="Password" runat="server"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="regPassword2" ControlToValidate="txtPassword2" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br> & < et > sont des characteres invalides." ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator>
                                <asp:CompareValidator ID="cmpValPassword2" runat="server" ControlToCompare="txtPassword1"
                                    ControlToValidate="txtPassword2" Display="Dynamic" ErrorMessage="<br>Passwords do not match."></asp:CompareValidator>
                            </td>
                        </tr>
                        <%--FB 2339 - Start--%>
                        <tr>
                        <td id=""  colspan = "2">
                           <span id="spanpassword" runat="server">Info :password should have atleast one upper case,one lower case,<br />one numeric and minimum length of 6 characters</span>
                        </td>
                        </tr>
                        <%--FB 2339 - End--%>
                        <tr>
                            <td style="width:20%" align="right" class="blackblodtext">Email de l'utilisateur<span class="reqfldstarText">*</span></td>
                            <td align="left">
                                <asp:TextBox ID="txtUserEmail" runat="server" CssClass="altText"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reqUserEmail" ControlToValidate="txtUserEmail" Display="dynamic" ErrorMessage="Requis" runat="server"></asp:RequiredFieldValidator>
                                <%--Code Modification For NGC Enhancement - Start--%>
                                <asp:RegularExpressionValidator ID="regEmail1_1" ControlToValidate="txtUserEmail" Display="dynamic" runat="server" 
                                    ErrorMessage="<br>Invalid email address." ValidationExpression="[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"></asp:RegularExpressionValidator>
                                <asp:RegularExpressionValidator ID="regEmail1_2" ControlToValidate="txtUserEmail" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ /(); ? | ^= ! ` , [ ] { } : # $ ~ et &#34; sont des characteres invalides." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=:#$%&'~]*$"></asp:RegularExpressionValidator>
                            </td>
                            <td style="width:20%" align="right" class="blackblodtext">Email secondaire</td>
                            <td style="width:30%" align="left">
                                <asp:TextBox CssClass="altText" ID="txtUserEmail2" runat="server" onblur="CheckSecondaryEmail()"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="regEmail2_1" ControlToValidate="txtUserEmail2" Display="dynamic" runat="server" 
                                    ErrorMessage="<br>Invalid email address." ValidationExpression="[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"></asp:RegularExpressionValidator>
                                <asp:RegularExpressionValidator ID="regEmail2_2" ControlToValidate="txtUserEmail2" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ /() ; ? | ^= ! ` , [ ] { } : # $ et &#34; sont des characteres invalides." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=:#$%&'~]*$"></asp:RegularExpressionValidator>
                                <%--Code Modification For NGC Enhancement - End--%>
                            </td>
                        </tr>
                        <tr><%-- FB 1773-Starts --%>
                        <td style="width:20%" align="right" class="blackblodtext">
                                <asp:Label ID="Label4" runat="server" Text="Ouverture de session de AD/LDAP"></asp:Label>
                        </td>
                        <td style="width:30%" align="left">
                        <asp:TextBox CssClass="altText" ID="txtUserLogin" runat="server"></asp:TextBox>
                        </td>
                        <td style="width:20%" align="right" class="blackblodtext">Notification par email?</td>
                        <td style="width:30%" align="left">
                                <asp:DropDownList ID="lstEmailNotification" runat="server" CssClass="altText">
                                    <asp:ListItem Selected="True" Value="1" Text="Oui"></asp:ListItem>
                                    <asp:ListItem Value="0" Text="Non"></asp:ListItem>
                                </asp:DropDownList>
                        </td>
                        </tr><%-- FB 1773-End --%>
                        <tr><%-- Phone - New Concepetion START--%>
                            <td style="width:20%" align="right" class="blackblodtext">
                                <asp:Label ID="Label2" runat="server" Text="Travail"></asp:Label>
                            </td>
                            <td style="width:30%" align="left" >
                                <asp:TextBox CssClass="altText" ID="txtWorkPhone" runat="server"></asp:TextBox>                               
                            </td>
                            <td style="width:20%" align="right" class="blackblodtext">
                                <asp:Label ID="Label3" runat="server" Text="T�l�phone Cellulaire"></asp:Label>
                            </td>
                            <td style="width:30%" align="left" >
                                <asp:TextBox CssClass="altText" ID="txtCellPhone" runat="server"></asp:TextBox>                               
                            </td>
                        </tr><%-- Phone - New Concepetion End--%>
                        <tr>
                            <%--Code changed for FB 1425 QA Bug -Start--%>
                            <td style="width:20%" align="right" class="blackblodtext" id ="TzTD1" runat="server">User-Preferred Fuseau Horaire<span class="reqfldstarText">*</span></td>
                            <td style="width:30%" align="left"  id ="TzTD2" runat="server">
                                <asp:DropDownList ID="lstTimeZone" runat="server" CssClass="altSelectFormat" DataTextField="timezoneName" DataValueField="timezoneID">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="reqTZ" ControlToValidate="lstTimeZone" ErrorMessage="Requis" InitialValue="-1" Display="dynamic" runat="server"></asp:RequiredFieldValidator>
                            </td>
                            <%--Code changed for FB 1425 QA Bug -End--%>
                            <td style="width:20%" align="right" class="blackblodtext">Carnet d'adresses pr�f�rees</td>
                            <td style="width:30%" align="left">
                                <asp:DropDownList ID="lstAddressBook" runat="server" CssClass="altSelectFormat" >
                                    <%--Code Modified. Removed selected text from existing on 21MAr09 - FB 412 - Start --%>
                                    <asp:ListItem  Value="-1" Text="Rien"></asp:ListItem> <%-- FB Case 526: Saima--%>
                                    <%--Code Modified. Removed selected text from existing on 21MAr09 - FB 412 - End    --%> 
                                    <%--<asp:ListItem Selected="True" Value="-1" Text="Rien"></asp:ListItem>--%> <%-- FB Case 526: Saima--%>
                                    <%--<asp:ListItem Value="1" Text="MS Outlook 2000/2002/XP"></asp:ListItem>--%>
<%--                                    <asp:ListItem Value="2" Text="Lotus Notes 6.x"></asp:ListItem>
--%>                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" Enabled="false" ControlToValidate="lstAddressBook" ErrorMessage="Requis" InitialValue="-1" Display="dynamic" runat="server"></asp:RequiredFieldValidator> <%-- FB Case 526: Saima--%>
                            </td>
                        </tr>
                        <tr>
                            <td style="width:20%" align="right" class="blackblodtext">Groupes de d�fauts</td>
                            <td style="width:30%" align="left">
                                <asp:DropDownList ID="lstDefaultGroup" CssClass="altSelectFormat" DataValueField="groupID" DataTextField="groupName" runat="server"></asp:DropDownList>
                            </td>
                            <td style="width:20%" align="right" class="blackblodtext">Role de l'utilisateur<span class="reqfldstarText">*</span></td>
                            <td style="width:30%" align="left">
                                <asp:DropDownList ID="lstUserRole" CssClass="altSelectFormat" DataValueField="ID" DataTextField="name" runat="server" OnSelectedIndexChanged="UpdateDepartments" AutoPostBack="true">
                                </asp:DropDownList>
                                <asp:DropDownList ID="lstAdmin" Visible="false" DataValueField="ID" DataTextField="level" runat="server"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="reqRole" ControlToValidate="lstUserRole" ErrorMessage="Requis" InitialValue="-1" Display="dynamic" runat="server"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td style="width:20%" align="right" class="blackblodtext">L'expiration de compte<span class="reqfldstarText">*</span></td>
                            <td style="width:30%" align="left">
                                <asp:TextBox ID="txtAccountExpiry" onblur="javascript:CheckDate(this)" onchange="javascript:CheckDate(this)" CssClass="altText" runat="server" ></asp:TextBox>
                                <%-- Code added by Offshore for FB Issue 1073 -- Start
                                 Code Modified For FB 1425- For Javascript date error.
                                 <img src="image/calendar.gif" border="0" style="width:20" height="20" id="Img1" style="cursor: pointer;" title="Date selector" onclick="return showCalendar('<%=txtAccountExpiry.ClientID %>', 'cal_triggerd', 1, '%m/%d/%Y');" />--%>
                                <img src="image/calendar.gif" border="0" width="20" height="20" id="cal_triggerd" style='cursor: pointer;<%=((txtAccountExpiry.Enabled)? "" : "display:none;")%>' title="Date selector" onclick="return showCalendar('<%=txtAccountExpiry.ClientID %>', 'cal_triggerd', 1, '<%=format%>');" />                                
                                 <%-- Code added by Offshore for FB Issue 1073 -- End --%>
                                <asp:RequiredFieldValidator ID="reqAccExp" ControlToValidate="txtAccountExpiry" runat="server" ErrorMessage="Requis" Display="Dynamic"></asp:RequiredFieldValidator>
<%--                                <asp:RangeValidator ID="rangeExpiry" ControlToValidate="txtAccountExpiry" runat="server" ErrorMessage="Invalid Expiry Date" Display="dynamic"></asp:RangeValidator>
--%>                            </td>
                            <td style="width:20%" align="right" class="blackblodtext">Minutes programm�es restantes<span class="reqfldstarText">*</span></td>
                            <td style="width:30%" align="left">
                                <asp:TextBox ID="txtTimeRemaining" runat="server"></asp:TextBox><%-- FB 1773 --%>
                                <asp:RequiredFieldValidator ID="reqMinutes" SetFocusOnError="true" runat="server" ErrorMessage="Requis" CssClass="lblError" ControlToValidate="txtTimeRemaining" ></asp:RequiredFieldValidator>
                                <asp:RangeValidator ID="ValidatorTimeremaining" SetFocusOnError="true" runat="server" CssClass="lblError" Display="dynamic" ControlToValidate="txtTimeRemaining"
                                    ErrorMessage="<br>Please enter a value between 0 and 2000000000." MaximumValue="2000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>                            </td>
                        </tr>
                        <%--<tr>
                            <td style="width:20%" align="right" class="blackblodtext">Email Notification?</td>
                            <td style="width:30%" align="left">
                                <asp:DropDownList ID="lstEmailNotification" runat="server" CssClass="altText">
                                    <asp:ListItem Selected="True" Value="1" Text="Oui"></asp:ListItem>
                                    <asp:ListItem Value="0" Text="Non"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td style="width:20%" align="right" class="blackblodtext">Recherche personalis�e</td>
                            <td style="width:30%" align="left">
                                <asp:DropDownList ID="lstSearchTemplate" CssClass="altSelectFormat" DataTextField="Name" DataValueField="ID" runat="server" onchange="javascript:DisplayWarning(this);"></asp:DropDownList>
                            </td>
                        </tr>--%><%-- FB 1773 --%>
                        <tr>
                            <td style="width:20%" align="right" class="blackblodtext" valign="top">Endroit Pr�f�re</td>
                            <td style="width:30%" align="left" valign="top" runat="server" id="TDprefLocation">
                                <asp:TextBox ID="txtLocation" runat="server" CssClass="altText" style="display:none;" Enabled="false" ></asp:TextBox>
                                <select size="4" name="RoomList" runat="server" id="RoomList" class="treeSelectedNode" Rows="3"></select>
                                 <input name="addRooms" type="button" id="addRooms" onclick="javascript:AddRooms();" style="display:none;" />
                                <a href="javascript: OpenRoomSearch('frmUserProfile');" onmouseover="window.status='';return true;" runat="server" id="roomclick"><img border="0" runat="server" id="roomimage" src="image/edit.gif" alt="edit" style="width:17" HEIGHT="15"></a>                                
                                <input type="hidden" id="hdnLocation" runat="server" />
                            </td>
                            <td style="width:20%" align="right" class="blackblodtext" valign="top">Utilisateur's D�partements</td><%-- FB 1773 --%>
                            <td style="width:30%" align="left" valign="top">
                                <asp:ListBox runat="server" ID="lstDepartment" CssClass="altSelectFormat" DataTextField="name" DataValueField="id" Rows="6" SelectionMode="Multiple"></asp:ListBox><%-- Added for FB 1507 --%>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="blackblodtext" valign="top" style="width:20%">
                                Envoyer les emails aux adresses primaires Secondary Addresses?</td>
                            <td align="left" valign="top" style="width:30%">
                                <asp:DropDownList ID="lstSendBoth" CssClass="altText" runat="server" onchange="javascript:CheckSecondaryEmail()">  <%-- --%>
                                    <asp:ListItem Text="Non" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="Oui" Value="1"></asp:ListItem>
                                </asp:DropDownList>
                            </td><%-- FB 1773 --%>
                            <td style="width:20%" align="right" class="blackblodtext">Recherche personalis�e</td>
                            <td style="width:30%" align="left">
                                <asp:DropDownList ID="lstSearchTemplate" CssClass="altSelectFormat" DataTextField="Name" DataValueField="ID" runat="server" onchange="javascript:DisplayWarning(this);"></asp:DropDownList>
                            </td>
                        </tr>
                        <%--Code added For FB Issue 1073 Start --%> 
                        <tr>
                            <td style="width:20%" align="right" class="blackblodtext">Format de la date</td>
                            <td style="width:30%" align="left" nowrap="nowrap"> <%--FB 2611--%>
                                <asp:DropDownList ID="DrpDateFormat" runat="server" CssClass="altSelectFormat" >
                                    <asp:ListItem Selected="True" Value="MM/dd/yyyy" Text="US (MM/dd/yyyy)"></asp:ListItem> 
                                    <asp:ListItem Value="dd/MM/yyyy" Text="UK (dd/MM/yyyy)"></asp:ListItem>                                  
                              </asp:DropDownList>/ 
                               <asp:DropDownList ID="lstTimeFormat" CssClass="altText" runat="server">
                                    <asp:ListItem Selected="True" Value="1" Text="12 hour (01:00 PM)"></asp:ListItem>
                                    <asp:ListItem Value="0" Text="24 hour (13:00)"></asp:ListItem>
                                </asp:DropDownList>                                
                            </td>
                            <%--Code changed for FB 1425 QA Bug -Start--%>
                            <td style="width:20%" align="right" class="blackblodtext" id ="TzTD3" runat="server">Montrer Fuseau Horaire<br /> sur tous les �crans</td>
                            <td style="width:50%" align="left" id ="TzTD4" runat="server">
                            <%-- organisation Css Module --%>   
                                <asp:DropDownList ID="lstTimeZoneDisplay" runat="server" CssClass="altSelectFormat">
                                    <asp:ListItem Selected="True" Value="1" Text="Oui"></asp:ListItem>
                                    <asp:ListItem Value="0" Text="Non"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <%--Code changed for FB 1425 QA Bug -Start--%>
                        </tr>
                    <%--Code added For FB Issue 1073 End --%>
                        <%--Code changed for MOJ Phase 2 QA Bug -Start--%>
                        <tr id="EnableTR" runat="server">
                       <%--Code changed for MOJ Phase 2 QA Bug -End--%>
                            <td style="width:20%" align="right" class="blackblodtext">
                                <asp:Label ID="ParLBL" runat="server" Text="Permettre Invit�s"></asp:Label>
                            </td>
                            <td style="width:30%" align="left" >
                                <asp:DropDownList ID="DrpEnvPar" runat="server" CssClass="altSelectFormat" >
                                    <asp:ListItem Selected="True" Value="1" Text="Oui"></asp:ListItem> 
                                    <asp:ListItem Value="0" Text="Non"></asp:ListItem>                                  
                              </asp:DropDownList>                                
                            </td>
                            <td style="width:20%" align="right" class="blackblodtext">
                                <asp:Label ID="AVLBL" runat="server" Text="Permettre arrangements A/V"></asp:Label>
                            </td>
                            <td style="width:30%" align="left" >
                                <asp:DropDownList ID="DrpEnableAV" runat="server" CssClass="altSelectFormat" >
                                    <asp:ListItem Selected="True" Value="1" Text="Oui"></asp:ListItem> 
                                    <asp:ListItem Value="0" Text="Non"></asp:ListItem>                                  
                              </asp:DropDownList>                                
                            </td>
                        </tr>
                        <%--Code Added for Licence modification START--%>
                        <tr id="trUser" runat="server">
                            <td style="width:20%" align="right" class="blackblodtext">
                                <asp:Label ID="LblExc" runat="server" Text="Permettre access � MS-Exchange"></asp:Label>
                            </td>
                            <td style="width:30%" align="left" >
                                <asp:DropDownList ID="DrpExc" runat="server" CssClass="altSelectFormat" >
                                    <asp:ListItem Value="0" Selected="True" Text="Non"></asp:ListItem> 
                                    <asp:ListItem  Value="1" Text="Oui"></asp:ListItem>                                  
                              </asp:DropDownList>                                
                            </td>
                            <td style="width:20%" align="right" class="blackblodtext">
                                <asp:Label ID="LblDom" runat="server" Text="Permettre acc�s � IBM Domino"></asp:Label>
                            </td>
                            <td style="width:30%" align="left" >
                                <asp:DropDownList ID="DrpDom" runat="server" CssClass="altSelectFormat" >
                                    <asp:ListItem Value="0" Selected="True" Text="Non"></asp:ListItem> 
                                    <asp:ListItem  Value="1" Text="Oui"></asp:ListItem> 
                                                                     
                              </asp:DropDownList>                                
                            </td>
                        </tr>
                        <tr id="tr3" runat="server"><%--FB 2023 Start--%>
                            <td style="width:20%" align="right" class="blackblodtext">
                                    <asp:Label ID="LblMob" runat="server" Text="Activer Mobile"></asp:Label>
                                </td>
                                <td style="width:30%" align="left" >
                                <asp:DropDownList ID="DrpMob" runat="server" CssClass="altSelectFormat" >
                                    <asp:ListItem Value="0" Selected="True" Text="Non"></asp:ListItem> 
                                    <asp:ListItem  Value="1" Text="Oui></asp:ListItem>                                  
                                </asp:DropDownList>                                
                                </td>
                            <%--FB 1830 - Start--%>                               
                            <td style="width:20%" align="right" class="blackblodtext">
                            <asp:Label ID="PrefLang" runat="server" Text="Langue pr�f�r�e"></asp:Label>
                            </td>
                            <td style="width:30%" align="left" >
                                <asp:DropDownList ID="lstLanguage" runat="server" CssClass="altSelectFormat" DataTextField="name" DataValueField="ID"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="reglstLanguage" ControlToValidate="lstLanguage" ErrorMessage="Requis" InitialValue="-1" Display="dynamic" runat="server"></asp:RequiredFieldValidator>
                            </td>
                            </tr>
                            <%--FB 1979 - Start--%>
                            <tr>
                                <td style="width:19%" align="right" valign="middle" class="blackblodtext">
                                <asp:Label ID="emaillang" runat="server" Text="Langue des Emails"></asp:Label>
                                </td>
                                <td align="left" style="width:30%" > <%--FB 1830 - DeleteEmailLang start--%>  
                                     <%-- FB 2029 starts --%>
                                    <asp:Button ID="btnDefine" runat="server" Text="Personalisation" OnClick="DefineEmailLanguage" class="altLongBlueButtonFormat" /> 
                                    <asp:TextBox ID="txtEmailLang" runat="server" ReadOnly="true" CssClass="altText" Visible="false"></asp:TextBox>  <%--FB 2104 --%> <%-- FB 2029 end --%>                           
                                    <asp:ImageButton ImageUrl="image/btn_delete.gif" runat="server" ID="delEmailLang" ToolTip="Delete Langue des Emails" onclick="DeleteEmailLangugage" OnClientClick="javascript:return fnDelEmailLan()" />
                                </td>
                                <td style="width:20%" align="right" class="blackblodtext">
                                    <asp:Label ID="Label5" runat="server" Text="Notifications PIM"></asp:Label>
                                </td>
                                <td style="width:30%" align="left" >
                                <asp:DropDownList ID="DrpPIMNotifications" runat="server" CssClass="altSelectFormat" >
                                    <asp:ListItem Value="0" Selected="True" Text="Non"></asp:ListItem> 
                                    <asp:ListItem  Value="1" Text="Oui"></asp:ListItem>                                  
                                </asp:DropDownList>                                
                                </td>
                            </tr>
                           <%--New Lobby Page--%>
                            <%--FB 1979 - End--%>
                            <tr id="tr4" runat="server">
                            <td style="width:19%" align="right" valign="top" class="blackblodtext">
                                <asp:Label ID="Loginmgmt" runat="server" Text="la gestion Lobby"></asp:Label>
                            </td>
                            <td style="width:10%" align="left" valign="bottom" >
                                <asp:Button id="btnlogin" runat="server" Text="Personalisation" class="altLongBlueButtonFormat" onclick="lobbyManage" />
                            </td>  
                            <td  align="right" valign="top" class="blackblodtext"> <%--FB 1860--%>
                                <asp:Label ID="LblBlockEmails" runat="server" Text="Bloquage des Emails"></asp:Label>
                            </td>
                             <%-- FB 2029 starts --%>
                            <td align="left"  colspan = "4"  > <%--FB 1830 - DeleteEmailLang end--%>
                            <table><tr><td>  
                                <asp:CheckBox ID="ChkBlockEmails" runat="server" />
                                </td><td>
                                <asp:Button ID="BtnBlockEmails" style="display:none;" runat="server" Text="Editer" OnClick="EditBlockEmails"  class="altShortBlueButtonFormat" />
                            </td></tr></table> </td><%-- FB 2029 end --%> 
                           </tr>
                           <tr>
                           <td style="width:19%" align="right" valign="top" class="blackblodtext">
                                <asp:Label ID="lblHelpReqPhone" runat="server" Text="Aide t�l�phonique demandeur"></asp:Label>
                            </td>
                            <td style="width:10%" align="left" valign="top" >
                                <asp:TextBox ID="txtHelpReqPhone" runat="server" CssClass="altText"></asp:TextBox>                                 
                            </td>
                            <td  align="right" valign="top" class="blackblodtext">
                              <asp:Label ID="lblHelpReqEmail" runat="server" Text="Aide email demandeur"></asp:Label>
                             </td>
                             <td align="left" style="width:30%" valign="top">
                                <asp:TextBox ID="txtHelpReqEmail" runat="server" CssClass="altText"></asp:TextBox> 
                                <asp:Button id="btnAddHelpReq" runat="server" Text="ajouter" class="altShortBlueButtonFormat" Width="20%" OnClientClick="javascript:return AddRemoveHelpReq('add')" />
                                 <asp:RegularExpressionValidator ID="RegtxtHelpReqEmail" ControlToValidate="txtHelpReqEmail" Display="dynamic" runat="server" 
                                    ErrorMessage="<br>Invalid email address." ValidationExpression="[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"></asp:RegularExpressionValidator>
                                 <asp:RegularExpressionValidator ID="RegtxtHelpReqEmail_1" ControlToValidate="txtHelpReqEmail" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ /(); ? | ^= ! ` , [ ] { } : # $ ~ et &#34; sont des characteres invalides." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=:#$%&'~]*$"></asp:RegularExpressionValidator>
                                <br \ /><br \ />
                                <asp:ListBox runat="server" ID="lstHelpReqEmail" CssClass="altSelectFormat" Rows="5" SelectionMode="Multiple"  onDblClick="javascript:return AddRemoveHelpReq('Rem')" ></asp:ListBox>
                             </td>
                           </tr>
                           <%--FB 2348 Start--%>
                           <tr id="trSurveyEmail" style="visibility:hidden">
                             <td style="width:20%" align="right" class="blackblodtext">
                                <asp:Label ID="LblSurveyEmail" runat="server" Text="Envoyer un email enqu�te"></asp:Label>
                            </td>
                            <td style="width:30%" align="left" >
                                <asp:DropDownList Width="20%" ID="drpSurveyEmail" runat="server" CssClass="altText" >
                                    <asp:ListItem Value="0" Selected="True" Text="Non"></asp:ListItem> 
                                    <asp:ListItem  Value="1" Text="Oui"></asp:ListItem>                                  
                                </asp:DropDownList>   
                            </td>
                            <td colspan="2"></td>
                           </tr>
                            <%--FB 2348 End--%>    
                            
                            <%--FB 2608 Start--%>                            
                            <tr>
                             <td style="width:20%" align="right" class="blackblodtext">
                                <asp:Label ID="Label6" runat="server" Text="Enable VNOC Selection"></asp:Label>
                            </td>
                            <td style="width:30%" align="left" >
                               <asp:CheckBox ID="chkVNOC" runat="server" />   
                            </td>
                            <td colspan="2"></td>
                           </tr>
                           <%--FB 2608 End--%>       
                                                  
                           <%--Audio Add On--%>
                           <tr id="tr5" style="width:10%; display:none" runat="server">
                            <td style="width:20%" align="right" class="blackblodtext">
                                <asp:Label ID="Label1" runat="server" Text="Permis Audio Add-On"></asp:Label>
                            </td>
                            <td style="width:30%" align="left" >
                                <asp:CheckBox ID="chkAudioAddon" runat="server" />
                            </td>
                            <td colspan="2"></td>
                           </tr>
                            <%--FB 1830 - End--%><%--FB 2023 end--%>
                        <%--Code Added for Licence modification END--%>                        
                         <%--Code changed for RSS Feed--%>
                         <%-- FB 1985--%>
                         <%if ((Application["Client"].ToString().ToUpper().Equals("DISNEY")))
                           {%>
                         <tr>
                            <td id="tblAVExpand" runat="server" colspan="4"> <%--Disney New Requirement--%>
                                <table border="0" align="left" width="80px">
                                    <tr> 
                                        <td id="Td1"  align="left"  onmouseover="javascript:return fnShowHideAVLink('1');" onmouseout="javascript:return fnShowHideAVLink('0');" runat="server">&nbsp;
                                            <asp:LinkButton ID="LnkAVExpand" style="display:none" runat="server" Text="Expand" OnClientClick="javascript:return fnShowAVParams()"></asp:LinkButton>
                                        </td>
                                     </tr>
                                </table>
                             </td>
                            </tr>    <%} %> 
                        <tr style="display:none;">
                        <td style="width:20%" align="right" class="blackblodtext">RSS Feed</td>
                        <td style="width:30%" align="left" >
                        <asp:ImageButton ID="ImageButton1"  src="image/rss.gif"  runat="server"  OnClick="RSSFeed" ToolTip="RSS Feed" />
                        </td>
                        </tr>
                    </table>
                </td>
            </tr>   
             <%--FB 2392-Whygo Start--%>
            <tr>
                <td>
                    <table cellpadding="2" border="0" cellspacing="3" id="whygouserSettings" runat="server" width="100%">      
            <tr id="trWhygUser" runat="server" >
                <td colspan="4">
                    <table cellspacing="5">
                        <tr>     
                        <td style="width:20">&nbsp;</td>                        
                            <td>
                                <span class="subtitleblueblodtext">WhyGo User Settings</span>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr runat="server" id="trwhygoRegion"> <%--FB 2594--%>
             <td style="width:20%" align="right" valign="top" class="blackblodtext">
                <asp:Label ID="lblParent" runat="server" Text="Parent/Reseller "></asp:Label>
            </td>
            <td style="width:23%" align="left" valign="top" >
                <%--<asp:TextBox ID="txtParentReseller" runat="server" CssClass="altText"></asp:TextBox>--%>
                 <input id="txtParentReseller" onkeypress="return isNumberKey(event)" maxlength="8" runat="server" type="text" />
            </td>
            <td style="width:27%" align="right" valign="top" class="blackblodtext">
                <asp:Label ID="lblRegion" runat="server" Text="Region"></asp:Label>
             </td>
             <td align="left" valign="top" >
                <asp:DropDownList ID="lstRegion" runat="server" Width="25%" CssClass="altText"></asp:DropDownList>                                 
            </td>
           </tr>
            <tr runat="server" id="trCropUser">  <%--FB 2594--%>
             <td align="right" valign="top" class="blackblodtext">
              <asp:Label ID="lblCorp" runat="server" Text="Corp User"></asp:Label>
            </td>
            <td align="left" valign="top">
                <%--<asp:TextBox ID="txtCorpUser" runat="server" CssClass="altText" ></asp:TextBox> --%>
                <input id="txtCorpUser" onkeypress="return isNumberKey(event)" maxlength="8" runat="server" type="text" name="txtCorpUser" />
             </td>
            <td align="right" valign="top" class="blackblodtext">
                <asp:Label ID="lblWhyGoUsrId" runat="server" Text="Whygo UserID "></asp:Label>
            </td>
            <td  align="left" valign="top" >
                <%--<asp:TextBox ID="txtESUserID" runat="server" CssClass="altText"></asp:TextBox>--%>
                 <input id="txtESUserID" onkeypress="return isNumberKey(event)"  maxlength="8"  runat="server" type="text" />
            </td>
           </tr>
                    </table>
                </td>
            </tr>
            <%--FB 2392-Whygo End--%>
            <%--FB 1985 - Start--%>         
            <tr id="tdOtherSettings"  runat="server"><td align="center">
            <table style="width:100%;" border="0" cellspacing="3" cellpadding="2">  <%--FB 2611--%>
            <tr id="trLblAV" runat="server"> <%--<Code Modified For MOJ Phase2--%> 
                <td align="Left">
                    <table cellspacing="5" style="width:100%;"> <%--FB 2611--%>
                        <tr>
                            <td style="width:20">&nbsp;</td>
                            <td>
                                <span class="subtitleblueblodtext">D�terminer le param�tres de Connection Param�tres</span>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="AVParams" runat="server">
                <%--Window Dressing--%>
                <td align="center" class="tableBody" > 
                    <table style="width:100%;" cellpadding="2" cellspacing="3"> <%--FB 2611--%>
                        <tr>
                            <td align="right" class="blackblodtext">Nom de point de connection profil</td>
                            <td align="left">
                                <asp:TextBox ID="txtEndpointID" runat="server" Visible=false></asp:TextBox>
                                <asp:TextBox ID="txtEndpointName" runat="server" maxlength="20" CssClass="altText"></asp:TextBox><%--FB 2523--%>
                                 <asp:RegularExpressionValidator ID="regConfName" ControlToValidate="txtEndpointName" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > + % \ / ( ) ? | ^ = ! ` [ ] { } # $ @ et ~ sont des characteres invalides." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+?|!`\[\]{}\=@#$%&()~]*$"></asp:RegularExpressionValidator> <%--FB 1888--%>
<%--                                <asp:RequiredFieldValidator ID="reqEPName" runat="server" ControlToValidate="txtEndpointName" ErrorMessage="Requis" Display="dynamic"></asp:RequiredFieldValidator>
--%>                            </td>
                            <td align="right" class="blackblodtext">Mot de passe pour le point de connexion</td>
                            <td align="left">
                                <asp:TextBox ID="txtEPPassword1" runat="server" TextMode="password" CssClass="altText"></asp:TextBox>
                                 <asp:RegularExpressionValidator ID="RegularExpressionValidator9" ControlToValidate="txtEPPassword1" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ ~ et &#34; sont des characteres invalides." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=:#$%&()'~]*$"></asp:RegularExpressionValidator><%--FB 2319--%>
                                <asp:CompareValidator ID="CmpEP1" runat="server" ControlToCompare="txtEPPassword2"
                                    ControlToValidate="txtEPPassword1" Display="Dynamic" ErrorMessage="<br>Re-enter password."></asp:CompareValidator>
                            </td>
                            <td align="right" class="blackblodtext">Confirm� votre mot de passe</td>
                            <td align="left"><asp:TextBox ID="txtEPPassword2" runat="server" TextMode="password" CssClass="altText"></asp:TextBox>
                                <asp:CompareValidator ID="CmpEP2" runat="server" ControlToCompare="txtEPPassword1"
                                    ControlToValidate="txtEPPassword2" Display="Dynamic" ErrorMessage="<br>Your passwords do not match."></asp:CompareValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="blackblodtext">Genre d'adresse</td>
                            <td align="left">
                                <asp:DropDownList ID="lstAddressType" CssClass="altSelectFormat" DataTextField="Name" DataValueField="ID" runat="server" OnSelectedIndexChanged="ValidateTypes" AutoPostBack="true"></asp:DropDownList>
                            </td>
                            <td align="right" class="blackblodtext">D�faut adresses de IP/ISDN</td>
                            <td align="left">
                                <asp:TextBox id="txtAddress" runat="server" CssClass="altText" ></asp:TextBox>
                                <%--FB 1972--%>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator4" ControlToValidate="txtAddress" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > + % \ / ( ) ? | ^ = ` [ ] { } $ et ~ sont des characteres invalides." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+?|`\[\]{}\=$%&()~]*$"></asp:RegularExpressionValidator> <%--FB 2267--%>
                                <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator4" ControlToValidate="txtAddress" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ and &#34; are invalid characters." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:RegularExpressionValidator>--%>
                            </td>
                            <td align="right" class="blackblodtext">�quipement d�faut</td>
                            <td align="left">
                                <asp:DropDownList ID="lstVideoEquipment" CssClass="altSelectFormat" DataTextField="VideoEquipmentName" DataValueField="VideoEquipmentID" runat="server"></asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="blackblodtext">Vitesse de base de transmission</td>
                            <td align="left">
                                <asp:DropDownList ID="lstLineRate" CssClass="altSelectFormat" DataTextField="LineRateName" DataValueField="LineRateID" runat="server"></asp:DropDownList>
                            </td>
                            <td align="right" class="blackblodtext">MCU assign�</td>
                            <td align="left">
                                <asp:DropDownList ID="lstBridges" CssClass="altSelectFormat" DataTextField="BridgeName" DataValueField="BridgeID" runat="server"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="reqBridges" Enabled="false" Display="dynamic" ControlToValidate="lstBridges" InitialValue="-1" ErrorMessage="Required for MPI." runat="server" SetFocusOnError="true" ></asp:RequiredFieldValidator>  
                            </td>
                            <td align="right" class="blackblodtext">d�faut Genre de connection</td>
                            <td align="left">
                                <asp:DropDownList ID="lstConnectionType" CssClass="altSelectFormat" runat="server" DataTextField="Name" DataValueField="ID"></asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="blackblodtext">Acc�s d'Internet URL</td>
                            <td align="left">
                                <asp:TextBox ID="txtWebAccessURL" CssClass="altText" runat="server"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" ControlToValidate="txtWebAccessURL" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ ( ) ; ? | ^ = ! ` , [ ] { } # $ @ ~ et &#34; sont des characteres invalides." ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>+?|!`,;\[\]{}\x22;=@#$%&()'~]*$"></asp:RegularExpressionValidator>                                
                            </td>
                            <td align="right" class="blackblodtext">Associ� avec l'adresses MCU</td>
                            <td align="left">
                                <asp:TextBox ID="txtAssociateMCUAddress" CssClass="altText" runat="server"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator10" ControlToValidate="txtAssociateMCUAddress" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ` , [ ] { } : $ @ ~ et &#34; sont des characteres invalides." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>^+;?|`,\[\]{}\x22;=:@$%&()'~]*$"></asp:RegularExpressionValidator> <%--FB 2267--%>
                            </td>
                            <td align="right" class="blackblodtext">MCU Genre d'adresse</td>
                            <td align="left">
                                <asp:DropDownList ID="lstMCUAddressType" CssClass="altSelectFormat" DataTextField="Name" DataValueField="ID" runat="server"></asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="blackblodtext">D�faut protocole</td>
                            <td align="left">
                                <asp:DropDownList CssClass="altSelectFormat" ID="lstProtocol" runat="server" DataTextField="Name" DataValueField="ID"></asp:DropDownList>
                            </td>
                            <td align="right" class="blackblodtext">R�seau ext�rieur?</td>
                            <td align="left">
                                <!--[Vivek 29th Apr 2008] Defaulted Reseau ext�rieur � Non as per Issue No: 299-->
                            <%--Window Dressing--%>
                                <asp:DropDownList ID="lstIsOutsideNetwork" CssClass="altText" runat="server">
                                    <asp:ListItem Value="1" Text="Oui"></asp:ListItem>                                    
                                    <asp:ListItem Selected="True" Value="0" Text="Non"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td align="right" class="blackblodtext">Chiffrement Preferred?</td>
                            <td align="left">
                                <asp:DropDownList ID="lstEncryption" CssClass="altText" runat="server">
                                    <asp:ListItem Selected="True" Value="1" Text="Oui"></asp:ListItem>
                                    <asp:ListItem Value="0" Text="Non"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="blackblodtext">
                                Identit� pour votre email
                            </td>
                            <%-- ICAL Cisco Telepresence fix--%>
                            <td align="left">
                                <asp:TextBox CssClass="altText" ID="txtExchangeID" runat="server" Width="230px" TextMode="SingleLine"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" ControlToValidate="txtExchangeID"
                                    ValidationGroup="Submit" Display="dynamic" runat="server" SetFocusOnError="true"
                                    ErrorMessage="<br>& < > ' + % \ ( ) ; ? | ^ = ! ` , [ ] { } # $ ~ et &#34; sont des characteres invalides."
                                    ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>+;?|!`,\[\]{}\x22;=^#$%&()'~]*$"></asp:RegularExpressionValidator>
                            </td>
                            <%--API Port Starts--%>
                            <td align="right" class="blackblodtext">
                                Prifice API
                            </td>
                            <td align="left" nowrap>
                                <asp:TextBox ID="txtApiportno" CssClass="altText" onblur="javascript:return IsNumeric()" MaxLength="5" runat="server"></asp:TextBox>
                                <label id="lblapierror" style="display:none;font-weight:normal" class="lblError" />
                            </td>
                            <%-- FB 1642 Audio add on- Starts --%>
                            <td align="right" class="blackblodtext">
                                Code de conf�rence
                            </td>
                            <td align="left" >
                                <asp:TextBox CssClass="altText" ID="txtConfCode" runat="server"></asp:TextBox> 
                            </td> 
                            <%-- FB 1642 Audio add on- End --%>                     
                            <%--API Port Ends--%>
                        </tr>
                        <%-- FB 1642 Audio add on- Starts --%> 
                        <tr>
                            <td align="right" class="blackblodtext">
                                Clef leader
                            </td>
                            <td align="left" >
                                <asp:TextBox CssClass="altText" ID="txtLeaderPin" runat="server"></asp:TextBox>                               
                            </td> 
                            <%--FB 2227--%>
                            <td align="right" class="blackblodtext">
                                Num�ro interne pont vid�o
                            </td>
                            <td align="left" >
                                <asp:TextBox CssClass="altText" ID="txtIntvideonum" MaxLength="25" runat="server"></asp:TextBox>                               
                            </td>
                            <td align="right" class="blackblodtext">
                                Externe Num�ro de pont vid�o
                            </td>
                            <td align="left" >
                                <asp:TextBox CssClass="altText" ID="txtExtvideonum" MaxLength="25" runat="server"></asp:TextBox>                               
                            </td>
                            <%--FB 2227--%>
                        </tr>
                        
                         <%--FB 2262 //FB 2599--%>
                        <tr>
                            <%--<td align="right" class="blackblodtext"><%--FB 2481--%>
                                <%--Private VMR--%>
                            <%--</td>--%>
                            <%--<td align="left">
                                <asp:Button ID="btnPrivateVMR1" runat="server" Text="Manage" class="altLongBlueButtonFormat" /> 
                            </td>--%>
                             <td align="right" class="blackblodtext">
                                External VMR
                            </td>
                            <td align="left" >
                                <asp:Button ID="btnPrivateVMR" runat="server" Text="Manage" class="altLongBlueButtonFormat" /> 
                            </td>
                            <td id="tdMeetlink" runat="server" visible="false" align="right" class="blackblodtext">
                                Cloud Meeting Link
                            </td>
                            <td align="left" >
                                <asp:TextBox Visible="false" CssClass="altText" ID="txtVidyoURL" runat="server"></asp:TextBox>                               
                            </td> 
                            <td id="tdExten" Visible="false" runat="server" align="right" class="blackblodtext">
                                Extension
                            </td>
                            <td align="left" >
                                <asp:TextBox Visible="false" CssClass="altText" ID="txtExtension" runat="server" MaxLength="50" ></asp:TextBox>                               
                            </td> 
                            
                        </tr>
                        <tr id="trpin" runat="server" visible="false">
                            <td align="right" class="blackblodtext">
                                Pin
                            </td>
                            <td align="left" >
                                <asp:TextBox CssClass="altText" ID="txtPin" MaxLength="25" runat="server"></asp:TextBox>  
                                <asp:CompareValidator ID="cmpPin" runat="server" ControlToCompare="txtConfirmPin"
                                   ControlToValidate="txtPin" Display="Dynamic" ErrorMessage="<br>Re-enter pin."></asp:CompareValidator>
                            </td>
                            <td align="right" class="blackblodtext">
                                Confirm Pin
                            </td>
                            <td align="left" >
                                <asp:TextBox CssClass="altText" ID="txtConfirmPin" runat="server" MaxLength="25"></asp:TextBox>   
                                <asp:CompareValidator ID="cmpConfirmPin" runat="server" ControlToCompare="txtPin"
                                    ControlToValidate="txtConfirmPin" Display="Dynamic" ErrorMessage="<br>Pin do not match."></asp:CompareValidator>
                            </td>
                            <td colspan="2"></td>
                         </tr>
                        <%--FB 2262 //FB 2599--%>
						 <%--FB 2481 Start--%>
                        <%--<tr>
                            <%--<td align="right" class="blackblodtext">
                                External VMR
                            </td>
                            <td align="left" >
                                <asp:Button ID="btnPrivateVMR" runat="server" Text="Manage" class="altLongBlueButtonFormat" /> 
                            </td>
                            <td colspan="4"></td>                           
                        </tr>--%>
                        
                        <tr>
                        <td colspan="4">
                            <ajax:ModalPopupExtender ID="PrivateVMRPopup" runat="server" TargetControlID="btnPrivateVMR"
                                PopupControlID="PopupPrivateVMRPanel" DropShadow="false" Drag="true" BackgroundCssClass="modalBackground"
                                CancelControlID="ClosePUp" BehaviorID="btnPrivateVMR">
                            </ajax:ModalPopupExtender>
                            <asp:Panel ID="PopupPrivateVMRPanel" runat="server" Width="60%" Height="60%" HorizontalAlign="Center"
                                CssClass="treeSelectedNode" ScrollBars="Vertical">
                                <table align="center" cellpadding="3" cellspacing="0" width="98%" style="border-collapse: collapse;height: 50%;">
                                    <tr>
                                        <td align="center">
                                            <table width="100%" border="0" cellpadding="3" style="border-collapse: collapse;height: 100%;">
                                                <tr>
                                                    <td colspan="6">
                                                        <h3>Priv� de code VMR</h3>
                                                    </td>
                                                </tr>
                                                <tr align="center" style="height: 50%">
                                                <td  align="center" colspan="6" height="21" align="left" valign="top" class="blackblodtext">
                                                    <dxHE:ASPxHtmlEditor ID="PrivateVMR" runat="server" Height="200px">
                                                        <SettingsImageUpload UploadImageFolder="~/image/maillogo/">
                                                            <ValidationSettings MaxFileSize="100000" MaxFileSizeErrorText="L'attachement footer image est sup�rieure � 100KB. Fichier n'a pas �t� t�l�charg�." />
                                                        </SettingsImageUpload>
                                                    </dxHE:ASPxHtmlEditor>
                                                    <input type="file" id="fmMap" contenteditable="false" size="50" class="altText" runat="server"
                                                        visible="false" />
                                                    <input type="hidden" id="fmMapImage" name="Map1ImageDt" runat="server" height="21%"
                                                        style="display: none" /><%--FB 1982 --%>
                                                 </td>
                                                 </tr>
                                                <tr align="center" style="height: 50%">
                                                    <td align="center" colspan="6">
                                                    <br />
                                                    <input align="middle" type="button" runat="server" id="ClosePUp"
                                                        value=" Ferm�e " class="altShortBlueButtonFormat" onserverclick="fnPrivateVMRCancel"/>
                                                    <asp:Button ID="btnPrivateVMRSubmit" runat="server" Text="Soumettre" 
                                                        OnClick="fnPrivateVMRSubmit" class="altShortBlueButtonFormat" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                        </tr>
                        <%--FB 2481 End--%>
                        <%-- FB 1642 Audio add on- End --%> 
                    </table>
                </td>
            </tr>
            <%--Tickers Start--%>
            <tr> 
                <td align="Left">
                    <table cellspacing="5" style="width:100%;"> <%--FB 2611--%>
                        <tr>
                            <td style="width:20">&nbsp;</td>
                            <td>
                                <span class="subtitleblueblodtext">Positionement du Ticker #1</span>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="Tr2" runat="server">
                <td align="center" class="tableBody"> 
                    <table style="width:100%" cellpadding="2" cellspacing="3"><%--FB 2611--%>
                        <tr>
                            <td align="right" class="blackblodtext">Conidtion de visualisation du Ruban</td>
                             <td align="left">
                                <asp:DropDownList ID="drpTickerStatus" CssClass="altText" runat="server">
                                    <asp:ListItem  Value="0" Text="Montrer"></asp:ListItem>                                    
                                    <asp:ListItem  Selected="True" Value="1" Text="Cach�"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td align="right" class="blackblodtext">Position de ruban</td>
                            <td align="left">
                             <asp:DropDownList ID="drpTickerPosition" CssClass="altText" runat="server">
                                    <asp:ListItem Selected="True" Value="0" Text="Haut de Page"></asp:ListItem>                                    
                                    <asp:ListItem  Value="1" Text="Bas de Page"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td align="right" class="blackblodtext">Vitesse de t�l�scripture</td>
                            <td align="left">
                            <asp:DropDownList ID="drpTickerSpeed" CssClass="altText" runat="server">
                                    <asp:ListItem  Value="3" Text="Lent"></asp:ListItem>                                    
                                    <asp:ListItem   Value="6" Text="Moyen"></asp:ListItem>
                                    <asp:ListItem  Value="18" Text="Vite"></asp:ListItem>
                            </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="blackblodtext">Couleur de fond du ruban</td>
                            <td align="left" style="width:200px"> <%-- FB 2050 --%>
                                <asp:TextBox ID="txtTickerBknd" CssClass="altText" runat="server" EnableViewState="True"></asp:TextBox>
                                <IMG title="Appuyer � select the color" onclick="show_RGBPalette('txtTickerBknd',event)"
					                height="23" alt="Color" src="../Image/color.jpg" width="27" name="imggen" style="vertical-align:middle">
					               
			                </td>
                             <td align="right" class="blackblodtext">Visualisation du ruban</td>
                            <td align="left">
                               <asp:DropDownList ID="drpTickerDisplay" CssClass="altText" runat="server" onclick="javascript:fnShowFeed();">
                                    <asp:ListItem  Selected="True"  Value="0" Text="My Conferences"></asp:ListItem>                                    
                                    <asp:ListItem  Value="1" Text="RSS feed"></asp:ListItem>
                            </asp:DropDownList>
                            </td>
                            <td align="right" class="blackblodtext" id="feedLink" runat="server" style="display:none;">Lien du flux RSS</td>
                            <td align="left">
                                <asp:TextBox ID="txtFeedLink" CssClass="altText" runat="server"></asp:TextBox>
                                
                                
                            </td>
                          </tr>
                           <tr>
                          <td colspan="6" align="right">
                          <asp:RegularExpressionValidator ID="RegularExpressionValidator6" ControlToValidate="txtFeedLink" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + %  | = ! ` , [ ] { }  # $ ~ et &#34; sont des characteres invalides." ValidationExpression="^(a-z|A-Z|0-9)*[^<>+|!`,[\]{}22;=#$%&~]*$"></asp:RegularExpressionValidator></td>
                          </tr>
                    </table>
                </td>
            </tr>
            <tr> 
                <td align="Left">
                    <table cellspacing="5" style="width:100%;"><%--FB 2611--%>
                        <tr>
                            <td style="width:20">&nbsp;</td>
                            <td>
                                <span class="subtitleblueblodtext">Ticker Arrangements 2</span>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="Tr1" runat="server">
                <td align="center" class="tableBody"> 
                    <table style="width:100%" cellpadding="2" cellspacing="3" border="0"> <%--FB 2611--%>
                        <tr>
                            <td align="right" class="blackblodtext">Conidtion de visualisation du Ruban</td>
                             <td align="left">
                                <asp:DropDownList ID="drpTickerStatus1" CssClass="altText" runat="server">
                                    <asp:ListItem  Value="0" Text="Montrer"></asp:ListItem>                                    
                                    <asp:ListItem  Selected="True" Value="1" Text="Cach�"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td align="right" class="blackblodtext">Position de ruban</td>
                            <td align="left">
                             <asp:DropDownList ID="drpTickerPosition1" CssClass="altText" runat="server">
                                    <asp:ListItem Selected="True" Value="0" Text="Haut de Page"></asp:ListItem>                                    
                                    <asp:ListItem  Value="1" Text="Bas de Page"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td align="right" class="blackblodtext">Vitesse de t�l�scripture</td>
                            <td align="left">
                            <asp:DropDownList ID="drpTickerSpeed1" CssClass="altText" runat="server">
                                    <asp:ListItem  Value="3" Text="Lent"></asp:ListItem>                                    
                                    <asp:ListItem  Selected="True" Value="6" Text="Moyen"></asp:ListItem>
                                    <asp:ListItem  Value="18" Text="Vite"></asp:ListItem>
                            </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="blackblodtext">Couleur de fond du ruban</td>
                            
                            <td align="left" style="width:200px"> <%-- FB 2050 --%>
                                <asp:TextBox ID="txtTickerBknd1" CssClass="altText" runat="server" EnableViewState="True"></asp:TextBox>
                                <IMG title="Appuyer � select the color" onclick="show_RGBPalette('txtTickerBknd1',event)"
					                height="23" alt="Color" src="../Image/color.jpg" width="27" name="imggen" style="vertical-align:middle">
					                
			                </td>
                             <td align="right" class="blackblodtext">Visualisation du ruban</td>
                            <td align="left">
                               <asp:DropDownList ID="drpTickerDisplay1" CssClass="altText" runat="server" onclick="javascript:fnShowFeed();">
                                    <asp:ListItem  Selected="True"  Value="0" Text="My Conferences"></asp:ListItem>                                    
                                    <asp:ListItem  Value="1" Text="RSS feed"></asp:ListItem>
                            </asp:DropDownList>
                            </td>
                            <td align="right" class="blackblodtext" id="feedLink1" runat="server">RSS Feed link</td>
                            <td align="left">
                                <asp:TextBox ID="txtFeedLink1" CssClass="altText" runat="server" style="display:none;"></asp:TextBox>
                                
                                
                            </td>
                          </tr>
                          <tr>
                          <td colspan="6" align="right">
                          &nbsp;&nbsp;&nbsp;<asp:RegularExpressionValidator ID="RegularExpressionValidator5" ControlToValidate="txtFeedLink1" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + %  | = ! ` , [ ] { }  # $ ~ et &#34; sont des characteres invalides." ValidationExpression="^(a-z|A-Z|0-9)*[^<>+|!`,[\]{}22;=#$%&~]*$"></asp:RegularExpressionValidator></td>
                          </tr>
                    </table>
                </td>
            </tr>
             <%--Tickers End--%>
             </table></td></tr> <%--FB 1985 - End--%>
            <tr>
                <td style="height:30">&nbsp;</td>
            </tr>            
            <tr>
                <td align="center">
                    <table style="width:100%"  ><%--FB 2611--%>
                        <tr>
                            <td style="width:30%" align="center"><%--FB 2611--%>
                                <asp:Button runat="server" ID="btnReset" Text="Remise � zero" ValidationGroup="Reset" OnClick="BindData" CssClass="altLongBlueButtonFormat" />  <%--OnClientClick="javascript:testConnection();return false;"--%>
                            </td>
                            <%-- FB 2025 starts --%>
                            <td style="width:30%" align="center"><%--FB 2611--%>
                                <asp:Button runat ="server" ID="btnnewSubmit" OnClientClick ="javascript:return RulePassword()" OnClick ="SubmitNewUser"
                                CssClass ="altLongBlueButtonFormat" Text ="Soumettre / Add New User" /><%--FB 2339--%>
                            </td>
                            <%-- FB 2025 end --%>
                            <td style="width:30%" align="center"><%--FB 2611--%>
                                <asp:Button ID="btnSubmit" OnClientClick="javascript:return RulePassword()"  OnClick="SubmitUser"
                                    runat="server" CssClass="altLongBlueButtonFormat" Text="Soumettre" /><%--API Port--%><%--FB 2339--%>
                            </td>
                            
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
<script language="javascript">
    SavePassword();
//FB 1830 - DeleteEmailLang start
    function fnDelEmailLan() {
        if (document.getElementById("txtEmailLang").value == "")
            return false;
        else
            return true;
    }
//FB 1830 - DeleteEmailLang end

    //FB 1985 - Start
    function fnShowHideAVLink()
	{
	  
	    var args = fnShowHideAVLink.arguments;
	    var obj = eval(document.getElementById("LnkAVExpand"));
	  
	    if(obj)
	    {
	       
	        obj.style.display = 'none';
	        if(args[0] == '1')
	        {
	            obj.style.display = '';
	        }
	        
	    }
	}
	
	function fnShowAVParams()
	{
	    //var obj = eval(document.getElementById("tdrssFeed"));
	    var obj1 = eval(document.getElementById("tdOtherSettings"));
	    var linkState = eval(document.getElementById("hdnAVParamState"));
	    var expandlink = eval(document.getElementById("LnkAVExpand"));
	    
	    if(linkState)
	    {
	        //if(obj && obj1)
	        if(obj1)
	        {
	            //obj.style.display = 'none';
	            obj1.style.display = 'none';
	            if(linkState.value == '')
	            {
	                //obj.style.display = '';
	                obj1.style.display = '';
	                linkState.value = '1';
	                
	                if(expandlink)
	                {
	                    expandlink.innerHTML = 'Collapse';
	                }
	            }
	            else
	            {
	                //obj.style.display = 'none';
	                obj1.style.display = 'none';
	                linkState.value = '';
	                if(expandlink)
	                {
	                    expandlink.innerHTML = 'Expand';
	                }
	            }
	        }
	     }
	     return false;
	}
	//FB 1985 - End

	function AddRemoveHelpReq(opr)//FB 2268
	{
	    var lstHelpReq = document.getElementById("lstHelpReqEmail");
	    var option = document.createElement("Option");
	    var txtHelpReq = document.getElementById("txtHelpReqEmail");
	    var hdnHelpReq = document.getElementById("hdnHelpReq");

	    if (!Page_ClientValidate())
	        return Page_IsValid;

	    if (opr == "Rem") {
	        var i;
	        for (i = lstHelpReq.options.length - 1; i >= 0; i--) {
	            if (lstHelpReq.options[i].selected) {
	                hdnHelpReq.value = hdnHelpReq.value.replace(lstHelpReq.options[i].text, "").replace(/��/i, "�");
	                lstHelpReq.remove(i);
	            }
	        }
	    }
	    else if (opr == "add") {

	        if (txtHelpReq.value.replace(/\s/g, "") == "") //trim the textbox
	            return false;

	        if (lstHelpReq.options.length >= 5) {
	            document.getElementById("errLabel").innerHTML = "Maximum 5 Emails";
	            document.getElementById("errLabel").className = "lblError";//FB 2487
	            document.getElementById("errLabel").focus();
	            return false;
	        }
	        else {
	            if (hdnHelpReq.value.indexOf(txtHelpReq.value) >= 0) {
	                document.getElementById("errLabel").innerHTML = "Already Added Email address";
	                document.getElementById("errLabel").className = "lblError";//FB 2487
	                return false;
	            }
	        }

	        if (lstHelpReq.options.length > 0)
	            hdnHelpReq.value = hdnHelpReq.value + "�";

	        var option = document.createElement("Option");
	        option.text = txtHelpReq.value;
	        option.title = txtHelpReq.value;
	        lstHelpReq.add(option);
	        hdnHelpReq.value = hdnHelpReq.value + txtHelpReq.value;

	        txtHelpReq.value = "";
	        txtHelpReq.focus();
	    }

	    return false;
	    
	    //FB 2348 Start
        if("<%=Session["EnableSurvey"]%>" == "1")
        {
            document.getElementById("trSurveyEmail").style.visibility = "visible";
        }
	    //FB 2348 End
	}

	//FB 2481 Start
	if(document.getElementById("dxHTMLEditor_TD_T0_DXI15_Img"))
	       document.getElementById("dxHTMLEditor_TD_T0_DXI15_Img").style.display = "none";
	//FB 2481 End
	
</script>
    </form>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
    <!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
</body>
</html>
<%--FB 2491 Start--%>
<script type="text/javascript">
    function fnScrollTop() {
        window.scrollTo(0, 0);
        document.getElementById('hideScreen').style.display = "none";
    }
    window.onload = setTimeout("fnScrollTop()", 1);
</script>
<%--FB 2491 End--%>


<%@ Page Language="C#" Inherits="ns_MyVRM.LocationList" ValidateRequest="false" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">

    protected void lstRoomSelection_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
</script>

<script type="text/javascript" src="script/DisplayDIV.js"></script>

<script type="text/javascript" src="script/mytreeNET.js"></script>

<html>
<head>
    <link rel="stylesheet" title="MyVRM base styles" type="text/css" href="<%=Session["OrgCSSPath"]%>">
    <meta http-equiv="Content-Type" content="text/html; charset=utf8" />
<title></title>
    <script language="javascript">

    </script>

</head>
<body>
    <form id="frmLocationList" runat="server" method="post" onsubmit="return true">
        <div>
            <table width="100%" id="tblMain">
                <tr>
                    <td align="center">
                        <h3>
                            Select a Location
                        </h3>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Label ID="errLabel" runat="server" Font-Size="Small" ForeColor="Red" Visible="False"
                            Font-Bold="True"></asp:Label>
                        <asp:TextBox ID="txtRoomID" runat="server" Visible="false"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top">
                        <asp:RadioButtonList ID="rdSelView" runat="server" OnSelectedIndexChanged="rdSelView_SelectedIndexChanged"
                            RepeatDirection="Horizontal" AutoPostBack="True" RepeatLayout="Flow">
                            <asp:ListItem Value="1" Selected="True"><span class='blackblodtext'>Level Visualisation</span></asp:ListItem>
                            <asp:ListItem Value="2"><span class='blackblodtext'>Liste séquentielle</span></asp:ListItem>
                        </asp:RadioButtonList><br />
                        <asp:Panel ID="pnlLevelView" runat="server" Height="550" Width="100%" ScrollBars="Auto"
                            BorderColor="Blue" BorderStyle="Solid" BorderWidth="1px" HorizontalAlign="Left">
                            <asp:TreeView ID="treeRoomSelection" runat="server" Height="90%" ShowCheckBoxes="Leaf"
                                ShowLines="True" Width="95%" onclick="javascript:getOneRoom(event)">
                                <NodeStyle CssClass="treeNode" />
                                <RootNodeStyle CssClass="treeRootNode" />
                                <SelectedNodeStyle CssClass="treeSelectedNode" />
                                <ParentNodeStyle CssClass="treeParentNode" />
                                <LeafNodeStyle CssClass="treeLeafNode" />
                            </asp:TreeView>
                        </asp:Panel>
                        <asp:Panel ID="pnlListView" runat="server" BorderColor="Blue" BorderStyle="Solid"
                            BorderWidth="1px" Height="500" ScrollBars="Auto" Visible="False" Width="100%"
                            HorizontalAlign="Left" Direction="LeftToRight" Font-Bold="True" Font-Names="Verdana"
                            Font-Size="Small" ForeColor="Green">
                            <asp:RadioButtonList ID="lstRoomSelection" runat="server" Height="95%" Width="95%"
                                Font-Size="Smaller" ForeColor="ForestGreen" Font-Names="Verdana" RepeatLayout="Flow"
                                OnSelectedIndexChanged="lstRoomSelection_SelectedIndexChanged">
                            </asp:RadioButtonList>
                        </asp:Panel>
                        <%--Added for Salle de Conférence Issues  - Start--%>
                        <asp:Panel ID="pnlNoData" runat="server" BorderColor="Blue" BorderStyle="Solid" BorderWidth="1px"
                            Height="300px" ScrollBars="Auto" Visible="False" Width="100%" HorizontalAlign="Left"
                            Direction="LeftToRight" Font-Size="Small">
                            <table>
                                <tr align="center">
                                    <td>
                                        You have Non Room(s) available
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <%--Added for Salle de Conférence Issues  - End--%>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Button ID="btnSubmit" OnClick="SetRoom" CssClass="altShortBlueButtonFormat"
                            Text="Soumettre" runat="server" />
                            <asp:Button ID="btnClose" OnClientClick="window.close();" CssClass="altShortBlueButtonFormat"
                            Text="Close" runat="server" Visible="false" />
                    </td>
                </tr>
            </table>
            <br />
            <br />
        </div>

        <script language="javascript">
        window.resizeTo(500, 700);
        </script>

    </form>
</body>
</html>

<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>


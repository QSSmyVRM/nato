<%@ Page Language="C#" Inherits="ns_UserTemplates.UserTemplates" Buffer="true" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <!-- FB 2050 -->
<meta http-equiv="X-UA-Compatible" content="IE=7" /> <!-- FB 2050 -->

<!-- #INCLUDE FILE="inc/maintopNET.aspx" --> 
    <script type="text/javascript" src="inc/functions.js"></script>

<script runat="server">

</script>
<script language="javascript">
function CheckName()
{
}
</script>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf8" />
    <title>G�rer Utilisateur Mod�les</title>

</head>
<body>
    <form id="frmInventoryManagement" runat="server" method="post" onsubmit="return true">
      <input type="hidden" id="helpPage" value="105">

    <div>
        <table width="100%">
            <tr>
                <td align="center">
                    <h3>
                        G�rer Utilisateur Templates
                    </h3>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError"></asp:Label>
                    <div id="dataLoadingDIV" style="z-index:1"></div>
                </td>
            </tr>
            <tr>
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td >&nbsp;</td>
                            <td>
                                <SPAN class=subtitleblueblodtext>Mod�les existant (Utilisateur)</SPAN>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:DataGrid ID="dgUserTemplates" runat="server" AutoGenerateColumns="False" CellPadding="4" GridLines="None"
                        BorderColor="blue" BorderStyle="none" BorderWidth="0" ShowFooter="true" OnItemCreated="BindRowsDeleteMessage"
                        OnDeleteCommand="DeleteUserTempate" OnEditCommand="EditUserTemplate" Width="90%" Visible="true" >
                        <AlternatingItemStyle CssClass="tableBody" />
                        <ItemStyle CssClass="tableBody" HorizontalAlign="left" />
                        <HeaderStyle CssClass="tableHeader" HorizontalAlign="left" />
                        <Columns>
                            <asp:BoundColumn DataField="ID" Visible="false" ><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>
                            <asp:BoundColumn DataField="Name" HeaderText="Template Name"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>
                            <asp:BoundColumn DataField="RoleName" HeaderText="r�le"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="Actions" ItemStyle-Width="15%">
                                <HeaderStyle CssClass="tableHeader" />
                                <ItemTemplate>
                                    <asp:LinkButton runat="server" Text="Modifier" OnClientClick="DataLoading(1)" ID="btnEdit" CommandName="Edit"></asp:LinkButton>
                                    &nbsp;<asp:LinkButton runat="server" Text="Supprimer" ID="btnDelete" CommandName="Delete"></asp:LinkButton>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <span class="blackblodtext"><b>Nomber total de Mod�les:</sapn> <asp:Label ID="lblTotalRecords" runat="server" Text=""></asp:Label> </b>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:Table runat="server" ID="tblNoUserTemplates" Visible="false" Width="90%">
                        <asp:TableRow CssClass="lblError">
                            <asp:TableCell CssClass="lblError">
                                Non Utilisateur Mod�les found.
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>                    
                </td>
            </tr>
            <tr>
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <SPAN class=subtitleblueblodtext>Cr�er un nouveau Template</SPAN>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table width="90%"><tr><td align="right">
                        <asp:Button ID="btnCreateNew" OnClick="CreateNewTemplate" CssClass="altLongBlueButtonFormat" Text="Soumettre" runat="server" />
                    </td></tr></table>
                </td>
            </tr>
        </table>
    </div>

<img src="keepalive.asp" name="myPic" width="1px" height="1px">
    </form>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
    <!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
</body>
</html>



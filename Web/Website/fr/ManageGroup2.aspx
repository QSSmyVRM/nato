<%@ Page Language="C#" AutoEventWireup="true" Inherits="ns_MyVRM.ManageGroup2"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <!-- FB 2050 -->
<meta http-equiv="X-UA-Compatible" content="IE=7" /> <!-- FB 2050 -->

<!-- #INCLUDE FILE="inc/maintopNET.aspx" --> 

<script runat="server">

</script>
<script type="text/javascript" src="script/mousepos.js"></script>
<script type="text/javascript" src="script/managemcuorder.js"></script>
<script type="JavaScript" src="inc/functions.js"></script>
<script type="text/javascript" src="extract.js"></script> <%--Login Management--%>
<script type="text/javascript" src="script/group2.js"></script>


<script type="text/javascript" language="javascript">

	function frmManagegroup2_Validator()
    {	
        var groupname = document.getElementById('<%=GroupName.ClientID%>');
        if( groupname.value == "")
        {
            var txtReqFieldGName = document.getElementById('<%=ReqFieldGName.ClientID%>');
            txtReqFieldGName.style.visibility = 'visible';
            groupname.focus();
            return false;
        }
        else if (groupname.value.search(/^(a-z|A-Z|0-9)*[^\\/<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$/)==-1)
        {        
            regGName.style.display = 'block';
            groupname.focus();
            return false;
        }           
	    // chk group number
	    if(document.frmManagegroup2.PartysInfo.value == "")  //EDITED for FF
	    {
		    alert(EN_128);
		    return false;
	    }
	    else
	    {
	        if(document.frmManagegroup2.PartysInfo.value == "|") //FB 1914
	        {
		        alert(EN_128);
		        return false;
	        }
	    }
	    if(chkGroupExists(document.frmManagegroup2.GroupName.value) == true) 
	    {	
		    alert("Group already exists! Please enter another name.");
		    document.frmManagegroup2.GroupName.focus();
		    return false;
	    }	    
    	
    	var grpDesc = document.getElementById('<%=GroupDescription.ClientID%>');
	    if (Trim(grpDesc.value) != "")
	    {
	        if (grpDesc.value.search(/^(a-z|A-Z|0-9)*[^\\/<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$/))
            {
                regGrpDescription.style.display = 'block';
                grpDesc.focus();
                return false;
            }
	    }
    	
	    // refresh the party list and calculate the party number
	    willContinue = ifrmMemberlist.bfrRefresh(); 
	    if (!willContinue)
		    return false;

	    
    	
	    return true;
    }
    
    function chkGroupExists(gName)
    {
	    for(var i=0; i< document.frmManagegroup2.Group.options.length; i++)
	    {
		    if (gName.toLowerCase() == document.frmManagegroup2.Group.options(i).text.toLowerCase())
		    {	
			    return true;
		    }
	    }
    }
    
    // FB 2050 Starts
    function refreshIframe()
    {
    var iframeId = document.getElementById('ifrmMemberlist');
    iframeId.src = iframeId.src;
    }
    // FB 2050 Ends

</script>

  <div id="tblViewDetails" style="display:none">
  </div>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server" id="Head1">
<meta http-equiv="Content-Type" content="text/html; charset=utf8" />
    <title>My Groupes</title>
    <script type="text/javascript" src="inc/functions.js"></script>
    <link rel="stylesheet" title="Expedite base styles" type="text/css" href="<%=Session["OrgCSSPath"]%>"/>  
</head>
<body>
    <form id="frmManagegroup2" runat="server">
    <div>
     <input type="hidden" id="helpPage" value="73" />
        <table width="75%" align="center" cellpadding="4" border="0">
            <tr>
                <td align="center" colspan="4">
                    <h3>
                        <asp:Label ID="lblHeader" runat="server" Text="Cr�ation d'un groupe"></asp:Label>
                    </h3>
                </td>
            </tr>
            <tr>
                <td align="center" colspan="4" style="width: 1168px">
                    <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="4" align="right">
                    <span class="reqfldstarText">*</span>
                    <span class="reqfldText">Element Obligatoire</span>
                 </td>
            </tr>
            <tr>
                <td colspan="4">
                  <span class="subtitleblueblodtext">Group renseignements </span>       
                </td>
            </tr>
            <tr>
                 <%--Window Dressing--%>
                <td align="right" style="width:16%;">
                   <span class="blackblodtext"> Nom de groupe</span>
                    <span class="reqfldstarText">*</span>                   
                </td>
                <td colspan="2">
                     <asp:TextBox ID="GroupName" runat="server" CssClass="altText" Width="170"></asp:TextBox>
                     <asp:RequiredFieldValidator ID="ReqFieldGName" Text="Required"  ErrorMessage="Requis" ControlToValidate="GroupName" runat="server" SetFocusOnError="true"></asp:RequiredFieldValidator>                     
                    <asp:RegularExpressionValidator ID="regGName" ControlToValidate="GroupName" Display="dynamic" runat="server" ValidationGroup="Submit" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ et &#34; sont des characteres invalides." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                </td>
                <%--Window Dressing--%>
                <td align="right">
                    <span class="blackblodtext">Publique (dans le Monde myVRM) Group</span>
                    <asp:CheckBox ID="GroupPublic" runat="server"  />
                </td>
            </tr>
            <tr>
                <td align="right">
                   <span class="blackblodtext">Description&nbsp;</span>
                </td>
                <td colspan="3">
                    <asp:TextBox ID="GroupDescription" TextMode="MultiLine" Width="200" Rows="4" Columns="15" runat="server" CssClass="altText"></asp:TextBox>                    
                    <asp:RegularExpressionValidator ID="regGrpDescription" ControlToValidate="GroupDescription" Display="dynamic" runat="server" ValidationGroup="Submit" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ et &#34; sont des characteres invalides." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td colspan="4">
		          <span class="subtitleblueblodtext">Member renseignements</span>
		        </td>
		     </tr>
		
		     <tr>
		       
		        <td colspan="4">
		            <table  cellpadding="2" cellspacing="0" width="100%"  height="95">
                      <tr>
                      <td></td>                        
                        <td  width="90%" bordercolor="#0000FF" align="left">
                          <table border="0" cellpadding="2" cellspacing="0" width="100%">
                           <%--Window Dressing start--%>
                            <tr class="tableHeader">
                              <td align="center" style="width:10%" class="tableHeader"><b>Supprimer</b></td>
                              <td align="center" style="width:50%" class="tableHeader"><b>Nom</b></td>
                              <td align="center" style="width:40%" class="tableHeader"><b>EMAIL</b></td>
                          <%--Window Dressing end--%>
                            </tr>
                          </table>
                        </td>
                      </tr>
                      <tr> 
                       <td height="10" align="right" valign="top">
                 <span class="blackblodtext">Membres</span>
                </td>                     
                        <td bordercolor="#0000FF" align="center">
                          <table  cellpadding="0" cellspacing="0" width="100%" style="height:99">
                            <tr >
                              <td style="width:100%;height:100" valign="top" align="left">                              
                                <!--Added for Group2Member.asp Start-->                             
                                <iframe src="group2member.aspx?wintype=ifr" name="ifrmMemberlist" id="ifrmMemberlist" width="100%" height="100" style="vertical-align:top; "> <%--Edited for FF--%>
                                  <p>Aller <a href="group2member.aspx?wintype=ifr">Membres</a></p>
                                </iframe> 
                                <!--Added for Group2Member.asp End-->   
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>    
                    </table> 		            
		        </td>
		    </tr>
		    <tr >
                <td style="height:62" valign="top" align="right">
                  <span class="blackblodtext">Groupes&nbsp;</span>
                </td>
                <td style="width:30%">
                   <asp:ListBox ID="Group" CssClass="altText" style="scrollbar-shadow-color: #DEE3E7;" runat="server" SelectionMode="Multiple" Width="100%">
                    </asp:ListBox>   
                </td>
                <td>
                    <span class="blackblodtext">Selectioner le groupe pour l'inviter. Double-click pour voir les Invit�s du groupe.<br /> Pour d�s�lectionner le groupe, appuyez sur ctrl + clic</span>
                    <asp:TextBox ID="UsersStr" runat="server" Width="0px" ForeColor="transparent" BackColor="transparent" BorderStyle="None" BorderColor="Transparent"></asp:TextBox>
                    <asp:TextBox ID="PartysInfo" runat="server" Width="0px" ForeColor="Black" BackColor="transparent" BorderStyle="None" BorderColor="Transparent"></asp:TextBox>
                </td>
                <td align="right" style="width:15%">
                <table> <%--Edited for FF--%>
                <tr>
                <td>              
                    <input type="button" name="Managegroup2Submit" value="Retirer Tout" style="width:150px" class="altShortBlueButtonFormat" onclick="deleteAllParty();" lang="JavaScript" />                    
                    </td>
                    </tr>
                <tr>
                <td>
                    <%--code changed for Softedge button--%>
                    <input type="button" onfocus="this.blur()" name="Managegroup2Submit" value="myVRM rechercher" style="width:150px" class="altShortBlueButtonFormat" onclick="getYourOwnEmailList();" lang="JavaScript" />
                 </td>
                </tr>
                </table>
                </td>
            </tr>
            <tr>
                <td colspan="4" align="left">
		          <span class="subtitleblueblodtext">Confirrmer Groupe Cr�ation</span>
                </td>
            </tr>
            <tr>
                <td colspan="4" align="right">                    
                    <%--code changed for Softedge button--%>
                    <input type="button" name="btnCancel" onfocus="this.blur()" class="altShortBlueButtonFormat" value="Soumettre" style="width:150px" onclick="javascript:window.location.replace('ManageGroup.aspx');" />
                    <asp:Button ID="Managegroup2Submit" onfocus="this.blur()" runat="server" Text="Soumettre" ValidationGroup="Submit"  style="width:150px" OnClick="Managegroup2Submit_Click" OnClientClick="javascript:return frmManagegroup2_Validator();" CssClass="altShortBlueButtonFormat" />
                </td>
            </tr>
         </table>
      </div>
    </form> 
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
    <!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
</body>
</html>


<%@ Page Language="C#" Inherits="ns_EntityCode.EntityCode" EnableSessionState="True" ValidateRequest="false"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<link rel="stylesheet" type="text/css" href="<%=Session["OrgCSSPath"]%>">
<script type="text/javascript">

function frmValidator()
{   
    var txtentityname = document.getElementById('<%=txtEntityName.ClientID%>');
    if(txtentityname.value == "")
    {        
        reqEntityName.style.display = 'block';
        txtentityname.focus();
        return false;
    }
    else if (txtentityname.value.search(/^(a-z|A-Z|0-9)*[^\\/<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$/)==-1)
    {        
        regItemName1.style.display = 'block';
        txtentityname.focus();
        return false;
    }    
    
    var txtentitydesc = document.getElementById('<%=txtEntityDesc.ClientID%>');
    
    if(txtentitydesc.value != "")
    {
        if (txtentitydesc.value.search(/^(a-z|A-Z|0-9)*[^\\/<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$/)==-1)
        {        
            regItemDesc.style.display = 'block';
            txtentitydesc.focus();
            return false;
        }    
    }
    
    return(true);
}

function fnGridValidation()
{
    
    var args = document.getElementById("hdnValue").value;
    var recCount = 2;	    
    var gridName = document.getElementById("dgEntityCode");
    
    for(var i=2; i<=args + 1; i++)
	{				
		statusValue = "";
		if(i < 10)
		    i = "0" + i;
		 
		var newText = document.getElementById("dgEntityCode_ctl"+ i + "_txtEntityName");
		var rqName = document.getElementById("dgEntityCode_ctl"+ i + "_reqName");
		var rgName = document.getElementById("dgEntityCode_ctl"+ i + "_regItemName1");
		
		if(newText)
		{
			if(newText.value == "")
            {        
                rqName.style.display = 'block';
                newText.focus();
                return false;
            }
            
            else if (newText.value.search(/^(a-z|A-Z|0-9)*[^\\<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$/)==-1)
            {        
                rgName.style.display = 'block';
                newText.focus();
                return false;
            }   
		}	
		
		var newText1 = document.getElementById("dgEntityCode_ctl"+ i + "_txtEntityDesc");		
		var rgName1 = document.getElementById("dgEntityCode_ctl"+ i + "_regItemName");
		
		if(newText1)
		{
			if(newText1.value != "")
            {        
               if (newText1.value.search(/^(a-z|A-Z|0-9)*[^\\<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$/)==-1)
                {      
                    rgName1.style.display = 'block';
                    newText1.focus();
                    return false;
                }   
		    }	
		}
    }
    
    return true;	    
}
</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf8" />
    <title>Billing Code</title>     <%--Entity Code Changed--%>
</head>
<body>
    <form id="frmItemsList" runat="server" method="post" onsubmit="return true">
    <center>
    <input type="hidden" runat="server" id="hdnValue" />
    <div>
        <table width="100%" border="0">
            <tr>
                <td align="center" colspan="3">
                    <h3>G�rer Billing Code </h3>       <%--Entity Code Changed--%>
                </td>
            </tr>
            <tr>
                <td align="center" colspan="3">
                    <asp:Label ID="errLabel" runat="server" CssClass="lblError"  Visible="False"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="center" colspan="3">
                    <asp:DataGrid BorderColor="blue" BorderStyle="solid" BorderWidth="1" ID="dgEntityCode" AutoGenerateColumns="false"
                     OnEditCommand="EditItem" OnItemCreated="BindRowsDeleteMessage" OnDeleteCommand="DeleteItem" OnCancelCommand="CancelItem" OnUpdateCommand="UpdateItem"
                     runat="server" Width="90%" GridLines="None"> 
                        <HeaderStyle Height="30" CssClass="tableHeader" HorizontalAlign="Center" />
                        <AlternatingItemStyle CssClass="tableBody"/>
                        <ItemStyle CssClass="tableBody" />
                        <FooterStyle CssClass="tableBody" />
                        <Columns>
                            <asp:BoundColumn DataField="OptionID" Visible="false"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="Nom" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody">
                                <ItemTemplate>
                                    <asp:Label ID="lblEntityName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DisplayCaption") %>' ></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtEntityName" CssClass="altText" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DisplayCaption") %>'></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="reqName" ValidationGroup="Update" runat="server" ControlToValidate="txtEntityName" ErrorMessage="Requis" Display="dynamic" ></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="regItemName1" ControlToValidate="txtEntityName" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ <br> et &#34; sont des characteres invalides." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Description" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody">
                                <ItemTemplate>
                                    <asp:Label ID="lblEntityDesc" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.HelpText") %>' ></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtEntityDesc" CssClass="altText" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.HelpText") %>'></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="regItemName" ControlToValidate="txtEntityDesc" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ <br> et &#34; sont des characteres invalides." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Actions" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody">
                                <ItemTemplate>
                                    <asp:LinkButton ID="btnEdit" Text="Editer" CommandName="Edit" runat="server"></asp:LinkButton>
                                    <asp:LinkButton ID="btnDelete" Text="Supprimer" CommandName="Delete" runat="server"></asp:LinkButton>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:LinkButton ID="btnUpdate" Text="Update" CommandName="Update" runat="server" OnClientClick="javascript:return fnGridValidation()" ValidationGroup="Update"></asp:LinkButton>
                                    <asp:LinkButton ID="btnCancel" Text="Annuler" CommandName="Cancel" runat="server"></asp:LinkButton>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </td>
            </tr>
            <tr>
                <td>
                    <br />
                    <br />
                    <br />
                    <table width="90%">
                        <tr>
                            <td align="left">
                                <span class="subtitleblueblodtext">Add New Billing Code:</span>     <%--Entity Code Changed--%>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <table width="100%" cellspacing="5" border="0">
                                    <tr>
                                        <td align="left" class="blackblodtext" style="width:30%;">
                                             Billing Code Nom      <%--Entity Code Changed--%>
                                        </td>
                                        <td align="left"  style="width:20%;">
                                            <asp:TextBox ID="txtEntityID" Text="new" Visible="false" runat="server"></asp:TextBox>
                                            <asp:TextBox ID="txtEntityName" runat="server" CssClass="altText" ></asp:TextBox>
                                         </td>
                                         <td align="left">
                                            <asp:RequiredFieldValidator ID="reqEntityName" ValidationGroup="Upload" runat="server" ControlToValidate="txtEntityName" ErrorMessage="Requis" Display="dynamic" ></asp:RequiredFieldValidator>                                            
                                            <asp:RegularExpressionValidator ID="regItemName1" ControlToValidate="txtEntityName" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ et &#34; sont des characteres invalides." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="blackblodtext" nowrap>
                                          Billing Code Description      <%--Entity Code Changed--%>
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="txtEntityDesc" runat="server" CssClass="altText"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:RegularExpressionValidator ID="regItemDesc" ControlToValidate="txtEntityDesc" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ et &#34; sont des characteres invalides." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" >
                                        </td>
                                        <td align="center" colspan="2">
                                            <asp:Button ID="btnAddEntityCode" OnClick="AddEntityCode" ValidationGroup="Upload" runat="server" CssClass="altShortBlueButtonFormat" Text="Soumettre" OnClientClick="javascript:return frmValidator()" ></asp:Button>
                                            <input id="Cancel" type="button" onclick="javascript:window.close()" value="Soumettre" class="altShortBlueButtonFormat"/>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
        <br />
            <br />
            &nbsp;
        </center>
    </form>
</body>
</html>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>


<%@ Page Language="C#" Inherits="ns_EndpointList.EndpointList" ValidateRequest="false" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <!-- FB 2050 -->
<meta http-equiv="X-UA-Compatible" content="IE=7" /> <!-- FB 2050 -->

<!-- Window Dressing-->
<!-- #INCLUDE FILE="inc/maintopNET.aspx" --> 


<html xmlns="http://www.w3.org/1999/xhtml" >

<head id="Head1" runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf8" />
</head>
<script language="javascript">
function ViewBridgeDetails(bid)
{
    url = "BridgeDetailsViewOnly.aspx?hf=1&bid=" + bid;
    window.open(url, "BrdigeDetails", "width=900,height=800,resizable=yes,scrollbars=yes,status=no");
    return false;
}
function CheckSelection(obj)
{
//      alert(obj.type);
      if (obj.tagName == "INPUT" && obj.type == "Radio")
            for (i=0; i<document.frmEndpoints.elements.length;i++)
            {
                var obj1 = document.frmEndpoints.elements[i];
                if (obj1.id != obj.id)
                    obj1.checked = false;
            }
}
    
</script>
    <title>G�rer Points de connexion</title>

<body>
    <form id="frmEndpoints" runat="server" method="post">
        <center><table border="0" width="98%" cellpadding="2" cellspacing="2">
            <tr>
                <td align="center">
                    <h3><asp:Label ID="lblHeader" runat="server" Text="G�rer Points de connexion"></asp:Label></h3><br />
                     <asp:Label ID="errLabel" runat="server" CssClass="lblError"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="Left">
                    <table>
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <asp:Label ID="lblSubHeader1" Text="Points de connection disponibles" runat="server" CssClass="subtitleblueblodtext" ></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
             </tr>
              <%--Endpoint Search--%>
             <tr>
             <td>
              <iframe id="EndpointFrame" runat="server" width="100%" valign="top" height="625px" scrolling="Non"></iframe>
             </td>
             </tr>
            <tr style="display:none">
                <td align="center">
                <asp:DataGrid ID="dgEndpointList" runat="server" AutoGenerateColumns="False" Font-Names="Verdana" Font-Size="Small"
                 Width="95%" OnItemDataBound="LoadProfiles" OnItemCreated="BindRowsDeleteMessage" CellPadding="4" GridLines="None"
                 BorderColor="blue" BorderStyle="solid" BorderWidth="1" AllowSorting="True" OnSortCommand="SortGrid"
                 OnEditCommand="EditEndpoint" OnCancelCommand="DeleteEndpoint" ShowFooter="true">
                <%--Window Dressing - Start--%>
                <SelectedItemStyle CssClass="tableBody" Font-Bold="True" />
                <EditItemStyle CssClass="tableBody" />
                <AlternatingItemStyle CssClass="tableBody" />
                <ItemStyle CssClass="tableBody" />
                <FooterStyle CssClass="tableBody" />
                <%--Window Dressing - End--%>
                    <Columns>
                        <asp:BoundColumn DataField="ID" Visible="false" HeaderText="ID" SortExpression="ID"></asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="Select" HeaderStyle-CssClass="tableHeader" Visible="false">
                            <ItemTemplate>
                                <asp:RadioButton ID="rdSelectEndpoint" runat="server" onclick="javascript:CheckSelection(this)" />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn ItemStyle-HorizontalAlign="left" ItemStyle-CssClass="tableBody" HeaderStyle-HorizontalAlign="left" HeaderStyle-CssClass="tableHeader" HeaderText="Endpoint<br>Nom" SortExpression="EndpointName">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblEPName" Text='<%#DataBinder.Eval(Container, "DataItem.EndpointName") %>'></asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <br /><asp:RadioButton onclick="javascript:CheckSelection(this)" Visible="true" Checked="true" ID="rdNewEndpoint" runat="server" Text="Cr�er un nouveau point de terminaison" />
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="DefaultProfileName" ItemStyle-CssClass="tableBody"  ItemStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left" SortExpression="DefaultProfileName" HeaderText="Profil <br> par d�faut">
                            <HeaderStyle CssClass="tableHeader" HorizontalAlign="left"/>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="TotalProfiles" ItemStyle-CssClass="tableBody"  ItemStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left" SortExpression="TotalProfiles" HeaderText="Total des <br>Profils">
                            <HeaderStyle CssClass="tableHeader" HorizontalAlign="left"/>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="VideoEquipment" ItemStyle-CssClass="tableBody"  ItemStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left" SortExpression="VideoEquipment" HeaderText="Mod�le <br> Endpoint">
                            <HeaderStyle CssClass="tableHeader" HorizontalAlign="left"/>
                        </asp:BoundColumn>
                         <asp:BoundColumn DataField="DefaultProtocol" ItemStyle-CssClass="tableBody"  ItemStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left" SortExpression="DefaultProtocol" HeaderText="protocole<br>vid�o">
                            <HeaderStyle CssClass="tableHeader" HorizontalAlign="left"/>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="AddressType"  ItemStyle-CssClass="tableBody" ItemStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left" SortExpression="AddressType" HeaderText="Address<br>Type">
                            <HeaderStyle CssClass="tableHeader" HorizontalAlign="left"/>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="Address"  ItemStyle-CssClass="tableBody" SortExpression="Adresse" HeaderText="Adresse" ItemStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left">
                            <HeaderStyle CssClass="tableHeader" HorizontalAlign="left"/>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="IsOutside"  ItemStyle-CssClass="tableBody" SortExpression="IsOutside" HeaderText="En dehors du<br>r�seau" ItemStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left">
                            <HeaderStyle CssClass="tableHeader" HorizontalAlign="left"/>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="ConnectionType"  ItemStyle-CssClass="tableBody"  SortExpression="ConnectionType" HeaderText="Composition<br>option de" ItemStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left">
                            <HeaderStyle CssClass="tableHeader" HorizontalAlign="left"/>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="LineRate"  ItemStyle-CssClass="tableBody" SortExpression="LineRate" HeaderText="Bande passante" ItemStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left">
                            <HeaderStyle CssClass="tableHeader" HorizontalAlign="left"/>
                        </asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="Assigned<br>MCU"  ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" > 
                        <ItemTemplate>
                            <asp:Label ID="lblBridgeID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Bridge")%>' Visible="false"></asp:Label>
                            <asp:Label ID="lblBridgeName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BridgeName")%>'></asp:Label>
                            <br /><asp:LinkButton Text="Visualisation D�tails" runat="server" ID="btnViewBridgeDetails" Visible='<%# !DataBinder.Eval(Container, "DataItem.BridgeName").ToString().Equals("") %>'></asp:LinkButton>
                        </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Profiles" Visible="false"  ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:DropDownList CssClass="altLong4SelectFormat" runat="server" ID="lstProfiles" DataTextField="ProfileName" DataValueField="ProfileID" Visible='<%#Request.QueryString["t"].ToUpper().Equals("TC") %>'></asp:DropDownList><%-- SelectedValue='<%#DataBinder.Eval(Container, "DataItem.DefaultProfileID") %>' --%>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Button ID="btnSubmitNew" runat="server" CssClass="altLongBlueButtonFormat" Visible="true" Text="Soumettre" OnClick="CreateNewEndpoint" />
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn Visible="false" ItemStyle-CssClass="tableBody" >
                            <ItemTemplate>
                                <asp:TextBox ID="txtProfilesXML" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ProfilesXML") %>'></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="ProfilesXML" Visible="false" ItemStyle-CssClass="tableBody" ></asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="Actions" ItemStyle-CssClass="tableBody" >
                            <HeaderStyle CssClass="tableHeader" />
                            <ItemTemplate>
                                <asp:LinkButton Text="�diter" runat="server" ID="btnEdit" CommandName="Edit"></asp:LinkButton>&nbsp;&nbsp;
                                <asp:LinkButton Text="Supprimer" runat="server" ID="btnDelete" CommandName="Update" Visible="true"></asp:LinkButton>&nbsp;&nbsp;
                            </ItemTemplate>
                            <FooterTemplate>
                                <b><span class="blackblodtext"> Total des points de Branchment: </span> <asp:Label ID="lblTotalRecords" runat="server" Text=""></asp:Label> </b>
                                <br>
				     <%--Added for Licence Modification START--%>                                
				    <b><span class="blackblodtext">Licenses&nbsp;Remaining:&nbsp;</span><asp:Label ID="lblRemaining" runat="server" Text=""></asp:Label> </b>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
                    <asp:Label ID="lblNoEndpoints" runat="server" Text="No Endpoints Found." Visible="False" CssClass="lblError"></asp:Label>
                </td>
            </tr>
            <tr style="display:none">
                <td>
                    <asp:Table ID="tblPage" Visible="false" runat="server">
                        <asp:TableRow ID="TableRow1" runat="server">
                            <asp:TableCell ID="TableCell1" Font-Bold="True" Font-Names="Verdana" Font-Size="Small" ForeColor="Blue" runat="server">Pages: </asp:TableCell>
                            <asp:TableCell ID="TableCell2" runat="server"></asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>

                </td>
            </tr>
            <tr style="display:none">
                <td align="Left">
                    <table>
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <SPAN class=subtitleblueblodtext>Recherches Points de connexion</SPAN>
                            </td>
                        </tr>
                    </table>
                </td>
             </tr>
             <tr style="display:none">
                <td align="Left">
                    <table width="95%">
                        <tr>
                            <%--Window Dressing - Start --%>
                            <td align=right class="blackblodtext"><b>Nom de point de connexion</b></td>
                            <td>
                                <asp:TextBox ID="txtEndpointName" runat="server" CssClass="altText" Text="" ValidationGroup="Search" ></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator4" ControlToValidate="txtEndpointName" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ et &#34; sont des characteres invalides." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=:@^#$%&()'~]*$"></asp:RegularExpressionValidator>
                            </td>
                            <td align=right class="blackblodtext"><b>Genre de point de connection</b></td>
                            <td>
                                <asp:DropDownList ID="lstAddressType" runat="server" CssClass="altText" DataTextField="Name" DataValueField="ID"></asp:DropDownList>
                                <asp:DropDownList ID="lstBridges" Visible="false" runat="server" CssClass="altText" DataTextField="BridgeName" DataValueField="BridgeID"></asp:DropDownList>
                                <asp:DropDownList ID="lstLineRate" Visible="false" runat="server" CssClass="altText" DataTextField="LineRateName" DataValueField="LineRateID"></asp:DropDownList>
                                <asp:DropDownList ID="lstVideoEquipment" Visible="false" runat="server" CssClass="altText" DataTextField="VideoEquipmentName" DataValueField="VideoEquipmentID"></asp:DropDownList>
                                <asp:DropDownList ID="lstVideoProtocol" Visible="false" runat="server" CssClass="altText" DataTextField="Name" DataValueField="ID"></asp:DropDownList>
                                <asp:DropDownList ID="lstConnectionType" runat="server" DataTextField="Name" DataValueField="ID" Visible="false"></asp:DropDownList>                                 <asp:DropDownList ID="DropDownList1" runat="server" DataTextField="Name" DataValueField="ID" Visible="false"></asp:DropDownList> <%--Fogbugz case 427--%>
                            </td>
                            <%--Window Dressing - End --%>
                        </tr>
                        <tr>
                            <td colspan="4" align="right">
                                <asp:Button runat="server" CssClass="altLongBlueButtonFormat" Text="Soumettre" ValidationGroup="Search" OnClick="SearchEndpoint" />
                            </td>
                        </tr>
                    </table>
                </td>
             </tr>
            <tr>
                <td align="Left">
                    <table>
                        <tr id="trNew" runat="server">
                            <td>&nbsp;</td>
                            <td>
                                <%--<SPAN class=subtitleblueblodtext>Cr�er un nouveau point de connexion <asp:Label ID="lblAddSelection" Text=" ou Choose Selected" Visible="false" runat="server"></asp:Label></SPAN>--%><%--Commented for FB 2094--%>  
                                <SPAN class=subtitleblueblodtext><asp:Label ID="Label1" Text=" ou Choose Selected" Visible="false" runat="server" ></asp:Label></SPAN><%--FB 2094--%> 
                            </td>
                        </tr>
                    </table>
                </td>
             </tr>
            <tr>
                <td align="Left">
                    <table width="95%">
                        <tr>
                            <td align="right">
                                <asp:Button Width="250" ID="btnSubmit" runat="server" CssClass="altLongBlueButtonFormat" Text="Cr�er un nouveau point de connexion" OnClick="CreateNewEndpoint" /><%--FB 2094--%> 
                            </td>
                        </tr>

                    </table>
                </td>
             </tr>
        </table>
            <asp:TextBox Visible="false" ID="txtEndpointID" runat="server"></asp:TextBox>
        </center>
    </form>
</body>
</html>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->


<%@ Page Language="C#" AutoEventWireup="true" Inherits="ns_OrgProfile.ManageOrganizationProfile" Buffer="true" ValidateRequest="false" %>
<meta http-equiv="X-UA-Compatible" content="IE=7" /> <!-- FB 2050 -->
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
<script type="text/javascript" src="inc/functions.js"></script>
<html xmlns="http://www.w3.org/1999/xhtml">
<script type="text/javascript" language="javascript">
function fnCheckOrg()
{
    var txtorgname = document.getElementById('<%=txtOrgName.ClientID%>');
    var reqOrgName = document.getElementById('<%=reqOrgName.ClientID%>');
    var regOrgName = document.getElementById('<%=regOrgName.ClientID%>');
    var txtzipcode = document.getElementById('<%=txtZipCode.ClientID%>');
    var regZipCode = document.getElementById('<%=regZipCode.ClientID%>');
    var txtPhone = document.getElementById('<%=txtPhoneNumber.ClientID%>');
    var regPhone = document.getElementById('<%=regPhone.ClientID%>');
    var txtfaxNumber = document.getElementById('<%=txtFaxNumber.ClientID%>');
    var regFaxNumber = document.getElementById('<%=regFaxNumber.ClientID%>');
    var txtemailID = document.getElementById('<%=txtEmailID.ClientID%>');
    var regEmailID = document.getElementById('<%=regEmailID.ClientID%>');
    var txtorgwebsite = document.getElementById('<%=txtOrgWebsite.ClientID%>');
    var regWebsite = document.getElementById('<%=regWebsite.ClientID%>');
    var txtCity = document.getElementById('<%=txtCity.ClientID%>');
    var regCity = document.getElementById('<%=regCity.ClientID%>');
        if(txtorgname.value == '')
        {        
            reqOrgName.style.display = 'block';
            regOrgName.style.display = 'none';
            regZipCode.style.display = 'none';
            regPhone.style.display = 'none';
            regFaxNumber.style.display = 'none';
            regEmailID.style.display = 'none';
            regWebsite.style.display = 'none';
            regCity.style.display = 'none';
            txtorgname.focus();
            return false;
        }
        else if(txtorgname.value != '' && txtorgname.value.search(/^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()'~]*$/)==-1)
        {
            regOrgName.style.display = 'block';
            reqOrgName.style.display = 'none';
            regZipCode.style.display = 'none';
            regPhone.style.display = 'none';
            regFaxNumber.style.display = 'none';
            regEmailID.style.display = 'none';
            regWebsite.style.display = 'none';
            regCity.style.display = 'none';
            return false;
        }
        //FB 2222
        if (txtzipcode.value != '' && txtzipcode.value.search(/^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()'~]*$/)==-1)
        {        
            regZipCode.style.display = 'block';
            regOrgName.style.display = 'none';
            reqOrgName.style.display = 'none';
            regPhone.style.display = 'none';
            regFaxNumber.style.display = 'none';
            regEmailID.style.display = 'none';
            regWebsite.style.display = 'none';
            regCity.style.display = 'none';
            txtzipcode.focus();
            return false;
        }  
        
        if(txtPhone.value != '' && txtPhone.value.search(/^(\(|\d| |-|\))*$/)==-1)
        {
            regPhone.style.display = 'block';
            regZipCode.style.display = 'none';
            regOrgName.style.display = 'none';
            reqOrgName.style.display = 'none';
            regFaxNumber.style.display = 'none';
            regEmailID.style.display = 'none';
            regWebsite.style.display = 'none';
            regCity.style.display = 'none';
            txtPhone.focus();
            return false;
        }
    
        if(txtfaxNumber.value != '' && txtfaxNumber.value.search(/^(\(|\d| |-|\))*$/)==-1)
        {
            regFaxNumber.style.display = 'block';
            regPhone.style.display = 'none';
            regZipCode.style.display = 'none';
            regOrgName.style.display = 'none';
            reqOrgName.style.display = 'none';
            regEmailID.style.display = 'none';
            regWebsite.style.display = 'none';
            regCity.style.display = 'none';
            txtfaxNumber.focus();
            return false;
        }
    
        if(txtemailID.value != '' && txtemailID.value.search(/^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$/)==-1)
        {
            regEmailID.style.display = 'block';
            regFaxNumber.style.display = 'none';
            regPhone.style.display = 'none';
            regZipCode.style.display = 'none';
            regOrgName.style.display = 'none';
            reqOrgName.style.display = 'none';
            regWebsite.style.display = 'none';
            regCity.style.display = 'none';
            txtemailID.focus();
            return false;
        }
    
        if(txtorgwebsite.value != '' && txtorgwebsite.value.search(/^(a-z|A-Z|0-9)*[^<>+;|!`,\[\]{}\x22;=^@#$%()'~]*$/)==-1)
        {
            regWebsite.style.display = 'block';
            regEmailID.style.display = 'none';
            regFaxNumber.style.display = 'none';
            regPhone.style.display = 'none';
            regZipCode.style.display = 'none';
            regOrgName.style.display = 'none';
            reqOrgName.style.display = 'none';
            regWebsite.style.display = 'none';
            regCity.style.display = 'none';
            txtorgwebsite.focus();
            return false;
        }
        
        if(txtCity.value != '' && txtCity.value.search(/^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()'~]*$/)==-1)
        {
            regWebsite.style.display = 'none';
            regEmailID.style.display = 'none';
            regFaxNumber.style.display = 'none';
            regPhone.style.display = 'none';
            regZipCode.style.display = 'none';
            regOrgName.style.display = 'none';
            reqOrgName.style.display = 'none';
            regWebsite.style.display = 'none';
            regCity.style.display = 'block';
            txtCity.focus();
            return false;
        
        }
        return (true);
}
function fnGoBack()
{
    url = "ManageOrganization.aspx";
    window.location.replace(url);
    return;
}



</script>
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf8" />
    <title>G�rer organisation Profile</title>
    <link rel="stylesheet" title="Expedite base styles" type="text/css" href="<%=Session["OrgCSSPath"]%>" />  
</head>
<body>
    <form id="frmOrgProfile" runat="server" method="post" enctype="multipart/form-data">
    <input type="hidden" runat="server" id="hdnOrganizationID" />
    <asp:TextBox ID="txtOrgID" style="width:0" Height="0" runat="server" BorderStyle="none" BorderWidth="0"></asp:TextBox>
    <div>
        <center>
            <div id="dataLoadingDIV" style="z-index:1"></div>
            <h3>
                <asp:Label ID="lblTitle" runat="server" CssClass="h3" Text=""></asp:Label>        
            </h3>
            <asp:Label ID="errLabel" runat="server" CssClass="lblError"></asp:Label>
        </center>
        <table cellpadding="0" cellspacing="0" border="0" style="width:100%">
            <tr>
                <td align="center">
                   <table id="tblBasicDetails" cellpadding="2" cellspacing="2" border="0" style="width:95%">
                        <tr>
                            <td align="left"><span class="subtitleblueblodtext">D�finition</span></td>
                            <td class="reqfldText" align="center">* Element Obligatoire</td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <table id="tblOrgBasicDetails" cellpadding="2" cellspacing="2" border="0" style="width:95%">
                                    <tr>
                                        <td style="width:15%" align="left" valign="top" class="blackblodtext"><b>Nom</b>
                                        <span class="reqfldText">*</span>
                                        </td>
                                         <%-- FB 1839 Start--%>
                                        <td style="width:25%" align="left" valign="top">
                                            <asp:TextBox ID="txtOrgName" runat="server" CssClass="altText"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="reqOrgName" runat="server" ControlToValidate="txtOrgName" Display="Dynamic" SetFocusOnError="true" Text="Required"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="regOrgName" ControlToValidate="txtOrgName"  Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ et &#34; sont des characteres invalides." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                        </td>
                                        <td style="width:15%" align="left" valign="top" class="blackblodtext"><b>Identit� pour votre email</b></td>
                                        <td style="width:25%" align="left" valign="top">
                                            <asp:TextBox ID="txtEmailID" runat="server" CssClass="altText"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="regEmailID" ControlToValidate="txtEmailID"  runat="server" Display="Dynamic" SetFocusOnError="true" ErrorMessage="Identit� pour votre email should be Example@exp.com format." ValidationExpression="^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"></asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width:15%" align="left" valign="top" class="blackblodtext"><b>Adresse 1</b></td>
                                        <td style="width:25%" align="left" valign="top">
                                            <asp:TextBox ID="txtAddress1" runat="server" TextMode="MultiLine" CssClass="altText"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" ControlToValidate="txtAddress1"  Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ et &#34; sont des characteres invalides." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@$%&()'~]*$"></asp:RegularExpressionValidator>
                                        </td>
                                        <td style="width:15%" align="left" valign="top" class="blackblodtext"><b>site web</b></td>
                                        <td style="width:25%" align="left" valign="top">
                                            <asp:TextBox ID="txtOrgWebsite" runat="server" CssClass="altText"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="regWebsite" runat="server" ControlToValidate="txtOrgWebsite" Display="Dynamic" SetFocusOnError="true"  ErrorMessage="<br>< > ' + % ( ) ;  | ^ = ! ` , [ ] { } # $ @ ~ et &#34; sont des characteres invalides." ValidationExpression="^(a-z|A-Z|0-9)*[^<>+;|!`,\[\]{}\x22;=^@#$%()'~]*$"></asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width:15%" align="left" valign="top" class="blackblodtext"><b>Adresse 2</b></td>
                                        <td style="width:25%" align="left" valign="top">
                                            <asp:TextBox ID="txtAddress2" runat="server" TextMode="MultiLine" CssClass="altText"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txtAddress2"  Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ et &#34; sont des characteres invalides." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@$%&()'~]*$"></asp:RegularExpressionValidator>
                                        </td>
                                        <td style="width:15%" align="left" valign="top" class="blackblodtext"><b>Num�ro de T�l�phone</b></td>
                                        <td style="width:25%" align="left" valign="top">
                                            <asp:TextBox ID="txtPhoneNumber" runat="server" CssClass="altText"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="regPhone" runat="server" ControlToValidate="txtPhoneNumber"  Display="Dynamic" SetFocusOnError="true" ErrorMessage="Enter Numbers Only" ValidationExpression="^(\(|\d| |-|\))*$"></asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width:15%" align="left" valign="top" class="blackblodtext"><b>Ville</b></td>
                                        <td style="width:25%" align="left" valign="top">
                                            <asp:TextBox ID="txtCity" runat="server" CssClass="altText"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="regCity" ControlToValidate="txtCity" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()'~]*$"  Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ et &#34; sont des characteres invalides."></asp:RegularExpressionValidator>
                                        </td>
                                        <td style="width:15%" align="left" valign="top" class="blackblodtext"><b>Num�ro de t�l�copieur</b></td>
                                        <td style="width:25%" align="left" valign="top">
                                            <asp:TextBox ID="txtFaxNumber" runat="server" CssClass="altText"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="regFaxNumber" runat="server" ControlToValidate="txtFaxNumber" Display="Dynamic" ErrorMessage="Entr�e de chiffres seulement." SetFocusOnError="true"  ValidationExpression="^(\(|\d| |-|\))*$"></asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width:15%" align="left" valign="top" class="blackblodtext"><b>Pays</b></td>
                                        <td style="width:25%" align="left" valign="top">
                                            <asp:DropDownList ID="lstCountries" runat="server" CssClass="altText" DataTextField="Name" DataValueField="ID" OnSelectedIndexChanged="UpdateStates" AutoPostBack="true"></asp:DropDownList><br />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width:15%" align="left" valign="top" class="blackblodtext"><b>R�gion / Code postal</b></td>
                                        <td style="width:25%" align="left" valign="top">
                                            <asp:DropDownList ID="lstStates" runat="server" CssClass="altText"  DataTextField="Code" DataValueField="ID"></asp:DropDownList>
                                            <asp:TextBox ID="txtZipCode" Width="50" runat="server" CssClass="altText"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="regZipCode" ControlToValidate="txtZipCode" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br> & < > ' + % \ ; ? | ^ = ! ` [ ] { } : # $ @ ~ et &#34; sont des characteres invalides."  ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>^+;?|!`\[\]{}\x22;=:@#$%&'~]*$"></asp:RegularExpressionValidator><%--FB 2222--%>
                                        </td>
                                    </tr>
                                    <%-- FB 1839 End--%>
                                    <tr>
                                        <td align="left" class="subtitleblueblodtext" colspan="4" style="height:21;font-weight:bold">
                                            organisation Resources
                                        </td>
                                    </tr>
                                    <tr style="display:none">
                                        <td style="width:15%" align="left" valign="top" class="blackblodtext"><b>Active salle de conf�rences</b></td>
                                        <td style="width:25%" align="left" valign="top">
                                            <asp:TextBox ID="TxtRooms" runat="server" CssClass="altText" Width="50" ></asp:TextBox>&nbsp;&nbsp;<span id="SpanActiveRooms" runat="server" class="orangesboldtext"></span>
                                            <asp:RegularExpressionValidator ID="regRooms" ControlToValidate="TxtRooms" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="Entr�e de chiffres seulement."  ValidationExpression="^(\(|\d|\))*$"></asp:RegularExpressionValidator>
                                            <input type="hidden" runat="server" id="hdnRooms" />
                                        </td>
                                        <td style="width:15%" align="left" valign="top" class="blackblodtext">&nbsp;</td>
                                        <td style="width:25%" align="left" valign="top">&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width:15%" align="left" valign="top" class="blackblodtext"><b>Active Salles de conf�rence avec video</b></td>
                                        <td style="width:30%" align="left" valign="top"> <%--Edited for FF--%>
                                            <asp:TextBox ID="TxtVRooms" runat="server" CssClass="altText" Width="50" ></asp:TextBox>&nbsp;&nbsp;<span id="SpanActiveVRooms" runat="server" class="orangesboldtext"></span>
                                            <asp:RegularExpressionValidator ID="regVRooms" ControlToValidate="TxtVRooms" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="Entr�e de chiffres seulement."  ValidationExpression="^(\(|\d|\))*$"></asp:RegularExpressionValidator>
                                            <input type="hidden" runat="server" id="hdnVRooms" />
                                        </td>
                                        
                                        <td style="width:15%" align="left" valign="top" class="blackblodtext"><b>Modules de commodit�s</b></td>
                                        <td style="width:25%" align="left" valign="top">
                                            <asp:CheckBox ID="ChkFacility" runat="server" />&nbsp;&nbsp;<span id="SpanActiveFacility" runat="server" class="orangesboldtext"></span>
                                            <input type="hidden" runat="server" id="hdnFacility" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width:15%" align="left" valign="top" class="blackblodtext"><b>Active Salle de conf�rence sans vid�o</b></td>
                                        <td style="width:25%" align="left" valign="top">
                                            <asp:TextBox ID="TxtNVRooms" runat="server" CssClass="altText" Width="50" ></asp:TextBox>&nbsp;&nbsp;<span id="SpanActiveNVRooms" runat="server" class="orangesboldtext"></span>
                                            <asp:RegularExpressionValidator ID="regNVRooms" ControlToValidate="TxtNVRooms" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="Entr�e de chiffres seulement."  ValidationExpression="^(\(|\d|\))*$"></asp:RegularExpressionValidator>
                                            <input type="hidden" runat="server" id="hdnNVRooms" />
                                        </td>
                                        <td style="width:15%" align="left" valign="top" class="blackblodtext"><b>Restauration Module</b></td>
                                        <td style="width:25%" align="left" valign="top">
                                            <asp:CheckBox ID="ChkCatering" runat="server" />&nbsp;&nbsp;<span id="SpanActiveCat" runat="server" class="orangesboldtext"></span>
                                            <input type="hidden" runat="server" id="hdnCatering" />
                                        </td>
                                    </tr>
                                    <tr><%--FB 2586--%>
                                    <td style="width:15%" align="left" valign="top" class="blackblodtext"><b>VMR Rooms</b></td>
                                        <td style="width:25%" align="left" valign="top">
                                            <asp:TextBox ID="TxtVMR" runat="server" CssClass="altText"  Width="50" ></asp:TextBox>&nbsp;&nbsp;<span id="SpanVMRRooms" runat="server" class="orangesboldtext"></span>
                                            <asp:RegularExpressionValidator ID="regVMR" ControlToValidate="TxtVMR" Display="dynamic" runat="server"   SetFocusOnError="true" ErrorMessage="Enter Numbers only." ValidationExpression="^(\(|\d|\))*$"></asp:RegularExpressionValidator>
                                            <input type="hidden" runat="server" id="hdnVMR" />
                                        </td>
                                       <%--Edited for FB 1706--%>
                                        <td style="width:15%" align="left" valign="top" class="blackblodtext"><b>Travaux M�nag�s Module</b></td>
                                        <td style="width:25%" align="left" valign="top">
                                            <asp:CheckBox ID="ChkHK" runat="server" />&nbsp;&nbsp;<span id="SpanActiveHK" runat="server" class="orangesboldtext"></span>
                                            <input type="hidden" runat="server" id="hdnHK" />  
                                        </td>
                                    </tr>
                                    <%--FB 2486--%>
                                    <tr>
                                     <td style="width:15%" align="left" valign="top" class="blackblodtext"><b>norme MCU</b></td>
                                        <td style="width:25%" align="left" valign="top">
                                            <asp:TextBox ID="TxtMCU" runat="server" CssClass="altText"  Width="50" ></asp:TextBox>&nbsp;&nbsp;<span id="SpanActiveMCU" runat="server" class="orangesboldtext"></span>
                                            <asp:RegularExpressionValidator ID="regMcu" ControlToValidate="TxtMCU" Display="dynamic" runat="server"   SetFocusOnError="true" ErrorMessage="Entr�e de chiffres seulement." ValidationExpression="^(\(|\d|\))*$"></asp:RegularExpressionValidator>
                                            <input type="hidden" runat="server" id="hdnMCU" />
                                        </td>
                                        <td style="width:15%" align="left" valign="top" class="blackblodtext"><b>Module API</b></td>
                                        <td style="width:25%" align="left" valign="top">
                                            <asp:CheckBox ID="ChkAPI" runat="server" />&nbsp;&nbsp;<span id="SpanActiveAPI" runat="server" class="orangesboldtext"></span>
                                            <input type="hidden" runat="server" id="hdnAPI" />
                                        </td>
                                    </tr>
                                    <tr>
                                    <td style="width:15%" align="left" valign="top" class="blackblodtext"><b>Am�lioration de MCU</b></td>
                                        <td style="width:25%" align="left" valign="top">
                                            <asp:TextBox ID="TxtMCUEncha" runat="server" CssClass="altText"  Width="50" ></asp:TextBox>&nbsp;&nbsp;<span id="SpanActiveMCUEnchanced" runat="server" class="orangesboldtext"></span>
                                            <asp:RegularExpressionValidator ID="regMcuencha" ControlToValidate="TxtMCUEncha" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="Enter Numbers only." ValidationExpression="^(\(|\d|\))*$"></asp:RegularExpressionValidator>
                                            <input type="hidden" runat="server" id="hdnMCUEncha" />
                                        </td>
                                         <td style="width:15%" align="left" valign="top" class="blackblodtext"><b>Module PC</b></td> <%--FB 2347--%>
                                        <td style="width:25%" align="left" valign="top">
                                            <asp:CheckBox ID="ChkPC" runat="server" />&nbsp;&nbsp;<span id="SpanActivePC" runat="server" class="orangesboldtext"></span>
                                            <input type="hidden" runat="server" id="hdnPC" />
                                        </td>
                                    </tr>
                                    <tr>
                                    <td style="width:15%" align="left" valign="top" class="blackblodtext"><b>Points de Branchment</b></td>
                                        <td style="width:25%" align="left" valign="top">
                                            <asp:TextBox ID="TxtEndPoint" runat="server" CssClass="altText"  Width="50" ></asp:TextBox>&nbsp;&nbsp;<span id="SpanActiveEpts" runat="server" class="orangesboldtext"></span>
                                            <asp:RegularExpressionValidator ID="regEndPoint" ControlToValidate="TxtEndPoint" Display="dynamic" runat="server"   SetFocusOnError="true" ErrorMessage="Entr�e de chiffres seulement." ValidationExpression="^(\(|\d|\))*$"></asp:RegularExpressionValidator>
                                            <input type="hidden" runat="server" id="hdnEndPoint" />
                                        </td>
                                        <%--FB 2594 Starts--%> 
                                        <td style="width:15%;" align="left" valign="top" class="blackblodtext" runat="server" id ="tdPublicRoom"><b>Public Room Service</b></td> 
                                        <td style="width:25%; " align="left" valign="top" runat ="server" id="tdChkPublicRoom">
                                            <asp:CheckBox ID="ChkPublicRoom" runat="server" />&nbsp;&nbsp;<span id="SpanActivePublicRoom" runat="server" class="orangesboldtext"></span>
                                        </td>
                                        <%--FB 2594 Ends--%> 
                                    </tr>
                                    <tr>
                                    <td style="width:15%" align="left" valign="top" class="blackblodtext"><b>Utilisateur Actif</b></td>
                                        <td style="width:25%" align="left" valign="top" > <%--FB 2347--%>
                                            <asp:TextBox ID="TxtUsers" runat="server" CssClass="altText"  Width="50" ></asp:TextBox>&nbsp;&nbsp;<span id="SpanActiveUsers" runat="server" class="orangesboldtext"></span>
                                            <asp:RegularExpressionValidator ID="regUsers" ControlToValidate="TxtUsers"   Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="Entr�e de chiffres seulement." ValidationExpression="^(\(|\d|\))*$"></asp:RegularExpressionValidator>
                                            <input type="hidden" runat="server" id="hdnUsers" />
                                        </td>
                                        <td id="tdcloud" runat="server" style="width:15%" align="left" valign="top" class="blackblodtext"><b>Cloud</b></td> <%--FB 2262 //FB 2599--%>
                                        <td id="tdChkCloud" runat="server" style="width:25%" align="left" valign="top">
                                            <asp:CheckBox ID="ChkCloud" runat="server" />&nbsp;&nbsp;<span id="SpanActiveCloud" runat="server" class="orangesboldtext"></span>
                                            <input type="hidden" runat="server" id="hdnCloud" />
                                        </td>
                                    </tr>
                                    <tr>
                                    <td style="width:15%" align="left" valign="top" class="blackblodtext"><b>�change d'utilisateur</b></td>
                                        <td style="width:25%" align="left" valign="top">
                                            <asp:TextBox ID="TxtExUsers" runat="server" CssClass="altText"  Width="50" ></asp:TextBox>&nbsp;&nbsp;<span id="SpanActiveExUsers" runat="server" class="orangesboldtext"></span>
                                            <asp:RegularExpressionValidator ID="regExcUsers" ControlToValidate="TxtExUsers" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="Entr�e de chiffres seulement." ValidationExpression="^(\(|\d|\))*$"></asp:RegularExpressionValidator>
                                            <input type="hidden" runat="server" id="hdnEUsers" />
                                        </td>
                                        </tr>
                                    <tr>
                                        <td style="width:15%" align="left" valign="top" class="blackblodtext"><b>Utilizateur de Domino</b></td>
                                        <td style="width:25%" align="left" valign="top" colspan="3">
                                            <asp:TextBox ID="TxtDomUsers" runat="server" CssClass="altText"  Width="50" ></asp:TextBox>&nbsp;&nbsp;<span id="SpanActiveDomUsers" runat="server" class="orangesboldtext"></span>
                                            <asp:RegularExpressionValidator ID="regDomUsers" ControlToValidate="TxtDomUsers" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="Entr�e de chiffres seulement." ValidationExpression="^(\(|\d|\))*$"></asp:RegularExpressionValidator>
                                            <input type="hidden" runat="server" id="hdnDUsers" />
                                        </td>
                                    </tr>
                                    <tr> <%--FB 1979--%>
                                        <td style="width:15%" align="left" valign="top" class="blackblodtext"><b>Les utilisateurs mobiles</b></td>
                                        <td style="width:25%" align="left" valign="top" colspan="3">
                                            <asp:TextBox ID="TxtMobUsers" runat="server" CssClass="altText"  Width="50" ></asp:TextBox>&nbsp;&nbsp;<span id="SpanActiveMobUsers" runat="server" class="orangesboldtext"></span>
                                            <asp:RegularExpressionValidator ID="regMobUsers" ControlToValidate="TxtMobUsers" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="Entr�e de chiffres seulement." ValidationExpression="^(\(|\d|\))*$"></asp:RegularExpressionValidator>
                                            <input type="hidden" runat="server" id="hdnMUsers" />
                                        </td>
                                    </tr>
                                    <%--FB 2426 Start--%>
                                     <tr> 
                                        <td style="width:15%" align="left" valign="top" class="blackblodtext"><b>Les chambres</b></td>
                                        <td style="width:25%" align="left" valign="top" colspan="3">
                                            <asp:TextBox ID="TxtExtRooms" runat="server" CssClass="altText"  Width="50" ></asp:TextBox>&nbsp;&nbsp;<span id="SpanExternalRooms" runat="server" class="orangesboldtext"></span>
                                            <asp:RegularExpressionValidator ID="regExtRooms" ControlToValidate="TxtExtRooms" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="Entr�e de chiffres seulement." ValidationExpression="^(\(|\d|\))*$"></asp:RegularExpressionValidator>
                                            <input type="hidden" runat="server" id="hdnExtRooms" />
                                        </td>
                                        </tr>
                                        <tr>
                                        <td style="width:15%" align="left" valign="top" class="blackblodtext"><b>Chambre d'h�tes par utilisateur</b></td>
                                        <td style="width:25%" align="left" valign="top" colspan="3">
                                            <asp:TextBox ID="TxtGstPerUser" runat="server" CssClass="altText"  Width="50" ></asp:TextBox>&nbsp;&nbsp;<span id="Span1" runat="server" class="orangesboldtext"></span>
                                            <asp:RegularExpressionValidator ID="RegGstPerUser" ControlToValidate="TxtGstPerUser" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="Entr�e de chiffres seulement." ValidationExpression="^(\(|\d|\))*$"></asp:RegularExpressionValidator>
                                            <input type="hidden" runat="server" id="hdnGstPerUser" />
                                        </td>
                                    </tr>
                                    <%--FB 2426 End--%>
                                </table>
                            </td>
                        </tr>
                   </table> 
                </td>
            </tr>
            
            <tr>
                <td style="height:60px">&nbsp;</td>
            </tr>
            <tr>
                <td align="center">
                    <table id="tblButtons" cellpadding="2" cellspacing="2" style="width:90%">
                        <tr>
                            <td align="center" style="width:33%">
                                <asp:Button ID="btnReset" runat="server" Text="Recommencer" CssClass="altLongBlueButtonFormat" OnClick="ResetOrganizationProfile" />
                            </td>
                            <td align="center" style="width:33%">
                                <input name="Go"  type="button" class="altLongBlueButtonFormat" value=" Retourner " onclick="javascript:return fnGoBack();" /> 
                            </td>
                            <td align="center" style="width:33%">
                                <asp:Button ID="btnSubmitAddNew" runat="server" Width="250px"  Text="Soumettre / Nouveau Organizaion" CssClass="altLongBlueButtonFormat" OnClientClick="javascript:return fnCheckOrg();" OnClick="SubmitAddNewOrganizationProfile" />
                            </td> 
                            <td align="center" style="width:33%">
                                <asp:Button ID="btnSubmit"  runat="server" Text="Soumettre" CssClass="altLongBlueButtonFormat" OnClick="SubmitOrganizationProfile" OnClientClick="javascript:return fnCheckOrg();" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
<script type="text/javascript" src="inc/softedge.js"></script>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" --> 


<%@ Page Language="C#" AutoEventWireup="true" Inherits="en_recurNET" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Assembly="MetaBuilders.WebControls.ComboBox" Namespace="MetaBuilders.WebControls"
    TagPrefix="mbcbb" %>

<script type="text/javascript" language="JavaScript" src="../en/inc/functions.js"></script>
<script type="text/javascript" language="JavaScript" src="script/errorList.js"></script>
<script type="text/javascript" language="javascript1.1" src="extract.js"></script>

<script type="text/javascript">
  var servertoday = new Date();
  var dFormat;
    dFormat = "<%=format %>";
    
var maxRecurrence = ' 24 instances for Monthly and  104 for weekly or equivalent';
  
var servertoday = new Date(parseInt("<%=DateTime.Today.Year%>", 10), parseInt("<%=DateTime.Today.Month%>", 10)-1, parseInt("<%=DateTime.Today.Day%>", 10),
        parseInt("<%=DateTime.Today.Hour%>", 10), parseInt("<%=DateTime.Today.Minute%>", 10), parseInt("<%=DateTime.Today.Second%>", 10)); 
 
 var  maxDuration = 24;
 if('<%=Application["MaxConferenceDurationInHours"] %>' != '')
    maxDuration = parseInt('<%=Application["MaxConferenceDurationInHours"] %>',10);  

</script>

<script type="text/javascript" src="script/cal-flat.js"></script>
<script type="text/javascript" src="script/calview.js"></script>
<script type="text/javascript" src="lang/calendar-en.js"></script>
<script type="text/javascript" src="script/calendar-setup.js"></script>
<script type="text/javascript" src="script/settings2.js"></script>
<script type="text/javascript" src="script/calendar-flat-setup.js"></script>

<script type="text/javascript">

var openerfrm = eval('opener.document.' + queryField("frm") );
function CheckDate(obj)
{    
    var endDate;//FB 2246
    if ('<%=format%>'=='dd/MM/yyyy')
        endDate = GetDefaultDate(obj.value,'dd/MM/yyyy');
    else
        endDate = obj.value;
        
    if (Date.parse(endDate) < Date.parse(new Date()))
        alert("Invalid Date");
}

function fnShow()
{
    if(rpstr != "")
    {    
        var rpstrSplit = rpstr.split("&");
        
        if(document.getElementById("daySel" + rpstrSplit[0]))
            document.getElementById("daySel" + rpstrSplit[0]).checked = true;  
        
        if(document.getElementById("selRecur" + rpstrSplit[1]))
            document.getElementById("selRecur" + rpstrSplit[1]).checked = true;            
        
        if(document.getElementById("selDay" + rpstrSplit[1]))
        {
            if(rpstrSplit[2] != "-1")
            document.getElementById("selDay" + rpstrSplit[1]).value = rpstrSplit[2];
        }
    }
}

function fnDefault()
{
    var recPattern = 5;
    for(var r =1; r<= recPattern;r++)
    {
        if(document.getElementById("selDay"+ r))
        {
             var opt = document.getElementById("selDay" + r);
	            if(opt != null)
	                opt.value = 1;
        }
    }
}

function initial()
{
    if(openerfrm.chkEnableBuffer.checked)
    {
        document.getElementById("NONRecurringConferenceDiv2").style.display = '';
        document.getElementById("NONRecurringConferenceDiv3").style.display = '';
    }
    else
    {
        document.getElementById("NONRecurringConferenceDiv2").style.display = 'None';
        document.getElementById("NONRecurringConferenceDiv3").style.display = 'None';
    }
    
	if (openerfrm.RecurSpec.value=="") {
		timeMin = openerfrm.confStartTime_Text.value.split(":")[1].split(" ")[0] ; 
		//timeDur = getConfDurNET(openerfrm);		
		
		sdt = GetDefaultDate(openerfrm.confStartDate.value,dFormat);
		sdt = new Date(sdt + " " + openerfrm.confStartTime_Text.value);
		
		if(queryField("pn") == "E")
		{
		    edt = GetDefaultDate(openerfrm.TearDownDate.value,dFormat);
		    edt = new Date(edt + " " + openerfrm.TeardownTime_Text.value) ;
		}
		else
		{
		    edt = GetDefaultDate(openerfrm.confEndDate.value,dFormat);
		    edt = new Date(edt + " " + openerfrm.confEndTime_Text.value) ;
		}
		
		var oDiff = get_time_difference(sdt,edt);
		var timeDur = oDiff.duration;
		
        if (oDiff.duration > (maxDuration * 60))
            alert(EN_314);
         
         /* *** code changed for buffer zone *** --Start */
		if('<%=Session["timeFormat"].ToString()%>' == '0')
		    tmpstr = openerfrm.lstConferenceTZ.options[openerfrm.lstConferenceTZ.selectedIndex].value + "&" + openerfrm.confStartTime_Text.value.split(":")[0] + "&" + timeMin + "&-1&" + timeDur + "#"
        else
		    tmpstr = openerfrm.lstConferenceTZ.options[openerfrm.lstConferenceTZ.selectedIndex].value + "&" + openerfrm.confStartTime_Text.value.split(":")[0] + "&" + timeMin + "&" + openerfrm.confStartTime_Text.value.split(" ")[1] + "&" + timeDur + "#"

		tmpstr += "1&1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1#"
		tmpstr += openerfrm.confStartDate.value + "&1&-1&-1";

 		document.frmRecur.RecurValue.value = tmpstr;
		document.frmRecur.RecurPattern.value = "1&1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1";
		
		document.frmRecur.RecurSubmit1.disabled = true;
		
		setuphr = openerfrm.hdnSetupTime.value.split(":")[0].split(" ")[0];
	    setupmin = openerfrm.hdnSetupTime.value.split(":")[1].split(" ")[0];
	    setupap = openerfrm.hdnSetupTime.value.split(":")[1].split(" ")[1];
	    
	    setuptime = openerfrm.hdnSetupTime.value;
	    teardowntime = openerfrm.hdnTeardownTime.value;
	    
	    teardownhr = openerfrm.hdnTeardownTime.value.split(":")[0].split(" ")[0];
	    teardownmin = openerfrm.hdnTeardownTime.value.split(":")[1].split(" ")[0];
	    teardownap = openerfrm.hdnTeardownTime.value.split(":")[1].split(" ")[1];
	    
	    fnDefault();	   
	}
	else
	{
	    setuptime = openerfrm.hdnBufferStr.value.split("&")[0];
	    teardowntime = openerfrm.hdnBufferStr.value.split("&")[1];
	 
	    setuphr = setuptime.split(":")[0].split(" ")[0];
	    setupmin = setuptime.split(":")[1].split(" ")[0];
	    setupap = setuptime.split(":")[1].split(" ")[1];
	    
	    teardownhr = teardowntime.split(":")[0].split(" ")[0];
	    teardownmin = teardowntime.split(":")[1].split(" ")[0];
	    teardownap = teardowntime.split(":")[1].split(" ")[1];
	}
     
	rpstr = AnalyseRecurStr(document.frmRecur.RecurValue.value);	
	
	if(atint[1] < 10 && (atint[1] != "0" || atint[1] != "00"))
	    atint[1] = "0" + atint[1];
	
	if(atint[2] < 10 && (atint[2] != "0" || atint[2] != "00"))
	    atint[2] = "0" + atint[2];
	    
	if('<%=Session["timeFormat"].ToString()%>' == '0')
	    startstr = (atint[1] == "12" && atint[3] == "AM"? "00" : atint[1]) + ":" + (atint[2]=="0" ? "00" : atint[2]);
	else
	    startstr = atint[1] + ":" + (atint[2]=="0" ? "00" : atint[2]) + " " + atint[3];
	
    if(document.getElementById("confStartTime_Text"))
	    document.getElementById("confStartTime_Text").value = startstr;
	    
	if(document.getElementById("SetupTime_Text"))
	    document.getElementById("SetupTime_Text").value = setuptime;
	    
	if(document.getElementById("TeardownTime_Text"))
	    document.getElementById("TeardownTime_Text").value = teardowntime;
    
	if (document.frmRecur.RecurDurationhr) {
		set_select_field (document.frmRecur.RecurDurationhr, parseInt(atint[4]/60, 10) , true);
		set_select_field (document.frmRecur.RecurDurationmi, atint[4]%60, true);
	}

	document.frmRecur.RecurPattern.value = rpstr;	
    document.frmRecur.Occurrence.value ="";
    document.frmRecur.EndDate.value ="";
   
	if (rpint[0] != 5) {
		document.frmRecur.StartDate.value = rrint[0];
	
		switch (rrint[1]) 
		{
			case 1:
		        document.frmRecur.EndType.checked = true;
				break;
			case 2:
		        document.frmRecur.REndAfter.checked = true;
				document.frmRecur.Occurrence.value = rrint[2];
				break;
			case 3:
		        document.frmRecur.REndBy.checked = true;
				document.frmRecur.EndDate.value = rrint[3];
				break;
			default:
				alert(EN_36);
				break;
		}
 }

 if (rpint[0] != null)  //FB 2052
 {
    var colorDay = document.frmRecur.rdlstDayColor;   
    if (colorDay)
    {
        for (var c = 0; c < colorDay.length; c++)
        {
            if (colorDay[c].defaultValue == rpint[0])
               colorDay[c].checked = true;
       }
       if (colorDay.length == null)
           if (colorDay.defaultValue == rpint[0])
           colorDay.checked = true;
    }

  }

}

</script>

<%--Submit Recurence --%>

<script type="text/javascript">

function SubmitRecurrence()
{
    if (validateConfDuration())
    {
        if(summary())
            return true;
    }
    return false;        
}

function validateDurationHr()
{
    var obj = document.getElementById("RecurDurationhr");
    
    if (obj.value == "") 
         obj.value = "0";
         
    if (isNaN(obj.value)) 
    {
        alert(EN_314);
        return false;
    }

    var maxdur = '<%= Application["MaxConferenceDurationInHours"] %>';
    
    if(maxdur == "")
        maxdur = "24"; 
    
    if (obj.value < 0 || eval(obj.value) > eval(maxdur))
    {
        alert(EN_314);
        return false;
    }
    return true;
}

function validateDurationMi()
{
    var obj = document.getElementById("RecurDurationmi");
    if (obj.value == "") 
        obj.value = "0";
    if (isNaN(obj.value))
    {
        alert(EN_314);
        obj.focus();
        return false;
    }
    return true;
}

function validateConfDuration()
{
    var obj = document.getElementById("RecurDurationhr");
    var obj1 = document.getElementById("RecurDurationmi");
    
    if (obj.value == "")
         obj.value = "0";
         
    if (obj1.value == "") 
        obj1.value = "0";
         
    if (isNaN(obj.value)) 
    {
        alert(EN_314);
        return false;
    }
    if (isNaN(obj1.value)) 
    {
        alert(EN_314);
        return false;
    }
    
    var maxdur = '<%= Application["MaxConferenceDurationInHours"] %>';
    if(maxdur == "")
        maxdur = "24";

    var totDur = parseInt(obj.value) * 60;
    totDur = totDur + parseInt(obj1.value);
    
    if (totDur < 0 || totDur > eval(maxdur*60))
    {
        alert(EN_314);
        return false;
    }
  
    return true;
}

function summary()
{
    var rp = "";
    var recPattern = 8; //FB 1911
    //FB 2052  start
    var colorDay = document.frmRecur.rdlstDayColor;
    if (!colorDay)
    {
        alert("Special recurrence must contain at least one color day.");
        document.frmRecur.Occurrence.focus();
        return false;
    }
    
    for (var c = 0; c < colorDay.length; c++)
    {
        if (colorDay[c].checked)
           document.frmRecur.hdnColor.value = colorDay[c].defaultValue;
   }
   
   if (colorDay.length == null)
       if (colorDay.checked)
           document.frmRecur.hdnColor.value = colorDay.defaultValue;
       
    //FB 2052 End
    for(var r =1; r<= recPattern;r++)
    {
        if(document.getElementById("selRecur" + r))
            if(document.getElementById("selRecur" + r).checked)
                document.frmRecur.RecPattern.value = r;
    }
    
	rp  = document.frmRecur.hdnColor.value + "&" +  document.frmRecur.RecPattern.value + "&";
	
    if(document.getElementById("selDay"+ document.frmRecur.RecPattern.value))
    {
         var opt = document.getElementById("selDay" + document.frmRecur.RecPattern.value);
		    if(opt != null)
		        rp += opt.value + "&";
    }
    else
        rp += "-1&";

    rp += "-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1";
	document.frmRecur.RecurPattern.value = rp;
	
     if(document.frmRecur.REndAfter.checked)
    {
       if(document.frmRecur.Occurrence.value <= 1)
       {
           alert("A recurring conference must contain at least (2) instances. Please modify R�currence pattern before submitting conference.");
           document.frmRecur.Occurrence.focus();
           return false;   
       }
    }

	var aryStart = document.getElementById("confStartTime_Text").value.split(" ");
	
	sh = aryStart[0].split(":")[0];
	sm = aryStart[0].split(":")[1];
	ss = aryStart[1];
	tz = openerfrm.lstConferenceTZ.options[openerfrm.lstConferenceTZ.selectedIndex].value;
	
	if('<%=Session["timeFormat"].ToString()%>' == '0')
	{
	    if(sh != "")
	    { 
	        if(eval(sh) >= 12)
	            ss = "PM";
	        else
	            ss = "AM";
	    }
	}
	
	var aryStup = document.getElementById("SetupTime_Text").value.split(" ");
	setuphr = aryStup[0].split(":")[0];
	setupmi = aryStup[0].split(":")[1];
	setupap = aryStup[1];

	var aryTear = document.getElementById("TeardownTime_Text").value.split(" ");
	tearhr = aryTear[0].split(":")[0];
	tearmi = aryTear[0].split(":")[1];
	tearap = aryTear[1];
	
    if('<%=Session["timeFormat"].ToString()%>' == '0')
	{ 
	    startTime = sh + ":" + (sm =="0" ? "00" : sm);
	    setupTime = setuphr + ":" + (setupmi =="0" ? "00" : setupmi);
        teardownTime =  tearhr + ":" + (tearmi =="0" ? "00" : tearmi);
    }
	else
	{
	    startTime = sh + ":" + (sm =="0" ? "00" : sm) + " " + ss;
	    setupTime = setuphr + ":" + (setupmi =="0" ? "00" : setupmi) + " " + setupap;
        teardownTime =  tearhr + ":" + (tearmi =="0" ? "00" : tearmi) + " " + tearap;
    }
    
    var insStDate = '';
    insStDate = document.frmRecur.StartDate.value;   
    startdate = insStDate + " " + startTime;
   
    var obj = document.getElementById("RecurDurationhr");
    var obj1 = document.getElementById("RecurDurationmi");
    
    var totDur = parseInt(obj.value) * 60;
    totDur = totDur + parseInt(obj1.value);
    
    if(totDur > 1440)
    {
        alert(EN_314);
        return false;
    }
    
    var confEndDt = '';
    var sysdate = dateAddition(startdate,"m",totDur);
    var dateP = sysdate.getDate();
	
	var monthN = sysdate.getMonth() + 1;
	var yearN = sysdate.getFullYear();
	var hourN = sysdate.getHours();
	var minN = sysdate.getMinutes();
	var secN = sysdate.getSeconds();
	var timset = 'AM';
	
	if('<%=Session["timeFormat"].ToString()%>' == '0')
	    timset = '';
	else
	{
	    if(hourN == 12)
	        timset = 'PM';
	    else if( hourN > 12)
	    {
	         timset = 'PM';
	         hourN = hourN - 12;
	    }
	    else if(hourN == 0)
	    {
	        timset = "AM";
	        hourN = 12;
	    }
	 }
	
	var confDtAlone = monthN + "/" + dateP + "/" + yearN;	
	confEndDt = monthN + "/" + dateP + "/" + yearN + " "+ hourN + ":" + minN + ":" + secN +" "+ timset;
	
    setupDate = insStDate + " " + setupTime;
    if(Date.parse(setupDate) < Date.parse(startdate))
        setupDate = confDtAlone + " " + setupTime;
    
    teardownDate = confDtAlone + " " + teardownTime;
    
    if(Date.parse(confEndDt) < Date.parse(teardownDate))
        teardownDate = insStDate + " " + teardownTime;
        
   if('<%=Application["Client"].ToString().ToUpper()%>' !="MOJ")
     {
        if(Date.parse(setupDate) < Date.parse(startdate))
        {
            alert(EN_316);
            return false;
        }

        if( (Date.parse(teardownDate) < Date.parse(setupDate)) || (Date.parse(teardownDate) < Date.parse(startdate)) || (Date.parse(teardownDate) > Date.parse(confEndDt)))
        {
             alert(EN_317);
             return false;
        }
      
        var setupDurMin = parseInt(Date.parse(setupDate) - Date.parse(startdate)) / (1000 * 60);
        var tearDurMin = parseInt(Date.parse(confEndDt) - Date.parse(teardownDate)) / (1000 * 60);
        
        if(isNaN(setupDurMin))
            setupDurMin = 0;
        
        if(isNaN(tearDurMin))
            tearDurMin = 0;
            
        if(setupDurMin >0 || tearDurMin > 0)
        {
            if( (totDur -(setupDurMin + tearDurMin)) < 15)
            {
                alert(EN_314);
                return false;
            }
        }   
    }

    openerfrm.hdnBufferStr.value = setupTime + "&" + teardownTime;
	openerfrm.hdnSetupTime.value = setupDurMin;
	openerfrm.hdnTeardownTime.value = tearDurMin;
	
	if (document.frmRecur.RecurDurationhr)
		dr = parseInt(document.frmRecur.RecurDurationhr.value, 10) * 60 + parseInt(document.frmRecur.RecurDurationmi.value, 10);
	
	if (document.frmRecur.RecurEndhr) 
	{
		rehr = document.frmRecur.RecurEndhr.options[document.frmRecur.RecurEndhr.selectedIndex].value;
		remi = document.frmRecur.RecurEndmi.options[document.frmRecur.RecurEndmi.selectedIndex].value;
		reap = document.frmRecur.RecurEndap.options[document.frmRecur.RecurEndap.selectedIndex].value;
		
		dr = calDur(sh, sm, ss, rehr, remi, reap, 0);
	}
	
	if ( dr < 15 ) {
		alert(EN_31);
		
		if (document.frmRecur.RecurDurationhr)
			document.frmRecur.RecurDurationhr.focus();
		if (document.frmRecur.RecurEndhr)
			document.frmRecur.RecurEndhr.focus();
			
		return (false);		
	}
	
	rv = tz + "&" + sh + "&" + sm + "&" + ss + "&" + dr + "#";
	
	recurrange = document.frmRecur.StartDate.value + "&" + (document.frmRecur.EndType.checked ? "1&-1&-1" : ( document.frmRecur.REndAfter.checked ? ("2&" + document.frmRecur.Occurrence.value + "&-1") : (document.frmRecur.REndBy.checked ? ("3&-1&" + document.frmRecur.EndDate.value) : "-1&-1&-1") ));
	rv += document.frmRecur.RecurPattern.value + "#" + recurrange;
	if (frmRecur_Validator(document.frmRecur.RecurPattern.value, recurrange)) 
	{
		openerfrm.RecurSpec.value = rv;
		
//		if (document.frmRecur.EndText)
//			endvalue = document.frmRecur.EndText.value;
//		if (document.frmRecur.RecurEndhr)
//			endvalue = document.frmRecur.RecurEndhr.value + ":" + document.frmRecur.RecurEndmi.value + " " + document.frmRecur.RecurEndap.value;

		openerfrm.RecurText.value = specialRecur_discription(rv, 0, openerfrm.lstConferenceTZ.options[openerfrm.lstConferenceTZ.selectedIndex].text,
		document.frmRecur.StartDate.value, '<%=Session["timeFormat"].ToString()%>', '<%=Session["timezoneDisplay"].ToString()%>', maxRecurrence);
		
		openerfrm.RecurringText.value = openerfrm.RecurText.value		
		openerfrm.hdnSpecRec.value = "1";
		BtnCheckAvailDisplay ();
		window.close();
	}
}

function frmRecur_Validator(rp, rr)
{
	if (parseInt(document.frmRecur.Occurrence.value) > parseInt('<%=Application["confRecurrence"]%>'))
	{
		alert("Maximum limite of " + '<%=Application["confRecurrence"]%>' + " instances/occurrences in the recurring series.");
		return false;
	}
	
	if (chkRange(rr))
		return true;
	else
		return false;
}

var british = false;

function chkRange(rr)
{
	rrary = rr.split("&");
    
    if('<%=Session["FormatDateType"]%>' == 'dd/MM/yyyy')
        british = true;
    
    var dDate = GetDefaultDate(rrary[0],dFormat);
     
	if ( (!isValidDate(rrary[0])) || ( caldatecompare(dDate, servertoday) == -1 ) ) {		// check start time is reasonable future time
		alert(EN_74);
		document.frmRecur.StartDate.focus();
		return (false);
	}

	switch (rrary[1]) {
		case "1":
			if ( (rrary[2]!= "-1") && (rrary[3]!= "-1") ) {
				alert(EN_38);
				return false;
			} else 
				return true;
			break;
		case "2":
			if (rrary[3]!= "-1") {
				alert(EN_38);
				return false;
			} else {
				if ( isPositiveInt(rrary[2], "times of occurrences")==1 )
					return true;
				else {
					document.frmRecur.Occurrence.focus();
					return false;
				}
			}
			break;
		case "3":
			if (rrary[2]!= "-1") {
				alert(EN_38);
				return false;
			} else {
				if ( (!isValidDate(rrary[3])) ||  (caldatecompare(GetDefaultDate(rrary[3],dFormat), servertoday)== -1) ) {
					alert(EN_108);
					document.frmRecur.EndDate.focus();
					return false;
	            } else {

	                var fstdate = rrary[0];  //FB 2366
	                var snddate = rrary[3];
	                if (!british) {
	                    fstdate = GetDefaultDate(rrary[0], "dd/MM/yyyy");
	                    snddate = GetDefaultDate(rrary[3], "dd/MM/yyyy");
	                }

	                if (!dateIsBefore(fstdate, snddate)) {
						alert(EN_109);
						document.frmRecur.EndDate.focus();
						return false;
					} else
						return true;
				}
			}
			return false;
			break;
		default:
			alert(EN_38);
			return false;
			break;
	}	
}

function BtnCheckAvailDisplay ()
{
	if (opener.ifrmLocation != null) {
		
		if ( (e = opener.document.getElementById("btnCheckAvailDIV")) != null ) {
			e.style.display = (openerfrm.RecurSpec.value=="") ? "" : "none";
		}
	}
}

function removerecur()
{
	openerfrm.RecurSpec.value=""; 
	openerfrm.RecurringText.value = "";
	openerfrm.hdnSpecRec.value = "";
	window.close();
}
</script>

<%--Time Calculations--%>
<script type="text/javascript">

function formatTime(timeText, regText)
{
   if('<%=Session["timeFormat"]%>' == '1')
   {    
        if (document.getElementById(timeText).value.search(/^[0|1][0-9]:[0-5][0-9] ?[a|A|p|P][M|m]/) != -1)
        {
            var a_p = "";
            //FB 2614
            //var t = new Date();
            //var d = new Date(t.getDay() + "/" + t.getMonth() + "/" + t.getYear() + " " + document.getElementById(timeText).value);
            var tText = document.getElementById(timeText);
            var d = new Date(GetDefaultDate(document.getElementById("StartDate").value,'<%=format%>') + " " + tText.value);
           
            var curr_hour = d.getHours();
            if (curr_hour < 12) a_p = "AM"; else a_p = "PM";

            if (curr_hour == 0) curr_hour = 12;
            if (curr_hour > 12) curr_hour = curr_hour - 12;
            
            curr_hour = curr_hour + "";             
            if (curr_hour.length == 1)
               curr_hour = "0" + curr_hour;
                     
            var curr_min = d.getMinutes();
            curr_min = curr_min + "";
            if (curr_min.length == 1)
               curr_min = "0" + curr_min;

            document.getElementById(timeText).value = curr_hour + ":" + curr_min + " " + a_p;            
            document.getElementById(regText).style.display = "None"; 
            return true;            
       }
       else
       {    
            document.getElementById(regText).style.display = ""; 
            return false;
       }
   }
    return true;
}

function ChangeEndDateTime(strCtrl)
{

    var confstdate = new Date();
    var endTime, startTime, setupTime, tearTime, oDiff ;
    //FB 2614
    var startDate = GetDefaultDate(document.getElementById("StartDate").value,'<%=format%>'); 
    
    startTime = new Date(startDate + " " + document.getElementById("confStartTime_Text").value);    
    setupTime = new Date(startDate + " " + document.getElementById("SetupTime_Text").value);    
    tearTime = new Date(startDate + " " + document.getElementById("TeardownTime_Text").value);    
    
    UpdateSetupDates(startTime, "confStartDate");
	
    endTime = new Date(startDate + " " + document.getElementById("confStartTime_Text").value);    
    
    if(strCtrl == "S")
    {
        
        if(setupTime > endTime)
        {
            oDiff = get_time_difference(endTime, setupTime);
            if(oDiff.duration < 60)
                AssignTimeFromDate(endTime, "SetupTime_Text");                 
        }
        else
            AssignTimeFromDate(endTime, "SetupTime_Text"); 
   }
   
   UpdateSetupDates(endTime, "SetupDate");
   
   endTime = new Date(startDate + " " + document.getElementById("SetupTime_Text").value);
   if(strCtrl == "S" || strCtrl == "U")
   {                  
        if(tearTime > endTime)
        {
            oDiff = get_time_difference(endTime, tearTime);
            if(oDiff.duration < 60)
                changeTime(endTime, "TeardownTime_Text", 1, 'H');
        }
        else
            changeTime(endTime, "TeardownTime_Text", 1, 'H');
    }   
    
   UpdateSetupDates(endTime, "TearDownDate");
   UpdateSetupDates(endTime, "confEndDate");
    
   CalculateDurationFromTime();
       
    
}

function changeTime(endTime, ctrlName, numDur, type)
{
    var apBfr = endTime.toLocaleTimeString().toString().split(" ")[1];          
    if(type == "M")
        endTime.setMinutes(endTime.getMinutes() + numDur);        
    else
        endTime.setHours(endTime.getHours() + numDur);

//    var month,date;
//    month = endTime.getMonth() + 1;
//    date = endTime.getDate();
//    
//    if(eval(endTime.getMonth() + 1) < 10)
//        month = "0" + (endTime.getMonth() + 1);
//    
//    if(eval(date) < 10)
//        date = "0" + endTime.getDate();
 
    AssignTimeFromDate(endTime, ctrlName)
}  

function AssignTimeFromDate(endTime, ctrlName)
{
    //FB 2614
    var hh = parseInt(endTime.getHours());// toLocaleTimeString().toString().split(" ")[0].toString().split(":")[0],10);            
    var ap = "AM";
    if (hh == 0) hh = 12;
    if (hh > 12) 
    {    
        hh = hh - 12;
        ap = "PM";
    }
    
    if (hh < 10)
        hh = "0" + hh;
    
    //FB 2614
    var mm = parseInt(endTime.getMinutes()); // toLocaleTimeString().toString().split(" ")[0].toString().split(":")[1],10);
    if (mm < 10)
        mm = "0" + mm;
        
    //var ap = endTime.toLocaleTimeString().toString().split(" ")[0];

    var evntime = parseInt(hh,10);
    
     if(evntime < 12 && ap == "PM")
        evntime = evntime + 12;
               
    if('<%=Session["timeFormat"]%>' == '0')
    {
        if(ap == "AM")
        {
            if(hh == "12")
                document.getElementById(ctrlName).value = "00:" + mm ;
            else
                document.getElementById(ctrlName).value = hh + ":" + mm ;
        }
        else
        {
            if(evntime == "24")
                document.getElementById(ctrlName).value = "12:" + mm ;
            else
                document.getElementById(ctrlName).value = evntime + ":" + mm ;
        }
    }
    else
        document.getElementById(ctrlName).value = hh + ":" + mm + " " + ap;
}

function CalculateDurationFromTime()
{
    startTime = new Date("01/01/1999 " + document.getElementById("confStartTime_Text").value);            
    tearTime = new Date("01/01/1999 " + document.getElementById("TeardownTime_Text").value); 
    
    var oDiff = get_time_difference(startTime, tearTime);
    
    document.getElementById("RecurDurationhr").value = oDiff.hours;
    document.getElementById("RecurDurationmi").value = oDiff.minutes;
}

function CalculateTimeFromDuration()
{   
    var startDate = GetDefaultDate(document.getElementById("StartDate").value,'<%=format%>'); //FB 2614
    startTime = new Date(startDate + " " + document.getElementById("confStartTime_Text").value); 

	UpdateSetupDates(startTime,"confStartDate");
	UpdateSetupDates(startTime,"SetupDate");
    
    if (document.frmRecur.RecurDurationhr) {
		rdhr = parseInt(document.frmRecur.RecurDurationhr.value, 10);
		rdmi = parseInt(document.frmRecur.RecurDurationmi.value, 10);
		
		recurduration = rdhr * 60 + rdmi;
        if (recurduration > (maxDuration * 60))
            alert(EN_314);		
	}
	
    AssignTimeFromDate(startTime, "SetupTime_Text"); 	 
	changeTime(startTime, "TeardownTime_Text", recurduration, "M");  
	
	startTime.setMinutes(startTime.getMinutes() + recurduration);
	UpdateSetupDates(startTime,"TearDownDate");
	UpdateSetupDates(startTime,"confEndDate");
}

function UpdateSetupDates(endTime, ctrlName)
{
    var month,date;
    month = endTime.getMonth() + 1;
    date = endTime.getDate();
    
    if(eval(month) < 10)
        month = "0" + month;
    
    if(eval(date) < 10)
        date = "0" + date;
           
    if('<%=format%>' == 'MM/dd/yyyy')
        opener.document.getElementById(ctrlName).value = month + "/" + date + "/" + endTime.getFullYear();
    else
        opener.document.getElementById(ctrlName).value =  date + "/" + month + "/" + endTime.getFullYear();
}

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf8" />
    <title>myVRM</title>
    <link rel="StyleSheet" href="css/divtable.css" type="text/css" />
    <link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/border-table.css" />
    <link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/main-table.css" />
    <link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/main.css" />
    <link rel="stylesheet" title="Expedite base styles" type="text/css" href="<%=Session["OrgCSSPath"]%>" />
    <link rel="stylesheet" type="text/css" media="all" href="css/calendar-blue.css" /> <%--FB 1982--%>
    <style type="text/css">
        .blackxxxsboldtext label
        {
            font-size: 11px;
            vertical-align: text-top;
            font-weight: normal;
        }
        .blackxxxstext label
        {
            vertical-align: text-top;
        }
        .style1
        {
            font-size: xx-small;
        }
         .rowbreak td
         {
     	    word-wrap: break-word;
         }
    </style>
</head>
<body>
    <form id="frmRecur" runat="server">
    <input type="hidden" id="TimeZoneText" name="TimeZoneText" value="" />
    <input type="hidden" id="TimeZoneValues" name="TimeZoneValues" value="" />
    <input type="hidden" id="EndDateText" name="EndDateText" value="" />
    <input type="hidden" id="RecurValue" name="RecurValue" value="" />
    <input type="hidden" id="RecurPattern" name="RecurPattern" value="" />
    <input type="hidden" id="CustomDates" name="CutomDates" value="" />
    <input type="hidden" id="RecPattern" name="RecPattern" value="" />
    <input type="hidden" id="HdnDateFormat" name="HdnDateFormat" value="<%=format %>" />
    <input type="hidden" id="hdnColor" runat="server" name="hdnColor" />
    <div>
        <table border="0" align="center">
            <tr>
                <td colspan="3" align="center">
                    <span class="subtitleblueblodtext">Special Recurring Pattern</span>
                </td>
            </tr>
            <tr id="NONRecurringConferenceDiv1">
                <td class="blackxxxsboldtext" align="left" id="SDateText" width="17%" valign="top">
                    Start
                </td>
                <td valign="top">
                    <table cellpadding="0px" cellspacing="1px" border="0" width="100%">
                        <tr>
                            <td valign="top" style="text-align: left; height: 27px; color: black; font-family: arial;
                                width: 403px;" align="center" colspan="2">
                                <mbcbb:ComboBox ID="confStartTime" runat="server" CssClass="altSelectFormat" Rows="10"
                                    CausesValidation="True" Style="width: 75px" onblur="javascript:return formatTime('confStartTime_Text'); ChangeEndDateTime();"
                                    AutoPostBack="false">
                                    <asp:ListItem Value="01:00 AM" Selected="True"></asp:ListItem>
                                    <asp:ListItem Value="02:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="03:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="04:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="05:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="06:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="07:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="08:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="09:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="10:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="11:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="12:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="01:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="02:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="03:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="04:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="05:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="06:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="07:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="08:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="09:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="10:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="11:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="12:00 AM"></asp:ListItem>
                                </mbcbb:ComboBox>
                                <asp:RequiredFieldValidator ID="reqStartTime" runat="server" ControlToValidate="confStartTime"
                                    Display="Dynamic" ErrorMessage="Heure est obligatoire"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="regStartTime" runat="server" ControlToValidate="confStartTime"
                                    Display="Dynamic" ErrorMessage="Format temporaire invalid (HH:MM AM/PM)" ValidationExpression="[0|1][0-9]:[0-5][0-9] ?[a|A|p|P][M|m]"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="NONRecurringConferenceDiv2" align="left">
                <td class="blackxxxsboldtext" align="left" id="tdConfSt" valign="top">
                    Setup Completed
                </td>
                <td valign="top" align="left">
                    <table cellpadding="0px" cellspacing="1px" border="0" align="left" width="100%">
                        <tr>
                            <td valign="top" style="text-align: left; height: 27px; color: black; font-family: arial;
                                width: 403px;" align="left" colspan="2">                                                                    
                                <mbcbb:ComboBox ID="SetupTime" runat="server" CssClass="altSelectFormat" Rows="10"
                                    CausesValidation="True" Style="width: 75px" onblur="javascript:return formatTime('SetupTime_Text');return ChangeEndDate(0);"
                                    AutoPostBack="false">
                                    <asp:ListItem Value="01:00 AM" Selected="True"></asp:ListItem>
                                    <asp:ListItem Value="02:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="03:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="04:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="05:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="06:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="07:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="08:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="09:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="10:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="11:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="12:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="01:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="02:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="03:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="04:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="05:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="06:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="07:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="08:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="09:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="10:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="11:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="12:00 AM"></asp:ListItem>
                                </mbcbb:ComboBox>
                                <asp:RequiredFieldValidator ID="reqSetupStartTime" runat="server" ControlToValidate="SetupTime"
                                    Display="Dynamic" ErrorMessage="Heure est obligatoire"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="regSetupStartTime" runat="server" ControlToValidate="SetupTime"
                                    Display="Dynamic" ErrorMessage="Format temporaire invalid (HH:MM AM/PM)" ValidationExpression="[0-1][0-9]:[0-5][0-9] ?[A|a|P|p][M|m]"></asp:RegularExpressionValidator><%--FB 1715--%>
                                <asp:TextBox runat="server" ID="SetupDateTime" Width="0px" Style="display: none"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="NONRecurringConferenceDiv3">
                <td class="blackxxxsboldtext" align="left" id="tdConfEd" valign="top">
                    Teardown Started 
                </td>
                <td valign="top">
                    <table cellpadding="0px" cellspacing="1px" border="0" align="right" width="100%">
                        <tr>
                            <td valign="top" style="text-align: left; height: 27px; color: black; font-family: arial;
                                width: 403px;" align="center" colspan="2">
                                <mbcbb:ComboBox ID="TeardownTime" runat="server" CssClass="altSelectFormat" Rows="10"
                                    CausesValidation="True" Style="width: 75px" onblur="javascript: formatTime('TeardownTime_Text');return ChangeStartDate(0);"
                                    AutoPostBack="false">
                                    <asp:ListItem Value="01:00 AM" Selected="True"></asp:ListItem>
                                    <asp:ListItem Value="02:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="03:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="04:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="05:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="06:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="07:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="08:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="09:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="10:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="11:00 AM"></asp:ListItem>
                                    <asp:ListItem Value="12:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="01:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="02:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="03:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="04:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="05:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="06:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="07:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="08:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="09:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="10:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="11:00 PM"></asp:ListItem>
                                    <asp:ListItem Value="12:00 AM"></asp:ListItem>
                                </mbcbb:ComboBox>
                                <asp:RegularExpressionValidator ID="regTearDownStartTime" runat="server" ControlToValidate="TeardownTime"
                                    Display="Dynamic" ErrorMessage="Format temporaire invalid (HH:MM AM/PM)" ValidationExpression="[0-1][0-9]:[0-5][0-9] ?[A|a|P|p][M|m]"></asp:RegularExpressionValidator><%--FB 1715--%>
                                <asp:TextBox runat="server" ID="TearDownDateTime" Width="0px" Style="display: none"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="left" class="blackxxxsboldtext" valign="top">
                    Duration
                </td>
                <td>
                    <asp:TextBox ID="RecurDurationhr" runat="server" CssClass="altText" Width="10%" onblur="Javascript:  return validateDurationHr();" 
                    onchange="javascript: CalculateTimeFromDuration();" ></asp:TextBox>
                    hrs
                    <asp:TextBox ID="RecurDurationmi" runat="server" CssClass="altText" Width="10%" onblur="Javascript: return validateDurationMi();" 
                    onchange="javascript: CalculateTimeFromDuration();"></asp:TextBox>
                    mins &nbsp; <span class="blackxxxsboldtext">( Maximum limite is
                        <%=Application["MaxConferenceDurationInHours"]%>
                        hours including buffer period) </span>
                    <asp:TextBox ID="EndText" runat="server" Style="display: none"></asp:TextBox>
                </td>
            </tr>
            <tr> <%--FB 2052--%>
                <td align="left" class="blackxxxsboldtext">
                    Day Color
                </td>
                <td align="left" colspan="2">
                    <asp:Label ID="lblNoneDayColor" runat="server" Text="None"></asp:Label>                
                    <asp:RadioButtonList ID="rdlstDayColor" runat="server" GroupName="daySelection" OnClick="javascript:hdnColor.value='1'" RepeatDirection="Vertical" CssClass="rowbreak">
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <span class="subtitleblueblodtext">Recurring Pattern</span>
                </td>
            </tr>
            <tr>
                <td colspan="3" align="left" valign="top" class="blackxxxstext">
                    <table width="100%" cellpadding="2" cellspacing="0">
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td class="blackxxxstext">
                                &nbsp;<asp:RadioButton ID="selRecur1" runat="server" GroupName="scenario" OnClick="javascript:RecPattern.value='1'; fnDefault();"
                                    value="1" Style="vertical-align: middle;" />
                                <span class="blackxxxsboldtext">R�currence 1: </span>&nbsp; Every
                                <asp:DropDownList ID="selDay1" runat="server" CssClass="altText">
                                    <asp:ListItem Value="1">Monday</asp:ListItem>
                                    <asp:ListItem Value="2">Mardi</asp:ListItem>
                                    <asp:ListItem Value="3">Mercredi</asp:ListItem>
                                    <asp:ListItem Value="4">Jeudi</asp:ListItem>
                                    <asp:ListItem Value="5">Vendredi</asp:ListItem>
                                </asp:DropDownList>
                                 for chosen color day
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td class="blackxxxstext">
                                &nbsp;<asp:RadioButton ID="selRecur2" runat="server" GroupName="scenario" OnClick="javascript:RecPattern.value='2'; fnDefault();"
                                    value="2" Style="vertical-align: middle;" />
                                <span class="blackxxxsboldtext">R�currence 2: </span>&nbsp; Every other
                                <asp:DropDownList ID="selDay2" runat="server" CssClass="altText">
                                    <asp:ListItem Value="1">Monday</asp:ListItem>
                                    <asp:ListItem Value="2">Mardi</asp:ListItem>
                                    <asp:ListItem Value="3">Mercredi</asp:ListItem>
                                    <asp:ListItem Value="4">Jeudi</asp:ListItem>
                                    <asp:ListItem Value="5">Vendredi</asp:ListItem>
                                </asp:DropDownList>
                                 for chosen color day
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td class="blackxxxstext">
                                &nbsp;<asp:RadioButton ID="selRecur3" runat="server" GroupName="scenario" OnClick="javascript:RecPattern.value='3'; fnDefault(); "
                                    value="3" Style="vertical-align: middle;" />
                                <span class="blackxxxsboldtext">R�currence 3: </span>&nbsp; Every first
                                <asp:DropDownList ID="selDay3" runat="server" CssClass="altText">
                                    <asp:ListItem Value="1">Monday</asp:ListItem>
                                    <asp:ListItem Value="2">Mardi</asp:ListItem>
                                    <asp:ListItem Value="3">Mercredi</asp:ListItem>
                                    <asp:ListItem Value="4">Jeudi</asp:ListItem>
                                    <asp:ListItem Value="5">Vendredi</asp:ListItem>
                                </asp:DropDownList>
                                 of the month for chosen color day
                            </td>
                        </tr>
                        <%-- FB 1911 --%>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td class="blackxxxstext">
                                &nbsp;<asp:RadioButton ID="selRecur4" runat="server" GroupName="scenario" OnClick="javascript:RecPattern.value='4'; fnDefault(); "
                                    value="7" Style="vertical-align: middle;" />
                                <span class="blackxxxsboldtext">R�currence 4: </span>&nbsp; Every second
                                <asp:DropDownList ID="selDay4" runat="server" CssClass="altText">
                                    <asp:ListItem Value="1">Monday</asp:ListItem>
                                    <asp:ListItem Value="2">Mardi</asp:ListItem>
                                    <asp:ListItem Value="3">Mercredi</asp:ListItem>
                                    <asp:ListItem Value="4">Jeudi</asp:ListItem>
                                    <asp:ListItem Value="5">Vendredi</asp:ListItem>
                                </asp:DropDownList>
                                 of the month for chosen color day
                            </td>
                        </tr>
                        
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td class="blackxxxstext">
                                &nbsp;<asp:RadioButton ID="selRecur5" GroupName="scenario" runat="server" OnClick="javascript:RecPattern.value='5'; fnDefault();"
                                    value="4" Style="vertical-align: middle;" />
                                    <%-- FB 1911 --%>
                                <span class="blackxxxsboldtext">R�currence 5: </span>&nbsp; Every last
                                <asp:DropDownList ID="selDay5" runat="server" CssClass="altText">
                                    <asp:ListItem Value="1">Monday</asp:ListItem>
                                    <asp:ListItem Value="2">Mardi</asp:ListItem>
                                    <asp:ListItem Value="3">Mercredi</asp:ListItem>
                                    <asp:ListItem Value="4">Jeudi</asp:ListItem>
                                    <asp:ListItem Value="5">Vendredi</asp:ListItem>
                                </asp:DropDownList>
                                 of the month for chosen color day
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td class="blackxxxstext">
                                &nbsp;<asp:RadioButton ID="selRecur6" runat="server" GroupName="scenario" OnClick="javascript:RecPattern.value='6'; fnDefault();"
                                    value="5" Style="vertical-align: middle;" />
                                  <%-- FB 1911 --%>  
                                <span class="blackxxxsboldtext">R�currence 6: </span>&nbsp; Every first color day of
                                the month for chosen color day (regardless of day)
                            </td>
                        </tr>
                        
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td class="blackxxxstext">
                                &nbsp;<asp:RadioButton ID="selRecur7" runat="server" GroupName="scenario" OnClick="javascript:RecPattern.value='7'; fnDefault();"
                                    value="8" Style="vertical-align: middle;" />
                                <span class="blackxxxsboldtext">R�currence 7: </span>&nbsp; Every second color day of
                                the month for chosen color day (regardless of day)
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td class="blackxxxstext">
                                &nbsp;<asp:RadioButton ID="selRecur8" runat="server" GroupName="scenario" OnClick="javascript:RecPattern.value='8'; fnDefault();"
                                    value="6" Style="vertical-align: middle;" />
                                    <%-- FB 1911 --%>
                                <span class="blackxxxsboldtext">R�currence 8: </span>&nbsp; Every Dernier color day
                                of the month for chosen color day (regardless of day)
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="RangeRow" runat="server">
                <td colspan="3">
                    <table width="100%" cellspacing="0">
                        <tr>
                            <td colspan="3">
                                <span class="subtitleblueblodtext">Range of R�currence </span>
                            </td>
                        </tr>
                        <tr valign="top">
                            <td class="blackblodtext" nowrap>
                                Start
                                <asp:TextBox ID="StartDate" Width="100px" Font-Size="9" CssClass="altText" runat="server"></asp:TextBox>
                                <img alt="" src="image/calendar.gif" border="0" id="cal_triggerd" style="cursor: pointer;
                                    vertical-align: top;" title="Date selector" onclick="return showCalendar('<%=StartDate.ClientID%>', 'cal_triggerd', 1, '<%=format %>');" />
                            </td>
                            <td style="width: 3%;">
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td class="blackxxxsboldtext" colspan="2">
                                            <asp:RadioButton ID="EndType" runat="server" Style="vertical-align: middle;" GroupName="RangeGroup"
                                                onClick="javascript: document.frmRecur.Occurrence.value=''; document.frmRecur.EndDate.value='';" />
                                            Non end date (<span class="style1">Maximum 24 instances for Monthly and 104 for
                                                weekly ou equivalent) </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="blackxxxsboldtext" nowrap>
                                            <asp:RadioButton ID="REndAfter" runat="server" Style="vertical-align: middle;" GroupName="RangeGroup"
                                                onClick="javascript: document.frmRecur.EndDate.value='';" />
                                            End after
                                        </td>
                                        <td class="blackxxxsboldtext">
                                            <asp:TextBox ID="Occurrence" CssClass="altText" Width="35%" runat="server" onClick="javascript: document.frmRecur.REndAfter.checked=true; document.frmRecur.EndDate.value='';"></asp:TextBox>
                                            occurrences
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="blackxxxsboldtext">
                                            <asp:RadioButton ID="REndBy" runat="server" Style="vertical-align: middle;" GroupName="RangeGroup"
                                                onClick="javascript: document.frmRecur.Occurrence.value='';" />
                                            End by
                                        </td>
                                        <td nowrap>
                                            <asp:TextBox ID="EndDate" Width="100px" onblur="javascript:CheckDate(this)" onchange="javascript:CheckDate(this)"
                                                CssClass="altText" runat="server" onClick="javascript: document.frmRecur.REndBy.checked=true; document.frmRecur.Occurrence.value='';"></asp:TextBox>
                                            <img alt="" src="image/calendar.gif" border="0" id="cal_trigger2" style="cursor: pointer;
                                                vertical-align: top;" title="Date selector" onclick="return showCalendar('<%=EndDate.ClientID%>', 'cal_trigger2', 1, '<%=format %>');" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="3" >
                    Note: Maximum limite of
                    <%=Application["confRecurrence"]%>
                    instances/occurrences in the recurring series.
                </td>
            </tr>
            <tr>
                <td colspan="3" align="center">
                    <asp:Button ID="Cancel" Text="Annuler" runat="server" CssClass="altShortBlueButtonFormat"
                        OnClientClick="javaScript: opener.visControls();window.close();" />
                    <asp:Button ID="Reset" Text="Remise � zero" runat="server" CssClass="altShortBlueButtonFormat"
                        OnClientClick="javaScript: frmRecur.RecurValue.value = openerfrm.RecurSpec.value; fnDefault(); initial(); " />
                    <asp:Button ID="RecurSubmit" Text="Soumettre" runat="server" OnClientClick="javascript:return SubmitRecurrence();"
                        CssClass="altShortBlueButtonFormat" />
                    <asp:Button ID="RecurSubmit1" Text="Retirer R�currence" runat="server" CssClass="altLongBlueButtonFormat"
                        OnClientClick="javaScript: removerecur();opener.visControls();" />
                </td>
            </tr>
        </table>
    </div>
    </form>

    <script type="text/javascript"> 

	    if (document.frmRecur.EndText)
		    document.frmRecur.EndText.disabled = true;
	    if (document.frmRecur.DurText)
		    document.frmRecur.DurText.disabled = true;

	    document.frmRecur.RecurValue.value = openerfrm.RecurSpec.value;
    	
        initial();
        
        fnShow();
        
        if(document.getElementById("confStartTime_Text"))
        {
            var confstarttime_text = document.getElementById("confStartTime_Text");
            confstarttime_text.onblur  =  function()
            {   
                if(formatTime('confStartTime_Text','regStartTime'))
                    ChangeEndDateTime('S');
            };
        }
        
        if(document.getElementById("SetupTime_Text"))
        {
            var confstarttime_text = document.getElementById("SetupTime_Text");
            confstarttime_text.onblur  =  function()
            {   
                if(formatTime('SetupTime_Text','regStartTime'))
                    ChangeEndDateTime('U');
            };
        }
        
        if(document.getElementById("TeardownTime_Text"))
        {
            var confstarttime_text = document.getElementById("TeardownTime_Text");
            confstarttime_text.onblur  =  function()
            {   
                if(formatTime('TeardownTime_Text','regStartTime'))
                    ChangeEndDateTime('T');
            };
        }
        
        //FB 2282 - Starts
        function onclosesp()
        {
           window.opener.visControls();
        }
    
        window.onbeforeunload = onclosesp;
        //FB 2282 - End
        
    </script>

</body>
</html>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
<!--  #INCLUDE FILE="../en/inc/Holiday.aspx"  -->

<%@ Page Language="C#" AutoEventWireup="true" Inherits="ns_MyVRM.Group" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <!-- FB 2050 -->
<meta http-equiv="X-UA-Compatible" content="IE=7" /> <!-- FB 2050 -->

<!--Window Dressing-->
<!-- #INCLUDE FILE="inc/maintopNET.aspx" --> 

<script runat="server">

</script>
<script type="text/javascript" src="script/mousepos.js"></script>
<script type="text/javascript" src="script/managemcuorder.js"></script>
<script type="text/javascript" src="inc/functions.js"></script>


<script language="javascript">
	
function OpenDetails(groupID)
{
 // alert(groupID);
  url = "MemberAllStatus.aspx?";
  url += "GroupID=" + groupID;
    
	window.open(url,"","left=50,top=50,width=400,height=250,resizable=yes,scrollbars=yes,status=no");
   return false;
}


</script>

  <div id="tblViewDetails" style="display:none">
  </div>
  
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server" id="Head1">
    <title>I miei gruppi</title>
</head>
<body >
    <form id="frmManagebridge" runat="server" method="post" onsubmit="return true">
    <div>
     <input type="hidden" id="helpPage" value="73">
        <table width="100%">
            <tr>
                <td align="center">
                    <h3>
                        <asp:Label ID="lblHeader" runat="server" Text="I miei gruppi"></asp:Label>
                    </h3>
                </td>
            </tr>
            <tr>
                <td align="center" style="width: 1168px">
                    <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError"></asp:Label>
                </td>
            </tr>
               <tr>
                <td align="center">
                    <asp:DataGrid ID="dgGroups" runat="server" AutoGenerateColumns="False" CellPadding="2" GridLines="None" AllowSorting="true" OnSortCommand="SortGroups"
                         BorderColor="blue" BorderStyle="solid" BorderWidth="1" ShowFooter="False" OnItemCreated="BindRowsDeleteMessage"
                        OnDeleteCommand="DeleteGroup" OnEditCommand="EditGroup" Width="70%" Visible="true" style="border-collapse:separate"> <%--Edited for FF--%>
                        <SelectedItemStyle  CssClass="tableBody"/>
                          <AlternatingItemStyle CssClass="tableBody" />
                         <ItemStyle CssClass="tableBody"  />
                        <HeaderStyle CssClass="tableHeader" Height="30px" />
                        <EditItemStyle CssClass="tableBody" />
                         <%--Window Dressing--%>
                        <FooterStyle CssClass="tableBody"/>
                        <Columns>
                            <asp:BoundColumn DataField="groupID" Visible="false" ><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>
                            <asp:BoundColumn DataField="groupName" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" HeaderText="Nome del gruppo" SortExpression="1" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn> <%-- FB 2050 --%>
                            <asp:BoundColumn DataField="description" ItemStyle-CssClass="tableBody"  HeaderStyle-CssClass="tableHeader" HeaderText="Descrizione"></asp:BoundColumn>
                            <asp:BoundColumn DataField="ownerName" ItemStyle-CssClass="tableBody"  HeaderStyle-CssClass="tableHeader" HeaderText="Proprietario" SortExpression="2"></asp:BoundColumn>
                            <asp:BoundColumn DataField="public" ItemStyle-CssClass="tableBody"  HeaderStyle-CssClass="tableHeader" HeaderText="Privato/pubblico" SortExpression="3"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="Visualizza membri" ItemStyle-CssClass="tableBody"  HeaderStyle-CssClass="tableHeader">
                                <ItemTemplate>
                                    <asp:Button ID="btnViewDetails" onfocus="this.blur()" Text="Dettagli" runat="server" CssClass="altShortBlueButtonFormat" />
                               </ItemTemplate>
                             </asp:TemplateColumn>
                              <asp:TemplateColumn HeaderText="Azioni" HeaderStyle-HorizontalAlign="center">
                                <HeaderStyle CssClass="tableHeader" />
                                <ItemTemplate>
                                    <table width="100%">
                                        <tr>
                                            <td>
                                                <asp:LinkButton runat="server" Text="Modifica" ID="btnEdit" CommandName="Edit"></asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton runat="server" Text="Elimina" ID="btnDelete" CommandName="Delete"></asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:Table runat="server" ID="tblNoGroups" Visible="false" Width="90%">
                        <asp:TableRow CssClass="lblError">
                            <asp:TableCell CssClass="lblError" HorizontalAlign="center" >
                                Nessun gruppo trovato.
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>   
                </td>
            </tr>
             <tr>
                <td height="50" colspan="3" align="center" valign="middle">
                <br>
                  <img border="0" src="image/aqualine.gif" width="200" height="2" align="middle">
                  <font color="#00CCFF" size="3"><b>OPPURE</b></font>
                  <img border="0" src="image/aqualine.gif" width="200" height="2" align="middle">
		        </td>
           </tr>
           
            <tr>
                <td>
                    <table cellspacing="5" width="100%">
                        <tr>
                            <td width="170"  align="lef">&nbsp;</td>
                            <td>
                                <SPAN class=subtitleblueblodtext>Cerca Gruppi e Membri del Gruppo</SPAN>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center" style="width: 1168px">
                    <table width="65%" align="center"> 
                        <tr>
                            <td align="center">
                                <table width="100%" align="center">
                                    <tr>
                                        <td align="right" class="blackblodtext" nowrap width="100">Nome del gruppo</td>
                                        <td align="left" width="350%">
                                            <asp:TextBox ID="txtSGroupName" runat="server" CssClass="altText"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator4" ControlToValidate="txtSGroupName" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ e &#34; sono caratteri non validi." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+^;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                        </td>
                                        
                                        <td align="right" class="blackblodtext" nowrap>Membro incluso</td>
                                        <td align="left">
                                            <asp:TextBox ID="txtSMember" runat="server" CssClass="altText"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txtSMember" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > + % \ / ? | = ! ` [ ] { } # $ @ e ~ sono caratteri non validi." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+?|!`\[\]{}\=@#$%&~]*$"></asp:RegularExpressionValidator> <%--FB 1888--%>
                                        </td>                                        
                                    </tr>
                                    <tr>
                                        <td align="right" class="blackblodtext">Descrizione</td>
                                        <td align="left">
                                            <asp:TextBox ID="txtSDescription" TextMode="multiline" Rows="2" Width="200" runat="server" CssClass="altText"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" ControlToValidate="txtSDescription" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ e &#34; sono caratteri non validi." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+^;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                        </td>
                                        <td align="right">&nbsp;</td>
                                        <td align="left">&nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                             <asp:Button ID="btnReset" onfocus="this.blur()" OnClick="ResetGroup" runat="server" CssClass="altLongBlueButtonFormat" Text="Reset" Width="15%" />
                             <asp:Button ID="btnSubmit" onfocus="this.blur()" OnClick="SearchGroup" runat="server" CssClass="altLongBlueButtonFormat" Text="Invia"  Width="15%"/>
                            </td>                            
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td height="50" colspan="3" align="center" valign="middle">
                <br>
                  <img border="0" src="image/aqualine.gif" width="200" height="2" align="middle">
                  <font color="#00CCFF" size="3"><b>OPPURE</b></font>
                  <img border="0" src="image/aqualine.gif" width="200" height="2" align="middle">
		        </td>
           </tr>
            <tr>
                <td align="Left">
                    <table cellspacing="5" width="70%">
                        <tr>
                            <td width="170" align="left">&nbsp;</td>
                            <td>
                                <SPAN class=subtitleblueblodtext>crea nuovo gruppo</SPAN>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
               <td align="left" style="width: 1168px">
                    <table width="90%">
                        <tr>
                            <td align="right">
                                <asp:Button ID="btnCreate" onfocus="this.blur()" OnClick="CreateNewGroup" runat="server" CssClass="altLongBlueButtonFormat" Text="Invia" Width="12%" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <asp:TextBox ID="txtSortBy" runat="server" Visible="false"></asp:TextBox>
</form>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
 <!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
</body>
</html>
    

<%@ Page Language="C#" Inherits="ns_EditEndpoint.EditEndpoint" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <!-- FB 2050 -->
<meta http-equiv="X-UA-Compatible" content="IE=7" /> <!-- FB 2050 -->
<!--Window Dressing-->
<!-- #INCLUDE FILE="inc/maintopNET.aspx" --> 

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Crea / Modifica Endpoint</title>
<script language="javascript">

function ValidateSelection(obj)
{

    var lstBridges = document.getElementById(obj.id.substring(0,obj.id.lastIndexOf("_")) + "_lstBridges"); //FB 2093
    var lstProtocol = document.getElementById(obj.id.substring(0,obj.id.lastIndexOf("_")) + "_lstVideoProtocol");
    var lstConnectionType = document.getElementById(obj.id.substring(0,obj.id.lastIndexOf("_")) + "_lstConnectionType");
    var lstMCUAddressType = document.getElementById(obj.id.substring(0,obj.id.lastIndexOf("_")) + "_lstMCUAddressType");
    var lstAddressType = document.getElementById(obj.id.substring(0,obj.id.lastIndexOf("_")) + "_lstAddressType");
    var reqBridges = document.getElementById(obj.id.substring(0,obj.id.lastIndexOf("_")) + "_reqBridges");
    var regAddress = document.getElementById(obj.id.substring(0,obj.id.lastIndexOf("_")) + "_regAddress");
    var reqPrefDial = document.getElementById(obj.id.substring(0,obj.id.lastIndexOf("_")) + "_reqPrefDial");    //FB 1394
    
    if("<%=Session["isAssignedMCU"]%>" == "1" || ("<%=Session["isAssignedMCU"]%>" == "0" && lstBridges.value != "-1")) //FB 2093
        ValidatorEnable(reqPrefDial, true);    //FB 1394
    
    if (obj == lstAddressType)
    {
        EnableDefaults(obj.id.substring(0,obj.id.lastIndexOf("_")), true);
        //ValidatorEnable(reqBridges, false);   //FB 1394
        if (lstAddressType.value == 5)
        {
            EnableDefaults(obj.id.substring(0,obj.id.lastIndexOf("_")), false);
            lstProtocol.selectedIndex = 4;
            lstConnectionType.selectedIndex = 3;
            ValidatorEnable(reqPrefDial, false);    //FB 1394
            lstMCUAddressType.selectedIndex = 5;
            //ValidatorEnable(reqBridges, true);    //FB 1394
        }
        else if (lstAddressType.value == 4)
        {
            lstProtocol.selectedIndex = 2;
            lstConnectionType.selectedIndex = 2;
            ValidatorEnable(reqPrefDial, false);    //FB 1394
            lstMCUAddressType.selectedIndex = 4;
        }
        else
        {
            if (lstProtocol.value == 4 || lstProtocol.value == 2)
                lstProtocol.selectedIndex = 1;
            if (lstConnectionType.value == 3)
            {
                lstConnectionType.selectedIndex = 1;
                ValidatorEnable(reqPrefDial, false);    //FB 1394
            }
            if (lstMCUAddressType.value == 5 || lstMCUAddressType.value == 4)
                lstMCUAddressType.selectedIndex = 1;
        }
    }
    if (obj == lstMCUAddressType)
    {
        EnableDefaults(obj.id.substring(0,obj.id.lastIndexOf("_")), true);
        //ValidatorEnable(reqBridges, false);   //FB 1394
        if (lstMCUAddressType.value == 5)
        {
            lstProtocol.selectedIndex = 4;
            lstConnectionType.selectedIndex = 3;
            ValidatorEnable(reqPrefDial, false);    //FB 1394
            lstAddressType.selectedIndex = 5;
            //ValidatorEnable(reqBridges, true);    //FB 1394
            EnableDefaults(obj.id.substring(0,obj.id.lastIndexOf("_")), false);
        }
        else if (lstMCUAddressType.value == 4)
        {
            lstProtocol.selectedIndex = 2;
            lstConnectionType.selectedIndex = 2;
            ValidatorEnable(reqPrefDial, false);    //FB 1394
            lstAddressType.selectedIndex = 4;
        }
        else
        {
            if (lstProtocol.value == 4 || lstProtocol.value == 2)
                lstProtocol.selectedIndex = 1;
            if (lstConnectionType.value == 3)
            {
                lstConnectionType.selectedIndex = 1;
                ValidatorEnable(reqPrefDial, false);    //FB 1394
            }
            if (lstAddressType.value == 5 || lstAddressType.value == 4)
                lstAddressType.selectedIndex = 1;
        }
    }
    if (obj == lstConnectionType)
    {
        EnableDefaults(obj.id.substring(0,obj.id.lastIndexOf("_")), true);
        //ValidatorEnable(reqBridges, false);   //FB 1394
        if (lstConnectionType.value == 3) //MPI-Direct
        {
            lstProtocol.selectedIndex = 4;
            lstAddressType.selectedIndex = 5;
            lstMCUAddressType.selectedIndex = 5;
            //ValidatorEnable(reqBridges, true);    //FB 1394
            EnableDefaults(obj.id.substring(0,obj.id.lastIndexOf("_")), false);
        }
        else //If not MPI-Direct
        {
            if (lstProtocol.value == 4)
                lstProtocol.selectedIndex = 1;
            if (lstAddressType.value == 5)
                lstAddressType.selectedIndex = 1;
            if (lstMCUAddressType.value == 5)
                lstMCUAddressType.selectedIndex = 1;
        }
    }
    if (obj == lstProtocol)
    {
        EnableDefaults(obj.id.substring(0,obj.id.lastIndexOf("_")), true);
        //ValidatorEnable(reqBridges, false);   //FB 1394
        if (lstProtocol.value == 4)
        {
            lstMCUAddressType.selectedIndex = 5;
            lstConnectionType.selectedIndex = 3;
            ValidatorEnable(reqPrefDial, false);    //FB 1394
            lstAddressType.selectedIndex = 5;
            //ValidatorEnable(reqBridges, true);    //FB 1394
            EnableDefaults(obj.id.substring(0,obj.id.lastIndexOf("_")), false);
        }
        else if (lstProtocol.value == 2)
        {
            lstMCUAddressType.selectedIndex = 4;
            if (lstConnectionType.value == 3)
            {
                lstConnectionType.selectedIndex = 1;
                ValidatorEnable(reqPrefDial, false);    //FB 1394
            }
            lstAddressType.selectedIndex = 4;
        }
        else
        {
            if (lstMCUAddressType.value == 5 || lstMCUAddressType.value == 4)
                lstMCUAddressType.selectedIndex = 1;
            if (lstConnectionType.value == 3)
            {
                lstConnectionType.selectedIndex = 1;
                ValidatorEnable(reqPrefDial, false);    //FB 1394
            }
            if (lstAddressType.value == 5 || lstAddressType.value == 4)
                lstAddressType.selectedIndex = 1;
        }
    }
}

function EnableDefaults(varName, flag)
{
    var elements = document.getElementsByTagName('input'); 
    var chkDefault = varName + "_rdDefault";
    var chkDelete = varName + "_chkDelete";
    
    if (flag == false)
        document.getElementById("IsMarkedDeleted").value = "1";
    else
        document.getElementById("IsMarkedDeleted").value = "0"; 
   
    for (i=0;i<elements.length;i++)
    {
        if ( (elements.item(i).type == "radio") && (elements.item(i).id.indexOf("Default") >= 0) ) 
        {
            if(elements.item(i).id != chkDefault) // all rows but the current datagrid row
            { 
                if (flag == false) //if MPI
                    elements.item(i).checked = flag; 
                elements.item(i).disabled = !flag; 
            }
            else // in current row
            {
                if (flag == false) //if MPI
                    elements.item(i).checked= !flag; 
                else
                    elements.item(i).disabled = !flag; 
            }
        }
        if ( (elements.item(i).type == "checkbox") && (elements.item(i).id.indexOf("Delete") >= 0) ) 
        {                
            if(elements.item(i).id != chkDelete) // all rows but the current datagrid row
            {
                elements.item(i).disabled = !flag; 
                elements.item(i).checked = !flag;
            }
            else
            {
                if (flag == false) // if MPI then disable the current row delete check box 
                {
                    elements.item(i).checked= flag; 
                    elements.item(i).disabled = !flag;
                }
                else // if not MPI then enable the delete checkbox
                     elements.item(i).disabled = !flag;
            }
            if (elements.item(i).checked)
                    ValidatorEnable(document.getElementById(varName + "_regAddress"), false);
            else
                    ValidatorEnable(document.getElementById(varName + "_regAddress"), true);
        }
    }
}

function IsMarkedForDeletion()
{
    if (typeof(Page_ClientValidate) == 'function') 
        if (!Page_ClientValidate())
            return false;
    if (document.getElementById("IsMarkedDeleted").value == "1")
    {
        if (confirm("Tutti i profili diversi da MPI saranno cancellati per questo punto di fine. Sei sicuro di voler continuare?"))
            return true;
        else return false;
    }
    else return true;
}

function DataLoading(val)
{
//alert(val);
//    if (val=="1")
//        document.getElementById("dataLoadingDIV").innerHTML="<b><font color='#FF00FF' size='2'>Data loading ...</font></b>&nbsp;&nbsp;&nbsp;&nbsp;<img border='0' src='image/wait1.gif' width='100' height='12'>";
//    else
//        document.getElementById("dataLoadingDIV").innerHTML="";                   
}

function SelectOneDefault(obj)
{
    if (obj.tagName == "INPUT" && obj.type == "radio" && obj.checked) 
    {
        var elements = document.getElementsByTagName('input'); 
        var chkDefault = obj.id.substring(0,obj.id.indexOf("rdDefault")) + "chkDelete";
        var objDef = document.getElementById(chkDefault);
//        alert(chkDefault);
        if(objDef.checked)
        {
//            obj.checked = false;
            alert("Non � possibile cancellare questo profilo.");
            objDef.checked = false;
        }
        for (i=0;i<elements.length;i++)
        if ( (elements.item(i).type == "radio") && (elements.item(i).id.indexOf("Default") >= 0) ) 
        {
            if(elements.item(i).id!=obj.id) 
            {
                elements.item(i).checked= false; 
            }
        } 
    }
}  

//FB 2044 - Starts
function CheckDefault(obj)
{
    var rdDefault = obj.id.substring(0,obj.id.indexOf("chkDelete")) + "rdDefault";
    var objDef = document.getElementById(rdDefault);
   
    if (obj.checked)
    {
        var objDef1 = document.getElementById(obj.id.substring(0,obj.id.indexOf("chkDelete")) + "reqName");
        var objDef2 = document.getElementById(obj.id.substring(0,obj.id.indexOf("chkDelete")) + "regProfileName"); 
        var objDef3 = document.getElementById(obj.id.substring(0,obj.id.indexOf("chkDelete")) + "regnumPassword1"); 
        var objDef4 = document.getElementById(obj.id.substring(0,obj.id.indexOf("chkDelete")) + "cmpPass1"); 
        var objDef5 = document.getElementById(obj.id.substring(0,obj.id.indexOf("chkDelete")) + "regnumPassword2"); 
        var objDef6 = document.getElementById(obj.id.substring(0,obj.id.indexOf("chkDelete")) + "cmpPass2"); 
        var objDef7 = document.getElementById(obj.id.substring(0,obj.id.indexOf("chkDelete")) + "regAddress"); 
        var objDef8 = document.getElementById(obj.id.substring(0,obj.id.indexOf("chkDelete")) + "regMPI"); 
        var objDef9 = document.getElementById(obj.id.substring(0,obj.id.indexOf("chkDelete")) + "reqAddressType"); 
        var objDef10 = document.getElementById(obj.id.substring(0,obj.id.indexOf("chkDelete")) + "reqVideoEquipment"); 
        var objDef11 = document.getElementById(obj.id.substring(0,obj.id.indexOf("chkDelete")) + "reqAddress");
        var objDef12 = document.getElementById(obj.id.substring(0,obj.id.indexOf("chkDelete")) + "reqLineRate"); 
        var objDef13 = document.getElementById(obj.id.substring(0,obj.id.indexOf("chkDelete")) + "reqBridges"); 
        var objDef14 = document.getElementById(obj.id.substring(0,obj.id.indexOf("chkDelete")) + "reqPrefDial"); 
        var objDef15 = document.getElementById(obj.id.substring(0,obj.id.indexOf("chkDelete")) + "RegMCUAddress");
        var objDef16 = document.getElementById(obj.id.substring(0,obj.id.indexOf("chkDelete")) + "RegURL"); 
        var objDef17 = document.getElementById(obj.id.substring(0,obj.id.indexOf("chkDelete")) + "RegExchangeID"); 
        var objDef18 = document.getElementById(obj.id.substring(0,obj.id.indexOf("chkDelete")) + "RegApiport");
        var objDef19 = document.getElementById(obj.id.substring(0,obj.id.indexOf("chkDelete")) + "regRearSecCamAdd"); 
        
        if(objDef.checked)
        {
            obj.checked = false;
            alert("Non � possibile eliminare il profilo predefinito.");
        }
        
        ValidatorEnable(objDef1, false);
        objDef1.style.display = 'none';

        ValidatorEnable(objDef2, false);
        objDef2.style.display = 'none';

        ValidatorEnable(objDef3, false);
        objDef3.style.display = 'none';

        ValidatorEnable(objDef4, false);
        objDef4.style.display = 'none';

        ValidatorEnable(objDef5, false);
        objDef5.style.display = 'none';

        ValidatorEnable(objDef6, false);
        objDef6.style.display = 'none';

        ValidatorEnable(objDef7, false);
        objDef7.style.display = 'none';

        ValidatorEnable(objDef8, false);
        objDef8.style.display = 'none';

        ValidatorEnable(objDef9, false);
        objDef9.style.display = 'none';

        ValidatorEnable(objDef10, false);
        objDef10.style.display = 'none';

        ValidatorEnable(objDef11, false);
        objDef11.style.display = 'none';

        ValidatorEnable(objDef12, false);
        objDef12.style.display = 'none';

        ValidatorEnable(objDef13, false);
        objDef13.style.display = 'none';

        ValidatorEnable(objDef14, false);
        objDef14.style.display = 'none';

        ValidatorEnable(objDef15, false);
        objDef15.style.display = 'none';

        ValidatorEnable(objDef16, false);
        objDef16.style.display = 'none';

        ValidatorEnable(objDef17, false);
        objDef17.style.display = 'none';

        ValidatorEnable(objDef18, false);
        objDef18.style.display = 'none';

        ValidatorEnable(objDef19, false);
        objDef19.style.display = 'none';
    }
}
//FB 2044 - End

</script>
</head>
<body>
    <form id="frmSearchConference" runat="server" method="post">
        <center><table border="0" width="98%" cellpadding="2" cellspacing="2">
            <tr>
                <td align="center">
                    <h3><asp:Label ID="lblHeader" runat="server" Text="Gestisci punti di fine"></asp:Label></h3><br />
                    <asp:Label ID="errLabel" runat="server" CssClass="lblError"></asp:Label>
                    <div id="dataLoadingDIV"></div>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <table>
                        <tr>
                            <td>
                                <SPAN class="subtitleblueblodtext">Nome di punto finale<span style="color:Red">*</span></SPAN>
                            </td>
                            <td>
                                <asp:TextBox ID="txtEndpointName" runat="server" CssClass="altText" Text="" maxlength="20" Width="500" ></asp:TextBox><%--FB 2523--%>
                                <asp:RequiredFieldValidator ID="reqEndpointName" SetFocusOnError="true" runat="server" ControlToValidate="txtEndpointName" ErrorMessage="Richiesto"  Display="dynamic" ValidationGroup="Submit"></asp:RequiredFieldValidator>
                                                             <%-- Code Added for FB 1640--%>                                                
                                <asp:RegularExpressionValidator ID="regEndpointName" SetFocusOnError="true" ControlToValidate="txtEndpointName" Display="dynamic" runat="server" ValidationGroup="Submit" ErrorMessage="<br> & < > ' + % \ ; ? | ^ = ! ` [ ] { } : $ @ ~ e &#34; sono caratteri non validi." ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>^+;?|!`\[\]{}\x22;=:@$%&'~]*$"></asp:RegularExpressionValidator> <%--FB 1888--%>
                                <asp:TextBox CssClass="altText"  ID="txtEndpointID" runat="server" Visible="false"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
                <asp:DataGrid runat="server" OnItemDataBound="InitializeLists" OnItemCreated="InitializeLists" ID="dgProfiles" AutoGenerateColumns="false"
                  CellSpacing="0" CellPadding="4" GridLines="None" BorderColor="blue" BorderStyle="solid" BorderWidth="1"  Width="98%" style="border-collapse:separate"><%-- Edited for FF--%>
               <%--Window Dressing - Start--%>
                <SelectedItemStyle CssClass="tableBody" Font-Bold="True" />
                <EditItemStyle CssClass="tableBody" />
                <AlternatingItemStyle CssClass="tableBody" />
                <ItemStyle CssClass="tableBody" />
                <HeaderStyle CssClass="tableHeader" HorizontalAlign="left" />
                <Columns>
                    <asp:TemplateColumn HeaderText="Profilo #" HeaderStyle-HorizontalAlign="Center">
                        <ItemStyle HorizontalAlign="center" VerticalAlign="Top" />
                        <HeaderStyle CssClass="tableHeader" />
                        <ItemTemplate>
                            <asp:Label ID="lblProfileCount" Text="" runat="server" Font-Bold="true"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Profilo" HeaderStyle-HorizontalAlign="Left">
                    <HeaderStyle CssClass="tableHeader" HorizontalAlign="center" Height="25" />
                        <ItemTemplate>
                        <table width="100%">
                            <tr>
                                <td align="right" class="tableBody">
                                    nome del profilo<span style="color:Red">*</span></td>
                                <td align="left">
                                    <asp:TextBox ID="txtProfileID" Visible="false" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ProfileID") %>'></asp:TextBox>
                                    <asp:TextBox CssClass="altText"  ID="txtProfileName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ProfileName") %>'></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="reqName" ControlToValidate="txtProfileName" runat="server" ValidationGroup="Submit" ErrorMessage="Richiesto" Display="Dynamic" ></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="regProfileName" ControlToValidate="txtProfileName" Display="dynamic" runat="server" ValidationGroup="Submit" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ /  ; ? | ^ = ! ` [ ] { } :  $ @ ~ e &#34;  sono caratteri non validi.." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`\[\]{}\x22;=^:@$%&'~]*$"></asp:RegularExpressionValidator>  <%--FB 2954--%>
                                </td>
                                <td align="right" class="tableBody">
                                    Password</td>
                                <td align="left">
                                    <asp:TextBox CssClass="altText"  ID="txtProfilePassword" runat="server" TextMode="Password" Value='<%# DataBinder.Eval(Container, "DataItem.Password") %>'></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="regnumPassword1" runat="server" ErrorMessage="Sono ammessi solo valori alfanumerici." SetFocusOnError="True" ValidationGroup="Submit" ToolTip="Sono ammessi solo valori alfanumerici." ControlToValidate="txtProfilePassword" ValidationExpression="^[a-zA-Z0-9@]+$" Display="Dynamic"></asp:RegularExpressionValidator> <%--FB Case 557 Saima--%><%--FB 2319--%>
                                    <asp:CompareValidator runat="server" ID="cmpPass1" ControlToCompare="txtProfilePassword2" ControlToValidate="txtProfilePassword" ValidationGroup="Submit" ErrorMessage="<br>Si prega di confermare la password" Display="dynamic"></asp:CompareValidator>
                                </td>
                                <td align="right" class="tableBody">
                                    Conferma Password</td>
                                <td align="left">
                                    <asp:TextBox CssClass="altText"  ID="txtProfilePassword2" runat="server" TextMode="Password" Value='<%# DataBinder.Eval(Container, "DataItem.Password") %>'></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="regnumPassword2" runat="server" ErrorMessage="Sono ammessi solo valori alfanumerici." ValidationGroup="Submit" SetFocusOnError="True" ToolTip="Sono ammessi solo valori alfanumerici." ControlToValidate="txtProfilePassword" ValidationExpression="^[a-zA-Z0-9@]+$" Display="Dynamic"></asp:RegularExpressionValidator> <%--FB Case 557 Saima--%><%--FB 2319--%>
                                    <asp:CompareValidator runat="server" ID="cmpPass2" ControlToCompare="txtProfilePassword" ControlToValidate="txtProfilePassword2" ValidationGroup="Submit" ErrorMessage="<br>Le password non corrispondono" Display="dynamic"></asp:CompareValidator>
                                </td>
                            </tr>
                            <tr><%--FB 2400 start--%>
                                <td align="right" class="tableBody">
                                    TelePresence</td>
                                <td align="left">
                                    <asp:CheckBox ID="chkTelepresence" runat="server" Checked='<%# DataBinder.Eval(Container, "DataItem.isTelePresence").Equals("1") %>' OnClick="javascript:enableSubAddProfile(this)"/>
                                </td>
                                <td align="right" class="tableBody">
                                    Indirizzo<span style="color:Red">*</span></td>
                                <td align="left">
                                    <asp:TextBox CssClass="altText"  ID="txtAddress" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Address") %>' SelectedValue='<%# DataBinder.Eval(Container, "DataItem.Address") %>'></asp:TextBox>
                                    <%--FB 1972--%>
                                    <asp:RegularExpressionValidator ID="regAddress" ControlToValidate="txtAddress" ValidationGroup="Submit" Display="dynamic" runat="server"  SetFocusOnError="true" ErrorMessage="<br>& < > % \ /  ? | ^ = ` [ ] { } $ e ~ sono caratteri non validi." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>?|`\[\]{}\=^$%&~]*$"></asp:RegularExpressionValidator>  <%--FB 2267--%> <%--FB 2594--%>
                                    <%--<asp:RegularExpressionValidator ID="regAddress" ControlToValidate="txtAddress" ValidationGroup="Submit" Display="dynamic" runat="server"  SetFocusOnError="true" Enabled="false" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ and &#34; are invalid characters." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()'~]*$"></asp:RegularExpressionValidator>--%>
                                    <asp:RegularExpressionValidator ID="regMPI" Enabled="false" ControlToValidate="txtAddress" ValidationGroup="Submit" Display="dynamic" runat="server" 
                                        ErrorMessage="Indirizzo MPI valido" ValidationExpression="^[0-9A-Za-z]+$"></asp:RegularExpressionValidator>
                                    <asp:Button id="btnProfileAddr" runat="server" Text="Add" class="altShortBlueButtonFormat" Width="20%"/>
                                </td>
                                 <td align="right" class="tableBody">
                                    Tipo di indirizzo<span style="color:Red">*</span></td>
                                <td align="left">
                                    <asp:DropDownList CssClass="altLong0SelectFormat" ID="lstAddressType" runat="server" SelectedValue='<%# DataBinder.Eval(Container, "DataItem.AddressType") %>' DataTextField="Name" DataValueField="ID" onchange="javascript:ValidateSelection(this);" ></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="reqAddressType" runat="server" InitialValue="-1" ControlToValidate="lstAddressType" ValidationGroup="Submit" ErrorMessage="Richiesto" Display="dynamic"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" class="tableBody">
                                    Modello<span style="color:Red">*</span></td>
                                <td align="left">
                                   <asp:DropDownList CssClass="altLong0SelectFormat" ID="lstVideoEquipment" runat="server" DataTextField="VideoEquipmentName" DataValueField="VideoEquipmentID" SelectedValue='<%# DataBinder.Eval(Container, "DataItem.VideoEquipment") %>'></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="reqVideoEquipment" runat="server" InitialValue="-1" ControlToValidate="lstVideoEquipment" ValidationGroup="Submit" ErrorMessage="Richiesto" Display="dynamic"></asp:RequiredFieldValidator>                                    
                                    </td>
                                <td align="right" valign="top"></td>
                                <td align="left">
                                    <asp:ListBox runat="server" ID="lstProfileAddress" CssClass="altSelectFormat" Rows="3" SelectionMode="Multiple"  onDblClick="javascript:return AddRemoveList('Rem',this)"></asp:ListBox>
                                    <asp:RequiredFieldValidator ID="reqAddress" ControlToValidate="lstProfileAddress" ValidationGroup="Submit" runat="server" ErrorMessage="Required" Display="Dynamic" Enabled="false" ></asp:RequiredFieldValidator>                                    
                                    <input type="hidden" name="hdnprofileAddresses" id="hdnprofileAddresses" runat="server" value='<%# DataBinder.Eval(Container, "DataItem.MultiCodec") %>' />
                                </td>
                                <td align="right" class="tableBody">
                                    Larghezza di banda preferita<span style="color:Red">*</span></td>
                                <td align="left">
                                    <asp:DropDownList CssClass="altLong0SelectFormat" ID="lstLineRate" runat="server" DataTextField="LineRateName" DataValueField="LineRateID" SelectedValue='<%# DataBinder.Eval(Container, "DataItem.LineRate") %>'></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="reqLineRate" runat="server" InitialValue="-1" ControlToValidate="lstLineRate" ValidationGroup="Submit" ErrorMessage="Richiesto" Display="dynamic"></asp:RequiredFieldValidator>                                    
                                    </td>
                            </tr>
                            <tr>
                                <td align="right" class="tableBody">
                                    Assegnato a MCU<span style="color:Red">*</span></td>
                                <td align="left">
                                    <asp:DropDownList CssClass="altLong0SelectFormat" ID="lstBridges" runat="server" DataTextField="BridgeName" DataValueField="BridgeID" SelectedValue='<%# DataBinder.Eval(Container, "DataItem.Bridge") %>'></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="reqBridges" runat="server" InitialValue="-1" Enabled="false" ControlToValidate="lstBridges" ValidationGroup="Submit" ErrorMessage="Richiesto" Display="dynamic"></asp:RequiredFieldValidator><%--FB 1901--%>                                    
                                    </td>
                                <td align="right" class="tableBody">
                                    Opzione preferita di composizione<span style="color:Red">*</span></td>
                                <td align="left">
                                    <asp:DropDownList CssClass="altLong0SelectFormat" ID="lstConnectionType" runat="server" DataTextField="Name" DataValueField="ID" SelectedValue='<%# DataBinder.Eval(Container, "DataItem.ConnectionType") %>' onchange="javascript:ValidateSelection(this);" >
                                    </asp:DropDownList><%--Fogbugz case 427--%>
                                    <asp:RequiredFieldValidator ID="reqPrefDial" runat="server" InitialValue="-1" Enabled="false" ControlToValidate="lstConnectionType" ValidationGroup="Submit" ErrorMessage="Richiesto" Display="dynamic"></asp:RequiredFieldValidator><%--FB 2093--%> 
                                </td>
                                <td align="right" class="tableBody">
                                    Protocollo predefinito</td>
                                <td align="left">
                                    <asp:DropDownList CssClass="altLong0SelectFormat" ID="lstVideoProtocol" runat="server" SelectedValue='<%# DataBinder.Eval(Container, "DataItem.DefaultProtocol") %>' DataTextField="Name" DataValueField="ID" onchange="javascript:ValidateSelection(this);"></asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" class="tableBody">
                                    Associato con indirizzo MCU</td>
                                <td align="left">
                                    <asp:TextBox CssClass="altText"  ID="txtMCUAddress" runat="server" TextMode="SingleLine" Text='<%# DataBinder.Eval(Container, "DataItem.MCUAddress") %>'></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="RegMCUAddress" ControlToValidate="txtMCUAddress" ValidationGroup="Submit" Display="dynamic" runat="server"  SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ e &#34; sono caratteri non validi." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                </td>
                                <td align="right" class="tableBody">
                                    Tipo di indirizzo MCU</td>
                                <td align="left">
                                    <asp:DropDownList CssClass="altLong0SelectFormat" ID="lstMCUAddressType" runat="server" SelectedValue='<%# DataBinder.Eval(Container, "DataItem.MCUAddressType") %>' DataTextField="Name" DataValueField="ID" onchange="javascript:ValidateSelection(this);" ></asp:DropDownList>
                                </td>
                                <td align="right" class="tableBody">
                                    URL di accesso Web</td>
                                <td align="left">
                                    <asp:TextBox CssClass="altText"  ID="txtURL" runat="server" TextMode="SingleLine" Text='<%# DataBinder.Eval(Container, "DataItem.URL") %>'></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="RegURL" ControlToValidate="txtURL" ValidationGroup="Submit" Display="dynamic" runat="server"  SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ ( ) ; ? | ^ = ! ` , [ ] { } # $ @ ~ e &#34; sono caratteri non validi." ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>+;?|!`,\[\]{}\x22;=^@#$%&()'~]*$"></asp:RegularExpressionValidator>    
                                </td>
                            </tr>
                             <%-- Code Added For FB 1422- Start--%>                            
                            <tr>
                                <td align="right" class="tableBody">
                                    Situato al di fuori della rete</td>
                                <td align="left">
                                    <asp:CheckBox ID="chkIsOutside" runat="server" Checked='<%# (DataBinder.Eval(Container, "DataItem.IsOutside").Equals("1")) %>' />
                                </td>
                                <td align="right" class="tableBody">
                                    Crittografia preferita</td>
                                <td align="left">
                                    <asp:CheckBox ID="chkEncryptionPreferred" runat="server" Checked='<%# DataBinder.Eval(Container, "DataItem.EncryptionPreferred").Equals("1") %>'/>
                                </td>
                                <td align="right" class="tableBody">
                                    Abilitato API  Telnet</td>
                                <td align="left">
                                    <asp:CheckBox ID="chkP2PSupport" runat="server" Checked='<%# DataBinder.Eval(Container, "DataItem.TelnetAPI").Equals("1") %>'/>
                                </td>
                            </tr>
                            <%-- Code Added For FB 1422- End--%>   
                            <tr>
                                <td align="right" class="tableBody">ID e-mail</td> <%--ICAL Fix--%>
                                <td align="left">
                                    <asp:TextBox CssClass="altText"  ID="txtExchangeID" runat="server" Width="200px" TextMode="SingleLine" Text='<%# DataBinder.Eval(Container, "DataItem.ExchangeID") %>'></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="RegExchangeID" ControlToValidate="txtExchangeID" ValidationGroup="Submit" Display="dynamic" runat="server"  SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ ( ) ; ? | ^ = ! ` , [ ] { } # $ ~ e &#34; sono caratteri non validi." ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>+;?|!`,\[\]{}\x22;=^#$%&()'~]*$"></asp:RegularExpressionValidator>
                                </td>
                                 <td align="right" class="tableBody">
                                    Invita IsCalendar</td>
                                <td align="left">
                                    <asp:CheckBox ID="chkIsCalderInvite" runat="server" Checked='<%# DataBinder.Eval(Container, "DataItem.IsCalendarInvite").Equals("1") %>' />
                                </td>
                                <%--API Port Starts--%>
                                <td align="right" class="tableBody">
                                     Porta API 
                                </td>
                                <td align="left"> <!-- FB 2050 -->
                                    <asp:TextBox   ID="txtApiport" runat="server" MaxLength="5"  Text='<%# DataBinder.Eval(Container, "DataItem.apiPortno") %>' CssClass="altText"></asp:TextBox>                                
                                    <asp:RegularExpressionValidator ID="RegApiport" ControlToValidate="txtApiport" ValidationGroup="Submit" Display="dynamic" runat="server"  SetFocusOnError="true" ErrorMessage="Solo valori numerici.." ValidationExpression="^\d{1,5}$"></asp:RegularExpressionValidator>
                                </td>
                                <%--API Port Ends--%>
                            </tr>
                            <tr>
                                <td align="right" class="tableBody">
                                 Posteriore / Camera Indirizzo Security
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtRearSecCamAdd" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.RearSecCameraAddress") %>' CssClass="altText" ValidationGroup="Submit"></asp:TextBox><br />
                                    <asp:RegularExpressionValidator ID="regRearSecCamAdd" Enabled="true" ValidationGroup="Submit" ControlToValidate="txtRearSecCamAdd" ValidationExpression="^([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}$" ErrorMessage="Invalid IP Address" Display="dynamic" runat="server"></asp:RegularExpressionValidator>
                                </td>
                            </tr>
                        </table>
                        </ItemTemplate>
                        <FooterTemplate>
                        </FooterTemplate>
                        <FooterStyle BackColor="beige" />
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Predefinito" ItemStyle-VerticalAlign="Top">
                    <HeaderStyle CssClass="tableHeader" />
                        <ItemTemplate>
                            <asp:RadioButton ID="rdDefault" runat="server" onclick="javascript:SelectOneDefault(this)" GroupName="Default" Checked='<%# DataBinder.Eval(Container, "DataItem.DefaultProfile").Equals("1") %>' /></td>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Elimina" ItemStyle-VerticalAlign="Top">
                    <HeaderStyle CssClass="tableHeader" />
                        <ItemTemplate>
                            <asp:CheckBox ID="chkDelete" runat="server" Checked="false" onclick="javascript:CheckDefault(this)" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                </Columns>
                <SelectedItemStyle BackColor="Beige" />
             </asp:DataGrid>
         <tr>
            <td align="center">
                <asp:Button ID="btnCancel" runat="server" CssClass="altShortBlueButtonFormat" Text="annulla" OnClick="CancelEndpoint" />
                <asp:Button ID="btnAddNewProfile" runat="server" CssClass="altLongBlueButtonFormat" Text="Invia/Aggiungi nuovo profilo" OnClick="AddNewProfile" ValidationGroup="Submit" />
                <asp:Button ID="btnSubmit" runat="server" CssClass="altLongBlueButtonFormat" Text="invia" OnClick="SubmitEndpoint" OnClientClick="javascript:return IsMarkedForDeletion();" ValidationGroup="Submit"/>
                <asp:CustomValidator ID="cvSubmit" ValidationGroup="Submit" OnServerValidate="ValidateInput" SetFocusOnError="true" runat="server" ErrorMessage="Indirizzo IP/ISDN non valido" Display="dynamic"></asp:CustomValidator>
            </td>
         </tr>

        </table>
</center>
                <input type="hidden" id="helpPage" value="29">
                <input type="hidden" id="isMarkedDeleted" value="0" runat="server" />
    <script language="javascript" type="text/javascript">

        //FB 2400 start
        function enableSubAddProfile(obj) {
            var datagridID = obj.id.replace(obj.id.split("_", 3)[2], "");
            if (document.getElementById(obj.id).checked) {
                document.getElementById(datagridID + "btnProfileAddr").disabled = false;
                document.getElementById(datagridID + "lstProfileAddress").disabled = false;
                //document.getElementById("btnAddNewProfile").disabled = true; FB 2602
                AddRemoveList('add', obj)
            }
            else {
                document.getElementById(datagridID + "btnProfileAddr").disabled = true;
                document.getElementById(datagridID + "lstProfileAddress").disabled = true;
                document.getElementById("btnAddNewProfile").disabled = false;

                var lstBox = document.getElementById(datagridID + "lstProfileAddress");
                if (lstBox.options.length > 0) {
                    document.getElementById(datagridID + "txtAddress").value = lstBox.options[0].text
                    document.getElementById(datagridID + "hdnprofileAddresses").value = "";
                }

                for (i = lstBox.options.length - 1; i >= 0; i--)
                    lstBox.remove(i);

            }

            return false;
        }

        function AddRemoveList(opr, obj) {
            if (obj.id == null)
                return false;

            var datagridID = obj.id.replace(obj.id.split("_", 3)[2], "");
            var lstBox = document.getElementById(datagridID + "lstProfileAddress");
            var txtUsrInput = document.getElementById(datagridID + "txtAddress");
            var hdnprofileAddresses = document.getElementById(datagridID + "hdnprofileAddresses");
            ValidatorEnable(document.getElementById(datagridID + "reqAddress"), false);

            if (opr == "Rem") {
                var i;
                for (i = lstBox.options.length - 1; i >= 0; i--) {
                    if (lstBox.options[i].selected) {

                        if (lstBox.options.length > 1) {
                            if (i == lstBox.options.length - 1)
                                lstBox.options[i].text = "�" + lstBox.options[i].text;
                            else if (i == 0)
                                lstBox.options[i].text = lstBox.options[i].text + "�";
                        }

                        hdnprofileAddresses.value = hdnprofileAddresses.value.replace(lstBox.options[i].text, "").replace(/��/i, "�")
                        lstBox.remove(i);
                    }
                }
            }
            else if (opr == "add") {
                if (txtUsrInput.value.replace(/\s/g, "") == "") //trim the textbox
                    return false;

                if (lstBox.options.length >= 8) {
                    document.getElementById("errLabel").innerHTML = "Maximum 8 addresses";
                    document.getElementById("errLabel").focus();
                    return false;
                }
                else {
                    for (i = lstBox.options.length - 1; i >= 0; i--) {
                        if (lstBox.options[i].text.replace(/\s/g, "") == txtUsrInput.value.replace(/\s/g, "")) {
                            document.getElementById("errLabel").innerHTML = "Already Added address";
                            return false;
                        }
                    }
                }

                if (lstBox.options.length > 0)
                    hdnprofileAddresses.value = hdnprofileAddresses.value + "�";

                var option = document.createElement("Option");
                option.text = txtUsrInput.value;
                option.title = txtUsrInput.value;
                lstBox.add(option);

                hdnprofileAddresses.value = hdnprofileAddresses.value + txtUsrInput.value;

                txtUsrInput.value = "";
                txtUsrInput.focus();
            }

            return false;
        }
        
    </script>
    </form>
</body>
</html>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
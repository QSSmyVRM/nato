<%@ Page Language="C#" AutoEventWireup="true" Inherits="ns_MyVRM.ViewBlockedMails"%>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>


<%if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("MSIE"))
{%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%}
else {%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN"> 
<%} %>
<!-- #INCLUDE FILE="inc/maintopNET.aspx" --> 

<link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/border-table.css">
<link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/main-table.css">
<link rel="stylesheet" title="Expedite base styles" type="text/css" href="<%=Session["OrgCSSPath"]%>">  

<script>

function FnCancel()
{
	window.location.replace('organisationsettings.aspx');
}
</script>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server" id="Head1">
    <title>Visualizza mail bloccate</title>
</head>
<body >
    <form id="frmCustomAttribute" runat="server" method="post" >
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
     <input type="hidden" id="helpPage" value="73"/>
     <input type="hidden" id="HdnCustOptID" runat="server" value="<%=customAttrID%>" />
        <table width="100%">
            <tr>
                <td align="center" >
                    <h3>
                       Visualizza mail bloccate
                    </h3>
                </td>
            </tr>
            <tr>
                <td align="center" style="width: 1168px" >
                    <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError"></asp:Label>
                </td>
            </tr>
            <tr style="display:none;">
                <td align="right">
                    <asp:LinkButton ID="LnkDeleteAll" runat="server" OnClientClick="javascript:return Deleteall();" OnClick="DeleteAllBlockMail" Text="Elimina tutto" CssClass="tabtreeRootNode"></asp:LinkButton>
                </td>
            </tr>
            <tr>
                <td align="center"> 
                    <asp:DataGrid ID="dgBlockEmail" runat="server" AutoGenerateColumns="False" CellPadding="3" GridLines="None" AllowSorting="true" 
                         BorderColor="blue" BorderStyle="solid" BorderWidth="1" ShowFooter="False" Width="95%"  CellSpacing="1"  
                         OnEditCommand="EditBlockEmail" Visible="true" style="border-collapse:separate" AllowPaging="true" PageSize="7"  OnPageIndexChanged="BindBlockEmail"
                         PagerStyle-HorizontalAlign="Right">
                        <SelectedItemStyle  CssClass="tableBody"/>
                          <AlternatingItemStyle CssClass="tableBody" />
                         <ItemStyle CssClass="tableBody"  />
                        <HeaderStyle CssClass="tableHeader" Height="30px" />
                        <EditItemStyle CssClass="tableBody" />
                        <FooterStyle CssClass="tableBody"/>
                        <PagerStyle CssClass="tableBody" Visible = "true" Wrap="False" HorizontalAlign="Right" ForeColor="Blue"  Mode="NumericPages" Position="Bottom" PageButtonCount="7" />
                        <Columns>
                            <asp:BoundColumn DataField="UUID" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Iscalendar" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="RowID" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" HeaderText="S.No"></asp:BoundColumn>
                            <asp:BoundColumn DataField="From" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" HeaderText="Da"></asp:BoundColumn>
                            <asp:BoundColumn DataField="To" ItemStyle-CssClass="tableBody"  HeaderStyle-CssClass="tableHeader" HeaderText="a"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Subject" ItemStyle-CssClass="tableBody" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="tableHeader" HeaderText="Invia"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Message" ItemStyle-CssClass="tableBody" ItemStyle-HorizontalAlign="Left"  HeaderStyle-CssClass="tableHeader" HeaderText="Messaggio"></asp:BoundColumn>
                            <asp:BoundColumn ItemStyle-CssClass="tableBody" ItemStyle-HorizontalAlign="Left"  HeaderStyle-CssClass="tableHeader" HeaderText="Ical"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="Modifica" HeaderStyle-HorizontalAlign="center">
                              <HeaderStyle CssClass="tableHeader" />
                              <ItemTemplate>                              
                                  <asp:LinkButton runat="server" Text="Modifica" ID="btnEdit" CommandName="Edit" ></asp:LinkButton>                              
                               </ItemTemplate>
                            </asp:TemplateColumn>
                             <asp:TemplateColumn HeaderText="Rilascio" HeaderStyle-HorizontalAlign="center">
                              <HeaderStyle CssClass="tableHeader" />
                              <ItemTemplate>                              
                                  <asp:CheckBox runat="server" ID="chkRelease" onClick="javascript:ChangeRelease(this)" />
                               </ItemTemplate>
                            </asp:TemplateColumn>
                             <asp:TemplateColumn HeaderText="Elimina" HeaderStyle-HorizontalAlign="center">
                              <HeaderStyle CssClass="tableHeader" />
                              <ItemTemplate>                              
                                  <asp:CheckBox runat="server" ID="chkDelete" onClick="javascript:ChangeDelete(this)" />
                               </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:Table runat="server" ID="tblNoBlockMail" Visible="false" Width="90%">
                        <asp:TableRow CssClass="lblError">
                            <asp:TableCell CssClass="lblError" HorizontalAlign="center"  >
                               <font class="lblError"> Nessuna E-mail bloccata trovata.</font>  
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>                   
                </td>
            </tr>    
            <tr>
                <td >
                    <table width="100%" align="center">
                        <tr>
                            <td align="right">
                                <asp:Button Text="Annulla" runat="server" ID="btnCancel" CssClass="altShortBlueButtonFormat" OnClick="RedirectToTargetPage"/>
                            </td>
                            <td width="2%"></td>
                            <td>
                                 <asp:Button Text="Invia" runat="server" ID="btnSubmit" CssClass="altShortBlueButtonFormat" OnClick="DeleteBlockMail" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>       
        </table>
    </form>
</body>
</html>
<script type="text/javascript">
// Release Email
function ChangeRelease(drpdwm)
    {
        var str=drpdwm.id;
        str = str.replace("chkRelease", "chkDelete");
        var dropdwn = document.getElementById(str);
        if(dropdwn)
        {
            if(drpdwm.checked)
            {
                if(dropdwn.checked)
                {
                   dropdwn.checked = false;
                } 
            }
        }
     
    }
    
    function ChangeDelete(drpdwm)
    {
        
        var str=drpdwm.id;
        str = str.replace("chkDelete", "chkRelease");
        var dropdwn = document.getElementById(str);
        if(dropdwn)
        {
            if(drpdwm.checked)
            {
                if(dropdwn.checked)
                {
                   dropdwn.checked = false;
                } 
            }
        }
    }
    
    function Deleteall()
    {
       if(confirm("Sei sicuro di voler eliminare tutte le e-mail bloccate visualizzate. Questa operazione � irreversibile."))
            return true;
       
       return false;
       
    }

// Release Email
</script>
<script type="text/javascript" src="inc/softedge.js"></script>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
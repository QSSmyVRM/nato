 <!DOCTYPE HTML Public "-//W3C//DTD HTML 4.0 Transitional//EN">
<%

if(Session["userID"] == null)
Response.Redirect("~/en/genlogin.aspx"); //FB 1830
%>
<!--  #INCLUDE FILE="../inc/browserdetect.aspx"  -->

<%--FB 1861--%>
<!--  #INCLUDE FILE="../inc/Holiday.aspx"  -->

<html>
<head>
<title>myVRM</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf8" />

<link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/border-table.css">
<link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/main-table.css">
<link rel="stylesheet" title="Expedite base styles" type="text/css" href="<%=Session["OrgCSSPath"]%>" > <%-- organisation Css Module --%>
<script language="javascript" src="script/RoboHelp_CSH.js"></script>
<style type="text/css">
@media print
{
#btprint
{
display: none;}
}</style>


<script language="javascript">
<!--

version = '<%=Application["Version"]%>';
copyrightsDur = '<%=Application["CopyrightsDur"]%>';//FB 1648
var timerRunning = false;
var toTimer = null;
defaultConfTempJS = '<%=Session["defaultConfTemp"]%>'; //FB 1746
var isExpressUser = '<%=Session["isExpressUser"]%>'; //FB 1779
var curtalker = ""
curtalker = "<%=Session["IMTalker"]%>";
var EnableAudioBridge = '<%=Session["EnableAudioBridges"]%>'; //FB 2023
var EnableEM7Opt='<%=Session["EnableEM7"]%>'; //FB 2633

function displayAlert()
{
alert("La sessione scadrà in 1 minuto a causa di inattività.\nFare clic sul pulsante Aggiorna del browser per evitare il timeout.");
}

function logout()
{
//	    alert("in .NET");
window.location.href = "expired_page.aspx";
}

function startTimer()
{
t2 = parseInt("<%=Application["timeoutSecond"]%>", 10) * 1000;
t1 = t2 - 60000;

toTimer = setTimeout('displayAlert()',t1);
toTimer = setTimeout('logout()',t2);
timerRunning = true;
}

function stopTimer() {
if (timerRunning) {
clearTimeout(toTimer);
}
}


function openFeedback()
{
//		window.open ('dispatcher/gendispatcher.asp?cmd=GetFeedback', 'Feedback', 'width=450,height=310,resizable=no,scrollbars=no,status=no,left=' + (screen.availWidth-478) + ',top=198');//Login Management
window.open ('feedback.aspx?wintype=pop', 'Feedback', 'width=450,height=410,resizable=no,scrollbars=no,status=no,left=' + (screen.availWidth-478) + ',top=198'); //Login Management

}

function close_popwin()
{
if (window.winrtc) {
winrtc.close();
}
if (window.wincrt) {
wincrt.close();
}
}


function errorHandler( e, f, l ){
alert("An error has ocurred in the JavaScript on this page.\nFile: " + f + "\nLine: " + l + "\nError:" + e);
return true;
}

//    function adjustWidth(obj)
//    {
//        if(obj.style.width == "")
//        if (obj.src.indexOf("lobbytop1024.jpg") >= 0)
//        {



//        }
//-->
</script>

<% if (Application["global"] == null )
Response.Redirect("expired_page.aspx");
if (Application["global"].Equals("Permettre les réservations") ) { %>

<script language="javascript">
<!--

function international (cb)
{
if ( cb.options[cb.selectedIndex].value != "" ) {
str="" + window.location;
newstr=str.replace(/\/en\//i, "/" + cb.options[cb.selectedIndex].value + "/");
if ((ind1 = newstr.indexOf("sct=")) != -1) {
if ( (ind2=newstr.indexOf("&", ind1)) != -1)
newstr = newstr.substring(0, ind1-1) + newstr.substring(ind2, newstr.length-1);
else
newstr = newstr.substring(0, ind1-1);
}
newstr += ((newstr.indexOf("?") == -1) ? "?" : "&") + "sct=" + cb.selectedIndex;
document.location = newstr;
}
}

//-->
</script>

<%}%>
</head>

<script language="JavaScript">
startTimer();
</script>




<%
if (Session["sMenuMask"] == null ){
Response.Redirect ("expired_page.aspx");
}
if (Session["userInterface"].Equals("2") ) {
%>

<body bottommargin="0" leftmargin="0" rightmargin="0" topmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="btprint">
<tr valign="top" width="100%">
<!--<td width="100%" height="72" align="right" valign="bottom" background="image/Lobbytop.jpg">-->
<td width="60%" height="50" nowrap>
<%--<img border="0" src="<%=Session["CompanyLogo"]%>" id="Img1" onload="javascript:adjustWidth(this)"/>--%>
<img  id="mainTop" height="72" runat="server"/> <%--Edited for FF--%>
</td>
<td align="right">
<table border="0" cellpadding="0" cellspacing="0" width="200px">
<tr>
<td align="right">
<table>
<tr>
<td align="left"><b><font face="Verdana" size="1" color="#FF0000"><%=Session["userName"].ToString()%></font></b></td>
<td align="left">
<table border="0">
<tr>
<td>|</td>
<td nowrap align="left">
<table border="0">
<tr>
<td align="center">
<a href="thankyou.aspx" title="Uscita">Uscita</a>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td align="left"><b><font face="Verdana" size="1" color="#FF0000"><%=Session["organizationName"].ToString()%></font></b></td>
<td align="left">
<table border="0">
<tr>
<%--FB 1639--%>
<% if((Session["UsrCrossAccess"].ToString().Trim()=="1")&& Session["OrganizationsLimit"].ToString().Trim()!= "1")
{
%>
<td>|</td>
<td nowrap align="left">
<table border="0">
<tr>
<td align="center">
<%-- FB 2164--%> 
<%--<a href="SuperAdministrator.aspx?c=1">Modifier</a>--%>                            
<a href="OrganisationSettings.aspx?c=1">Cambia</a>
</td>
</tr>
</table>
</td>
<% }%>
<%--else
{ Response.Write("&nbsp;");}--%>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
<td style="width:0px">
<form name="frmMenu">
<input type="hidden" name="menumask" value="<% =Session["sMenuMask"] %>">
<input type="hidden" name="adminlevel" value="<% =Session["admin"] %>">
<input type="hidden" name="userinterface" value="<% =Session["userInterface"] %>">
<input type="hidden" name="feedback_enable" value="<% =Session["hasFeedback"] %>">
<input type="hidden" name="help_enable" value="<% =Session["hasHelp"] %>">
<input type="hidden" name="roomfood" value="<%=Convert.ToInt16(Session["hkModule"]) * 4 + Convert.ToInt16(Session["foodModule"])*2 + Convert.ToInt16(Session["roomModule"])%>">
<input type="hidden" name="p2p" value="<%=Session["P2PEnable"]%>">
<input type="hidden" name="ssoMode" value="<%=Application["ssoMode"]%>">
<input type="hidden" name="txtClient" id="txtClient" value="<%=Application["Client"]%>">
<input type="hidden" name="txtSAlerts" id="txtSAlerts" value="<%= Session["Alerts"] %>" />
<input type="hidden" name="txtSAlertsP" id="txtSAlertsP" value="<%= Session["AlertsP"] %>" />
<input type="hidden" name="txtSAlertsA" id="txtSAlertsA" value="<%= Session["AlertsA"] %>" />
</form>
<iframe name="ifrmListen" width="0" height="0" src="blank.htm">
<p>Listening Page</p>
</iframe>

</td>
</tr>
</table>
</td>
</tr>
</table>

<%
}
else
{
%>

<body bottommargin="0" leftmargin="0" rightmargin="0" topmargin="0" marginheight="0" marginwidth="0">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="btprint" bgcolor="Green" background="image/Lobbytop.jpg">
<tr valign="top">
<td bgcolor="Green"><img border="0" src="image/VRM.GIF" width="130" height="72"></td>
<td bgcolor="Green" background="image/Lobbytop.jpg" width="50%">
<table>
<tr valign="top" width="70%">
<!--<td width="100%" height="72" align="right" valign="bottom" background="image/Lobbytop.jpg">-->
<td width="70%" height="72">
<%-- <img border="0"  id="mainTop"  height="72">--%>
<%--<%--<img border="--%><%--0" src="mirror/image/lobby_logo.jpg" id="Img1" onload="javascript:adjustWidth(this)">--%> --%>
</td>
<td align="right">
<table border="0" cellpadding="0" cellspacing="0" width="80%">
<tr>
<td align="right">
<table>
<tr>
<td align="left"><b><font face="Verdana" size="1" color="#FF0000"><%=Session["userName"].ToString()%></font></b></td>
<td align="left">
<table border="0">
<tr>
<td>|</td>
<td nowrap align="left">
<table border="0">
<tr>
<td align="center">
<a href="thankyou.aspx" title="Uscita">Uscita</a>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td align="left"><b><font face="Verdana" size="1" color="#FF0000"><%=Session["organizationName"].ToString()%></font></b></td>
<td align="left">
<table border="0">
<tr>
<%--FB 1639--%>
<% if((Session["UsrCrossAccess"].ToString().Trim()=="1")&& Session["OrganizationsLimit"].ToString().Trim()!= "1")
{
%>
<td>|</td>
<td nowrap align="left">
<table border="0">
<tr>
<td align="center">
<a href="SuperAdministrator.aspx?c=1">Cambia</a>
</td>
</tr>
</table>
</td>
<% }%>
<%--else
{ Response.Write("&nbsp;");}--%>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
<td width="0px">
<form name="frmMenu">
<input type="hidden" name="menumask" value="<% =Session["sMenuMask"] %>">
<input type="hidden" name="adminlevel" value="<% =Session["admin"] %>">
<input type="hidden" name="userinterface" value="<% =Session["userInterface"] %>">
<input type="hidden" name="feedback_enable" value="<% =Session["hasFeedback"] %>">
<input type="hidden" name="help_enable" value="<% =Session["hasHelp"] %>">
<input type="hidden" name="roomfood" value="<%=Convert.ToInt16(Session["hkModule"]) * 4 + Convert.ToInt16(Session["foodModule"])*2 + Convert.ToInt16(Session["roomModule"])%>">
<input type="hidden" name="p2p" value="<%=Session["P2PEnable"]%>">
<input type="hidden" name="ssoMode" value="<%=Application["ssoMode"]%>">
<input type="hidden" name="txtClient" id="Hidden1" value="<%=Application["Client"]%>">
<input type="hidden" name="txtSAlerts" id="Hidden2" value="<%= Session["Alerts"] %>" />
<input type="hidden" name="txtSAlertsP" id="Hidden3" value="<%= Session["AlertsP"] %>" />
<input type="hidden" name="txtSAlertsA" id="Hidden4" value="<%= Session["AlertsA"] %>" />

</form>
</td>
<td width="50%" bgcolor="Green" background="image/Lobbytop.jpg"><img border="0" src="image/topright.jpg" width="580" height="72"></td>
</tr>
</table>
</td>
</tr>
</table>

<%
}

%>


<!-- script begin -->
<script Language="JavaScript">
<!--

var imtime1 = parseInt("<%=Session["ImRefreshRate"]%>", 10);
imtime1 = ( (imtime1 < 0.5) || isNaN(imtime1) ) ? 0.5 : imtime1;

//-->
</script>

<script type="text/javascript" src="script/errorList.js"></script>
<div class="btprint" id="mainmenu">
<script language="javascript" src="inc/menuinc.js"></script>
<script type="text/javascript"> // FB 2050
var path = 'mirror/javascript/menu_array.js';
document.write('<script type="text/javascript" src="' + path + '?' + new Date().getTime() + '"><\/script>');
</script>

<script language="javascript1.1" src="mmenu.js"></script>
</div>

<script language="javascript1.1" src="script/ajaxmulti.js"></script>
<link rel="StyleSheet" href="css\myprompt.css" type="text/css" />
<script language="javascript" src="script/showimuser.js"></script>


<a name="Haut de page"></a>

<script Language="JavaScript">
<!--
showMenu(0, "", "", "", "", "<%=Session["IMEnabled"]%>", "<%=Session["listenOn"]%>");

//-->
</script>
<%--
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="btprint">
<tr>
<td colspan="2">--%>   

<table width="100%" border="0" cellpadding="0" cellspacing="0" class="btprint">
<tr>
<td>

<%
if (Session["tickerStatus1"].Equals("1") && Session["tickerStatus"].Equals("1") ) {
%>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
<tr valign="top">
<td width="3%"></td>
<td align="left">
<%--<b><font face="Verdana" size="1" color="#FF0000">

<%
Response.Write("Welcome " + Session["userName"] + " - ("+ Session["organizationName"].ToString() + ")" );
%>

</font></b>--%>
</td>
<td align="right">
<b><font face="Verdana" size="1" color="#FF0000">
<%--<%
string dtCurrent2;
string frmtDT2;
DateTime dt2;
char[] splitter2  = {'/'};

dt2 = DateTime.Parse(Session["systemDate"].ToString() +"  "+  Session["systemTime"].ToString());

frmtDT2 = dt2.ToString("MMMM dd" + ", " + "yyyy");

if(Session["FormatDateType"] != null)
{

if (Session["FormatDateType"].ToString() == "dd/MM/yyyy")
frmtDT2 = dt2.ToString("dddd, dd MMMM yyyy");

}


dtCurrent2 = frmtDT2 + ", " + Session["systemTime"] ;

if(Session["timeFormat"] != null)
{
if(Session["timeFormat"].ToString() == "0")
{
dtCurrent2 = frmtDT2 + ", " + dt2.ToString("HH:mm") +" " ;
}

}

if(Session["timeZoneDisplay"] != null)
{
if(Session["timeZoneDisplay"].ToString() == "1")
{
dtCurrent2 +=  " " + Session["systemTimezone"]  +" " ;
}

}

Response.Write(""+ dtCurrent2 +" ");
%>--%>
</font></b>
</td>
</tr>
</table>
</td>
</tr>
<%
}
%>



<%
if (Session["tickerStatus"].Equals("0") && Session["tickerPosition"].Equals("0") ) {
%>
<tr>
<td style="width:4px"></td>
<td  bgcolor="#848484" style="font-weight:bold;" nowrap><div id="martickerDiv" style="width:999px; overflow:hidden"> <%-- FB 2050 --%>
<%
if(Session["tickerDisplay"].Equals("0")) {
%>
<%
Response.Write("My Conferences");
%> <%} %>
<%
if(Session["tickerDisplay"].Equals("1")) {
%>
<%
Response.Write(Session["RSSTitle"]);
%> <%} %>
<%--Edited for FF --%><marquee id="marticker" onmouseover="this.stop()" onmouseout="this.start()"
bgcolor="<%=Session["tickerBackground"]%>" SCROLLAMOUNT="<%=Session["tickerSpeed"]%>"></>
<%
string dtCurrent;
string frmtDT;
DateTime dt;
char[] splitter  = {'/'};

dt = DateTime.Parse(Session["systemDate"].ToString() +"  "+  Session["systemTime"].ToString());

frmtDT = dt.ToString("dddd, MMMM dd yyyy");

if(Session["FormatDateType"] != null)
{

if (Session["FormatDateType"].ToString() == "dd/MM/yyyy")
frmtDT = dt.ToString("dddd, dd MMMM yyyy");

}


dtCurrent= frmtDT + ", " + " Current time is " + Session["systemTime"] ;

if(Session["timeFormat"] != null)
{
if(Session["timeFormat"].ToString() == "0")
{
dtCurrent= frmtDT + ", " + " Current time is " + dt.ToString("HH:mm") +" " ;
}

}

if(Session["timeZoneDisplay"] != null)
{
if(Session["timeZoneDisplay"].ToString() == "1")
{
dtCurrent +=  " " + Session["systemTimezone"]  +" " ;
}
}
Response.Write("Welcome " + Session["userName"] + ". "+ dtCurrent + "  "+ Session["ticker"] + " ");
%>
</marquee></div> <%-- FB 2050 --%>
</td>
</tr>
<%
}
%>
<%
if (Session["tickerStatus1"].Equals("0") && Session["tickerPosition1"].Equals("0") ) {
%>
<tr style="height:15px">
<td style="width:4px"></td>
<td  bgcolor="#BDBDBD" style="font-weight:bold;" nowrap><div id="marticDiv" style="width:999px; overflow:hidden"> <%-- FB 2050 --%>
<%
if(Session["tickerDisplay1"].Equals("0")) {
%>
<%
Response.Write("My Conferences");
%> <%} %>
<%
if(Session["tickerDisplay1"].Equals("1")) {
%>
<%
Response.Write(Session["RSSTitle1"]);
%> <%} %>
<%--Edited for FF --%><marquee id="martic" onmouseover="this.stop()" onmouseout="this.start()"
bgcolor="<%=Session["tickerBackground1"]%>" SCROLLAMOUNT="<%=Session["tickerSpeed1"]%>"></>
<%
string dtCurrent1;
string frmtDT1;
DateTime dt1;
char[] splitter1  = {'/'};

dt1 = DateTime.Parse(Session["systemDate"].ToString() +"  "+  Session["systemTime"].ToString());

frmtDT1 = dt1.ToString("dddd, MMMM dd yyyy");

if(Session["FormatDateType"] != null)
{

if (Session["FormatDateType"].ToString() == "dd/MM/yyyy")
frmtDT1 = dt1.ToString("dddd, dd MMMM yyyy");

}


dtCurrent1 = frmtDT1 + ", " + " Current time is " + Session["systemTime"] ;

if(Session["timeFormat"] != null)
{
if(Session["timeFormat"].ToString() == "0")
{
dtCurrent1 = frmtDT1 + ", " + " Current time is " + dt1.ToString("HH:mm") +" " ;
}

}

if(Session["timeZoneDisplay"] != null)
{
if(Session["timeZoneDisplay"].ToString() == "1")
{
dtCurrent1 +=  " " + Session["systemTimezone"]  +" " ;
}
}

Response.Write("Welcome " + Session["userName"] + ". "+ dtCurrent1 + "  "+ Session["ticker1"] + " ");
%>

</marquee></div> <%-- FB 2050 --%>
</td>
</tr>

<%} %>

<%

if (Application["global"].Equals("Permettre les réservations") ) {
%>
<tr>
<td>
<table height="25" cellSpacing="0" cellPadding="0" align="right" border="0" class="btprint">
<tr>
<td noWrap><span class="para_small">Choose language&nbsp;</span></td>
<td>
<table cellSpacing="0" cellPadding="1" border="0">
<tr>
<form name="frmInternational">
<td>
<select class="para_small"  onchange="international(this)" id="select1" name="select1">
<option value="en">English</option>
<option value="ch">Chinese Simplified</option>
<option value="zh">Chinese Traditional</option>
<option value="fr">French</option>
<option value="sp">Spanish</option>
</select>
</td>
</form>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
<%
}

%>


</table>

<script language="javascript" type="text/javascript">
    var obj = document.getElementById('mainTop');
          
    //FB 1830
   var std = '../it/' + '<%=Session["OrgBanner1024Path"]%>';
   var high = '../it/' + '<%=Session["OrgBanner1600Path"]%>';
    //var std = '../' + '<%=Session["OrgBanner1024Path"]%>';                    
  // var high = '../' + '<%=Session["OrgBanner1600Path"]%>';
   
   
    //obj.style.width=window.screen.width-25; // Commented to show actual size of Image
    if (window.screen.width <= 1024)
        obj.src = std;
    else
        obj.src = high; 
            
    
    /*
    if(obj != null)
    {
    if (window.screen.width <= 1152)
        {
            obj.src = std;
            obj.style.width = window.screen.width - 220;//FB 1981
        }
    else
        {
            obj.src = high;
            //FB 1633 start
            img = new Image(); 
            img.src = obj.src;
            obj.style.width= img.width;   
            //FB 1633 end
        }
        
     }
     if(navigator.appName != "Microsoft Internet Explorer") //Edited for FF
     { 
        //obj.style.height ='72';
        obj.style.height ='70'; //FB 1836//FB 1981
     } */
</script>
</body>
</html>

<table width="100%" cellpadding="6">
<tr>
<td>








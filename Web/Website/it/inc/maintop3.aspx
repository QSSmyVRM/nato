﻿<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN"> 

<html>
<head>
  <title>myVRM</title>
  <meta name="Description" content="myVRM (Videoconferencing Resource Management) is a revolutionary Web-based software application that manages any video conferencing environment.">
  <meta name="Keywords" content="VRM, myVRM, Videoconferencing Resource Management, video conferencing, video bridges, video endpoints">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  
  <link title="Expedite base styles" href="Mirror/Styles/main.css" type=text/css rel=stylesheet/>
  <script type="text/javascript" src="script/errorList.js"></script>
 
  
  <script language="javascript" type="text/javascript">
  <!--
	
	function errorHandler( e, f, l ){
		alert("An error has ocurred in the JavaScript on this page.\nFile: " + f + "\nLine: " + l + "\nError:" + e);
		return true;
	}
	
  //-->
  </script>

</head>

<body bottommargin="0" leftmargin="5" rightmargin="3" topmargin="8" marginheight="0" marginwidth="0">

<!--------------------------------- CONTENT START HERE --------------->

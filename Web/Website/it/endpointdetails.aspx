<%@ Page Language="C#" AutoEventWireup="true" Inherits="en_endpointdetails" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title>Endpoint Details</title>  
<script runat="server">
</script>

<link title="Expedite base styles" href="<%=Session["OrgCSSPath"]%>" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="script/errorList.js"></script>
  <script type="text/javascript" language="javascript">
 //FB 2400
     function toggleDiv(id, flagit)
     {
         if (flagit == "1") 
          document.getElementById("multiCodecPopUp").style.display = 'block';
         else if (flagit == "0") 
          document.getElementById("multiCodecPopUp").style.display = 'none';
     }
 </script>
</head>
<body>
<form runat="server" id="frmEndpointDetails">
<%--FB 2400 start--%>
<div id="multiCodecPopUp"  runat="server" align="center" style="top: 150px;left:365px; POSITION: absolute; WIDTH:30%; HEIGHT: 350px;VISIBILITY: visible; Z-INDEX: 3; display:none"> 
      <table cellpadding="2" cellspacing="1"  width="70%" class="tableBody" align="center">
         <tr>
            <td class="subtitleblueblodtext" align="center" colspan="2">
                 Address
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="multicodec" runat="server"></asp:Label>               
            </td>
        </tr>
      </table>
</div>
<%--FB 2400 end--%>
<div>
       <table width="96%" >
        <tr>
            <td align="center">
                <h3>Dettagli del punto di fine</h3>
            </td>
        </tr>  
        <tr>
            <td align="center" style="width: 1168px">
                <asp:Label ID="ErrLabel" runat="server" Text="" CssClass="lblError"></asp:Label>
            </td>
        </tr>              
        </table> 
        <table border="0" cellpadding="2" cellspacing="2" width="95%">     
         <tr>
         <%--Window Dressing--%>
            <td align="right" style="width:54%" class="blackblodtext"><b>Nome di punto finale</b>
            </td> 
            <td align="left">
                <asp:Label ID="LblName" runat="server"></asp:Label>
            </td>
         </tr> 
         <tr>
         <%--Window Dressing--%>
            <td align="right" style="width:54%" class="blackblodtext"><b>Password</b>
            </td>
            <td align="left">
                <asp:Label ID="LblPass" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
         <%--Window Dressing--%>
            <td align="right" style="width:54%" class="blackblodtext"><b>Tipo di indirizzo</b>
            </td>
            <td align="left">
                <asp:Label ID="LblAddrType" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
         <%--Window Dressing--%>
            <td align="right" style="width:54%" class="blackblodtext"><b>Indirizzo</b>
            </td>
            <td align="left">
                <asp:Label ID="LblAddr" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
         <%--Window Dressing--%>
            <td align="right" style="width:54%" class="blackblodtext"><b>Modello</b>
            </td>
            <td align="left">
                <asp:Label ID="LblMdl" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
         <%--Window Dressing--%>
            <td align="right" style="width:54%" class="blackblodtext"><b>Opzione preferita di composizione</b>
            </td>
            <td align="left">
                <asp:Label ID="LblPreDialingOpn" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
         <%--Window Dressing--%>
            <td align="right" style="width:54%" class="blackblodtext"><b>Larghezza di banda preferita</b>
            </td>
            <td align="left">
                <asp:Label ID="LblPreBandWidth" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
         <%--Window Dressing--%>
            <td align="right" style="width:54%" class="blackblodtext"><b>Assegnato a MCU</b>
            </td>
            <td align="left">
                <asp:Label ID="LblAssToMcu" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
         <%--Window Dressing--%>
            <td align="right" style="width:54%" class="blackblodtext"><b>Situato al di fuori della rete</b>
            </td>
            <td align="left">
                <asp:Label ID="LblLocatedOutsideNet" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
         <%--Window Dressing--%>
            <td align="right" style="width:54%" class="blackblodtext"><b>URL di accesso Web</b>
            </td>
            <td align="left">
                <asp:Label ID="LblWebAccURL" runat="server"></asp:Label>
            </td>
        </tr>
       <%-- Code Added For FB 1422--%>
         <tr>
         <%--Window Dressing--%>
            <td align="right" style="width:54%" class="blackblodtext"><b>Abilitato API  Telnet</b>
            </td>
            <td align="left">
                <asp:Label ID="LblTelnet" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
         <%--Window Dressing--%>
            <td align="right" style="width:54%" class="blackblodtext"><b>ID e-mail</b> <%--ICAL Fix--%>
            </td>
            <td align="left">
                <asp:Label ID="LblExchange" runat="server"></asp:Label>
            </td>
        </tr>
        </table>
  
    <br />
      <table border="0" cellpadding="1" cellspacing="0" width="100%">
        <tr>
            <td align="center">
                    <%--code added for Soft Edge button--%>
                <input type="button" name="Close" onclick="Javascript: window.close();" value="Chiudi" class="altShort7BlueButtonFormat"/>           
            </td> 
        </tr>
      </table>
    </div>
   </form>
  </body>
</html>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
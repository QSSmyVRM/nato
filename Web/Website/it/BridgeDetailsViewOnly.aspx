<%@ Page Language="C#" Inherits="ns_Bridges.BridgeDetails" Buffer="true" CodeFile="~/en/bridgedetailsviewonly.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="X-UA-Compatible" content="IE=7" /> <!-- FB 2050 -->
<!-- #INCLUDE FILE="inc/maintop4.aspx" --> 

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>MCU Dettagli</title>
    <script type="text/javascript" src="inc/functions.js"></script>
<link rel="stylesheet" title="Expedite base styles" type="text/css" href="<%=Session["OrgCSSPath"]%>" >   <%-- Organization Css Module --%>
</head>
<body>
    <form id="frmMCUManagement" runat="server" method="post" onsubmit="return true">
    <input id="confPassword" runat="server" type="hidden" />
    <input id="txtApprover1_1" runat="server" type="hidden" />
    <input id="txtApprover2_1" runat="server" type="hidden" />
    <input id="txtApprover3_1" runat="server" type="hidden" />
    <input id="txtApprover4_1" runat="server" type="hidden" />
    <input id="hdnApprover1_1" runat="server" type="hidden" />
    <input id="hdnApprover2_1" runat="server" type="hidden" />
    <input id="hdnApprover3_1" runat="server" type="hidden" />
    <input id="hdnApprover4_1" runat="server" type="hidden" />

    <div>
      <input type="hidden" id="helpPage" value="65">
        
        <table width="100%">
            <tr>
                <td align="center">
                    <h3>
                        <asp:Label ID="lblHeader" runat="server"></asp:Label>
                    </h3>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td width="20"></td>
                            <td>
                                <SPAN class="subtitleblueblodtext">Configurazione di base</SPAN>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table width="90%" cellspacing="3" cellpadding="2">
                        <tr>
                                <%--Window Dressing--%>
                            <td width="20%" align="right" style="font-weight:bold" class="blackblodtext">MCU Nome:</td>
                            <td width="30%" align="left">
                                <asp:Label ID="txtMCUID" Visible="false" runat="server"></asp:Label>
                                <asp:Label ID="txtMCUName"  runat="server"></asp:Label>
                            </td>
                                <%--Window Dressing--%>
                            <td width="20%" align="right" style="font-weight:bold" class="blackblodtext">Accesso MCU:</td>
                            <td width="30%" align="left">
                                <asp:Label  ID="txtMCULogin" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr style="display:none">
                                <%--Window Dressing--%>
                            <td width="20%" align="right" style="font-weight:bold" class="blackblodtext">Password MCU:</td>
                            <td width="30%" align="left">
                                <asp:Label ID="txtPassword1"  runat="server"></asp:Label>
                            </td>
                                <%--Window Dressing--%>
                            <td width="20%" align="right" style="font-weight:bold" class="blackblodtext">Riscrivi la password:</td>
                            <td width="30%" align="left">
                                <asp:Label  ID="txtPassword2" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                                <%--Window Dressing--%>
                            <%--Code changed for FB 1425 QA Bug -Start--%> 
                            <td width="20%" align="right" style="font-weight:bold" id ="TzTD1" runat="server" class="blackblodtext">Fuso orario MCU:</td>
                            <td width="30%" align="left"  id ="TzTD2" runat="server">
                                <asp:Label ID="lblTimezone" runat="server" ></asp:Label>
                                <asp:DropDownList Visible="false" ID="lstTimezone" CssClass="altSelectFormat" DataTextField="timezoneName" DataValueField="timezoneID" runat="server"></asp:DropDownList>
                            </td>
                          <%--Code changed for FB 1425 QA Bug -End--%>
                                <%--Window Dressing--%>
                            <td width="20%" align="right" style="font-weight:bold" class="blackblodtext">Tipo di MCU:</td>
                            <td width="30%" align="left">
                                <asp:Label ID="lblMCUType" runat="server" ></asp:Label>
                                <asp:DropDownList ID="lstMCUType" Visible="false" CssClass="altSelectFormat" DataTextField="name" DataValueField="ID" runat="server"></asp:DropDownList>
                                <asp:DropDownList ID="lstInterfaceType" Visible="false" DataTextField="interfaceType" DataValueField="ID" runat="server"></asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                                <%--Window Dressing--%>
                            <td width="20%" align="right" style="font-weight:bold" class="blackblodtext">Stato MCU:</td>
                            <td width="30%" align="left">
                                <asp:Label ID="lblStatus" runat="server" ></asp:Label>
                                <asp:DropDownList ID="lstStatus" Visible="false" CssClass="altSelectFormat" runat="server" DataTextField="name" DataValueField="ID"></asp:DropDownList>
                            </td>
                                <%--Window Dressing--%>
                            <td width="20%" align="right" style="font-weight:bold" class="blackblodtext">MCU virtuale:</td>
                            <td width="30%" align="left">
                                <asp:Label ID="chkIsVirtual" runat="server" />
                            </td>
                        </tr>
                        <tr>
                                <%--Window Dressing--%>
                            <td width="20%" align="right" style="font-weight:bold" class="blackblodtext">Amministratore:</td>
                            <td width="30%" align="left">
                                <asp:Label ID="txtApprover4" EnableViewState="true"  runat="server"></asp:Label>
                                <asp:Label ID="hdnApprover4" Visible="false" runat="server" Width="0" Height="0" ForeColor="transparent" BorderColor="transparent"></asp:Label>
                            </td>
                                <%--Window Dressing--%>
                            <td width="20%" align="right" style="font-weight:bold" class="blackblodtext">Versione firmware:</td>
                            <td width="30%" align="left">
                                <asp:DropDownList ID="lstFirmwareVersion" Visible="false" CssClass="altSelectFormat" DataTextField="Name" DataValueField="ID" runat="server"></asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                                <%--Window Dressing--%>
                            <td width="20%" align="right" style="font-weight:bold" class="blackblodtext">Porte Solo audio <br />Totali:</td> <%--FB 1937--%>
                            <td width="30%" align="left" valign="middle">
                                <asp:Label ID="txtMaxAudioCalls"  runat="server"></asp:Label>
                            </td>
                                <%--Window Dressing--%>
                            <td width="20%" align="right" style="font-weight:bold" class="blackblodtext">Porte Audio / Video <br />totali:</td> <%--FB 1937--%>
                            <td width="30%" align="left" valign="middle">
                                <asp:Label  ID="txtMaxVideoCalls" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td width="20"></td>
                            <td>
                                <SPAN class=subtitleblueblodtext>Approvatori MCU</SPAN>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table width="90%" cellspacing="3" cellpadding="2">
                        <tr>
                                <%--Window Dressing--%>
                            <td width="20%" align="right" style="font-weight:bold" class="blackblodtext">Approvatore primario:</td>
                            <td width="30%" align="left">
                                <asp:Label ID="txtApprover1" EnableViewState="true"  runat="server"></asp:Label>
                            </td>
                            <td width="50%" align="left">
                                <asp:Label ID="hdnApprover1" Width="0" Height="0" ForeColor="transparent" BackColor="transparent" BorderWidth="0" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                                <%--Window Dressing--%>
                            <td width="20%" align="right" style="font-weight:bold" class="blackblodtext">Revisore secondario 1:</td>
                            <td width="30%" align="left">
                                <asp:Label ID="txtApprover2"  runat="server"></asp:Label>
                            </td>
                            <td width="50%" align="left">
                                <asp:Label ID="hdnApprover2" Width="0" Height="0" ForeColor="transparent" BackColor="transparent" BorderWidth="0" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                                <%--Window Dressing--%>
                            <td width="20%" align="right" style="font-weight:bold" class="blackblodtext">Revisore secondario 2:</td>
                            <td width="30%" align="left">
                                <asp:Label ID="txtApprover3"  runat="server"></asp:Label>
                            </td>
                            <td width="50%" align="left">
                                <asp:Label ID="hdnApprover3" Width="0" Height="0" ForeColor="transparent" BackColor="transparent" BorderWidth="0" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>                 
                </td>
            </tr>
            <tr>
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td width="20"></td>
                            <td>
                                <asp:Label CssClass="subtitleblueblodtext" ID="lblHeader1" runat="server" Text="Configurazione MCU MGC Accord" ></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="tr1" runat="server">
                <td align="center" >
                    <table width="90%" align="center"><%--Edited for FF--%>
                        <tr>
                                <%--Window Dressing--%>
                            <td width="20%" align="right" style="font-weight:bold" class="blackblodtext">Indirizzo IP porta di comando:</td>
                            <td width="30%" align="left">
                                <asp:Label ID="txtPortP"  runat="server"></asp:Label>
                            </td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr id="trMCUCards" runat="server">
                                <%--Window Dressing--%>
                            <td colspan="4" class="subtitleblueblodtext" align="center"> <%--Edited for FF--%>
                                <h5>Schede MCU</h5>
                                <asp:DataGrid ID="dgMCUCards" GridLines="Horizontal" BorderStyle="Solid" BorderColor="blue" Width="75%" AutoGenerateColumns="false" ShowFooter="false" runat="server">
                                    <HeaderStyle CssClass="tableHeader" Height="30" HorizontalAlign="Left" VerticalAlign="Middle" />
                                    <ItemStyle HorizontalAlign="left" />
                                    <Columns>
                                        <asp:BoundColumn DataField="ID" Visible="false"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="Name" HeaderText="Nome" ItemStyle-Width="40%" HeaderStyle-Width="50%" ItemStyle-CssClass="tableBody"></asp:BoundColumn>
                                        <asp:TemplateColumn HeaderText="Numero massimo di chiamate" ItemStyle-CssClass="tableBody">
                                            <ItemTemplate>
                                                <asp:Label ID="txtMaxCalls" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.MaximumCalls") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                                </td>
                            </tr>
                        <tr id="trIPServices" runat="server">
                                <%--Window Dressing--%>
                            <td colspan="4" class="subtitleblueblodtext" align="center"> <%--Edited for FF--%>
                                <h5>Servizi IP</h5>
                                <asp:DataGrid ID="dgIPServices" GridLines="Horizontal" BorderStyle="Solid" BorderColor="blue" Width="75%" AutoGenerateColumns="false" runat="server">
                                    <HeaderStyle CssClass="tableHeader" Height="30" />
                                    <FooterStyle Height="30" />
                                    <Columns>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Nome" ItemStyle-CssClass="tableBody">
                                            <ItemTemplate>
                                                <asp:Label ID="txtName"  runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.name") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Tipo di indirizzo" ItemStyle-CssClass="tableBody">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAddressType" runat="server"></asp:Label>
                                                <asp:DropDownList Visible="false" ID="lstAddressType" DataTextField="Name" DataValueField="ID"  runat="server" OnInit="BindAddressType" SelectedValue='<%#DataBinder.Eval(Container, "DataItem.addressType") %>'></asp:DropDownList>  
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Indirizzo" ItemStyle-CssClass="tableBody">
                                            <ItemTemplate>
                                                <asp:Label ID="txtAddress"  runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.address") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Accesso alla rete" ItemStyle-CssClass="tableBody">
                                            <ItemTemplate>
                                                <asp:Label ID="lblNetworkAccess" runat="server"></asp:Label>
                                                <asp:DropDownList ID="lstNetworkAccess" visible="false" runat="server" SelectedValue='<%#DataBinder.Eval(Container, "DataItem.networkAccess") %>'>
                                                    <asp:ListItem Text="Pubblico" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="privato" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="Entrambi" Value="3"></asp:ListItem>
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Uso" ItemStyle-CssClass="tableBody">
                                            <ItemTemplate>
                                                <asp:Label ID="lblUsage" runat="server"></asp:Label>
                                                <asp:DropDownList ID="lstUsage"  runat="server" Visible="false" SelectedValue='<%#DataBinder.Eval(Container, "DataItem.usage") %>'>
                                                    <asp:ListItem Text="Audio" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="Video" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="Entrambi" Value="3"></asp:ListItem>
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                            </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                                <asp:Label ID="lblNoIPServices" Text="Nessun Servizio IP trovato" CssClass="lblError" runat="server" Visible="false"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <%--Window Dressing--%>
                                <td colspan="4" class="subtitleblueblodtext" align="center"> <%--Edited for FF--%>
                                
                                <h5>Servizi ISDN</h5>
                                <asp:DataGrid ID="dgISDNServices" GridLines="Horizontal" BorderStyle="Solid" BorderColor="blue" Width="75%" AutoGenerateColumns="false" runat="server">
                                    <HeaderStyle CssClass="tableHeader" Height="30" />
                                    <FooterStyle Height="30" />
                                    <Columns>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Nome" ItemStyle-CssClass="tableBody">
                                            <ItemTemplate>
                                                <asp:Label ID="txtName"  runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.name") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Prefisso" ItemStyle-CssClass="tableBody">
                                            <ItemTemplate>
                                                <asp:Label ID="txtPrefix"  Width="50" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.prefix") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Intervallo di inizio" ItemStyle-CssClass="tableBody">
                                            <ItemTemplate>
                                                <asp:Label ID="txtStartRange"  Width="80" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.startRange") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Intervallo di fine" ItemStyle-CssClass="tableBody">
                                            <ItemTemplate>
                                                <asp:Label ID="txtEndRange"  Width="80" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.endRange") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Ordinamento di gamma" ItemStyle-CssClass="tableBody">
                                            <ItemTemplate>
                                                <asp:Label id="lblRangeSortOrder" runat="server"></asp:Label>
                                                <asp:DropDownList ID="lstRangeSortOrder" Visible="false" runat="server" SelectedValue='<%#DataBinder.Eval(Container, "DataItem.RangeSortOrder") %>'>
                                                    <asp:ListItem Text="Dall'inizio alla fine" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="Dalla fine all'inizio" Value="1"></asp:ListItem>
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Accesso alla rete" ItemStyle-CssClass="tableBody">
                                            <ItemTemplate>
                                                <asp:Label ID="lblNetworkAccess" runat="server"></asp:Label>
                                                <asp:DropDownList ID="lstNetworkAccess" Visible="false" runat="server" SelectedValue='<%#DataBinder.Eval(Container, "DataItem.networkAccess") %>' CssClass="altText">
                                                    <asp:ListItem Text="Pubblico" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="privato" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="Entrambi" Value="3"></asp:ListItem>
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Uso" ItemStyle-CssClass="tableBody">
                                            <ItemTemplate>
                                                <asp:Label ID="lblUsage" runat="server"></asp:Label>
                                                <asp:DropDownList ID="lstUsage"  Visible="false" runat="server" SelectedValue='<%#DataBinder.Eval(Container, "DataItem.usage") %>' CssClass="altText">
                                                    <asp:ListItem Text="Audio" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="Video" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="Entrambi" Value="3"></asp:ListItem>
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                            </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                                <asp:Label ID="lblNoISDNServices" Text="Nessun Servizio ISDN trovato" CssClass="lblError" runat="server" Visible="false"></asp:Label>
                                </td>
                            </tr>
                        <tr id="trMPIServices" runat="server">
                                <%--Window Dressing--%>
                            <td colspan="4" class="subtitleblueblodtext" align="center"> <%--Edited for FF--%>
                                <h5>Servizi MPI</h5>
                                <asp:DataGrid ID="dgMPIServices" GridLines="Horizontal" BorderStyle="Solid" BorderColor="blue" Width="75%" AutoGenerateColumns="false" runat="server">
                                    <HeaderStyle CssClass="tableHeader" Height="30" />
                                    <FooterStyle Height="30" />
                                    <Columns>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Nome" ItemStyle-CssClass="tableBody">
                                            <ItemTemplate>
                                                <asp:Label ID="txtName"  runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.name") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Tipo di indirizzo" ItemStyle-CssClass="tableBody">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAddressType" runat="server"></asp:Label>
                                                <asp:DropDownList Visible="false" ID="lstAddressType" DataTextField="Name" DataValueField="ID"  runat="server" OnInit="BindAddressType" SelectedValue='<%#DataBinder.Eval(Container, "DataItem.addressType") %>'></asp:DropDownList>  
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Indirizzo" ItemStyle-CssClass="tableBody">
                                            <ItemTemplate>
                                                <asp:Label ID="txtAddress"  runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.address") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Accesso alla rete" ItemStyle-CssClass="tableBody">
                                            <ItemTemplate>
                                                <asp:Label ID="lblNetworkAccess" runat="server"></asp:Label>
                                                <asp:DropDownList ID="lstNetworkAccess" visible="false" runat="server" SelectedValue='<%#DataBinder.Eval(Container, "DataItem.networkAccess") %>' CssClass="altText">
                                                    <asp:ListItem Text="Pubblico" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="privato" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="Entrambi" Value="3"></asp:ListItem>
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Uso" ItemStyle-CssClass="tableBody">
                                            <ItemTemplate>
                                                <asp:Label ID="lblUsage" runat="server"></asp:Label>
                                                <asp:DropDownList ID="lstUsage"  runat="server" Visible="false" SelectedValue='<%#DataBinder.Eval(Container, "DataItem.usage") %>' CssClass="altText">
                                                    <asp:ListItem Text="Audio" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="Video" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="Entrambi" Value="3"></asp:ListItem>
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                            </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                                <asp:Label ID="lblNoMPIServices" Text="Nessun Servizio MPI trovato" CssClass="lblError" runat="server" Visible="false"></asp:Label>
                                </td>
                            </tr>
                    </table>
                </td>
            </tr>
            <tr id="tr3" runat="server">
                <td align="center" >
                    <table width="90%">
                        <tr>
                                <%--Window Dressing--%>
                            <td width="20%" align="right" style="font-weight:bold">Porta A:</td>
                            <td width="30%" align="left">
                                <asp:Label ID="txtPortA"  runat="server" ItemStyle-CssClass="tableBody"></asp:Label>
                            </td>
                                <%--Window Dressing--%>
                            <td width="20%" align="right" style="font-weight:bold">Porta B:</td>
                            <td width="30%" align="left">
                                <asp:Label ID="txtPortB"  runat="server" ItemStyle-CssClass="tableBody"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="tr4" runat="server">
                <td align="center" >
                    <table width="90%">
                        <tr>
                                <%--Window Dressing--%>
                            <td width="20%" class="blackblodtext" align="right" style="font-weight:bold">Porta A:</td>
                            <td width="30%" align="left">
                                <asp:Label ID="txtPortT"  runat="server" ItemStyle-CssClass="tableBody"></asp:Label>
                            </td>
                            <td width="20%" align="right">&nbsp;</td>
                            <td width="30%" align="left">&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
             <tr>
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td width="20"></td>
                            <td>
                                <SPAN class=subtitleblueblodtext>Avvisi</SPAN>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center" >
                    <table width="90%">
                        <tr>
                                <%--Window Dressing--%>
                            <td width="20%" align="right" style="font-weight:bold" class="blackblodtext">Avviso di soglia ISDN</td>
                            <td width="30%" align="left">
<%--                                <asp:CheckBox ID="chkISDNThresholdAlert" onclick="javascript:ShowISDN(this)" runat="server" />
--%>                            </td>
                                <%--Window Dressing--%>
                            <td width="20%" align="right" style="font-weight:bold" class="blackblodtext">Segnalazione di malfunzionamento:</td>
                            <td width="30%" align="left">
                                <asp:Label ID="chkMalfunctionAlert" runat="server" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trISDN" runat="server">
                <td align="center" >
                    <table width="90%">
                        <tr>
                                <%--Window Dressing--%>
                            <td width="20%" class="blackblodtext" align="right">Caricamento porta ISDN MCU:</td>
                            <td width="30%" align="left">
                                <asp:Label ID="txtMCUISDNPortCharge"  runat="server"></asp:Label>
                            </td>
                                <%--Window Dressing--%>
                            <td width="20%"  class="blackblodtext" align="right">Costo linea ISDN:</td>
                            <td width="30%" align="left">
                                <asp:Label ID="txtISDNLineCost"  runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                                <%--Window Dressing--%>
                            <td width="20%" align="right" class="blackblodtext">Costo Max ISDN:</td>
                            <td width="30%" align="left">
                                <asp:Label ID="txtISDNMaxCost"  runat="server"></asp:Label>
                            </td>
                                <%--Window Dressing--%>
                            <td width="20%" align="right" class="blackblodtext">Tempo di soglia ISDN:</td>
                            <td width="30%" align="left">
                                <asp:Label ID="lblISDNThresholdTimeframe" runat="server"></asp:Label>
                                <asp:RadioButtonList ID="rdISDNThresholdTimeframe" Visible="false" runat="server">
                                    <asp:ListItem Text="mensile" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="Annuale" Value="2"></asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                         <tr>
                                <%--Window Dressing--%>
                            <td width="20%" align="right" class="blackblodtext">Percentuale di soglia ISDN:</td>
                            <td width="30%" align="left">
                                <asp:Label ID="txtISDNThresholdPercentage"  runat="server"></asp:Label>
                            </td>
                            <td width="20%" align="right">&nbsp;</td>
                            <td width="30%" align="left">&nbsp;</td>
                        </tr>
                   </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:DropDownList CssClass="altLong0SelectFormat" ID="lstAddressType" Visible="false" runat="server" DataTextField="Name" DataValueField="ID"></asp:DropDownList>
                    <asp:DropDownList runat="server" ID="lstNetworkAccess" Visible="false">
                        <asp:ListItem Text="Pubblico" Value="1"></asp:ListItem>
                        <asp:ListItem Text="privato" Value="2"></asp:ListItem>
                        <asp:ListItem Text="Entrambi" Value="3"></asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
        </table>
    </div>   
    </form>
</body>
</html>

<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
function save_in_group_prompt(promptpicture, prompttitle) 
{ 
	promptbox = document.createElement('div'); 
	promptbox.setAttribute ('id' , 'prompt');
	document.getElementsByTagName('body')[0].appendChild(promptbox);
	promptbox = eval("document.getElementById('prompt').style");

	w = 300;
	promptbox.position = 'absolute';
	document.getElementById('prompt').style.top = mousedownY-50 + 'px'; // FB 2050
	document.getElementById('prompt').style.left = mousedownX + 'px'; // FB 2050
//	document.getElementById('prompt').style.top = mousedownY; 
//	document.getElementById('prompt').style.left = mousedownX - w;
	
	promptbox.width = w;
	promptbox.border = 'outset 1 #bbbbbb'; 

    //Window Dressing - start
	m = "<table cellspacing='0' cellpadding='0' border='0' width='100%'><tr valign='middle'><td width='22' height='22' style='text-indent:2;' class='tableHeader'><img src='" + promptpicture + "' height='18' width='18'></td><td class='tableHeader'>" + prompttitle + "</td></tr></table>" 
	m += "<table cellspacing='0' cellpadding='0' border='0' width='100%' class='tableBody'><tr><td id='sigdlgcontent'>";
    //Window Dressing - end
	
	m += "<table border=0>";
	m += "  <tr><td class='blackblodtext'>";
	m += "Sono consentiti solo caratteri alfanumerici.";
	m += "  </td></tr>";
	m += "  <tr><td class='blackblodtext'>";
	m += "    Nome del gruppo <input type='text' name='gname' id='gname' class='altText' value='' style='width: 80pt'>";
	m += "  </td></tr>"
	m += "  <tr><td class='blackblodtext'>";
	m += "    Gruppo privato <input type='checkbox' name='gprivate' id='gprivate' value='1' checked>";
	m += "  </td></tr>"
	m += "  <tr><td align='right'>"
	//-code changed for Softedge button
//	m += "    <input type='button' name ='Cancel' class='altShortBlueButtonFormat' value='Annulla' onMouseOver='this.style.border=\"1 outset transparent\"' onMouseOut='this.style.border=\"1 solid transparent\"' onClick='document.getElementsByTagName(\"body\")[0].removeChild(document.getElementById(\"prompt\"))'>"
//	m += "    <input type='button' name = 'submit' class='altShortBlueButtonFormat' value='Invia' onMouseOver='this.style.border=\"1 outset #dddddd\"' onMouseOut='this.style.border=\"1 solid transparent\"' onClick='saveGroup(" + "document.getElementById(\"gname\").value" + "," + "document.getElementById(\"gprivate\").checked" + ");'>"
	m += "    <input type='button' onfocus='this.blur()' class='altShortBlueButtonFormat' value='Annulla'  onClick='document.getElementsByTagName(\"body\")[0].removeChild(document.getElementById(\"prompt\"))'>"
	m += "    <input type='button' onfocus='this.blur()' class='altShortBlueButtonFormat' value='Invia'  onClick='saveGroup(" + "document.getElementById(\"gname\").value" + "," + "document.getElementById(\"gprivate\").checked" + ");'>"
	m += "  </td></tr>"
	m += "</table>" 

	m += "</td></tr></table>" 
	
	document.getElementById('prompt').innerHTML = m;
} 


function saveGroup(gname, gprivate) 
{
	if (Trim(gname) == "") {
		alert(EN_10);
		document.getElementById("gname").focus();
		return false;
	}

	if (ifrmSaveingroup == null) {
		alert("Error: can not support file upload now. Please notify administrator about this.");
		document.getElementsByTagName("body")[0].removeChild(document.getElementById("prompt"));
		return -1;
	}
	
	var gID;
	var grpExists = false;
	if (document.location.href.indexOf(".aspx") < 0)
	    for(var i=0; i< document.frmSettings2.Group.options.length; i++)
	    {
			    if (gname.toLowerCase() == document.frmSettings2.Group.options(i).text.toLowerCase())
			    {	
				    grpExists = true;
				    gID = document.frmSettings2.Group.options(i).value;
			    }
	    }
	else
	    for(var i=0; i < document.frmSettings2.Group.options.length; i++)
	    {
			    if (gname.toLowerCase() == document.frmSettings2.Group.options[i].text.toLowerCase())
			    {	
				    grpExists = true;
				    gID = document.frmSettings2.Group.options[i].value;
			    }
	    }
    allValid = isAlphanumeric(gname);
	if (grpExists == true) //if group already exists
	{
		alert("Gruppo esiste già. Si prega di inserire un altro nome.");
		return false;
	}
	if (allValid == true) {
		ifrmSaveingroup.document.frmSaveingroup.GroupName.value = gname;
		ifrmSaveingroup.document.frmSaveingroup.GroupPrivate.value = (gprivate ? 1 : 0);
		ifrmSaveingroup.document.frmSaveingroup.GroupPublic.value = (gprivate ? "" : 1); //Code Added  for Aspx Conversion
		if (document.location.href.indexOf(".aspx") < 0)
		    ifrmSaveingroup.document.frmSaveingroup.PartysInfo.value = document.frmSettings2.PartysInfo.value;
		else
		    ifrmSaveingroup.document.frmSaveingroup.PartysInfo.value = document.frmSettings2.txtPartysInfo.value;
		ifrmSaveingroup.document.frmSaveingroup.submit(); 
	}
	else 
	{
		alert("Solo caratteri alfanumerici sono ammessi per Nome gruppo.");
		return false; 
	}

} 


function saveGroupSucc() 
{
	m = "<table width=100%>" 
	m += "  <tr><td class='blackblodtext'>Salva nel gruppo : <span class=succdonetxt>Avvenuto con successo!</span></td></tr>"
	m += "  <tr><td class='blackblodtext'><i>[ Nuovo gruppo sarà mostrato dopo che questa pagina sarà inserita. ]</i></td></tr>"
	m += "  <tr><td align='right'>"
	//-code changed for Softedge button
//	m += "    <input type='button' class='prompt' value='Close' onMouseOver='this.style.border=\"1 outset transparent\"' onMouseOut='this.style.border=\"1 solid transparent\"' onClick='document.getElementsByTagName(\"body\")[0].removeChild(document.getElementById(\"prompt\"))'>"
	m += "    <input type='button' onfocus='this.blur()' class='altShortBlueButtonFormat'  value='Chiudi' onClick='document.getElementsByTagName(\"body\")[0].removeChild(document.getElementById(\"prompt\"))'>"
	m += "  </td></tr>"
	m += "</table>"
	document.getElementById('sigdlgcontent').innerHTML = m;
} 


function saveGroupFail(em) 
{
	m = "<table width=100%>" 
	m += "  <tr><td class='blackblodtext'>Salva nel gruppo : <span class=faildonetxt>Failed!</span></td></tr>"
	m += "  <tr><td class='faildonetxt'>Error: <u><b>" + em + "</b></u></td></tr>"
	m += "  <tr><td class='blackblodtext'> Tutti i partecipanti in un gruppo devono avere indirizzi e-mail unici.</td></tr>"
	m += "  <tr><td class='blackblodtext'>Si prega di riprovare o notificare all'amministratore.</td></tr>"
	m += "  <tr><td align='right'>"
	//Window Dressing
	m += "    <input type='button' class='altBlueButtonFormat' value='Chiudi' onMouseOver='this.style.border=\"1 outset transparent\"' onMouseOut='this.style.border=\"1 solid transparent\"' onClick='document.getElementsByTagName(\"body\")[0].removeChild(document.getElementById(\"prompt\"))'>"
	m += "  </td></tr>"
	m += "</table>"

	document.getElementById('sigdlgcontent').innerHTML = m;
	promptbox = document.getElementById('prompt'); 
	promptbox.style.width = 300;
	//Code commented by Offshore FB issue no:412-Start
	    //ifrmSaveingroup.document.frmSaveingroup.submit();
	//Code commented by Offshore FB issue no:412-End
} 

// max 220

//user info
 var EN_1 = "Inserire un nome utente.";
 var EN_2 = "Inserire una password.";
 var EN_3 = "Le voci per la password non corrispondono.";
 var EN_4 = "Inserire un nome.";
 var EN_5 = "Inserire un Cognome.";
 var EN_6 = "Inserire un valido indirizzo e-mail.";
 var EN_51 = "Si prega di selezionare un utente dall'elenco degli utenti.";
 var EN_81 = "Inserire un indirizzo email valido Company";
 var EN_89 = "Inserisci il tuo Nome e Cognome";
 var EN_90 = "Inserisci un oggetto";
 var EN_91 = "Inserisci un commento";
 var EN_129 = "Inserisci il nome del contatto.";
 var EN_130 = "Inserisci un numero di telefono.";
 var EN_172 = "Inserire Login Email utente.";
 var EN_200 = "Due indirizzi di posta elettronica non corrispondono.";



//date and time info
 var EN_7 = "Controlla Da Data e rientrare.";
 var EN_8 = "Dalla data non pu� essere una data futura.";
 var EN_9 = "Inserire un data che � da prima che il fino ad oggi.";
 var EN_17 = "Inserire un numero in TimeBefore.";
 var EN_18 = "Inserire un numero in tempo delta.";
 var EN_69 = "Inserire un To Date.";
 var EN_70 = "Inserire un Dalla data.";
 var EN_92 = "Per data non pu� essere una data futura.";
 var EN_93 = "Inserisci una data a questo � da dopo la data";
 var EN_117 = "Inserisci un Up To Date.";
 var EN_118 = "Dalla data non pu� essere una data passata.";
 var EN_119 = "Up To Date non pu� essere una data passata.";
 var EN_120 = "Questa data � stata scelta Vi preghiamo di selezionare un'altra.�.";
 var EN_177 = "Inserire un orario di fine che � almeno 15 minuti dopo l'ora di inizio.";
 var EN_202 = ".. cambiamento Date per la personalizzazione esempio, non � consentito trascinare Si prega di esempio in un altro slot di tempo Il tempo non cambier�.";
 var EN_215 = "Si prega di inserire il formato corretto tempo (hh: mm AM / PM).";
 var EN_216 = "Controlla l'ora di inizio stanza con l'ora di fine conferenza.";
 var EN_217 = "Controlla l'ora di fine camera con l'ora di fine conferenza.";
 var EN_218 = "Controlla l'ora di inizio stanza con il tempo di inizio della conferenza.";
 var EN_220 = "Controlla l'ora di fine camera con l'ora di inizio della conferenza.";

// bridge and port info
 var EN_11 = "Inserire un nome di MCU.";
 var EN_12 = "Inserire un indirizzo MCU.";
 var EN_13 = "Inserire un login MCU.";
 var EN_19 = "Inserire un numero in Interval Survey.";
 var EN_20 = "Inserire un numero (tra 1 e 12) in canali T1 per scheda.";
 var EN_21 = "Inserire un numero (tra 1 e 12) in numero di porte IP per scheda";
 var EN_22 = "Inserire un numero (tra 1 e 12) in numero di canali riservati T1.";
 var EN_23 = "Inserire un numero (tra 1 e 12) in numero di porte IP riservate.";
 var EN_24 = "Inserire un numero (tra 1 e 12) in numero di porte audio per scheda.";
 var EN_25 = "Inserire un numero (tra 1 e 12) in numero riservato di porte Audio.";
 var EN_28 = "Inserire un indirizzo IP valido.";
 var EN_82 = "Inserire un indirizzo IP o il nome Remote SMTP.";
 var EN_83 = "Inserire un numero di porta";
 var EN_94 = "Inserire un numero di telefono ISDN";
 var EN_95 = "Controlla il numero di telefono ISDN e rientrare";
 var EN_96 = "Controlla l'indirizzo IP e rientrare.";
 var EN_98 = "Inserire un numero valido ISDN.";
 var EN_122 = "Inserire un nome di servizio.";
 var EN_123 = "Inserire un indirizzo.";
 var EN_124 = "Inserire un intervallo valido Start.";
 var EN_125 = "Inserire un intervallo valido Fine.";
 var EN_126 = "Inserisci un fine pi� grande di Gamma Gamma Start.";
 var EN_133 = "Inserire un nome di Cascade.";
 var EN_134 = "Seleziona un tipo di protocollo.";
 var EN_135 = "Seleziona un tipo di connessione.";
 var EN_136 = "Seleziona un ponte.";
 var EN_137 = "Inserire un indirizzo IP valido o ISDN per il ponte.";
 var EN_138 = "Selezionare un servizio IP di default.";
 var EN_139 = "Selezionare un servizio ISDN di default.";
 var EN_141 = "Seleziona un servizio privato.";
 var EN_142 = "Seleziona un servizio pubblico.";
 var EN_143 = "Seleziona un Media.";
 var EN_171 = "Selezionare un tipo di interfaccia.";
 var EN_181 = "Inserire un prefisso.";
 var EN_182 = "Inserire un nome di endpoint.";
 var EN_185 = "Inserire un indirizzo endpoint.";
 var EN_189 = "La preghiamo di inserire un numero compreso tra 0 e 100% in porta riservata.";
 var EN_213 = "Inserire un indirizzo IP valido per la porta di controllo.";
 var EN_214 = "Inserire un indirizzo IP valido per la porta A.";
 var EN_314 = "Inserire un Durata valido.";
 var EN_315 = "durata Conference dovrebbe essere minimo di 15 minuti (differenza tra la durata e il periodo di buffer).";
 var EN_316 = "Inserire un valido Durata inizio della conferenza.";
 var EN_317 = "Inserire un valido Durata End Conference.";

//Group info
 var EN_10 = "Inserire un nome di gruppo.";
 var EN_39 = "Seleziona gruppo diverso (s) per gruppi e gruppi di CC."
 var EN_53 = "Per favore, fare doppio clic sul nome del gruppo.";
 var EN_99 = "Gruppo deve avere almeno un membro.";
 var EN_105 = "Seleziona gruppo diverso (s) per il gruppo di default e CC Gruppo predefinito.";
 var EN_128 = "Si prega di aggiungere atleast un utente del gruppo.";
 //FB 1914

//recurring
 var EN_32 = "Si prega di controllare il tempo ricorrente di avvio e rientrare.";
 var EN_33 = "Controlla Durata ricorrenti e rientrare.";
 var EN_34 = "Controlla Timezone ricorrenti e rientrare.";
 var EN_35 = "Controlla il tipo di pattern ricorrenti e rientrare.";
 var EN_36 = "Si prega di verificare il tipo Fine campo di ricorrente e di rientrare.";
 var EN_37 = "Controlla il modello della Range ricorrenti e rientrare.";
 var EN_38 = "Si prega di controllare il tipo di intervallo ricorrenti e rientrare.";
 var EN_74 = "Inserire un valido futuro Data di inizio Recurring \ n (formato data: gg / mm / aaaa).";
 var EN_79= "Numero di partecipanti per la conferenza ricorrente non pu� pi�";
 var EN_107 = "Seleziona un giorno feriale.";
 var EN_108 = "Inserire un valido futuro End Data \ n (formato data: gg / mm / aaaa).";
 var EN_109 = "Inserire una data futura di fine \ n (formato data: gg / mm / aaaa).";
 var EN_193 = "Seleziona almeno una data.";
 var EN_211 = "� stato raggiunto il limite massimo del costume data selezionata.";
//conference and conference room
 var EN_26 = "Inserire un nome Conference Room.";
 var EN_27 = "Si prega di inserire un numero di capacit�.";
 var EN_30 = "Inserire un nome conferenza.";
//var EN_31 = "Please select a Conference Duration of at least 15 minutes.";
 var EN_31 = "Conferenza deve essere di almeno 15 minuti.";
 var EN_40 = "Inserire un numero in numero di partecipanti in pi�.";
 var EN_41 = "Inserire un numero positivo nel numero di partecipanti in pi�.";
var EN_43  = "Seleziona una sala conferenze.";
var EN_44  = "Inserire una password conferenza.";
var EN_45  = "Controlla il formato di data Conferenza e rientrare.";
var EN_49  = "Inserire una data futura conferenza.";
var EN_52  = "Inserire tutte le informazioni per il nuovo utente \ nPrima di sostituire l'utente precedente.";
var EN_54  = "Per favore, fare doppio clic sul nome della stanza.";
var EN_61  = "La stanza seguente (s) non hanno alcun impianto di video continuano ad impostare la videoconferenza.?";
var EN_62  = "Durante questo periodo di tempo, alla conferenza successiva (s) sono previste anche:"
var EN_71  = "Seleziona spazio per i partecipanti esterni.";
var EN_72  = "Si prega di selezionare i partecipanti esterni prima di selezionare tutta la stanza.";
var EN_75  = "Seleziona spazio per i partecipanti esterni facendo clic su Schedule camera (s).";
var EN_76  = "Devi selezionare partecipanti esterni e sala (s) \ nPrima di programmazione basato su una camera conferenza.";
var EN_87  = "Si prega di selezionare una delle opzioni di cui sopra.";
var EN_97  = "Inserire un numero Sala Conferenze.";
var EN_102 = "Inserisci una data conferenza.";
var EN_103 = "Si prega di controllare il numero di partecipanti supplementari e rientrare.";
var EN_104 = "Seleziona spazio per partecipanti esterni.";
var EN_112 = "Seleziona la tua camera prima di inviare la richiesta.";
var EN_113 = "Seleziona una conferenza.";
var EN_114 = "Seleziona un ambiente dalla camera selezionato (s) lista.";
var EN_131 = "Si prega di inserire un numero in numero massimo di telefonate contemporanee.";
var EN_140 = "L'importo massimo di 512 caratteri Descrizione della Conferenza � stato raggiunto.";
var EN_144 = "Hai scelto una o pi� stanze senza partecipi. Ti piace ancora continuare?"
var EN_145 = "Seleziona almeno un protocollo.";
var EN_146 = "Seleziona almeno un protocollo Audio.";
var EN_147 = "Seleziona almeno un protocollo Video.";
var EN_148 = "Seleziona almeno uno Velocit� di linea.";
var EN_149 = "Seleziona un attrezzature di default.";
var EN_150 = "Per favore Camera Phone Number ingresso.";
var EN_186 = "Nessun partecipante camera prescelta Sei sicuro di voler continuare.?";
var EN_187 = "Password Conference dovrebbe essere solo numerico." ;// FB 2017
var EN_190 = "Seleziona un endpoint per visualizzare i dettagli.";
var EN_192 = "Si prega di inserire fino a 10 messaggi di posta elettronica una mail per assistenti di pi�.";
var EN_194 = "Seleziona assistente incaricato dalla rubrica myVRM \ nPer spettacolo rubrica myVRM, fare clic su Modifica icona sulla destra del campo di testo.�.";
var EN_195 = "Seleziona una stanza dall'elenco o fornire informazioni sulla propria connessione.";
var EN_203 = "Seleziona almeno un reparto da associare a camera.";
var EN_204 = "Quando si seleziona endpoint, seleziona Media per esso.";
var EN_205 = "Seleziona un endpoint, perch� si seleziona un tipo di supporto.";
var EN_208 = "� stato raggiunto il limite massimo di caratteri consentiti in questo campo.";
var EN_212 = "assegnazione delle camere avanzata pu� essere assegnato solo con il non-video conferenza \ n sia rimuovere ripartizione avanzata camera o creare non-video conferenza.�.";
var EN_219 = "Camera � gi� stato aggiunto.";
//Template
var EN_50  = "Inserire un nome di modello.";
var EN_63  = "Un massimo di cinque modelli possono essere selezionati.";

var EN_55  = "Sei sicuro di voler rimuovere questo gruppo?";
var EN_56  = "Sei sicuro di voler rimuovere i gruppi selezionati?";
var EN_57  = "Sei sicuro di voler rimuovere questa conferenza \ nTutti i partecipanti in precedenza invitati saranno informati della cancellazione del volo?";
var EN_58  = "Sei sicuro di voler rimuovere la conferenza selezionato (s)?";
var EN_59  = "Sei sicuro di voler rimuovere questo modello?";
var EN_60  = "Sei sicuro di voler rimuovere i modelli selezionati?";
var EN_77  = "Fai doppio clic sul modello o Conferenza.";
var EN_191 = "Inserire un numero intero positivo nel tempo iniziale.";
// Food/Resource
var EN_156  = "Si prega di ordine Nome input.";
var EN_157  = "Un ordine deve contenere almeno un prodotto alimentare Se si sceglie di continuare, questo ordine verr� rimosso \ nSi sicuro di voler continuare..?";
var EN_158  = "Seleziona immagine";
var EN_159  = "Seleziona una risorsa immagine.";
var EN_160  = "Seleziona un elemento risorsa.";
var EN_161  = "Per favore Nome elemento input.";
var EN_162  = "Si prega di inserire una corretta quantit� Item Dovrebbe essere un numero intero positivo.";
var EN_163  = "item Se la quantit� � pari a 0, questo articolo sar� cancellato \ nSei sicuro di voler continuare.?";
var EN_164  = "Questa voce risorsa � gi� nella lista prega di inserire un nome diverso o selezionarla da modificare.";
var EN_165  = "Si prega di inserire un prezzo giusto dovrebbe essere il numero positivo e con un massimo di due decimali.";
var EN_166  = "Tutti i campi sono vuoti \ nQuesto per non verranno salvate, se � nuovo ordine;. \ n o sar� cancellato, se � vecchio \ n \ nSi sicuro di voler continuare.?";
var EN_167  = "Hai ordine per questa stanza sar� salvato \ nATTENZIONE:...? Un ordine deve contenere almeno un elemento di risorsa Se si sceglie di continuare, questo ordine verr� rimosso \ nSei sicuro di voler continuare";
var EN_168  = "Per favore Item Nome input, o rimuovere questo nuovo elemento.";
var EN_169  = "Seleziona una categoria.";
var EN_170  = "Per favore Articolo Prezzo di ingresso di unit�.";
var EN_178  = "System supportano solo il formato JPG e l'immagine JPEG Si prega di convertirlo in formato JPG o JPEG da qualche strumento grafico, ad esempio Microsoft Photo Editor.";
var EN_179  = "Per favore non caricare file eseguibile per evitare qualsiasi virus.";
var EN_188  = "La preghiamo di inserire un numero intero positivo in Quantit� di articoli.";
// terminal control
var EN_183  = "Inserire un nome di endpoint.";
var EN_184  = "Inserire un indirizzo email valido Guest.";
var EN_206  = "Sei sicuro di voler interrompere l'endpoint selezionato?";


//misc
 var EN_64 = "Devi inserire un nuovo utente per sostituire il precedente utente.";
 var EN_65 = "Conferenza salvato con successo al vostro calendario di Lotus Notes.";
 var EN_66 = "Conferenza salvato con successo al vostro calendario di Outlook.";
 var EN_67 = "Hai permissioned questo utente come un utente generico, ma hanno dato all'utente l'accesso al menu Preferenze Super Administrator \ nSuggestions: 1) Autorizzazione questo utente come un Super Administrator o n2 \) Disabilitare il / Super suo menu Preferenze amministratore. ";
 var EN_68 = "Spiacente, questa pagina � scaduta Fai clic qui per accedere nuovamente.�.";
 var EN_14 = "Inserire un numero positivo o negativo in posizione in Chain.";
 var EN_15 = "Numero di carta non � un numero positivo o negativo, \ n� il numero massimo.";
 var EN_16 = "Inserire un Home Link User pagina.";
 var EN_73 = "Il tag URL in Descrizione Conference dovrebbe essere in coppia (<url> </ url>) \ nPer favore correggerlo e riprovare.�.";
 var EN_78 = "Si prega di utilizzare il tasto SELECT per selezionare una Main Room conferenza";
 var EN_80 = "Fai doppio clic sul percorso.";
 var EN_84 = "Inserire un timeout di connessione.";
 var EN_85 = "Si prega di controllare la data e per rientrare.";
 var EN_86 = "Spiacenti, non Resource ha un ID risorsa di";
 var EN_88 = "finestra originale manca questa finestra verr� chiusa \ nSi prega di tornare alla pagina originale e riprovare...";
 var EN_100 = "Il commento non pu� essere pi� lungo di 500 caratteri.";
 var EN_106 = "Attendere mentre elaborare la tua richiesta.";
 var EN_110 = "Inserire informazioni nuovo utente.";
 var EN_111 = "Nuovo utente e l'utente precedente ha la stessa email Si prega di rientrare.�.";
 var EN_115 = "Seleziona un valore in sessione video.";
 var EN_116 = "Il tuo browser non supporta la stampa. Si prega di utilizzare voce di menu per la stampa."
 var EN_127 = "Attenzione:.? � stato modificato il modello di preferenza \ nSalva questi nuovi cambiamenti";
 var EN_151 = "Inserire un nome di categoria.";
 var EN_152 = "Inserire un nome file.";
 var EN_154 = "Spiacente, non � possibile selezionare Sort By Camera se si � scelto Nessuno nelle camere.";
 var EN_155 = "Spiacente, non � possibile selezionare Sort By Camera se si � scelto Nessuno o Qualsiasi nelle camere.";
 var EN_174 = "Sei sicuro di voler rimuovere il file?";
 var EN_175 = "Errore:.. non � riuscito a caricare il file [motivo - cartella] Si prega di fornire questo amministratore";
 var EN_176 = "Errore.: ci sono alcuni file duplicati nella lista dei file upload";
 var EN_180 = "Seleziona almeno una relazione da eliminare.";
 var EN_196 = "Si prega di inserire un corretto MCU Charge Port ISDN Dovrebbe essere un numero positivo con un massimo di due decimali.�.";
 var EN_197 = "Si prega di inserire un costo corretta linea ISDN Dovrebbe essere un numero positivo con un massimo di due decimali.�.";
 var EN_198 = "Si prega di inserire un corretto Charge Port IP Dovrebbe essere un numero positivo con un massimo di due decimali.�.";
 var EN_199 = "Si prega di inserire un costo corretto Line IP Dovrebbe essere un numero positivo con un massimo di due decimali.�.";
 var EN_201 = "Questa pagina � scaduto a causa di inattivit� \ nPer favore login e riprovare.�.";
 var EN_207 = "Spiacente, qualche errore accadere in SIM. Pu� essere causata da Internet o di sistema. \ nRiprovare. Se accade ancora, si prega di avvisare l'amministratore. \ nSIM verr� chiusa ora";
 var EN_209 = "solo caratteri alfanumerici sono consentiti nei nomi dei file.";
 var EN_210 = "Inserire un ID univoco email."

// browser
 var EN_132 = "Il tuo browser ha abilitato popup blocker. Si prega di disabilitarlo per il sito web myVRM, come myVRM utilizza popup che sono necessari per l'immissione delle informazioni.";



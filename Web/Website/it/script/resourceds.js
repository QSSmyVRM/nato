// JavaScript 
// Usage: include collection.js before this.

///////////////////////////////////////////////////////////////////////
// Data Structures for Resource Allocation
///////////////////////////////////////////////////////////////////////

/* room class
function Room(id, name, conference,imgSrc)
{
	this.id = id;
	this.name = name;
	this.conference = conference;
	this.resources = new Collection("Resources", compareName);
	this.photo = new Image();
	this.photo.src = imgSrc;
	this.icons = new Collection("Resource Icons", compareName);
}
*/

// resource class
function Resource(rid, name, qty, imgSrc)
{
	this.rid = rid;
	this.name = name;
	this.qty = qty;
	this.avail = qty;
	this.allocated = 0;
	this.icon = new Image();
	this.icon.src = imgSrc;
	this.toString = resourceToString;
}

function ResPosition(rid, x, y)
{
	this.rid = rid;
	this.x = x;
	this.y = y;
}

function resourceToString()
{
	if (this.avail <= 0)
		return null;
	else if (this.avail == 1)
		return this.name;
	else 
		return this.name + " (" + this.avail + ")";
}

function compareRid(a, b)
{
	if (a.rid > b.rid) 
		return 1;
	else if (a.rid < b.rid)
		return -1;
	else 
		return 0;
}

function compareName(a, b)
{
	var aname = a.name.toUpperCase();
	var bname = b.name.toUpperCase();
	if (aname > bname) 
		return 1;
	else if (aname < bname)
		return -1;
	else 
		return 0;
}

// utilities

function basename(path)
{
	return path.substr(path.lastIndexOf("/")+1);
}


<%@ Page Language="C#" AutoEventWireup="true" Inherits="en_partyinfoinput" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="X-UA-Compatible" content="IE=7" /> <!-- FB 2050 -->
<link title="Expedite base styles" href="<%=Session["OrgCSSPath"]%>" type="text/css"
        rel="stylesheet" />
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>MyVRM</title>
</head>
<script language="JavaScript" src="inc/functions.js"></script>
<script type="text/javascript" src="script/errorList.js"></script>
<!--Added for IP validation Start-->
<script >
function verifyIP (IPvalue) {
errorString = "";
theName = "IPaddress";

var ipPattern = /^(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})$/;
var ipArray = IPvalue.match(ipPattern);


if (ipArray == null)
errorString = errorString + theName + ': '+IPvalue+' non � un indirizzo IP valido.';
else {
for (i = 0; i < 4; i++) {
thisSegment = ipArray[i];
if (thisSegment > 255) {
errorString = errorString + theName + ': '+IPvalue+' non � un indirizzo IP valido.';
i = 4;
}
if ((i == 0) && (thisSegment > 255)) {
errorString = errorString + theName + ': '+IPvalue+' � un indirizzo IP speciale e non pu� essere utilizzato qui.';
i = 4;
      }
   }
}
extensionLength = 3;
if (errorString == "")
{   
    return true;
}
else
{
    alert (errorString);
    return false;
}
}

</script>
<!--Added for IP validation End-->
<script language="JavaScript">
<!--

function assemble(ary, endstr)
{
	var newstr = "";
	for (var i=0; i < ary.length-1; i++) {
		newstr += ary[i] + endstr;
	}
	newstr += ary[ary.length-1];
	return newstr;
}


function frmsubmit(psum)
{

	if (!frmPartyinfoinput_Vlidator(psum))
		return false;
	
	//eno = -1;
	eno = 0;//FB 1888D
	
	pi = opener.document.frmSettings2.txtPartysInfo.value;	
	piary = pi.split("||");//FB 1888
	//piary = pi.split(";");
	for (i=0; i<piary.length-1; i++)
		piary[i] = piary[i].split("!!");//FB 1888
		//piary[i] = piary[i].split(",");

//	alert(psum)
//		alert(document.frmPartyinfoinput.elements[0].value);
	for (j=0; j<psum; j++) {
		email = document.frmPartyinfoinput.elements[++eno].value;
		protocol = document.frmPartyinfoinput.elements[++eno].value;
		connectiontype = document.frmPartyinfoinput.elements[++eno].value;
		address = document.frmPartyinfoinput.elements[++eno].value;
		addresstype = document.frmPartyinfoinput.elements[++eno].value;
		isoutside = document.frmPartyinfoinput.elements[++eno].checked ? "1" : "0";;

		for (i=0; i<piary.length-1; i++) {
			if (email == piary[i][3]) {
				piary[i][10] = protocol;
				piary[i][11] = connectiontype;
				piary[i][13] = address;
				piary[i][14] = addresstype;
				piary[i][15] = isoutside;
				//Added for Addresstype Validation Start				
				if(addresstype==1)
				{if(!verifyIP(address))
				    return false;
				 if(protocol!=0)
				 {
				    alert("Protocollo non valido");
				    return false;
				    }
				}
				else if(addresstype==4)
				{if(verifyIP(address))
				  {alert("Numero ISDN non valido");
				    return false;
				   }
				   if(protocol!=1)
				 {
				    alert("Protocollo non valido");
				    return false;
				    }
				}
				//Added for Addresstype Validation End	
				break;
			}
		}
	}
	//FB 1888 - Starts
	var strArr = "";
	for(var a=0;a<piary.length-1;a++)
	{
	    var tmpAry = piary[a];
	    for(var j=0;j<tmpAry.length;j++)
	    {
	        strArr += tmpAry[j] + "!!";
	    }
	    piary[a] = strArr;
	    strArr = "";
	}
	partysinfo = assemble (piary, "||");
	//partysinfo = assemble (piary, ";");
    //FB 1888 - End
	opener.document.frmSettings2.txtPartysInfo.value = partysinfo;	
	opener.newpartysubmit();

	window.close();
}


function frmPartyinfoinput_Vlidator(psum)
{
	//eno = -1;
	eno = 0;//FB 1888D
	for (i=0; i<psum; i++) {
		// get for validation
		email = document.frmPartyinfoinput.elements[++eno].value;
		protocol = document.frmPartyinfoinput.elements[++eno].value;
		connectiontype = document.frmPartyinfoinput.elements[++eno].value;

		cb = document.frmPartyinfoinput.elements[++eno];
		cb.value = Trim(cb.value);
		if (cb.value == "") {
			alert(EN_123);
			cb.focus();
			return false;
		}

		addresstype = document.frmPartyinfoinput.elements[++eno].value;
		isoutside = document.frmPartyinfoinput.elements[++eno].checked ? "1" : "0";;
		
	}

	return true;
}


//-->
</script>

    

<!-- JavaScript begin -->
<script language="JavaScript">
<!--

	_d = document;
	var mt = "", psum = 0;

	mt += "<form name='frmPartyinfoinput' method='POST'>"
	mt += "  <center>";
	mt += "  <table border='0' cellpadding='1' cellspacing='1' width='98%'>";
	mt += "    <tr>";
	mt += "      <td colspan='8' align='left'>";
	mt += "        <font color='blue'><b>";
	mt += "        Poich� si tratta di una conferenza immediata, abbiamo bisogno di informazioni in pi� di ciascuna delle nuove parti invitate per la connessione.";
	mt += "        Si prega di inserire informazioni. (*: required fields)";
	mt += "        </b></font>";
	mt += "      </td>";
	mt += "    </tr>";
	mt += "    <tr height=10>";
	mt += "      <td colspan='8' align=left><hr width='90%' height='1'></td>";
	mt += "    </tr>";
	mt += "    <tr>";
	mt += "      <td><SPAN class=tableblackblodtext>Nome</SPAN></td>";

	mt += "      <td><SPAN class=tableblackblodtext>Protocollo *</SPAN></td>";
	mt += "      <td><SPAN class=tableblackblodtext>Tipo di <br>connessione *</SPAN></td>";
	mt += "      <td><SPAN class=tableblackblodtext>Indirizzo *</SPAN></td>";
	mt += "      <td><SPAN class=tableblackblodtext>Tipo di <br>indirizzo *</SPAN></td>";
	mt += "      <td><SPAN class=tableblackblodtext>Al di fuori <br>della rete *</SPAN></td>";
	mt += "    </tr>";



	partysinfo = opener.document.frmSettings2.txtPartysInfo.value;
	partysary = partysinfo.split("||");//FB 1888
	//partysary = partysinfo.split(";");

	var addTypeName = "<%=addressTypeNameList%>";
	typeNameary = addTypeName.split(",");
	
	var addTypeID = "<%=addressTypeIDList%>";
	typeIDary = addTypeID.split(",");
	

	for (i=0; i < partysary.length-1; i++) {
		partyary = partysary[i].split("!!");//FB 1888
		//partyary = partysary[i].split(",");

		//if ( (partyary[0]=="new") && (partyary[4]=="1") ) {
		if (partyary[4]=="1") {
			psum ++;
			tdbgcolor = ((psum % 2) == 0) ? "" : "#E0E0E0";
			
			mt += "    <tr>";
			mt += "      <td bgcolor='" + tdbgcolor + "'><a href='mailto: " + partyary[3] + "'>" + partyary[1] + " " + partyary[2] + "</a></td>";
			mt += "      <input type='hidden' name='Email" + psum + "' value='" + partyary[3] + "'>";
			
			mt += "      <td bgcolor='" + tdbgcolor + "'>"
			mt += "        <select size='1' class='altSelectFormat' name='InterfaceType" + psum + "'>"   //Organization Css Module
			mt += "          <option value='0'" + ((partyary[10]=="0") ? " selected" : "") + ">IP</option>"
			mt += "          <option value='1'" + ((partyary[10]=="1") ? " selected" : "") + ">ISDN</option>"

			mt += "        </select>"
			mt += "      </td>"

			mt += "      <td bgcolor='" + tdbgcolor + "'>"
			mt += "        <select size='1' class='altSelectFormat' name='ConnectionType" + psum + "'>"    //Organization Css Module
			mt += "          <option value='1'" + ((partyary[11]=="1") ? " selected" : "") + ">Dial-in</option>"
//			mt += "          <option value='0'" + ((partyary[11]=="0") ? " selected" : "") + ">Dial-out</option>"
			mt += ( parseInt("<%=Session["dialoutEnabled"]%>", 10) == 1) ? "          <option value='0'" + ((partyary[11]=="0") ? " selected" : "") + ">Dial-out</option>" : "";
			mt += "        </select>"
			mt += "      </td>"
			
			mt += "      <td bgcolor='" + tdbgcolor + "'>"
			mt += "        <input type='text' name='Address" + psum + "' size='15' value='" + partyary[13] + "' onKeyUp='javascript:isIPISDN(this);' class='altText'>"
			mt += "      </td>"
			
			mt += "      <td bgcolor='" + tdbgcolor + "'>"
			mt += "        <select size='1' class='altSelectFormat' name='AddressType'" + psum + "'>";    //Organization Css Module
			for (var j=1; j < typeNameary.length; j++) {
				mt += "          <option value='" + typeIDary[j] + "'" + ( (typeIDary[j]==partyary[14]) ? " selected" : "") + ">" + typeNameary[j] + "</option>";
			}
			mt += "        </select>";
			mt += "      </td>";
			
			mt += "      <td bgcolor='" + tdbgcolor + "'>"
			mt += "        <input type='checkbox' name='isOutside' value='1'" + ( parseInt(partyary[15], 10) ? " checked" : "" ) + ">"
			mt += "      </td>"
			mt += "    </tr>"
		}
	}

	mt += "    <tr>";
	mt += "      <td height=30 colspan='8' align=left valign=bottom><hr width='90%' height='1'></td>";
	mt += "    </tr>";
/*	
	mt += "    <tr>";
	mt += "      <td width='80%' colspan='7' align=left>";
	mt += "        <SPAN class=blackItalictext>";
	mt += "        - when ISDN selectioned, ISDN format: (PBX-specific)-(country code)-(area code)-(ISDN number). e.g. 0-1-123-4567890. <a href='http://kropla.com/dialcode.htm' target='_blank'>country code reference</a>"
	mt += "        </SPAN>";
	mt += "      </td>";
	mt += "    </tr>";
*/	
	mt += "  </table>"
	mt += "  </center>"
	
	mt += "  <table border='0' cellpadding='3' cellspacing='0' width='90%'>"
	mt += "    <tr>"
	mt += "      <td align='center'><input type='Reset' value='Reset' name='PartyinfoinputSubmit'  class='altShort4BlueButtonFormat'></td>" //Organization Css Module
	mt += "      <td width='5%'></td>"
	mt += "      <td align='center'><input type='button' value='Annulla' name='PartyinfoinputSubmit' onClick='Javascript: window.close();'  class='altShort4BlueButtonFormat'></td>" //Organization Css Module
	mt += "      <td width='5%'></td>"
	mt += "      <td align='center'><input type='button' value='Invia' name='PartyinfoinputSubmit' onClick='Javascript: frmsubmit(psum);'  class='altShort4BlueButtonFormat'></td>" //Organization Css Module
	mt += "    </tr>"
	mt += "  </table>"
	mt += "  </center>"

	mt += "</form>"
	
	_d.write(mt)


//-->
</script>
<!-- JavaScript finish -->
  

</body>

</html>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
<%@ Page Language="C#" AutoEventWireup="true" Inherits="en_ConfirmTemplate" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <!-- FB 2050 -->
<meta http-equiv="X-UA-Compatible" content="IE=7" /> <!-- FB 2050 -->
<!-- #INCLUDE FILE="inc/maintopNET.aspx" --> 
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Conferma modello</title>
</head>
<body>
          <script language="JavaScript">
<!--

	function frmsubmit(opr)
	{
		switch (opr) {
			case "MODIFY":
				//document.frmConfirmtemplate.action = "dispatcher/userdispatcher.asp";
				document.frmConfirmtemplate.action = "managetemplate2.aspx?tid=<%=templateID%>&cmd=GetOldTemplate";
				document.frmConfirmtemplate.cmd.value="GetOldTemplate";
				break;
			case "LIST":
				//document.frmConfirmtemplate.action = "dispatcher/conferencedispatcher.asp?cmd=GetTemplateList&frm=manage";
				document.frmConfirmtemplate.action = "ManageTemplate.aspx";
				//document.frmConfirmtemplate.cmd.value="GetTemplateList";
				break;
		}
		
		document.frmConfirmtemplate.opr.value = opr;
		document.frmConfirmtemplate.submit ();
	}


//-->
</script>
<div id="TempOK" runat="server" style="display:block">

            <center>
              <h3>Congratulazioni <% =userName %>!</h3>
            </center>            
			<br/>
            
            <center>
			<table width="90%" border="0" cellspacing="2" cellpadding="4">
              <tr> 
              <%--Window Dressing--%>
                <td colspan="3"  class="lblMessage" align="center"><%--FB 2487--%>
                  <p><b><%if(Application["Client"].ToString().ToUpper() == "MOJ") {%>Il tuo modello � stato inviato correttamente. Si pu� usare per la creazione di conferenze in seguito.<%}else{ %> Il tuo modello � stato inviato correttamente. Si pu� usare per la creazione di conferenze in seguito.<%} %></b></p><%--Edited for FB 1428--%>
                </td>
              </tr>
              <tr>
                <td align="center" colspan="3">
                    <asp:Label ID="errLabel" runat="server" Text="" ForeColor="red" CssClass="lblError"></asp:Label>
                </td>
              </tr>
              <tr> 
                <td colspan="3">&nbsp;</td>
              </tr>
              
              <tr> 
                <td width="5%">&nbsp;</td>
              <%--Window Dressing--%>
                <td width="30%" align="left" class="subtitlexxsblueblodtext"><b>Riepilogo dei modelli</b></td>
                <td width="65%">&nbsp; </td>
              </tr>
			  <tr> 
                <td></td>
              <%--Window Dressing--%>
                <td align="left" class="blackblodtext">Nome modello</td>
                <td align="left">
                  <% =templateName %> &nbsp;&nbsp; 
				  <font color="darkblue"><b><i>

<% 
	if (templatePublic == "1" )
		Response.Write ("pubblico");
	else
		Response.Write ("privato");
	
%>

                  </i></b></font>
                </td>
              </tr>
 			  <tr> 
                <td></td>
              <%--Window Dressing--%>
                <td align="left" class="blackblodtext">Descrizione modello</td>
                <td align="left"><% =templateDescription %></td>
              </tr>
              <tr> 
                <td></td>
                <td></td>
                <td></td>
              </tr>
              
              <tr> 
                <td></td>
              <%--Window Dressing--%>
                <td align="left" class="subtitlexxsblueblodtext"><b><%if(Application["Client"].ToString().ToUpper() == "MOJ") {%>Hearing Summary<%}else{ %> Sintesi della conferenza.<%} %></b></td><%--Edited for FB 1428--%>
                <td>&nbsp; </td>
              </tr>
              <tr> 
                <td></td>
              <%--Window Dressing--%>
                <td align="left" class="blackblodtext">Nome</td>
                <td align="left">
                  <% =confName %>&nbsp;&nbsp; 
				  <font color="darkblue"><b><i>

<% 
	if (publicConf == "1")
		Response.Write ("pubblico");
	else
		Response.Write ("privato");
	
%>

                  </i></b></font>
                </td>
              </tr>
              <%if(!(Application["Client"].ToString().ToUpper() == "MOJ")){%><%--Added for MOJ Phase 2 QA --%>
			  <tr> 
                <td></td>
              <%--Window Dressing--%>
                <td align="left" class="blackblodtext">Password</td>
                <td align="left"><% =confPassword %></td>
              </tr>
              <tr> 
              <%} %><%--Added for MOJ Phase 2 QA --%>
                <td></td>
              <%--Window Dressing--%>
                <td align="left" class="blackblodtext"><%if(Application["Client"].ToString().ToUpper() == "MOJ") {%>Hearing Description<%}else{ %>Descrizione della Conferenza<%} %></td><%--Edited for FB 1428--%>
                <td align="left"><% =description %></td>
              </tr>
              <tr> 
                <td></td>
              <%--Window Dressing--%>
                <td align="left" class="blackblodtext">Durata</td>
                <td align="left">
                <%
	                dhour = (Int32)durationMin / 60;
                    dmin =(Int32)durationMin - dhour * 60;

	                //Code added for FB 1216 - start
                    if (dhour == 0)
	                    Response.Write(dmin  + " minuto(s)");
                    else if( dmin == 0 )
	                    Response.Write(dhour + " ora/e");
	                else
	                    Response.Write(dhour + " ora/e e " + dmin  + " minuto(s)");
                	
                    //Code added for FB 1216 - end
                %>
                </td>
              </tr>
              
			  <tr> 
                <td></td>
              <%--Window Dressing--%>
                <td align="left" class="blackblodtext">Posizione</td>
                <td align="left"><% =locations %></td>
              </tr>           
              <tr id="trPart" runat="server"> <%--Added for FB 1425 QA Bug--%>
                <td></td>
              <%--Window Dressing--%>
                <td valign="top" align="left" class="blackblodtext">Elenco Partecipanti</td>
                <td align="left"><table border=0 cellspacing=2 cellpadding=0><% =invited + invitee + cc %></table></td>
              </tr>
              <tr> 
                <td></td>
                <td></td>
                <td></td>
              </tr>              
              <tr> 
                <td></td>
              <%--Window Dressing--%>
                <td align="left" class="subtitlexxsblueblodtext"><b>Informazioni aggiuntive</b></td>
                <td></td>
              </tr>
              <tr> 
                <td></td>
              <%--Window Dressing--%>
                <td align="left" class="blackblodtext"> 
                  Numero verde di assistenza
                </td>
                <td align="left"><%=Application["contactPhone"]%></td>
              </tr>
            </table>
            </center>
            
			<br><br>


			<form name="frmConfirmtemplate" method="POST" action="">
			  <input type="hidden" name="cmd" value="">
			  <input type="hidden" name="templateID" value="<% =templateID %>">
			  <input type="hidden" name="opr" value="">
			  
			  <div align="center">
	            <table>
                  <tr> 
                    <td align="center"> 
					  <input type="button" runat="server" id="BtnEdit" name="ConfirmTemplateSubmit" value="Modifica impostazioni modello" class="altLongBlueButtonFormat" onclick="JavaScript: frmsubmit('MODIFY');">
                    </td>
                    <td width="10%">&nbsp;</td>
                    <td align="center"> 
                      <%--code added for Soft Edge button--%>                    
					  <input type="button" onfocus="this.blur()" name="ConfirmTemplateSubmit0" value="Torna alla lista modelli" class="altLongBlueButtonFormat" onclick="JavaScript: frmsubmit('LIST');">
                    </td>
                  </tr>
                </table>
              </div>	
              
    </div>  
 <div id="divError" runat="server" style="display:none">
  <center>
 <table width="90%" border="0" cellspacing="2" cellpadding="4">
  <tr>
    <td align="center" height="10">
    </td>
  </tr>
  <tr>
    <td align="center">
      <font size="4"><b> La tua richiesta non pu� essere completata a causa del seguente motivo:</b></font>
    </td>
  </tr>
  <tr>
    <td align="center" height="20">
    </td>
  </tr>
    <tr>
        <td align="center">
        <font size='3' color='red'><b>
        Non � consentito modificare questo modello..<br/>
        Si prega di contattare l'amministratore VRM per ulteriore assistenza.
        </b>
        </font>
        </td>
    </tr>
    <tr>
    <td align="center" height="20">
    </td>
  </tr>
  </table>
  </center>
 </div>
   
<script language="JavaScript">
<!--
 
if(document.getElementById("TempOK").style.display=='block')
    {
    document.frmConfirmtemplate.ConfirmTemplateSubmit0.focus ();
    document.getElementById("divError").style.display='none';
    }
    //Added for FB 1425 QA Bug START
    if('<%=Application["Client"].ToString().ToUpper()%>' =="MOJ")
	document.getElementById("trPart").style.display ="none";
	//Added for FB 1425 QA Bug END
    

//-->
</script>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->			


</body>
</html>

﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="ns_myVRMNet.en_ManageEmailDomain" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- #INCLUDE FILE="inc/maintopNET.aspx" --> 
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Gestisci dominio di posta elettronica</title>
</head>
<body>
    <form id="frmEmailDomain" runat="server" method="post" >
    <input type="hidden" id="hdnDomainID" runat="server" />
    <input type="hidden" id="hdnDomainStatus" runat="server" />
    <div>
        <table width="100%">
            <tr>
                <td align="center">
                    <h3>
                        <asp:Label ID="lblHeader" runat="server"></asp:Label>
                    </h3>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <table cellspacing="5">
                        <tr>
                            <td >&nbsp;</td>
                            <td>
                                <span class=subtitleblueblodtext>Domini di posta elettronica esistenti</span>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:DataGrid ID="dgEmailDomain" runat="server" AutoGenerateColumns="False" CellPadding="4" GridLines="None"
                         BorderColor="blue" BorderStyle="solid" BorderWidth="1" ShowFooter="true" OnItemDataBound="BindEmailDomain"
                          OnEditCommand="EditEmailDomain" OnUpdateCommand="UpdateEmailDomain" OnCancelCommand="CancelEmailDomain" Width="65%" Visible="true" style="border-collapse:separate">
                        <SelectedItemStyle  CssClass="tableBody"  Font-Bold="True" />
                        <EditItemStyle CssClass="tableBody" />
                        <AlternatingItemStyle CssClass="tableBody" />
                        <ItemStyle CssClass="tableBody" />
                        <FooterStyle CssClass="tableBody" />
                        <HeaderStyle CssClass="tableHeader" />
                        <Columns>
                            <asp:BoundColumn DataField="DomainID" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="isActive" Visible="false"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="Nome Azienda" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"> <%-- FB 2050 --%>
                                <ItemTemplate>
                                    <asp:Label ID="lblCompanyName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Companyname") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtCompanyName" CssClass="altText" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Companyname")%>'></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="reqCompanyName" ControlToValidate="txtCompanyName" runat="server" ErrorMessage="Richiesto" Display="dynamic" ValidationGroup="Update" ></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegExpCompanyName" ControlToValidate="txtCompanyName" Display="dynamic" runat="server" ValidationGroup="Update" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ e &#34; sono caratteri non validi." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Dominio di posta elettronica" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"> <%-- FB 2050 --%>
                                <ItemTemplate>
                                    <asp:Label ID="lblEmailDomain" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Emaildomain") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtEmailDomain" CssClass="altText" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Emaildomain")%>'></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="reqEmailDomain" ControlToValidate="txtEmailDomain" runat="server" ErrorMessage="Richiesto" Display="dynamic" ValidationGroup="Update" ></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegExpEmailDomain" ControlToValidate="txtEmailDomain" Display="dynamic" runat="server" ValidationGroup="Update" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ e &#34; sono caratteri non validi." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Azioni" ItemStyle-Width="15%">
                                <HeaderStyle CssClass="tableHeader" />
                                <ItemTemplate>
                                    <asp:LinkButton runat="server" Text="Modifica" ID="btnEdit" CommandName="Edit"></asp:LinkButton>
                                    <asp:LinkButton runat="server" ID="btnStatus" Text=""></asp:LinkButton>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:LinkButton runat="server" Text="Aggiorna" ID="btnUpdate" CommandName="Update" ValidationGroup="Update"></asp:LinkButton>
                                    <asp:LinkButton runat="server" Text="Annulla" ID="btnCancel" CommandName="cancel" ValidationGroup="Update"></asp:LinkButton>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:Table runat="server" ID="tblNoEmailDomain" Visible="false" Width="90%">
                        <asp:TableRow CssClass="lblError">
                            <asp:TableCell CssClass="lblError">
                                Nessun dominio di posta elettronica trovato.
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>                    
                </td>
            </tr>
            <tr>
                <td align="left">
                    <table cellspacing="5">
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <span class=subtitleblueblodtext><asp:Label ID="lblCreateEditDepartment" runat="server" Text="Crea Nuovo "></asp:Label> Dominio di posta elettronica</span>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table border="0" cellpadding="5" cellspacing="5" width="90%">
                        <tr>
                            <td align="right" width="20%" class="blackblodtext">
                                Nome Azienda: </td>
                            <td align="left"height="21" style="font-weight:bold" width="490px">
                                <asp:TextBox ID="txtNewCompanytName" runat="server" CssClass="altText" Width="200px" ></asp:TextBox>
                                <asp:Label ID="lblRequired" CssClass="lblError" runat="server"></asp:Label>
                                <asp:RequiredFieldValidator ID="reqCompanytName" ControlToValidate="txtNewCompanytName" ErrorMessage="Richiesto" ValidationGroup="Submit" runat="server" Display="dynamic"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegExpCompanytName" ControlToValidate="txtNewCompanytName" Display="dynamic" runat="server" ValidationGroup="Submit" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ e &#34; sono caratteri non validi." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                            </td>
                            <td></td>
                            </tr>
                            <tr>
                            <td align="right" width="20%" class="blackblodtext">
                                Dominio di posta elettronica:</td>
                            <td align="left"height="21" style="font-weight:bold" width="490px">
                                <asp:TextBox ID="txtNewEmailDomain" runat="server" CssClass="altText" Width="200px" ></asp:TextBox>
                                <asp:Label ID="lblRequired2" CssClass="lblError" runat="server"></asp:Label>
                                <asp:RequiredFieldValidator ID="ReqEmailDomain" ControlToValidate="txtNewEmailDomain" ErrorMessage="Richiesto" ValidationGroup="Submit" runat="server" Display="dynamic"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegExpEmailDomain" ControlToValidate="txtNewEmailDomain" Display="dynamic" runat="server" ValidationGroup="Submit" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ e &#34; sono caratteri non validi." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                            </td>
                            <td align="left"height="21" style="font-weight:bold" width="0.5%">
                                <asp:Button runat="server" ID="btnSaveDomain" Text="Invia" CssClass="altShortBlueButtonFormat" OnClick="SaveDomain" ValidationGroup="Submit" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
<script type="text/javascript">
    document.getElementById("txtNewCompanytName").value = "";
    document.getElementById("txtNewEmailDomain").value = "";
    
    function DomainStatus(id, status) 
    {
      document.getElementById("hdnDomainID").value = id;
      var domainStatus =  document.getElementById("hdnDomainStatus");
      if (status == "1")
          domainStatus.value = "0";
      else
          domainStatus.value = "1";

      document.getElementById("txtNewCompanytName").value = ".";
      document.getElementById("txtNewEmailDomain").value = ".";
      if (document.getElementById("btnSaveDomain"))
          document.getElementById("btnSaveDomain").click();

      return false;
    }

</script>

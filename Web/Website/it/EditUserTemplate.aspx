<%@ Page Language="C#" Inherits="ns_EditUserTemplates.UserTemplates" Buffer="true" ValidateRequest="false" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <!-- FB 2050 -->
<meta http-equiv="X-UA-Compatible" content="IE=7" /> <!-- FB 2050 -->
<!-- Window Dressing -->
<!-- #INCLUDE FILE="inc/maintopNET.aspx" --> 
<script type="text/javascript">
  var servertoday = new Date();
</script>
<script type="text/javascript" src="inc/functions.js"></script>
<%--FB 1861--%>
<%--<script type="text/javascript" src="script/cal.js"></script>--%>
<script type="text/javascript" src="script/cal-flat.js"></script>
<script type="text/javascript" src="lang/calendar-en.js"></script>
<script type="text/javascript" src="script/calendar-setup.js"></script>
<script type="text/javascript" src="script/calendar-flat-setup.js"></script>
<link rel="stylesheet" type="text/css" media="all" href="css/calendar-blue.css" /> <%--FB 1861--%> <%--FB 1982--%>

<script type="text/javascript" src="script/mytreeNET.js"></script>

<script runat="server">

</script>
<script language="javascript">
//<%--FB 481 Saima--%>
function DisableButton()
{    
      if(document.getElementById("txtAccountExpiry").value == "")
      {
          document.getElementById("lblExpError").style.display = "";
          document.getElementById("lblExpError").innerHTML = "Richiesto";
      }
      else
      {
         document.getElementById("lblExpError").innerHTML="";
      }
      
      if (typeof(Page_ClientValidate) == 'function') 
      if (Page_ClientValidate())
      {
            if(!CheckDate(document.getElementById("txtAccountExpiry").value))  //Added for FB issue 1493
            return false;
            DataLoading(1);
            document.getElementById("<%=btnSubmit.ClientID %>").style.display="none";
            document.getElementById("<%=btnSubmitNew.ClientID %>").style.display="none";
            return true;
      }
      
}
//Code added for FB issue 1493 - Start
function CheckDate(obj)
{
//debugger;
//    var licenseExp = GetDefaultDate('<%=licenseDate%>','<%=format%>'); // FB 1747
    var accExpDate = GetDefaultDate(document.getElementById("txtAccountExpiry").value,'<%=format%>');

      
      if(document.getElementById("txtAccountExpiry").value == "")
      {
          document.getElementById("lblExpError").style.display = "";
          document.getElementById("lblExpError").innerHTML = "Richiesto";
                 
          return false;
      }
      else
      {
         document.getElementById("lblExpError").innerHTML="";
      }
      // FB 1747 - Commented (Start)
//      if(Date.parse(accExpDate) > Date.parse('<%=licenseDate%>'))
//      {
//          document.getElementById("lblExpError").style.display = "";
//          document.getElementById("lblExpError").innerHTML = "Invalid Date<br>Maximum date allowed is the<br>site license expiry date ("+licenseExp+")";
//                 
//          return false;
//      }
//      else
//      {
//         document.getElementById("lblExpError").innerHTML="";
//      }
//      
// FB 1747 - Commented (End)
      if (Date.parse(accExpDate) <= Date.parse(new Date()))
      {
           document.getElementById("lblExpError").style.display = "";
           document.getElementById("lblExpError").innerHTML = "Data non valida";
            
           return false;
            
      } 
       else
      {
         document.getElementById("lblExpError").innerHTML="";
      }
    
    return true;  
    
}
//Code added for FB issue 1493 - End
</script>
<script type="text/javascript" src="script/RoomSearch.js"></script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<body>
    <form id="frmInventoryManagement" runat="server" method="post" onsubmit="return true">
      <input type="hidden" id="helpPage" value="105">
      <%--Code changed for FB 1425 QA Bug -Start--%>
      <input type="hidden" id="hdntzone" runat="server"/>
      <%--Code changed for FB 1425 QA Bug -End--%>
    <input name="selectedloc" type="hidden" id="selectedloc" runat="server"  /> <!--Added room search-->
     <input name="locstrname" type="hidden" id="locstrname" runat="server"  /> <!--Added room search-->
<b>
    <div>
        <table width="100%">
            <tr>
                <td align="center">
                    <h3>
                        <asp:Label ID="lblHeader" runat="server" Text=""></asp:Label>
                    </h3>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError"></asp:Label>
                    <div id="dataLoadingDIV" style="z-index:1"></div>
                </td>
            </tr>
        </table>
        <table border="0" width="100%">
            <tr>
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <SPAN class=subtitleblueblodtext>Nome modello</SPAN>
                            </td>
                            <td>
                                <asp:TextBox CssClass="altText" runat="server" ID="txtTemplateName" Text="" Width="200"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reqName" runat="server" ControlToValidate="txtTemplateName" ErrorMessage="Richiesto" Display="dynamic"></asp:RequiredFieldValidator>
                                 <asp:RegularExpressionValidator ID="regTemplateName" ControlToValidate="txtTemplateName" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ e &#34; sono caratteri non validi." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                    </table>
                </td>
                <td align="left" rowspan="6" valign="top" class="blackblodtext">
                    Posizione preferita di riunione
                    <table>
                    <tr>
                        <td align="right" valign="top" style="width:10%">
                        <input name="opnRooms" type="button" id="opnRooms" onclick="javascript:OpenRoomSearch('frmInventoryManagement');" value="Aggiungi stanza" class="altShortBlueButtonFormat" />
                    
                    <input name="addRooms" type="button" id="addRooms" onclick="javascript:AddRooms();" style="display:none;" /><br />
                    <span class="blackblodtext"> <font size="1">Fai doppio clic sulla stanza per rimuoverla dall'elenco.</font></span>
                        </td>
                        <td align="right" style="width:90%">
                        <select size="4" wrap="false" name="RoomList" id="RoomList" class="treeSelectedNode" onDblClick="javascript:Delroms(this.value)"  style="height:350px;width:100%;" runat="server"></select>
                         
                        </td>
                    </tr>
                </table>
                     <asp:RadioButtonList style="display:none;" ID="rdSelView" runat="server" OnSelectedIndexChanged="rdSelView_SelectedIndexChanged"
                          RepeatDirection="Horizontal" AutoPostBack="True" RepeatLayout="Flow" CssClass="blackblodtext">
                          <%--Window Dressing start --%>
                          <asp:ListItem Selected="True" Value="1"><font class="blackblodtext">Vista livello</font></asp:ListItem>
                          <asp:ListItem Value="2"><font class="blackblodtext">Vista elenco</font></asp:ListItem>
                          <%--Window Dressing end --%>
                    </asp:RadioButtonList><br />
                    <asp:Panel  style="display:none;" ID="pnlLevelView" runat="server" Height="550" Width="100%" ScrollBars="Auto" BorderColor="Blue" BorderStyle="Solid" BorderWidth="1px" HorizontalAlign="Left">
                        <asp:TreeView ID="treeRoomSelection" runat="server" BorderColor="White" Height="90%" ShowCheckBoxes="Leaf"
                            ShowLines="True" Width="95%" onclick="javascript:getOneRoom(event)">
                            <NodeStyle Font-Size="Smaller" />
                            <RootNodeStyle BorderStyle="None" Font-Size="Smaller" ForeColor="Blue" />
                            <SelectedNodeStyle />
                            <ParentNodeStyle BorderStyle="None" ForeColor="#404040" />
                            <LeafNodeStyle Font-Size="Smaller" />
                        </asp:TreeView>
                        </asp:Panel>
                    <asp:Panel style="display:none;" ID="pnlListView" runat="server" BorderColor="Blue" BorderStyle="Solid"
                            BorderWidth="1px" Height="500" ScrollBars="Auto" Visible="False" Width="100%" HorizontalAlign="Left" Direction="LeftToRight" Font-Bold="True" Font-Names="Verdana" Font-Size="Small" ForeColor="Green">
                            <asp:RadioButtonList ID="lstRoomSelection" runat="server" Height="95%" Width="95%" Font-Size="Smaller" ForeColor="ForestGreen" Font-Names="Verdana" RepeatLayout="Flow">
                            </asp:RadioButtonList>
                        </asp:Panel>
                        <%--Added for Location Issues  - Start--%>
                        <asp:Panel style="display:none;" ID="pnlNoData" runat="server" BorderColor="Blue" BorderStyle="Solid" BorderWidth="1px"
                            Height="300px" ScrollBars="Auto" Visible="False" Width="100%" HorizontalAlign="Left"
                            Direction="LeftToRight" Font-Size="Small">
                            <table>
                                <tr align="center">
                                    <td>
                                        Non hai stanze disponibili
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <%--Added for Location Issues  - End--%>
                </td>
            </tr>
           <tr>
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <SPAN class=subtitleblueblodtext>Opzioni personali</SPAN>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center" valign="top">
                    <table width="70%" cellpadding="0" cellspacing="3" border="0">
                        <%--Code changed for FB 1425 QA Bug -Start--%>
                        <tr id="TzTR" runat="server">
                     <%--Code changed for FB 1425 QA Bug -End--%>
                          <%--Window Dressing --%>
                            <td align="right" width="20%" class="blackblodtext">
                                Fuso orario
                            </td>
                            <td align="left" width="40%">
                                <asp:DropDownList ID="lstTimeZone" runat="server" CssClass="altLong0SelectFormat" DataTextField="timezoneName" DataValueField="timezoneID">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator runat="server" ID="reqTZ" InitialValue="-1" ErrorMessage="Richiesto" Display="dynamic" ControlToValidate="lstTimeZone" ></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                          <%--Window Dressing --%>
                            <td align="right" class="blackblodtext">
                                Rubrica personale preferita
                            </td>
                            <td align="left">
                                 <asp:DropDownList ID="lstAddressBook" runat="server" CssClass="altLong0SelectFormat">
                                    <asp:ListItem Value="0" Text="None"></asp:ListItem>
                                    <asp:ListItem Value="1" Text="MS Outlook"></asp:ListItem>
<%--                                    <asp:ListItem Value="2" Text="Lotus Notes 6.x"></asp:ListItem>
--%>                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                          <%--Window Dressing --%>
                            <td align="right" class="blackblodtext">
                                Gruppo predefinito
                            </td>
                            <td align="left">
                                <asp:DropDownList ID="lstGroup" runat="server" CssClass="altLong0SelectFormat" OnLoad="GetGroups" DataTextField="groupName" DataValueField="groupID">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                          <%--Window Dressing --%>
                            <td align="right" class="blackblodtext">
                               Ruolo utente
                            </td>
                            <td align="left">
                                 <asp:DropDownList ID="lstUserRole" runat="server" CssClass="altLong0SelectFormat" DataTextField="Name" DataValueField="ID">
                                </asp:DropDownList>
                           </td>
                        </tr>
                        <tr>
                          <%--Window Dressing --%>
                            <td align="right" class="blackblodtext">
                                Scadenza account
                            </td>
                            <td align="left">
                            
                                <asp:TextBox CssClass="altText" runat="server" onfocusout="javascript:CheckDate(this)" ID="txtAccountExpiry"></asp:TextBox> <%-- Code Changed for FB issue 1493 --%>
                               
                                <%-- Code changed by Offshore for FB Issue 1073 -- start --%>
                            
                                <img alt="CalImg" src="image/calendar.gif" border="0" width="20"  id="cal_triggerd" style="cursor: pointer;height:20;vertical-align:bottom" title="Data di selezione" onclick="return showCalendar('<%=txtAccountExpiry.ClientID %>', 'cal_triggerd', 1, '<%=format%>');" />
                                
                                <%-- Code changed by Offshore for FB Issue 1073 -- End--%>
                                <%--<asp:RequiredFieldValidator runat="server"  ID="reqExpiryDate" ControlToValidate="txtAccountExpiry" ErrorMessage="Richiesto" Display="dynamic" ></asp:RequiredFieldValidator>--%>
                                <%-- Code added for FB issue 1493 - Start --%>
                                <span id="lblExpError" style="color:Red;display:none;"></span>
                       
                                <%--<asp:RangeValidator ID="rangeExpiryDate" runat="server" Display="dynamic" ErrorMessage="Invalid Date" ControlToValidate="txtAccountExpiry" Type="Date"></asp:RangeValidator>--%>
                                <%-- Code added for FB issue 1493 - End --%>
                            </td>
                        </tr>
                        <tr>
                          <%--Window Dressing --%>
                            <td align="right" class="blackblodtext">
                                Tempo iniziale di portafoglio
                            </td>
                            <td align="left">
                                <asp:TextBox CssClass="altText" runat="server" ID="txtInitialTime" Text=""></asp:TextBox>
                                (minuti)
                                <asp:RequiredFieldValidator ID="reqMinutes" SetFocusOnError="true" runat="server" ErrorMessage="Richiesto" CssClass="lblError" ControlToValidate="txtInitialTime" ></asp:RequiredFieldValidator>
                                <asp:RangeValidator ID="ValidatorTimeremaining" SetFocusOnError="true" runat="server" CssClass="lblError" Display="dynamic" ControlToValidate="txtInitialTime"
                                    ErrorMessage="<br>Si prega di inserire un valore compreso tra 0 e 2000000000." MaximumValue="2000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>                            </td>
                           </td>
                        </tr>
                        <tr>
                          <%--Window Dressing --%>
                            <td align="right" class="blackblodtext">
                                Notifica e-mail
                            </td>
                            <td align="left">
                                <asp:CheckBox runat="server" ID="chkEmailNotification" />
                            </td>
                        </tr>
                        <tr>
                          <%--Window Dressing --%>
                            <td align="right" valign="top" class="blackblodtext">
                                Reparto preferito
                            </td>
                            <td align="left">
                                <asp:DropDownList ID="lstDepartment" runat="server" CssClass="altLong0SelectFormat" OnInit="LoadDepartments" DataTextField="name" DataValueField="id">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr><%--FB 1830--%>
                          <%--Window Dressing --%>
                            <td align="right" class="blackblodtext">
                                Lingua preferita
                            </td>
                            <td align="left">
                                 <asp:DropDownList ID="lstLanguage" runat="server" CssClass="altLong0SelectFormat" DataTextField="name" DataValueField="ID">
                                 </asp:DropDownList>
                                 <asp:RequiredFieldValidator ID="reglstLanguage" ControlToValidate="lstLanguage" ErrorMessage="Richiesto" InitialValue="-1" Display="dynamic" runat="server"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
           <tr id="trAudvid" runat="server"><%--Added for MOJ Phase 2 QA--%>
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <SPAN class=subtitleblueblodtext>Parametri di connessione Audio/Video</SPAN>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
                <td align="center" id="trTbAudvid" runat="server"><%--Added for MOJ Phase 2 QA--%>
                    <asp:CustomValidator runat="server" Display="dynamic" ID="cusVal1" SetFocusOnError="true" OnServerValidate="ValidateIPAddress" CssClass="lblError"></asp:CustomValidator>
                    <table width="70%" cellpadding="0" cellspacing="3" border="0">
                        <tr>
                          <%--Window Dressing --%>
                            <td align="right" width="20%" class="blackblodtext">
                               Tasso di linea predefinito
                            </td>
                            <td align="left" width="40%">
                                <asp:DropDownList ID="lstLineRate" runat="server" CssClass="altLong0SelectFormat" DataTextField="lineRateName" DataValueField="lineRateID">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                          <%--Window Dressing --%>
                            <td align="right" class="blackblodtext">
                                Tipo di connessione predefinito
                            </td>
                            <td align="left">
                                 <asp:DropDownList ID="lstConnectionType" CssClass="altLong0SelectFormat" runat="server" DataTextField="Name" DataValueField="ID" ></asp:DropDownList> <%--Fogbugz case 427--%>
                            </td>
                        </tr>
                        <tr>
                          <%--Window Dressing --%>
                            <td align="right" class="blackblodtext">
                                Protocollo predefinito
                            </td>
                            <td align="left">
                                <asp:DropDownList ID="lstProtocol" runat="server" CssClass="altSelectFormat" DataTextField="Name" DataValueField="ID"></asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                          <%--Window Dressing --%>
                            <td align="right" class="blackblodtext">
                                Indirizzo IP/ISDN predefinito <%--&nbsp;<span class="reqfldstarText">*</span>--%>
                            </td>
                            <td align="left">
                                 <asp:TextBox CssClass="altText" runat="server" ID="txtIPISDNAddress" Text=""></asp:TextBox>
                                 <asp:RequiredFieldValidator ID="req1" Enabled="false" ErrorMessage="Richiesto" ControlToValidate="txtIPISDNAddress" runat="server" Display="dynamic"></asp:RequiredFieldValidator> <%--Fogbugz case 375 enabled=false--%>
                                 <%--FB 1972--%>
                                 <asp:RegularExpressionValidator ID="RegularExpressionValidator2" ControlToValidate="txtIPISDNAddress" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > + % \ / ( ) ? | ^ = ! ` [ ] { } # $ and ~ sono caratteri non validi." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+?|!`\[\]{}\=^#$%&()~]*$"></asp:RegularExpressionValidator>
                                 <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txtIPISDNAddress" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ and &#34; are invalid characters." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()'~]*$"></asp:RegularExpressionValidator>--%>
                          </td>
                        </tr>
                        <tr>
                          <%--Window Dressing --%>
                            <td align="right" class="blackblodtext">
                                MCU assegnato
                            </td>
                            <td align="left">
                                <asp:DropDownList ID="lstMCU" runat="server" CssClass="altLong0SelectFormat" DataTextField="name" DataValueField="ID">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                          <%--Window Dressing --%>
                            <td align="right" class="blackblodtext">
                                Attrezzature predefinite
                            </td>
                            <td align="left">
                                <asp:DropDownList ID="lstEquipment" runat="server" CssClass="altLong0SelectFormat" OnInit="LoadEquipment" DataTextField="videoEquipmentName" DataValueField="videoEquipmentID">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                          <%--Window Dressing --%>
                            <td align="right" class="blackblodtext">
                                Al di fuori della rete
                            </td>
                            <td align="left">
                                <asp:CheckBox runat="server" ID="chkOutsideNetwork" />
                            </td>
                        </tr>
                    </table>
                </td>
        </table>
        <table width="100%">
            <tr>
                <td align="center">
                    <table width="90%" cellpadding="0" cellspacing="3" border="0">
                        <tr>
                            <td align="center">
                                <asp:Button ID="benReset" Text="Reset" CssClass="altLongBlueButtonFormat" runat="server" OnClientClick="DataLoading(1)" OnClick="ResetTemplate" ValidationGroup="Reset" /> <!-- FB Case 229 - Saima - Validation Group added for reset to avoid triggering validation on controls -->
<%--                                <input type="reset" value="Reset" id="btnReset" class="altLongBlueButtonFormat" runat="server" />
--%>                            </td>
                            <td align="center">
                                <asp:Button runat="server" CssClass="altLongBlueButtonFormat" Text="Invia/Nuovo modello utente" OnClick="SubmitNew" ID="btnSubmitNew" OnClientClick="javascript:DisableButton()" /><br />
                            </td>
                            <td align="center">
                                <asp:Button runat="server" CssClass="altLongBlueButtonFormat" Text="Invia" OnClick="SubmitOnly" ID="btnSubmit" OnClientClick="javascript:return DisableButton()" /><%-- FB issue 1493 --%>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
<input type="hidden" id="hdnLocation" />
<img src="keepalive.asp" name="myPic" width="1px" height="1px">
</b>
    </form>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
    <!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
</body>
</html>


<%@ Page Language="C#" Inherits="ns_MyVRM.Template" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <!-- FB 2050 -->
<meta http-equiv="X-UA-Compatible" content="IE=7" /> <!-- FB 2050 -->

<!--Window Dressing-->
<!-- #INCLUDE FILE="inc/maintopNET.aspx" --> 
<script runat="server">

</script>
<script type="text/javascript" src="script/mousepos.js"></script>
<script type="text/javascript" src="script/managemcuorder.js"></script>

<script language="javascript">

	function ManageOrder ()
	{
		change_mcu_order_prompt('image/pen.gif', 'Gestisci ordine dei modelli', document.frmManagebridge.Bridges.value, "Modelli");
	}
	function CreateNewConference(confid)
    {
//        window.location.href = "aspToAspNet.asp?tp=ConferenceSetup.aspx&t=t&confid=" + confid; //Login Management
          window.location.href = "ConferenceSetup.aspx&t=t&confid=" + confid;
    }
	function showTemplateDetails(tid)
	{
//		popwin = window.open("dispatcher/conferencedispatcher.asp?cmd=GetTemplate&f=td&tid=" + tid,'templatedetails','status=yes,width=750,height=400,scrollbars=yes,resizable=yes')
        popwin = window.open("TemplateDetails.aspx?nt=1&f=td&tid=" + tid,'templatedetails','status=yes,width=750,height=400,scrollbars=yes,resizable=yes') //Login Management
		if (popwin)
			popwin.focus();
		else
			alert(EN_132);
	}
	function frmsubmit(save, order)
	{
	    document.getElementById("__EVENTTARGET").value="ManageOrder";
	    document.frmManagebridge.submit();
	}
</script>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
 
    <title>I miei modelli</title>
    <script type="text/javascript" src="inc/functions.js"></script>

</head>
<body>
    <form id="frmManagebridge" runat="server" method="post" onsubmit="return true">
    <div>
      <input type="hidden" id="helpPage" value="73">

        <table width="100%">
            <tr>
                <td align="center">
                    <h3>
                        <asp:Label ID="lblHeader" runat="server" Text="I miei modelli"></asp:Label>
                    </h3>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td width="20" >&nbsp;</td>
                            <td>
                                <SPAN class=subtitleblueblodtext>Modelli esistenti</SPAN>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:DataGrid ID="dgTemplates" runat="server" AutoGenerateColumns="False" CellPadding="4" GridLines="None" AllowSorting="true" OnSortCommand="SortTemplates"
                         BorderColor="blue" BorderStyle="solid" BorderWidth="1" ShowFooter="true" OnItemCreated="BindRowsDeleteMessage"
                        OnDeleteCommand="DeleteTemplate" OnEditCommand="EditTemplate" OnCancelCommand="CreateConference" Width="90%" Visible="true" style="border-collapse:separate"> <%--Edited for FF--%>
                        <SelectedItemStyle  CssClass="tableBody"/>
                         <AlternatingItemStyle CssClass="tableBody" />
                         <ItemStyle CssClass="tableBody"  />
                        <HeaderStyle CssClass="tableHeader" Height="30px" />
                        <EditItemStyle CssClass="tableBody" />
                         <%--Window Dressing--%>
                        <FooterStyle CssClass="tableBody" />
                        <Columns>
                            <asp:BoundColumn DataField="ID" Visible="false" ><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>
                            <asp:BoundColumn DataField="name" ItemStyle-CssClass="tableBody" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="tableHeader" HeaderText="Nome" SortExpression="1"></asp:BoundColumn> <%-- FB 2050 --%>
                            <asp:BoundColumn DataField="description" ItemStyle-CssClass="tableBody" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="tableHeader" HeaderText="Descrizione"></asp:BoundColumn> <%-- FB 2050 --%>
                            <asp:BoundColumn DataField="administrator" ItemStyle-CssClass="tableBody" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="tableHeader" HeaderText="Proprietario" SortExpression="2"></asp:BoundColumn> <%-- FB 2050 --%>
                            <asp:BoundColumn DataField="public" ItemStyle-CssClass="tableBody"  HeaderStyle-CssClass="tableHeader" HeaderText="Privato/pubblico" SortExpression="3"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="visualizza dettagli" ItemStyle-CssClass="tableBody"  HeaderStyle-CssClass="tableHeader">
                                <ItemTemplate>
                                    <asp:Button ID="btnViewDetails" Text="Visualizza" runat="server" CssClass="altShortBlueButtonFormat" />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Azioni" ItemStyle-CssClass="tableBody"  ItemStyle-Width="25%" FooterStyle-HorizontalAlign="Right">
                                <HeaderStyle CssClass="tableHeader" />
                                <ItemTemplate>
                                    <table width="100%">
                                        <tr>
                                            <td>
                                                <%--Code Changed for FB 1428--%>
                                                <asp:LinkButton runat="server" ID="btnCreateConf" CommandName="Cancel">
                                                    <span id="Field1">Crea conferenza</span>
                                                </asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton runat="server" Text="Modifica" ID="btnEdit" CommandName="Edit"></asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton runat="server" Text="Elimina" ID="btnDelete" CommandName="Delete"></asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <span class="blackblodtext"> <b>Modelli totali: </span><asp:Label ID="lblTotalRecords" runat="server" Text=""></asp:Label> </b>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:Table runat="server" ID="tblNoTemplates" Visible="false" Width="90%">
                        <asp:TableRow CssClass="lblError">
                            <asp:TableCell CssClass="lblError">
                                Nessun modello trovato.
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>                    
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table width="90%">
                        <tr>
                            <td align="center">
                                <asp:Button ID="btnManageOrder" runat="server" OnClientClick="javascript:ManageOrder();return false;" Text="Gestisci ordine dei modelli" CssClass="altLongBlueButtonFormat" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td width="20" >&nbsp;</td>
                            <td>
                                <SPAN class=subtitleblueblodtext>Ricerca nei modelli</SPAN>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                           <%--Removed Class for Window Dressing                      --%>
                            <td class="blackblodtext">
                                Immetti i criteri di ricerca per cercare tutti i modelli pubblici e privati.
                                <br />Sebbene sia possibile solo modificare i tuoi modelli privati, � possibile utilizzare un modello pubblico o privato come base per creare un nuovo modello.    
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table width="90%"> 
                        <tr>
                            <td align="center">
                                <table width="100%">
                                    <tr>
                                        <td align="right" class="blackblodtext">Nome modello</td>
                                        <td align="left">
                                            <asp:TextBox ID="txtSTemplateName" runat="server" CssClass="altText"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator4" ControlToValidate="txtSTemplateName" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ e &#34; sono caratteri non validi." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+^;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                        </td>
                                        <%if(!(Application["Client"].ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.MOJ))){%> <%--Added for FB 1425 MOJ--%>
                                        <td align="right" class="blackblodtext">partecipanti inclusi</td>
                                        <td align="left">
                                            <asp:TextBox ID="txtSParticipant" runat="server" CssClass="altText"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txtSParticipant" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > + % \ /  ? | ^ = ! ` [ ] { } # $ @ e ~ sono caratteri non validi." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+^?|!`\[\]{}\=@#$%&~]*$"></asp:RegularExpressionValidator> <%--FB 1888--%>
                                        </td> 
                                        <%}%> <%--Added for FB 1425 MOJ--%>                                      
                                    </tr>
                                    <tr>
                                        <td align="right" class="blackblodtext">Descrizione</td>
                                        <td align="left">
                                            <asp:TextBox ID="txtSDescription" TextMode="multiline" Rows="2" Width="200" runat="server" CssClass="altText"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" ControlToValidate="txtSDescription" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ e &#34; sono caratteri non validi." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+^;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                        </td>
                                        <td align="right">&nbsp;</td>
                                        <td align="left">&nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Button ID="btnSearch" OnClick="SearchTemplate" runat="server" CssClass="altLongBlueButtonFormat" Text="Invia" />
                            </td>                            
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td width="20" >&nbsp;</td>
                            <td>
                                <!--<SPAN class=subtitleblueblodtext></SPAN>--><%--Commented for FB 2094--%>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
               <td align="center">
                    <table width="90%">
                        <tr>
                            <td align="right">
                               <asp:Button ID="btnCreate" OnClick="CreateNewTemplate" runat="server" CssClass="altLongBlueButtonFormat" Text="Crea nuovo modello" /><%--FB 2094--%> 
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <asp:TextBox ID="Bridges" runat="server" Width="0" Height="0" ForeColor="transparent" BackColor="transparent" BorderColor="transparent"></asp:TextBox>
    <asp:TextBox ID="txtSortBy" runat="server" Visible="false"></asp:TextBox>
</form>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
    <!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
</body>
</html>


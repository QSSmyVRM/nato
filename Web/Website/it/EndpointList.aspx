<%@ Page Language="C#" Inherits="ns_EndpointList.EndpointList" ValidateRequest="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <!-- FB 2050 -->
<meta http-equiv="X-UA-Compatible" content="IE=7" /> <!-- FB 2050 -->

<!-- Window Dressing-->
<!-- #INCLUDE FILE="inc/maintopNET.aspx" --> 

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">

<script language="javascript">
function ViewBridgeDetails(bid)
{
    url = "BridgeDetailsViewOnly.aspx?hf=1&bid=" + bid;
    window.open(url, "BrdigeDetails", "width=900,height=800,resizable=yes,scrollbars=yes,status=no");
    return false;
}
function CheckSelection(obj)
{
//      alert(obj.type);
      if (obj.tagName == "INPUT" && obj.type == "radio")
            for (i=0; i<document.frmEndpoints.elements.length;i++)
            {
                var obj1 = document.frmEndpoints.elements[i];
                if (obj1.id != obj.id)
                    obj1.checked = false;
            }
}
    
</script>
    <title>Gestisci punti di fine</title>
</head>
<body>
    <form id="frmEndpoints" runat="server" method="post">
        <center><table border="0" width="98%" cellpadding="2" cellspacing="2">
            <tr>
                <td align="center">
                    <h3><asp:Label ID="lblHeader" runat="server" Text="Gestisci punti di fine"></asp:Label></h3><br />
                     <asp:Label ID="errLabel" runat="server" CssClass="lblError"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="Left">
                    <table>
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <asp:Label ID="lblSubHeader1" Text="Punti di fine disponibili" runat="server" CssClass="subtitleblueblodtext" ></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
             </tr>
              <%--Endpoint Search--%>
             <tr>
             <td>
              <iframe id="EndpointFrame" runat="server" width="100%" valign="top" height="625px" scrolling="no"></iframe>
             </td>
             </tr>
            <tr style="display:none">
                <td align="center">
                <asp:DataGrid ID="dgEndpointList" runat="server" AutoGenerateColumns="False" Font-Names="Verdana" Font-Size="Small"
                 Width="95%" OnItemDataBound="LoadProfiles" OnItemCreated="BindRowsDeleteMessage" CellPadding="4" GridLines="None"
                 BorderColor="blue" BorderStyle="solid" BorderWidth="1" AllowSorting="True" OnSortCommand="SortGrid"
                 OnEditCommand="EditEndpoint" OnCancelCommand="DeleteEndpoint" ShowFooter="true">
                <%--Window Dressing - Start--%>
                <SelectedItemStyle CssClass="tableBody" Font-Bold="True" />
                <EditItemStyle CssClass="tableBody" />
                <AlternatingItemStyle CssClass="tableBody" />
                <ItemStyle CssClass="tableBody" />
                <FooterStyle CssClass="tableBody" />
                <%--Window Dressing - End--%>
                    <Columns>
                        <asp:BoundColumn DataField="ID" Visible="false" HeaderText="ID" SortExpression="ID"></asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="Seleziona" HeaderStyle-CssClass="tableHeader" Visible="false">
                            <ItemTemplate>
                                <asp:RadioButton ID="rdSelectEndpoint" runat="server" onclick="javascript:CheckSelection(this)" />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn ItemStyle-HorizontalAlign="left" ItemStyle-CssClass="tableBody" HeaderStyle-HorizontalAlign="left" HeaderStyle-CssClass="tableHeader" HeaderText="Nome<br>punto di fine" SortExpression="EndpointName">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblEPName" Text='<%#DataBinder.Eval(Container, "DataItem.EndpointName") %>'></asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <br /><asp:RadioButton onclick="javascript:CheckSelection(this)" Visible="true" Checked="true" ID="rdNewEndpoint" runat="server" Text="Crea un nuovo punto di fine" />
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="DefaultProfileName" ItemStyle-CssClass="tableBody"  ItemStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left" SortExpression="DefaultProfileName" HeaderText="Profilo<br>predefinito">
                            <HeaderStyle CssClass="tableHeader" HorizontalAlign="left"/>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="TotalProfiles" ItemStyle-CssClass="tableBody"  ItemStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left" SortExpression="TotalProfiles" HeaderText="Profili<br>totali">
                            <HeaderStyle CssClass="tableHeader" HorizontalAlign="left"/>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="VideoEquipment" ItemStyle-CssClass="tableBody"  ItemStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left" SortExpression="VideoEquipment" HeaderText="Modello<br>di punto finale">
                            <HeaderStyle CssClass="tableHeader" HorizontalAlign="left"/>
                        </asp:BoundColumn>
                         <asp:BoundColumn DataField="DefaultProtocol" ItemStyle-CssClass="tableBody"  ItemStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left" SortExpression="DefaultProtocol" HeaderText="Protocollo<br>Video">
                            <HeaderStyle CssClass="tableHeader" HorizontalAlign="left"/>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="AddressType"  ItemStyle-CssClass="tableBody" ItemStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left" SortExpression="AddressType" HeaderText="Tipo di<br>indirizzo">
                            <HeaderStyle CssClass="tableHeader" HorizontalAlign="left"/>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="Address"  ItemStyle-CssClass="tableBody" SortExpression="Address" HeaderText="Indirizzo" ItemStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left">
                            <HeaderStyle CssClass="tableHeader" HorizontalAlign="left"/>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="IsOutside"  ItemStyle-CssClass="tableBody" SortExpression="IsOutside" HeaderText="Al di fuori<br>della rete" ItemStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left">
                            <HeaderStyle CssClass="tableHeader" HorizontalAlign="left"/>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="ConnectionType"  ItemStyle-CssClass="tableBody"  SortExpression="ConnectionType" HeaderText="Opzione di<br>composizione" ItemStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left">
                            <HeaderStyle CssClass="tableHeader" HorizontalAlign="left"/>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="LineRate"  ItemStyle-CssClass="tableBody" SortExpression="LineRate" HeaderText="Larghezza di banda" ItemStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left">
                            <HeaderStyle CssClass="tableHeader" HorizontalAlign="left"/>
                        </asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="MCU<br>assegnato"  ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" > 
                        <ItemTemplate>
                            <asp:Label ID="lblBridgeID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Bridge")%>' Visible="false"></asp:Label>
                            <asp:Label ID="lblBridgeName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BridgeName")%>'></asp:Label>
                            <br /><asp:LinkButton Text="visualizza dettagli" runat="server" ID="btnViewBridgeDetails" Visible='<%# !DataBinder.Eval(Container, "DataItem.BridgeName").ToString().Equals("") %>'></asp:LinkButton>
                        </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Profili" Visible="false"  ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:DropDownList CssClass="altLong4SelectFormat" runat="server" ID="lstProfiles" DataTextField="ProfileName" DataValueField="ProfileID" Visible='<%#Request.QueryString["t"].ToUpper().Equals("TC") %>'></asp:DropDownList><%-- SelectedValue='<%#DataBinder.Eval(Container, "DataItem.DefaultProfileID") %>' --%>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Button ID="btnSubmitNew" runat="server" CssClass="altLongBlueButtonFormat" Visible="true" Text="Invia" OnClick="CreateNewEndpoint" />
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn Visible="false" ItemStyle-CssClass="tableBody" >
                            <ItemTemplate>
                                <asp:TextBox ID="txtProfilesXML" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ProfilesXML") %>'></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="ProfilesXML" Visible="false" ItemStyle-CssClass="tableBody" ></asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="Azioni" ItemStyle-CssClass="tableBody" >
                            <HeaderStyle CssClass="tableHeader" />
                            <ItemTemplate>
                                <asp:LinkButton Text="Modifica" runat="server" ID="btnEdit" CommandName="Edit"></asp:LinkButton>&nbsp;&nbsp;
                                <asp:LinkButton Text="Elimina" runat="server" ID="btnDelete" CommandName="Cancel" Visible="true"></asp:LinkButton>&nbsp;&nbsp;
                            </ItemTemplate>
                            <FooterTemplate>
                                <b><span class="blackblodtext"> Punti di fine totali: </span> <asp:Label ID="lblTotalRecords" runat="server" Text=""></asp:Label> </b>
                                <br>
				     <%--Added for License Modification START--%>                                
				    <b><span class="blackblodtext">licenze&nbsp;rimanenti:&nbsp;</span><asp:Label ID="lblRemaining" runat="server" Text=""></asp:Label> </b>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
                    <asp:Label ID="lblNoEndpoints" runat="server" Text="Nessun punto finale trovato." Visible="False" CssClass="lblError"></asp:Label>
                </td>
            </tr>
            <tr style="display:none">
                <td>
                    <asp:Table ID="tblPage" Visible="false" runat="server">
                        <asp:TableRow ID="TableRow1" runat="server">
                            <asp:TableCell ID="TableCell1" Font-Bold="True" Font-Names="Verdana" Font-Size="Small" ForeColor="Blue" runat="server">pagine: </asp:TableCell>
                            <asp:TableCell ID="TableCell2" runat="server"></asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>

                </td>
            </tr>
            <tr style="display:none">
                <td align="Left">
                    <table>
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <SPAN class=subtitleblueblodtext>Cerca punti di fine</SPAN>
                            </td>
                        </tr>
                    </table>
                </td>
             </tr>
             <tr style="display:none">
                <td align="Left">
                    <table width="95%">
                        <tr>
                            <%--Window Dressing - Start --%>
                            <td align=right class="blackblodtext"><b>Nome di punto finale</b></td>
                            <td>
                                <asp:TextBox ID="txtEndpointName" runat="server" CssClass="altText" Text="" ValidationGroup="Search" ></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator4" ControlToValidate="txtEndpointName" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ e &#34; sono caratteri non validi." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=:@^#$%&()'~]*$"></asp:RegularExpressionValidator>
                            </td>
                            <td align=right class="blackblodtext"><b>tipo di punto finale</b></td>
                            <td>
                                <asp:DropDownList ID="lstAddressType" runat="server" CssClass="altText" DataTextField="Name" DataValueField="ID"></asp:DropDownList>
                                <asp:DropDownList ID="lstBridges" Visible="false" runat="server" CssClass="altText" DataTextField="BridgeName" DataValueField="BridgeID"></asp:DropDownList>
                                <asp:DropDownList ID="lstLineRate" Visible="false" runat="server" CssClass="altText" DataTextField="LineRateName" DataValueField="LineRateID"></asp:DropDownList>
                                <asp:DropDownList ID="lstVideoEquipment" Visible="false" runat="server" CssClass="altText" DataTextField="VideoEquipmentName" DataValueField="VideoEquipmentID"></asp:DropDownList>
                                <asp:DropDownList ID="lstVideoProtocol" Visible="false" runat="server" CssClass="altText" DataTextField="Name" DataValueField="ID"></asp:DropDownList>
                                <asp:DropDownList ID="lstConnectionType" runat="server" DataTextField="Name" DataValueField="ID" Visible="false"></asp:DropDownList>                                 <asp:DropDownList ID="DropDownList1" runat="server" DataTextField="Name" DataValueField="ID" Visible="false"></asp:DropDownList> <%--Fogbugz case 427--%>
                            </td>
                            <%--Window Dressing - End --%>
                        </tr>
                        <tr>
                            <td colspan="4" align="right">
                                <asp:Button runat="server" CssClass="altLongBlueButtonFormat" Text="Invia" ValidationGroup="Search" OnClick="SearchEndpoint" />
                            </td>
                        </tr>
                    </table>
                </td>
             </tr>
            <tr>
                <td align="Left">
                    <table>
                        <tr id="trNew" runat="server">
                            <td>&nbsp;</td>
                            <td>
                                <%--<SPAN class=subtitleblueblodtext>Create New Endpoint <asp:Label ID="lblAddSelection" Text=" or Choose Selected" Visible="false" runat="server" ></asp:Label></SPAN>--%><%--Commented for FB 2094--%> 
                                <SPAN class=subtitleblueblodtext><asp:Label ID="Label1" Text=" o Scegli selezionati" Visible="false" runat="server" ></asp:Label></SPAN><%--FB 2094--%> 
                            </td>
                        </tr>
                    </table>
                </td>
             </tr>
            <tr>
                <td align="Left">
                    <table width="95%">
                        <tr>
                            <td align="right">
                              <asp:Button ID="btnSubmit" runat="server" CssClass="altLongBlueButtonFormat" Text="Crea Nuovo Punto di Fine" OnClick="CreateNewEndpoint" /><%--FB 2094--%> 
                            </td>
                        </tr>

                    </table>
                </td>
             </tr>
        </table>
            <asp:TextBox Visible="false" ID="txtEndpointID" runat="server"></asp:TextBox>
        </center>
    </form>
</body>
</html>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
<%@ Page Language="C#" Inherits="ns_SuperAdministrator.SuperAdministrator" %>
<%@ Register Assembly="MetaBuilders.WebControls.ComboBox" Namespace="MetaBuilders.WebControls"
    TagPrefix="mbcbb" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register TagPrefix="cc1" Namespace="myVRMWebControls" Assembly="myVRMWebControls" %>

<%--Edited for FF--%>
<%if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("MSIE"))
{%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%}
else {%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN"> 
<%} %>
<meta http-equiv="X-UA-Compatible" content="IE=6" />

<!--window Dressing-->
<!-- #INCLUDE FILE="inc/maintopNET.aspx" --> 
<script runat="server"></script>
<script type="text/javascript" language="javascript" src='script/lib.js'></script>
<script type="text/javascript" src="script/calview.js"></script>
<script type="text/javascript" src="inc/functions.js"></script>
<script language="JavaScript" src="inc\functions.js"></script>

<script language="JavaScript">
<!--



    

//FB Case 807 starts here
function SavePassword()// FB 1896
{

    if(document.getElementById("hdnMailServer").value != "")
    {

      document.getElementById("txtMSPassword1").value = document.getElementById("hdnMailServer").value;
      document.getElementById("txtMSPassword2").value = document.getElementById("hdnMailServer").value;
    }
     if(document.getElementById("hdnLDAPPassword").value != "")
    {
      document.getElementById("txtLDAPAccountPassword1").value = document.getElementById("hdnLDAPPassword").value;
      document.getElementById("txtLDAPAccountPassword2").value = document.getElementById("hdnLDAPPassword").value;
  }
  // FB 2501 EM7 Starts
  if (document.getElementById("hdnEM7Password").value != "") {
      document.getElementById("txtEM7Password").value = document.getElementById("hdnEM7Password").value;
      document.getElementById("txtEM7ConformPassword").value = document.getElementById("hdnEM7Password").value;
  }
  // FB 2501 EM7 Ends
}

function PreservePassword()// FB 1896
{
      document.getElementById("hdnMailServer").value = document.getElementById("txtMSPassword1").value
      document.getElementById("hdnLDAPPassword").value = document.getElementById("txtLDAPAccountPassword2").value;
      document.getElementById("hdnEM7Password").value = document.getElementById("txtEM7Password").value; //FB 2501 EM7
}
//FB Case 807 ends here
function TestLDAPServerConnection()
{
	if (document.getElementById("txtLDAPAccountPassword1").value == "")
	{
		alert("Si prega di inserire un valore alla connessione Test LDAP Server.");
		document.getElementById("txtLDAPAccountPassword1").focus();
		return false;
	}
	
	if (document.getElementById("txtLDAPAccountLogin").value == "")
	{
		alert("Si prega di inserire un valore alla connessione Test LDAP Server.");
		document.getElementById("txtLDAPAccountLogin").focus();
		return false;
	}
	 
	if (document.getElementById("txtLDAPServerPort").value == "")
	{
		alert("Si prega di inserire un valore alla connessione Test LDAP Server.");
		document.getElementById("txtLDAPServerPort").focus();
		return false;
	}
	if (document.getElementById("txtLDAPServerAddress").value == "")
	{
		alert("Immetti un indirizzo valido.");
		document.getElementById("txtLDAPServerAddress").focus();
		return false;
	}
	if (document.getElementById("lstLDAPConnectionTimeout").value == "")
	{
		alert("Si prega di inserire un valido valore di timeout di connessione.");
		document.getElementById("lstLDAPConnectionTimeout").focus();
		return false;
	}
//	alert(document.frmMainsuperadministrator.LDAPConnectionTimeout.value);
	url = "dispatcher/admindispatcher.asp?cmd=TestLDAPConnection" + "&sadd=" + escape(document.getElementById("txtLDAPServerAddress").value) + "&port=" + document.getElementById("txtLDAPServerPort").value + "&lg=" + escape(document.getElementById("txtLDAPAccountLogin").value) + "&pd=" + escape(document.getElementById("txtLDAPAccountPassword1").value) + "&cto=" + document.getElementById("lstLDAPConnectionTimeout").value;

	if (!window.winrtc) {	// has not yet been defined
		winrtc = window.open(url, "", "width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
		winrtc.focus();
	} else // has been defined
	    if (!winrtc.closed) {     // still open
	    	winrtc.close();
	        winrtc = window.open(url, "", "width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
			winrtc.focus();
		} else {
	        winrtc = window.open(url, "", "width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
	        winrtc.focus();
	}
}


function TestMailServerConnection()
{
	if (document.getElementById("txtMailServerLogin").value == "")
	{
		alert("Si prega di inserire un nome di accesso per testare la connessione del server di posta.");
		document.getElementById("txtMailServerLogin").focus();
		return false;
	}
	
	if (document.getElementById("txtMSPassword1").value == "")
	{
		alert("Si prega di inserire una password per testare la connessione del server di posta.");
		document.getElementById("txtMSPassword1").focus();
		return false;
	}
	
	if (document.getElementById("txtServerAddress").value == "")
	{
		alert("Inserisci un indirizzo server per verificare la connessione al Server di posta.");
		document.getElementById("txtServerAddress").focus();
		return false;
	}
	url = "dispatcher/admindispatcher.asp?cmd=TestMailConnection" + "&sa=" + escape(document.getElementById("txtServerAddress").value) + "&lg=" + escape(document.getElementById("txtMailServerLogin").value) + "&sp=" + document.getElementById("txtServerPort").value + "&se=" + escape(document.getElementById("txtSystemEmail").value) + "&dn=" + escape(document.getElementById("txtDisplayName").value) + "&pd=" + escape(document.getElementById("txtMSPassword1").value);
//alert(url);
	if (!window.winrtc) {	// has not yet been defined
		winrtc = window.open(url, "", "width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
		winrtc.focus();
	} else // has been defined
	    if (!winrtc.closed) {     // still open
	    	winrtc.close();
	        winrtc = window.open(url, "", "width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
			winrtc.focus();
		} else {
	        winrtc = window.open(url, "", "width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
	        winrtc.focus();
	}
}


function getYourOwnEmailList (i)
{
	url = "dispatcher/conferencedispatcher.asp?frm=approverNET&frmname=frmMainsuperadministrator&no=" + i + "&cmd=GetEmailList&emailListPage=1&wintype=pop";
	if (!window.winrtc) {	// has not yet been defined
		winrtc = window.open(url, "", "width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
		winrtc.focus();
	} else // has been defined
	    if (!winrtc.closed) {     // still open
	    	winrtc.close();
	        winrtc = window.open(url, "", "width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
			winrtc.focus();
		} else {
	        winrtc = window.open(url, "", "width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
	        winrtc.focus();
		}
}

function frmMainsuperadministrator_Validator ()
{
    if (Trim(document.getElementById("txtSystemEmail").value) == "") {
        alert(EN_81);
		document.getElementById("txtSystemEmail").focus();
		return (false);		
	}

    if (!checkemail(document.getElementById("txtSystemEmail").value)) {
		alert(EN_81);
		document.getElementById("txtSystemEmail").focus();
		return (false);	
	}

    if (Trim(document.getElementById("txtwebsiteURL").value) == "") {
        alert("Si prega di inserire un valore per l'URL del sito web.");
		document.getElementById("txtwebsiteURL").focus();
		return (false);		
	}
	
	if (Trim(document.getElementById("txtServerAddress").value) == "") {
		alert(EN_82);
		document.getElementById("txtServerAddress").focus();
		return (false);
	}

	if (Trim(document.getElementById("txtServerPort").value) == "") {
		alert(EN_83);
		document.getElementById("txtServerPort").focus();
		return (false);
	}
	
	if (isPositiveInt(document.getElementById("txtServerPort").value, "Port No") != 1) {
		document.getElementById("txtServerPort").focus();
		return (false);
	}

	if (Trim(document.getElementById("txtLDAPServerPort").value) != "") {
		if (isPositiveInt(document.getElementById("txtLDAPServerPort").value, "LDAP Port No") != 1) {
			document.getElementById("txtLDAPServerPort").focus();
			return (false);
		}
		
		pn = parseInt(document.getElementById("txtLDAPServerPort").value, 10);
		if (pn != 389) {
			var isChangeLDAPPortNo = confirm("Sei sicuro che non voler utilizzare il n. di porta LDAP 389?")
			if (isChangeLDAPPortNo == false) {
				document.getElementById("txtLDAPServerPort").focus()
				return (false);		
			}
		}
	}
	return (true);	
}
//Code addded for organisation
function OpenOrg()
{
    window.location.replace("ManageOrganization.aspx");
}

//Code added for displaying User login FB 1969
function Openuser()
{
    window.location.replace("UserHistoryReport.aspx");
}
/*Commented for FB 1849
function fnChangeOrganization()
{
    var btnchng = document.getElementById("BtnChangeOrganization");
    var drporg = document.getElementById("DrpOrganization");
    var cnfrm = confirm("Hereafter all the transactions performed in system will be for the selected organization.Do you wish to continue");
            
    if(cnfrm)
        return true;
    else
        return false;
   
}*/
 
//-->
//FB 2594 Starts
function SetButtonStatus(sender, target) {

    if (sender.value.length > 0)

        document.getElementById(target).disabled = false;

    else

        document.getElementById(target).disabled = true;

}
//    function PollWhygo() {
//        if ((document.getElementById("txtWhyGoURL").value == "") || (document.getElementById("txtWhygoUsr").value == "") || document.getElementById("trWhygoPassword").value == "") ){
//            alert("Please enter the Whygo Integration Settings.");
//            document.getElementById("txtWhyGoURL").focus();
//            return false;
//        } 
//    }
//FB 2594 Ends
</script>

<script type="text/javascript" src="script\approverdetails.js">

</script>
<html>
<%--FB 1969 Start--%>
<head>
    <style type="text/css">
        #btnuser
        {
            width: 100px;
        }
    </style>
    <title>Super administartor</title>
    <script type="text/javascript" src="script/cal-flat.js"></script>
    <script type="text/javascript" src="lang/calendar-en.js"></script>
    <script type="text/javascript" src="script/calendar-setup.js"></script>
    <script type="text/javascript" src="script/calendar-flat-setup.js"></script>
    <link rel="stylesheet" type="text/css" media="all" href="css/calendar-blue.css" />
</head>
<%--FB 1969 End--%>
    <body>
    
    
<form name="frmMainsuperadministrator" id="frmMainsuperadministrator" method="Post" action="SuperAdministrator.aspx" language="JavaScript" runat="server"> <!-- -->
  
  
      <asp:ScriptManager ID="CalendarScriptManager" runat="server" AsyncPostBackTimeout="600">
                 </asp:ScriptManager>
<center>    
  <input type="hidden" id="helpPage" value="92">
  <input type="hidden" id="hdnMailServer" runat="server" /> 
  <input type="hidden" id="hdnLDAPPassword" runat="server" /> 
  <input type="hidden" id="hdnExternalPassword" runat="server" />  
  <input type="hidden" id="hdnESMailUsrSent" runat="server" /> <%--FB 2363--%>
  <input type="hidden" id="hdnEM7Password" runat="server" /><%-- FB 2501 EM7--%>
<h3>Impostazioni sito</h3>
        <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError"></asp:Label><br />
            </center>
        <table width="100%" border="0">
        <tr valign="top" id="trSwt" runat="server">
            <td colspan="5" align="right" valign="top" style="display:none">
                <a id="ChgOrg" runat="server" href="#" class="blueblodtext">Cambia Organizzazione </a>
            </td>
        </tr>
            <tr>
                <td align="left" class="subtitleblueblodtext" colspan="5" style="height:21;font-weight:bold">
                    licenza</td>
            </tr>
            <tr>
                <td align="right" height="21" style="font-weight:bold" width="2%">
                </td>
                <%--Window Dressing--%>
                <td align="right" class="blackblodtext" height="21" style="font-weight:bold" width="18%" valign="top">
                    Chiave di licenza myVRM</td>
                <td style="height: 21px;" valign="top" width="30%">
                    <asp:TextBox ID="txtLicenseKey" runat="server" Columns="8" Rows="4" CssClass="altText" TextMode="MultiLine"></asp:TextBox></td>
                <%--Window Dressing--%>
                <td align="right" valign="top" style="font-weight:bold" class="blackblodtext">
                    Dettagli di licenza</td>
                <%--Window Dressing--%>
               <td class="altblackblodttext" width="45%" colspan="2" align="left">
                <table width="100%" border="0">
                <tr>
                    <td align="left" nowrap valign="top">
                    <span class="blackblodtext"></span>
                    </td>
                    <td align="left">
                        <asp:Label ID="lblLicenseDetails" style="vertical-align:baseline;" runat="server"  Width="95%"></asp:Label>
                        </td>
                        </tr>
                    </table>
                    <table width="100%" cellspacing="3" cellpadding="3" border="0">
                         <tr>
                            <td width="30%" valign="top" nowrap>
                                <span  class="blackblodtext">stato licenza :</span><asp:Label ID="ActStatus" runat="server"></asp:Label>
                                <asp:Label ID="EncrypTxt" runat="server" style="display:none;"></asp:Label>
                            </td>
                            <td width="70%" id="ExportTD" runat="server" style="display:none;" align="left">
                            <asp:Button ID="TxyButton" runat="server" CssClass="altShortBlueButtonFormat"
                                            OnClick="btnExportTxt_Click" Text="Esporta in testo" />
                            </td>
                            </tr>
                           
                    </table>
                    <table><%-- added for FF--%>
                    <tr id="ActivationTR" runat="server" style="display:none;">
                                <td colspan="2" Width="100%">
                            <p><font face="Verdana, Arial, Helvetica, sans-serif" size=2>Il sistema sar� bloccato su: </font><asp:Label ID="DeactLbl" runat="server"  CssClass="lblError"></asp:Label>.Si prega di cliccare sopra il pulsante per ottenere il codice di attivazione e inviare per mail a <span class=contacttext><a  href='mailto:Support@myvrm.com'>Support@myvrm.com</a></span> 
                             per ottenere la licenza crittografata attivata.</p> 
                                </td>
                            </tr>
                    </table>
                    
                </td>
            </tr>
            <tr>
                <td align="left" class="subtitleblueblodtext" colspan="5" style="height:21;font-weight:bold">
                    Opzioni Organizzazione</td>
            </tr>
            <tr>
                <td align="right" height="21" style="font-weight:bold" width="2%"></td>
                <td align="right"  height="21" style="font-weight:bold" class="blackblodtext"  width="18%">Organizzazione</td>
                <td align="left" valign="bottom" colspan="3">
                     <input type="button" id="btnOrg" runat="server" name="btnOrganization" value="Gestisci Organizzazioni"
                      class="altLongBlueButtonFormat" onclick="javascript:OpenOrg();"/>
                       <%--FB 1662--%>
                </td>
                <td align="right" style="height: 21px; width: 15%;">
                </td>
                <td style="height: 21px;" width="35%">
                </td>
            </tr>
            <tr>
                <td align="left" class="subtitleblueblodtext" colspan="5">
                    Impostazioni dell'interfaccia utente</td>
            </tr>
            <tr>
                <td align="right" height="21" style="font-weight:bold" width="2%" colspan="5">
                </td>
                
            </tr>
            <%-- Code Added for FB 1428--%>
            <tr>
             <td align="right"
                    height="21" style="font-weight:bold" width="2%">
             </td>
             <td align="right" valign="top" style="font-weight:bold" class="blackblodtext">
                    Messaggio societ�
                </td>
                <td valign="top">
                <asp:TextBox ID="txtCompanymessage" runat="server"  Columns="30" CssClass="altText" Width="212px" Rows="2"></asp:TextBox> <%--FB 2512--%>
                <asp:Label ID="hdnCompanymessage" Text="" Visible="false" runat="server"></asp:Label>
                <asp:Label ID="lblvalidation" Text=""  runat="server"></asp:Label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator9" ValidationGroup="submit" runat="server" ControlToValidate="txtCompanymessage" ErrorMessage="Richiesto." Display="dynamic"></asp:RequiredFieldValidator>
                </td>
				<%-- //Commented for FB 1633 start  --%>
                <td align="right" valign="top"  height="21" style="font-weight:bold" class="blackblodtext" nowrap>
                        banner di risoluzione standard
                    </td>
				<%-- //Commented for FB 1633 end  --%>
                  <td valign="top"  style="height: 21px;" nowrap>
                        <input type="file" id="fleMap2" contenteditable="false" enableviewstate="true" size="20" class="altText" runat="server" />
                       
                        <asp:Label ID="hdnUploadMap2" Text="" Visible="false" runat="server"></asp:Label>
                        <input type="hidden" id="Map2ImageDt" name="Map1ImageDt" runat="server" />
                        <asp:Label ID="hdnImageId1" Text="" Visible="false" runat="server"></asp:Label>
                        <span class="blackItalictext"> (1024 x 72 pixels)</span>
                  	    <asp:ImageButton ImageUrl="image/btn_delete.gif" runat="server" ID="btnRemoveMap2" ToolTip="Elimina" OnCommand="RemoveStdBanner" />													   
                    </td>
            </tr>
            <tr>
				<%-- //Commented for FB 1633 start  --%>
                   <td align="right"
                         style="font-weight:bold" width="2%">
                    </td>
                    <%--FB 2512 Starts--%>
                    <%--<td></td>
                    <td></td>--%>
                    
                    <td align="right" valign="top" style="font-weight:bold" class="blackblodtext" style="height:9px">
                    Cronologia di accesso
                </td>
                <td align="left" valign="bottom" >
                     <input type="button" id="btnuser" runat="server" name="btnview" value="Visualizza" style="margin-bottom:5px" class="altLongBlueButtonFormat" onclick="javascript:Openuser();" />
                </td>
               <%-- FB 2512 Ends--%>
                     <td align="right" valign="top" style="font-weight:bold" class="blackblodtext" style="height:9px">
                    logo del sito
                </td>
                <%-- //Commented for FB 1633 end  --%>
                <td valign="top" style="height: 9px;">
                    <input type="file" id="fleMap1" contenteditable="false" enableviewstate="true" size="20" class="altText" runat="server" />
                    <span class="blackItalictext"> (244 x 88 pixels)</span><%--(122 x 44)FB 2407--%>
                    <asp:ImageButton ImageUrl="image/btn_delete.gif" runat="server" ID="btnRemoveMap1" ToolTip="elimina" OnCommand="RemoveFile" />
                    
                    <asp:Label ID="hdnUploadMap1" Text="" Visible="false" runat="server"></asp:Label>
                    <input type="hidden" id="Map1ImageDt" name="Map1ImageDt" runat="server" />
                    <asp:Label ID="hdnImageId" Text="" Visible="false" runat="server"></asp:Label>
                    <br />
					<asp:RegularExpressionValidator ID="regfleMap1" runat="server" Display="Dynamic" ValidationGroup="submit" ControlToValidate="fleMap1" CssClass="lblError" ErrorMessage="Tipo di file non � valido." ValidationExpression="^.*\.((j|J)(p|P)(e|E)?(g|G)|(g|G)(i|I)(f|F)|(p|P)(n|N)(g|G))$"></asp:RegularExpressionValidator>    
                        
                    
                </td>
			<%-- //Commented for FB 1633 start  --%>
            </tr>
          
            <tr>
                <td align="right"
                    height="21" style="font-weight:bold;display:none" width="2%"><%-- FB 2512--%>
                </td>
                 <td valign="top" align="right" style="font-weight:bold;display:none"  class="blackblodtext" rowspan="2" width="15%">
                        Testo della pagina di accesso</td>
                <td style="font-weight:bold;display:none" width="30%">
                    <asp:TextBox ID="CompanyInfo" runat="server" rowspan="2" Columns="20" CssClass="altText" Rows="4" TextMode="MultiLine"></asp:TextBox>
                </td>
                
                <%-- //Commented for FB 1633  
                <td align="right" valign="top" style="font-weight:bold" class="blackblodtext">
                    High Resolution Banner 
                </td>--%>
            <td valign="top" nowrap>
                <input type="file" id="fleMap3" contenteditable="false" enableviewstate="true" size="20" class="altText" runat="server"  visible="false"/>
                   <span class="blackItalictext"> <%--(1600 x 72 pixels)--%></span>
                  	<asp:ImageButton ImageUrl="image/btn_delete.gif" runat="server" ID="btnRemoveMap3" ToolTip="elimina" OnCommand="RemoveHighBanner" Visible="false" />
                    
                    <asp:Label ID="hdnUploadMap3" Text="" Visible="false" runat="server"></asp:Label>
                    <input type="hidden" id="Map3ImageDt" name="Map1ImageDt" runat="server" />
                    <asp:Label ID="hdnImageId2" Text="" Visible="false" runat="server"></asp:Label>
                    
                   
                </td>
               <%-- //Commented for FB 1633 end --%>
            </tr>
             <%--FB 1969 - Start--%> <%--FB 2512 Starts--%>
             <%--<tr>
               <td align="right"
                         style="font-weight:bold" >
                    </td>
                    <td></td>
                    <td align="right" valign="top" style="font-weight:bold" class="blackblodtext" style="height:9px">
                    Cronologia di accesso
                </td>
                <td align="left" valign="bottom" >
                     <input type="button" id="btnuser" runat="server" name="btnview" value="Visualizza" class="altLongBlueButtonFormat" onclick="javascript:Openuser();" />
                </td>
            </tr>--%> 
            <%--FB 2512 Ends--%>
             <%--FB 1969 - End--%>
            <tr>
                <td class="subtitleblueblodtext" colspan="5">
                    Configurazioni di server di posta</td>
            </tr>
            <tr>
                <td align="right"
                    height="21" style="font-weight:bold" width="2%">
                </td>
               <%--Window Dressing--%>
                <td align="right" height="21" style="font-weight:bold" class="blackblodtext" width="18%">
                    e-mail di sistema</td>
                <td style="height: 21px;" width="30%">
                    <asp:TextBox ID="txtSystemEmail" runat="server" CssClass="altText" Text="support@myvrm.com">
                    </asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="TestEmail" runat="server" ControlToValidate="txtSystemEmail" ErrorMessage="Richiesto." Display="dynamic"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator is="regSystemEmail" runat="server" ControlToValidate="txtSystemEmail" ErrorMessage="Indirizzo e-mail non valido" Display="dynamic" ValidationExpression="^[a-zA-Z()^][()^\w\.-]*[a-zA-Z0-9()^]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"></asp:RegularExpressionValidator> <%--FB Case 699--%>
                </td>
                <%--Window Dressing--%>
                <td align="right" style="font-weight:bold" class="blackblodtext" >
                    accesso al server di posta</td>
                <td style="height: 21px;" width="35%">
                    <asp:TextBox ID="txtMailServerLogin" runat="server" CssClass="altText"></asp:TextBox>
                    
                    </td>
            </tr>
            <tr>
                <td align="right">
                </td>
                <%--Window Dressing--%>
                <td align="right" style="font-weight:bold" width="18%" class="blackblodtext" >
                    password del server di posta</td>
                <td style="height: 37px;" width="30%">
                    <asp:TextBox ID="txtMSPassword1" onchange="javascript:PreservePassword()" runat="server" CssClass="altText" TextMode="Password"></asp:TextBox><asp:CompareValidator ID="CompareValidator2" runat=server ControlToValidate=txtMSPassword1 ControlToCompare=txtMSPassword2 ErrorMessage="Le password non corrispondono." Font-Names="Verdana" Font-Size="X-Small" Font-Bold="False" Display="Dynamic" />
                    
                    </td>
                <%--Window Dressing--%>
                <td align="right" style="font-weight:bold" class="blackblodtext" >
                    Riscrivi la password</td>
                <td style="height: 37px;" width="35%">
                    <asp:TextBox ID="txtMSPassword2" runat="server" onchange="javascript:PreservePassword()" CssClass="altText" TextMode="Password"></asp:TextBox>
                    
            </td>
            </tr>
            <tr>
                <td align="right"
                    height="21" style="font-weight:bold" >  <%--FB 1710--%>
                <%--Window Dressing--%>
                <td align="right"  height="21" style="font-weight:bold" class="blackblodtext" width="18%">
                    nome visualizzato</td>
                <td style="height: 21px;" width="30%">
                    <asp:TextBox ID="txtDisplayName" runat="server" CssClass="altText"></asp:TextBox>
                    
                </td>
                <%--Window Dressing--%><%--FB 1710 Starts--%>
                <td align="right" style="font-weight:bold" class="blackblodtext">URL del sito web</td>
                <td style="height: 21px;" width="35%">
                    <asp:TextBox ID="txtWebsiteURL" runat="server" CssClass="altText">
                    </asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat=server ControlToValidate="txtWebsiteURL" ErrorMessage="URL del sito � necessario." Font-Names="Verdana" Font-Size="X-Small" Font-Bold="False"><font color="red" size="1pt"> required</font></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator6" ControlToValidate="txtWebsiteURL" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ ( ) ; ? | ^ = ! ` , [ ] { } # $ @ ~ e &#34; sono caratteri non validi." ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>+;?|!`,\[\]{}\x22;=^@#$%&()']*$"></asp:RegularExpressionValidator>
                </td>
				<%--FB 1710 ends--%>
            </tr>
            <tr style="height:35px"> <%--FB 1710--%>
                <td align="right"
                    height="21" style="font-weight:bold"> <%--FB 1710--%>
                </td>
                 <%--Window Dressing--%>
                <td align="right" height="21" style="font-weight:bold" width="18%" class="blackblodtext">
                    indirizzo del server di posta</td>
                <td style="height: 21px;" width="30%">
                    <asp:TextBox ID="txtServerAddress" runat="server" CssClass="altText"></asp:TextBox>&nbsp;
                    <asp:RequiredFieldValidator ID="reqEmailServerAddress" ValidationGroup="TestEmail" Display="Dynamic" ErrorMessage="Richiesto" runat="server" CssClass="lblError" ControlToValidate="txtServerAddress"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txtServerAddress" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ e &#34; sono caratteri non validi." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                    <asp:RegularExpressionValidator ID="regPortP" Enabled="false" SetFocusOnError="true" ControlToValidate="txtServerAddress" ValidationExpression="^([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}$" ErrorMessage="Indirizzo IP non valido" Display="dynamic" runat="server"></asp:RegularExpressionValidator> <%--FB Case 491: Saima --%>
                </td>
                 <%--Window Dressing--%>
                <td align="right" height="21" style="font-weight:bold" class="blackblodtext">
                    porta del server di posta</td>
                <td style="height: 21px;" width="35%">
                    <asp:TextBox ID="txtServerPort" runat="server" CssClass="altText" Text="25"></asp:TextBox>
                    <span class="blackblodtext">25 � predefinito</span> <%--FB 2552--%>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="TestEmail" runat=server ControlToValidate="txtServerPort" ErrorMessage="Richiesto."></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator9" ControlToValidate="txtServerPort" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>Solo valori numerici." ValidationExpression="[\d]+"></asp:RegularExpressionValidator>  
                </td>
            </tr>
            <%--FB 2552 Start--%>
            <tr>
                <td align="right" height="21" style="font-weight: bold" width="2%">
                </td>
                <td align="right" height="21" style="font-weight: bold" width="18%" class="blackblodtext">
                    Tentativi di posta
                </td>
                <td align="left">
                    <asp:TextBox ID="txtRetryCount" CssClass="altText" runat="server"></asp:TextBox>
                    <span class="blackblodtext">(Max 30)</span>
                    <asp:RegularExpressionValidator ID="RetryCountValidator" ControlToValidate="txtRetryCount"
                        Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>Solo valori numerici." ValidationGroup="submit"
                        ValidationExpression="[\d]+"></asp:RegularExpressionValidator>
                   <asp:RangeValidator ID="retryCountRangeValid" SetFocusOnError="true" Type="Integer" MinimumValue="0" MaximumValue="30"
                        Display="Dynamic" ControlToValidate="txtRetryCount" ValidationGroup="submit" runat="server" ErrorMessage="<br/>Tentativi di massima � 30."></asp:RangeValidator>
                        
                </td>
            </tr>
            <%--FB 2552 End--%>
            <tr>
                <td align="right"
                    height="21" style="font-weight:bold">
                </td>
                <%--Commented for FB 1710 start--%> 
                     <%--Window Dressing //Add Message Text--%>
                     <td align="right" valign="top"  style="font-weight:bold" class="blackblodtext" visible="false" width="18%">
                                </td>
                            <td valign="top" width="30%">
                                <asp:TextBox ID="txtMailMessage" runat="server" Columns="20" CssClass="altText" Rows="4"
                                    TextMode="MultiLine" Width="200px" Wrap="False" Visible="false"></asp:TextBox>
                                <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator10" ControlToValidate="txtMailMessage" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ and &#34; are invalid characters." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()'~]*$"></asp:RegularExpressionValidator>     Mail Footer      --%>
                            </td>
                <%--Commented for FB 1710 End--%>
                <td align="right" height="21"></td>
                <td style="height: 21px;" width="35%" class="blackblodtext"></td>
            </tr>
            <tr>
                <td align="right" colspan="1" height="21" style="font-weight:bold" width="2%">
                </td>
                <td align="center" colspan="4" width="18%">
                    <asp:Button ID="btnTestMailConnection" Text="Verifica connessione" ValidationGroup="TestEmail" CssClass="altLongBlueButtonFormat" runat="server"  OnClick="TestMailServerConnection" />
                    <%--<input id="btnTestMailServer" class="altLongBlueButtonFormat" type="button" value="Test Connection" onclick="javascript:TestMailServerConnection();" />--%>
                </td>
            </tr>
            <tr>
                <td align="left" class="subtitleblueblodtext" colspan="5" style="font-weight:bold">
                    Directory attiva MS o configurazione della directory LDAP</td>
            </tr>
            <tr>
                <td align="right"
                    height="21" style="font-weight:bold" width="2%">
                </td>
                 <%--Window Dressing--%>
                <td align="right" height="21" style="font-weight:bold" width="18%" class="blackblodtext">
                    indirizzo del server</td>
                <td style="height: 21px;" width="30%">
                    <asp:TextBox ID="txtLDAPServerAddress" runat="server" CssClass="altText"></asp:TextBox>
<%--                     <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txtLDAPServerAddress" Display="dynamic" runat="server" 
                         ErrorMessage="<br>+'&<>%;:( )& / \ ^#$@ and double quotes are invalid characters for this field." ValidationExpression="[A-Za-z0-9._~?!`* \-]+"></asp:RegularExpressionValidator>                    
--%>                    <asp:RegularExpressionValidator runat="server" ID="regAddress" Enabled="false" ControlToValidate="txtLDAPServerAddress" Display="Dynamic" ValidationExpression="^([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}$" ErrorMessage="Indirizzo IP non valido" ></asp:RegularExpressionValidator>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ValidationGroup="LDAP" ErrorMessage="Richiesto" ControlToValidate="txtLDAPServerAddress" Display="dynamic" runat="server" SetFocusOnError="true"></asp:RequiredFieldValidator>
                </td>
                <%--Window Dressing--%>
                <td align="right" style="font-weight:bold" class="blackblodtext">
                    numero di porta</td>
                <td >
                    <asp:TextBox ID="txtLDAPServerPort" runat="server" CssClass="altText" Text="389"></asp:TextBox>
                   <span class="blackblodtext">389 � predefinito</span> <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server"
                        ControlToValidate="txtLDAPServerPort" ErrorMessage="Richiesto."></asp:RequiredFieldValidator></td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ValidationGroup="LDAP" ErrorMessage="Richiesto" ControlToValidate="txtLDAPServerPort" Display="dynamic" runat="server" SetFocusOnError="true"></asp:RequiredFieldValidator>
            </tr>
            <tr>
                <td align="right"
                    height="21" style="font-weight:bold" width="2%">
                </td>
                <%--Window Dressing--%>
                <td align="right" height="21" style="font-weight:bold" width="18%" class="blackblodtext">
                    Accesso account</td>
                <td style="height: 21px;" width="30%">
                    <asp:TextBox ID="txtLDAPAccountLogin" runat="server" CssClass="altText"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" ControlToValidate="txtLDAPAccountLogin" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ e &#34; sono caratteri non validi." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()']*$"></asp:RegularExpressionValidator>                                        
                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" ValidationGroup="LDAP" ErrorMessage="Required" ControlToValidate="txtLDAPAccountLogin" Display="dynamic" runat="server" SetFocusOnError="true"></asp:RequiredFieldValidator>--%><%--PSU--%>
                </td>
                <%--Window Dressing--%>
                <td align="right" style="font-weight:bold" class="blackblodtext">
                    Timeout connessione</td>
                <td style="height: 21px;" width="35%">
                    <asp:DropDownList ID="lstLDAPConnectionTimeout" runat="server" CssClass="altSelectFormat">
                        <asp:ListItem Value="10">10 secondi</asp:ListItem>
                        <asp:ListItem Value="20">20 secondi</asp:ListItem>
                        <asp:ListItem Value="30">30 secondi</asp:ListItem>
                        <asp:ListItem Value="60">60 secondi</asp:ListItem>
                    </asp:DropDownList></td>
            </tr>
            <tr>
                <td align="right"
                    height="21" style="font-weight:bold" width="2%">
                </td>
                <%--Window Dressing--%>
                <td align="right" height="21" style="font-weight:bold" width="18%" class="blackblodtext">
                    Password account</td>
                <td style="height: 21px;" width="30%">
                    <asp:TextBox ID="txtLDAPAccountPassword1" onchange="javascript:PreservePassword();" runat="server" CssClass="altText" TextMode="Password"></asp:TextBox>
                     <asp:RegularExpressionValidator ID="RegularExpressionValidator3" Enabled="false" ControlToValidate="txtLDAPAccountPassword1" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ e &#34; sono caratteri non validi." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()']*$"></asp:RegularExpressionValidator>                    
                    <asp:CompareValidator ID="CompareValidator3" Display="dynamic" runat=server ControlToValidate=txtLDAPAccountPassword1 ControlToCompare=txtLDAPAccountPassword2 ErrorMessage="Le password non corrispondono." Font-Names="Verdana" Font-Size="X-Small" Font-Bold="False" />
                    <%--<asp:RequiredFieldValidator ID="reqLDAPPass" ValidationGroup="LDAP" ErrorMessage="Required" ControlToValidate="txtLDAPAccountPassword1" Display="dynamic" runat="server" SetFocusOnError="true"></asp:RequiredFieldValidator>--%><%--PSU--%></td>
                <%--Window Dressing--%>
                <td align="right" style="font-weight:bold" class="blackblodtext">
                    Riscrivi la password</td>
                <td style="height: 21px;" width="35%">
                    <asp:TextBox ID="txtLDAPAccountPassword2" onchange="javascript:PreservePassword();" runat="server" CssClass="altText" TextMode="Password"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator4" Enabled="false" ControlToValidate="txtLDAPAccountPassword2" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ e &#34; sono caratteri non validi." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()']*$"></asp:RegularExpressionValidator>                    
                    <asp:CompareValidator ID="CompareValidator4" runat="server" ControlToValidate="txtLDAPAccountPassword2" ControlToCompare=txtLDAPAccountPassword1 ErrorMessage="Le password non corrispondono." Font-Names="Verdana" Font-Size="X-Small" Font-Bold="False" /></td>
            </tr>
            <tr>
                <td align="right"
                    height="21" style="font-weight:bold" width="2%">
                </td>
                <%--Window Dressing--%>
                <td align="right" height="21" style="font-weight:bold" width="18%" class="blackblodtext">
                    chiave di accesso</td>
                <td style="height: 21px;" width="30%">
                    <asp:TextBox ID="txtLDAPLoginKey" runat="server" CssClass="altText"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator5" ControlToValidate="txtLDAPLoginKey" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ e &#34; sono caratteri non validi." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()']*$"></asp:RegularExpressionValidator>                    
                </td>
                <%--Window Dressing--%>
                <td align="right" height="21" style="font-weight:bold" width="18%" class="blackblodtext" valign="top">
                    filtro di ricerca
                </td>
                    <!--Added Regular expression by Vivek to perform junk character validation as a fix for issue number 315 -->
                <td style="height: 21px;" width="30%">
                    <asp:TextBox ID="txtLDAPSearchFilter" runat="server" CssClass="altText"></asp:TextBox>
                    &nbsp; <br /><span style="color: #666666;">ex: cn=users,dc=domain,dc=local</span>  <%--FB 2096--%>
                    <asp:RegularExpressionValidator ID="regLDAPSearchFilter" ControlToValidate="txtLDAPSearchFilter" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>' + % \ / ; ? | ^ ` [ ] { } : # $ @ ~ e &#34; sono caratteri non validi." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/+;?|`\[\]{}\x22;^:@#$%~']*$"></asp:RegularExpressionValidator> <%--FB Case 491: Saima --%>
                </td>
            </tr>
            <tr>
                <td align="right"
                    height="21" style="font-weight:bold" width="2%">
                </td>
                <%--Window Dressing--%>
                <td align="right" height="21" style="font-weight:bold" width="18%" class="blackblodtext">
                    prefisso di dominio
                </td>
               <td align="left">
                    <asp:TextBox ID="txtLDAPPrefix" CssClass="altText" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="regConfName" ControlToValidate="txtLDAPPrefix" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ e &#34; sono caratteri non validi." ValidationExpression="^(a-z|A-Z|0-9)*[^<>+;?|!`,\[\]{}\x22;=^:@#$%&()~']*$"></asp:RegularExpressionValidator>
                </td>
                <%--Window Dressing--%>
                <td id="Td1" align="right" runat="server" style="font-weight:bold;display:none" class="blackblodtext">
                    Orario di sincronizzazione
                </td>
                <td align="left" style="display:none">
                <%--Window Dressing--%>
                     <mbcbb:combobox id="lstLDAPScheduleTime" CssClass="altText" CausesValidation="true" runat="server" Enabled="true" Rows="15" Width="100px" style="display:none">
                            <asp:ListItem Value="01:00 AM" Selected="true"></asp:ListItem>
                            <asp:ListItem Value="02:00 AM"></asp:ListItem>
                            <asp:ListItem Value="03:00 AM"></asp:ListItem>
                            <asp:ListItem Value="04:00 AM"></asp:ListItem>
                            <asp:ListItem Value="05:00 AM"></asp:ListItem>
                            <asp:ListItem Value="06:00 AM"></asp:ListItem>
                            <asp:ListItem Value="07:00 AM"></asp:ListItem>
                            <asp:ListItem Value="08:00 AM"></asp:ListItem>
                            <asp:ListItem Value="09:00 AM"></asp:ListItem>
                            <asp:ListItem Value="10:00 AM"></asp:ListItem>
                            <asp:ListItem Value="11:00 AM"></asp:ListItem>
                            <asp:ListItem Value="12:00 PM"></asp:ListItem>
                            <asp:ListItem Value="01:00 PM"></asp:ListItem>
                            <asp:ListItem Value="02:00 PM"></asp:ListItem>
                            <asp:ListItem Value="03:00 PM"></asp:ListItem>
                            <asp:ListItem Value="04:00 PM"></asp:ListItem>
                            <asp:ListItem Value="05:00 PM"></asp:ListItem>
                            <asp:ListItem Value="06:00 PM"></asp:ListItem>
                            <asp:ListItem Value="07:00 PM"></asp:ListItem>
                            <asp:ListItem Value="08:00 PM"></asp:ListItem>
                            <asp:ListItem Value="09:00 PM"></asp:ListItem>
                            <asp:ListItem Value="10:00 PM"></asp:ListItem>
                            <asp:ListItem Value="11:00 PM"></asp:ListItem>
                            <asp:ListItem Value="12:00 AM" Selected="true"></asp:ListItem>
                    </mbcbb:combobox>
                </td>
             </tr>
             <tr>
                <td align="right"
                    height="21" style="font-weight:bold" width="2%">
                </td>
                <%--Window Dressing--%>
                <td align="right" height="21" width="18%" class="blackblodtext" style="display:none">
                    Giorni di sincronizzazione
                </td>
                <td align="left" colspan="4" style="font-weight:normal; color:Blue">
                <%--Window Dressing--%>
                    <asp:CheckBoxList ID="chkLstDays" runat="server" RepeatColumns="9" RepeatLayout="Flow" TextAlign="right" style="display:none">
                        <asp:ListItem Text="Monday" Value="1"><font class='blackblodtext'>Luned�</font></asp:ListItem>
                        <asp:ListItem Text="Tuesday" Value="2"><font class='blackblodtext'>Marted�</font></asp:ListItem>
                        <asp:ListItem Text="Wednesday" Value="3"><font class='blackblodtext'>Mercoled�</font></asp:ListItem>
                        <asp:ListItem Text="Thursday" Value="4"><font class='blackblodtext'>Gioved�</font></asp:ListItem>
                        <asp:ListItem Text="Friday" Value="5"><font class='blackblodtext'>Venerd�</font></asp:ListItem>
                        <asp:ListItem Text="Saturday" Value="6"><font class='blackblodtext'>Sabato</font></asp:ListItem>
                        <asp:ListItem Text="Sunday" Value="7"><font class='blackblodtext'>Domenica</font></asp:ListItem>
                    </asp:CheckBoxList>
                </td>
             </tr>
            <tr>
                <td align="right" colspan="1" style="font-weight:bold" width="2%">
                </td>
                <td align="right" colspan="4" style="font-weight: bold; font-size: small; color: black;
                    font-family: verdana; height: 21px; text-align: center;">
                        <asp:Button runat="server" ID="btnTestLDAPConnection" ValidationGroup="LDAP" Text="verifica connessione" CssClass="altLongBlueButtonFormat"  OnClick="TestLDAPConnection" />
<%--               			<input type="button" name="btnTestLDAPServer" value="Test Connection" class="altLongBlueButtonFormat" onclick="javascript:TestLDAPServerConnection();">
--%>               			&nbsp;<asp:Button runat="server" ID="btnSyncLdapNow" Text="Sincronizza ora" CssClass="altLongBlueButtonFormat" onClick="SyncNow" style="display:none;"/>
                </td>
            </tr>
            
            <tr>
                <td align="right"
                    height="21" style="font-weight:bold" width="2%">
                </td>
                <td align="right"
                    height="21" style="font-weight:bold" width="18%">
                </td>
                <td style="height: 21px; text-align: center;" width="30%">
                </td>
                <td align="right" style="font-weight: bold; font-size: small; width: 15%; color: black;
                    font-family: verdana; height: 21px; text-align: center">
                </td>
                <td style="height:21; font-weight:bold" width="35%">
                </td>
            </tr>
            <tr>
          <td colspan="5" align=left>
		    <table width=100%>
              <tr>
                <td width="96%" height="20" align=left>
		          <SPAN class=subtitleblueblodtext>opzioni server</SPAN>
                </td>
              </tr>
      		</table>
		  </td>
        </tr>
        <tr>
            <td style="width: 2%"></td>
            <td align="right" height="21" style="font-weight:bold" width="18%" class="blackblodtext"><b>fuso orario server</b></td>
            <td align="left" style="width: 183px"> 
               <asp:DropDownList ID="PreferTimezone" runat="server" CssClass="altLong0SelectFormat" DataTextField="timezoneName" OnInit="UpdateTimezones" DataValueField="timezoneID" Width="205px">
               </asp:DropDownList>
              <asp:RequiredFieldValidator ID="TimeZoneValidator" runat="server" ControlToValidate="PreferTimezone" ErrorMessage="Richiesto" Font-Bold="True"  InitialValue="-1"  Display="Dynamic"></asp:RequiredFieldValidator>
            </td> <%--FB 2007--%>
            <td align="right" height="21" style="font-weight:bold" width="18%" class="blackblodtext">
                Lancia buffer (in minuti)</td>
            <td style="height: 21px;" width="30%">
                <asp:TextBox ID="txtLaunchBuffer" runat="server" CssClass="altText"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator7" ControlToValidate="TxtLaunchBuffer" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>Si prega di inserire solo numeri." ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>                    
            </td>
        </tr>
        <tr> <%--FB 2437--%>
            <%--FB 2501 starts--%>
         <td style="width: 3%">
            </td>
         <td align="right" height="21" style="font-weight: bold; width: 18%" class="blackblodtext">
                 <b>Modalit� di avvio</b>
         </td>
         <td align="left" style="width: 15%">
                <asp:DropDownList ID="lstStartMode" runat="server" Width="30%" CssClass="alt2SelectFormat">
             <asp:ListItem Value="0" Selected="True" Text="automatico"></asp:ListItem>
             <asp:ListItem Value="1" Text="manuale"></asp:ListItem>
           </asp:DropDownList>  
         </td>
         <td align="right" height="21" style="font-weight: bold" class="blackblodtext">
            <asp:Label ID="Label8" runat="server" Text="Abilitare l'avvio Buffer per P2P"></asp:Label>
         </td>
        <td style="height: 21px;">
           <asp:DropDownList ID="lstEnableLaunchBufferP2P" runat="server" Width="20%" CssClass="alt2SelectFormat">
             <asp:ListItem Value="0" Selected="True" Text="No"></asp:ListItem>
             <asp:ListItem Value="1" Text="s�"></asp:ListItem>
           </asp:DropDownList>
        </td>
        <%--FB 2501 ends--%>
        </tr>
         <%--FB 2501 EM7  Start--%>
         <%-- FB 2598 EnableEM7 Starts tr-id --%>
        <tr id="trEM7Connectivity" runat="server">
            <td colspan="5" align="left">
                <table width="100%">
                    <tr>
                        <td width="96%" height="20" align="left">
                            <span class="subtitleblueblodtext">EM7 Connettivit�</span>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
         <tr id="trEM7URIUsrName" runat="server">
            <td align="right" height="21" style="font-weight: bold" width="2%">
            </td>
            <td align="right" height="21" style="font-weight: bold" class="blackblodtext" width="18%">
                EM7 URI
            </td>
            <td style="height: 21px;" width="30%">
                <asp:TextBox ID="txtEM7URI" runat="server" CssClass="altText" MaxLength="256"></asp:TextBox>                                
                <asp:RegularExpressionValidator ID="RegularExpressionValidator18" ControlToValidate="txtEM7URI" ValidationGroup="submit"
                    Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \  ( ) ; ? | ^ = ! ` , [ ] { }  # $ @ ~  &#34; sono caratteri non validi."
                    ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()']*$"></asp:RegularExpressionValidator>
                
            </td>
           <td align="right" style="font-weight: bold" class="blackblodtext">
                EM7 nome utente
            </td>
            <td style="height: 21px;" width="35%">
                <asp:TextBox ID="txtEM7Username" runat="server" CssClass="altText" MaxLength="50"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator17" ControlToValidate="txtEM7Username" ValidationGroup="submit"
                    Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~  &#34; sono caratteri non validi."
                    ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()']*$"></asp:RegularExpressionValidator>
                
            </td>
        </tr>
        <tr id="trEM7Pwd" runat="server">
            <td align="right">
            </td>
            <td align="right" style="font-weight: bold" width="18%" class="blackblodtext">
                EM7 password
            </td>
            <td style="height: 37px;" width="30%" nowrap="nowrap">
                <asp:TextBox ID="txtEM7Password" runat="server" CssClass="altText" TextMode="Password" onchange="javascript:PreservePassword()" MaxLength="20"></asp:TextBox>   
                <asp:CompareValidator ID="cmpValPassword1" runat="server" ValidationGroup="submit" ControlToCompare="txtEM7ConformPassword"
                    ControlToValidate="txtEM7Password" Display="Dynamic" ErrorMessage="<br>Inserire nuovamente la password."></asp:CompareValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator12" Enabled="true" ValidationGroup="submit"
                    ControlToValidate="txtEM7Password" Display="dynamic" runat="server" SetFocusOnError="true"
                    ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ <br> &#34; sono caratteri non validi."
                    ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()']*$"></asp:RegularExpressionValidator>             
            </td>  
             <td align="right" style="font-weight: bold" width="18%" class="blackblodtext">
                EM7 Riscrivi la password
            </td>
            <td style="height: 37px;" width="30%">
                <asp:TextBox ID="txtEM7ConformPassword" runat="server" CssClass="altText" onchange="javascript:PreservePassword()" TextMode="Password" MaxLength="20"></asp:TextBox> 
                <asp:CompareValidator ID="CompareValidator6" runat="server" ValidationGroup="submit"
                    ControlToValidate="txtEM7ConformPassword" ControlToCompare="txtEM7Password" ErrorMessage="La password non corrisponde."
                    Font-Bold="False" />   
                <asp:RegularExpressionValidator ID="RegularExpressionValidator13" Enabled="true" ValidationGroup="submit"
                    ControlToValidate="txtEM7ConformPassword" Display="dynamic" runat="server" SetFocusOnError="true"
                    ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ <br> &#34;sono caratteri non validi."
                    ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()']*$"></asp:RegularExpressionValidator> 
             </td>  
        </tr>
         <tr id="trEM7Port" runat="server">
          <td align="right" height="21" style="font-weight: bold" width="2%">
            </td>
            <td align="right" height="21" style="font-weight: bold" class="blackblodtext" width="18%">
                EM7 Porta
            </td> 
          <td style="height: 21px;" width="35%">
                <asp:TextBox ID="txtEM7Port" runat="server" CssClass="altText" MaxLength="4" ></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator19" ControlToValidate="txtEM7Port"
                    Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>Solo i valori numerici."
                    ValidationExpression="[\d]+"></asp:RegularExpressionValidator>
          </td>
         </tr>
         <%-- FB 2598 EnableEM7 Ends tr-id --%>
         <%--FB 2501 EM7 End--%>
         <%-- FB 2363 - Start--%>
        <%if((Application["External"].ToString() != "")){%>
        <tr>
            <td colspan="5" align="left">
                <span class="subtitleblueblodtext">Impostazioni di pianificazione esterni</span>
            </td>
        </tr>        
        <tr>
            <td style="width: 3%">
            </td>
            <td class="blackblodtext" align="right">
                Nome partner
            </td>
            <td>
                <asp:TextBox ID="txtPartnerName" runat="server" CssClass="altText" Width="200px"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator14" ControlToValidate="txtPartnerName"
                    ValidationGroup="submit" Display="dynamic" runat="server" SetFocusOnError="true"
                    ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ e &#34; sono caratteri non validi."
                    ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()']*$"></asp:RegularExpressionValidator>
            </td>
            <td class="blackblodtext" align="right">
                Partner e-mail
            </td>
            <td>
                <asp:TextBox ID="txtPartnerEmail" runat="server" CssClass="altText" Width="200px"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator15" is="regSystemEmail"
                    runat="server" ControlToValidate="txtPartnerEmail" ValidationGroup="submit" ErrorMessage="Indirizzo e-mail non valido"
                    Display="dynamic" ValidationExpression="^[a-zA-Z()^][()^\w\.-]*[a-zA-Z0-9()^]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"></asp:RegularExpressionValidator>
                <%--FB Case 699--%>
            </td>
        </tr>
        <tr>
            <td style="width: 3%">
            </td>
            <td class="blackblodtext" align="right">
                Partner URL
            </td>
            <td>
                <asp:TextBox ID="txtPartnerURL" runat="server" CssClass="altText" TextMode="MultiLine"
                    Width="200px"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator11" ControlToValidate="txtPartnerURL"
                    ValidationGroup="submit" Display="dynamic" runat="server" SetFocusOnError="true"
                    ErrorMessage="<br>& < > ' + % \ ( ) ; ? | ^ = ! ` , [ ] { } # $ @ ~ e &#34; sono caratteri non validi."
                    ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>+;?|!`,\[\]{}\x22;=^@#$%&()']*$"></asp:RegularExpressionValidator>
            </td>
            <td class="blackblodtext" align="right">
                Nome Utente
            </td>
            <td>
                <asp:TextBox ID="txtPUserName" runat="server" CssClass="altText" Rows="3" Width="200px"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator16" ControlToValidate="txtPUserName"
                    ValidationGroup="submit" Display="dynamic" runat="server" SetFocusOnError="true"
                    ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ e &#34; sono caratteri non validi."
                    ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()']*$"></asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td style="width: 3%">
            </td>
            <td class="blackblodtext" align="right">
                Password
            </td>
            <td style="height: 21px;">
                <asp:TextBox ID="txtP1Password" onchange="javascript:PreservePassword();" runat="server"
                    CssClass="altText" TextMode="Password"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator8" Enabled="false"
                    ControlToValidate="txtP1Password" Display="dynamic" runat="server" SetFocusOnError="true"
                    ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ e &#34; sono caratteri non validi."
                    ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()']*$"></asp:RegularExpressionValidator>
                <asp:CompareValidator ID="CompareValidator1" Display="dynamic" ValidationGroup="submit"
                    runat="server" ControlToValidate="txtP1Password" ControlToCompare="txtP2Password"
                    ErrorMessage="Le password non corrispondono." Font-Names="Verdana" Font-Size="X-Small"
                    Font-Bold="False" />
            </td>
            <td align="right" style="font-weight: bold" class="blackblodtext">
                Riscrivi la Password
            </td>
            <td style="height: 21px;">
                <asp:TextBox ID="txtP2Password" onchange="javascript:PreservePassword();" runat="server"
                    CssClass="altText" TextMode="Password"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator10" Enabled="false"
                    ControlToValidate="txtP2Password" Display="dynamic" runat="server" SetFocusOnError="true"
                    ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ e &#34; sono caratteri non validi."
                    ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()']*$"></asp:RegularExpressionValidator>
                <asp:CompareValidator ID="CompareValidator5" runat="server" ValidationGroup="submit"
                    ControlToValidate="txtP2Password" ControlToCompare="txtP1Password" ErrorMessage="Le password non corrispondono."
                    Font-Names="Verdana" Font-Size="X-Small" Font-Bold="False" />
            </td>
        </tr>
        <tr>
            <td style="width: 3%"> <%--FB 2363K--%>
            </td>
            <td class="blackblodtext" align="right">
                Il valore di timeout
            </td>
            <td style="height: 21px;">
                <asp:TextBox ID="txtTimeoutValue" runat="server" CssClass="altText"></asp:TextBox>
                <asp:RegularExpressionValidator ID="regTimeoutVlaue" runat="server" ControlToValidate="txtTimeoutValue" ValidationGroup="submit" Display="Dynamic" ErrorMessage="Solo valori numerici." ValidationExpression="[\d]+" ></asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td colspan="5" align="left">
                <span class="subtitleblueblodtext">evento coda</span>
            </td>
        </tr>
        <tr>
            <td style="width: 2%"></td>
            <td  style="width: 18%"></td>
            <td align="left" colspan="2">
                <input type="button" id="btnEvent" runat="server" name="btnEvent" value="Gestione degli eventi"
                    class="altLongBlueButtonFormat"  onclick="javascript:fnView();"/>
            </td>
            <td></td>
        </tr>
        <tr>
            <td colspan="5" align="left">
                <span class="subtitleblueblodtext">Mailing utente Impostazioni report</span>
            </td>
        </tr>
        <tr>
            <td style="width: 2%">
            </td>
            <td class="blackblodtext" align="right" style="width: 18%">
                Nome cliente
            </td>
            <td style="width: 30%">
                <asp:TextBox ID="txtUsrRptCustmName" runat="server" CssClass="altText" Width="200px"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegtxtUsrRptCustmName" ControlToValidate="txtUsrRptCustmName"
                    ValidationGroup="submit" Display="dynamic" runat="server" SetFocusOnError="true"
                    ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ e &#34; sono caratteri non validi."
                    ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()']*$"></asp:RegularExpressionValidator>
            </td>
            <td class="blackblodtext" align="right">
                destinazione
            </td>
            <td>
                <asp:TextBox ID="txtUsrRptDestination" runat="server" CssClass="altText" Width="200px"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegtxtUsrRptDestination" ControlToValidate="txtUsrRptDestination"
                    ValidationGroup="submit" Display="dynamic" runat="server" SetFocusOnError="true"
                    ErrorMessage="<br>Indirizzo e-mail non valido." ValidationExpression="[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"></asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td style="width: 3%"></td>
            <td class="blackblodtext" align="right">
                Ora di inizio
            </td>
            <td>
                <asp:TextBox ID="txtUsrRptStartTime" runat="server" CssClass="altText" Width="70px"></asp:TextBox>
                <img alt="" src="image/calendar.gif" border="0" width="20" height="20" id="cal_trigger1"
                    style="cursor: pointer; vertical-align: middle" onclick="return showCalendar('<%=txtUsrRptStartTime.ClientID %>', 'cal_trigger1', 0, '<%=dtFormatType%>');" />
            </td>
            <td class="blackblodtext" align="right">
                Tempo di Sent
            </td>
            <td>
                <asp:TextBox ID="txtUsrRptSentTime" runat="server" CssClass="altText" Enabled="false" Width="70px"
                    ReadOnly="true"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 3%">
            </td>
            <td class="blackblodtext" align="right">
                Frequenza di conteggio / Tipo
            </td>
            <td>
                <asp:DropDownList ID="lstUsrRptFrequencyCount" runat="server" CssClass="altSelectFormat"
                    Width="15%">
                    <asp:ListItem Value="1">1</asp:ListItem>
                    <asp:ListItem Value="2">2</asp:ListItem>
                    <asp:ListItem Value="3">3</asp:ListItem>
                    <asp:ListItem Value="4">4</asp:ListItem>
                </asp:DropDownList>
                <asp:DropDownList ID="lstUsrRptFrequencyType" runat="server" CssClass="altSelectFormat"
                    Width="30%">
                    <asp:ListItem Value="1">giorni</asp:ListItem>
                    <asp:ListItem Value="2">settimane</asp:ListItem>
                    <asp:ListItem Value="3">mesi</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td colspan="2"></td>
        </tr>
        <tr style="display:none">
            <td style="width: 3%">
            </td>
            <td class="blackblodtext" align="right">
                DeliverType
            </td>
            <td>
                <asp:DropDownList ID="lstUsrRptDeliverType" runat="server" CssClass="altSelectFormat"
                    Width="15%">
                    <asp:ListItem Value="1">1</asp:ListItem>
                    <asp:ListItem Value="2">2</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td colspan="2"></td>            
          </tr>
        <%} %>
        <%-- FB 2363 - End--%>
          <%-- FB 2392 - Start--%>
        <tr>
            <td colspan="5" align="left">
            </td>
         </tr>
         <tr runat="server" id="trWhygo" >
            <td colspan="5" align="left">
                <span class="subtitleblueblodtext">WhyGo Integration Settings</span>
            </td>
         </tr>
         <tr runat="server" id="trWhygoURL">
            <td style="width: 2%">
            </td>
            <td class="blackblodtext" align="right" style="width: 18%">
                URL
            </td>
            <td style="width: 30%">
                <asp:TextBox ID="txtWhyGoURL" runat="server" CssClass="altText" Width="200px" onkeyup="SetButtonStatus(this, 'PollWhyGo') "></asp:TextBox> <%--FB 2594--%>
                <asp:RegularExpressionValidator ID="regttxtWhyGoURL" ControlToValidate="txtWhyGoURL"
                    ValidationGroup="submit" Display="dynamic" runat="server" SetFocusOnError="true"
                    ErrorMessage="<br> & < and > are invalid characters." ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator>
            </td>
             <td class="blackblodtext" align="right">
                 User Name
            </td>
            <td>
                <asp:TextBox ID="txtWhygoUsr" runat="server" CssClass="altText" Width="200px"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RglrvalidatorWhygo12" ControlToValidate="txtUsrRptCustmName"
                    ValidationGroup="submit" Display="dynamic" runat="server" SetFocusOnError="true"
                    ErrorMessage="<br> & < and > are invalid characters." ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator>
            </td>
        </tr>
         <tr runat="server" id="trWhygoPassword">
             <td style="width: 3%">
            </td>
            <td class="blackblodtext" align="right">
                Password
            </td>
            <td style="height: 21px;">
                <asp:TextBox ID="txtWhygoPwd" onchange="javascript:PreservePassword();" runat="server"
                    CssClass="altText" TextMode="Password"></asp:TextBox>
                <asp:RegularExpressionValidator ID="regtxtWhygoPwd" Enabled="false"
                    ControlToValidate="txtWhygoPwd" Display="dynamic" runat="server" SetFocusOnError="true"
                    ErrorMessage="<br> & < and > are invalid characters." ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator>
                <asp:CompareValidator ID="cmptxtWhygoPwd" Display="dynamic" ValidationGroup="submit"
                    runat="server" ControlToValidate="txtWhygoPwd" ControlToCompare="txtWhygoPwd2"
                    ErrorMessage="Passwords do not match." Font-Names="Verdana" Font-Size="X-Small"
                    Font-Bold="False" />
            </td>
            <td align="right" style="font-weight: bold" class="blackblodtext">
                Retype Password
            </td>
            <td style="height: 21px;">
                <asp:TextBox ID="txtWhygoPwd2" onchange="javascript:PreservePassword();" runat="server"
                    CssClass="altText" TextMode="Password"></asp:TextBox>
                <asp:RegularExpressionValidator ID="regtxtWhygoPwd2" Enabled="false"
                    ControlToValidate="txtWhygoPwd2" Display="dynamic" runat="server" SetFocusOnError="true"
                    ErrorMessage="<br> & < and > are invalid characters." ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator>
                <asp:CompareValidator ID="cmptxtWhygoPwd2" runat="server" ValidationGroup="submit"
                    ControlToValidate="txtWhygoPwd2" ControlToCompare="txtWhygoPwd" ErrorMessage="Passwords do not match."
                    Font-Names="Verdana" Font-Size="X-Small" Font-Bold="False" />
            </td>
        </tr>
         <tr runat="server" id="trPoll" >
            <td style="width: 3%">
            </td>
            <td class="blackblodtext" align="right">
                 Poll
            </td>
            <td style="height: 21px;">
                <asp:Button ID="PollWhyGo" runat="server" ValidationGroup="submit" Enabled="false" CssClass="altShortBlueButtonFormat" Text="Now" OnClick="PollWhyGoNow"/>  <%-- OnClick="PollWhyGo"--%>
                <asp:ImageButton ImageUrl="image/btn_delete.gif" runat="server" ID="delPubliRoom"
                                        ToolTip="Remove Public Rooms" OnClick="DeletePublicRooms" />
            </td>
            <td colspan="2">
            </td>
        </tr>
        <%-- FB 2392 - End--%>    
        <tr>
            <%--  Commented for FB 1849
            <td colspan="5" align="center"> 
            <ajax:ModalPopupExtender ID="RoomPopUp" runat="server" TargetControlID="ChgOrg" BackgroundCssClass="modalBackground"   PopupControlID="switchOrgPnl" DropShadow="false" Drag="true"  CancelControlID="ClosePUp"></ajax:ModalPopupExtender>
            <asp:Panel ID="switchOrgPnl" runat="server" HorizontalAlign="Center" Width="35%"  CssClass="treeSelectedNode" >
                <table width="100%" align="center" border="0"> 
                    <tr>
                      <td align="center" class="blackblodtext"><SPAN class=subtitleblueblodtext>Switch Organization</SPAN><br />
                      <p>
                      All the transactions performed will be for the below selected organization after the switch.
                     </p>
                      </td>
                      </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td align="center"> 
                        <asp:DropDownList ID="DrpOrganization" DataTextField="OrganizationName" DataValueField="OrgId" runat="server" CssClass="altLong0SelectFormat" Width="205px">
                        </asp:DropDownList>
                      </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="center">
                     <asp:button runat="server" ID="BtnChangeOrganization" CssClass="altShortBlueButtonFormat" Text=" Submit " OnClientClick="javascript:return fnChangeOrganization();" OnClick="btnChgOrg_Click" ></asp:button>&nbsp;
                     <input align="middle" type="button" runat="server" style="width:100px;height:21px;" id="ClosePUp" value=" Close " class="altButtonFormat" onclick="javascript:fnClearOrg();" />
                      </td>
                    </tr>
                </table>
              </asp:Panel>
            </td>--%>
        </tr> 
           <tr>
                <td align="center" colspan="5" style="font-weight: bold; font-size: small; color: black;
                    font-family: verdana; height="21" style="font-weight:bold">
                    <input id="btnReset" type="reset" value="Reset" class="altShortBlueButtonFormat" />&nbsp;
                            <asp:button runat="server" ID="btnSubmit" CssClass="altShortBlueButtonFormat"  OnClick="btnSubmit_Click"  Text="Invia" ValidationGroup="submit" ></asp:button>
                                        
            </tr>
        </table>
        <input type="hidden" name="formname" id="formname" value="frmMainsuperadministrator" />
<img src="keepalive.asp" name="myPic" width="1" height="1">
<script  language="javascript">
    //document.getElementById("lstLDAPScheduleTime_Text").style.cssText = "altText";
//PreservePassword();

    //FB 2363
    function fnView() {
        window.location.replace("ESEventReport.aspx");
    }
    
//Code added fro FB 1428
    function fnTransferPage()
    {
        window.location.replace("UITextChange.aspx");
    }
    
    /*Commented for FB 1849
    function fnClearOrg() 
    {
        var obj1 = document.getElementById('DrpOrganization');
        if(obj1)
        {
            obj1.value = '<%=orgid%>';
        }        
    }*/
    function Validation()
    {
    var file1 = document.getElementById('fleMap1');
    var msg = document.getElementById('txtCompanymessage');
    var lblval = document.getElementById('lblvalidation');
    var lblfile = document.getElementById('lblfilevalidation');
    if(file1.value== "")
    {
    lblval.value = "Richiesto";
    return false;
    }
    else if(msg.value== "")
    {
    lblfile.value = "Richiesto";
    return false;
    }
    else
    return true;
    
    }
    
    SavePassword();//FB 1896
</script>
</form>
</body>
</html>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
<!-- FB 2050 Start -->
<script type="text/javascript">

function refreshImage()
{
  var obj = document.getElementById("mainTop");
  if(obj != null)
  {
      var src = obj.src;
      var pos = src.indexOf('?');
      if (pos >= 0) {
         src = src.substr(0, pos);
      }
      var date = new Date();
      obj.src = src + '?v=' + date.getTime();
      
      if(obj.width > 804)
      obj.setAttribute('width','804');
  }
  //refreshStyle(); // Commented for Refresh Issue
  setMarqueeWidth();
  return false;
}

function refreshStyle()
{
	var i,a,s;
	a=document.getElementsByTagName('link');
	for(i=0;i<a.length;i++) {
		s=a[i];
		if(s.rel.toLowerCase().indexOf('stylesheet')>=0&&s.href) {
			var h=s.href.replace(/(&|\\?)forceReload=d /,'');
			s.href=h+(h.indexOf('?')>=0?'&':'?')+'forceReload='+(new Date().valueOf());
		}
	}
}

function setMarqueeWidth()
{
    var screenWidth = screen.width - 25;
    if(document.getElementById('martickerDiv')!=null)
        document.getElementById('martickerDiv').style.width = screenWidth + 'px';
        
    if(document.getElementById('marticDiv')!=null)
        document.getElementById('marticDiv').style.width = screenWidth + 'px';
    
    if(document.getElementById('marticker2Div')!=null)
        document.getElementById('marticker2Div').style.width = (screenWidth-15) + 'px';
    
    if(document.getElementById('martic2Div')!=null)
        document.getElementById('martic2Div').style.width = (screenWidth-15) + 'px';
}

window.onload = refreshImage;

</script>

<!-- FB 2050 End -->
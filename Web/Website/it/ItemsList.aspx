<%@ Page Language="C#" Inherits="ns_ItemsList.ItemsList" EnableSessionState="True" ValidateRequest="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--Window Dressing Start-->
  <link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/border-table.css"/>
  <link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/main-table.css"/>
   <!--Window Dressing End-->
<link rel="stylesheet" type="text/css" href="<%=Session["OrgCSSPath"]%>">
<script>

</script>
<script runat="server">

</script>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Elenco elementi</title>
    <script language="javascript">
        function checkFileExtension(elem) {
            var filePath = elem.value;

            if(filePath.indexOf('.') == -1)
                return false;
            
            var validExtensions = new Array();
            var ext = filePath.substring(filePath.lastIndexOf('.') + 1).toLowerCase();

            validExtensions[0] = 'jpg';
            validExtensions[1] = 'jpeg';
            validExtensions[2] = 'bmp';
            validExtensions[3] = 'png';
            validExtensions[4] = 'gif';  

            for(var i = 0; i < validExtensions.length; i++) {
                if(ext == validExtensions[i])
                    return true;
            }
            alert('Lestensione del file \"' + ext.toUpperCase() + '\" non � consentita!');
            return false;
        }
        
    </script>
</head>
<body>
    <form id="frmItemsList" runat="server" method="post" onsubmit="return true">
        <center>
    <div>
        <table width="100%" border="0">
            <tr>
                <td align="center" colspan="3">
                    <h3>Seleziona elementi dalla lista qui sotto</h3>
                </td>
            </tr>
            <tr>
                <td align="center" colspan="3">
                    <asp:Label ID="errLabel" runat="server" Font-Size="Small" Visible="False"></asp:Label><%--FB 2487--%>
                    <input type="hidden" id="txtType" value=""  />
                    <input runat="server" type="hidden" id="txtSrcID" value=""  />
            </td>
            </tr>
            <tr>
                <td align="center" colspan="3">
                    <asp:DataGrid BorderColor="blue" BorderStyle="solid" BorderWidth="1" ID="dgItems" AutoGenerateColumns="false"
                     OnItemCreated="BindRowsDeleteMessage" OnDeleteCommand="DeleteItem" OnItemDataBound="BindImages"
                     runat="server" Width="90%" GridLines="None" style="border-collapse:separate"> <%--Edited for FF--%>
                        <HeaderStyle Height="30" CssClass="tableHeader" HorizontalAlign="Center" />
                         <%--Window Dressing - Start --%>
                        <AlternatingItemStyle CssClass="tableBody" />
                        <ItemStyle CssClass="tableBody" HorizontalAlign="Center"/> <%--Edited for FF--%>
                        <FooterStyle CssClass="tableBody" />
                        <%--Window Dressing - End --%>
                        <Columns>
                            <asp:BoundColumn DataField="ID" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Image" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="ImageId" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="ImageName" Visible="false"></asp:BoundColumn>
                                                                                    
                            <asp:TemplateColumn HeaderText="Seleziona" HeaderStyle-CssClass="tableHeader" ItemStyle-Width="50" ItemStyle-HorizontalAlign="center">
                                <ItemTemplate>
                                    <asp:CheckBox id="chkSelectItem" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="Name" Visible="true" HeaderText="Nome" HeaderStyle-CssClass="tableHeader"></asp:BoundColumn>
                            
                            <asp:TemplateColumn HeaderText="Immagine" HeaderStyle-CssClass="tableHeader" ItemStyle-Width="50" ItemStyle-HorizontalAlign="center">
                                <ItemTemplate>
                                    <%--<cc1:ImageControl id="itemImage" Width="30" Height="30" Visible="false" Runat="server"></cc1:ImageControl>--%>
                                    <%--<asp:Image ID="itemImage" visible="true" ImageUrl='<%# DataBinder.Eval(Container, "DataItem.ImagePath") %>'  Width="30" Height="30" runat="server" />--%>
                                    <asp:Image ID="itemImage" visible="true" Width="30" Height="30" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Azioni" HeaderStyle-CssClass="tableHeader" HeaderStyle-HorizontalAlign="Center"> <%--Edited for FF--%>
                                <ItemTemplate>
                                    <asp:LinkButton ID="btnDelete" Text="Elimina" CommandName="Delete" runat="server"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </td>
            </tr>
            <tr>
                <td align="center" colspan="3">
            <table>
                <tr>
                    <td colspan="3" rowspan="3" height="40" valign="bottom">
                        <%--Code changed for Softedge button--%>
                        <input type="button" name="Annulla" onclick="javascript:window.close()" value="Annulla" class="altShortBlueButtonFormat"/>&nbsp;
                        <asp:Button ID="btnSubmit" runat="server" CssClass="altShortBlueButtonFormat" OnClick="btnSubmit_Click" Text="Invia" />
                    </td>
                </tr>
                <tr>
                </tr>
            </table>
                    <asp:HiddenField ID="selItems" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <table width="90%">
                        <tr>
                            <td align="left">
                                <span class="subtitleblueblodtext">Carica nuovo elemento:</span>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <table width="90%" cellspacing="5">
                                    <tr>
                                    <%--Window Dressing--%>
                                        <td align="right" class="blackblodtext">
                                            Inserisci nome
                                        </td>
                                        <td align="left">
                                             <%--Window Dressing--%>
                                            <asp:TextBox ID="txtItemID" Text="new" Visible="false" CssClass="altText" runat="server"></asp:TextBox>
                                            <asp:TextBox ID="txtItemName" runat="server" CssClass="altText" ></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="reqName" ValidationGroup="Upload" runat="server" ControlToValidate="txtItemName" ErrorMessage="Richiesto" Display="dynamic" ></asp:RequiredFieldValidator>
                                            <!--[Vivek Issue no - 282] Regular expression validation added for ItemName -->
                                            <asp:RegularExpressionValidator ID="regItemName1" ControlToValidate="txtItemName" Display="dynamic" runat="server" ValidationGroup="Upload" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ e &#34; sono caratteri non validi." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                       <%--Window Dressing--%>
                                        <td align="right" class="blackblodtext">
                                            Carica Immagini
                                        </td>
                                        <td align="left">
                                            <asp:FileUpload ID="fleImage" runat="server" CssClass="altText" ></asp:FileUpload>
                                            <asp:RequiredFieldValidator ID="reqImage" ValidationGroup="Upload" runat="server" ControlToValidate="fleImage" ErrorMessage="Richiesto" Display="dynamic" ></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" colspan="2">
                                            <asp:Button ID="btnAddItem" OnClick="AddCategoryItem" ValidationGroup="Upload" runat="server" CssClass="altShortBlueButtonFormat" Text="Invia"></asp:Button>
                                            <asp:CustomValidator ID="cvAdd" Text="File non valido" OnServerValidate="ValidateInput" ValidationGroup="Upload" runat="server" ></asp:CustomValidator>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
        <br />
            <br />
            &nbsp;
        </center>
        <script language="javascript">
            document.getElementById("txtType").value = "<%= Request.QueryString["type"].ToString() %>";
            document.getElementById("txtSrcID").value = "<%= Request.QueryString["srcID"].ToString() %>";
            document.getElementById("txtSrcID").value = document.getElementById("txtSrcID").value.substring(0,document.getElementById("txtSrcID").value.lastIndexOf("_")) + "_selCateringItems";
            
            //FB 2487 - Start RIUSCITA - Success, NON RIUSCITA - Unsuccess, ERRORE - Error
            var obj = document.getElementById("errLabel");
            if (obj != null) {
                var strInput = obj.innerHTML.toUpperCase();    
                if (((strInput.indexOf("SUCC") > -1) || (strInput.indexOf("RIUSCITA") > -1)) && !(strInput.indexOf("UNSUCC") > -1) && !(strInput.indexOf("NON RIUSCITA") > -1) && !(strInput.indexOf("ERRORE") > -1))
                {      
                    obj.setAttribute("class", "lblMessage");
                    obj.setAttribute("className", "lblMessage");
                }
                else 
                {
                    obj.setAttribute("class", "lblError");
                    obj.setAttribute("className", "lblError");
                }
            }
            //FB 2487 - End 
        </script>
    </form>
</body>
</html>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
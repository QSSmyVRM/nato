<%@ Page Language="C#" AutoEventWireup="true"  Inherits="en_EmailSearch"  %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>myVRM</title>
     <link title="Expedite base styles" href="<%=Session["OrgCSSPath"]%>" type="text/css" rel="stylesheet" />
     
</head>

<body>
   <form name="frmEmailsearch" id="frmEmailsearch" runat="server">
  <input type="hidden" name="frm" value="<% =Request.QueryString["frm"].ToString() %>" />
  <input type="hidden" name="t" value="<% =Request.QueryString["t"].ToString() %>" />
  <script language="JavaScript" src="inc/functions.js"></script>
 
  <center>
    <h3>Ricerca in Rubrica myVRM</h3>
         <%--Window Dressing--%>
    <br /><font  class="blackblodtext">Si prega di utilizzare qualsiasi stringa di lettere per la ricerca nella rubrica.</font> <br /><br /><br />
  
    <table cellpadding="6" cellspacing="5">
<%
if (Request.QueryString ["t"] != "g" ){
%>
      <tr>
         <%--Window Dressing--%>
        <td class="blackblodtext"> 
          <div align="center">Nome di accesso</div>
        </td>
        <td> 
          <input type="text" name="LoginName" size="15" class="altText" maxlength="256"  onkeyup="javascript:chkLimit(this,'2');" />
        </td>
      </tr>

<%	
}
%>
      <tr>
      <%--Window Dressing--%>
        <td class="blackblodtext"> 
          <div align="center">Nome</div>
        </td>
        <td>           
            <%--FB 1888--%>
          <asp:TextBox ID="FirstName" runat="server" size="15" class="altText" maxlength="256" ></asp:TextBox>
          <asp:RegularExpressionValidator ID="RegularExpressionValidator20" ControlToValidate="FirstName" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > + % \ / ? | = ! ` [ ] { } # $ @ e ~ sono caratteri non validi." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+?|!`\[\]{}\=@#$%&~]*$"></asp:RegularExpressionValidator> 
        </td>
        <td>&nbsp;</td>
      <%--Window Dressing--%>
        <td class="blackblodtext"> 
          <div align="center">Cognome</div>
        </td>
        <td>        
            <%--FB 1888--%>   
          <asp:TextBox ID="LastName" runat="server" size="15" class="altText" maxlength="256" ></asp:TextBox>
          <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="LastName" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > + % \ / ? | = ! ` [ ] { } # $ @ e ~ sono caratteri non validi." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+?|!`\[\]{}\=@#$%&~]*$"></asp:RegularExpressionValidator> 
        </td>
      </tr>
    </table>

   <br /><br />

    <table cellpadding="4" cellspacing="6">
      <tr>
        <td>
         <input type="button" name="EmailSearchSubmit" value="Chiudi finestra" class="altBlueButtonFormat" onclick="JavaScript: window.close();" />
        </td>
        <td>
        <%--Code Changed for SoftEdge Corner--%>
         <%--<input type="submit" onfocus="this.blur()" name="EmailSearchSubmit" value="Search" class="altShortBlueButtonFormat" />--%>
       <asp:Button ID="btnSearch" runat="server" onfocus="this.blur()" Text="Ricerca" OnClick="SearchUsers" class="altShortBlueButtonFormat" />
</td>
      </tr>
    </table>
    
  </center>

  <input type="hidden" name="fn" value="<% =Request.QueryString["fn"].ToString() %>" />
  <input type="hidden" name="n" value="<% =Request.QueryString["n"].ToString() %>" /> 
  <input type="hidden" name="cmd" value="RetrieveUsers" />
  </form>	 
</body>
</html>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
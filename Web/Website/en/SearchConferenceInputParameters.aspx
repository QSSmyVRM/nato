<%@ Page Language="C#" Inherits="ns_SearchConference.SearchConference" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <!-- FB 2050 -->
<meta http-equiv="X-UA-Compatible" content="IE=7" /> <!-- FB 2050 -->
<!-- Window Dressing -->
<!-- #INCLUDE FILE="inc/maintopNET.aspx" --> 
<script type="text/javascript">
  var servertoday = new Date();
</script>
<script type="text/javascript" src="inc/functions.js"></script><%--Added For 1420--%>
<%--FB 1861--%>
<%--<script type="text/javascript" src="script/cal.js"></script>--%>
<script type="text/javascript" src="script/cal-flat.js"></script>
<script type="text/javascript" src="lang/calendar-en.js"></script>
<script type="text/javascript" src="script/calendar-setup.js"></script>
<script type="text/javascript" src="script/calendar-flat-setup.js"></script>
<link rel="stylesheet" type="text/css" media="all" href="css/calendar-blue.css" /> <%--FB 1861--%>  <%--FB 1982--%>


<script type="text/javascript" src="script/mytreeNET.js"></script>
<script type="text/javascript" src="script/RoomSearch.js"></script>
<script language="javascript">

function SaveSearch(val)
{
    //Added for FB 1420 --  Start
    var confenddate = '';
    confenddate = GetDefaultDate(document.getElementById("txtDateTo").value,'<%=format%>');
    var confstdate = '';
    confstdate = GetDefaultDate(document.getElementById("txtDateFrom").value,'<%=format%>');
    
   
    
    if (Date.parse(confstdate) > Date.parse(confenddate))
    {
            alert("Date To should be greater than Date From");
             return false;
    }
    
    if(document.getElementById("trSaveSearch").style.display != "none" && document.getElementById("txtSearchTemplateName").value == "")
    {
        alert("Please enter search name");
        return false;
    }

    //FB 2632 - Starts
    if (document.getElementById("chkDedicatedVNOCOperator").checked) {
        if (document.getElementById("hdnApprover1").value == "") {
            alert("Please select Dedicated VNOC Operator.");
            return false;
        }
    }
    //FB 2632 - End
    
    //Edited for FB 1420 -- End
    if (val == '1') // from edit
    {
        document.getElementById("trSaveSearch").style.display="";
        document.getElementById("btnSubmit").disabled = true;
        document.getElementById("txtSearchTemplateID").style.display = "none";
	    return true;
    }
    else if (val == "2") //from Search Button
    {
//        if ('<%=Session["EnableEntity"]%>' == '1')//FB 2607
//          {
//              if(document.getElementById("txtSearchTemplateName").value != "") 
//	            alert("Custom option values cannot be saved in a Template.");
//	      }
	    document.getElementById("txtSearchTemplateID").style.display = "none";
	    if (document.getElementById("trSaveSearch").style.display == "")
	    {
		    document.getElementById("trSaveSearch").style.display="none";
		    return true;
            }
	    else
	    {
	        document.getElementById("txtSearchTemplateID").value = "new";
		    document.getElementById("trSaveSearch").style.display="";
		    return false;
	    }
	   
    } 
    else
	return false;
	
	
}

function changeRoomSelection(objValue)
{
    //alert(objValue);
    if(objValue == "2")
    {
        document.getElementById("trRooms").style.display = "";
        //document.getElementById("trRooms1").style.display = "";
    }
    else
    {
        document.getElementById("trRooms").style.display = "none";
       // document.getElementById("trRooms1").style.display = "none";
    }
}

function changeDateSelection(objvalue)
{
    if(objvalue == "5")
    {
        document.getElementById("trDateFromTo").style.display = "";
        Page_ValidationActive=true;    }
    else
        document.getElementById("trDateFromTo").style.display = "none";
}

//Edited for FB 1420 -- Start
function ChangeEndDate(frm)
{
    var confstdate = '';
    confstdate = GetDefaultDate(document.getElementById("txtDateFrom").value,'<%=format%>');
    var confenddate = '';
        confenddate = GetDefaultDate(document.getElementById("txtDateTo").value,'<%=format%>');
       
    if (Date.parse(confenddate) > Date.parse(confstdate) )
        {
            if (frm == "0")
            {
                alert("Date From Should be lesser than Date To");
                return false;
            }
        }
        else
        return true;
}

function ChangeStartDate(frm)
{
        var confenddate = '';
        confenddate = GetDefaultDate(document.getElementById("txtDateTo").value,'<%=format%>');
        var confstdate = '';
        confstdate = GetDefaultDate(document.getElementById("txtDateFrom").value,'<%=format%>');
        
        if (Date.parse(confstdate) > Date.parse(confenddate))
        {
            if (frm == "0") 
            {
                alert("Date To should be greater than Date From");
                 return false;
            }
        }
        else
        return true;
}

//Edited for FB 1420 --  End

//FB 2632 - Starts
function getYourOwnEmailList(i) {
    url = "emaillist2main.aspx?t=e&frm=approverNET&fn=Setup&n=" + i;

    if (!window.winrtc) {
        winrtc = window.open(url, "", "width=950,height=450,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
        winrtc.focus();
    }
    else if (!winrtc.closed) {
        winrtc.close();
        winrtc = window.open(url, "", "width=950,height=450,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
        winrtc.focus();
    }
    else {
        winrtc = window.open(url, "", "width=950ss,height=450,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
        winrtc.focus();
    }
}

function deleteApprover(id) {
    eval("document.getElementById('hdnApprover" + (id) + "')").value = "";
    eval("document.getElementById('txtApprover" + (id) + "')").value = "";
}
//FB 2632 Ends

</script>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Search Conference</title>
</head>
<body>
    <form id="frmSearchConference" runat="server" method="post">
    <input name="selectedloc" type="hidden" id="selectedloc" runat="server"  /> <!--Added room search-->
     <input name="locstrname" type="hidden" id="locstrname" runat="server"  /> <!--Added room search-->
        <center><table border="0" width="98%" cellpadding="2" cellspacing="2">
            <tr>
                <td align="center" style="height: 23px">
                    <h3><%if(Application["Client"].ToString().ToUpper() == "MOJ") {%>Search Hearing<%}else{ %>Search Conferences<%} %></h3><br /> <%--Edited for FB 1428--%>
                     <asp:Label ID="errLabel" runat="server" Visible="False" CssClass="lblError"></asp:Label></td>
            </tr>
            <tr>
                <td align="left">
                    <asp:Label ID="Label1" runat="server" CssClass="subtitleblueblodtext">Saved Searches</asp:Label></td>
            </tr>
            <tr>
                <td align="center">
                <asp:DataGrid ID="dgScheduledSearches" runat="server" AutoGenerateColumns="False" CellPadding="5" BorderStyle="none" CellSpacing="0"
                 Width="50%" OnItemCreated="BindRowsDeleteMessage"
                 OnEditCommand="EditSearchTemplate" OnCancelCommand="SearchConferenceFromTemplate" OnDeleteCommand="DeleteSearchTemplate">
                    <AlternatingItemStyle CssClass="tableBody"/>
                    <HeaderStyle CssClass="tableHeader" Height ="30"/>
                    <SelectedItemStyle BackColor="Orange" />
                    <ItemStyle CssClass="tableBody"/>
                    <Columns>
                        <asp:BoundColumn DataField="ID" Visible="False"></asp:BoundColumn>
                        <%--Window Dressing--%>
                        <asp:BoundColumn DataField="name" HeaderText="Name" ItemStyle-CssClass="tableBody" ItemStyle-Width="60%" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"> <%-- FB 2050 --%>
                            <HeaderStyle CssClass="tableHeader"/>
                        </asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="Actions">
                            <HeaderStyle CssClass="tableHeader" />
                            <ItemTemplate>
                                <asp:LinkButton Text="Search" runat="server" ID="btnSearchNow" CommandName="Cancel"></asp:LinkButton>&nbsp;&nbsp;&nbsp;&nbsp;
                                <asp:LinkButton Text="Edit" OnClientClick="javascript:return SaveSearch('1')" runat="server" ID="btnEdit" CommandName="Edit"></asp:LinkButton>&nbsp;&nbsp;&nbsp;&nbsp;
                                <asp:LinkButton Text="Delete" runat="server" ID="btnDelete" CommandName="Delete"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                    <ItemStyle Height="20px" />
                
                </asp:DataGrid>
                    <asp:Label ID="lblNoSearchTemplates" runat="server"
                        Text="There are no templates available." Visible="False" CssClass="lblError"></asp:Label>&nbsp;
                </td>
            </tr>
            <tr>
                <td align="left">
                        <asp:Label ID="lblSearch1" runat="server" CssClass="subtitleblueblodtext"><%if(Application["Client"].ToString().ToUpper() == "MOJ")%>Quick Search (If you already know your hearing ID.)<%else%>Quick Search (If you already know your conference ID.)</asp:Label> <%--Edited for FB 1428 MOJ--%>
                </td>
            </tr>
            <tr>
                <td>
                    <table width="100%" cellpadding="2" cellspacing="2" border="0"> <%-- FB 2050 --%>
                        <tr>
                            <%--Added for FB 1428 Start--%>
                                <% if (Application["Client"].ToString().ToUpper() == "MOJ")
                               {
                                %>
                                <td width="10%" align="right" valign="top" class="blackblodtext">
                                    Hearing ID
                                </td>
                                <%}%>
                                <% else
                               { %>
                                <td width="10%" align="right" valign="top" class="blackblodtext">
                                    Conference ID
                                </td>
                                <% } %>
                                <%--Added for FB 1428 End--%>
                            <td align="left">
                                <asp:TextBox ID="txtConferenceUniqueID" runat="server" CssClass="altText" Text=""></asp:TextBox>
                                <asp:Button id="btnSubmitID" CssClass="altShortBlueButtonFormat" Text="Submit" OnClick="SubmitSearch" ValidationGroup="ID" runat="server" />
                                <asp:RequiredFieldValidator ID="req1" runat="server" ControlToValidate="txtConferenceUniqueID" ErrorMessage="Required" Display="dynamic" ValidationGroup="ID"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="reg1" runat="server" ControlToValidate="txtConferenceUniqueID" ValidationGroup="ID" ErrorMessage="Numeric only" ValidationExpression="\d+" Display="dynamic"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <asp:Label ID="Label2" runat="server" CssClass="subtitleblueblodtext" Text="Advanced Search (Please enter any and all information you do know.)"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="left"> <%-- FB 2050 --%>
                    <table width="90%" cellpadding="2" cellspacing="2" border="0">
                        <tr>
                            <%--Added for FB 1428 Start--%>
                            <% if (Application["Client"].ToString().ToUpper() == "MOJ")
                               {
                            %>
                            <td align="right" class="blackblodtext" style="width: 10%">
                                Hearing Name
                            </td>
                            <%}%>
                            <% else
                               { %>
                            <td align="right" class="blackblodtext" style="width: 10%">
                                Conference Name
                            </td>
                            <% } %>
                            <%--Added for FB 1428 End--%>
                            <td align="left" style="width:40%">
                                <!--[Vivek: 29th Apr 2008]Changed Regular expression as per issue number 306-->
                                <asp:TextBox ID="txtConferenceName" runat="server" CssClass="altText" Text="" ></asp:TextBox>
                                                             <%-- Code Added for FB 1640--%>                                                
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator20" ControlToValidate="txtConferenceName" Display="dynamic" runat="server" ValidationGroup="DateSubmit" SetFocusOnError="true" ErrorMessage="<br> & < and > are invalid characters." ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator> <%--FB 2321--%>
                            </td>
                            <td width="50%" rowspan="6" valign="top">
                                <table width="100%">
                                    <tr>
                                        <td align="left" valign="top" class="blackblodtext">
                                            Locations
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" CssClass="blackblodtext">
                                            <%--Window Dressing - Start--%>
                                            <asp:RadioButtonList   ID="rdRoomOption" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" >
                                                <asp:ListItem Selected="False"  Text="<span class='blackblodtext'>None</span>" Value="0"></asp:ListItem>
                                                <asp:ListItem Selected="True" Text="<span class='blackblodtext'>Any</span>"  Value="1"></asp:ListItem>
                                                <asp:ListItem Selected="False" Text="<span class='blackblodtext'>Selected</span>" Value="2"></asp:ListItem>
                                            </asp:RadioButtonList>
                                        <%--Window Dressing - End--%>
                                        </td>
                                    </tr>
                                    <tr id="trRooms1" style="display:none">
                                        <td>
                                            <table border="0" style="width: 100%">
                                                <tr>
                                                    <td valign="top" align="left" width="80" id="tdCom" runat="server"> <%--Edited for FB 1415,1416,1417,1418--%>
                                                    <input type="button" value="Compare" id="btnCompare" onclick="javascript:compareselected();" class="altShortBlueButtonFormat" runat="server" />
                                                    </td>
                                                    <td valign="top" align="left">
                                                          <asp:RadioButtonList ID="rdSelView" runat="server" OnSelectedIndexChanged="rdSelView_SelectedIndexChanged"
                                                              RepeatDirection="Horizontal" AutoPostBack="True" RepeatLayout="Flow">
                                                              <asp:ListItem Selected="True" Value="1"><span class='blackblodtext'>Level View</span></asp:ListItem>
                                                              <asp:ListItem Value="2"><span class='blackblodtext'>List View</span></asp:ListItem>
                                                          </asp:RadioButtonList>
                                                    </td>
                                                </tr>
                                            </table>                     
                                        </td>
                                    </tr>
                                    <tr id="trRooms" style="display:none">
                                    
                                    <td  valign="top" align="left">
                <table>
                    <tr>
                        <td align="right" valign="top" style="width:10%">
                        <input name="opnRooms" type="button" id="opnRooms" onclick="javascript:OpenRoomSearch('frmSearchConference');" value="Add Room" class="altShortBlueButtonFormat" />
                    
                    <input name="addRooms" type="button" id="addRooms" onclick="javascript:AddRooms();" style="display:none;" /><br />
                    <span class="blackblodtext"> <font size="1">Double-click on the room to remove from list.</font></span>
                        </td>
                        <td align="right" style="width:90%">
                        <select size="4" wrap="false" name="RoomList" id="RoomList" class="treeSelectedNode" onDblClick="javascript:Delroms(this.value)"  style="height:350px;width:100%;" runat="server"></select>
                        <iframe style="display:none;" name="ifrmLocation" src=""   width="100%" height="300" align="left" valign="top">
                    <p>go to <a id="aLocation" href="" name="aLocation">Location List</a></p>
                  </iframe> 
                        </td>
                    </tr>
                </table>
                  
                </td>
                                        <td style="display:none;"><%--Edited for FB 1415,1416,1417,1418,Panel is Edited && Room Search--%>
                                            <asp:Panel ID="pnlLevelView" runat="server" Height="300px" Width="100%" ScrollBars="Auto" BorderColor="Blue" BorderStyle="Solid" BorderWidth="1px" HorizontalAlign="Left">
                                                <asp:TreeView ID="treeRoomSelection" runat="server" BorderColor="White" Height="90%" ShowCheckBoxes="All" onclick="javascript:getRooms(event)"
                                                    ShowLines="True" Width="95%"> 
                                                    <NodeStyle CssClass="treeNode"/>
                                                    <RootNodeStyle CssClass="treeRootNode"/>                                                    
                                                    <ParentNodeStyle CssClass="treeParentNode"/>
                                                    <LeafNodeStyle CssClass="treeLeafNode"/>
                                                </asp:TreeView>
                                            </asp:Panel>
                                            <asp:Panel ID="pnlListView" runat="server" BorderColor="Blue" BorderStyle="Solid"
                                                BorderWidth="1px" Height="300px" ScrollBars="Auto" Visible="False" Width="100%" HorizontalAlign="Left" Direction="LeftToRight" Font-Bold="True" Font-Names="Verdana" Font-Size="Small" ForeColor="Green">
                                                <%--Added for FB 1415,1416,1417,1418 - Start--%>
                                                <input type="checkbox" id="selectAllCheckBox" runat="server" onclick="CheckBoxListSelect('lstRoomSelection',this);" /><font size="2"> Select All</font>
                                                <br />
                                                <asp:CheckBoxList ID="lstRoomSelection" runat="server" Height="95%" Width="95%" Font-Size="Smaller" ForeColor="ForestGreen" Font-Names="Verdana" RepeatLayout="Flow"  onclick="javascript:getValues(event)">
                                                </asp:CheckBoxList>
                                                <%--Added for FB 1415,1416,1417,1418  - End--%>
                                            </asp:Panel>
                                             <%--Added for FB 1415,1416,1417,1418  - Start--%>
                                            <asp:Panel ID="pnlNoData" runat="server" BorderColor="Blue" BorderStyle="Solid"
                                                BorderWidth="1px" Height="300px" ScrollBars="Auto" Visible="False" Width="100%" HorizontalAlign="Left" Direction="LeftToRight" Font-Size="Small">
                                                <table><tr align="center"><td>
                                                You have no Room(s) available
                                                </td></tr></table>
                                                
                                            </asp:Panel>
                                            <%--Added for FB 1415,1416,1417,1418  - End--%>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <%--Added for FB 1428 Start--%>
                            <% if (Application["Client"].ToString().ToUpper() == "MOJ")
                               {
                            %>
                            <td align="right" valign="top" class="blackblodtext">
                                Hearing Date
                            </td>
                            <%}%>
                            <% else
                               { %>
                            <td align="right" valign="top" class="blackblodtext">
                                Conference Date
                            </td>
                            <% } %>
                            <%--Added for FB 1428 End--%>
                            <td align="left">
                                <table cellpadding="2" cellspacing="2" width="100%">
                                    <tr>
                                        <td>
                                             <%--Window Dressing - Start--%>
                                            <asp:RadioButtonList ID="rdDateOption" runat="server" RepeatDirection="Horizontal" RepeatLayout="Table" RepeatColumns="3" >
                                                <asp:ListItem Selected="False"  Text="<span class='blackblodtext'>Yesterday</span>" Value="6"></asp:ListItem>
                                                <asp:ListItem Selected="False" Text="<span class='blackblodtext'>Today</span>"  Value="2"></asp:ListItem>
                                                <asp:ListItem Selected="False" Text="<span class='blackblodtext'>Tomorrow</span>"  Value="7"></asp:ListItem>
                                                <asp:ListItem Selected="True" Text="<span class='blackblodtext'>This Week</span>" Value="3"></asp:ListItem>
                                                <asp:ListItem Selected="False" Text="<span class='blackblodtext'>This Month</span>"  Value="4"></asp:ListItem>
                                                <%--<asp:ListItem Selected="False" Text="Past" Value="0"></asp:ListItem> FB Case 652 Saima --%>
                                                <asp:ListItem Selected="False" Text="<span class='blackblodtext'>Ongoing</span>" Value="1"></asp:ListItem>
                                                <asp:ListItem Selected="False" Text="<span class='blackblodtext'>Custom</span>" Value="5"></asp:ListItem>
                                            </asp:RadioButtonList>
                                        <%--Window Dressing - End--%>
                                        </td>
                                    </tr>
                                    <tr id="trDateFromTo" style="display:none" >
                                        <td>
                                            <table>
                                                <tr>
                                                    <td align="right" class="blackblodtext">Date From:
                                                        <asp:TextBox ID="txtDateFrom" runat=server Text="" CssClass="altText" onblur="javascript:ChangeEndDate(0)"></asp:TextBox> <%--Edited for FB 1420--%>
                                                        <%--//Code changed by Offshore for FB Issue 1073,1420 -- Start
                                                 <img src="image/calendar.gif" border="0" width="20" height="20" id="cal_triggerFrom" style="cursor: pointer;" title="Date selector" onclick="return showCalendar('txtDateFrom', 'cal_triggerFrom', 0, '%m/%d/%Y');" /> --%>
                                                 <img src="image/calendar.gif" border="0" width="20" height="20" id="cal_triggerFrom" style="cursor: pointer;" title="Date selector" onblur="javascript:ChangeEndDate(0)" onclick="return showCalendar('txtDateFrom', 'cal_triggerFrom', 0, '<%=format%>');" />
                                                 <!--//Code changed by Offshore for FB Issue 1073,1420 -- End-->
						                                <asp:RequiredFieldValidator ID="reqFrom" Enabled="false" ControlToValidate="txtDateFrom" Display="dynamic" ErrorMessage="Required" ValidationGroup="DateSubmit" runat="server"></asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" class="blackblodtext">Date To:
                                                        <asp:TextBox ID="txtDateTo" runat=server Text="" CssClass="altText" onblur="javascript:ChangeStartDate(0)"></asp:TextBox> <%--Edited for FB 1420--%>
                                                        <%--//Code changed by Offshore for FB Issue 1073,1420 -- Start
                                                        <img src="image/calendar.gif" border="0" width="20" height="20" id="cal_triggerTo" style="cursor: pointer;" title="Date selector" onclick="return showCalendar('txtDateTo', 'cal_triggerTo', 0, '%m/%d/%Y');" /> --%>
                                                        <img src="image/calendar.gif" border="0" width="20" height="20" id="cal_triggerTo" style="cursor: pointer;" title="Date selector" onblur="javascript:ChangeStartDate(0)" onclick="return showCalendar('txtDateTo', 'cal_triggerTo', 0, '<%=format%>');" />
                                                        <!--//Code changed by Offshore for FB Issue 1073,1420 -- End-->
						                                <asp:RequiredFieldValidator ID="reqTo" Enabled="false" ControlToValidate="txtDateTo" Display="dynamic" ErrorMessage="Required" ValidationGroup="DateSubmit" runat="server"></asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="blackblodtext">
                            <%if ((Application["Client"].ToString().ToUpper() == "MOJ")){%> <%--Added For FB 1428--%>
                                Created By
                            <%}else{ %>
                                Host
                              <%} %>
                            </td>
                            <td align="left">
                                <!--[Vivek: 29th Apr 2008]Changed Regular expression added ValidationGroup tag 306-->
                                <asp:TextBox ID="txtHost" runat="server" CssClass="altText" Text="" ></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" ControlToValidate="txtHost" Display="dynamic" runat="server" ValidationGroup="DateSubmit" SetFocusOnError="true" ErrorMessage="<br>& < > + % \ / ( ) ? | ^ = ! ` [ ] { } # $ @ and ~ are invalid characters." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>^+?|!`\[\]{}\=@#$%&()~]*$"></asp:RegularExpressionValidator> <%--FB 1888--%>
                            </td>
                        </tr>
                        <%if (!(Application["Client"].ToString().ToUpper() == "MOJ")){%> <%--Added For FB 1425--%>
                        <tr>
                            <td align="right" class="blackblodtext">
                                Participant
                            </td>
                            <td align="left">
                                <!--[Vivek: 29th Apr 2008]Changed Regular expression added ValidationGroup tag 306-->
                                <asp:TextBox ID="txtParticipant" runat="server" CssClass="altText" Text="" ></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txtParticipant" Display="dynamic" runat="server" ValidationGroup="DateSubmit" SetFocusOnError="true" ErrorMessage="<br>& < > + % \ / ( ) ? | ^ = ! ` [ ] { } # $ @ and ~ are invalid characters." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>^+?|!`\[\]{}\=@#$%&()~]*$"></asp:RegularExpressionValidator><%--FB 1888--%> 
                            </td>
                        </tr>
                        <%}%><%--Added For FB 1425--%>
                        <tr>
                            <td align="right" class="blackblodtext">
                                Status
                            </td>
                            <td align="left">
                             <%--Window Dressing - Start--%>                            
                                 <asp:RadioButtonList ID="rdStatus" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" >
                                    <asp:ListItem Selected="True" Text="<span class='blackblodtext'>Any</span>" Value="0"></asp:ListItem>
                                    <asp:ListItem Selected="False" Text="<span class='blackblodtext'>Pending</span>" Value="1"></asp:ListItem>
                                    <asp:ListItem Selected="false" Text="<span class='blackblodtext'>Non-pending</span>" Value="2"></asp:ListItem>
                                </asp:RadioButtonList>
                             <%--Window Dressing - End--%>
                           </td>
                        </tr>
                        <tr>
                            <td align="right" class="blackblodtext" style="height: 40px"><%--Edited For FB 1421--%>
                                Access
                            </td>
                            <td align="left">
                                <asp:RadioButtonList ID="rdPublic" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" >
                                    <asp:ListItem Selected="True" Text="<span class='blackblodtext'>Any</span>" Value="0"></asp:ListItem>
                                    <asp:ListItem Selected="False" Text="<span class='blackblodtext'>Public</span>"  Value="1"></asp:ListItem>
                                    <asp:ListItem Selected="False" Text="<span class='blackblodtext'>Private</span>"  Value="2"></asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <%--FB 2632 Starts--%>
                        <tr id="trConcierge" runat="server" >
                            <td align="left" colspan="2">
                            <asp:Label ID="Label4" runat="server" CssClass="subtitleblueblodtext">Concierge Support Search</asp:Label>
                                <table id="tblConciergeNew" cellspacing="2" cellpadding="3" border="0" style="width:80%;">
                                    <tr id="trOnSiteAVSupport" runat="server">
                                        <td align="left" valign="middle" nowrap="nowrap" style="width: 2%;">
                                            <input id="chkOnSiteAVSupport" type="checkbox" runat="server" />
                                           <span class='blackblodtext'>On-Site A/V Support</span>
                                        </td>
                                        <td align="left" valign="middle" nowrap="nowrap" style="width: 2%;">
                                            <input id="chkConciergeMonitoring" type="checkbox" runat="server"  />
                                            <span class='blackblodtext'>Concierge Monitoring</span>
                                        </td>
                                    </tr>
                                    <tr id="trMeetandGreet" runat="server">
                                        <td align="left" valign="middle" nowrap="nowrap" style="width: 2%;">
                                            <input id="chkMeetandGreet" type="checkbox" runat="server"  />
                                            <span class='blackblodtext'>Meet and Greet</span>
                                        </td>
                                        <td align="left" nowrap="nowrap" valign="middle" style="width: 2%;">
                                        <input id="chkDedicatedVNOCOperator" type="checkbox" runat="server"  />
                                        <span class='blackblodtext'>Dedicated VNOC Operator</span>
                                        </td>
                                    </tr>
                                    <tr>
                                    <td align="left" valign="middle" nowrap="nowrap" style="width: 2%;"></td>
                                    <td align="left" nowrap="nowrap" valign="middle" style="width: 2%;">
                                        <asp:TextBox ID="txtApprover1" runat="server" CssClass="altText"></asp:TextBox>
                                         &nbsp;<img id="imgVNOC" onclick="javascript:getYourOwnEmailList(0)" src="image/edit.gif" alt="" />
                                         <a href="javascript: deleteApprover(1);" onmouseover="window.status='';return true;">
                                         <img border="0" id="imgdeleteVNOC" src="image/btn_delete.gif" alt="delete" width="16" height="16" runat="server" /></a>
                                        <asp:TextBox ID="hdnApprover1" runat="server" BackColor="Transparent" BorderColor="White"
                                            BorderStyle="None" Width="0px" ForeColor="Black" style="display:none"></asp:TextBox>
                                    </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <%--FB 2632 Starts--%>
                         <tr>
                         <%--Custom Attributes--%>
                            <td align="left" colspan="2">
                           <asp:Label ID="Label3" runat="server" CssClass="subtitleblueblodtext"><%if(Session["EnableEntity"].ToString().ToUpper() == "1")%>Custom Options Search</asp:Label> <%--Edited for FB 1428 MOJ Changes--%>
                                <asp:Table runat="server" ID="tblCustomAttribute" Width="80%" CellPadding="3" cellspacing="2" Visible="true">
                                </asp:Table>
                            </td>
                        </tr>
                        <%--FB 2377 FB 2632--%>
                    </table>
                </td>
            </tr>
            <tr runat="server" id="trSaveSearch" style="display:none">
                <td align="left">
                    <asp:Label ID="lblSearch" runat="server" CssClass="subtitleblueblodtext" Text="Save search as"></asp:Label>
                    <asp:TextBox ID="txtSearchTemplateName" ValidationGroup="TemplateSubmit" runat="server" MaxLength="26" CssClass="altText" Text=""></asp:TextBox><%--FB 1953--%>
                    <asp:TextBox ID="txtSearchTemplateID" runat="server" CssClass="altText" Text=""></asp:TextBox>
                    <asp:RequiredFieldValidator ID="reqName" runat="server" ErrorMessage="Required" Display="dynamic" ControlToValidate="txtSearchTemplateName" ValidationGroup="TemplateSubmit"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" ControlToValidate="txtSearchTemplateName" Display="dynamic" runat="server" ValidationGroup="TemplateSubmit" SetFocusOnError="true"
                        ErrorMessage="<br>+'&<>%;:( ) / \ ^#$@ and double quotes are invalid characters for this field." ValidationExpression="[A-Za-z0-9._~?!`* \-]+"></asp:RegularExpressionValidator> <%--fogbugz case 280--%>
                </td>
            </tr>
            <tr>
                <td>
                <%--Window Dressing--%>
                    <table cellpadding="2" cellspacing="2" border="0" align="center">
                        <tr>
                            <td>
                                <asp:Button id="btnReset" CssClass="altShortBlueButtonFormat" Text="Reset" OnClick="Reset" runat="server" />
                            </td>
                            <td>
                                <asp:Button id="btnSaveSearch" CssClass="altShortBlueButtonFormat" OnClick="SaveSearch" Text="Save Search" OnClientClick="javascript:return SaveSearch('2');" runat="server" ValidationGroup="TemplateSubmit" />
                            </td>
                            <td>
                                <asp:Button id="btnSubmit" CssClass="altShortBlueButtonFormat" Text="Submit" OnClick="SubmitSearch" runat="server" OnClientClick="javascript:return SubSearch();" ValidationGroup="DateSubmit" /><%--Edited for FB 1420--%>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
</center>
                <input type="hidden" id="helpPage" value="81">

    </form>
    <script language="javascript">
    //alert(document.getElementById("rdRoomOption_2").checked);
    if (document.getElementById("rdRoomOption_2").checked)
        changeRoomSelection("2");
    else
        changeRoomSelection("0");

//if (document.getElementById("trSaveSearch").style.display == "")
//	SaveSearch('0');
//Added FB 1420 -- Start
function SubSearch() 
{
    //FB 2632 - Starts
    if (document.getElementById("chkDedicatedVNOCOperator").checked) {
        if (document.getElementById("hdnApprover1").value == "") {
            alert("Please select Dedicated VNOC Operator.");
            return false;
        }
    }
    //FB 2632 - End
    if(document.getElementById("trSaveSearch").style.display != "none")
    {
        if(document.getElementById("txtSearchTemplateName").value == "")
        {
        alert("Please enter search name");
        return false;
        }
        return true;
    }
    if(document.getElementById("rdRoomOption_2").value == "2")
    {
        if(document.getElementById("trRooms1").style.display != "none")
        {
            if(document.getElementById("rdSelView").disabled == false)
            {
                if(document.getElementById("selectedloc").value == "")
                {
                    alert("Please Select the Room(s)");
                    return false;
                }
            }
         }
    }
    if(document.getElementById("trDateFromTo").style.display != "none")
    {
        if(document.getElementById("txtDateFrom").value == "" || document.getElementById("txtDateTo").value == "")
        {   
            alert("Please Select the Custom Date From and Date To");
            return false;
        }
        return true;
    }
    
        
}
//Added for FB 1420 -- End
    
</script>
</body>
</html>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
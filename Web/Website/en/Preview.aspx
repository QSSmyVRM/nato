<%@ Page language="c#" AutoEventWireup="false" Inherits="myVRMAdmin.Web.en.Preview" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Preview</title>
		<meta name="Description" content="VRM (Videoconferencing Resource Management) is a revolutionary Web-based software application that manages any video conferencing environment.">
		<meta name="Keywords" content="VRM, Videoconferencing Resource Management, video conferencing, video bridges, video endpoints">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link title="Expedite base styles" href="Original/Styles/border-table.css" type="text/css" rel="stylesheet">
		<link title="Expedite base styles" href="Original/Styles/main-table.css" type="text/css" rel="stylesheet">
		
    <%--code added for Soft Edge button--%>		
		<link title="Expedite base styles" href="<%=Session["OrgCSSPath"]%>" type="text/css" rel="stylesheet">
	</HEAD>
	<body leftmargin="0" rightmargin="0" bottommargin="0" topmargin="0" scroll="no">
		<form id="Form1" method="post" runat="server">
			<table width="100%">
				<tr>
					<td>
						<font face="Verdana" size="2pt" color="blue">
						 &nbsp;&nbsp;Below are the sample pages.<br>&nbsp;&nbsp;Operations cannot be performed.
						</font>
					</td>
				</tr
				<tr height="540">
					<td width="100%">
						<iframe src="PreviewLogin.aspx" id="previewframe" scrolling="yes" width="100%" height="500" style="TEXT-DECORATION: none">
						</iframe>
					</td>
				</tr>
				<tr>
					<td>
						
						<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0" ID="Table8">
							<TBODY>
								<tr>
									<td align="center">
										<input type="button" class="altBlueButtonFormat" name="btnLogin" value="Login Preview" onclick="fnOpen('PreviewLogin.aspx');">
										<input type="button" class="altBlueButtonFormat" name="btnLobby" value="Lobby Preview" onclick="fnOpen('settingselect2Preview.aspx');">
										<input type="button" class="altBlueButtonFormat" name="btnvrm" value="Advanced Settings Preview" onclick="fnOpen('AdvSettings.aspx');">
										<input type="button" class="altBlueButtonFormat" name="Close" value="Close" onclick="FnClose()">
									</td>
								</tr>
							</TBODY>
						</TABLE>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>

<script language="javascript">

	function FnClose()
	{
		window.self.close();
	}

	function fnOpen()
	{
		args = fnOpen.arguments;
		var previewframe = document.getElementById("previewframe");
		previewframe.src = args[0];
	}
</script>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
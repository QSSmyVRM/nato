<%@ Page Language="C#" AutoEventWireup="true" Inherits="ns_Login.genlogin" %>
<html>
<head>
<link rel="stylesheet" title="Expedite base styles" type="text/css" href="en/App_Themes/CSS/main.css" /> 



<script language="javascript" type="text/javascript">
    //FB-1669 Fix
//    <!--
    if (document.images)
    {
      preload_image = new Image(25,25); 
      preload_image.src="image/wait1.gif"; 
    }
    //-->
    //Fb-1669 Fix End here
    </script>
</head>
<!--  #INCLUDE FILE="inc/maintop5.aspx"  --> 
<script language="JavaScript" src="inc/functions.js" type="text/javascript"></script>
<body class="Bodybackgorund">
<%--Added for FB 1648--%>
<% 
HttpContext.Current.Application.Lock();
//Version and Year upgradation start 
HttpContext.Current.Application.Remove("CopyrightsDur");
HttpContext.Current.Application.Remove("Version");
HttpContext.Current.Application.Add("Version", "2.8");//Can Edit for version upgradation
HttpContext.Current.Application.Add("CopyrightsDur", "2002-2012");//FB 1814//Can Edit for year upgradation    
//Version and Year upgradation end
HttpContext.Current.Application.UnLock();

%>
<% if (wizard_enable)
   { %>

	<script language="JavaScript" type="text/javascript">
	<!--

	var timerRunning = false;
	var toTimer = null;
	
	function startTimer()
	{
		toTimer = setTimeout('startTimer()',1000);
		checkAndRefresh(0);
		timerRunning = true;
	}
	
	function stopTimer() {
		checkAndRefresh(1);
		if (timerRunning) {
			clearTimeout(toTimer);
		}
	}
	
	function NewbuttonLogin()
	{
	    if(document.getElementById('btnSubmit') != null)
        {            
            document.getElementById('btnSubmit').click();
        }
	}
		
	function validateLogin()
	{
	
	    
	    
	    DataLoading("1");
	    var strUsr;
	    var stralert;
		
		var obj1  ;//= document.frmGenlogin.getElementById("errLabel");
	    //if(obj1)
	    //  obj1.innerHTML = "";
		
		if (document.frmGenlogin.UserName.value == ""){
		    
			if(obj1)
			{
			    
			    obj1.innerHTML = "Please enter Email"
			    DataLoading("0");
			    return false; // FB 2310
            }
			//return false; 
		}
		else
		{
		
		
		    strUsr= Trim(document.frmGenlogin.UserName.value);
		    
		    
		    
		    if(strUsr.indexOf('@')== -1)
		    {
		    
		        //FB 1943
		   
		    
		       if("<%=mailextn %>" != "")
		       {
		            strUsr = strUsr + "@" + "<%=mailextn %>";
		            document.frmGenlogin.UserName.value = strUsr;
		       }
		       else
		       {
		    
		            if(obj1)
			        {
    			        
		                obj1.innerHTML = "Invalid Email";
		                DataLoading("0");
		            }
		            return false;
		        }
		    }
		    
		    
		        //FB 1943
			if(checkInvalidChar(strUsr) == false)
			{

			    if(obj1)
			    {
			        
		            obj1.innerHTML = "Invalid Email";
		            DataLoading("0");
		        }
				return false;
			}
		}
		var str;
		str= Trim(document.frmGenlogin.UserPassword.value);
		
		
		
		if (str == "")
		{
		    if(obj1)
		    {
		      obj1.innerHTML = "Please enter the password";
		      DataLoading("0");
		      return false; // FB 2310
	        }	
		    //return false;
		}
		else{
			if(checkInvalidPassChar(str) == false){//FB 2339
			    DataLoading("0");
				return false;
			}
		}
		//checkAndRefresh(1);
		//DataLoading("0");
		return true;
	}
	
	function DataLoading(val)
    {
    
        if (val=="1")
            document.getElementById("dataLoadingDIV").innerHTML='<b><img border="0" src="image/wait1.gif" width="100" height="22">';  //Edited for FF FB-1669
        else
            document.getElementById("dataLoadingDIV").innerHTML="";                   
    }


	//-->
	</script>
		
<% }%>

	

<% if (Application["global"] != null) if (Application["global"].ToString() == "enable")
       {%>
	<script language="JavaScript" type="text/javascript">
	<!--

	function international (cb)
	{
		if ( cb.options[cb.selectedIndex].value != "" ) {
			str="" + window.location; 
			newstr=str.replace(/\/en\//i, "/" + cb.options[cb.selectedIndex].value + "/"); 
			if ((ind1 = newstr.indexOf("sct=")) != -1) {
				if ( (ind2=newstr.indexOf("&", ind1)) != -1)
					newstr = newstr.substring(0, ind1-1) + newstr.substring(ind2, newstr.length-1);
				else
					newstr = newstr.substring(0, ind1-1);
			}
			newstr += ((newstr.indexOf("?") == -1) ? "?" : "&") + "sct=" + cb.selectedIndex; 
			document.location = newstr; 
		} 
	}
	
	//-->
	</script>
<% } %>


<table width="100%" border="0" cellpadding="0" cellspacing="0">
  </table>
  
  <br/>
  
   
  <center>
<table  width="100%" border="0" cellpadding="3" cellspacing="0">
<tr>
<td>&nbsp;</td>
</tr>

<tr>
<td>&nbsp;</td>
</tr>

<tr>
<td>&nbsp;</td>
</tr>
  
  <tr>
    

    <td width="100%" valign="top" align="center">
      <table width="100%" cellpadding="0" border="0" align="center">
        <tr>
          <td width="100%" valign="top" align="center">
			  

    
<form id="frmGenlogin" method="post" runat="server" defaultbutton="btnSubmit" onsubmit="return validateLogin();">
    <div >
      <input type="hidden" name="cmd" value="GetHome"/>
      <input type="hidden" name="start" value="0"/>
      <input type="hidden" name="init" value="1"/>
      
      <center >
      
    <table border="0" cellpadding="0" cellspacing="0" style="background-color:White;width:386;height:450;border-collapse:collapse;">
        <tr valign="middle">
      <td width="100%" height="150" align="center"  colspan="2">
           <img id="siteLogoId" src="../image/company-logo/SiteLogo.jpg" onload="" alt="" /> <%-- FB 2050 --%> <%--width="234" height="84" FB 2407--%>
          </td>
        </tr>
        <tr valign="bottom" align="center">
            <td align="center"  colspan="2">
                <h3>
                    <span id="CompanyTagLine" runat="server" class="lblTagline">
                        Saving the planet...one meeting at a time</span><sup class="lblTagline">&#8482;</sup>
                </h3>
            </td>
        </tr>
        <tr valign="top"><td width="100%" align="center"  colspan="2"> 
                    <div id="dataLoadingDIV" name="dataLoadingDIV" align="center" >                
                          <asp:Label id="errLabel" runat="server" CssClass="lblError" ></asp:Label></div>
                          </td></tr>
                           <tr>
                <td style="width:141; vertical-align:middle" align="center">
                   
                  <label for="UserName" class="blackblodtext">Username</label>
                </td>
                <td style="width:245"> 
                
                  <input type="text" name="UserName" runat="server" value="" id="UserName" maxlength="256"  onkeyup="javascript:chkLimit(this,'u');"  class="altText" style="width:188;height:27;" />
                </td>
                
              </tr>
              <tr valign="top">
                <td style="width:141; vertical-align:middle" align="center"> 
                    
                  <label for="UserPassword" class="blackblodtext">Password</label>
                </td>
                <td style="width:245"> 
                  <input type="password" name="UserPassword" runat="server" id="UserPassword"  value="" maxlength="256" onkeyup="javascript:chkloginLimit(this,'5');" class="altText" style="width:188;height:27;"/><%--FB 2339--%>
                </td>
                
              </tr>
              

              <tr valign="top" style="height:10px">
              <td style="width:141" align="center">                    
                  &nbsp;
                </td>
                <td style="width:245">                  
                 <asp:CheckBox name="RememberMe" ID="RememberMe" value="1" runat="server"/> 
                  <label for="RememberMe"><font style="font-weight: normal;" class="blackblodtext">Remember me</font></label>
                </td>
              </tr>
              <tr valign="top">             
               <td style="width:141" align="center">                    
                  &nbsp;
                </td>
                <td style="width:245">&nbsp;<a href="emaillogin.aspx" class="Passwordlink">Forgotten Password?</a></td>
                </tr>
                 <% if (Application["GetActLNK"] != null) if (Application["GetActLNK"].ToString() == "enable")
                { 
            %>
                 <tr valign="top">             
               <td style="width:141" align="center">                    
                  &nbsp;
                </td>
                <td style="width:245">&nbsp;<a href="requestaccount.aspx">Need a myVRM user's account?</a></td>
                </tr><% 
                }   
            %>

            <% if (Application["ViwPubLNK"] != null) if (Application["ViwPubLNK"].ToString() == "enable")
                {   
            %>
                 <tr valign="top">             
               <td style="width:141" align="center">                    
                  &nbsp;
                </td>
                <td style="width:245">&nbsp;<a href="ConferenceList.aspx?t=4&hf=1" onclick="javascript:DataLoading('1')">View Public Conferences?</a></td>
                </tr>
                <% 
                }   
            %>
              <tr valign="top">                
                 
                <td colspan="2" align="center">&nbsp;</td>
                </tr>

              <tr valign="top"> 
                <td align="center" colspan="2">
                                                
                  <asp:Button  id="btnSubmit" runat="server" Text="Log In"  CssClass="shiny-blue" style="width:105;display:none;" OnClientClick="return validateLogin();" OnClick="btnSubmit_Click"/>
                  <button id="btnCSS"  runat="server" value="Log In" class="shiny-blue" style="width:105;height:37" onclick="javascript:NewbuttonLogin();" >Log In</button>
                </td>
              </tr>
              <tr valign="top">                
                 
                <td colspan="2" align="center">&nbsp;</td>
                </tr>

            
       
    </table>
    </center>
    </div>
    </form>
 <script language="JavaScript" type="text/javascript">
<!--

	document.frmGenlogin.UserName.focus();
//	startTimer();

//-->
</script>
<!-------------------------------------------------------------------->


          </td>
        </tr>
      </table>
  
    </td>
   
  </tr>
</table>
</center>




<script language="javascript" type="text/javascript">
if(document.layers) document.captureEvents(Event.KEYPRESS)
document.onkeypress=kpress; 
function kpress(e){
key=(document.layers)?e.which:window.event.keyCode

    if (key==13)
    { 
    
        if(document.getElementById('btnSubmit') != null)
        {            
            //document.getElementById('btnSubmit').click();
        }
            
    }
}
</script>
<br/>
<br/>
<script language="JavaScript" type="text/javascript"> //mainbottom1.asp Code Lines
	var mt = "";
	var _d = document;

	mt += "<center><table border='0' cellpadding='2' cellspacing='2'>";
	mt += "<tr valign='bottom'>";
	mt += "<td colspan=5 align=center>";
    //FB 1985 - Starts
	if('<%=Application["Client"]%>'.toUpperCase() == "DISNEY")
	{
	    mt += "<span class=srcstext2>myVRM Version <%=Application["Version"].ToString()%>(c), Copyright <%=Application["CopyrightsDur"].ToString()%> myVRM.com.  All Rights Reserved.</span>";//FB 1648
	}
	else
	{
	    mt += "<span class=srcstext2>myVRM Version <%=Application["Version"].ToString()%>(c), Copyright <%=Application["CopyrightsDur"].ToString()%> <a href='http://www.myvrm.com' target='_blank'>myVRM.com</a>.  All Rights Reserved.</span>";//FB 1648
	}
    //FB 1985 - Starts
	mt += "</td>";
	mt += "</tr>";
	mt += "</table></center>";


	_d.write(mt)

</script>


<!--code added for CSS Module-->
<script language="javascript" type="text/javascript">
//    function adjustWidth(obj)
//    {
//        alert('hi');
//        if(obj.style.width == "")
//        if (obj.src.indexOf("lobbytop1024.jpg") >= 0)
//        {
//            obj.style.width=window.screen.width-25;            
            
            var obj = document.getElementById('mainTop');
            if(obj != null)
            {
            //FB 1830
//                if (window.screen.width <= 1024)
//                    obj.src = "../en/image/company-logo/StdBanner.jpg"; // Organization Css Module 
//                else
//                    obj.src = "../en/image/company-logo/HighBanner.jpg";  //Organization Css Module 
                    
                if (window.screen.width <= 1024)
                    obj.src = "../image/company-logo/StdBanner.jpg"; // Organization Css Module  //FB 1830
                else
                    obj.src = "../image/company-logo/HighBanner.jpg";  //Organization Css Module //FB 1830
            }
//        }
//    }

   
 
</script>


<!-- PB gradiend -->

<!-- FB 2050 Start -->
<script type="text/javascript">
function refreshImage()
{
  var obj = document.getElementById("siteLogoId");
  if(obj != null)
  {
      var src = obj.src;
      var pos = src.indexOf('?');
      if (pos >= 0) {
         src = src.substr(0, pos);
      }
      var date = new Date();
      obj.src = src + '?v=' + date.getTime();
  }
  return false;
}
window.onload = refreshImage;

//FB 2487 - Start
    var obj = document.getElementById("errLabel");
    if (obj != null) {
        var strInput = obj.innerHTML.toUpperCase();

        if ((strInput.indexOf("SUCCESS") > -1) && !(strInput.indexOf("UNSUCCESS") > -1) && !(strInput.indexOf("ERROR") > -1)) {
            obj.setAttribute("class", "lblMessage");
            obj.setAttribute("className", "lblMessage");
        }        
        else {
            obj.setAttribute("class", "lblError");
            obj.setAttribute("className", "lblError");
        }         
    }
//FB 2487 - End

</script>
<!-- FB 2050 End -->

</body>
</html>

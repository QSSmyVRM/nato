using System;
using System.Data;
using System.Xml;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections;

/// <summary>
/// Summary description for Tier1
/// </summary>
/// 
namespace ns_DataImport
{
    public class Department
    {
        int externalDatabaseType;
        DataTable masterDT;
        string configPath;
        ns_Logger.Logger log;

        public Department(int external, DataTable masterDataTable, string config)
        {
            externalDatabaseType = external;
            masterDT = masterDataTable;
            configPath = config;
            log = new ns_Logger.Logger();
        }

        public bool Process(ref int cnt)
        {
            // split the array list and extract the tiers
            myVRMNet.NETFunctions obj = new myVRMNet.NETFunctions();

            foreach (DataRow dr in masterDT.Rows)
            {
                if (dr["DepartmentName"].ToString().IndexOf("(none)") < 0)
                {
                    log.Trace("Department ID/Name: " + dr["DepartmentID"] + "/" + dr["DepartmentName"]);
                    String deptInXML = Create_DeptInXML(dr["DepartmentID"].ToString(), dr["DepartmentName"].ToString());
                    log.Trace("<br>" + deptInXML);
                    String outXML = obj.CallCOM("UpdateManageDepartment", deptInXML, configPath);
                    if (outXML.IndexOf("<error>") >= 0)
                    {
                        //write to a log file                 
                        log.Trace(obj.ShowErrorMessage(outXML));
                    }
                    else
                    {
                        log.Trace("Success");
                        cnt++;
                    }
                }
            }

            // put that in a messenger object 
            // call the com cmd 
            // get the tiers 
            // save the info in csv file
            return true;
        }
        private String Create_DeptInXML(String deptID, String deptName)
        {
            try
            {
                String inXML = ""; // <login><userID>11</userID><edit/><delete><departmentID>1</departmentID></delete></login>
                inXML += "<login>";
                inXML += "  <userID>11</userID>";
                inXML += "  <delete></delete>";
                inXML += "  <edit>";
                inXML += "      <departmentID>new</departmentID>";
                inXML += "      <multiDepartment>1</multiDepartment>";
                inXML += "      <departmentName>" + deptName + "</departmentName>";
                inXML += "      <approvers>";
                inXML += "          <approver>";
                inXML += "              <ID></ID>";
                inXML += "              <firstName></firstName>";
                inXML += "              <lastName></lastName>";
                inXML += "          </approver>";
                inXML += "          <approver>";
                inXML += "              <ID></ID>";
                inXML += "              <firstName></firstName>";
                inXML += "              <lastName></lastName>";
                inXML += "          </approver>";
                inXML += "          <approver>";
                inXML += "              <ID></ID>";
                inXML += "              <firstName></firstName>";
                inXML += "              <lastName></lastName>";
                inXML += "          </approver>";
                inXML += "      </approvers>";
                inXML += "  </edit>";
                inXML += "</login>"; 
                return inXML;
            }
            catch (Exception ex)
            {
                return "";
            }
        }
    }
}
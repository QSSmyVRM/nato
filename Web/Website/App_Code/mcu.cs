﻿using System;
using System.Data;
using System.Xml;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections;

/// <summary>
/// Summary description for Tier1
/// </summary>
/// 
namespace ns_DataImport
{
    public class mcu
    {
        int externalDatabaseType;
        DataTable masterDT;
        string configPath;
        ns_Logger.Logger log;
        myVRMNet.NETFunctions obj;

        public mcu(int external, DataTable masterDataTable, string config)
        {
            externalDatabaseType = external;
            masterDT = masterDataTable;
            configPath = config;
            log = new ns_Logger.Logger();
            obj = new myVRMNet.NETFunctions();
        }

        public bool Process(ref int cnt)
        {
            DataRow dr;
            DataRow drNext; // = new DataRow();
            String outXML = "";
            for (int j = 0; j < masterDT.Rows.Count - 1; j++)
            {
                dr = masterDT.Rows[j];
                drNext = masterDT.Rows[j + 1];
                String MCUInXML = Create_MCUInXML(dr["Name"].ToString(), dr["Login"].ToString(), dr["Password"].ToString(), dr["Vendor Type"].ToString());
                log.Trace("<br>" + MCUInXML);
                outXML = obj.CallMyVRMServer("SetBridge", MCUInXML, configPath);
                if (outXML.IndexOf("<error>") >= 0)
                {
                    //write to a log file                 
                    log.Trace(obj.ShowErrorMessage(outXML));
                }
                else
                {
                    log.Trace("Success");
                    cnt++;
                }
            }
               return true;
        }
        private String Create_MCUInXML(String Name, String Login, String Password, String VendorType)
        {
            try
            {
                int confserID = 0; //FB 2016
                String inXML = "<setBridge>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "    <userID>" + HttpContext.Current.Session["userID"] + "</userID>";
                inXML += "    <bridge>";
                inXML += "      <bridgeID>new</bridgeID>";
                inXML += "      <name>" + Name + "</name>";
                inXML += "      <login>" + Login + "</login>";
                inXML += "      <password>" + Password + "</password>";
                inXML += "      <timeZone>26</timeZone>";
                inXML += "      <maxAudioCalls>10</maxAudioCalls>";
                inXML += "      <maxVideoCalls>10</maxVideoCalls>";
                inXML += "      <bridgeType>4</bridgeType>";
                inXML += "      <bridgeStatus>1</bridgeStatus>";
                inXML += "      <virtualBridge>1</virtualBridge>";
                inXML += "      <virtualBridge>0</virtualBridge>";
                inXML += "      <isPublic>1</isPublic>";
              //inXML += "      <isPublic>0</isPublic>";
                inXML += "      <bridgeAdmin>";
                inXML += "      <ID>1</ID>";
                inXML += "      </bridgeAdmin>";
                inXML += "      <firmwareVersion>2.x</firmwareVersion>";
                inXML += "      <percentReservedPort>20</percentReservedPort>";
                inXML += "      <approvers>";
                inXML += "      <approver>";
                inXML += "      <ID></ID>";
                inXML += "      </approver>";
                inXML += "      <approver>";
                inXML += "      <ID></ID>";
                inXML += "      </approver>";
                inXML += "      <approver>";
                inXML += "      <ID></ID>";
                inXML += "      </approver>";
                inXML += "      </approvers>";
                inXML += "      <ApiPortNo>80</ApiPortNo>";//API Port...
                inXML += "      <bridgeDetails>";
                inXML += "      <controlPortIPAddress>172.1.1.1</controlPortIPAddress>";
                inXML += "      <EnableIVR>1</EnableIVR>";
                inXML += "      <IVRServiceName>s</IVRServiceName>";
                inXML += "      <enableRecord>1</enableRecord>";
                inXML += "      <enableLPR>1</enableLPR>";
                inXML += "      <IPServices>0</IPServices>";
                inXML += "      <ISDNServices>0</ISDNServices>";
                inXML += "          <ISDNService></ISDNService>";
                inXML += "          <MPIService>0</MPIService>";
                inXML += "      <MCUCards>0</MCUCards>";
                inXML += "          <portA></portA>";
                inXML += "          <portB></portB>";
                inXML += "          <ISDNAudioPref></ISDNAudioPref>";//FB 2003
                inXML += "          <ISDNVideoPref></ISDNVideoPref>";
                inXML += "      </bridgeDetails>";
                inXML += "  <ISDNThresholdAlert></ISDNThresholdAlert>";
                inXML += "  <ISDNPortCharge></ISDNPortCharge>";
                inXML += "  <ISDNLineCharge></ISDNLineCharge>";
                inXML += "  <ISDNMaxCost></ISDNMaxCost>";
                inXML += "  <ISDNThresholdTimeframe></ISDNThresholdTimeframe>";
                inXML += "  <ISDNThreshold></ISDNThreshold>";
                inXML += "      <malfunctionAlert>1</malfunctionAlert>";
                inXML += " <DTMF>";
                inXML += "     <PreConfCode></PreConfCode>";
                inXML += "     <PreLeaderPin></PreLeaderPin>";
                inXML += "     <PostLeaderPin></PostLeaderPin>";
                inXML += "</DTMF>";
                inXML += "  </bridge>";
                inXML += "</setBridge>";
                return inXML;
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
                return "";
            }
        }
    }
}
using System;
using System.Data;
using System.Xml;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections;

/// <summary>
/// Summary description for Tier1
/// </summary>
/// 
namespace ns_DataImport
{
    public class Room
    {
        int externalDatabaseType;
        DataTable masterDT;
        string configPath;
        ns_Logger.Logger log;
        myVRMNet.NETFunctions obj;
        System.Web.UI.WebControls.DropDownList lstTimeZone;

        public Room(int external, DataTable masterDataTable, string config)
        {
            externalDatabaseType = external;
            masterDT = masterDataTable;
            configPath = config;
            log = new ns_Logger.Logger();
            obj = new myVRMNet.NETFunctions();
        }

        public bool Process(ref int cnt)
        {
            string tzID = "-1";
            String inXML = "";
            string eptnme = "";//FB 2362
            inXML += "<GetLocations>";
            inXML += obj.OrgXMLElement();//Organization Module Fixes
            inXML += "  <UserID>" + HttpContext.Current.Session["userID"].ToString() + "</UserID>";
            inXML += "</GetLocations>";
            String outXML = obj.CallMyVRMServer("GetLocations", inXML, configPath);
            XmlDocument xmldoc = new XmlDocument();
            xmldoc.LoadXml(outXML);
            //XmlNodeList nodesTier1 = xmldoc.SelectNodes("//locationList/tier1List/tier1");
            XmlNodeList nodesTier1 = xmldoc.SelectNodes("//GetLocations/Location");

            //inXML = "<login><userID>11</userID></login>";
            inXML = "";
            inXML += "<login>";
            inXML += "  <userID>" + HttpContext.Current.Session["userID"].ToString() + "</userID>";
            inXML += obj.OrgXMLElement();
            inXML += "</login>";
            outXML = obj.CallMyVRMServer("GetManageDepartment", inXML, configPath);
            XmlDocument xmldoc1 = new XmlDocument();
            xmldoc1.LoadXml(outXML);
            XmlNodeList nodesDept = xmldoc1.SelectNodes("//getManageDepartment/departments/department");
            int i = 0;

            lstTimeZone = new DropDownList();
            lstTimeZone.DataValueField = "timezoneID";
            lstTimeZone.DataTextField = "timezoneName";
            obj.GetTimezones(lstTimeZone, ref tzID);

            foreach (DataRow dr in masterDT.Rows)
            {
                if ((dr["Room Name"].ToString().IndexOf("(none)") < 0) && (dr["Tier One"].ToString().IndexOf("(none)") < 0) && (dr["Tier Two"].ToString().IndexOf("(none)") < 0))
                {
                    i++;

                    eptnme = dr["Endpoint Name"].ToString();//FB 2362

                    if (eptnme.Trim() != "")
                        eptnme = dr["Room Name"].ToString();

                    inXML = "<SearchEndpoint>";
                    inXML += "  <UserID>11</UserID>";
                    inXML += obj.OrgXMLElement();//FB 2362
                    inXML += "  <EndpointName>" + eptnme + "</EndpointName>";//FB 2362
                    inXML += "  <EndpointType>1</EndpointType>";
                    inXML += "  <PageNo>1</PageNo>";
                    inXML += "</SearchEndpoint>";
                    outXML = obj.CallMyVRMServer("SearchEndpoint", inXML, HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());
                    XmlDocument xmldocEP = new XmlDocument();
                    xmldocEP.LoadXml(outXML);
                    //int endpointID = GetEndpointID(nodesEP, dr["Room"].ToString());
                    String endpointID = "0";
                    if (xmldocEP.SelectNodes("//SearchEndpoint/Endpoints/Endpoint").Count > 0)
                        endpointID = xmldocEP.SelectSingleNode("//SearchEndpoint/Endpoints/Endpoint/ID").InnerText;

                    int tier1ID = 0;
                    if (dr["Tier One"].ToString().IndexOf("(none)") < 0)
                        tier1ID = GetTier1ID(nodesTier1, dr["Tier One"].ToString());

                    int deptID = 0;
                    if (dr["Department"].ToString().IndexOf("(none)") < 0)
                        deptID = GetDepartmentID(nodesDept, dr["Department"].ToString());


                    int tier2ID = 0;
                    if (dr["Tier Two"].ToString().IndexOf("(none)") < 0)
                        tier2ID = GetTier2ID(tier1ID, dr["Tier Two"].ToString());

                    if (dr["Room Name"].ToString().Trim() != "")//FB 2362
                    {
                        log.Trace("<br>" + i + ": tier1ID: " + tier1ID + " : " + dr["Tier One"] + "tier2ID: " + tier2ID + " : " + deptID + " : " + dr["Department"]);
                        String roomInXML = Create_roomInXMLNew(deptID, tier1ID, tier2ID, endpointID, dr["Room Name"].ToString(), GetTimeZone(dr["Time Zone"].ToString()));
                        outXML = obj.CallMyVRMServer("SetRoomProfile", roomInXML, HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());


                        if (outXML.IndexOf("<error>") >= 0)
                        {
                            //write to a log file                 
                            log.Trace(obj.ShowErrorMessage(outXML));
                        }
                        else
                        {
                            log.Trace("Success");
                            cnt++;
                        }
                    }
                }
            }

            // put that in a messenger object 
            // call the com cmd 
            // get the tiers 
            // save the info in csv file
            return true;
        }
        protected int GetTier1ID(XmlNodeList nodes, String tier1Name)
        {
            try
            {
                foreach (XmlNode node in nodes)
                {
                    if (node.SelectSingleNode("Name").InnerText.Trim().Equals(tier1Name.Trim()))
                        return Int32.Parse(node.SelectSingleNode("ID").InnerText);
                }
                return -1;
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
                return -1;
            }
        }
        protected int GetTier2ID(int tier1ID, String tier2Name)
        {
            try
            {
                //String inXML = "<login><userID>11</userID><tier1ID>" + tier1ID + "</tier1ID></login>";
                String inXML = "";
                inXML += "<GetLocations2>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "  <UserID>" + HttpContext.Current.Session["userID"].ToString() + "</UserID>";
                inXML += "  <Tier1ID>" + tier1ID + "</Tier1ID>";
                inXML += "</GetLocations2>";
                String outXML = obj.CallMyVRMServer("GetLocations2", inXML, configPath);
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);
                XmlNodeList nodes = xmldoc.SelectNodes("//GetLocations2/Location");
                foreach (XmlNode node in nodes)
                {
                    if (node.SelectSingleNode("Name").InnerText.Trim().Equals(tier2Name.Trim()))
                        return Int32.Parse(node.SelectSingleNode("ID").InnerText);
                }
                return -1;
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
                return -1;
            }
        }
        protected int GetDepartmentID(XmlNodeList nodes, String deptName)
        {
            try
            {
                foreach (XmlNode node in nodes)
                {
                    if (node.SelectSingleNode("name").InnerText.Trim().Equals(deptName.Trim()))
                        return Int32.Parse(node.SelectSingleNode("id").InnerText);
                }
                return -1;
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
                return -1;
            }
        }
        protected int GetEndpointID(XmlNodeList nodes, String EPName)
        {
            try
            {
                foreach (XmlNode node in nodes)
                {
                    if (node.SelectSingleNode("EndpointName").InnerText.Trim().Equals(EPName.Trim()))
                        return Int32.Parse(node.SelectSingleNode("ID").InnerText);
                }
                return -1;
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
                return -1;
            }
        }

        protected string GetTimeZone(string timeZoneName)
        {
            string timeZoneID = "26";
            try
            {
                if (lstTimeZone != null)
                {

                    for (int j = 0; j < lstTimeZone.Items.Count; j++)
                    {
                        if (lstTimeZone.Items[j].Text.ToUpper().Contains(timeZoneName.ToUpper()) || timeZoneName.ToUpper().Contains(lstTimeZone.Items[j].Text.ToUpper()))
                            timeZoneID = lstTimeZone.Items[j].Value;

                    }
                }
                return timeZoneID;
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
                return "26";
            }
        }

        private String Create_roomInXMLNew(int deptID, int tier1ID, int tier2ID, String endpointID, String RoomName, string timezoneID)
        {
            try
            {
                String inXML = "";
                inXML += "<SetRoomProfile>";
                inXML += obj.OrgXMLElement();
                inXML += "<UserID>11</UserID>";
                inXML += "<RoomID>new</RoomID>";
                //inXML += "<RoomName>" + RoomName + " test</RoomName>";
                inXML += "<RoomName>" + RoomName + "</RoomName>";//Code commented for BCS
                inXML += "<RoomPhoneNumber></RoomPhoneNumber>";
                inXML += "<MaximumCapacity>0</MaximumCapacity>";
                inXML += "<MaximumConcurrentPhoneCalls>0</MaximumConcurrentPhoneCalls>";
                inXML += "<SetupTime>0</SetupTime>";
                inXML += "<TeardownTime>0</TeardownTime>";
                inXML += "<AssistantInchargeID>257</AssistantInchargeID>";//FB 2362
                inXML += "<AssistantInchargeName></AssistantInchargeName>";
                inXML += "<MultipleAssistantEmails></MultipleAssistantEmails>";
                inXML += "<Tier1ID>" + tier1ID + "</Tier1ID>";
                inXML += "<Tier2ID>" + tier2ID + "</Tier2ID>";
                inXML += "<Floor>0</Floor>";
                inXML += "<RoomNumber></RoomNumber>";
                inXML += "<Handicappedaccess>0</Handicappedaccess>";
                inXML += "<isVIP></isVIP>";
                inXML += "<isTelepresence></isTelepresence>";
                inXML += "<ServiceType></ServiceType>";
                inXML += "<DedicatedVideo></DedicatedVideo>";
                inXML += "<StreetAddress1></StreetAddress1>";
                inXML += "<StreetAddress2></StreetAddress2>";
                inXML += "<City></City>";
                inXML += "<State>34</State>";
                inXML += "<ZipCode></ZipCode>";
                inXML += "<Country>225</Country>";
                inXML += "<MapLink></MapLink>";
                inXML += "<ParkingDirections></ParkingDirections>";
                inXML += "<AdditionalComments></AdditionalComments>";
                inXML += "<TimezoneID>" + timezoneID + "</TimezoneID>";
                inXML += "<Longitude></Longitude>";
                inXML += "<Latitude></Latitude>";
                inXML += "<RoomImageName></RoomImageName>";
                inXML += "<RoomImages></RoomImages>";
                //inXML += "<Image>0</Image>";
                //inXML += "</RoomImages>";
                inXML += "<Images>";
                inXML += "<Map1></Map1>";
                inXML += "<Map1Image></Map1Image>";
                inXML += "<Map2></Map2>";
                inXML += "<Map2Image></Map2Image>";
                inXML += "<Security1></Security1>";
                inXML += "<Security1ImageId></Security1ImageId>";
                inXML += "<Security2></Security2>";
                inXML += "<Misc1></Misc1>";
                inXML += "<Misc1Image></Misc1Image>";
                inXML += "<Misc2></Misc2>";
                inXML += "<Misc2Image></Misc2Image>";
                inXML += "</Images>";
                inXML += "<Approvers>";
                inXML += "<Approver1ID>11</Approver1ID>";
                inXML += "<Approver1Name></Approver1Name>";
                inXML += "<Approver2ID></Approver2ID>";
                inXML += "<Approver2Name></Approver2Name>";
                inXML += "<Approver3ID></Approver3ID>";
                inXML += "<Approver3Name></Approver3Name>";
                inXML += "</Approvers>";
                inXML += "<EndpointID>" + endpointID + "</EndpointID>";
                inXML += "<Custom1></Custom1>";
                inXML += "<Custom2></Custom2>";
                inXML += "<Custom3></Custom3>";
                inXML += "<Custom4></Custom4>";
                inXML += "<Custom5></Custom5>";
                inXML += "<Custom6></Custom6>";
                inXML += "<Custom7></Custom7>";
                inXML += "<Custom8></Custom8>";
                inXML += "<Custom9></Custom9>";
                inXML += "<Custom10></Custom10>";
                inXML += "<Projector>1</Projector>";//Changed for BCS
                if(endpointID != "0")//FB 2362
                    inXML += "<Video>2</Video>";
                else
                    inXML += "<Video>0</Video>";//FB 2362
                inXML += "<DynamicRoomLayout></DynamicRoomLayout>";
                inXML += "<CatererFacility></CatererFacility>";
                inXML += "<Departments>" + deptID + "</Departments>";
                inXML += "<RoomImage>room1</RoomImage>";
                inXML += "<RoomQueue>" + RoomName + "@york.com </RoomQueue>";
                inXML += "</SetRoomProfile>";

                return inXML;

            }
            catch (Exception ex)
            {
                return "";
            }
        }
    
    }
}
﻿<%@ Application Language="C#" %>

<script runat="server">
    
    void Application_Start(object sender, EventArgs e) 
    {
        // Code that runs on application startup
        Application["OnlineUserList"] = "";
        //ReadCOMConfig();

    }
    
    void Application_End(object sender, EventArgs e) 
    {
        //  Code that runs on application shutdown

    }
        
    void Application_Error(object sender, EventArgs e) 
    { 
        // Code that runs when an unhandled error occurs

    }

    void Session_Start(object sender, EventArgs e) 
    {
        // Code that runs when a new session is started
        Session["DefaultDateType"] = "MM/dd/yyyy";
        ReadCOMConfig();

    }

    void Session_End(object sender, EventArgs e) 
    {
        // Code that runs when a session ends. 
        // Note: The Session_End event is raised only when the sessionstate mode
        // is set to InProc in the Web.config file. If session mode is set to StateServer 
        // or SQLServer, the event is not raised.
        if(Session["userID"] != null )
        {
            if(Session["userID"].ToString().Trim() != "")
            {
                if (Application["OnlineUserList"] != null)
                {
                    string localUserList = Application["OnlineUserList"].ToString();
                    string userid = Session["userID"].ToString().Trim();
                    string[] usrArray = localUserList.Split(',');

                    if (usrArray.Length > 0)
                    {
                        localUserList = "";
                        for (int lp = 0; lp < usrArray.Length; lp++)
                        {
                            if (usrArray[lp].ToString().Trim() == userid)
                                continue;

                            localUserList = localUserList + usrArray[lp].ToString().Trim() + ",";
                        }
                        Application.Lock();
                        Application["OnlineUserList"] = localUserList;
                        Application.UnLock();
                    }
                }
            }
        }
    }

    /// <summary>
    /// ReadCOMConfig
    /// To retreive XML value from the vrmwebconfig.xml
	/// and put to the Application variables for use throughout the session
    /// </summary>
    private void ReadCOMConfig()
    {
        //msxml4_Net.DOMDocument40Class XmlDoc = null; //Modified to remove msXML refs
        System.Xml.XmlDocument XmlDoc = null;
        try
        {
            XmlDoc = new System.Xml.XmlDocument(); // msxml4_Net.DOMDocument40Class();
           // XmlDoc.async = false;
            XmlDoc.Load("C:\\VRMSchemas_v1.8.3\\VRMConfig.xml");

            Application["mode"] = XmlDoc.SelectSingleNode("//VRMConfig/WebMode").InnerText.Trim();
            Application["debugMode"] = XmlDoc.SelectSingleNode("//VRMConfig/DebugMode").InnerText.Trim();
            Application["ssoMode"] = XmlDoc.SelectSingleNode("//VRMConfig/SSOMode").InnerText.Trim().ToLower();
            Application["Client"] = XmlDoc.SelectSingleNode("//VRMConfig/Client").InnerText.Trim().ToLower();
            Application["confRecurrence"] = XmlDoc.SelectSingleNode("//VRMConfig/Conf_Reccurance").InnerText.Trim().ToLower();
            Application["MaxConferenceDurationInHours"] = XmlDoc.SelectSingleNode("//VRMConfig/MaxConferenceDurationInHours").InnerText.Trim();

            Application["CosignEnable"] = XmlDoc.SelectSingleNode("//VRMConfig/CosignEnable").InnerText.Trim();
            Application["LDAPDomain"] = XmlDoc.SelectSingleNode("//VRMConfig/LDAPDomain").InnerText.Trim();
            Application["External"] = XmlDoc.SelectSingleNode("//VRMConfig/External").InnerText.Trim(); //FB 2363
            
			
	        if(Application["confRecurrence"] == null)
            {
		        Application["confRecurrence"] = 30;
            }
            if(Application["confRecurrence"].ToString() == "")
            {
		        Application["confRecurrence"] = 30;
            }
	        if(Application["MaxConferenceDurationInHours"] == null)
            {
		        Application["MaxConferenceDurationInHours"] = 24;
            }
            if(Application["MaxConferenceDurationInHours"].ToString() == "")
            {
                Application["MaxConferenceDurationInHours"] = 24;
            }
	        XmlDoc = null;
        }
        catch (Exception) { }
    }
       
</script>

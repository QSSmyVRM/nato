using System;
using System.Collections;
using System.Collections.Generic;

namespace myVRM.DataLayer
{
    /// <summary>
    /// Extends the <see cref="IDao{T,IdT}" /> behavior, 
    /// </summary>
    public interface IT3RoomDAO : IDao<vrmTier3, int>{}
    public interface IT2RoomDAO : IDao<vrmTier2, int> { }
    public interface IRoomDAO : IDao<vrmRoom, int>
    {
        List<vrmRoom> GetByT3Id(int Id);
        List<vrmRoom> GetByT2Id(int Id);
        vrmRoom GetByRoomId(int Id);
        vrmRoom GetByRoomQueue(string RmEmail); //FB 2342
        //Boolean isRoomVIP(int Id);
    }
    public interface ILocDeptDAO : IDao<vrmLocDepartment, int> { }
    public interface ILocApprovDAO : IDao<vrmLocApprover, int> { }
    //FB 2392 -WhyGo
    public interface IESPublicRoomDAO : IDao<ESPublicRoom, int>
    {
        List<ESPublicRoom> GetBymyVRMRoomId(ICollection Id);
        ESPublicRoom GetBymyVRMRoomId(int Id);
        ESPublicRoom GetByWhygoRoomId(int Id);
    } 
}

using System;
using System.Collections;
using System.Collections.Generic;

namespace myVRM.DataLayer
{
    /// <summary>
    /// Extends the <see cref="IDao{T,IdT}" /> behavior, 
    /// </summary>
    public interface ICountryDAO : IDao<vrmCountry, int> 
    {
        vrmCountry GetCountryById(int Id);
    }
    public interface IStateDAO : IDao<vrmState, int> 
    {
        vrmState GetStateById(int Id);
    }
    //FB 1830
    public interface ILanguageDAO : IDao<vrmLanguage, int>
    {
        vrmLanguage GetLanguageById(int Id);
    }
    //NewLobby
    public interface IIconsRefDAO : IDao<vrmIconsRef, int>{ }
}

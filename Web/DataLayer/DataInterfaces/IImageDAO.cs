﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace myVRM.DataLayer
{
    /// <summary>
    /// Extends the <see cref="IDao{T,IdT}" /> behavior, 
    /// </summary>
    public interface IImageDAO : IDao<vrmImage, int> { vrmImage GetById(int id); }
    public interface ISecBadgeDao : IDao<vrmSecurityImage, int> { vrmSecurityImage GetByBadgeId(int id); } //FB 2136
}

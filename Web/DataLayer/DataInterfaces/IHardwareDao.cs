using System;
using System.Collections;
using System.Collections.Generic;

namespace myVRM.DataLayer
{
    /// <summary>
    /// Extends the <see cref="IDao{T,IdT}" /> behavior, 
    /// </summary>
    public interface IEptDao : IDao<vrmEndPoint, int> { vrmEndPoint GetByEptId(int id);} //FB 2027
    public interface IMCUDao : IDao<vrmMCU, int> { }
    public interface IMCUISDNServicesDao : IDao<vrmMCUISDNServices, int> { }
    public interface IMCUIPServicesDao   : IDao<vrmMCUIPServices,   int> { }
    public interface IMCUMPIServicesDao : IDao<vrmMCUMPIServices, int> { }
    public interface IMCUCardListDao : IDao<vrmMCUCardList, int>    { }
    public interface IMCUApproverDao : IDao<vrmMCUApprover, int> { }
    public interface IMessageDao : IDao<vrmMessage, int> { vrmMessage GetByMsgId(int id); } //FB 2486
    public interface IMCUParamsDao : IDao<vrmMCUParams, int> { vrmMCUParams GetByBridgeTypeId(int id); } //FB 2501 - Call Monitoring
   
    public interface IMCUProfilesDao : IDao<vrmMCUProfiles, int> { } //FB 2591 

    public interface IMCUE164ServicesDao : IDao<vrmMCUE164Services, int> { } // FB 2636
}

using System;
using System.Collections;
using System.Collections.Generic;

namespace myVRM.DataLayer
{
    /// <summary>
    /// Extends the <see cref="IDao{T,IdT}" /> behavior, 
    /// </summary>
    public interface IReportScheduleDao : IDao<ReportSchedule, int>
    {
        List<ReportSchedule> GetSubmitBetween(DateTime startDate, DateTime endDate);
    }
    public interface IReportTemplateDao : IDao<ReportTemplate, int>
    {
       // List<ReportTemplate> GetSubmitBetween(DateTime startDate, DateTime endDate);
    }
    public interface IReportInputItemsDao : IDao<ReportInputItem, int>{};
    // FB 2343 Start
    public interface IReportMonthlyDaysDAO : IDao<ReportMonthlyDays, int>{}; 
    public interface IReportWeeklyDaysDAO : IDao<ReportWeeklyDays, int> { };
    // FB 2343 End
}

using System;
using System.Collections;
using System.Collections.Generic;

namespace myVRM.DataLayer
{
    /// <summary>
    /// Extends the <see cref="IDao{T,IdT}" /> behavior, 
    /// </summary>
    public interface ISystemDAO : IDao<sysData, int> { sysData GetByAdminId(int id); }
    public interface ISysTimeZonePrefDAO : IDao<sysTimeZonePref, int> { List<sysTimeZonePref> GetAllPrefTimeZones(string ConfigPath);}
    public interface ISysMailDAO : IDao<sysMailData, int> { sysMailData GetByMailId(int id); }
    public interface IAccSchemeDAO : IDao<sysAccScheme, int> { }
    //LDAP Configuration Settings
    public interface ILDAPConfigDAO : IDao<vrmLDAPConfig, int> { vrmLDAPConfig GetLDAPConfigDetails(string ConfigPath); }
    public interface IExternalSchedulingDAO : IDao<sysExternalScheduling, int> { }//FB 2363
    public interface IESMailUsrRptSettingsDAO : IDao<ESMailUsrRptSettings, int> { }//FB 2363
    public interface IsysWhygoSettings : IDao<sysWhygoSettings, int> { }//FB 2392
}

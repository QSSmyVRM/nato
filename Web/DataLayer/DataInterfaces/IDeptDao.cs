using System;
using System.Collections;
using System.Collections.Generic;

namespace myVRM.DataLayer
{
    /// <summary>
    /// Extends the <see cref="IDao{T,IdT}" /> behavior, 
    /// </summary>
    public interface IDeptDao : IDao<vrmDept, int> { }
    public interface IUserDeptDao : IDao<vrmUserDepartment, int> { }
    public interface IDeptApproverDao : IDao<vrmDeptApprover, int> { }

    public interface IDeptCustomAttrDao : IDao<vrmDeptCustomAttr, int> { }
    public interface IDeptCustomAttrExDao : IDao<vrmDeptCustomAttrEx, int> { }
    public interface IDeptCustomAttrOptionDao : IDao<vrmDeptCustomAttrOption, int> { }
    //FB 1970 
    public interface ICustomLangDao : IDao<VrmCustomLanguage, int>
    {
        List<VrmCustomLanguage> GetCustomAttrTitlesByID(int ID);
    }

}

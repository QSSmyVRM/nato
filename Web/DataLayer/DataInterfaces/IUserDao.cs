using System;
using System.Collections;
using System.Collections.Generic;

namespace myVRM.DataLayer
{
    /// <summary>
    /// Extends the <see cref="IDao{T,IdT}" /> behavior, 
    /// </summary>
    public interface IUserDao : IDao<vrmUser, int>
    {
        vrmUser GetByUserId(int id);
        vrmUser GetByUserEmail(string emailID);//FB 2342
    } 
    public interface IGuestUserDao : IDao<vrmGuestUser, int> { vrmGuestUser GetByUserId(int id);}
    public interface IInactiveUserDao : IDao<vrmInactiveUser, int> { vrmInactiveUser GetByUserId(int id);}
//    public interface IEmailDao : IDao<vrmEmail, int> { }
    public interface IUserLotusDao : IDao<vrmUserLotusNotesPrefs, int> { } //FB 2027
    public interface IUserRolesDao : IDao<vrmUserRoles, int> { }
    public interface IVRMLdapUserDao : IDao<vrmLDAPUser, int> { }
    public interface IUserTemplateDao : IDao<vrmUserTemplate, int> { }
    public interface IUserAccountDao : IDao<vrmAccount, int> { vrmAccount GetByUserId(int id);} //FB 2027SetBulkUserAddMinutes
    public interface IUserSearchTemplateDao : IDao<vrmUserSearchTemplate, int> { }
    public interface IGrpDetailDao : IDao<vrmGroupDetails, int> { }
    public interface IGrpParticipantsDao : IDao<vrmGroupParticipant, int> { }
    public interface IAccountGroupListDAO : IDao<vrmAccountGroupList, int> { } //FB 2027
	public interface ILanguageTextDao : IDao<vrmLanguageText, int> { List<vrmLanguageText> GetByLanguageId(int id); } //FB 1830 - Translation
	//NewLobby
    public interface IUserLobbyDAO : IDao<vrmUserLobby, int>{ }

}

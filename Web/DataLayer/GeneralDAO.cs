using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;

using NHibernate;
using NHibernate.Criterion;

namespace myVRM.DataLayer
{
    public class GeneralDAO
    {
        private ISession session;
        private log4net.ILog m_log;
        private string m_configPath;
        
        public GeneralDAO(string config, log4net.ILog olog)
        {
            try
            {
                m_log = olog;
                m_configPath = config;

                bool valid = Init();
                if (!valid)
                    throw new Exception("Could not initialize GeneralDAO");

            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
            }
        }
        private bool Init()
        {
            try
            {
                session = SessionManagement.GetSession(m_configPath);
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
        }
        public ICountryDAO GetCountryDAO()
        {
            return new CountryDAO(m_configPath);
        }
        public IStateDAO GetStateDAO()
        {
            return new StateDAO(m_configPath);
        }
        public class CountryDAO :AbstractPersistenceDao<vrmCountry, int>, ICountryDAO
        {
            public CountryDAO(string ConfigPath) : base(ConfigPath) { }
            public new List<vrmCountry> GetActive()
            {
                List<int> SelectedValues = new List<int>();
                SelectedValues.Add(0);
                SelectedValues.Add(1);
                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.In("Selected", SelectedValues));
                return GetByCriteria(criterionList);
            }
            public vrmCountry GetCountryById(int Id)
            {
                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("CountryID", Id));
                List<vrmCountry> Countries = GetByCriteria(criterionList);
                if (Countries.Count > 0)
                    return Countries[0];
                else
                    return null;
            }
        }
        public class StateDAO : AbstractPersistenceDao<vrmState, int>, IStateDAO
        {
            public StateDAO(string ConfigPath) : base(ConfigPath) { }
            public new List<vrmState> GetActive()
            {
                List<int> SelectedValues = new List<int>();
                SelectedValues.Add(0);
                SelectedValues.Add(1);
                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.In("Selected", SelectedValues));
                return GetByCriteria(criterionList);
            }
            public vrmState GetStateById(int Id)
            {
                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("Id", Id));
                List<vrmState> States = GetByCriteria(criterionList);
                if (States.Count > 0)
                    return States[0];
                else
                    return null;
            }
        }

        //FB 1830 start
        public ILanguageDAO GetLanguageDAO()
        {
            return new LanguageDAO(m_configPath);
        }
        public class LanguageDAO : AbstractPersistenceDao<vrmLanguage, int>, ILanguageDAO
        {
            public LanguageDAO(string ConfigPath) : base(ConfigPath) { }
            public vrmLanguage GetLanguageById(int Id)
            {
                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("Id", Id));
                List<vrmLanguage> langs = GetByCriteria(criterionList);
                if (langs.Count > 0)
                    return langs[0];
                else
                    return null;
            }
        }
        //FB 1830 end
        //NewLobby start
        public IIconsRefDAO GetIconRefDAO()
        {
            return new IconsRefDAO(m_configPath);
        }
        public class IconsRefDAO : AbstractPersistenceDao<vrmIconsRef, int>, IIconsRefDAO
        {
            public IconsRefDAO(string ConfigPath) : base(ConfigPath) { }
        }
        //NewLobby end
    }
}

using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;

using NHibernate;
using NHibernate.Criterion;


namespace myVRM.DataLayer
{
    /// <summary>
    /// Data Access Object for Departments.
    /// Implement IDispose to close session
    /// </summary>
    public class hardwareDAO : vrmDAO
    {
        public hardwareDAO(string config, log4net.ILog log) : base(config, log) { }


        public IEptDao GetEptDao() { return new eptDao(m_configPath); }
        public IMCUDao GetMCUDao() { return new MCUDao(m_configPath); }


        // FB 2636
        public IMCUE164ServicesDao GetE164ServicesDao()
        { return new MCUE164ServicesDao(m_configPath); }

        public IMCUISDNServicesDao GetMCUISDNServicesDao()
        { return new MCUISDNServicesDao(m_configPath); }
        public IMCUIPServicesDao GetMCUIPServicesDao()
        { return new MCUIPServicesDao(m_configPath); }
        public IMCUMPIServicesDao GetMCUMPIServicesDao()
        { return new MCUMPIServicesDao(m_configPath); }
        public IMCUCardListDao GetMCUCardListsDao()
        { return new MCUCardListDao(m_configPath); }
        public IMCUApproverDao GetMCUApproverDao()
        { return new MCUApproverDao(m_configPath); }
        //FB 2486
        public IMessageDao GetMessageDao()
        { return new MessageDao(m_configPath); }

        //FB 2501 - Call Monitoring
        public IMCUParamsDao GetMCUParamsDao() { return new MCUParamsDao(m_configPath); }

        //FB 2591 Starts
        public IMCUProfilesDao GetMCUProfilesDao()
        {
            return new MCUProfilesDao(m_configPath);
        }
        //FB 2591 Ends
      
        public class eptDao :
               AbstractPersistenceDao<vrmEndPoint, int>, IEptDao
        {
            public eptDao(string ConfigPath) : base(ConfigPath) { }
            
            //FB 2027 - Starts
            public vrmEndPoint GetByEptId(int id)
            {
                try
                {
                    List<ICriterion> criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("endpointid", id));
                    List<vrmEndPoint> myVrmEpt = GetByCriteria(criterionList);
                    if (myVrmEpt.Count <= 0)
                        return null;
                    return myVrmEpt[0];
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
            //FB 2027 - End
        }
        public class MCUDao :
            AbstractPersistenceDao<vrmMCU, int>, IMCUDao
        {
            public MCUDao(string ConfigPath) : base(ConfigPath) { }

        }

        public class MCUISDNServicesDao :
            AbstractPersistenceDao<vrmMCUISDNServices, int>, IMCUISDNServicesDao
        {
            public MCUISDNServicesDao(string ConfigPath) : base(ConfigPath) { }

        }
        public class MCUIPServicesDao :
           AbstractPersistenceDao<vrmMCUIPServices, int>, IMCUIPServicesDao
        {
            public MCUIPServicesDao(string ConfigPath) : base(ConfigPath) { }

        }
        public class MCUMPIServicesDao :
           AbstractPersistenceDao<vrmMCUMPIServices, int>, IMCUMPIServicesDao
        {
            public MCUMPIServicesDao(string ConfigPath) : base(ConfigPath) { }

        }
        public class MCUE164ServicesDao : //FB 2636
          AbstractPersistenceDao<vrmMCUE164Services, int>, IMCUE164ServicesDao
        {
            public MCUE164ServicesDao(string ConfigPath) : base(ConfigPath) { }

        }
        public class MCUCardListDao :
           AbstractPersistenceDao<vrmMCUCardList, int>, IMCUCardListDao
        {
            public MCUCardListDao(string ConfigPath) : base(ConfigPath) { }

        }
        public class MCUApproverDao :
           AbstractPersistenceDao<vrmMCUApprover, int>, IMCUApproverDao
        {
            public MCUApproverDao(string ConfigPath) : base(ConfigPath) { }

        }
        //FB 2486
        public class MessageDao :
          AbstractPersistenceDao<vrmMessage, int>, IMessageDao
        {
            public MessageDao(string ConfigPath) : base(ConfigPath) { }

            public vrmMessage GetByMsgId(int id)
            {
                try
                {
                    List<ICriterion> criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("msgId", id));
                    List<vrmMessage> TxtMsg = GetByCriteria(criterionList);
                    if (TxtMsg.Count <= 0)
                        return null;
                    return TxtMsg[0];
                }
                catch (Exception e)
                {
                    throw e;
                }
            }

        }

        //FB 2501 - Call Monitoring
        public class MCUParamsDao :
           AbstractPersistenceDao<vrmMCUParams, int>, IMCUParamsDao
        {
            public MCUParamsDao(string ConfigPath) : base(ConfigPath) { }

            public vrmMCUParams GetByBridgeTypeId(int id)
            {
                try
                {
                    List<ICriterion> criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("BridgeTypeid", id));
                    List<vrmMCUParams> myVrmMCU = GetByCriteria(criterionList);
                    if (myVrmMCU.Count <= 0)
                        return null;
                    return myVrmMCU[0];
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }

        //FB 2591 
        public class MCUProfilesDao :
            AbstractPersistenceDao<vrmMCUProfiles, int>, IMCUProfilesDao
        {
            public MCUProfilesDao(string ConfigPath ): base(ConfigPath){}

        }

      
    }

}

using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Xml;


namespace myVRM.DataLayer
{
	/// <summary>
	/// Summary description of conference status flags
    /// </summary>
    public class vrmConfStatus
    {
        public static int Scheduled = 0;
        public static int Pending = 1;
        public static int Terminated = 3;
        public static int Ongoing = 5;
        public static int OnMCU = 6;
        public static int Completed = 7;
        public static int Deleted = 9;
    }
    public class vrmConfType
    {
        public static int Video = 1;
        public static int AudioVideo = 2;
        public static int Immediate = 3;
        public static int P2P = 4; // Point to point
        public static int Template = 5;
        public static int AudioOnly = 6;
        public static int RooomOnly = 7;
        public static int Phantom = 11;
    }
    public class vrmConfUserType
    {
        public static int External = 1;
        public static int Room = 2;
        public static int CC = 3;
    }
    public class vrmConfUserStatus
    {
        public static int Undecided = 0;
        public static int Accepted = 1;
        public static int Rejectetd = 2;
        public static int Reschedule = 3;
    }
    //FB 2501 Call Monitoring start
    public class vrmConfTerminalType
    {
        public static int NormalUser = 1;
        public static int Room = 2;
        public static int GuestUser = 3;
        public static int ConfCascade = 4;
    }
    //FB 2501 Call Monitoring End
	/// <summary>
	/// Summary description for user table Conf_Confernce_D 
	/// (unigue)ID is confnumname
	/// </summary>
    public class vrmGeneric
    {
        #region Private Internal Members
        private int m_uId, m_confid, m_instanceid, m_confUid;
        #endregion

        #region Public Properties
        public int uId
        {
            get { return m_uId; }
            set { m_uId = value; }
        }
        public int confid
        {
            get { return m_confid; }
            set { m_confid = value; }
        }
        public int instanceid
        {
            get { return m_instanceid; }
            set { m_instanceid = value; }
        }
        public int confUid
        {
            get { return m_confUid; }
            set { m_confUid = value; }
        }

        #endregion

        vrmGeneric() { }

    }

    public class vrmAdvAvParams
    {
        #region Private Internal Members
        
        private int m_linerateID,
                    m_audioAlgorithmID,
		            m_videoProtocolID,
		            m_mediaID,
		            m_videoLayoutID,
		            m_dualStreamModeID,
		            m_conferenceOnPort,
		            m_encryption,
		            m_maxAudioParticipants,
		            m_maxVideoParticipants,
		            m_videoSession,
                    m_lectureMode,
                    m_videoMode,
                    m_singleDialin,
                    m_feccMode; // FB 2501 FECC
        private string m_internalBridge,  //FB 2376
                       m_externalBridge;
        //FB 2501 Call Monitoring
        private int m_Layout, m_MuteTxaudio, m_MuteRxaudio, m_MuteTxvideo, m_Recording, m_MuteRxvideo, m_Camera, m_ConfLockUnlock;
        private string m_PacketLoss, m_PolycomTemplate;//FB 2441
        private int m_PolycomSendMail; //FB 2636 //FB 2441

        #endregion

        #region Public Properties

        public int linerateID
        {
            get { return m_linerateID; }
            set { m_linerateID = value; }
		}
        public int audioAlgorithmID
        {
            get { return m_audioAlgorithmID; }
            set { m_audioAlgorithmID = value; }
		}	
        public int videoProtocolID{
            get { return m_videoProtocolID; }
            set { m_videoProtocolID = value; }
		}	
        public int mediaID{
            get { return m_mediaID; }
            set { m_mediaID = value; }
		}	
        public int videoLayoutID{
            get { return m_videoLayoutID; }
            set { m_videoLayoutID = value; }
		}	
        public int dualStreamModeID{
            get { return m_dualStreamModeID; }
            set { m_dualStreamModeID = value; }
		}	
        public int conferenceOnPort{
            get { return m_conferenceOnPort; }
            set { m_conferenceOnPort = value; }
		}	
        public int encryption{
            get { return m_encryption; }
            set { m_encryption = value; }
		}	
        public int maxAudioParticipants{
            get { return m_maxAudioParticipants; }
            set { m_maxAudioParticipants = value; }
		}	
        public int maxVideoParticipants{
            get { return m_maxVideoParticipants; }
            set { m_maxVideoParticipants = value; }
		}	
        public int videoSession{
            get { return m_videoSession; }
            set { m_videoSession = value; }
		}	
        public int lectureMode{
            get { return m_lectureMode; }
            set { m_lectureMode = value; }
        }
        public int videoMode
        {
            get { return m_videoMode; }
            set { m_videoMode = value; }
        }
        public int singleDialin
        {
            get { return m_singleDialin; }
            set { m_singleDialin = value; }
        }
        /** Fb 2376 **/

        public string internalBridge
        {
            get { return m_internalBridge; }
            set { m_internalBridge = value; }
        }

        public string externalBridge
        {
            get { return m_externalBridge; }
            set { m_externalBridge = value; }
        }

        // FB 2501 FECC Starts
        public int feccMode
        {
            get { return m_feccMode; }
            set { m_feccMode = value; }
        }
        // FB 2501 FECC Ends

        //FB 2501 Call Monitoring Start
        public int Layout
        {
            get { return m_Layout; }
            set { m_Layout = value; }
        }
        public int MuteTxaudio
        {
            get { return m_MuteTxaudio; }
            set { m_MuteTxaudio = value; }
        }
        public int MuteRxaudio
        {
            get { return m_MuteRxaudio; }
            set { m_MuteRxaudio = value; }
        }
        public int MuteRxvideo
        {
            get { return m_MuteRxvideo; }
            set { m_MuteRxvideo = value; }
        }
        public int MuteTxvideo
        {
            get { return m_MuteTxvideo; }
            set { m_MuteTxvideo = value; }
        }
        public int ConfLockUnlock 
        {
            get { return m_ConfLockUnlock; }
            set { m_ConfLockUnlock = value; }
        }
        public int Recording
        {
            get { return m_Recording; }
            set { m_Recording = value; }
        }
        public int Camera 
        {
            get { return m_Camera; }
            set { m_Camera = value; }
        }
        public string PacketLoss
        {
            get { return m_PacketLoss; }
            set { m_PacketLoss = value; }
        }
        //FB 2501 Call Monitoring End
        
        //FB 2441 start
        public int PolycomSendEmail
        {
            get { return m_PolycomSendMail; }
            set { m_PolycomSendMail = value; }
        }
        public string PolycomTemplate
        {
            get { return m_PolycomTemplate; }
            set { m_PolycomTemplate = value; }
        }
        //FB 2441 End

        public vrmAdvAvParams()
        {
            maxAudioParticipants = 0;
            maxVideoParticipants = 0;
            videoProtocolID = 0;
            mediaID = 0;
            videoLayoutID = 1;
            linerateID = 0;
            audioAlgorithmID = 0;
            videoSession = 0;
            dualStreamModeID = 0;
            conferenceOnPort = 0;
            encryption = 0;
            lectureMode = 0;
            videoMode = 0;
            singleDialin = 0;
            feccMode = 0; // FB 2501 FECC
            PolycomSendEmail = 0;//FB 2441 Starts
          
        }

        #endregion
    }
    public class vrmConfAdvAvParams : vrmAdvAvParams
    {
        #region Private Internal Members

        private int m_uId,
                    //m_confNumname,//FB 2027
                    m_confid,
                    m_instanceid,
                    m_confuId;
        //private vrmConference m_conf;//FB 2027

        #endregion

        #region Public Properties

        public int uId
        {
            get { return m_uId; }
            set { m_uId = value; }
        }
        public int confuId
        {
            get { return m_confuId; }
            set { m_confuId = value; }
        }
        public int confid
        {
            get { return m_confid; }
            set { m_confid = value; }
        }
        public int instanceid
        {
            get { return m_instanceid; }
            set { m_instanceid = value; }
        }
        //FB 2027 Modification - Start
        public vrmConfAdvAvParams()
        {
        }
        /*public int confnumname
            {
                get { return m_confNumname; }
                set { m_confNumname = value; }
            }
        
          public vrmConfAdvAvParams()
            {
                m_conf = new vrmConference();
            }
          public vrmConference Conf
            {
                get { return m_conf; }
                set { m_conf = value; }
            }*/
        //FB 2027 Modification - End

        /// <summary>
        /// This is the composite key for all confid/instanecid pairs..
        ///   you must overload equals and gethashcode to implement this class
        /// </summary>
       

        #endregion
    }
    public class vrmConfApproval
    {
        #region Private Internal Members
        private int m_confid;
        private int m_instanceid;
        private int m_uId;
        private int m_entitytype;
		private int m_entityid;
		private int m_approverid;
		private int m_decision;
		private DateTime m_responsetimestamp;
		private string m_responsemessage;
     
        #endregion

        #region Public Properties
        public int confid{
            get { return m_confid; }
            set { m_confid = value; }
		}
        public int instanceid
        {
            get { return m_instanceid; }
            set { m_instanceid = value; }
        }
        public int uId
        {
            get { return m_uId; }
            set { m_uId = value; }
        }	
        public int entitytype
        {
            get { return m_entitytype; }
            set { m_entitytype = value; }
		}
        public int entityid{
            get { return m_entityid; }
            set { m_entityid = value; }
		}
        public int approverid{
            get { return m_approverid; }
            set { m_approverid = value; }
		}
        public int decision{
            get { return m_decision; }
            set { m_decision = value; }
		}
        public DateTime responsetimestamp{
            get { return m_responsetimestamp; }
            set { m_responsetimestamp = value; }
		}
        public string responsemessage{
            get { return m_responsemessage; }
            set { m_responsemessage = value; }
        }
     
        #endregion
        public vrmConfApproval()
        {
            responsetimestamp = timeZone.nullTime();
        }
    }

    //FB 2566 Start                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
    public class vrmConfBridge
    {
        #region Private Internal Members
        private int m_uId, m_ConfID, m_InstanceID, m_BridgeID, m_Synchronous; //FB 2441
        private int m_confuId, m_BridgeTypeid, m_TotalPortsUsed, m_E164Dialnumber; //FB 2636
        private string m_BridgeName, m_BridgeIPISDNAddress;
        string m_BridgeExtNo; //FB 2610
        #endregion

        #region Public Properties
        public int uId
       {
           get { return m_uId; }
           set { m_uId = value; }
		}
        public int ConfID
        {
            get { return m_ConfID; }
            set { m_ConfID = value; }
        }
        public int InstanceID
        {
            get { return m_InstanceID; }
            set { m_InstanceID = value; }
        }
        public int BridgeID
       {
           get { return m_BridgeID; }
           set { m_BridgeID = value; }
       }
       public int confuId
       {
           get { return m_confuId; }
           set { m_confuId = value; }
       }
       public int BridgeTypeid
       {
           get { return m_BridgeTypeid; }
           set { m_BridgeTypeid = value; }
       }
       public int TotalPortsUsed
       {
           get { return m_TotalPortsUsed; }
           set { m_TotalPortsUsed = value; }
       }
       public string BridgeName
       {
           get { return m_BridgeName; }
           set { m_BridgeName = value; }
       }
       public string BridgeIPISDNAddress
       {
           get { return m_BridgeIPISDNAddress; }
           set { m_BridgeIPISDNAddress = value; }
       }

       public string BridgeExtNo //FB 2610
       {
           get { return m_BridgeExtNo; }
           set { m_BridgeExtNo = value; }

       }
       public int E164Dialnumber
       {
           get { return m_E164Dialnumber; }
           set { m_E164Dialnumber = value; }

       }
       public int Synchronous //FB 2441
       {
           get { return m_Synchronous; }
           set { m_Synchronous = value; }
       }
        #endregion
    }
    //FB 2566 end

    public class vrmConfCascade
    {
        #region Private Internal Members
        private int m_uId, m_cascadeLinkId, m_confid, m_instanceid;
        private string m_cascadelinkname;
        private int m_masterOrSlave;
        private int m_videoProtocolID;
        private int m_defVideoProtocol;
        private int m_connectionType;
        private string m_ipisdnAddress;
        private int m_bridgeId;
        private string m_bridgeipisdnAddress;
        private int m_connectStatus;
        private int m_outsidenetwork;
        private string m_mcuservicename;
        private int m_audioOrVideo;
        private int m_deflinerate;
        private int m_mute;
        private int m_addresstype;
        private int m_bridgeAddressType;
        private int m_layout;
        private string m_prefix;
        private int m_OnlineStatus; //FB 1650 Endpoint Status Issue
        private DateTime m_LastRunDateTime = DateTime.Today; //FB 1650 Endpoint Status Issue//FB 2027(GetTerminalControl)
        private string m_remoteEndPointIP;//Blue Status Glowpoint...
        //FB 2501 Call Monitoring
        private string m_RxAudioPacketsReceived, m_RxAudioPacketErrors, m_RxAudioPacketsMissing, m_RxVideoPacketsReceived, m_RxVideoPacketErrors, m_RxVideoPacketsMissing;
        private int m_TerminalType;
        #endregion

        #region Public Properties
        public int cascadeLinkId
        {
            get { return m_cascadeLinkId; }
            set { m_cascadeLinkId = value; }
        }
        public int uId
        {
            get { return m_uId; }
            set { m_uId = value; }
        }
        public int confid
        {
            get { return m_confid; }
            set { m_confid = value; }
        }
        public int instanceid
        {
            get { return m_instanceid; }
            set { m_instanceid = value; }
        }
    
        public string cascadelinkname{
            get { return m_cascadelinkname; }
            set { m_cascadelinkname = value; }
        }
        public int masterOrSlave{
            get { return m_masterOrSlave; }
            set { m_masterOrSlave = value; }
        }
        public int videoProtocolID{
            get { return m_videoProtocolID; }
            set { m_videoProtocolID = value; }
        }
        public int defVideoProtocol{
            get { return m_defVideoProtocol; }
            set { m_defVideoProtocol = value; }
        }
        public int connectionType{
            get { return m_connectionType; }
            set { m_connectionType = value; }
        }
        public string ipisdnAddress{
            get { return m_ipisdnAddress; }
            set { m_ipisdnAddress = value; }
        }
        public int bridgeId{
            get { return m_bridgeId; }
            set { m_bridgeId = value; }
        }
        public string bridgeipisdnAddress{
            get { return m_bridgeipisdnAddress; }
            set { m_bridgeipisdnAddress = value; }
        }
        public int connectStatus{
            get { return m_connectStatus; }
            set { m_connectStatus = value; }
        }
        public int outsidenetwork{
            get { return m_outsidenetwork; }
            set { m_outsidenetwork = value; }
        }
        public string mcuservicename{
            get { return m_mcuservicename; }
            set { m_mcuservicename = value; }
        }
        public int audioOrVideo{
            get { return m_audioOrVideo; }
            set { m_audioOrVideo = value; }
        }
        public int deflinerate{
            get { return m_deflinerate; }
            set { m_deflinerate = value; }
        }
        public int mute{
            get { return m_mute; }
            set { m_mute = value; }
        }
        public int addresstype{
            get { return m_addresstype; }
            set { m_addresstype = value; }
        }
        public int bridgeAddressType{
            get { return m_bridgeAddressType; }
            set { m_bridgeAddressType = value; }
        }
        public int layout{
            get { return m_layout; }
            set { m_layout = value; }
        }
        public string prefix
        {
            get { return m_prefix; }
            set { m_prefix = value; }
        }
        public int OnlineStatus //FB 1650 Endpoint Status Issue
        {
            get { return m_OnlineStatus; }
            set { m_OnlineStatus = value; }
        }
        //Blue Status Glowpoint starts...
        public string remoteEndPointIP
        {
            get { return m_remoteEndPointIP; }
            set { m_remoteEndPointIP = value; }
        }
        //Blue Status Glowpoint ends...
        //FB 2501 Call Monitoring Start
        public string RxAudioPacketsReceived
        {
            get { return m_RxAudioPacketsReceived; }
            set { m_RxAudioPacketsReceived = value; }
        }
        public string RxAudioPacketErrors
        {
            get { return m_RxVideoPacketErrors; }
            set { m_RxVideoPacketErrors = value; }
        }
        public string RxAudioPacketsMissing
        {
            get { return m_RxAudioPacketsMissing; }
            set { m_RxAudioPacketsMissing = value; }
        }
        public string RxVideoPacketsReceived
        {
            get { return m_RxVideoPacketsReceived; }
            set { m_RxVideoPacketsReceived = value; }
        }
        public string RxVideoPacketErrors
        {
            get { return m_RxVideoPacketErrors; }
            set { m_RxVideoPacketErrors = value; }
        }
        public string RxVideoPacketsMissing
        {
            get { return m_RxVideoPacketsMissing; }
            set { m_RxVideoPacketsMissing = value; }
        }
        public int TerminalType 
        {
            get { return m_TerminalType; }
            set { m_TerminalType = value; }
        }
        
        //FB 2501 Call Monitoring End
        //public DateTime LastRunDateTime //FB 1650 Endpoint Status Issue
        //{
        //    get { return m_LastRunDateTime; }
        //    set { m_LastRunDateTime = value; }
        //}
        public DateTime LastRunDateTime //FB 1650 Endpoint Status Issue //FB 2027(GetTerminalControl)
        {
            get { return m_LastRunDateTime; }
            set
            {
                //FB 1939
                if (value.ToString().IndexOf("0001") > 0)
                    m_LastRunDateTime = DateTime.Today;
                else
                    m_LastRunDateTime = value;
            }
        }

        #endregion
    }
    public class vrmRecurInfo
    {
        #region Private Internal Members
        private int m_uId, m_confid, m_confuId;
       	private int m_timezoneid, m_duration, m_recurType, m_subType;
        private int m_yearMonth, m_dayno, m_gap;
		private DateTime m_startTime, m_endTime;
        private int m_endType, m_occurrence, m_dirty;
        private string m_pattern, m_days;

        private vrmConference m_Conf;
        #endregion

        #region Public Properties
        public int uId
        {
            get { return m_uId; }
            set { m_uId = value; }
        }
        public int confid
        {
            get { return m_confid; }
            set { m_confid = value; }
        }
        public int confuId
        {
            get { return m_confuId; }
            set { m_confuId = value; }
        }
        public int confnumname
        {
            get { return m_confuId; }
            set { m_confuId = value; }
        }
        public int timezoneid
        {
            get { return m_timezoneid; }
            set { m_timezoneid = value; }
        }
        public int duration
        {
            get { return m_duration; }
            set { m_duration = value; }
        }
        public int recurType
        {
            get { return m_recurType; }
            set { m_recurType = value; }
        }
        public int subType
        {
            get { return m_subType; }
            set { m_subType = value; }
        }
        public int yearMonth
        {
            get { return m_yearMonth; }
            set { m_yearMonth = value; }
        }
        public string days
        {
            get { return m_days; }
            set { m_days = value; }
        }
        public int dayno
        {
            get { return m_dayno; }
            set { m_dayno = value; }
        }
        public int gap
        {
            get { return m_gap; }
            set { m_gap = value; }
        }
        public DateTime startTime
        {
            get { return m_startTime; }
            set { m_startTime = value; }
        }
         public DateTime endTime
        {
            get { return m_endTime; }
            set { m_endTime = value; }
        }
        public int endType
        {
            get { return m_endType; }
            set { m_endType = value; }
        }
        public int occurrence
        {
            get { return m_occurrence; }
            set { m_occurrence = value; }
        }
        public int dirty
        {
            get { return m_dirty; }
            set { m_dirty = value; }
        }
        public string RecurringPattern
        {
            get { return m_pattern; }
            set { m_pattern = value; }
        }

        public vrmConference Conf
        {
            get { return m_Conf; }
            set { m_Conf = value; }
        }
        public vrmRecurInfo()
        {
        }
        #endregion
    }

    //FB 2218

    public class vrmRecurInfoDefunct
    {
        #region Private Internal Members
        private int m_uId, m_confid, m_confuId;
        private int m_timezoneid, m_duration, m_recurType, m_subType;
        private int m_yearMonth, m_dayno, m_gap;
        private DateTime m_startTime, m_endTime;
        private int m_endType, m_occurrence, m_dirty;
        private string m_pattern, m_days;
        private int m_setupDuration, m_teardownDuration;

        private vrmConference m_Conf;
        #endregion

        #region Public Properties
        public int uId
        {
            get { return m_uId; }
            set { m_uId = value; }
        }
        public int confid
        {
            get { return m_confid; }
            set { m_confid = value; }
        }
        public int confuId
        {
            get { return m_confuId; }
            set { m_confuId = value; }
        }
        public int confnumname
        {
            get { return m_confuId; }
            set { m_confuId = value; }
        }
        public int timezoneid
        {
            get { return m_timezoneid; }
            set { m_timezoneid = value; }
        }
        public int duration
        {
            get { return m_duration; }
            set { m_duration = value; }
        }
        public int recurType
        {
            get { return m_recurType; }
            set { m_recurType = value; }
        }
        public int subType
        {
            get { return m_subType; }
            set { m_subType = value; }
        }
        public int yearMonth
        {
            get { return m_yearMonth; }
            set { m_yearMonth = value; }
        }
        public string days
        {
            get { return m_days; }
            set { m_days = value; }
        }
        public int dayno
        {
            get { return m_dayno; }
            set { m_dayno = value; }
        }
        public int gap
        {
            get { return m_gap; }
            set { m_gap = value; }
        }
        public DateTime startTime
        {
            get { return m_startTime; }
            set { m_startTime = value; }
        }
        public DateTime endTime
        {
            get { return m_endTime; }
            set { m_endTime = value; }
        }
        public int endType
        {
            get { return m_endType; }
            set { m_endType = value; }
        }
        public int occurrence
        {
            get { return m_occurrence; }
            set { m_occurrence = value; }
        }
        public int dirty
        {
            get { return m_dirty; }
            set { m_dirty = value; }
        }
        public string RecurringPattern
        {
            get { return m_pattern; }
            set { m_pattern = value; }
        }
        public int SetupDuration
        {
            get { return m_setupDuration; }
            set { m_setupDuration = value; }
        }
        public int TeardownDuration
        {
            get { return m_teardownDuration; }
            set { m_teardownDuration = value; }
        }

        public vrmConference Conf
        {
            get { return m_Conf; }
            set { m_Conf = value; }
        }
        public vrmRecurInfoDefunct()
        {
        }
        #endregion
    }

    //FB 2218

    public class vrmConfBasicUser
    {
        #region Private Internal Members
        private int m_userid;
        private int m_status;
        private int m_invitee;
        private int m_roomID;
        private int m_defVideoProtocol;
        private string m_ipAddress;
        private int m_connectionType;
        private int m_partyNotify;
        private int m_audioOrVideo;
        private int m_connectStatus;
        private int m_interfaceType;
        private string m_IPISDNAddress;
        private int m_isLecturer;

        #endregion

        #region Public Properties
        
        public int userid
        {
            get { return m_userid; }
            set { m_userid = value; }
        }
       
        public int status
        {
            get { return m_status; }
            set { m_status = value; }
        }
        
        
        public int invitee
        {
            get { return m_invitee; }
            set { m_invitee = value; }
        }
        public int roomID
        {
            get { return m_roomID; }
            set { m_roomID = value; }
        }
        public int defVideoProtocol
        {
            get { return m_defVideoProtocol; }
            set { m_defVideoProtocol = value; }
        }
        public string ipAddress
        {
            get { return m_ipAddress; }
            set { m_ipAddress = value; }
        }
        public int connectionType
        {
            get { return m_connectionType; }
            set { m_connectionType = value; }
        }
       
        public int audioOrVideo
        {
            get { return m_audioOrVideo; }
            set { m_audioOrVideo = value; }
        }
        
        public string IPISDNAddress
        {
            get { return m_IPISDNAddress; }
            set { m_IPISDNAddress = value; }
        }
        
        public int connectStatus
        {
            get { return m_connectStatus; }
            set { m_connectStatus = value; }
        }
        
        public int partyNotify
        {
            get { return m_partyNotify; }
            set { m_partyNotify = value; }
        }
        public int interfaceType
        {
            get { return m_interfaceType; }
            set { m_interfaceType = value; }
        }
        public int isLecturer
        {
            get { return m_isLecturer; }
            set { m_isLecturer = value; }
        }

        #endregion
    }
    public class vrmConfBasicEntity
    {
        #region Private Internal Members
        private int m_uId, m_confid, m_instanceid;
        private int m_confnumname;
        private int m_RoomId;
		private int m_defLineRate;
		private int m_defVideoProtocol;
        private string m_ipAddress;
		private int m_connect2;
		private string m_bridgeIPISDNAddress;
		private int m_bridgeid;
        private int m_addressType;
		private int m_bridgeAddressType;
        private int m_mute;
        private int m_outsideNetwork;
        private int m_connectionType;
        private int m_connectStatus;
        private int m_layout;
        private string m_ipisdnaddress;
        private string m_mcuServiceName;
        private string m_prefix;
        private int m_confuId;
        private int m_isLecturer;

        #endregion
		
		#region Public Properties
    
        public int uId
        {
            get { return m_uId; }
            set { m_uId = value; }
        }
        public int confuId
        {
            get { return m_confuId; }
            set { m_confuId = value; }
        }

        public int confid
        {
            get { return m_confid; }
            set { m_confid = value; }
        }
        public int instanceid
        {
            get { return m_instanceid; }
            set { m_instanceid = value; }
        }

        public int confnumname
        {
            get { return m_confnumname; }
            set { m_confnumname = value; }
        }
	
		public int roomId 
		{
			get {return m_RoomId;}
			set {m_RoomId = value;}
		}		
		public int defLineRate 
		{
			get {return m_defLineRate;}
			set {m_defLineRate = value;}
		}		
		public int defVideoProtocol 
		{
			get {return m_defVideoProtocol;}
			set {m_defVideoProtocol = value;}
		}			
		public int connect2 
		{
			get {return m_connect2;}
			set {m_connect2 = value;}
		}	
		public string bridgeIPISDNAddress 
		{
			get {return m_bridgeIPISDNAddress;}
			set {m_bridgeIPISDNAddress = value;}
		}			
		public int bridgeid 
		{
			get {return m_bridgeid;}
			set {m_bridgeid = value;}
		}	
		public int connectionType 
		{
			get {return m_connectionType;}
			set {m_connectionType = value;}
		}
        public string ipisdnaddress 
		{
            get { return m_ipisdnaddress; }
            set { m_ipisdnaddress = value; }
		}
        public string ipAddress
        {
            get { return m_ipAddress; }
            set { m_ipAddress = value; }
        }
        public int connectStatus 
		{
			get {return m_connectStatus;}
			set {m_connectStatus = value;}
		}	
		public int outsideNetwork 
		{
			get {return m_outsideNetwork;}
			set {m_outsideNetwork = value;}
		}	
		public string mcuServiceName 
		{
			get {return m_mcuServiceName;}
			set {m_mcuServiceName = value;}
		}	
    
        public int mute 
		{
			get {return m_mute;}
			set {m_mute = value;}
		}	
		public int addressType 
		{
			get {return m_addressType;}
			set {m_addressType = value;}
		}	
		public int bridgeAddressType 
		{
			get {return m_bridgeAddressType;}
			set {m_bridgeAddressType = value;}
		}	
		public int layout 
		{
			get {return m_layout;}
			set {m_layout = value;}
		}
        public string prefix
        {
            get { return m_prefix; }
            set { m_prefix = value; }
        }
        public int isLecturer
        {
            get { return m_isLecturer; }
            set { m_isLecturer = value; }
        }
 
        #endregion
		
    }	  	
	public class vrmConfRoom : vrmConfBasicEntity
	{
		#region Private Internal Members
		private DateTime m_StartDate;
		private int m_Duration;
        private int m_audioorvideo;
        private int m_endpointId;
        private int m_profileId;
        private int m_OnlineStatus; //FB 1650 Endpoint Status Issue
        private DateTime m_LastRunDateTime; //FB 1650 Endpoint Status Issue
        private int m_ApiPortNo;//API Port...
        private vrmConference m_Conf;
        private vrmRoom m_Room;
        private string m_endptURL, m_MultiCodecAddress ; //FB 2400
        private string m_remoteEndPointIP;//Blue point status...
        private int m_disabled;//FB 1675
        private int m_Extroom, m_isTextMsg;//FB 2426  //FB 2486
        //FB 2501 Call monitoring
        private int m_MuteTxvideo, m_MuteRxaudio, m_MuteRxvideo, m_Setfocus, m_Message, m_camera, m_Packetloss, m_Record, m_LockUnLock;
        private string m_RxAudioPacketsReceived, m_RxAudioPacketErrors, m_RxAudioPacketsMissing, m_RxVideoPacketsReceived, m_RxVideoPacketErrors, m_RxVideoPacketsMissing;
        private string m_GUID;
        private string m_Stream;
        private int m_TerminalType;
        private string m_BridgeExtNo; //FB 2610
        private int m_Secured;//FB 2595
		#endregion
		
		#region Public Properties


		public DateTime StartDate 
		{
			get {return m_StartDate;}
			set {m_StartDate = value;}
		}			
		public int Duration 
		{
			get {return m_Duration;}
			set {m_Duration = value;}
		}	
		public int audioorvideo 
		{
			get {return m_audioorvideo;}
			set {m_audioorvideo = value;}
		}	
      
        public int endpointId
        {
            get { return m_endpointId; }
            set { m_endpointId = value; }
        }
      
        public int profileId
        {
            get { return m_profileId; }
            set { m_profileId = value; }
        }
        public int OnlineStatus //FB 1650 Endpoint Status Issue
        {
            get { return m_OnlineStatus; }
            set { m_OnlineStatus = value; }
        }
        public DateTime LastRunDateTime //FB 1650 Endpoint Status Issue
        {
            get { return m_LastRunDateTime; }
            set { m_LastRunDateTime = value; }
        }

        public vrmConference Conf
        {
            get { return m_Conf; }
            set { m_Conf = value; }
        }
        public vrmRoom Room
        {
            get { return m_Room; }
            set { m_Room = value; }
        }
        //API Port Starts..
        public int ApiPortNo
        {
            get { return m_ApiPortNo; }
            set { m_ApiPortNo = value; }
        }
        //API Port Ends..
        //During API Port Starts..
        public string endptURL
        {
            get { return m_endptURL; }
            set { m_endptURL = value; }
        }
        //During API Port Ends..
        public vrmConfRoom()
        {
            m_Conf = new vrmConference();
            m_Room = new vrmRoom();
            StartDate = timeZone.nullTime();
        }
        //Blue point status starts...
        public string remoteEndPointIP
        {
            get { return m_remoteEndPointIP; }
            set { m_remoteEndPointIP = value; }
        }
        //Blue point status ends...

        //FB 1675...
        public int disabled
        {
            get { return m_disabled; }
            set { m_disabled = value; }
        }
        //FB 1675...
        public string MultiCodecAddress //FB 2400
        {
            get { return m_MultiCodecAddress; }
            set { m_MultiCodecAddress = value; }
        }
        //FB 2426
        public int Extroom
        {
            get { return m_Extroom; }
            set { m_Extroom = value; }
        }
        //FB 2426
        public int isTextMsg //FB 2486
        {
            get { return m_isTextMsg; }
            set { m_isTextMsg = value; }
        }
        //FB 2501 Call Monitoring Start
        public int MuteRxaudio
        {
            get { return m_MuteRxaudio; }
            set { m_MuteRxaudio = value; }
        }
        public int MuteRxvideo
        {
            get { return m_MuteRxvideo; }
            set { m_MuteRxvideo = value; }
        }
        public int MuteTxvideo
        {
            get { return m_MuteTxvideo; }
            set { m_MuteTxvideo = value; }
        }
        public int Setfocus 
        {
            get { return m_Setfocus; }
            set { m_Setfocus = value; }
        }
        public int Message 
        {
            get { return m_Message; }
            set { m_Message = value; }
        }
        public int Camera
        {
            get { return m_camera; }
            set { m_camera = value; }
        }
        public int Packetloss 
        {
            get { return m_Packetloss; }
            set { m_Packetloss = value; }
        }
        public int LockUnLock 
        {
            get { return m_LockUnLock; }
            set { m_LockUnLock = value; }
        }
        public int Record
        {
            get { return m_Record; }
            set { m_Record = value; }
        }
        public string Stream
        {
            get { return m_Stream; }
            set { m_Stream = value; }
        }
        public string GUID
        {
            get { return m_GUID; }
            set { m_GUID = value; }
        }
        public string RxAudioPacketsReceived
        {
            get { return m_RxAudioPacketsReceived; }
            set { m_RxAudioPacketsReceived = value; }
        }
        public string RxAudioPacketErrors
        {
            get { return m_RxAudioPacketErrors; }
            set { m_RxAudioPacketErrors = value; }
        }
        public string RxAudioPacketsMissing
        {
            get { return m_RxAudioPacketsMissing; }
            set { m_RxAudioPacketsMissing = value; }
        }
        public string RxVideoPacketsReceived
        {
            get { return m_RxVideoPacketsReceived; }
            set { m_RxVideoPacketsReceived = value; }
        }
        public string RxVideoPacketErrors
        {
            get { return m_RxVideoPacketErrors; }
            set { m_RxVideoPacketErrors = value; }
        }
        public string RxVideoPacketsMissing
        {
            get { return m_RxVideoPacketsMissing; }
            set { m_RxVideoPacketsMissing = value; }
        }
        public int TerminalType
        {
            get { return m_TerminalType; }
            set { m_TerminalType = value; }
        }
        //FB 2501 Call Monitoring Start
        
        public string BridgeExtNo //FB 2610
        {
            get { return m_BridgeExtNo; }
            set { m_BridgeExtNo = value; }
        }

        public int Secured //FB 2595
        {
            get { return m_Secured; }
            set { m_Secured = value; }
        }
        
 	#endregion
	}
    public class vrmConfUser : vrmConfBasicEntity
    {
        #region Private Internal Members
       	private int m_cc, m_userid;
		private string m_reason;
        private DateTime m_responseDate;		
        private int m_emailReminder;
        private int m_IPISDN;
        private int m_videoEquipment;
        private int m_partyNotify;
        private int m_interfaceType;
        private int m_audioOrVideo;
        private int m_status;
        private int m_invitee;
        private string m_endptURL;
        private String m_ExchangeID; //Cisco Telepresence fix
        private int m_OnlineStatus; //FB 1650 Endpoint Status Issue
        private int m_ApiPortNo;//API Port..
        private DateTime m_LastRunDateTime = DateTime.Today; //FB 1650 Endpoint Status Issue//FB 2027(GetTerminalControl)
        private string m_remoteEndPointIP;//Blue point status...
        private int m_NotifyOnEdit; //FB 1830 Email Edit
        private int m_Survey, m_isTextMsg; //FB 2348 //FB 2486
        private vrmConference m_Conf;
        //FB 2501 Call monitoring
        private int m_MuteTxvideo, m_MuteRxaudio, m_MuteRxvideo, m_Setfocus, m_Message, m_camera, m_Packetloss, m_Record, m_LockUnLock;
        private string m_RxAudioPacketsReceived, m_RxAudioPacketErrors, m_RxAudioPacketsMissing, m_RxVideoPacketsReceived, m_RxVideoPacketErrors, m_RxVideoPacketsMissing;
        private string m_GUID;
        private string m_Stream;
        private int m_TerminalType;
        private int m_PublicVMRParty, m_Secured;//FB 2550 FB 2595
        #endregion

        #region Public Properties
        public int cc{
            get { return m_cc; }
            set { m_cc = value; }
        }
         public int userid{
            get { return m_userid; }
            set { m_userid = value; }
        }
       
        public string reason{
            get { return m_reason; }
            set { m_reason = value; }
        }
        public DateTime responseDate
        {
            get { return m_responseDate; }
            set { m_responseDate = value; }
        }
        public int IPISDN{
            get { return m_IPISDN; }
            set { m_IPISDN = value; }
        }
        public int emailReminder{
            get { return m_emailReminder; }
            set { m_emailReminder = value; }
        }
          public int partyNotify
        {
            get { return m_partyNotify; }
            set { m_partyNotify = value; }
        }
        public int interfaceType
        {
            get { return m_interfaceType; }
            set { m_interfaceType = value; }
        }
        public int audioOrVideo
        {
            get { return m_audioOrVideo; }
            set { m_audioOrVideo = value; }
        }
        
        public int status
        {
            get { return m_status; }
            set { m_status = value; }
        }
        public int invitee
        {
            get { return m_invitee; }
            set { m_invitee = value; }
        }
        public int videoEquipment
        {
            get { return m_videoEquipment; }
            set { m_videoEquipment = value; }
        }
        public string ExchangeID    //Cisco Telepresence fix
        {
            get { return m_ExchangeID; }
            set { m_ExchangeID = value; }
        }
        public string endptURL    
        {
            get { return m_endptURL; }
            set { m_endptURL = value; }
        }
        public int OnlineStatus //FB 1650 Endpoint Status Issue
        {
            get { return m_OnlineStatus; }
            set { m_OnlineStatus = value; }
        }
        //API Port Starts...
        public int ApiPortNo 
        {
            get { return m_ApiPortNo; }
            set { m_ApiPortNo = value; }
        }
        //API Port Ends...
        //public DateTime LastRunDateTime //FB 1650 Endpoint Status Issue//FB 2027(GetTerminalControl)
        //{
        //    get { return m_LastRunDateTime; }
        //    set { m_LastRunDateTime = value; }
        //}
        public DateTime LastRunDateTime //FB 1650 Endpoint Status Issue //FB 2027(GetTerminalControl)
        {
            get { return m_LastRunDateTime; }
            set
            {
                //FB 1939
                if (value.ToString().IndexOf("0001") > 0)
                    m_LastRunDateTime = DateTime.Today;
                else
                    m_LastRunDateTime = value;
            }
        }
        //Blue point status starts...
        public string remoteEndPointIP 
        {
            get { return m_remoteEndPointIP; }
            set { m_remoteEndPointIP = value; }
        }

        //Blue point status ends...
        public int NotifyOnEdit  //FB 1830 Email Edit
        {
            get { return m_NotifyOnEdit; }
            set { m_NotifyOnEdit = value; }
        }

        //FB 2348
        public int Survey
        {
            get { return m_Survey; }
            set { m_Survey = value; }
        }
        public int isTextMsg //FB 2486
        {
            get { return m_isTextMsg; }
            set { m_isTextMsg = value; }
        }
        //FB 2501 Call Monitoring Start
        public int MuteRxaudio
        {
            get { return m_MuteRxaudio; }
            set { m_MuteRxaudio = value; }
        }
        public int MuteRxvideo
        {
            get { return m_MuteRxvideo; }
            set { m_MuteRxvideo = value; }
        }
        public int MuteTxvideo
        {
            get { return m_MuteTxvideo; }
            set { m_MuteTxvideo = value; }
        }
        public int Setfocus
        {
            get { return m_Setfocus; }
            set { m_Setfocus = value; }
        }
        public int Message
        {
            get { return m_Message; }
            set { m_Message = value; }
        }
        public int Camera
        {
            get { return m_camera; }
            set { m_camera = value; }
        }
        public int Packetloss
        {
            get { return m_Packetloss; }
            set { m_Packetloss = value; }
        }
        public int LockUnLock
        {
            get { return m_LockUnLock; }
            set { m_LockUnLock = value; }
        }
        public int Record
        {
            get { return m_Record; }
            set { m_Record = value; }
        }
        public string Stream
        {
            get { return m_Stream; }
            set { m_Stream = value; }
        }
        public string GUID
        {
            get { return m_GUID; }
            set { m_GUID = value; }
        }
        public string RxAudioPacketsReceived
        {
            get { return m_RxAudioPacketsReceived; }
            set { m_RxAudioPacketsReceived = value; }
        }
        public string RxAudioPacketErrors
        {
            get { return m_RxAudioPacketErrors; }
            set { m_RxAudioPacketErrors = value; }
        }
        public string RxAudioPacketsMissing
        {
            get { return m_RxAudioPacketsMissing; }
            set { m_RxAudioPacketsMissing = value; }
        }
        public string RxVideoPacketsReceived
        {
            get { return m_RxVideoPacketsReceived; }
            set { m_RxVideoPacketsReceived = value; }
        }
        public string RxVideoPacketErrors
        {
            get { return m_RxVideoPacketErrors; }
            set { m_RxVideoPacketErrors = value; }
        }
        public string RxVideoPacketsMissing
        {
            get { return m_RxVideoPacketsMissing; }
            set { m_RxVideoPacketsMissing = value; }
        }
        public int TerminalType
        {
            get { return m_TerminalType; }
            set { m_TerminalType = value; }
        }
        //FB 2501 Call Monitoring Start
        public int PublicVMRParty //FB 2550
        {
            get { return m_PublicVMRParty; }
            set { m_PublicVMRParty = value; }
        }
        public int Secured //FB 2595
        {
            get { return m_Secured; }
            set { m_Secured = value; }
        }
        #endregion
        
        #region Private Relationships
    //    private vrmUser m_User;
        #endregion

        #region Public Relationships  
        //public vrmUser User
        //{
        //    get { return m_User; }
        //    set { m_User = value; }
        //}
        #endregion

        public vrmConference Conf
        {
            get { return m_Conf; }
            set { m_Conf = value; }
        }
   
        public vrmConfUser()
        {
            responseDate = timeZone.nullTime();
       //     User = new vrmUser();
            m_Conf = new vrmConference();
        }
    }
	public class vrmConf
	{
		#region Private Internal Members
		
		private string   m_externalname, m_password;
		private int      m_owner;
		private int      m_audio, m_videoprotocol;
		private int      m_videosession, m_linerate, m_duration;
		private string   m_description;
		private int      m_public, m_deleted, m_continous, m_transcoding;
		private string   m_deletereason;
		private int      m_advanced;
		private int      m_videolayout,	m_manualvideolayout, m_conftype, m_status, m_lecturemode;
		private string   m_lecturer;
		private int      m_dynamicinvite;
        private int      m_orgId;

		#endregion
		
		#region Public Properties
	
		public string externalname
		{
			get {return m_externalname;}
			set {m_externalname = value;}
		}	
		public string password
		{
			get {return m_password;}
			set {m_password = value;}
		}	
		public int owner
		{
			get {return m_owner;}
			set {m_owner = value;}
		}	
		public int audio
		{
			get {return m_audio;}
			set {m_audio = value;}
		}	
		public int videoprotocol
		{
            get { return m_videoprotocol; }
			set {m_videoprotocol = value;}
		}	
		public int videosession
		{
			get {return m_videosession;}
			set {m_videosession = value;}
		}	
		public int linerate
		{
			get {return m_linerate;}
			set {m_linerate = value;}
		}	
		public int duration
		{
			get {return m_duration;}
			set {m_duration = value;}
		}	
		public string description
		{
			get {return m_description;}
			set {m_description = value;}
		}	
		public int isPublic
		{
			get {return m_public;}
			set {m_public = value;}
		}	
		public int deleted
		{
			get {return m_deleted;}
			set {m_deleted = value;}
		}	
		public int continous
		{
			get {return m_continous;}
			set {m_continous = value;}
		}	
		public int transcoding
		{
			get {return m_transcoding;}
			set {m_transcoding = value;}
		}	
		public string deletereason
		{
			get {return m_deletereason;}
			set {m_deletereason = value;}
		}	
		public int advanced
		{
			get {return m_advanced;}
			set {m_advanced = value;}
		}	
		public int videolayout
		{
			get {return m_videolayout;}
			set {m_videolayout = value;}
		}	
		public int manualvideolayout
		{
			get {return m_manualvideolayout;}
			set {m_manualvideolayout = value;}
		}	
		public int conftype
		{
			get {return m_conftype;}
			set {m_conftype = value;}
		}	
		public int status
		{
			get {return m_status;}
			set {m_status = value;}
		}	
		public int lecturemode
		{
			get {return m_lecturemode;}
			set {m_lecturemode = value;}
		}	
		public string lecturer
		{
			get {return m_lecturer;}
			set {m_lecturer = value;}
		}	
		public int dynamicinvite
		{
			get {return m_dynamicinvite;}
			set {m_dynamicinvite = value;}
		}

        public int orgId
        {
            get { return m_orgId; }
            set { m_orgId = value; }
        }
		#endregion
        
    
        public vrmConf()
   		{
		}
	}
    public class vrmConference : vrmConf, ICloneable
    {
        #region Private Internal Members

        private int m_userid;
        private int m_confid, m_instanceid, m_confuId;
        private int m_confnumname;
        private DateTime m_confdate, m_conftime, m_confEnd, m_SetupTime, m_TearDownTime; //buffer zone
        private int m_timezone, m_immediate;
        private int m_recuring;
        private int m_totalpoints, m_connect2;
        private DateTime m_settingtime;
        private int m_CreateType, m_ConfDeptID;
        private string m_internalname;
        private DateTime m_LastRunDateTime; //DashBoard
        private IList<vrmMCU> m_mcuList;
        private string m_icalID;//FB 1782
        private int m_confMode; //FB 1830
        private int m_isvip,// FB 1864
                    m_isdedicatedengineer,
                    m_isliveassitant,
                    m_isReminder,
                    m_isVMR;//FB 2376
        private int m_serviceType,m_sentSurvey;//FB 2219 //FB 2348
        private string m_conceirgeSupport;//FB 2341
        //FB 2363 -Start
        private string m_ESId;
        private string m_ESType;
        private int m_PushedToExternal, m_isTextMsg; //FB 2486
        //FB 2363 -End        
		private int m_startmode; //FB 2501
		private int m_loginuser; //FB 2501
        private int m_confVNOC; //FB 2501
        private string m_GUID; //FB 2501 Call Monitoring
        private int m_MeetandGreet, m_OnSiteAVSupport, m_ConciergeMonitoring, m_DedicatedVNOCOperator, m_E164Dialing, m_H323Dialing; //FB 2632 //FB 2636
        private int m_Secured, m_NetworkSwitch;//FB 2595
        private string m_DialString, m_Etag; //FB 2441

        #region Added for SetConference
        private int m_conforigin;
        #endregion 

        #endregion

        #region Public Properties

        public int confnumname
        {
            get { return m_confnumname; }
            set { m_confnumname = value; }
        }
        public int userid
        {
            get { return m_userid; }
            set { m_userid = value; }
        }
        public int confid
        {
            get { return m_confid; }
            set { m_confid = value; }
        }
        public int instanceid
        {
            get { return m_instanceid; }
            set { m_instanceid = value; }
        }
        public DateTime confdate
        {
            get { return m_confdate; }
            set { m_confdate = value; }
        }
        public DateTime conftime
        {
            get { return m_conftime; }
            set { m_conftime = value; }
        }
        public DateTime confEnd
        {
            get { return m_confEnd; }
            set { m_confEnd = value; }
        }
        public int timezone
        {
            get { return m_timezone; }
            set { m_timezone = value; }
        }
        public int immediate
        {
            get { return m_immediate; }
            set { m_immediate = value; }
        }
        public int recuring
        {
            get { return m_recuring; }
            set { m_recuring = value; }
        }
        public int totalpoints
        {
            get { return m_totalpoints; }
            set { m_totalpoints = value; }
        }
        public int connect2
        {
            get { return m_connect2; }
            set { m_connect2 = value; }
        }
        public DateTime settingtime
        {
            get { return m_settingtime; }
            set { m_settingtime = value; }
        }
        
        public string internalname
        {
            get { return m_internalname; }
            set { m_internalname = value; }
        }
        public int CreateType
        {
            get { return m_CreateType; }
            set { m_CreateType = value; }
        }
        public int ConfDeptID
        {
            get { return m_ConfDeptID; }
            set { m_ConfDeptID = value; }
        }
        public int confuId
        {
            get { return m_confuId; }
            set { m_confuId = value; }
        }
        /* *** Code Added for Buffer Zone *** -- Start */
        public DateTime SetupTime
        {
            get { return m_SetupTime; }
            set { m_SetupTime = value; }
        }
        public DateTime TearDownTime
        {
            get { return m_TearDownTime; }
            set { m_TearDownTime = value; }
        }
        /* *** Code Added for Buffer Zone *** -- End */

        public DateTime LastRunDateTime //Dashboard
        {
            get { return m_LastRunDateTime; }
            set { m_LastRunDateTime = value; }
        }
        public string IcalID//FB 1782
        {
            get { return m_icalID; }
            set { m_icalID = value; }
        }
        public int ConfMode  //FB 1830
        {
            get { return m_confMode; }
            set { m_confMode = value; }
        }

        // FB 1864
        public int isVIP
        {
            get { return m_isvip; }
            set { m_isvip = value; }
        }
        //FB 2219
        public int ServiceType
        {
            get { return m_serviceType; }
            set { m_serviceType = value; }
        }
        public int isLiveAssistant
        {
            get { return m_isliveassitant; }
            set { m_isliveassitant = value; }
        }

        public int isDedicatedEngineer
        {
            get { return m_isdedicatedengineer; }
            set { m_isdedicatedengineer = value; }
        }

        // FB 1864

        //FB 1926
        public int isReminder
        {
            get { return m_isReminder; }
            set { m_isReminder = value; }
        }

        public string ConceirgeSupport//FB 2341
        {
            get { return m_conceirgeSupport; }
            set { m_conceirgeSupport = value; }
        }

        //FB 2376
        public int isVMR
        {
            get { return m_isVMR; }
            set { m_isVMR = value; }
        }
        
        public int sentSurvey //FB 2348
        {
            get { return m_sentSurvey; }
            set { m_sentSurvey = value; }
        }
		
		//FB 2363 - Start
        public string ESId
        {
            get { return m_ESId; }
            set { m_ESId = value; }
        }
        public string ESType
        {
            get { return m_ESType; }
            set { m_ESType = value; }
        }
        public int PushedToExternal
        {
            get { return m_PushedToExternal; }
            set { m_PushedToExternal = value; } 
        }
        //FB 2363 - End
        //FB 2486
         public int isTextMsg
        {
            get { return m_isTextMsg; }
            set { m_isTextMsg = value; } 
        }
         //FB 2501 Starts
         public int StartMode
         {
             get { return m_startmode; }
             set { m_startmode = value; }
         }
		 public int loginUser
         {
             get { return m_loginuser; }
             set { m_loginuser = value; }
         }
         public int confVNOC
         {
             get { return m_confVNOC; }
             set { m_confVNOC = value; }
         }
         //FB 2501 Ends
        //FB 2501 Call Monitoring
         public string GUID
         {
             get { return m_GUID; }
             set { m_GUID = value; }
         }
        //FB 2632 - Starts
         public int MeetandGreet
         {
             get { return m_MeetandGreet; }
             set { m_MeetandGreet = value; }
         }
         public int OnSiteAVSupport
         {
             get { return m_OnSiteAVSupport; }
             set { m_OnSiteAVSupport = value; }
         }
         public int ConciergeMonitoring
         {
             get { return m_ConciergeMonitoring; }
             set { m_ConciergeMonitoring = value; }
         }
         public int DedicatedVNOCOperator
         {
             get { return m_DedicatedVNOCOperator; }
             set { m_DedicatedVNOCOperator = value; }
         }
         //FB 2632 - End
         public int E164Dialing //FB 2636
         {
             get { return m_E164Dialing; }
             set { m_E164Dialing = value; }
         }
         public int H323Dialing //FB 2636
         {
             get { return m_H323Dialing; }
             set { m_H323Dialing = value; }
         }
		 //FB 2595
         public int Secured
         {
             get { return m_Secured; }
             set { m_Secured = value; }
         }
         public int NetworkSwitch //FB 2595
         {
             get { return m_NetworkSwitch; }
             set { m_NetworkSwitch = value; }
         }

         //FB 2441 Starts
         public string DialString
         {
             get { return m_DialString; }
             set { m_DialString = value; }
         }
         public string Etag 
         {
             get { return m_Etag; }
             set { m_Etag = value; }
         }
         //FB 2441 End
        #region Added for SetConference 
        public int ConfOrigin
        {
            get { return m_conforigin; }
            set { m_conforigin = value; }
        }
        #endregion 

        #endregion

        #region Private Relationships

        private IList<vrmConfAdvAvParams> m_AdvAvParams;
        private IList<vrmRecurInfo> m_RecurInfo;
        private IList<vrmRecurInfoDefunct> m_RecurInfoDefunct;//FB 2218
        private IList m_ConfApproval;
        private IList<vrmConfBridge> m_ConfBridge;//FB 2566 24 Dec,2012
        private IList<vrmConfUser> m_ConfUser;
        private IList m_ConfCascade;
        private IList<vrmConfRoom> m_ConfRoom;
        private IList m_ConfWorkOrder;
        private IList m_ConfAttribute;
       
        #endregion

        #region Public Relationships Properties

        /// <summary>
        /// This is an implementation of one-to-one mapping (which NHibernate does not support very well)
        /// basically a collection is used for the mapping but only one element is get/set
        /// </summary>
        public IList<vrmConfAdvAvParams> AdvAvParams
        {
            get { return m_AdvAvParams; }
            set { m_AdvAvParams = value; }
        }
        public vrmConfAdvAvParams ConfAdvAvParams
        {
            get { if(m_AdvAvParams.Count > 0) return m_AdvAvParams[0]; else return new vrmConfAdvAvParams(); }
            set { if (m_AdvAvParams.Count > 0) m_AdvAvParams[0] = value; else m_AdvAvParams.Add(value); }
        }
        public IList<vrmRecurInfo> RecurInfo
        {
            get { return m_RecurInfo; }
            set { m_RecurInfo = value; }
        }
        public vrmRecurInfo ConfRecurInfo
        {
            get { if (m_RecurInfo.Count > 0)return m_RecurInfo[0]; else return new vrmRecurInfo(); }
            set { if (m_RecurInfo.Count > 0)m_RecurInfo[0] = value; else m_RecurInfo.Add(value); }
        }
        //FB 2218 Start
        public IList<vrmRecurInfoDefunct> RecurInfoDefunct
        {
            get { return m_RecurInfoDefunct; }
            set { m_RecurInfoDefunct = value; }
        }

        public vrmRecurInfoDefunct ConfRecurInfoDefunct
        {
            get { if (m_RecurInfoDefunct.Count > 0)return m_RecurInfoDefunct[0]; else return new vrmRecurInfoDefunct(); }
            set { if (m_RecurInfoDefunct.Count > 0)m_RecurInfoDefunct[0] = value; else m_RecurInfoDefunct.Add(value); }
        }
        //FB 2218 End
        public IList<vrmConfBridge> ConfBridge //FB 2566 24 Dec,2012
        {
            get { return m_ConfBridge; }
            set { m_ConfBridge = value; }
        }
        public IList ConfApproval
        {
            get { return m_ConfApproval; }
            set { m_ConfApproval = value; }
        }
        public IList<vrmConfUser> ConfUser
        {
            get { return m_ConfUser; }
            set { m_ConfUser = value; }
        }
        public IList ConfCascade
        {
            get { return m_ConfCascade; }
            set { m_ConfCascade = value; }
        }
        public IList<vrmConfRoom> ConfRoom
        {
            get { return m_ConfRoom; }
            set { m_ConfRoom = value; }
        }
        public IList ConfWorkOrder
        {
            get { return m_ConfWorkOrder; }
            set { m_ConfWorkOrder = value; }
        }
        public IList<vrmMCU> mcuList
        {
            get { return m_mcuList; }
            set { m_mcuList = value; }
        }
        public IList ConfAttribute
        {
            get { return m_ConfAttribute; }
            set { m_ConfAttribute = value; }
        }
       

        #endregion
        
        public vrmConference()
        {
            m_confid = 0;
            m_instanceid = 0;
            m_confnumname = 0;
            m_conforigin = 0;

            m_AdvAvParams     = new List<vrmConfAdvAvParams>();
            m_ConfApproval    = new ArrayList();
            m_ConfBridge      = new List<vrmConfBridge>();//FB 2566 24 Dec,2012
            m_ConfUser        = new List<vrmConfUser>();
            m_RecurInfo       = new List<vrmRecurInfo>();
            m_RecurInfoDefunct = new List<vrmRecurInfoDefunct>();//FB 2218
            m_ConfCascade     = new ArrayList();
            m_ConfRoom        = new List<vrmConfRoom>();
            m_ConfWorkOrder   = new ArrayList();
            m_mcuList         = new List<vrmMCU>();
            m_ConfAttribute   = new ArrayList();
      
        }
        public object Clone() // ICloneable implementation
        {
            vrmConference cf = this.MemberwiseClone() as vrmConference;
            return cf;
        }
    }
    
    public class vrmRoomSplit
    {
        #region Private Internal Members
        private int m_uId, m_confid, m_instanceid;
        private int m_duration, m_roomId;
        private DateTime m_startTime;
        #endregion

        #region Public Properties
        public int uId  
        {
            get { return m_uId; }
            set { m_uId = value; }
        }
        public int confid
        {
            get { return m_confid; }
            set { m_confid = value; }
        }
        public int instanceid
        {
            get { return m_instanceid; }
            set { m_instanceid = value; }
        }
        public int duration
        {
            get { return m_duration; }
            set { m_duration = value; }
        }
        public int roomId
        {
            get { return m_roomId; }
            set { m_roomId = value; }
        }
        public DateTime startTime
        {
            get { return m_startTime; }
            set { m_startTime = value; }
        }
        #endregion

        public vrmRoomSplit()
        {
            startTime = timeZone.nullTime();
        }

    }
    public class vrmConfMonitor
	{

		#region Private Internal Members

        private int m_uId;
		private int m_confid;
		private int m_instanceid;
		private int m_endpointid;
		private int m_endpointtype;
		private DateTime m_timestamp; //FB 2027 SetTerminalCtrl
        private String m_endpointaddress;//FB 2027 SetTerminalCtrl
		private int m_bandwidth;
		private int m_status;

		#endregion
		
		#region Public Properties

        public int Uid //FB 2027 SetTerminalCtrl
		{
            get { return m_uId; }
            set { m_uId = value; }
		}	
		public int confid 
		{
			get {return m_confid;}
			set {m_confid = value;}
		}	
		public int instanceid
		{
			get {return m_instanceid;}
			set {m_instanceid = value;}
		}	
		public int endpointid 
		{
			get {return m_endpointid;}
			set {m_endpointid = value;}
		}	
		public int endpointtype 
		{
			get {return m_endpointtype;}
			set {m_endpointtype = value;}
		}
        public DateTime timestamp //FB 2027 SetTerminalCtrl
		{
			get {return m_timestamp;}
			set {m_timestamp = value;}
		}
        public String endpointaddress  //FB 2027 SetTerminalCtrl
		{
			get {return m_endpointaddress;}
			set {m_endpointaddress = value;}
		}	
		public int bandwidth 
		{
			get {return m_bandwidth;}
			set {m_bandwidth = value;}
		}	
		public int status 
		{
			get {return m_status;}
			set {m_status = value;}
		}	
	
		#endregion
	}
    public class vrmConfGroup
    {
        #region Private Internal Members

        private int m_uId, m_groupId, m_cc;

		#endregion
		
		#region Public Properties
        public int uId
        {
            get { return m_uId; }
            set { m_uId = value; }
        }
        public int groupId
        {
            get { return m_groupId; }
            set { m_groupId = value; }
        }
        public int cc
        {
            get { return m_cc; }
            set { m_cc = value; }
        }
       
        #endregion
    }
    public class vrmConfAttachments
    {

        #region Private Internal Members

        private int m_id;
        private int m_confid;
        private int m_instanceid;
        private string m_attachment;
     
        #endregion

        #region Public Properties

        public int id
        {
            get { return m_id; }
            set { m_id = value; }
        }
        public int confid
        {
            get { return m_confid; }
            set { m_confid = value; }
        }
        public int instanceid
        {
            get { return m_instanceid; }
            set { m_instanceid = value; }
        }
        public string attachment
        {
            get { return m_attachment; }
            set { m_attachment = value; }
        }
        
        #endregion
    }

    //custom attributes start
    public class vrmConfAttribute
    {
        #region Private Internal Members
        private int m_confAttrID;
        private int m_confid;
        private int m_instanceid;
        private int m_customAttributeId;
        private int m_selectedOptionId;
        private string m_selectedValue;
        #endregion

        #region Public Properties
        public int ConfAttrID
        {
            get { return m_confAttrID; }
            set { m_confAttrID = value; }
        }
        public int ConfId
        {
            get { return m_confid; }
            set { m_confid = value; }
        }
        public int InstanceId
        {
            get { return m_instanceid; }
            set { m_instanceid = value; }
        }
        public int CustomAttributeId
        {
            get { return m_customAttributeId; }
            set { m_customAttributeId = value; }
        }
        public int SelectedOptionId
        {
            get { return m_selectedOptionId; }
            set { m_selectedOptionId = value; }
        }
        public string SelectedValue
        {
            get { return m_selectedValue; }
            set { m_selectedValue = value; }
        }
        #endregion
        
    }
    //custom attributes end

    
    public enum LevelEntity
    {
        ROOM = 1, MCU, DEPT, SYSTEM, APPROVED
    }
    public enum status
    {
        UNDECIDED = 0, APPROVE, DECLINE
    }

    //FB 2486
    public class vrmConfMessage
    {
        #region Private Internal Members

        private int m_orgid, m_instanceid, m_confid, m_duration, m_uID, m_controlID, m_Languageid;
        string m_durationID, m_confMessage;

        #endregion

        #region Public Properties
        public int orgid
        {
            get { return m_orgid; }
            set { m_orgid = value; }
        }
        public int instanceid
        {
            get { return m_instanceid; }
            set { m_instanceid = value; }
        }
        public int confid
        {
            get { return m_confid; }
            set { m_confid = value; }
        }
        public string confMessage
        {
            get { return m_confMessage; }
            set { m_confMessage = value; }
        }
        public int duration
        {
            get { return m_duration; }
            set { m_duration = value; }
        }
        public int uID
        {
            get { return m_uID; }
            set { m_uID = value; }
        }
        public int controlID
        {
            get { return m_controlID; }
            set { m_controlID = value; }
        }
        public string durationID
        {
            get { return m_durationID; }
            set { m_durationID = value; }
        }
        public int Languageid
        {
            get { return m_Languageid; }
            set { m_Languageid = value; }
        }
       
        #endregion
    }

    //FB 2441
    public class vrmConfSyncMCUadjustments
    {
        #region Private Internal Members

        private int m_Confid,
                    m_instanceID,
                    m_deleteDecision, m_Confnumname, m_UID;
        private string m_esID;
        private string m_Etag;
        private string m_DialString;

        #endregion

        #region Public Properties

        public int UID
        {
            get { return m_UID; }
            set { m_UID = value; }
        }
        public int Confid
        {
            get { return m_Confid; }
            set { m_Confid = value; }
        }
        public int instanceID
        {
            get { return m_instanceID; }
            set { m_instanceID = value; }
        }
        public int Confnumname
        {
            get { return m_Confnumname; }
            set { m_Confnumname = value; }
        }
        public int deleteDecision
        {
            get { return m_deleteDecision; }
            set { m_deleteDecision = value; }
        }
        public string esID
        {
            get { return m_esID; }
            set { m_esID = value; }
        }
        public string Etag
        {
            get { return m_Etag; }
            set { m_Etag = value; }
        }
        public string DialString
        {
            get { return m_DialString; }
            set { m_DialString = value; }
        }


        #endregion
    }
}

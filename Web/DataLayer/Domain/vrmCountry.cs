using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Xml;

using NHibernate;
using NHibernate.Criterion;

using log4net;


namespace myVRM.DataLayer
{
    public class vrmCountry
    {
        #region Private Internal Members
        private int m_countryID, m_selected;
        private string m_countryName;
        private IList<vrmState> m_states;
        #endregion

        #region Public Properties
        public int CountryID
        {
            get { return m_countryID; }
            set { m_countryID = value; }
        }
        public string CountryName
        {
            get { return m_countryName; }
            set { m_countryName = value; }
        }
        public int Selected
        {
            get { return m_selected; }
            set { m_selected = value; }
        }
        public IList<vrmState> States
        {
            get { return m_states; }
            set { m_states = value; }
        }
        #endregion

        public vrmCountry()
        {
            m_states = new List<vrmState>();
        }
    }
}

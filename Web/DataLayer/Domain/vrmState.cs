using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Xml;

using NHibernate;
using NHibernate.Criterion;

using log4net;


namespace myVRM.DataLayer
{
    public class vrmState
    {
        #region Private Internal Members
        private int m_Id, m_selected,m_countryId;
        private string m_statecode,m_state;
        private vrmCountry m_country;
        #endregion

        #region Public Properties
        public int Id
        {
            get { return m_Id; }
            set { m_Id = value; }
        }
        public string StateCode
        {
            get { return m_statecode; }
            set { m_statecode = value; }
        }
        public string State
        {
            get { return m_state; }
            set { m_state = value; }
        }
        public int CountryID
        {
            get { return m_countryId; }
            set { m_countryId = value; }
        }
        public int Selected
        {
            get { return m_selected; }
            set { m_selected = value; }
        }
        public vrmCountry Country
        {
            get { return m_country; }
            set { m_country = value; }
        }
        #endregion

        public vrmState()
        {
            m_country = new vrmCountry();
        }


    }
}
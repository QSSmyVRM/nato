using System;
using System.Collections;

namespace myVRM.DataLayer
{
	/// <summary>
	/// Summary description(s) for Reports.
	/// </summary>
	class SQLQueryModify
	{
		public const int SQLQueryStringPreQuery    = 1;
		public const int SQLQueryStringPostQuery   = 2;
		public const int SQLQueryStringPreFilter   = 3;
		public const int SQLQueryStringSelectQuery = 4;
	}
	/// <summary>
	/// Summary description for Report ReportSchedule.
	/// </summary>
	public class ReportSchedule
	{
		#region Private Internal Members
	
		private int m_ID;
		private int m_userId;
		private int m_tID;
		private DateTime m_submitDate;
		private string m_Title;
		private string m_Description;
		private DateTime m_completeDate;
		private int m_timeZone;
		private string m_inXml;
		private int m_status;
		private string m_notifyEmail;
		private string m_report;
		private int m_deleted;

		#endregion
		
		#region Public Properties

		public int ID
		{
			get {return m_ID;}
			set {m_ID = value;}
		}
		public int userId
		{
			get {return m_userId;}
			set {m_userId = value;}
		}
		public int tID
		{
			get {return m_tID;}
			set {m_tID = value;}
		}
		public DateTime SubmitDate
		{
			get {return m_submitDate;}
			set {m_submitDate = value;}
		}
		public string reportTitle
		{
			get {return m_Title;}
			set {m_Title = value;}
		}
		public string reportDescription
		{
			get {return m_Description;}
			set {m_Description = value;}
		}		
		public DateTime CompleteDate
						
		{
			get {return m_completeDate;}
			set {m_completeDate = value;}
		}

		public int timeZone
		{
			get {return m_timeZone;}
			set {m_timeZone = value;}
		}
		public string inXml
		{
			get {return m_inXml;}
			set {m_inXml = value;}
		}		
		public int status
		{
			get {return m_status;}
			set {m_status = value;}
		}
		public string notifyEmail
		{
			get {return m_notifyEmail;}
			set {m_notifyEmail = value;}
		}		
		public string report
		{
			get 
			{
				if(m_report == null) return string.Empty;
				else return m_report;
			}
			set	
			{
				if(value == null) m_report  = string.Empty;
				else m_report  = value;
			}
		}
		public int deleted
		{
			get {return m_deleted;}
			set {m_deleted = value;}
		}
		
		#endregion		
		
		public ReportSchedule()
		{
		}
	}
	/// <summary>
	/// Summary description for Report Template.
	/// </summary>
	public class ReportTemplate
	{
		#region Private Internal Members
	
		private int m_ID;
		private int m_userId;
		private int m_InputID;
		private DateTime m_LastModified;
		private string m_Title;
		private string m_Description;
		private string m_query;
		private string m_tables;
		private string m_orderby;
		private string m_filter;
		private string m_outputformat;
		private string m_totalquery;
		private string m_outputTotal;
		private string m_columnHeadings;
		private int m_Public;
		private int m_deleted;

		#endregion
		
		#region Public Properties

		public int ID
		{
			get {return m_ID;}
			set {m_ID = value;}
		}
		public int userId
		{
			get {return m_userId;}
			set {m_userId = value;}
		}
		public int InputID
		{
			get {return m_InputID;}
			set {m_InputID = value;}
		}
		public DateTime LastModified
		{
			get {return m_LastModified;}
			set {m_LastModified = value;}
		}
		public string templateTitle
		{
			get {return m_Title;}
			set {m_Title = value;}
		}
		public string templateDescription
		{
			get {return m_Description;}
			set {m_Description = value;}
		}		
		public string query
		{
			get {return m_query;}
			set {m_query = value;}
		}		
		public string tables
		{
			get {return m_tables;}
			set {m_tables = value;}
		}
		public string orderby
		{
			get {return m_orderby;}
			set {m_orderby = value;}
		}		
		public string filter
		{
			get {return m_filter;}
			set {m_filter = value;}
		}
		public string outputformat
		{
			get {return m_outputformat;}
			set {m_outputformat = value;}
		}		
		public string totalquery
		{
			get {return m_totalquery;}
			set {m_totalquery = value;}
		}		
		public string outputTotal
		{
			get {return m_outputTotal;}
			set {m_outputTotal = value;}
		}
		public string columnHeadings
		{
			get {return m_columnHeadings;}
			set {m_columnHeadings = value;}
		}
		public int isPublic
		{
			get {return m_Public;}
			set {m_Public = value;}
		}
		public int deleted
		{
			get {return m_deleted;}
			set {m_deleted = value;}
		}
		
		#endregion		
		
		public ReportTemplate()
		{
		}
	}
	
	public class TokenObject
	{
		public string tokenName;
		public string tokenValue;
	}

	// just what we need for now
	public class VRMuser
	{
		public int    userId;
		public string firstName,lastName,email;
	}

	/// <summary>
	/// Summary description for Report Input.
	/// </summary>
	public class ReportInputItem
	{
		#region Private Internal Members
	
		private int m_inputID;
		private string m_inputClause;

		#endregion
		
		#region Public Properties

		public int inputID
		{
			get {return m_inputID;}
			set {m_inputID = value;}
		}
		public string inputClause
		{
			get {return m_inputClause;}
			set {m_inputClause = value;}
		}
		#endregion		
		
		public ReportInputItem()
		{
		}
	}

	public class InputItem
	{
		public int inputID;
		public int itemID;
		public string inputItem;
	}

    //FB 2343 Start
    public class ReportMonthlyDays
    {
        #region Private Internal Members

        private int m_UID;
        private int m_ID;
        private int m_OrgId;
        private string m_MonthName;
        private int m_MonthWorkingDays;
        private DateTime m_MonthStartDate;
        private DateTime m_MonthEndDate;
        private int m_CurrentYear;

        #endregion

        #region Public Properties

        public int UID
        {
            get { return m_UID; }
            set { m_UID = value; }
        }
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        public int OrgId
        {
            get { return m_OrgId; }
            set { m_OrgId = value; }
        }
        public string MonthName
        {
            get { return m_MonthName; }
            set { m_MonthName = value; }
        }
        public int MonthWorkingDays
        {
            get { return m_MonthWorkingDays; }
            set { m_MonthWorkingDays = value; }
        }
        public DateTime MonthStartDate
        {
            get { return m_MonthStartDate; }
            set { m_MonthStartDate = value; }
        }
        public DateTime MonthEndDate
        {
            get { return m_MonthEndDate; }
            set { m_MonthEndDate = value; }
        }
        public int CurrentYear
        {
            get { return m_CurrentYear; }
            set { m_CurrentYear = value; }
        }
        #endregion

        public ReportMonthlyDays() //FBDoubt
        {
        }
    }

    public class ReportWeeklyDays
    {
        #region Private Internal Members

        private int m_UID;
        private int m_ID;
        private int m_OrgId;
        private string m_WeekNumber;
        private int m_WeekWorkingDays;
        private DateTime m_WeekStartDate;
        private DateTime m_WeekEndDate;
        private int m_CurrentYear;

        #endregion

        #region Public Properties

        public int UID
        {
            get { return m_UID; }
            set { m_UID = value; }
        }
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        public int OrgId
        {
            get { return m_OrgId; }
            set { m_OrgId = value; }
        }
        public string WeekNumber
        {
            get { return m_WeekNumber; }
            set { m_WeekNumber = value; }
        }
        public int WeekWorkingDays
        {
            get { return m_WeekWorkingDays; }
            set { m_WeekWorkingDays = value; }
        }
        public DateTime WeekStartDate
        {
            get { return m_WeekStartDate; }
            set { m_WeekStartDate = value; }
        }
        public DateTime WeekEndDate
        {
            get { return m_WeekEndDate; }
            set { m_WeekEndDate = value; }
        }
        public int CurrentYear
        {
            get { return m_CurrentYear; }
            set { m_CurrentYear = value; }
        }
        #endregion

        public ReportWeeklyDays()
        {
        }
    }
    //FB 2343 End
}

using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.ComponentModel;
using System.Xml;

using NHibernate;
using NHibernate.Criterion;

using log4net;

namespace myVRM.DataLayer
{
    #region sysData
    /// <summary>
	/// Summary description for system settings.
	/// (this is a static class as system settings dont change);
	/// </summary>
	public class sysData
	{
        /* ** This class is modified as per Organization Module ** */

		#region Private Internal Members

        public static readonly int SUPER_ADMIN_ID = 11;
        public static readonly int PHANTOM_ROOM_ID = 11;

		private DateTime m_LastModified;
		private int m_Admin;
		private string m_SecurityKey;
        private int m_superAdmin, m_TimeZone;
		private string m_foodsecuritykey, m_resourcesecuritykey;
		private string m_emaildisclaimer, m_license;
        private int m_LastModifiedUser;
		private DateTime m_ExpiryDate;
        private int m_RoomLimit, m_MCULimit, m_UserLimit, m_MaxGuests, m_ExtRoomLimit, m_MCUEnchancedLimit, m_Cloud;//FB 2426 //FB 2486//FB 2426 //FB 2262 //FB 2599 
					//m_MaxGroups,m_MaxTemplates, - Depricated as per new license

        private List<String> m_CpuID; //Activation module
        private List<String> m_MacId;
        private int m_IsDemo;
        private DateTime m_Activated;
        //System LOGO..
        private int m_SiteLogoId;
        private string m_Companymessage;
        private int m_StdBannerId;
        private int m_HighBannerId;
        //Merging for License START
        private int m_MaxOrganizations, m_IsLDAP, m_MaxNVidRooms, m_MaxVidRooms, m_EnablePublicRooms, m_MaxVMRooms, //FB 2594 //FB 2586
                    m_MaxEndPts, m_MaxCDR, m_MaxGstPerUsrs,
                    m_MaxExcUsrs, m_MaxDomUsrs,m_MaxMobUsrs, m_MaxFacilities, m_MaxAPIs, m_MaxCatering, //FB 1979
                    m_MaxHousekeeping,m_MaxPCModule, //FB 2347
                    m_MaxSpanish, m_MaxMandarin, m_MaxHindi, m_MaxFrench;
        //Merging for License END
        private int m_LaunchBuffer;//FB 2007
        private int m_EnableLaunchBufferP2P; //FB 2437
        private int m_StartMode; //FB 2501
        private string m_EM7URI, m_EM7Username, m_EM7Password;//FB 2501 EM7
        private int m_EM7Port; //FB 2501 EM7
		#endregion

		#region Public Properties

		public DateTime LastModified
		{
			get {return m_LastModified;}
			set {m_LastModified = value;}
		}
		public int Admin
		{
			get {return m_Admin;}
			set {m_Admin = value;}
		}	
		public string SecurityKey
		{
			get {return m_SecurityKey;}
			set {m_SecurityKey = value;}	
		}	
		public int superAdmin 
		{
			get {return m_superAdmin;}
			set {m_superAdmin = value;}
		}
        public int TimeZone 
		{
            get { return m_TimeZone; }
            set { m_TimeZone = value; }
		}
		public string foodsecuritykey 
		{
			get {return m_foodsecuritykey;}
			set {m_foodsecuritykey = value;}
		}
		public string resourcesecuritykey 
		{
			get {return m_resourcesecuritykey;}
			set {m_resourcesecuritykey = value;}
		}
		public string emaildisclaimer
		{
			get {return m_emaildisclaimer;}
			set {m_emaildisclaimer = value;}
		}
		public string license
		{
			get {return m_license;}
			set {m_license = value;}
		}
        public int LastModifiedUser
		{
            get { return m_LastModifiedUser; }
            set { m_LastModifiedUser = value; }
		}	
		public  DateTime ExpiryDate
		{
			get {return m_ExpiryDate;}
			set {m_ExpiryDate = value;}
		}
		public int RoomLimit
		{
			get {return m_RoomLimit;}
			set {m_RoomLimit = value;}
		}
		public int MCULimit 
		{
			get {return m_MCULimit;}
			set {m_MCULimit = value;}
		}
        public int MCUEnchancedLimit //FB 2486
        {
            get { return m_MCUEnchancedLimit; }
            set { m_MCUEnchancedLimit = value; }
        }
        
		public int UserLimit 
		{
			get {return m_UserLimit;}
			set {m_UserLimit = value;}
		}
        public int ExtRoomLimit //FB 2426
        {
            get { return m_ExtRoomLimit; }
            set { m_ExtRoomLimit = value; }
        }
        //FB 2599 Start
        public int Cloud //FB 2262 - J
        {
            get { return m_Cloud; }
            set { m_Cloud = value; }

        }
        //FB 2599 End
        //System LOGO..
        public int SiteLogoId
        {
            get { return m_SiteLogoId; }
            set { m_SiteLogoId = value; }
        }
        public string Companymessage
        {
            get { return m_Companymessage; }
            set { m_Companymessage = value; }
        }
        public int StdBannerId
        {
            get { return m_StdBannerId; }
            set { m_StdBannerId = value; }
        }
        public int HighBannerId
        {
            get { return m_HighBannerId; }
            set { m_HighBannerId = value; }
        }
        //FB 2007
        public int LaunchBuffer
        {
            get { return m_LaunchBuffer; }
            set { m_LaunchBuffer = value; }
        }
        //FB 2007
        public int EnableLaunchBufferP2P //FB 2437
        {
            get { return m_EnableLaunchBufferP2P; }
            set { m_EnableLaunchBufferP2P = value; }
        }
        //FB 2501 starts
        public int StartMode
        {
            get { return m_StartMode; }
            set { m_StartMode = value; }
        }
        //FB 2501 EM7 Starts
        public string EM7URI
        {
            get { return m_EM7URI; }
            set { m_EM7URI = value; }
        }
        public string EM7Username
        {
            get { return m_EM7Username; }
            set { m_EM7Username = value; }
        }
        public string EM7Password
        {
            get { return m_EM7Password; }
            set { m_EM7Password = value; }
        }
         public int EM7Port
        {
            get { return m_EM7Port; }
            set { m_EM7Port = value; }
        }
        //FB 2501 EM7 Ends
        //public int MaxGroups - Depricated as per new license
        //{
        //    get {return m_MaxGroups;}
        //    set {m_MaxGroups = value;}
        //}
        //public int MaxTemplates
        //{
        //    get {return m_MaxTemplates;}
        //    set {m_MaxTemplates = value;}
        //}
		public int MaxGuests
		{
			get {return m_MaxGuests;}
			set {m_MaxGuests = value;}
		}
        /** Code added for Activation -- Start **/

        public List<String> CPUID
        {
            get { return m_CpuID; }
            set { m_CpuID = value; }
        }
        public List<String> MacID
        {
            get { return m_MacId; }
            set { m_MacId = value; }
        }

        public Int32 IsDemo
        {
            get { return m_IsDemo; }
            set { m_IsDemo = value; }
        }

        public DateTime ActivatedDate
        {
            get { return m_Activated; }
            set { m_Activated = value; }
        }

        /** Code added for Activation -- End **/
        //License modification START
        public int MaxOrganizations
        {
            get { return m_MaxOrganizations; }
            set { m_MaxOrganizations = value; }
        }
        public int EnablePublicRooms //FB 2594
        {
            get { return m_EnablePublicRooms; }
            set { m_EnablePublicRooms = value; }
        }
        public int IsLDAP
        {
            get { return m_IsLDAP; }
            set { m_IsLDAP = value; }
        }
        public int MaxNVidRooms
        {
            get { return m_MaxNVidRooms; }
            set { m_MaxNVidRooms = value; }
        }
        public int MaxVidRooms
        {
            get { return m_MaxVidRooms; }
            set { m_MaxVidRooms = value; }
        }
        //FB 2586 Start
        public int MaxVMRooms
        {
            get { return m_MaxVMRooms; }
            set { m_MaxVMRooms = value; }
        }
        //FB 2586 End
        public int MaxEndPts
        {
            get { return m_MaxEndPts; }
            set { m_MaxEndPts = value; }
        }
        public int MaxCDR
        {
            get { return m_MaxCDR; }
            set { m_MaxCDR = value; }
        }
        public int MaxGstPerUsrs
        {
            get { return m_MaxGstPerUsrs; }
            set { m_MaxGstPerUsrs = value; }
        }
        public int MaxExcUsrs
        {
            get { return m_MaxExcUsrs; }
            set { m_MaxExcUsrs = value; }
        }
        public int MaxDomUsrs
        {
            get { return m_MaxDomUsrs; }
            set { m_MaxDomUsrs = value; }
        }
        public int MaxMobUsrs //FB 1979
        {
            get { return m_MaxMobUsrs; }
            set { m_MaxMobUsrs = value; }
        }
        public int MaxFacilities
        {
            get { return m_MaxFacilities; }
            set { m_MaxFacilities = value; }
        }
        public int MaxCatering
        {
            get { return m_MaxCatering; }
            set { m_MaxCatering = value; }
        }
        public int MaxHousekeeping
        {
            get { return m_MaxHousekeeping; }
            set { m_MaxHousekeeping = value; }
        }
        public int MaxAPIs
        {
            get { return m_MaxAPIs; }
            set { m_MaxAPIs = value; }
        }
        public int MaxSpanish
        {
            get { return m_MaxSpanish; }
            set { m_MaxSpanish = value; }
        }
        public int MaxMandarin
        {
            get { return m_MaxMandarin; }
            set { m_MaxMandarin = value; }
        }
        public int MaxHindi
        {
            get { return m_MaxHindi; }
            set { m_MaxHindi = value; }
        }
        public int MaxFrench
        {
            get { return m_MaxFrench; }
            set { m_MaxFrench = value; }
        }
        public int MaxPCModule //FB 2347
        {
            get { return m_MaxPCModule; }
            set { m_MaxPCModule = value; }
        }
        //License modification END
		#endregion	
    }
    #endregion

    #region sysSettings

    public class sysSettings 
	{
		private static string m_configPath;
		private static sysData m_sysData;
      
		public sysSettings(string configPath)
		{


            try
            {

                m_configPath = configPath;
                m_sysData = new sysData();
                Init(m_configPath);
            }
            catch (Exception e)
            {
                throw e;
            }
		}
		
		public static bool Init(string configPath)
		{
			try
			{              
                SessionManagement.GetSession(configPath);
                ISession session = SessionManagement.GetSession(configPath);
                m_sysData = (sysData)session.Load(typeof(sysData), 11);

                ParseLicenseXML();
            }
			catch (Exception e)
			{
				throw e;
			}
			return true;
        }

        #region ParseLicenseXML
        /// <summary>
        /// ParseLicenseXML
        /// </summary>
        private static void ParseLicenseXML()
        {
            // Load the string into xml parser and retrieve the tag values.
            XmlDocument xd = new XmlDocument();
            cryptography.Crypto crypto = new cryptography.Crypto();
            string templicense = crypto.decrypt(license);

            xd.LoadXml(templicense);
            XmlNode node;

            node = xd.SelectSingleNode("//myVRMSiteLicense/Site/ExpirationDate");
            string ExpirationDate = node.InnerXml.Trim();
            m_sysData.ExpiryDate = DateTime.Parse(ExpirationDate, CultureInfo.InvariantCulture);  // FB 1774

            node = xd.SelectSingleNode("//myVRMSiteLicense/Site/MaxOrganizations");
            m_sysData.MaxOrganizations = Int32.Parse(node.InnerXml.Trim());

            node = xd.SelectSingleNode("//myVRMSiteLicense/Site/Cloud");//FB 2599
            int maxCloud = 0;
            if (node != null)
                int.TryParse(node.InnerXml.Trim(), out maxCloud);
            m_sysData.Cloud = maxCloud;

            node = xd.SelectSingleNode("//myVRMSiteLicense/Site/PublicRoomService"); //FB 2594
            int enablePublicRooms = 0;
            if (node != null)
                int.TryParse(node.InnerXml.Trim(), out enablePublicRooms);
            m_sysData.EnablePublicRooms = enablePublicRooms;

            node = xd.SelectSingleNode("//myVRMSiteLicense/Site/IsLDAP");
            //if (node.InnerText.Trim().ToLower() == "enabled")
            if (node.InnerText.Trim().Equals("1"))
                m_sysData.IsLDAP = 1;
            else
                m_sysData.IsLDAP = 0;

            /** Code added for Activation -- Start **/

            
            node = xd.SelectSingleNode("//myVRMSiteLicense/Site/ServerActivation/ProcessorIDs");

            m_sysData.CPUID = new List<string>();

            if (node != null)
            {

                XmlNodeList nodes = node.SelectNodes("ID");

                if (nodes != null)
                {


                    foreach (XmlNode nde in nodes)
                        m_sysData.CPUID.Add(nde.InnerText);
                }
            }


            node = xd.SelectSingleNode("//myVRMSiteLicense/Site/ServerActivation/MACAddresses");

            m_sysData.MacID = new List<string>();

            if (node != null)
            {
                XmlNodeList nodes = node.SelectNodes("ID");

                if (nodes != null)
                {


                    foreach (XmlNode nde in nodes)
                        m_sysData.MacID.Add(nde.InnerText);
                }
            }

            /** Code added for Activation -- End **/

            node = xd.SelectSingleNode("//myVRMSiteLicense/Organizations/Rooms/MaxNonVideoRooms");
            int maxnvrm = 0;
            Int32.TryParse(node.InnerXml.Trim(), out maxnvrm);
            m_sysData.MaxNVidRooms = maxnvrm;

            node = xd.SelectSingleNode("//myVRMSiteLicense/Organizations/Rooms/MaxVideoRooms");
            int maxvrm = 0;
            Int32.TryParse(node.InnerXml.Trim(), out maxvrm);
            m_sysData.MaxVidRooms = maxvrm;

            node = xd.SelectSingleNode("//myVRMSiteLicense/Organizations/Rooms/MaxVMRRooms");//FB 2586
            int maxvmr = 0;
            Int32.TryParse(node.InnerXml.Trim(), out maxvmr);
            m_sysData.MaxVMRooms = maxvmr;

            m_sysData.RoomLimit = m_sysData.MaxNVidRooms + m_sysData.MaxVidRooms + m_sysData.MaxVMRooms;//FB 2586

            node = xd.SelectSingleNode("//myVRMSiteLicense/Organizations/Rooms/MaxGuestRooms"); //FB 2426
            int maxextrooms = 0;
            Int32.TryParse(node.InnerXml.Trim(), out maxextrooms);
            m_sysData.ExtRoomLimit = maxextrooms;

            node = xd.SelectSingleNode("//myVRMSiteLicense/Organizations/Hardware/MaxStandardMCUs"); 
            int maxmcu = 0;
            Int32.TryParse(node.InnerXml.Trim(), out maxmcu);
            m_sysData.MCULimit = maxmcu;

            node = xd.SelectSingleNode("//myVRMSiteLicense/Organizations/Hardware/MaxEnhancedMCUs");//FB 2486
            int maxenchamcu = 0;
            Int32.TryParse(node.InnerXml.Trim(), out maxenchamcu);
            m_sysData.MCUEnchancedLimit = maxenchamcu;

            node = xd.SelectSingleNode("//myVRMSiteLicense/Organizations/Hardware/MaxEndpoints");
            int maxep = 0;
            Int32.TryParse(node.InnerXml.Trim(), out maxep);
            m_sysData.MaxEndPts = maxep;

            node = xd.SelectSingleNode("//myVRMSiteLicense/Organizations/Hardware/MaxCDRs");
            int maxcdr = 0;
            Int32.TryParse(node.InnerXml.Trim(), out maxcdr);
            m_sysData.MaxCDR = maxcdr;

            node = xd.SelectSingleNode("//myVRMSiteLicense/Organizations/Users/MaxTotalUsers");
            int maxusers = 0;
            Int32.TryParse(node.InnerXml.Trim(), out maxusers);
            m_sysData.UserLimit = maxusers;
                       
            node = xd.SelectSingleNode("//myVRMSiteLicense/Organizations/Users/MaxGuestsPerUser");
            int maxGuest = 0;
            Int32.TryParse(node.InnerXml.Trim(), out maxGuest);
            m_sysData.MaxGstPerUsrs = maxGuest;

            node = xd.SelectSingleNode("//myVRMSiteLicense/Organizations/Users/MaxExchangeUsers");
            int maxexusr = 0;
            Int32.TryParse(node.InnerXml.Trim(), out maxexusr);
            m_sysData.MaxExcUsrs = maxexusr;

            node = xd.SelectSingleNode("//myVRMSiteLicense/Organizations/Users/MaxDominoUsers");
            int maxDominousr = 0;
            Int32.TryParse(node.InnerXml.Trim(), out maxDominousr);
            m_sysData.MaxDomUsrs = maxDominousr;

            node = xd.SelectSingleNode("//myVRMSiteLicense/Organizations/Users/MaxMobileUsers"); //FB 1979
            int maxMobileusr = 0;
            Int32.TryParse(node.InnerXml.Trim(), out maxMobileusr);
            m_sysData.MaxMobUsrs = maxMobileusr;

            node = xd.SelectSingleNode("//myVRMSiteLicense/Organizations/Modules/MaxFacilitiesModules");
            int maxav = 0;
            Int32.TryParse(node.InnerXml.Trim(), out maxav);
            m_sysData.MaxFacilities = maxav;

            node = xd.SelectSingleNode("//myVRMSiteLicense/Organizations/Modules/MaxAPIModules");
            int maxapi = 0;
            Int32.TryParse(node.InnerXml.Trim(), out maxapi);
            m_sysData.MaxAPIs = maxapi;
            
            node = xd.SelectSingleNode("//myVRMSiteLicense/Organizations/Modules/MaxCateringModules");
            int maxcat = 0;
            Int32.TryParse(node.InnerXml.Trim(), out maxcat);
            m_sysData.MaxCatering = maxcat;

            node = xd.SelectSingleNode("//myVRMSiteLicense/Organizations/Modules/MaxHousekeepingModules");
            int maxhkg = 0;
            Int32.TryParse(node.InnerXml.Trim(), out maxhkg);
            m_sysData.MaxHousekeeping = maxhkg;

            node = xd.SelectSingleNode("//myVRMSiteLicense/Organizations/Modules/MaxPCModules"); //FB 2347
            int MaxPC = 0;
            Int32.TryParse(node.InnerXml.Trim(), out MaxPC);
            m_sysData.MaxPCModule = MaxPC;

            node = xd.SelectSingleNode("//myVRMSiteLicense/Organizations/Modules/Languages/MaxSpanish");
            int maxspanish = 0;
            Int32.TryParse(node.InnerXml.Trim(), out maxspanish);
            m_sysData.MaxSpanish = maxspanish;

            node = xd.SelectSingleNode("//myVRMSiteLicense/Organizations/Modules/Languages/MaxMandarin");
            int maxMandarin = 0;
            Int32.TryParse(node.InnerXml.Trim(), out maxMandarin);
            m_sysData.MaxMandarin = maxMandarin;

            node = xd.SelectSingleNode("//myVRMSiteLicense/Organizations/Modules/Languages/MaxHindi");
            int MaxHindi = 0;
            Int32.TryParse(node.InnerXml.Trim(), out MaxHindi);
            m_sysData.MaxHindi = MaxHindi;

            node = xd.SelectSingleNode("//myVRMSiteLicense/Organizations/Modules/Languages/MaxFrench");
            int MaxFrench = 0;
            Int32.TryParse(node.InnerXml.Trim(), out MaxFrench);
            m_sysData.MaxFrench = MaxFrench;
        }
        #endregion

        #region getSysMail
        public bool getSysMail(ref sysMailData mailData)
        {
            try
            {
                SessionManagement.GetSession(m_configPath);
                ISession session = SessionManagement.GetSession(m_configPath);
                mailData =(sysMailData)session.Load(typeof(sysMailData), 1);
            }
            catch (Exception e)
            {
                throw e;
            }
            return true;
        }
        #endregion 

        #region Public Properties

        public static DateTime LastModified{get {return m_sysData.LastModified;}}
		public static int Admin{get  {return m_sysData.Admin;}}
		public static string SecurityKey{get  {return m_sysData.SecurityKey;}}
		public static int superAdmin{get  {return m_sysData.superAdmin;}}
        public static int TimeZone { get { return m_sysData.TimeZone; } }
		public static string foodsecuritykey{get  {return m_sysData.foodsecuritykey;}}
		public static string resourcesecuritykey{get  {return m_sysData.resourcesecuritykey;}}
		public static string emaildisclaimer{get  {return m_sysData.emaildisclaimer;}}
		public static string license{get  {return m_sysData.license;}}
        public static int LastModifiedUser { get { return m_sysData.LastModifiedUser; } }
		/// <summary>
		/// Summary description for License information.
		/// (this is a static class as system settings dont change);
		/// </summary>
		public static DateTime ExpiryDate{get {return m_sysData.ExpiryDate;}}
		public static int RoomLimit{get {return m_sysData.RoomLimit;}}
		public static int MCULimit{get {return m_sysData.MCULimit;}}
        public static int MCUEnchancedLimit { get { return m_sysData.MCUEnchancedLimit; } }//FB 2486
        public static int UserLimit{get {return m_sysData.UserLimit;}}
        public static int ExtRoomLimit { get { return m_sysData.ExtRoomLimit; } } //FB 2426
        public static int Cloud { get { return m_sysData.Cloud; } } //FB 2262 - J //FB 2599
        //public static int MaxGroups{get {return m_sysData.MaxGroups;}} - Depricated as per new license
        //public static int MaxTemplates{get {return m_sysData.MaxTemplates;}}
		public static int MaxGuests{get {return m_sysData.MaxGuests;}}

        /** Code added for Activation -- Start **/
        public static int IsDemo { get { return m_sysData.IsDemo; } }
        public static List<String> CPUID { get { return m_sysData.CPUID; } }
        public static List<String> MACID { get { return m_sysData.MacID; } }
        public static DateTime ActivatedDate { get { return m_sysData.ActivatedDate; } }
        /** Code added for Activation -- End **/
        //Merging for License modification START
        public static int MaxOrganizations { get { return m_sysData.MaxOrganizations; } }
        public static int EnablePublicRooms { get { return m_sysData.EnablePublicRooms; } } //FB 2594
        public static int IsLDAP { get { return m_sysData.IsLDAP; } }
        public static int MaxNVidRooms { get { return m_sysData.MaxNVidRooms; } }
        public static int MaxVidRooms { get { return m_sysData.MaxVidRooms; } }
        public static int MaxEndPts { get { return m_sysData.MaxEndPts; } }
        public static int MaxCDR { get { return m_sysData.MaxCDR; } }
        public static int MaxGstPerUsrs { get { return m_sysData.MaxGstPerUsrs; } }
        public static int MaxExcUsrs { get { return m_sysData.MaxExcUsrs; } }
        public static int MaxDomUsrs { get { return m_sysData.MaxDomUsrs; } }
        public static int MaxMobUsrs { get { return m_sysData.MaxMobUsrs; } } //FB 1979
        public static int MaxFacilities { get { return m_sysData.MaxFacilities; } }
        public static int MaxHousekeeping { get { return m_sysData.MaxHousekeeping; } }
        public static int MaxCatering { get { return m_sysData.MaxCatering; } }
        public static int MaxAPIs { get { return m_sysData.MaxAPIs; } }
        public static int MaxSpanish { get { return m_sysData.MaxSpanish; } }
        public static int MaxMandarin { get { return m_sysData.MaxMandarin; } }
        public static int MaxHindi { get { return m_sysData.MaxHindi; } }
        public static int MaxFrench { get { return m_sysData.MaxFrench; } }
        public static int MaxPCModule { get { return m_sysData.MaxPCModule; } } //FB 2347
        public static int MaxVMRooms { get { return m_sysData.MaxVMRooms; } } //FB 2586
        //Merging for License modification END
        //System LOGO..
        public static int SysData { get { return m_sysData.SiteLogoId; } }
        public static string m_Companymessage { get { return m_sysData.Companymessage; } }
        public static int StdBannerId { get { return m_sysData.StdBannerId; } }
        public static int HighBannerId { get { return m_sysData.HighBannerId; } }

        public static int LaunchBuffer { get { return m_sysData.LaunchBuffer; } }//FB 2007
        public static int EnableLaunchBufferP2P { get { return m_sysData.EnableLaunchBufferP2P; } }//FB 2437
        public static int StartMode { get { return m_sysData.StartMode; } } //FB 2051
        //FB 2501 EM7 Starts
		public static string EM7URI { get { return m_sysData.EM7URI; } }
        public static string EM7Username { get { return m_sysData.EM7Username; } }
        public static string EM7Password { get { return m_sysData.EM7Password; } }
        public static int EM7Port { get { return m_sysData.EM7Port; } }
		//FB 2501 EM7 Ends
        
        #endregion
    }
    #endregion

    #region sysMailData
    /// <summary>
	/// Summary description for system Email Server settings.
	/// (this is a static class as system settings dont change);
	/// </summary>
	public class sysMailData
	{
        /* *** This class is modified during organization module *** */

		#region Private Internal Members
        private int m_id;
		private	string m_websiteURL;
		private	int m_IsRemoteServer;
		private	string m_ServerAddress, m_Login, m_password;
        private int m_portNo, m_ConTimeOut;
        private string m_CompanyMail, m_displayname, m_messagetemplate;
		private DateTime m_LastRunDateTime; //Dashboard
        private int m_RetryCount;//FB 2552
        
		#endregion

		#region Public Properties
        public int id
        {
            get { return m_id; }
            set { m_id = value; }
        }
		public	string websiteURL
		{
			get {return m_websiteURL;}
			set { m_websiteURL = value;}
		}
		public	int IsRemoteServer
		{
			get {return m_IsRemoteServer;}
			set {m_IsRemoteServer = value;}
		}
		public	string ServerAddress
		{
			get {return m_ServerAddress;}
			set {m_ServerAddress = value;}
		}
		public	string Login
		{
			get {return m_Login;}
			set {m_Login = value;}
		}
		public	string password
		{
			get {return m_password;}
			set {m_password = value;}
		}
		public	int portNo
		{
			get {return m_portNo;}
			set { m_portNo = value; }
		}
		public	int ConTimeOut
		{
            get { return m_ConTimeOut; }
			set { m_ConTimeOut = value; }
		}
		public string displayname
		{
			get {return m_displayname;}
			set {m_displayname = value;}
		}
		public string CompanyMail
		{
			get {return m_CompanyMail;}
			set {m_CompanyMail = value;}
		}
		public string messagetemplate
		{
			get {return m_messagetemplate;}
			set {m_messagetemplate = value;}
		}
        public int RetryCount //FB 2552
        {
            get { return m_RetryCount; }
            set { m_RetryCount = value; }
        }
		#endregion

        #region Added for DashBoard
        public DateTime LastRunDateTime
        {
            get { return m_LastRunDateTime; }
            set { m_LastRunDateTime = value; }
        }
        #endregion
    }
    #endregion
        
    #region vrmUtility

    public class vrmUtility
    {
        private static string PASSPHRASE = "Ac923rKJbsf98hoAFG`1=-] `12r7cb0l";

        public bool simpleEncrypt(ref string data)
        {
            try
            {
                string temp = string.Empty;
                string pass = PASSPHRASE;
                int i, p = 0;
                for (i = 0; i < data.Length; i++) /* process all character in the buffer */
                {
            
                    int iChar = ( data[i] ^ pass[p]);
                    int x = data[i];
                    int y = pass[p];       
                    temp += String.Format("{0:x2}", iChar);
                    p = (p + 1) % pass.Length;  /* determin the next character in the password string to be used; wrap is necessary */
                }
                data = temp.ToUpper(); //Email Link Changes
                return true;
            }
            catch (Exception e)
            {
                throw e;
            }
       
        }

        private string StringToHex(string asciiString)
        {
            string hex = "";
            try
            {
                foreach (char c in asciiString)
                {
                    int tmp = c;
                    hex += String.Format("{0:x2}", (uint)System.Convert.ToUInt32(tmp.ToString()));
                }
            }
            catch (Exception e)
            {
                throw e;
            }
       
            return hex;
        }
        //FB 2027 - Starts
        private string HexToString(string buff)
        {
            string stringValue = "";
            try
            {
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i <= buff.Length - 2; i += 2)
                {
                    sb.Append(Convert.ToString(Convert.ToChar(Int32.Parse(buff.Substring(i, 2), System.Globalization.NumberStyles.HexNumber))));
                }
                stringValue = sb.ToString();
            }
            catch (Exception e)
            {

                throw e;
            }
            return stringValue;
        }


        private string HexString2Ascii(string hexString)
        {
            hexString = hexString.Substring(2, hexString.Length - 2);
            try
            {
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i <= hexString.Length - 2; i += 2)
                {
                    sb.Append(Convert.ToString(Convert.ToChar(Int32.Parse(hexString.Substring(i, 2), System.Globalization.NumberStyles.HexNumber))));
                }
                return sb.ToString();

            }
            catch (Exception e)
            {
                throw e;
            }
               }
		//FB 2007
        public bool simpleDecrypt(ref string data)
        {
            try
            {
                data = HexString2Ascii(data);
                string temp = string.Empty;
                string pass = PASSPHRASE;
                int i, p = 0;
                for (i = 0; i < data.Length; i++) /* process all character in the buffer */
                {

                    int iChar = (data[i] ^ pass[p]);
                    int x = data[i];
                    int y = pass[p];
                    temp += String.Format("{0:x2}", iChar);
                    p = (p + 1) % pass.Length;  /* determin the next character in the password string to be used; wrap is necessary */
                }
                data = HexToString(temp.ToUpper()); //Email Link Changes
                return true;
            }
            catch (Exception e)
            {
                throw e;
            }

        }
    }
    #endregion

    //FB 2027 - Ends

    #region sysTimeZonePref
    // Added for the FB  1069,1070 Starts
    public class sysTimeZonePref
    {
        #region Private Internal Members

        private int m_systemID;
        private string m_name;

        #endregion

        #region Public Properties
        public int systemid
        {
            get { return m_systemID; }
            set { m_systemID = value; }
        }

        public string name
        {
            get { return m_name; }
            set { m_name = value; }
        }
        #endregion
    }
    // Added for the FB  1069,1070 Starts
    #endregion

    #region AccountScheme

    public class sysAccScheme
    {
        #region Private Internal Members
        private int m_id;
        private string m_name;
        #endregion

        #region Public Properties
        public int id
        {
            get { return m_id; }
            set { m_id = value; }
        }
        public string name
        {
            get { return m_name; }
            set { m_name = value; }
        }
        #endregion
    }
    #endregion

    //FB 2363
    #region sysExternalScheduling

    public class sysExternalScheduling
    {
        #region Private Internal Members

        private int m_id;        
        private string m_partnerName;
        private string m_partnerEmail;
        private string m_partnerURL;
        private string m_userName;
        private string m_password;
        private DateTime m_modifiedDate = DateTime.Now;
        private int m_timeoutValue; //FB 2363

        #endregion

        #region Public Properties
        public int UId
        {
            get { return m_id; }
            set { m_id = value; }
        }

        public string PartnerName
        {
            get { return m_partnerName; }
            set { m_partnerName = value; }
        }

        public string PartnerEmail
        {
            get { return m_partnerEmail; }
            set { m_partnerEmail = value; }
        }

        public string PartnerURL
        {
            get { return m_partnerURL; }
            set { m_partnerURL = value; }
        }

        public string UserName
        {
            get { return m_userName; }
            set { m_userName = value; }
        }

        public string Password
        {
            get { return m_password; }
            set { m_password = value; }
        }

        public DateTime ModifiedDate
        {
            get { return m_modifiedDate; }
            set { m_modifiedDate = value; }
        }
        public int TimeoutValue //FB 2363
        {
            get { return m_timeoutValue; }
            set { m_timeoutValue = value; }

        }
        #endregion
    }
    #endregion

    #region ESMailUsrRptSettings

    public class ESMailUsrRptSettings
    {
        #region Private Internal Members

        private int m_UId,m_DeliveryType, m_FrequencyType, m_FrequencyCount, m_Sent,m_orgid;
        private string m_CustomerName, m_RptDestination,m_type;//FB 2363
        private DateTime m_StartTime = DateTime.Now;
        private DateTime m_SentTime = DateTime.Now;
        
        #endregion

        #region Public Properties
        public int UId
        {
            get { return m_UId; }
            set { m_UId = value; }
        }

        public string CustomerName
        {
            get { return m_CustomerName; }
            set { m_CustomerName = value; }
        }

        public string RptDestination
        {
            get { return m_RptDestination; }
            set { m_RptDestination = value; }
        }

        public int DeliveryType
        {
            get { return m_DeliveryType; }
            set { m_DeliveryType = value; }
        }

        public int FrequencyType
        {
            get { return m_FrequencyType; }
            set { m_FrequencyType = value; }
        }

        public int Sent
        {
            get { return m_Sent; }
            set { m_Sent = value; }
        }

        public int FrequencyCount
        {
            get { return m_FrequencyCount; }
            set { m_FrequencyCount = value; }
        }
        
        public DateTime StartTime
        {
            get { return m_StartTime; }
            set { m_StartTime = value; }
        }

        public DateTime SentTime
        {
            get { return m_SentTime; }
            set { m_SentTime = value; }
        }

        public string Type
        {
            get { return m_type; }
            set { m_type = value; }
        }

        public int orgId
        {
            get { return m_orgid; }
            set { m_orgid = value; }
        }
        #endregion
    }
    #endregion
	//FB 2392
    #region sysWhygo
    public class sysWhygoSettings
    {
        #region Private Internal Members

        private int m_id;
        private String m_WhyGoURL, m_WhyGoUserName, m_WhyGoPassword; //FB 2392
        private DateTime m_modifiedDate = DateTime.Now;

        #endregion

        #region Public Properties
        public int UId
        {
            get { return m_id; }
            set { m_id = value; }
        }

        
        public string WhyGoURL
        {
            get { return m_WhyGoURL; }
            set { m_WhyGoURL = value; }
        }
        public string WhyGoUserName
        {
            get { return m_WhyGoUserName; }
            set { m_WhyGoUserName = value; }
        }
        public string WhyGoPassword
        {
            get { return m_WhyGoPassword; }
            set { m_WhyGoPassword = value; }
        }

        public DateTime ModifiedDate
        {
            get { return m_modifiedDate; }
            set { m_modifiedDate = value; }
        }
        #endregion
    }
    #endregion
}

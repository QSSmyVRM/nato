using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;

using NHibernate;
using NHibernate.Criterion;

namespace myVRM.DataLayer
{
    /// <summary>
    /// Generic Data Access Object for all classes.
    /// Implement IDispose to close session
    /// </summary>
    public class vrmDAO
    {
        protected log4net.ILog m_log;
        protected string m_configPath;

        public vrmDAO(string config, log4net.ILog log)
        {
            m_log = log;
            m_configPath = config;
        }
    }   

}

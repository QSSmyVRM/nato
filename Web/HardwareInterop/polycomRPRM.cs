/* FILE : PolycomAccord.cs
 * DESCRIPTION : All Polycom MCU api commands are stored in this file. 
 * AUTHOR : Kapil M
 */

namespace NS_POLYCOMRPRM
{
    #region References
    using System;
    using System.Xml;
    using System.Net;
    using System.Collections;
    using System.Collections.Generic;
    using System.Text;
    using System.IO;
    using System.Web;
    using System.Linq;
    using System.Diagnostics;
    using System.Xml.Linq;
    #endregion
    class POLYCOMRPRM
    {
        private NS_LOGGER.Log logger;
        internal string errMsg = null;
        NS_MESSENGER.ConfigParams configParams;
        private NS_DATABASE.Database db;

        bool ret = false;
        StringBuilder str_build;
        XmlWriterSettings settings;
        XmlWriter xWriter = null;
        XDocument xdoc;
        int DMAorRPRM = 0;
        string credentials = "";
        string MCUIP = "";
        string MCULogin = "";
        string MCUPassword = "";
        int MCUPort = 0;
        string strUrl = "";
        string ContentType = "", RequestMethod = "", RestURI = "", responseXml = "", Etag = "";
        DateTime Date = new DateTime();
        List<NS_MESSENGER.Conference> confList = null;
        internal bool isRecurring = false; //FB 2441

        #region POLYCOMRPRM
        /// <summary>
        /// polycomRPMS
        /// </summary>
        /// <param name="config"></param>
        internal POLYCOMRPRM(NS_MESSENGER.ConfigParams config)
        {
            configParams = new NS_MESSENGER.ConfigParams();
            configParams = config;
            logger = new NS_LOGGER.Log(configParams);
            db = new NS_DATABASE.Database(configParams);
        }
        #endregion

        #region GeneralMethod

        #region CallType
        /// <summary>
        /// CallType
        /// </summary>
        /// <param name="partyObj"></param>
        /// <returns></returns>
        // <xs:enumeration value="IN_PERSON"/>  
        //<xs:enumeration value="AUDIO"/>  
        //<xs:enumeration value="VIDEO"/> 
        private string CallType(ref NS_MESSENGER.Party.eCallType eType)
        {
            string CallType = "IN_PERSON";
            if (eType == NS_MESSENGER.Party.eCallType.VIDEO)
                CallType = "VIDEO";
            else if (eType == NS_MESSENGER.Party.eCallType.AUDIO)
                CallType = "AUDIO";

            return CallType;
        }
        #endregion

        public enum lockunlockconference
        {
            lockconfernece = 1, unlockconference = 0
        };

        public enum Startstoprecord
        {
            startRecord = 1, stopRecord = 0
        };

        public enum PolycomRedirect
        {
           FromService = 1, FromWebsite = 0
        };

        public class User
        {
            public string ParticipantGUID { get; set; }
            public int Status { get; set; }
            public string IPISDNAddress { get; set; }
        }

        //public class Template
        //{
        //    public int MCUid { get; set; }
        //    public int ProfileID { get; set; }
        //    public string ProfileName { get; set; }
        //}

        #region AddressType
        /// <summary>AddressType
        //  <xs:enumeration value="H323"/>  
        //  <xs:enumeration value="ISDN"/>  
        //  <xs:enumeration value="SIP"/>  
        //  <xs:enumeration value="H323_E164"/>  
        //  <xs:enumeration value="H323_ANNEX_O"/>  
        //  <xs:enumeration value="H323_ID"/>  
        /// </summary>
        /// <param name="partyObj"></param>
        /// <returns></returns>
        private string AddressType(ref NS_MESSENGER.Party.eAddressType eAddType)
        {
            string AddType = "";

            if (eAddType == NS_MESSENGER.Party.eAddressType.H323_ID)
                AddType = "H323";
            else if (eAddType == NS_MESSENGER.Party.eAddressType.IP_ADDRESS)
                AddType = "H323";
            else if (eAddType == NS_MESSENGER.Party.eAddressType.ISDN_PHONE_NUMBER)
                AddType = "SIP";
            else if (eAddType == NS_MESSENGER.Party.eAddressType.E_164)
                AddType = "SIP";

            return AddType;
        }
        #endregion

        #region DialDirection
        /// <summary>DialDirection
        //<xs:enumeration value="DIAL_OUT"/>  
        // <xs:enumeration value="DIAL_IN"/>   
        /// </summary>
        /// <param name="partyObj"></param>
        /// <returns></returns>
        private string DialDirection(ref NS_MESSENGER.Party.eConnectionType ConnType)
        {
            string DialDir = "";

            if (ConnType == NS_MESSENGER.Party.eConnectionType.DIAL_IN)
                DialDir = "DIAL_IN";
            else if (ConnType == NS_MESSENGER.Party.eConnectionType.DIAL_OUT)
                DialDir = "DIAL_OUT";

            return DialDir;
        }
        #endregion

        #region isValidIPAddress
        /// <summary>
        /// isValidIPAddress
        /// </summary>
        /// <param name="ip"></param>
        /// <returns></returns>
        private bool isValidIPAddress(string ip)
        {
            ret = true;
            string[] octets = ip.Split('.');
            if (octets.Length != 4)
                ret = false;
            else
            {
                for (int i = 0; i < 4; i++)
                {
                    int dig = Convert.ToInt32(octets[i]);
                    if (dig < 0 || dig > 255)
                    {
                        ret = false;
                        break;
                    }
                }
            }
            return ret;
        }
        #endregion

        #region EquateVideoCodecs
        /// <summary>
        /// EquateVideoCodecs
        /// </summary>
        /// <param name="videoCodec"></param>
        /// <param name="polycomVideoCodec"></param>
        /// <returns></returns>

        private bool EquateVideoCodecs(NS_MESSENGER.Conference.eVideoCodec videoCodec, ref string polycomVideoCodec)
        {
            try
            {
                // equating to VRM to Polycom values
                if (videoCodec == NS_MESSENGER.Conference.eVideoCodec.AUTO)
                {
                    polycomVideoCodec = "auto";
                }
                else
                {
                    if (videoCodec == NS_MESSENGER.Conference.eVideoCodec.H261)
                    {
                        polycomVideoCodec = "h261";
                    }
                    else
                    {
                        if (videoCodec == NS_MESSENGER.Conference.eVideoCodec.H263)
                        {
                            polycomVideoCodec = "h263";
                        }
                        else
                        {
                            polycomVideoCodec = "h264";
                        }
                    }
                }

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        #endregion

        #region EquateAudioCodec
        /// <summary>
        /// EquateAudioCodec
        /// </summary>
        /// <param name="audioCodec"></param>
        /// <param name="polycomAudioCodec"></param>
        /// <returns></returns>
        private bool EquateAudioCodec(NS_MESSENGER.Conference.eAudioCodec audioCodec, ref string polycomAudioCodec)
        {
            try
            {

                if (audioCodec == NS_MESSENGER.Conference.eAudioCodec.G722_56)
                    polycomAudioCodec = "g722_56";
                else
                {
                    if (audioCodec == NS_MESSENGER.Conference.eAudioCodec.G728)
                        polycomAudioCodec = "g728";
                    else
                    {
                        if (audioCodec == NS_MESSENGER.Conference.eAudioCodec.G722_32)
                            polycomAudioCodec = "g722_32";
                        else
                        {
                            if (audioCodec == NS_MESSENGER.Conference.eAudioCodec.G722_24)
                                polycomAudioCodec = "g722_24";
                            else
                            {
                                if (audioCodec == NS_MESSENGER.Conference.eAudioCodec.G711_56)
                                    polycomAudioCodec = "g711_56";
                                else
                                {
                                    if (audioCodec == NS_MESSENGER.Conference.eAudioCodec.AUTO)
                                        polycomAudioCodec = "auto";
                                }
                            }
                        }
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        #endregion

        #region EquateLineRate
        /// <summary>
        /// EquateLineRate
        /// </summary>
        /// <param name="lineRate"></param>
        /// <param name="isIP"></param>
        /// <param name="polycomLineRate"></param>
        /// <returns></returns>
        private bool EquateLineRate(NS_MESSENGER.LineRate.eLineRate lineRate, bool isIP, ref string polycomLineRate)
        {
            try
            {
                // equating VRM to Polycom values


                if (isIP)
                {
                    #region IP linerate mappings
                    //Modified during FB 1742
                    switch (lineRate)
                    {
                        case NS_MESSENGER.LineRate.eLineRate.K64:
                            {
                                polycomLineRate = "64";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.K128:
                            {
                                polycomLineRate = "128";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.K192:
                            {
                                polycomLineRate = "128";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.K256:
                            {
                                polycomLineRate = "256";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.K320:
                            {
                                polycomLineRate = "256";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.K384:
                            {
                                polycomLineRate = "384";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.K512:
                            {
                                polycomLineRate = "512";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.K768:
                            {
                                polycomLineRate = "768";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.M1024:
                            {
                                polycomLineRate = "1024";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.M1152:
                            {
                                polycomLineRate = "1152";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.M1250:
                            {
                                polycomLineRate = "1152";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.M1472:
                            {
                                polycomLineRate = "1472";
                                break;
                            }

                        case NS_MESSENGER.LineRate.eLineRate.M1536:
                            {
                                polycomLineRate = "1472";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.M1792:
                            {
                                polycomLineRate = "1472";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.M1920:
                            {
                                polycomLineRate = "1920";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.M2048:
                            {
                                polycomLineRate = "1920";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.M2560:
                            {
                                polycomLineRate = "1920";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.M3072:
                            {
                                polycomLineRate = "1920";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.M3584:
                            {
                                polycomLineRate = "1920";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.M4096:
                            {
                                polycomLineRate = "4096";
                                break;
                            }
                        default:
                            {
                                // default is 384 kbps
                                polycomLineRate = "384";
                                break;
                            }
                    }
                    #endregion

                    #region IP linerate mappings - Commented during FB 1742

                    //if (lineRate == NS_MESSENGER.LineRate.eLineRate.K64)
                    //    polycomLineRate = "64";
                    //else
                    //{
                    //    if (lineRate == NS_MESSENGER.LineRate.eLineRate.K128)
                    //        polycomLineRate = "128";
                    //    else
                    //    {
                    //        if (lineRate == NS_MESSENGER.LineRate.eLineRate.K256)
                    //            polycomLineRate = "256";
                    //        else
                    //        {
                    //            if (lineRate == NS_MESSENGER.LineRate.eLineRate.K512)
                    //                polycomLineRate = "512";
                    //            else
                    //            {
                    //                if (lineRate == NS_MESSENGER.LineRate.eLineRate.K768)
                    //                    polycomLineRate = "768";
                    //                else
                    //                {
                    //                    if (lineRate == NS_MESSENGER.LineRate.eLineRate.M1152)
                    //                        polycomLineRate = "1152";
                    //                    else
                    //                    {
                    //                        if (lineRate == NS_MESSENGER.LineRate.eLineRate.M1472)
                    //                            polycomLineRate = "1472";
                    //                        else
                    //                        {
                    //                            if (lineRate == NS_MESSENGER.LineRate.eLineRate.M1536)
                    //                                polycomLineRate = "1536";
                    //                            else
                    //                            {
                    //                                if (lineRate >= NS_MESSENGER.LineRate.eLineRate.M1920)
                    //                                    polycomLineRate = "1920";
                    //                            }
                    //                        }

                    //                    }
                    //                }
                    //            }
                    //        }
                    //    }
                    //}
                    #endregion
                }
                else
                {
                    #region ISDN linerate mappings
                    if (lineRate == NS_MESSENGER.LineRate.eLineRate.K64)
                        polycomLineRate = "channel_1";
                    else
                    {
                        if (lineRate == NS_MESSENGER.LineRate.eLineRate.K128)
                            polycomLineRate = "channel_2";
                        else
                        {
                            if (lineRate == NS_MESSENGER.LineRate.eLineRate.K256)
                                polycomLineRate = "channel_4";
                            else
                            {
                                if (lineRate == NS_MESSENGER.LineRate.eLineRate.K512)
                                    polycomLineRate = "channel_8";
                                else
                                {
                                    if (lineRate == NS_MESSENGER.LineRate.eLineRate.K768)
                                        polycomLineRate = "channel_12";
                                    else
                                    {
                                        if (lineRate == NS_MESSENGER.LineRate.eLineRate.M1152)
                                            polycomLineRate = "channel_18";
                                        else
                                        {
                                            if (lineRate == NS_MESSENGER.LineRate.eLineRate.M1472)
                                                polycomLineRate = "channel_23";
                                            else
                                            {
                                                if (lineRate == NS_MESSENGER.LineRate.eLineRate.M1536)
                                                    polycomLineRate = "channel_24";
                                                else
                                                {
                                                    if (lineRate == NS_MESSENGER.LineRate.eLineRate.M1920)
                                                        polycomLineRate = "channel_30";
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    #endregion
                }

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        #endregion

        #region EquateVideoLayout
        /// <summary>
        /// EquateVideoLayout
        /// </summary>
        /// <param name="videoLayoutID"></param>
        /// <param name="polycomVideoLayoutValue"></param>
        /// <returns></returns>
        private bool EquateVideoLayout(int videoLayoutID, ref string polycomVideoLayoutValue)
        {

#if COMMENT_DO_NOT_DELETE
Dan's email 10/22/06 - MGC equivalent mappings of Codian layout code :
enumeration 	1x1 Codian # 1 – Classic and Quad Mode 
enumeration 	1x2 No Codian layout avail – Classic and Quad Mode Image 16
enumeration 	2x1 No Codian layout avail – Classic and Quad Mode 
enumeration 	2x2 Codian # 2 – Classic and Quad Mode
enumeration 	1and5 Codian # 5 – Classic Mode
enumeration 	3x3 Codian # 3 – Classic and Quad Mode
enumeration 	1x2Ver Codian # 16 – Classic Mode
enumeration 	1x2Hor Codian # 12 – Classic Mode
enumeration 	1and2Hor No Codian layout avail – Classic Mode Image 61
enumeration 	1and2Ver Codian # 17 – Classic Mode
enumeration 	1and3Hor No Codian layout avail – Classic Mode Image 62
enumeration 	1and3Ver Codian # 18 – Classic Mode
enumeration 	1and4Ver Codian # 19 – Classic Mode
enumeration 	1and4Hor No Codian layout avail – Classic Mode Image 63
enumeration 	1and8Central Codian # 24 – Classic Mode
enumeration 	1and8Upper No Codian layout avail – Classic Mode Image 60
enumeration 	1and2HorUpper Codian # 13 – Classic Mode
enumeration 	1and3HorUpper Codian # 14 – Classic Mode
enumeration 	1and4HorUpper Codian # 15 – Classic Mode
enumeration 	1and8Lower Codian # 20 – Classic Mode
enumeration 	1and7 Codian # 6 – Classic Mode
enumeration 	4x4 Codian # 4 – Quad Mode
enumeration 	2and8 Codian # 25 – Quad Mode
enumeration 	1and12 Codian # 33 – Quad Mode
enumeration 	1x1Qcif No Codian layout avail – Quad Mode
#endif
            polycomVideoLayoutValue = "1x1";
            try
            {
                switch (videoLayoutID)
                {
                    case 1:
                        {
                            polycomVideoLayoutValue = "1x1";
                            break;
                        }
                    case 2:
                        {
                            polycomVideoLayoutValue = "2x2";
                            break;
                        }
                    case 3:
                        {
                            polycomVideoLayoutValue = "3x3";
                            break;
                        }
                    case 4: //FB 2335
                        {
                            polycomVideoLayoutValue = "4x4";
                            break;
                        }
                    case 5:
                        {
                            polycomVideoLayoutValue = "1and5";
                            break;
                        }
                    case 6:
                        {
                            polycomVideoLayoutValue = "1and7";
                            break;
                        }
                    // case 7-11 : layout is not supported 
                    case 12:
                        {
                            polycomVideoLayoutValue = "1x2Hor";
                            break;
                        }
                    case 13:
                        {
                            polycomVideoLayoutValue = "1and2HorUpper";
                            break;
                        }
                    case 14:
                        {
                            polycomVideoLayoutValue = "1and3HorUpper";
                            break;
                        }
                    case 15:
                        {
                            polycomVideoLayoutValue = "1and4HorUpper";
                            break;
                        }
                    case 16:
                        {
                            // Changed from 1x2Ver to 1x2 on BTBoces request (case 1606)
                            polycomVideoLayoutValue = "1x2";
                            break;
                        }
                    case 17:
                        {
                            polycomVideoLayoutValue = "1and2Ver";
                            break;
                        }
                    case 18:
                        {
                            polycomVideoLayoutValue = "1and3Ver";
                            break;
                        }
                    case 19:
                        {
                            polycomVideoLayoutValue = "1and4Ver";
                            break;
                        }
                    case 20:
                        {
                            polycomVideoLayoutValue = "1and8Lower";
                            break;
                        }

                    //case 21-23 : layout is not supported
                    case 24:
                        {
                            polycomVideoLayoutValue = "1and8Central";
                            break;
                        }
                    case 25:
                        {
                            polycomVideoLayoutValue = "2and8";
                            break;
                        }
                    //case 26-32: layout is not supported					
                    case 33:
                        {
                            polycomVideoLayoutValue = "1and12";
                            break;
                        }
                    case 60://FB 2335
                        {
                            polycomVideoLayoutValue = "1and8Upper";
                            break;
                        }
                    case 61://FB 2335
                        {
                            polycomVideoLayoutValue = "1and2Hor";
                            break;
                        }
                    case 62://FB 2335
                        {
                            polycomVideoLayoutValue = "1and3Hor";
                            break;
                        }
                    case 63://FB 2335
                        {
                            polycomVideoLayoutValue = "1and4Hor";
                            break;
                        }





                    //case 33-42: layout is not supported					

                    //case 101-109 are all custom to btboces for MGC accord bridges
                    case 101:
                        {
                            polycomVideoLayoutValue = "1x1";
                            break;
                        }
                    case 102:
                        {
                            polycomVideoLayoutValue = "1x2";//"1x2Ver"; 
                            break;
                        }
                    case 103:
                        {
                            polycomVideoLayoutValue = "1and2HorUpper"; //"1x2"; 
                            break;
                        }
                    case 104:
                        {
                            polycomVideoLayoutValue = "2x2"; //"1and2HorUpper"; 
                            break;
                        }
                    case 105:
                        {
                            polycomVideoLayoutValue = "1and5"; //"1and2Ver"; 
                            break;
                        }
                    case 106:
                        {
                            polycomVideoLayoutValue = "1x2Ver"; //"2x2"; 
                            break;
                        }
                    case 107:
                        {
                            polycomVideoLayoutValue = "1and2Ver";//"1and3Ver"; 
                            break;
                        }
                    case 108:
                        {
                            polycomVideoLayoutValue = "1and3Ver"; //"1and5"; 
                            break;
                        }
                    case 109:
                        {
                            polycomVideoLayoutValue = "1and7";
                            break;
                        }

                    default:
                        {
                            // layout not supported
                            this.errMsg = "This video layout is not supported on Polycom MCU.";
                            return false;
                        }
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return (false);
            }
        }
        #endregion

        #region ProcessError
        /// <summary>
        /// ProcessError
        /// </summary>
        /// <param name="error"></param>
        /// <returns></returns>
        private string ProcessError(string error)
        {
            string retError = "";
            XElement elem = null;
            try
            {
                elem = XElement.Parse(error);

                XNamespace env = "urn:com:polycom:api:rest:plcm-error";

                retError = "Error Code :" + elem.Element(env + "status-code").Value + " Error :" + elem.Element(env + "description").Value;
                retError = retError.Replace("'", "");


            }
            catch (Exception ex)
            {
                retError = error;

            }
            return retError;
        }
        #endregion

        #region encodeString
        /// <summary>
        /// encodeString
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        private static string encodeString(string data)
        {
            string encoded = "";
            try
            {
                byte[] bytes = System.Text.Encoding.UTF8.GetBytes(data);
                encoded = System.Convert.ToBase64String(bytes, Base64FormattingOptions.None);


            }
            catch (Exception ex)
            {
            }

            return encoded;

        }
        #endregion

        #endregion

        #region PublicMethod

        #region TestMCUConnection
        /// <summary>
        /// TestMCUConnection
        /// </summary>
        /// <param name="mcu"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        internal bool TestMCUConnection(NS_MESSENGER.MCU mcu, int type)
        {
            try
            {
                DMAorRPRM = type;
                RequestMethod = "GET";
                if (DMAorRPRM == (int)PolycomRedirect.FromWebsite)
                {
                    MCUIP = mcu.sIp.Trim();
                    MCUPort = mcu.iHttpPort;
                    MCULogin = mcu.sLogin;
                    MCUPassword = mcu.sPwd;
                }
                else
                {
                    MCUIP = mcu.sDMAip;
                    MCUPort = mcu.iDMAHttpport;
                    MCULogin = mcu.sDMALogin;
                    MCUPassword = mcu.sDMAPassword;
                }

                if (MCUPort == 8443)
                    strUrl = "https://" + MCUIP + ":" + MCUPort;
                else if (MCUPort == 443)
                    strUrl = "https://" + MCUIP;
                else
                    strUrl = "http://" + MCUIP + ":" + MCUPort;

                credentials = MCULogin + ":" + MCUPassword;
                credentials = encodeString(credentials);

                Date = new DateTime(); ret = false;
                ret = GetETagValue(strUrl, credentials, ref Date, ref Etag);
                if (!ret)
                {
                    logger.Exception(100, "TestConnection failed");
                    return false;
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        #endregion

        internal bool FetchMCUProfileDetails(NS_MESSENGER.MCU mcu)
        {
            try
            {
                NS_MESSENGER.Conference conf = new NS_MESSENGER.Conference();
                conf.cMcu = mcu;
                RestURI = "/api/rest/conference-templates";
                ContentType = "application/vnd.plcm.plcm-conference-template-list+xml";
                RequestMethod = "GET";
                DMAorRPRM = (int)PolycomRedirect.FromService;

                ret = false; responseXml = ""; str_build = new StringBuilder(); responseXml = "";
                ret = SendTransaction_overXMLHTTP(conf, str_build.ToString(), ContentType, RestURI, RequestMethod, DMAorRPRM, ref responseXml);
                if (!ret)
                {
                    logger.Exception(100, "SetConference SendTransaction failed");
                    return false;
                }
                xdoc = XDocument.Parse(responseXml);
                XNamespace ns2 = "urn:com:polycom:api:rest:plcm-simulcast-stream";
                XNamespace ns3 = "urn:com:polycom:api:rest:plcm-line-rate";
                XNamespace ns5 = "urn:com:polycom:api:rest:plcm-conference-template-list";
                XNamespace ns4 = "urn:com:polycom:api:rest:plcm-conference-template";
                var Template = (from temp in xdoc.Descendants(ns5 + "plcm-conference-template-list").Elements(ns4 + "plcm-conference-template")
                                select new NS_MESSENGER.MCUProfile
                                {
                                    iId = int.Parse(temp.Element(ns4 + "conference-template-identifier").Value),
                                    sDisplayname = temp.Element(ns4 + "name").Value,
                                    sName = ""
                                }).ToList();

                

                ret = false;
                ret = db.UpdateMcuProfile(conf.cMcu.iDbId, Template);
                if (!ret)
                {
                    logger.Exception(100, "FetchMCUProfileDetails failed");
                    return false;
                }

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }

        #region SetConference
        /// <summary>
        /// SetConference
        /// </summary>
        /// <param name="conf"></param>
        /// <returns></returns>
        internal bool SetConference(NS_MESSENGER.Conference templateConf)
        {
            logger.Trace("Entering SetConference function...");
            int cnfCnt = 0;
            NS_MESSENGER.Conference conf = null;
            NS_EMAIL.Email sendEmail = null;
            try
            {
                sendEmail = new NS_EMAIL.Email(configParams);
                ret = false;

                confList = new List<NS_MESSENGER.Conference>();
                confList.Add(templateConf);

                for (cnfCnt = 0; cnfCnt < confList.Count; cnfCnt++)
                {
                    xWriter = null;
                    settings = new XmlWriterSettings();
                    str_build = new StringBuilder();
                    settings.CheckCharacters = false;
                    settings.Encoding = Encoding.UTF8;
                    xWriter = XmlWriter.Create(str_build, settings);
                    conf = confList[cnfCnt];

                    ret = CreateXML_Reservation(ref conf, ref xWriter); //Call for Generate XML


                    if (!ret || str_build.ToString().Length < 1)
                    {
                        logger.Exception(100, "Creation of CreateXML_Reservation failed ");
                        return false;
                    }

                    if (str_build.ToString().Contains("utf-16"))
                        str_build.Replace("utf-16", "utf-8");
                    if (str_build.ToString().Contains(":n1"))
                        str_build.Replace(":n1", "");
                    if (str_build.ToString().Contains(":n2"))
                        str_build.Replace(":n2", "");
                    if (str_build.ToString().Contains(":n3"))
                        str_build.Replace(":n3", "");

                    ContentType = "application/vnd.plcm.plcm-reservation+xml";
                    if (conf.ESId == "")
                    {
                        RequestMethod = "POST";
                        RestURI = "/api/rest/reservations";
                    }
                    else
                    {
                        RequestMethod = "PUT";
                        RestURI = "/api/rest/reservations/" + conf.ESId;
                    }


                    //Change the innerText of "start-time" and "end-time" for all instances.

                    ret = false; responseXml = "";
                    ret = SendTransaction_overXMLHTTP(conf, str_build.ToString(), ContentType, RestURI, RequestMethod, DMAorRPRM, ref responseXml);
                    if (!ret)
                    {
                        logger.Exception(100, "SetConference SendTransaction failed");
                        sendEmail.SendEmailtoHostAndMCUAdmin(conf, this.errMsg);
                        return false;
                    }

                    if (conf.ESId.Trim() != "")
                    {

                        ret = false;
                        ret = db.UpdateConferenceExternalID(conf.iDbID, conf.iInstanceID, conf.ESId);
                        if (!ret)
                        {
                            logger.Exception(100, "SetConference ProcessXML_Reservation failed");
                            return false;
                        }
                        ret = false; responseXml = "";
                        ret = GetReservationDialString(conf, ref responseXml);
                        if (!ret)
                        {
                            logger.Exception(100, "GetReservationDialString failed");
                            return false;
                        }
                        ret = Process_ReservationDialString(conf, responseXml);
                        if (!ret)
                        {
                            logger.Exception(100, "Process_ReservationDialString failed");
                            return false;
                        }
                    }
                }

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        #endregion

        #region SetConference for reccurence
        /// <summary>
        /// SetConference for reccurence
        /// </summary>
        /// <param name="conf"></param>
        /// <returns></returns>
        internal bool SetConferenceRecurrence(NS_MESSENGER.Conference templateConf)
        {
            logger.Trace("Entering SetConference function...");
            int cnfCnt = 0;
            NS_MESSENGER.Conference conf = null;
            NS_EMAIL.Email sendEmail = null;
            try
            {
                sendEmail = new NS_EMAIL.Email(configParams);
                ret = false;

                confList = new List<NS_MESSENGER.Conference>();
                confList.Add(templateConf);

                if (templateConf.iRecurring == 1 && isRecurring) //FB 2441
                {
                    DeleteSyncConference(templateConf);
                    db.FetchConf(ref confList, 0); //for new and edit conference need to send delStatus as 0
                }

                for (cnfCnt = 0; cnfCnt < confList.Count; cnfCnt++)
                {
                    xWriter = null;
                    settings = new XmlWriterSettings();
                    str_build = new StringBuilder();
                    settings.CheckCharacters = false;
                    settings.Encoding = Encoding.UTF8;
                    xWriter = XmlWriter.Create(str_build, settings);
                    conf = confList[cnfCnt];

                    ret = CreateXML_Reservation(ref conf, ref xWriter); //Call for Generate XML


                    if (!ret || str_build.ToString().Length < 1)
                    {
                        logger.Exception(100, "Creation of CreateXML_Reservation failed ");
                        
                    }

                    if (str_build.ToString().Contains("utf-16"))
                        str_build.Replace("utf-16", "utf-8");
                    if (str_build.ToString().Contains(":n1"))
                        str_build.Replace(":n1", "");
                    if (str_build.ToString().Contains(":n2"))
                        str_build.Replace(":n2", "");
                    if (str_build.ToString().Contains(":n3"))
                        str_build.Replace(":n3", "");

                    ContentType = "application/vnd.plcm.plcm-reservation+xml";
                    if (conf.ESId == "")
                    {
                        RequestMethod = "POST";
                        RestURI = "/api/rest/reservations";
                    }
                    else
                    {
                        RequestMethod = "PUT";
                        RestURI = "/api/rest/reservations/" + conf.ESId;
                    }


                    //Change the innerText of "start-time" and "end-time" for all instances.

                    ret = false; responseXml = "";
                    ret = SendTransaction_overXMLHTTP(conf, str_build.ToString(), ContentType, RestURI, RequestMethod, DMAorRPRM, ref responseXml);
                    if (!ret)
                    {
                        logger.Exception(100, "SetConference SendTransaction failed");
                        sendEmail.SendEmailtoHostAndMCUAdmin(conf, this.errMsg);
                        
                    }

                    if (conf.ESId.Trim() != "")
                    {

                        ret = false;
                        ret = db.UpdateConferenceExternalID(conf.iDbID, conf.iInstanceID, conf.ESId);
                        if (!ret)
                        {
                            logger.Exception(100, "SetConference ProcessXML_Reservation failed");
                            
                        }
                        ret = false; responseXml = "";
                        ret = GetReservationDialString(conf, ref responseXml);
                        if (!ret)
                        {
                            logger.Exception(100, "GetReservationDialString failed");
                            
                        }
                        ret = Process_ReservationDialString(conf, responseXml);
                        if (!ret)
                        {
                            logger.Exception(100, "Process_ReservationDialString failed");
                            
                        }
                    }
                }

                return ret;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        #endregion

        #region GetReservationDialString
        /// <summary>
        /// GetReservationDialString
        /// </summary>
        /// <param name="conf"></param>
        /// <returns></returns>
        internal bool GetReservationDialString(NS_MESSENGER.Conference conf, ref string responseXml)
        {
            try
            {
                DMAorRPRM = (int)PolycomRedirect.FromWebsite;
                RestURI = "/api/rest/reservations/" + conf.ESId;
                RequestMethod = "GET";
                ContentType = "application/vnd.plcm.plcm-reservation+xml";

                ret = false; responseXml = ""; str_build = new StringBuilder();
                ret = SendTransaction_overXMLHTTP(conf, str_build.ToString(), ContentType, RestURI, RequestMethod, DMAorRPRM, ref responseXml);
                if (!ret)
                {
                    logger.Exception(100, "SetConference SendTransaction failed");
                    return false;
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        #endregion

        #region GetTerminalStatus
        /// <summary>
        /// GetTerminalStatus
        /// </summary>
        /// <param name="conf"></param>
        /// <returns></returns>
        internal bool GetEndpointStatus(NS_MESSENGER.Conference conf)
        {
            try
            {

                RestURI = "/api/rest/conferences/" + conf.sGUID + "/participants";
                RequestMethod = "GET";
                ContentType = "application/vnd.plcm.plcm-participant-list+xml";
                int Status = 0;

                ret = false; str_build = new StringBuilder(); DMAorRPRM = (int)PolycomRedirect.FromService; responseXml = "";
                ret = SendTransaction_overXMLHTTP(conf, str_build.ToString(), ContentType, RestURI, RequestMethod, DMAorRPRM, ref responseXml);
                if (!ret)
                {
                    logger.Exception(100, "GetTerminalStatus SendTransaction failed");
                    return false;
                }
                xdoc = XDocument.Parse(responseXml);
                XNamespace ns2 = "urn:com:polycom:api:rest:plcm-participant";
                XNamespace ns3 = "urn:com:polycom:api:rest:plcm-participant-list";
                var UserData = (from usr in xdoc.Descendants(ns3 + "plcm-participant-list").Elements(ns2 + "plcm-participant")
                                select new User
                                {
                                    ParticipantGUID = usr.Element(ns2 + "participant-identifier").Value,
                                    Status = usr.Element(ns2 + "connection-status").Value == "CONNECTED_DIAL_OUT" || xdoc.Element(ns2 + "connection-status").Value == "CONNECTED_DIAL_IN" ? 2 : 0,
                                    IPISDNAddress = usr.Element(ns2 + "endpoint-name").Value
                                }).ToList();
                for (int i = 0; i < UserData.Count; i++)
                {
                    ret = false;
                    ret = db.UpdateParticipantGUID(conf.iDbID, conf.iInstanceID, UserData[i].IPISDNAddress, UserData[i].ParticipantGUID, UserData[i].Status);
                    if (!ret)
                    {
                        logger.Exception(100, "GetEndpointStatus  update failed");
                        return false;
                    }

                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        #endregion

        #region TerminateConference
        /// <summary>
        /// TerminateConference
        /// </summary>
        /// <param name="conf"></param>
        /// <returns></returns>
        internal bool TerminateConference(NS_MESSENGER.Conference conf)
        {
            try
            {
                confList = new List<NS_MESSENGER.Conference>();
                confList.Add(conf);
                if (conf.iRecurring == 1 && isRecurring)
                    db.FetchConf(ref confList,1); //for delete mode need to send delStatus as 1

                for (int cnfCnt = 0; cnfCnt < confList.Count; cnfCnt++)
                {
                    DMAorRPRM = confList[cnfCnt].iRPRM;
                    if (DMAorRPRM == (int)PolycomRedirect.FromWebsite)
                        RestURI = "/api/rest/reservations/" + confList[cnfCnt].ESId;
                    else
                        RestURI = "/api/rest/conferences/" + confList[cnfCnt].sGUID;

                    ContentType = "*/*";
                    RequestMethod = "DELETE";
                    ret = false; str_build = new StringBuilder(); responseXml = "";
                    ret = SendTransaction_overXMLHTTP(confList[cnfCnt], str_build.ToString(), ContentType, RestURI, RequestMethod, DMAorRPRM, ref responseXml);
                    if (!ret)
                    {
                        logger.Exception(100, "TerminateConference failed");
                        //return false;
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Trace("TerminateConference:" + e.Message);
                return false;
            }
        }

        internal bool DeleteSyncConference(NS_MESSENGER.Conference conf)
        {
            List<NS_MESSENGER.Conference> confListDelete = null;
            try
            {
                confListDelete = new List<NS_MESSENGER.Conference>();
                confListDelete.Add(conf);

                db.FetchSyncAdjustments(ref confListDelete);

                for (int cnfCnt = 0; cnfCnt < confListDelete.Count; cnfCnt++)
                {

                    confListDelete[cnfCnt].cMcu = conf.cMcu;
                    DMAorRPRM = (int)PolycomRedirect.FromWebsite;
                    RestURI = "/api/rest/reservations/" + confListDelete[cnfCnt].ESId;
                    ContentType = "*/*";
                    RequestMethod = "DELETE";
                    ret = false; str_build = new StringBuilder(); responseXml = "";
                    ret = SendTransaction_overXMLHTTP(confListDelete[cnfCnt], str_build.ToString(), ContentType, RestURI, RequestMethod, DMAorRPRM, ref responseXml);
                    if (!ret)
                    {
                        logger.Exception(100, "TerminateConference failed");
                        //return false;
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Trace("TerminateConference:" + e.Message);
                return false;
            }
        }

        #endregion

        #region ConnectDisconnectEndpoint
        /// <summary>
        /// ConnectDisconnectEndpoint
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="party"></param>
        /// <param name="connectDisconnect"></param>
        /// <returns></returns>
        internal bool ConnectDisconnectEndpoint(NS_MESSENGER.Conference conf, NS_MESSENGER.Party party, bool connectDisconnect)
        {
            try
            {
                DMAorRPRM = (int)PolycomRedirect.FromService;
                settings = new XmlWriterSettings();
                str_build = new StringBuilder();
                conf.cMcu = party.cMcu;

                RestURI = "/api/rest/conferences/" + conf.sGUID + "/participants/" + party.sGUID+"/disconnect";
                RequestMethod = "POST";
                ContentType = "*/*";
                ret = false; str_build = new StringBuilder(); responseXml = "";
                ret = SendTransaction_overXMLHTTP(conf, str_build.ToString(), ContentType, RestURI, RequestMethod, DMAorRPRM, ref responseXml);
                if (!ret)
                {
                    logger.Exception(100, "TerminateConference failed");
                    return false;
                }
                else


                return true;
            }
            catch (Exception e)
            {

                logger.Trace("ConnectDisconnectEndpoint:" + e.Message);
                return false;
            }
        }
        #endregion

        #region ConferenceMessage
        /// <summary>
        /// ConferenceMessage
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="Message"></param>
        /// <returns></returns>
        internal bool ConferenceMessage(NS_MESSENGER.Conference conf, string Message)
        {
            try
            {
                DMAorRPRM = (int)PolycomRedirect.FromService;
                str_build = new StringBuilder();
                ContentType = "*/*";
                RequestMethod = "POST";
                RestURI = "/api/rest/conferences/" + conf.sGUID + "/set-display-text/" + Message;

                ret = false; responseXml = "";
                ret = SendTransaction_overXMLHTTP(conf, str_build.ToString(), ContentType, RestURI, RequestMethod, DMAorRPRM, ref responseXml);
                if (!ret)
                {
                    logger.Exception(100, "ConferenceMessage SendTransaction failed");
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                logger.Trace("ConferenceMessage:" + ex.Message);
                return false;

            }
        }
        #endregion

        #region LockUnlockConference
        /// <summary>
        /// LockUnlockConference
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="lockunlock"></param>
        /// <returns></returns>
        internal bool LockUnlockConference(NS_MESSENGER.Conference conf, int lockunlock)
        {
            try
            {
                DMAorRPRM = (int)PolycomRedirect.FromService;
                str_build = new StringBuilder();
                ContentType = "*/*";
                RequestMethod = "POST";

                if (lockunlock == (int)lockunlockconference.lockconfernece)
                    RestURI = "/api/rest/conferences/" + conf.sGUID + "/lock-conference";
                else
                    RestURI = "/api/rest/conferences/" + conf.sGUID + "/unlock-conference";


                ret = false; responseXml = "";
                ret = SendTransaction_overXMLHTTP(conf, str_build.ToString(), ContentType, RestURI, RequestMethod, DMAorRPRM, ref responseXml);
                if (!ret)
                {
                    logger.Exception(100, "LockUnlockConference SendTransaction failed");
                    return false;
                }


                return true;
            }
            catch (Exception ex)
            {
                logger.Trace("LockUnlockConference Block:" + ex.Message);
                return false;
            }
        }
        #endregion

        #region MuteUnMuteParties
        /// <summary>
        /// MuteUnMuteParties
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="PartyList"></param>
        /// <param name="mute"></param>
        /// <returns></returns>
        internal bool MuteUnMuteParties(NS_MESSENGER.Conference conf, List<NS_MESSENGER.Party> PartyList, bool mute)
        {
            try
            {
                DMAorRPRM = (int)PolycomRedirect.FromService;
                settings = new XmlWriterSettings();
                str_build = new StringBuilder();
                settings.CheckCharacters = false;
                settings.Encoding = Encoding.UTF8;
                xWriter = XmlWriter.Create(str_build, settings);

                ret = false;
                //mute true-MuteAllPartiesExcept; false-UnMuteAllParties; 
                if (mute)
                {
                    ret = CreateXML_MuteAllExceptParticipant(ref conf, PartyList, ref xWriter);

                    if (!ret || str_build.ToString().Length < 1)
                    {
                        logger.Exception(100, "Creation of CreateXML_Reservation failed");
                        return false;
                    }

                    if (str_build.ToString().Contains("utf-16"))
                        str_build.Replace("utf-16", "utf-8");
                    if (str_build.ToString().Contains(":n1"))
                        str_build.Replace(":n1", "");
                }

                ContentType = "*/*";
                RequestMethod = "POST";

                if (mute)
                    RestURI = "/api/rest/conferences/" + conf.sGUID + "/mute-all-except";
                else
                    RestURI = "/api/rest/conferences/" + conf.sGUID + "/unmute-all";

                ret = false; responseXml = "";
                ret = SendTransaction_overXMLHTTP(conf, str_build.ToString(), ContentType, RestURI, RequestMethod, DMAorRPRM, ref responseXml);
                if (!ret)
                {
                    logger.Exception(100, "MuteUnMuteParties SendTransaction failed");
                    return false;
                }
                return true;
            }

            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        #endregion

        #region StartStopRecord
        /// <summary>
        /// StartStopRecord
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="StartStopRecord"></param>
        /// <returns></returns>
        internal bool StartStopRecord(NS_MESSENGER.Conference conf, int StartStopRecord)
        {
            try
            {
                DMAorRPRM = (int)PolycomRedirect.FromService;
                str_build = new StringBuilder();
                ContentType = "*/*";
                RequestMethod = "POST";

                if (StartStopRecord == (int)Startstoprecord.startRecord)
                    RestURI = "/api/rest/conferences/" + conf.sGUID + "/start-recording";
                else
                    RestURI = "/api/rest/conferences/" + conf.sGUID + "/stop-recording";

                ret = false; responseXml = "";
                ret = SendTransaction_overXMLHTTP(conf, str_build.ToString(), ContentType, RestURI, RequestMethod, DMAorRPRM, ref responseXml);
                if (!ret)
                {
                    logger.Exception(100, "StartStopRecord SendTransaction failed ");
                    return false;
                }


                return true;
            }
            catch (Exception ex)
            {
                logger.Trace("Start Stop Record block:" + ex.Message);
                return false;
            }
        }
        #endregion

        #region ExtendConference
        /// <summary>
        /// ExtendConference
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="extendTime"></param>
        /// <returns></returns>
        internal bool ExtendConference(NS_MESSENGER.Conference conf, int extendTime)
        {
            logger.Trace("Entering Extend Conference function...");
            ret = false;
            string esID = "";
            try
            {
                if (conf != null)
                {
                    conf.iDuration = conf.iDuration + extendTime;
                    esID = conf.ESId;

                    settings = new XmlWriterSettings();
                    str_build = new StringBuilder();
                    settings.CheckCharacters = false;
                    settings.Encoding = Encoding.UTF8;
                    xWriter = XmlWriter.Create(str_build, settings);

                    ret = false;
                    ret = CreateXML_Reservation(ref conf, ref xWriter); //Call for Generate XML


                    if (!ret || str_build.ToString().Length < 1)
                    {
                        logger.Exception(100, "Creation of CreateXML_Reservation failed");
                        return false;
                    }

                    if (str_build.ToString().Contains("utf-16"))
                        str_build.Replace("utf-16", "utf-8");
                    if (str_build.ToString().Contains(":n1"))
                        str_build.Replace(":n1", "");
                    if (str_build.ToString().Contains(":n2"))
                        str_build.Replace(":n2", "");
                    if (str_build.ToString().Contains(":n3"))
                        str_build.Replace(":n3", "");

                    ContentType = "application/vnd.plcm.plcm-reservation+xml";
                    RequestMethod = "POST";
                    RestURI = "/api/rest/reservations";

                    //Send the xml to the bridge
                    ret = false;
                    DMAorRPRM = (int)PolycomRedirect.FromService;
                    responseXml = "";
                    ret = SendTransaction_overXMLHTTP(conf, str_build.ToString(), ContentType, RestURI, RequestMethod, DMAorRPRM, ref responseXml);
                    if (!ret)
                    {
                        logger.Exception(100, "SendTransaction failed");
                        return false;
                    }

                    // Parse the response.

                    if (esID.Trim() != "")
                    {

                        ret = false;
                        ret = db.UpdateConferenceExternalID(conf.iDbID, conf.iInstanceID, conf.ESId);
                        if (!ret)
                        {
                            logger.Exception(100, "ProcessXML_Reservation failed");
                            return false;
                        }
                    }


                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }

        #endregion

        #region MuteEndpoint
        /// <summary>
        /// MuteEndpoint
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="party"></param>
        /// <param name="mute"></param>
        /// <param name="PartyTerminalType"></param>
        /// <returns></returns>
        internal bool MuteEndpoint(NS_MESSENGER.Conference conf, NS_MESSENGER.Party party, bool mute, int PartyTerminalType)
        {
            logger.Trace("Entering MuteEndpoint function...");
            try
            {
                DMAorRPRM = (int)PolycomRedirect.FromService;
                settings = new XmlWriterSettings();
                str_build = new StringBuilder();
                conf.cMcu = party.cMcu;
                if (mute == true)
                {
                    if (PartyTerminalType == (int)NS_OPERATIONS.Operations.AudioVideoParty.Audio)
                        RestURI = "/api/rest/conferences/" + conf.sGUID + "/participants/" + party.sGUID + "/mute-audio";
                    else
                        RestURI = "/api/rest/conferences/" + conf.sGUID + "/participants/" + party.sGUID + "/suspend-video";
                }
                else
                {
                    if (PartyTerminalType == (int)NS_OPERATIONS.Operations.AudioVideoParty.Audio)
                        RestURI = "/api/rest/conferences/" + conf.sGUID + "/participants/" + party.sGUID + "/unmute-audio";
                    else
                        RestURI = "/api/rest/conferences/" + conf.sGUID + "/participants/" + party.sGUID + "/resume-video";
                }

                ContentType = "*/*";
                RequestMethod = "POST";

                ret = false; responseXml = "";
                ret = SendTransaction_overXMLHTTP(conf, str_build.ToString(), ContentType, RestURI, RequestMethod, DMAorRPRM, ref responseXml);
                if (!ret)
                {
                    logger.Exception(100, "MuteEndpoint SendTransaction failed");
                    return false;
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }

        }
        #endregion

        #region GetETagValue
        /// <summary>
        /// GetETagValue
        /// </summary>
        /// <param name="URI"></param>
        /// <param name="credentials"></param>
        /// <returns></returns>
        internal bool GetETagValue(string URI, string credentials,ref DateTime Date,ref string Etag)
        {
            try
            {
                System.Net.ServicePointManager.ServerCertificateValidationCallback =
                    ((sender1, certificate, chain, sslPolicyErrors) => true);
                ServicePointManager.Expect100Continue = false;

                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(URI);
                //req.Credentials = cache;

                req.MediaType = "HTTP::/1.1";
                req.Headers["Authorization"] = "Basic " + credentials;
                req.Method = "GET";
                req.KeepAlive = true;
                HttpWebResponse resp = (HttpWebResponse)req.GetResponse();
                if (resp.Headers["ETag"] != null)
                    Etag = resp.Headers["ETag"].ToString();
                if (resp.Headers["Date"] != null)
                    Date = DateTime.Parse(resp.Headers["Date"].ToString());
                return true;

            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
           
        }
        #endregion

        #region AddPartyToMcu

        /// <summary>
        /// AddPartyToMcu
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="party"></param>
        /// <returns></returns>
        internal bool AddPartyToMcu(NS_MESSENGER.Conference conf, NS_MESSENGER.Party party)
        {
            try
            {
                DMAorRPRM = (int)PolycomRedirect.FromService;
                settings = new XmlWriterSettings();
                str_build = new StringBuilder();
                settings.CheckCharacters = false;
                settings.Encoding = Encoding.UTF8;
                xWriter = XmlWriter.Create(str_build, settings);

                CreatePartyListXML(ref conf, ref xWriter); //Call for Generate Participant XML

                xWriter.Flush();

                if (!ret || str_build.ToString().Length < 1)
                {
                    logger.Exception(100, "Creation of AddPartyToMcu failed . commonXML = " + str_build.ToString());
                    return false;
                }

                if (str_build.ToString().Contains("utf-16"))
                    str_build.Replace("utf-16", "utf-8");

                if (str_build.ToString().Contains(":n3"))
                    str_build.Replace(":n3", "");
                ContentType = "*/*";
                ContentType = "POST";
                RestURI = "/api/rest/conferences/" + conf.sGUID + "/participants";


                ret = false; responseXml = "";
                ret = SendTransaction_overXMLHTTP(conf, str_build.ToString(), ContentType, RestURI, RequestMethod, DMAorRPRM, ref responseXml);
                if (!ret)
                {
                    logger.Exception(100, "AddPartyToMcu SendTransaction failed");
                    return false;
                }

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        #endregion

        #region DeletePartyFromMcu
        /// <summary>
        /// DeletePartyFromMcu
        /// </summary>
        /// <param name="party"></param>
        /// <param name="dbConfName"></param>
        /// <returns></returns>
        internal bool DeletePartyFromMcu(NS_MESSENGER.Party party, string dbConfName)
        {
            try
            {
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        #endregion

        #region  OngoingConfOps
        /// <summary>
        /// OngoingConfOps
        /// </summary>
        /// <param name="operation"></param>
        /// <param name="conf"></param>
        /// <param name="party"></param>
        /// <param name="msg"></param>
        /// <param name="newMuteValue"></param>
        /// <param name="newLayoutValue"></param>
        /// <param name="extendTime"></param>
        /// <param name="connectOrDisconnect"></param>
        /// <param name="terminalStatus"></param>
        /// <returns></returns>
        internal bool OngoingConfOps(string operation, NS_MESSENGER.Conference conf, int layout)
        {
            try
            {
                bool ret = false;
                switch (operation)
                {
                    case "ChangeConferenceDisplayLayout":
                        {
                            ret = false;
                            ret = ChangeDisplayLayoutOfConference(conf, layout);
                            if (!ret) return false;
                            break;
                        }
                    case "TerminateConference":
                        {
                            ret = false;
                            ret = TerminateConference(conf);
                            if (!ret) return false;
                            break;
                        }

                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        #endregion

        #region ChangeDisplayLayoutOfConference
        /// <summary>
        /// ChangeDisplayLayoutOfConference
        /// </summary>
        /// <param name="newLayoutPartyXml"></param>
        /// <param name="mcu"></param>
        /// <param name="confIdOnMcu"></param>
        /// <param name="layout"></param>
        /// <returns></returns>
        private bool ChangeDisplayLayoutOfConference(NS_MESSENGER.Conference conf, int layout)
        {
            try
            {
                DMAorRPRM = (int)PolycomRedirect.FromService;
                str_build = new StringBuilder();
                ContentType = "*/*";
                RequestMethod = "POST";
                string strLayout = "";

                ret = false;
                ret = EquateVideoLayout(layout, ref strLayout);

                RestURI = "/api/rest/conferences/" + conf.sGUID + "/set-conference-layout/" + strLayout;  // NEED to change the Dynamic Layout 

                ret = false; responseXml = "";
                ret = SendTransaction_overXMLHTTP(conf, str_build.ToString(), ContentType, RestURI, RequestMethod, DMAorRPRM, ref responseXml);
                if (!ret)
                {
                    logger.Exception(100, "ChangeDisplayLayoutOfConference SendTransaction failed");
                    return false;
                }

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        #endregion

        #region CheckMcuTime
        /// <summary>
        /// CheckMcuTime
        /// </summary>
        /// <param name="mcu"></param>
        /// <returns></returns>
        internal bool FetchMCUDetails(NS_MESSENGER.MCU mcu)
        {
            try
            {
                MCUIP = mcu.sDMAip;
                MCUPort = mcu.iDMAHttpport;
                MCULogin = mcu.sDMALogin;
                MCUPassword = mcu.sDMAPassword;
                
                if (MCUPort == 8443)
                    strUrl = "https://" + MCUIP + ":" + MCUPort;
                else
                    strUrl = "http://" + MCUIP + ":" + MCUPort;

                credentials = MCULogin + ":" + MCUPassword;
                credentials = encodeString(credentials);

                Date = new DateTime(); ret = false;
                ret = GetETagValue(strUrl, credentials, ref Date, ref Etag);
                if (!ret)
                {

                    logger.Exception(100, "FetchMCUProfileDetails failed");
                    return false;
                }
                else
                    if (Date != DateTime.MinValue)
                    {
                        db = new NS_DATABASE.Database(configParams);
                        db.UpdateMcuCurrentTime(mcu.iDbId, Date);
                    }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }


            //return true;
        }
        #endregion

        #region GetDMAConferenceDetails
        /// <summary>
        /// GetDMAConferenceDetails
        /// </summary>
        /// <param name="conf"></param>
        /// <returns></returns>
        public bool GetDMAConferenceDetails(NS_MESSENGER.Conference conf)
        {
            try
            {
                DMAorRPRM = (int)PolycomRedirect.FromService;
                str_build = new StringBuilder();
                ContentType = "application/vnd.plcm.plcm-conference-list+xml";
                RequestMethod = "GET";
                RestURI = "/api/rest/conferences?conference-room-identifier=" + conf.sDialString.Remove(0, 2);

                ret = false; responseXml = "";
                ret = SendTransaction_overXMLHTTP(conf, str_build.ToString(), ContentType, RestURI, RequestMethod, DMAorRPRM, ref responseXml);
                if (!ret)
                {
                    logger.Exception(100, "GetDMAConferenceDetails SendTransaction failed" + responseXml);
                    return false;
                }

                if (responseXml.Length > 0)
                {
                    xdoc = XDocument.Parse(responseXml);
                    XNamespace ns4 = "urn:com:polycom:api:rest:plcm-conference-list";
                    XNamespace ns3 = "urn:com:polycom:api:rest:plcm-conference";
                    conf.sGUID = xdoc.Element(ns4 + "plcm-conference-list").Element(ns3 + "plcm-conference").Element(ns3 + "conference-identifier").Value;

                    ret = db.UpdateConference(conf.iDbID, conf.iImmediate, conf.sGUID);
                    if (!ret)
                        logger.Trace("GUID Updatation is failed");
                }
            }
            catch (Exception ex)
            {
                logger.Trace("GetDMAConference failed: " + ex.Message);
                return false;
            }

            return true;
        }
        #endregion

        #endregion

        #region SendCommand
        /// <summary>
        /// SendTransaction_overXMLHTTP
        /// </summary>
        /// <param name="mcu"></param>
        /// <param name="sendXML"></param>
        /// <param name="receiveXML"></param>
        /// <param name="method"></param>
        /// <param name="esID"></param>
        /// <returns></returns>
        private bool SendTransaction_overXMLHTTP(NS_MESSENGER.Conference conf, string sendXML, string ContentType, string RestURI, string method, int DMAorRPRM, ref string responseXml)
        {

            String[] delimitedArr = { @"\" };
            string error = "";

            if (sendXML.Length < 1)
            {
                logger.Trace("No data being sent so it is delete command");

            }
            try
            {
                bool tryAgain = false;
                do
                {
                    tryAgain = false;

                    if (DMAorRPRM == (int)PolycomRedirect.FromWebsite)
                    {
                        MCUIP = conf.cMcu.sIp.Trim();
                        MCUPort = conf.cMcu.iHttpPort;
                        MCULogin = conf.cMcu.sLogin;
                        MCUPassword = conf.cMcu.sPwd;
                    }
                    else
                    {
                        MCUIP = conf.cMcu.sDMAip;
                        MCUPort = conf.cMcu.iDMAHttpport;
                        MCULogin = conf.cMcu.sDMALogin;
                        MCUPassword = conf.cMcu.sDMAPassword;
                    }

                    if (MCUPort == 8443)
                        strUrl = "https://" + MCUIP + ":" + MCUPort + RestURI;
                    else if (MCUPort == 443)
                        strUrl = "https://" + MCUIP + RestURI;
                    else
                        strUrl = "http://" + MCUIP + ":" + MCUPort;

                    logger.Trace("*********SendXML***********");
                    logger.Trace(sendXML);

                    Uri mcuUri = new Uri(strUrl);


                    credentials = MCULogin + ":" + MCUPassword;
                    credentials = encodeString(credentials);

                    System.Net.ServicePointManager.ServerCertificateValidationCallback =
                    ((sender1, certificate, chain, sslPolicyErrors) => true);
                    ServicePointManager.Expect100Continue = false;

                    HttpWebRequest req = (HttpWebRequest)WebRequest.Create(strUrl);

                    req.MediaType = "HTTP::/1.1";
                    req.Headers["Authorization"] = "Basic " + credentials;
                    req.Method = method;
                    req.KeepAlive = true;
                    if (sendXML != "")
                    {
                        req.ContentType = ContentType;
                        req.Headers.Add("Accept-Encoding", "gzip,deflate");//x-www-form-urlencoded";
                        System.IO.Stream stream = req.GetRequestStream();
                        byte[] arrBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(sendXML);
                        stream.Write(arrBytes, 0, arrBytes.Length);
                        stream.Close();
                    }
                    HttpWebResponse resp = (HttpWebResponse)req.GetResponse();

                    Stream respStream = resp.GetResponseStream();
                    StreamReader rdr = new StreamReader(respStream, System.Text.Encoding.ASCII);
                    responseXml = rdr.ReadToEnd();
                    logger.Trace("*********ResponseXML***********");
                    logger.Trace(responseXml);
                    if (DMAorRPRM == (int)PolycomRedirect.FromWebsite)
                    {
                        if (resp.Headers["Location"] != null)
                        {
                            conf.ESId = resp.Headers["Location"].ToString();
                            conf.ESId = conf.ESId.Split('/')[conf.ESId.Split('/').Length - 1];
                            logger.Trace("Reservation Succeed:" + resp.StatusDescription);
                        }
                        if (RestURI == "/api/rest/status")
                        {
                            if (resp.StatusDescription.ToString() == "No Content")
                                return true;
                            else
                                return false;
                        }
                    }
                }
                while (tryAgain == true);
                return true;
            }

            catch (WebException wex)
            {
                if (wex.Response != null)
                {
                    using (var errorResponse = (HttpWebResponse)wex.Response)
                    {
                        using (var reader = new StreamReader(errorResponse.GetResponseStream()))
                        {
                            error = reader.ReadToEnd();
                        }
                    }
                }

                if (error == "")
                    throw wex;

                this.errMsg = ProcessError(error);
                return false;



            }

            catch (Exception e)
            {
                this.errMsg = e.Message;
                string debugString = "Error sending data to bridge. Error = " + e.Message;
                debugString = "SendXML = " + sendXML;
                logger.Exception(100, debugString);
                return false;
            }

        }
        #endregion

        #region GenerateXML


        #region CreateXML_Reservation
        /// <summary>
        /// Block for Generate Reservation XML
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="reservationXml"></param>
        /// <returns></returns>
        private bool CreateXML_Reservation(ref NS_MESSENGER.Conference conf, ref XmlWriter xWriter)
        {
            try
            {
                string confStartTime = "", ConfEndTime = "";
                TimeSpan timeDiff = conf.dtStartDateTimeInUTC.Subtract(DateTime.UtcNow);

                if (timeDiff.Days != 0 || timeDiff.Hours != 0 || timeDiff.Minutes >= 5)
                    confStartTime = conf.dtStartDateTimeInUTC.ToString(@"yyyy-MM-dd\THH:mm:ss\Z");    // scheduled reservation
                else
                    confStartTime = DateTime.UtcNow.AddSeconds(15).ToString(@"yyyy-MM-dd\THH:mm:ss\Z");// immediate conf. so it needs adjustment.


                ConfEndTime = conf.dtStartDateTimeInUTC.AddMinutes(conf.iDuration).ToString(@"yyyy-MM-dd\THH:mm:ss\Z");

                xWriter.WriteStartElement("plcm-reservation");
                xWriter.WriteAttributeString("xmlns", "n1", null, "urn:com:polycom:api:rest:plcm-reservation");
                xWriter.WriteStartElement("plcm-reserved-participant-list");
                xWriter.WriteAttributeString("xmlns", "n2", null, "urn:com:polycom:api:rest:plcm-reserved-participant-list");


                CreatePartyListXML(ref conf, ref xWriter);  // Call for  ADD participant XML
                xWriter.WriteElementString("name", conf.sDbName);

                if (conf.ESId != "")
                    xWriter.WriteElementString("reservation-identifier", conf.ESId);

                if (conf.sPwd.Trim() != "")
                    xWriter.WriteElementString("conf-passcode", conf.sPwd);

                xWriter.WriteElementString("chair-passcode", ""); // chair person and passcode is not myvrm so set as empty

                if (conf.sPolycomTemplate != null && conf.sPolycomTemplate != "")
                    xWriter.WriteElementString("template-name", conf.sPolycomTemplate);
                else
                    xWriter.WriteElementString("template-name", conf.cMcu.sDMATemplate);


                if (conf.cMcu.iDMASendmail == 1 && conf.iPolycomSendMail == 1)
                    xWriter.WriteElementString("send-email", "true");
                else
                    xWriter.WriteElementString("send-email", "false");

                xWriter.WriteElementString("supported-language-enum", "ENGLISH");
                if (conf.ESId == "")
                    xWriter.WriteElementString("dial-in-number", conf.iDbNumName.ToString());
                else
                {
                    xWriter.WriteElementString("dial-in-number", conf.sDialString.ToString());
                    xWriter.WriteElementString("entity-tag", conf.sEtag);
                }
                xWriter.WriteElementString("start-time", confStartTime);
                xWriter.WriteElementString("end-time", ConfEndTime); //2012-03-17T02:01:01+05:30 
                xWriter.WriteEndElement();
                xWriter.Flush();
            }

            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
            return true;
        }//end of Reservation XML
        #endregion

        #region CreatePartyListXML

        /// <summary>
        /// CreatePartyListXML
        /// </summary>
        /// <param name="conf"></param>
        /// <returns></returns>


        private void CreatePartyListXML(ref NS_MESSENGER.Conference conf, ref XmlWriter xWriter)
        {

            string setConfowner = "true";   // As of now it is set as true need to change depends on host
            try
            {
                while (conf.qParties.Count > 0)
                {
                    NS_MESSENGER.Party party = new NS_MESSENGER.Party();
                    party = (NS_MESSENGER.Party)conf.qParties.Dequeue();

                    if (conf.stLineRate.etLineRate < party.stLineRate.etLineRate)
                        party.stLineRate.etLineRate = conf.stLineRate.etLineRate;

                    SinglePartyXML(ref conf, ref party, ref setConfowner, ref xWriter);

                }

                xWriter.WriteEndElement();

            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);

            }

        }
        #endregion

        #region SinglePartyXML
        /// <summary>
        /// SinglePartyXML
        /// </summary>
        /// <param name="Conf"></param>
        /// <param name="partyObj"></param>
        /// <returns></returns>
        internal void SinglePartyXML(ref NS_MESSENGER.Conference Conf, ref NS_MESSENGER.Party partyObj, ref string setConfOwner, ref XmlWriter xWriter)
        {
            string videoBitRate = "";

            try
            {
                logger.Trace("Party = " + partyObj.sName);
                if (partyObj.sName == null || partyObj.sName.Length < 3)
                {
                    logger.Trace("Invalid participant Name:");
                }

                EquateLineRate(partyObj.stLineRate.etLineRate, true, ref videoBitRate);

                xWriter.WriteStartElement("plcm-reserved-participant");
                xWriter.WriteAttributeString("xmlns", "n3", null, "urn:com:polycom:api:rest:plcm-reserved-participant");

                if (setConfOwner == "true")
                {
                    xWriter.WriteElementString("username", Conf.cMcu.sLogin);
                    xWriter.WriteElementString("domain", Conf.cMcu.sDMADomain); // Need to Get the Dynamic domain name
                }
                xWriter.WriteElementString("participant-name", partyObj.sName);
                xWriter.WriteElementString("connection-type-enum", AddressType(ref partyObj.etAddressType));
                xWriter.WriteElementString("dial-direction-enum", DialDirection(ref partyObj.etConnType));
                xWriter.WriteElementString("dial-number", partyObj.sAddress.Trim());
                xWriter.WriteElementString("chairperson", "false"); // Chair Person is not in myvrm so set as False
                xWriter.WriteElementString("conf-owner", setConfOwner.ToString());
                xWriter.WriteElementString("email-address", partyObj.Emailaddress);
                xWriter.WriteEndElement();

                setConfOwner = "false";

            }
            catch (Exception e)
            {
                logger.Exception(100, e.StackTrace);

            }
        }
        #endregion

        #region CreateXML_PartyList
        /// <summary>
        /// CreateXML_PartyList
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="partyList"></param>
        /// <returns></returns>
        private bool CreateXML_PartyList(ref NS_MESSENGER.Conference conf, ref string partyList)
        {
            try
            {
                //Create the participant xml list 
                string party = null;
                logger.Trace("Create Party List count =" + conf.qParties.Count.ToString());

                //cycle through each participant.
                while (conf.qParties.Count > 0)
                {
                    //individual party info
                    NS_MESSENGER.Party partyObj = new NS_MESSENGER.Party();
                    partyObj = (NS_MESSENGER.Party)conf.qParties.Dequeue();

                    // Requested by Dan for Yorktel - Disney (Jan, 29 2010)
                    // check line rate of party with conference. if conference is lesser than party, then use conference line rate. Else, don't change anything.
                    if (conf.stLineRate.etLineRate < partyObj.stLineRate.etLineRate)
                    {
                        partyObj.stLineRate.etLineRate = conf.stLineRate.etLineRate;
                    }

                    bool ret = CreateXML_Party(ref party, partyObj);
                    if (!ret) party = null;

                    // append party info to partylist
                    partyList += party;
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        #endregion

        #region CreateXML_Party
        /// <summary>
        /// CreateXML_Party
        /// </summary>
        /// <param name="partyXml"></param>
        /// <param name="partyObj"></param>
        /// <returns></returns>
        private bool CreateXML_Party(ref string partyXml, NS_MESSENGER.Party partyObj)
        {
            logger.Trace("Party = " + partyObj.sName);
            if (partyObj.sName == null)
            {
                this.errMsg = "Invalid participant name.";
                return false;
            }
            if (partyObj.sName.Length < 3)
            {
                this.errMsg = "Invalid participant name.";
                return false;
            }

            partyXml = null;
            try
            {
                if (partyObj.etProtocol == NS_MESSENGER.Party.eProtocol.IP)
                {
                    // ip participant 
                    partyXml = "<PARTY>";
                    partyXml += "<NAME>" + partyObj.sName + "</NAME>";
                    partyXml += "<ID>" + partyObj.iDbId.ToString() + "</ID>";
                    partyXml += "<PHONE_LIST></PHONE_LIST>";
                    partyXml += "<INTERFACE>h323</INTERFACE>";
                    if (partyObj.etConnType == NS_MESSENGER.Party.eConnectionType.DIAL_IN)
                    {
                        partyXml += "<CONNECTION>dial_in</CONNECTION>";
                    }
                    else
                    {
                        if (partyObj.etConnType == NS_MESSENGER.Party.eConnectionType.DIAL_OUT)
                        {
                            partyXml += "<CONNECTION>dial_out</CONNECTION>";
                        }
                        else
                        {
                            partyXml += "<CONNECTION>direct</CONNECTION>";
                        }
                    }
                    //Meet me method
                    partyXml += "<MEET_ME_METHOD>party</MEET_ME_METHOD>";
                    partyXml += "<NUM_TYPE>taken_from_service</NUM_TYPE>";
                    partyXml += "<BONDING>auto</BONDING>";
                    partyXml += "<MULTI_RATE>auto</MULTI_RATE>";
                    partyXml += "<NET_CHANNEL_NUMBER>auto</NET_CHANNEL_NUMBER>";

                    //if (partyObj.etVideoProtocol == NS_MESSENGER.Party.eVideoProtocol.AUTO)
                    if (configParams.client == "VAOHIO")
                    {
                        partyXml += "<VIDEO_PROTOCOL>h263</VIDEO_PROTOCOL>";
                    }
                    else
                    {
                        partyXml += "<VIDEO_PROTOCOL>auto</VIDEO_PROTOCOL>";
                    }
                    //else
                    //{
                    //	if (partyObj.etVideoProtocol == NS_MESSENGER.Party.eVideoProtocol.H261)
                    //		partyXml += "<VIDEO_PROTOCOL>h261</VIDEO_PROTOCOL>"; 
                    //	else 
                    //		partyXml += "<VIDEO_PROTOCOL>h263</VIDEO_PROTOCOL>"; 
                    //}

                    //video conf = framed , audio conf = voice

                    if (partyObj.etCallType == NS_MESSENGER.Party.eCallType.AUDIO)
                    {
                        partyXml += "<CALL_CONTENT>voice</CALL_CONTENT>";
                    }
                    else
                    {
                        partyXml += "<CALL_CONTENT>framed</CALL_CONTENT>";
                    }

                    // Check if the address is an alias or an IP

                    if (partyObj.etAddressType == NS_MESSENGER.Party.eAddressType.H323_ID || partyObj.etAddressType == NS_MESSENGER.Party.eAddressType.IP_ADDRESS)
                    {
                        // IP address or H323.ID 
                        partyXml += "<ALIAS><NAME /><ALIAS_TYPE>323_id</ALIAS_TYPE></ALIAS>";
                        partyXml += "<IP>" + partyObj.sAddress.Trim() + "</IP>";
                    }
                    else
                    {
                        if (partyObj.etAddressType == NS_MESSENGER.Party.eAddressType.E_164)
                        {
                            // It is an E164 alias.
                            partyXml += "<ALIAS><NAME>" + partyObj.sAddress + "</NAME>";
                            partyXml += "<ALIAS_TYPE>e164</ALIAS_TYPE></ALIAS>";
                            partyXml += "<IP>0.0.0.0</IP>";
                        }
                        else
                        {
                            logger.Exception(100, "Invalid participant address type");
                        }
                    }
                    partyXml += "<SIGNALING_PORT>1720</SIGNALING_PORT>";
                    partyXml += "<VOLUME>5</VOLUME>";
                    partyXml += "<MCU_PHONE_LIST/>";
                    partyXml += "<BONDING_PHONE/>";

                    // mcu service name
                    partyXml += "<SERVICE_NAME>" + partyObj.sMcuServiceName + "</SERVICE_NAME>";

                    partyXml += "<SUB_SERVICE_NAME></SUB_SERVICE_NAME>";
                    partyXml += "<AUTO_DETECT>false</AUTO_DETECT>";
                    partyXml += "<RESTRICT>false</RESTRICT>";
                    partyXml += "<ENHANCED_VIDEO>false</ENHANCED_VIDEO>";

                    //FB 1742 start.
                    string videoBitRate = "automatic";
                    if (partyObj.cMcu.etType == NS_MESSENGER.MCU.eType.RMX)
                    {
                        ret = false;
                        ret = EquateLineRate(partyObj.stLineRate.etLineRate, true, ref videoBitRate);
                        if (!ret)
                        {
                            logger.Trace("Invalid Video Bit Rate.");
                        }
                    }
                    //FB 1742 end

                    if (configParams.client == "VAOHIO")
                    {
                        partyXml += "<VIDEO_BIT_RATE>384</VIDEO_BIT_RATE>";
                    }
                    else
                    {
                        partyXml += "<VIDEO_BIT_RATE>" + videoBitRate + "</VIDEO_BIT_RATE>";
                    }
                    partyXml += "<AGC>true</AGC>";

                    if (partyObj.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv7)
                    {
                        partyXml += "<IP_QOS>";
                        partyXml += "<QOS_ACTION>from_service</QOS_ACTION>";
                        partyXml += "<QOS_DIFF_SERV>precedence</QOS_DIFF_SERV>";
                        partyXml += "<QOS_IP_AUDIO>4</QOS_IP_AUDIO>";
                        partyXml += "<QOS_IP_VIDEO>4</QOS_IP_VIDEO>";
                        partyXml += "<QOS_TOS>delay</QOS_TOS>";
                        partyXml += "</IP_QOS>";
                        partyXml += "<RECORDING_PORT>no</RECORDING_PORT>";
                        partyXml += "<LAYOUT_TYPE>conference</LAYOUT_TYPE>";
                        partyXml += "<PERSONAL_LAYOUT>1x1</PERSONAL_LAYOUT>";
                        partyXml += "<PERSONAL_FORCE_LIST />";
                        partyXml += "<VIP>false</VIP> ";
                        partyXml += "<CONTACT_INFO_LIST />";
                        partyXml += "<LISTEN_VOLUME>5</LISTEN_VOLUME>";
                        partyXml += "<SIP_ADDRESS />";
                        partyXml += "<SIP_ADDRESS_TYPE>uri_type</SIP_ADDRESS_TYPE>";
                        partyXml += "<WEB_USER_ID>0</WEB_USER_ID>";
                        partyXml += "<UNDEFINED>false</UNDEFINED>";
                        partyXml += "<BACKUP_SERVICE_NAME />";
                        partyXml += "<BACKUP_SUB_SERVICE_NAME />";
                        partyXml += "<DEFAULT_TEMPLATE>false</DEFAULT_TEMPLATE>";
                        partyXml += "<NODE_TYPE>terminal</NODE_TYPE>";
                        partyXml += "<ENCRYPTION_EX>auto</ENCRYPTION_EX>";
                        partyXml += "<H323_PSTN>false</H323_PSTN>";
                        partyXml += "<EMAIL />";
                        partyXml += "<IS_RECORDING_LINK_PARTY>false</IS_RECORDING_LINK_PARTY>";
                        partyXml += "<USER_IDENTIFIER_STRING />";
                        partyXml += "<IDENTIFICATION_METHOD>called_phone_number</IDENTIFICATION_METHOD>";
                    }
                    partyXml += "</PARTY>";
                }
                else if (partyObj.etProtocol == NS_MESSENGER.Party.eProtocol.ISDN)
                {
                    // ISDN participant
                    string partyAddress = "";  //FB 1740 - Audio Add-on implementation for Polycom sites
                    string phonePrefix = "";
                    partyAddress = partyObj.sAddress.Trim();

                    partyXml = "<PARTY>";
                    partyXml += "<NAME>" + partyObj.sName + "</NAME>";
                    partyXml += "<ID>" + partyObj.iDbId.ToString() + "</ID>";

                    // participant's isdn address : to be added only in dial-out scenario
                    if (partyObj.etConnType == NS_MESSENGER.Party.eConnectionType.DIAL_OUT)
                    {
                        //FB 1740 - Audio Add-on implementation for Polycom sites - code start

                        //partyXml += "<PHONE_LIST><PHONE1>" + partyObj.sAddress.Trim() + "</PHONE1></PHONE_LIST>";
                        if (partyAddress.Trim().Contains("G"))
                        {
                            int Gindex = partyAddress.IndexOf("G");
                            partyAddress = partyAddress.Substring(0, Gindex);
                        }
                        if (partyAddress.Trim().Contains("D"))
                        {
                            int Dindex = partyAddress.IndexOf("D");
                            partyAddress = partyAddress.Substring(0, Dindex);

                            //Audio Dial-in prefix is added before the address
                            phonePrefix = configParams.audioDialNoPrefix;
                        }

                        //if (partyObj.etVideoEquipment == NS_MESSENGER.Party.eVideoEquipment.RECORDER)
                        //{ 
                        //    //send gatewayAddress
                        //}
                        // check for gateway  
                        if (partyObj.sAddress.Trim().Contains("G"))
                        {
                            // gateway exists
                            if (partyObj.sAddress.Trim().Contains("D"))
                            {
                                // dtmf sequence also exists. Hence, gateway value needs to be extracted between G & D.
                                int Gindex = partyObj.sAddress.IndexOf("G");
                                int Dindex = partyObj.sAddress.IndexOf("D");
                                partyObj.sGatewayAddress = partyObj.sAddress.Substring(Gindex + 1, Dindex - Gindex - 1);
                            }
                            else
                            {
                                int Gindex = partyObj.sAddress.IndexOf("G");
                                partyObj.sGatewayAddress = partyObj.sAddress.Substring(Gindex + 1);
                            }
                        }
                        // check for dtmf sequence
                        if (partyObj.sAddress.Trim().Contains("D"))
                        {
                            // dtmf sequence exists                            
                            int Dindex = partyObj.sAddress.IndexOf("D");
                            partyObj.sDTMFSequence = partyObj.sAddress.Substring(Dindex + 1);

                            // FB case#1632
                            // GENERIC STRING: ,,,,,,,,CONFCODE,#,,,,,*,,,,,LEADERPIN,# ,,,,,1 (MGC uses "p" for pauses)

                            // add the pre-ConfCode DTMF codes
                            partyObj.sDTMFSequence = partyObj.cMcu.preConfCode + partyObj.sDTMFSequence;
                            //partyObj.sDTMFSequence = partyObj.cMcu.preConfCode + ":" + partyObj.sDTMFSequence;

                            // add the pre-LeaderPIN DTMF codes
                            partyObj.sDTMFSequence = partyObj.sDTMFSequence.Replace("+", partyObj.cMcu.preLPin);
                            //partyObj.sDTMFSequence = partyObj.sDTMFSequence.Replace("+", (":"+ partyObj.cMcu.preLPin));

                            // add the post-LeaderPIN DTMF codes
                            partyObj.sDTMFSequence = partyObj.sDTMFSequence.Trim() + partyObj.cMcu.postLPin;
                            //partyObj.sDTMFSequence = partyObj.sDTMFSequence.Trim() +":"+ partyObj.cMcu.postLPin;
                        }

                        if (partyObj.sDTMFSequence != null)
                        {

                            if (partyObj.sDTMFSequence.Trim() != "")
                            {
                                partyXml += "<PHONE_LIST><PHONE1>" + phonePrefix.Trim() + partyAddress.Trim() + "</PHONE1></PHONE_LIST>";
                            }
                            else
                            {
                                partyXml += "<PHONE_LIST><PHONE1>" + partyAddress.Trim() + "</PHONE1></PHONE_LIST>";
                            }


                        }
                        else
                        {
                            partyXml += "<PHONE_LIST><PHONE1>" + partyAddress.Trim() + "</PHONE1></PHONE_LIST>";
                        }
                        //FB 1740 - Audio Add-on implementation for Polycom sites - code end
                    }
                    else
                    {
                        partyXml += "<PHONE_LIST></PHONE_LIST>";
                    }

                    // isdn 
                    partyXml += "<INTERFACE>isdn</INTERFACE>";

                    // connection type
                    if (partyObj.etConnType == NS_MESSENGER.Party.eConnectionType.DIAL_IN)
                    {
                        partyXml += "<CONNECTION>dial_in</CONNECTION>";
                    }
                    else
                    {
                        if (partyObj.etConnType == NS_MESSENGER.Party.eConnectionType.DIAL_OUT)
                        {
                            partyXml += "<CONNECTION>dial_out</CONNECTION>";
                        }
                        else
                        {
                            partyXml += "<CONNECTION>direct</CONNECTION>";
                        }
                    }
                    partyXml += "<MEET_ME_METHOD>party</MEET_ME_METHOD>";
                    logger.Trace("After meet me method...");
                    partyXml += "<NUM_TYPE>taken_from_service</NUM_TYPE>";
                    partyXml += "<BONDING>auto</BONDING>";
                    partyXml += "<MULTI_RATE>disabled</MULTI_RATE>";

                    // ISDN channels.
                    string polycomLineRate = "channel_6";
                    ret = false;
                    ret = EquateLineRate(partyObj.stLineRate.etLineRate, false, ref polycomLineRate);
                    if (!ret)
                    {
                        logger.Trace("Line rate conversion failed.");
                    }
                    partyXml += "<NET_CHANNEL_NUMBER>" + polycomLineRate + "</NET_CHANNEL_NUMBER>";

                    //if (partyObj.m_videoProtocol == NS_MESSENGER.Party.eVideoProtocol.AUTO)
                    //	partyXml += "<VIDEO_PROTOCOL>auto</VIDEO_PROTOCOL>"; 
                    //else
                    //{
                    //	if (partyObj.m_videoProtocol == NS_MESSENGER.Party.eVideoProtocol.H261)
                    partyXml += "<VIDEO_PROTOCOL>h261</VIDEO_PROTOCOL>";
                    //	else 
                    //		partyXml += "<VIDEO_PROTOCOL>h263</VIDEO_PROTOCOL>"; 
                    //}

                    //video conf = framed , audio conf = voice
                    if (partyObj.etCallType == NS_MESSENGER.Party.eCallType.AUDIO)
                    {
                        partyXml += "<CALL_CONTENT>voice</CALL_CONTENT>";
                    }
                    else
                    {
                        partyXml += "<CALL_CONTENT>framed</CALL_CONTENT>";
                    }

                    partyXml += "<ALIAS><NAME /><ALIAS_TYPE>323_id</ALIAS_TYPE></ALIAS>";
                    partyXml += "<IP>255.255.255.255</IP>";
                    partyXml += "<SIGNALING_PORT>1720</SIGNALING_PORT>";
                    partyXml += "<VOLUME>5</VOLUME>";

                    // mcu's isdn address : to be added only in dial-in scenario
                    if (partyObj.etConnType == NS_MESSENGER.Party.eConnectionType.DIAL_IN)
                    {
                        partyXml += "<MCU_PHONE_LIST><PHONE1>" + partyObj.sMcuAddress.Trim() + "</PHONE1></MCU_PHONE_LIST>";
                    }
                    else
                    {
                        partyXml += "<MCU_PHONE_LIST><PHONE1></PHONE1></MCU_PHONE_LIST>";
                    }

                    if (partyObj.etConnType == NS_MESSENGER.Party.eConnectionType.DIAL_OUT)
                    {
                        if (partyObj.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv7)
                        {
                            partyXml += "<BONDING_PHONE/>"; //FB 1740 Audio Add-on implementation for Polycom
                        }
                        else
                        {
                            //partyXml += "<BONDING_PHONE>" + partyObj.sAddress.Trim() + "</BONDING_PHONE>"; //Commented during FB 1740
                            partyXml += "<BONDING_PHONE>" + partyAddress.Trim() + "</BONDING_PHONE>"; //FB 1740 - Audio Add-on implementation for Polycom
                        }
                    }
                    else
                    {
                        partyXml += "<BONDING_PHONE></BONDING_PHONE>";
                    }

                    // mcu service name
                    partyXml += "<SERVICE_NAME>" + partyObj.sMcuServiceName.Trim() + "</SERVICE_NAME>";

                    partyXml += "<SUB_SERVICE_NAME>ZERO</SUB_SERVICE_NAME>";
                    partyXml += "<AUTO_DETECT>false</AUTO_DETECT>";
                    partyXml += "<RESTRICT>false</RESTRICT>";
                    partyXml += "<ENHANCED_VIDEO>false</ENHANCED_VIDEO>";
                    partyXml += "<VIDEO_BIT_RATE>automatic</VIDEO_BIT_RATE>";
                    partyXml += "<AGC>true</AGC>";
                    if (partyObj.sDTMFSequence != null)
                    {
                        if (partyObj.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv7) //FB 1740
                        {
                            if (partyObj.sDTMFSequence.Trim() != "")
                            {
                                partyXml += "<USER_IDENTIFIER_STRING>" + partyObj.sDTMFSequence.Trim() + "</USER_IDENTIFIER_STRING>";
                            }
                            else
                            {
                                partyXml += "<USER_IDENTIFIER_STRING/>";
                            }
                            //partyXml += "<IDENTIFICATION_METHOD>called_phone_number</IDENTIFICATION_METHOD>";
                        }
                    }
                    else
                    {
                        partyXml += "<USER_IDENTIFIER_STRING/>";
                    }
                    partyXml += "</PARTY>";
                }
                else if (partyObj.etProtocol == NS_MESSENGER.Party.eProtocol.MPI)
                {
                    // MPI participant
                    partyXml = "<PARTY>";
                    partyXml += "<NAME>" + partyObj.sName + "</NAME>";
                    partyXml += "<ID>" + partyObj.iDbId.ToString() + "</ID>";

                    // participant's isdn address : to be added only in dial-out scenario
                    //if (partyObj.etConnType == NS_MESSENGER.Party.eConnectionType.DIAL_OUT)
                    //    partyXml += "<PHONE_LIST><PHONE1>" + partyObj.sAddress.Trim() + "</PHONE1></PHONE_LIST>";
                    //else
                    partyXml += "<PHONE_LIST></PHONE_LIST>";

                    // V35 
                    partyXml += "<INTERFACE>mpi</INTERFACE>";

                    // connection type
                    if (partyObj.etConnType == NS_MESSENGER.Party.eConnectionType.DIAL_IN)
                    {
                        partyXml += "<CONNECTION>dial_in</CONNECTION>";
                    }
                    else if (partyObj.etConnType == NS_MESSENGER.Party.eConnectionType.DIAL_OUT)
                    {
                        partyXml += "<CONNECTION>dial_out</CONNECTION>";
                    }
                    else if (partyObj.etConnType == NS_MESSENGER.Party.eConnectionType.DIRECT)
                    {
                        partyXml += "<CONNECTION>direct</CONNECTION>";

                    }
                    partyXml += "<MEET_ME_METHOD>party</MEET_ME_METHOD>";
                    logger.Trace("After meet me method...");
                    partyXml += "<NUM_TYPE>taken_from_service</NUM_TYPE>";
                    partyXml += "<BONDING>disabled</BONDING>";
                    partyXml += "<MULTI_RATE>enabled</MULTI_RATE>";

                    // V35 channels.
                    string polycomLineRate = "channel_6";
                    ret = false;
                    ret = EquateLineRate(partyObj.stLineRate.etLineRate, false, ref polycomLineRate);
                    if (!ret)
                    {
                        logger.Trace("Line rate conversion failed.");
                    }
                    partyXml += "<NET_CHANNEL_NUMBER>" + polycomLineRate + "</NET_CHANNEL_NUMBER>";


                    // Video Protocol
                    //if (partyObj.m_videoProtocol == NS_MESSENGER.Party.eVideoProtocol.AUTO)
                    partyXml += "<VIDEO_PROTOCOL>auto</VIDEO_PROTOCOL>";
                    //else
                    //{
                    //	if (partyObj.m_videoProtocol == NS_MESSENGER.Party.eVideoProtocol.H261)
                    //        partyXml += "<VIDEO_PROTOCOL></VIDEO_PROTOCOL>";
                    //	else 
                    //		partyXml += "<VIDEO_PROTOCOL>h263</VIDEO_PROTOCOL>"; 
                    //}

                    // Video conf = framed , Audio conf = voice
                    if (partyObj.etCallType == NS_MESSENGER.Party.eCallType.AUDIO)
                    {
                        partyXml += "<CALL_CONTENT>voice</CALL_CONTENT>";
                    }
                    else
                    {
                        partyXml += "<CALL_CONTENT>framed</CALL_CONTENT>";
                    }

                    partyXml += "<ALIAS><NAME /><ALIAS_TYPE>323_id</ALIAS_TYPE></ALIAS>";
                    partyXml += "<IP>255.255.255.255</IP>";
                    partyXml += "<SIGNALING_PORT>1720</SIGNALING_PORT>";
                    partyXml += "<VOLUME>5</VOLUME>";

                    // mcu's isdn address : to be added only in dial-in scenario
                    if (partyObj.etConnType == NS_MESSENGER.Party.eConnectionType.DIAL_IN)
                    {
                        partyXml += "<MCU_PHONE_LIST><PHONE1>" + partyObj.sMcuAddress.Trim() + "</PHONE1></MCU_PHONE_LIST>";
                    }
                    else
                    {
                        partyXml += "<MCU_PHONE_LIST><PHONE1></PHONE1></MCU_PHONE_LIST>";
                    }

                    if (partyObj.etConnType == NS_MESSENGER.Party.eConnectionType.DIAL_OUT)
                    {
                        partyXml += "<BONDING_PHONE>" + partyObj.sAddress.Trim() + "</BONDING_PHONE>";
                    }
                    else
                    {
                        partyXml += "<BONDING_PHONE></BONDING_PHONE>";
                    }

                    if (partyObj.etConnType == NS_MESSENGER.Party.eConnectionType.DIRECT)
                    {
                        partyXml += "<BONDING_PHONE></BONDING_PHONE>";
                    }
                    // mcu service name
                    partyXml += "<SERVICE_NAME>CODEC</SERVICE_NAME>";

                    partyXml += "<SUB_SERVICE_NAME>ZERO</SUB_SERVICE_NAME>";
                    partyXml += "<AUTO_DETECT>false</AUTO_DETECT>";
                    partyXml += "<RESTRICT>false</RESTRICT>";
                    partyXml += "<ENHANCED_VIDEO>false</ENHANCED_VIDEO>";
                    partyXml += "<VIDEO_BIT_RATE>automatic</VIDEO_BIT_RATE>";
                    partyXml += "<AGC>true</AGC>";
                    partyXml += "</PARTY>";
                }
                return true;
            }
            catch (Exception e)
            {
                // log the error and trash this participant info.
                string debugString = "Participant Not Added . Error = " + e.Message;
                debugString += ". PartyXML = " + partyXml;
                logger.Exception(100, debugString);
                return false;
            }
        }
        #endregion

        #region CreateXML_OngoingConf
        /// <summary>
        /// CreateXML_OngoingConf
        /// </summary>
        /// <param name="confXml"></param>
        /// <param name="conf"></param>
        /// <returns></returns>
        private bool CreateXML_OngoingConf(ref string confXml, NS_MESSENGER.Conference conf)
        {

            return true;
        }
        #endregion

        #region CreateXML_AddParty
        /// <summary>
        /// CreateXML_AddParty
        /// </summary>
        /// <param name="addPartyXml"></param>
        /// <param name="party"></param>
        /// <param name="confIdOnMcu"></param>
        /// <returns></returns>
        private bool CreateXML_AddParty(ref string addPartyXml, NS_MESSENGER.Party party, string confIdOnMcu)
        {
            try
            {

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        #endregion

        #region CreateXML_TerminateParty
        /// <summary>
        /// CreateXML_TerminateParty
        /// </summary>
        /// <param name="deletePartyXml"></param>
        /// <param name="mcu"></param>
        /// <param name="confIdOnMcu"></param>
        /// <param name="partyIdOnMcu"></param>
        /// <returns></returns>
        private bool CreateXML_TerminateParty(ref string deletePartyXml, NS_MESSENGER.MCU mcu, int confIdOnMcu, int partyIdOnMcu)
        {
            try
            {


                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        #endregion

        #region CreateXML_ChangeDisplayLayoutOfParty
        /// <summary>
        /// CreateXML_ChangeDisplayLayoutOfParty
        /// </summary>
        /// <param name="newLayoutPartyXml"></param>
        /// <param name="mcu"></param>
        /// <param name="confIdOnMcu"></param>
        /// <param name="partyIdOnMcu"></param>
        /// <param name="layout"></param>
        /// <returns></returns>
        private bool CreateXML_ChangeDisplayLayoutOfParty(ref string newLayoutPartyXml, NS_MESSENGER.MCU mcu, int confIdOnMcu, int partyIdOnMcu, int layout)
        {
            try
            {


                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        #endregion

        #region CreateXML_SetDisplayLayoutTypeToPersonal
        /// <summary>
        /// CreateXML_SetDisplayLayoutTypeToPersonal
        /// </summary>
        /// <param name="newLayoutTypeXml"></param>
        /// <param name="mcu"></param>
        /// <param name="confIdOnMcu"></param>
        /// <param name="partyIdOnMcu"></param>
        /// <returns></returns>
        private bool CreateXML_SetDisplayLayoutTypeToPersonal(ref string newLayoutTypeXml, NS_MESSENGER.MCU mcu, int confIdOnMcu, int partyIdOnMcu)
        {
            try
            {


                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        #endregion

        #region CreateXML_MuteAllExceptParticipant
        /// <summary>
        /// CreateXML_MuteAllExceptParticipant
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="partyList"></param>
        /// <param name="xWriter"></param>
        /// <returns></returns>
        private bool CreateXML_MuteAllExceptParticipant(ref NS_MESSENGER.Conference conf, List<NS_MESSENGER.Party> partyList, ref XmlWriter xWriter)
        {
            try
            {
                xWriter.WriteStartElement("plcm-conference-mute-all-except-request");
                xWriter.WriteAttributeString("xmlns", "n1", null, "urn:com:polycom:api:rest:plcm-conference-mute-all-except-request");

                for (int i = 0; i < partyList.Count; i++)
                {
                    xWriter.WriteStartElement("excluded-participant-identifier");
                    xWriter.WriteElementString("excluded-participant-identifier", partyList[i].sGUID);
                    xWriter.WriteEndElement();
                }
                xWriter.WriteEndElement();
                xWriter.Flush();

                return true;
            }
            catch (Exception e)
            {

                logger.Exception(100, e.StackTrace);
                return false;
            }
        }
        #endregion

        #region CreateXML_SetLectureMode
        /// <summary>
        /// CreateXML_SetLectureMode
        /// </summary>
        /// <param name="lectureModeXml"></param>
        /// <param name="mcu"></param>
        /// <param name="confIdOnMcu"></param>
        /// <param name="lecturer"></param>
        /// <returns></returns>
        private bool CreateXML_SetLectureMode(ref string lectureModeXml, NS_MESSENGER.MCU mcu, int confIdOnMcu, string lecturer)
        {
            try
            {

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        #endregion

        #region CreateXML_MuteParty
        /// <summary>
        /// CreateXML_MuteParty
        /// </summary>
        /// <param name="mutePartyXml"></param>
        /// <param name="mcu"></param>
        /// <param name="confIdOnMcu"></param>
        /// <param name="partyIdOnMcu"></param>
        /// <param name="audioMute"></param>
        /// <returns></returns>
        private bool CreateXML_MuteParty(ref string mutePartyXml, NS_MESSENGER.MCU mcu, int confIdOnMcu, int partyIdOnMcu, bool audioMute)
        {
            try
            {


                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        #endregion

        #region CreateXML_TerminateConference
        /// <summary>
        /// CreateXML_TerminateConference
        /// </summary>
        /// <param name="terminateXml"></param>
        /// <param name="mcu"></param>
        /// <param name="confIdOnMcu"></param>
        /// <returns></returns>
        private bool CreateXML_TerminateConference(NS_MESSENGER.Conference conf)
        {
            try
            {
                DMAorRPRM = 0;
                str_build = new StringBuilder();
                ContentType = "*/*";
                RequestMethod = "DELETE";

                RestURI = "/api/rest/reservation/" + conf.sGUID;

                ret = false; responseXml = "";
                ret = SendTransaction_overXMLHTTP(conf, str_build.ToString(), ContentType, RestURI, RequestMethod, DMAorRPRM, ref responseXml);
                if (!ret)
                {
                    logger.Exception(100, "TerminateConference SendTransaction failed");
                    return false;
                }

                return true;

            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        #endregion

        #region CreateXML_GetMcuTime
        /// <summary>
        /// CreateXML_GetMcuTime
        /// </summary>
        /// <param name="mcu"></param>
        /// <param name="getTimeXml"></param>
        /// <returns></returns>
        internal bool CreateXML_GetMcuTime(NS_MESSENGER.MCU mcu, ref string getTimeXml)
        {
            try
            {

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        #endregion

        #region CreateXML_ExtendEndTime
        /// <summary>
        /// CreateXML_ExtendEndTime
        /// </summary>
        /// <param name="extendXml"></param>
        /// <param name="mcu"></param>
        /// <param name="iConfMcuID"></param>
        /// <param name="newConfEndTime"></param>
        /// <returns></returns>
        private bool CreateXML_ExtendEndTime(ref string extendXml, NS_MESSENGER.MCU mcu, int iConfMcuID, DateTime newConfEndTime)
        {
            try
            {


                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        #endregion

        #region CreateXML_OngoingConfList
        /// <summary>
        /// CreateXML_OngoingConfList
        /// </summary>
        /// <param name="mcu"></param>
        /// <param name="ongoingXml"></param>
        /// <returns></returns>
        private bool CreateXML_OngoingConfList(NS_MESSENGER.MCU mcu, ref string ongoingXml)
        {
            try
            {

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        #endregion

        #region CreateXML_ReservationConfList
        /// <summary>
        /// CreateXML_ReservationConfList
        /// </summary>
        /// <param name="mcu"></param>
        /// <param name="resXml"></param>
        /// <returns></returns>
        private bool CreateXML_ReservationConfList(NS_MESSENGER.MCU mcu, ref string resXml)
        {
            try
            {

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        #endregion

        #region CreateXML_OngoingConf
        /// <summary>
        /// CreateXML_OngoingConf
        /// </summary>
        /// <param name="confXml"></param>
        /// <param name="mcu"></param>
        /// <param name="conf"></param>
        /// <returns></returns>
        private bool CreateXML_OngoingConf(ref string confXml, NS_MESSENGER.MCU mcu, NS_MESSENGER.Conference conf)
        {

            try
            {
                // xml for fetching individual conf details. 



                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        #endregion

        #region CreateXML_ConnectDisconnectParty
        /// <summary>
        /// CreateXML_ConnectDisconnectParty
        /// </summary>
        /// <param name="connectionXML"></param>
        /// <param name="mcu"></param>
        /// <param name="confIdOnMcu"></param>
        /// <param name="partyIdOnMcu"></param>
        /// <param name="isConnect"></param>
        /// <returns></returns>
        private bool CreateXML_ConnectDisconnectParty(ref string connectionXML, NS_MESSENGER.MCU mcu, int confIdOnMcu, int partyIdOnMcu, bool isConnect)
        {
            try
            {

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        #endregion

        #region CreateXML_TerminateConf
        /// <summary>
        /// CreateXML_TerminateConf
        /// </summary>
        /// <param name="terminateXml"></param>
        /// <param name="conf"></param>
        /// <returns></returns>
        private bool CreateXML_TerminateConf(ref string terminateXml, NS_MESSENGER.Conference conf)
        {
            try
            {


                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        #endregion

        #region CreateXML_SetMcuTime
        /// <summary>
        /// CreateXML_SetMcuTime
        /// </summary>
        /// <param name="mcu"></param>
        /// <param name="setTimeXml"></param>
        /// <returns></returns>
        internal bool CreateXML_SetMcuTime(NS_MESSENGER.MCU mcu, ref string setTimeXml)
        {
            try
            {

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        #endregion




        #endregion

        #region ResponseXML


        #region Process_ReservationDialString
        /// <summary>
        /// Process_ReservationDialString
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="?"></param>
        /// <returns></returns>
        private bool Process_ReservationDialString(NS_MESSENGER.Conference conf, string responseXml)
        {
            try
            {
                xdoc = XDocument.Parse(responseXml);
                XNamespace ns4 = "urn:com:polycom:api:rest:plcm-reservation";
                if (xdoc.Element(ns4 + "plcm-reservation").Element(ns4 + "dial-in-number") != null)
                    conf.sDialString = xdoc.Element(ns4 + "plcm-reservation").Element(ns4 + "dial-in-number").Value;
                if (xdoc.Element(ns4 + "plcm-reservation").Element(ns4 + "entity-tag") != null)
                    conf.sEtag = xdoc.Element(ns4 + "plcm-reservation").Element(ns4 + "entity-tag").Value;

                if (conf.sDialString.Length > 0)
                {
                    ret = false;
                    ret = db.UpdateConfDailString(conf);
                    if (!ret)
                    {
                        logger.Exception(100, "UpdateConfDailString failed");
                        return false;
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }

        }
        #endregion





        #region ProcessXML_OngoingConf
        /// <summary>
        /// ProcessXML_OngoingConf
        /// </summary>
        /// <param name="responseXml"></param>
        /// <param name="conf"></param>
        /// <returns></returns>
        private bool ProcessXML_OngoingConf(string responseXml, ref NS_MESSENGER.Conference conf)
        {
            // process each conf to get the latest status for each participant
            try
            {
                //Load the response xml recd from the bridge and parse it
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(responseXml);

                XmlNodeList nodelist;
                XmlNode node;

                // if description = OK , then continue processing
                node = xd.SelectSingleNode("//RESPONSE_TRANS_CONF/RETURN_STATUS/DESCRIPTION");
                string description = node.InnerXml;
                description = description.Trim();
                if (description.Length < 0 || description.Contains("OK") != true)
                {
                    this.errMsg = description;
                    return false; //error
                }

                // Retreive all conferences	
                nodelist = xd.SelectNodes("//RESPONSE_TRANS_CONF/GET/CONFERENCE/ONGOING_PARTY_LIST/ONGOING_PARTY");

                //cycle through each party one-by-one
                foreach (XmlNode node1 in nodelist)
                {
                    // party object 
                    NS_MESSENGER.Party party = new NS_MESSENGER.Party();
                    XmlNode partyNode = node1.SelectSingleNode("PARTY");

                    // name , partyID on mcu
                    party.sMcuName = partyNode.ChildNodes.Item(0).InnerText; //NAME
                    party.iMcuId = int.Parse(partyNode.ChildNodes.Item(1).InnerText); //ID

                    //interface type  - ip or isdn
                    //XmlNode interfaceType = node1.SelectSingleNode("INTERFACE");
                    //string interfaceTypeVal = interfaceType.InnerText ; 					
                    string interfaceTypeVal = partyNode.ChildNodes.Item(3).InnerText;
                    interfaceTypeVal = interfaceTypeVal.Trim();
                    if (interfaceTypeVal == "h323")
                        party.etProtocol = NS_MESSENGER.Party.eProtocol.IP;
                    else
                        party.etProtocol = NS_MESSENGER.Party.eProtocol.ISDN;

                    // connection type - dial-in or dial-out 
                    //XmlNode connectionType = node1.SelectSingleNode("CONNECTION");
                    //string connectionTypeVal = connectionType.InnerText;
                    string connectionTypeVal = partyNode.ChildNodes.Item(4).InnerText;
                    connectionTypeVal = connectionTypeVal.Trim();
                    if (connectionTypeVal == "dial_in")
                        party.etConnType = NS_MESSENGER.Party.eConnectionType.DIAL_IN;
                    else
                        party.etConnType = NS_MESSENGER.Party.eConnectionType.DIAL_OUT;

                    // ip or isdn address 
                    if (party.etProtocol == NS_MESSENGER.Party.eProtocol.IP)
                    {
                        //XmlNode ipaddress = node1.SelectSingleNode("IP");						
                        //party.m_mcuAddress =ipaddress.InnerText ; 
                        party.sMcuAddress = partyNode.SelectSingleNode("IP").InnerText;
                    }
                    else
                    {
                        // party is connected on ISDN
                        party.etProtocol = NS_MESSENGER.Party.eProtocol.ISDN;
                        if (party.etConnType == NS_MESSENGER.Party.eConnectionType.DIAL_IN)
                        {
                            // Dial-In : Fetch the address from mcu phone list 
                            //XmlNode isdnaddress = node1.SelectSingleNode("MCU_PHONE_LIST");						
                            party.sMcuAddress = partyNode.SelectSingleNode("MCU_PHONE_LIST").SelectSingleNode("PHONE1").InnerText;
                        }
                        else
                        {
                            // Dial-Out : Fetch the address from the participant phone list
                            //XmlNode isdnaddress = node1.SelectSingleNode("");						
                            party.sMcuAddress = partyNode.SelectSingleNode("BONDING_PHONE").InnerText;
                        }
                    }

                    // party ongoing status
                    XmlNode partyStatus = node1.SelectSingleNode("ONGOING_PARTY_STATUS");
                    string ongoingPartyStatus = partyStatus.ChildNodes.Item(1).InnerText; //description
                    ongoingPartyStatus = ongoingPartyStatus.Trim();
                    if (ongoingPartyStatus.CompareTo("connected") == 0)
                    {
                        party.etStatus = NS_MESSENGER.Party.eOngoingStatus.FULL_CONNECT;
                    }
                    else
                    {
                        if (ongoingPartyStatus.CompareTo("connecting") == 0)
                        {
                            party.etStatus = NS_MESSENGER.Party.eOngoingStatus.PARTIAL_CONNECT;
                        }
                        else
                        {
                            party.etStatus = NS_MESSENGER.Party.eOngoingStatus.DIS_CONNECT;
                        }
                    }
                    logger.Trace("Ongoing party status : " + party.etStatus.ToString());

                    //Audio Mute status 
                    XmlNode partyAudioMute = node1.SelectSingleNode("AUDIO_MUTE");
                    string audioMute = partyAudioMute.InnerXml.Trim();
                    if (audioMute == "true")
                    {
                        party.bMute = true;
                    }
                    else
                    {
                        party.bMute = false;
                    }

                    // individual party details added to the resp conf
                    conf.qParties.Enqueue(party);
                }
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
            return true;
        }
        #endregion


        #region ProcessXML_TerminateConf
        /// <summary>
        /// ProcessXML_TerminateConf
        /// </summary>
        /// <param name="responseXml"></param>
        /// <returns></returns>
        private bool ProcessXML_TerminateConf(string responseXml)
        {
            try
            {
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
            }

            // either correct confirmation not recd or conf doesnot exist on the bridge anymore.		
            return false;
        }
        #endregion





        #endregion


    }
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;

namespace NS_CallDetailRecords
{
    class CallDetailRecords
    {
        private NS_LOGGER.Log logger;
        private NS_MESSENGER.ConfigParams configParams;
        private NS_DATABASE.Database db;
        internal String errMsg = "";
        Dictionary<String, NS_MESSENGER.Conference> listedConference = null;

        internal CallDetailRecords(NS_MESSENGER.ConfigParams config)
        {
            configParams = new NS_MESSENGER.ConfigParams();
            configParams = config;
            logger = new NS_LOGGER.Log(configParams);
            db = new NS_DATABASE.Database(configParams);
        }

        public class CDR
        {
            public string conferenceGUID { get; set; }
            public string participantGUID { get; set; }
            public string eventType { get; set; }
            public string confName { get; set; }
            public string numericId { get; set; }
            public string uri { get; set; }

        }

        public bool CDRforCisco8710(NS_MESSENGER.MCU MCU)
        {
            XmlDocument xd = new XmlDocument();
            NS_MESSENGER.CDREvent cdrEvent = null;
            List<NS_MESSENGER.CDREvent> cdrEventLists = null;
            XmlNode oEventsNode = null;
            XmlNodeList oNodeList = null;
            int ndeCnt = 0; int eventsRemainin = 0;
            XmlNodeList oInnerNodeList = null;
            XmlNode oNode = null; ;
            XmlNode oInnerNode = null; ;
            NS_CiscoTPServer.CiscoTPServer tpServerCDR = null;
            String strResponse = "";
            KeyValuePair<String, NS_MESSENGER.Conference> foundConference = new KeyValuePair<string, NS_MESSENGER.Conference>();
            String dtPattern = "yyyyMMddTHH:mm:ss";
            DateTime dtCdrTime = DateTime.Now;
            try
            {
                logger.Trace("Entering inot CDRforCisco8710 Method");
                int index = 0, CDRIndex = 0;
                String strCurrentTime = "";
                String year, month, date, hour, min, sec;

            GetCDR: tpServerCDR = new NS_CiscoTPServer.CiscoTPServer(configParams);

                if (index == 0)
                {
                    CDRIndex = db.FetchMaxCDRIndex(CDRIndex);
                    index = CDRIndex + 1;
                }
                logger.Trace("CDR Index: " + index);

                if (!tpServerCDR.CallDetailRecords(MCU, index, ref strResponse))
                    return false;

                xd.LoadXml(strResponse);
                oEventsNode = xd.SelectSingleNode("//member");

                if (oEventsNode != null)
                {
                    oNodeList = oEventsNode.SelectNodes("value/array/data/value");
                    for (ndeCnt = 0; ndeCnt < oNodeList.Count; ndeCnt++)
                    {
                        cdrEvent = new NS_MESSENGER.CDREvent();
                        strCurrentTime = "";
                        year = ""; month = ""; date = ""; hour = ""; min = ""; sec = "";

                        oNode = oNodeList[ndeCnt];
                        if (oNode.SelectSingleNode("struct/member[name = \"index\"]/value/int") != null)
                            Int32.TryParse(oNode.SelectSingleNode("struct/member[name = \"index\"]/value/int").InnerText.ToString().Trim(), out cdrEvent.iIndex);

                        if (oNode.SelectSingleNode("struct/member[name = \"time\"]/value/dateTime.iso8601") != null)
                            strCurrentTime = oNode.SelectSingleNode("struct/member[name = \"time\"]/value/dateTime.iso8601").InnerText.ToString().Trim();

                        DateTime.TryParseExact(strCurrentTime, dtPattern, null, DateTimeStyles.None,out dtCdrTime);

                        if (dtCdrTime <= DateTime.MinValue)
                            dtCdrTime = DateTime.Now;

                        cdrEvent.dTime = dtCdrTime.ToUniversalTime();

                        logger.Trace("strCurrentTime:" + strCurrentTime);
                        logger.Trace("cdrEvent.dTime :" + cdrEvent.dTime);

                        if (oNode.SelectSingleNode("struct/member[name = \"type\"]/value/string") != null)
                            cdrEvent.sEventType = oNode.SelectSingleNode("struct/member[name = \"type\"]/value/string").InnerText.ToString().Trim();

                        if (oNode.SelectSingleNode("struct/member[name = \"" + cdrEvent.sEventType.Trim() + "\"]") != null)
                        {
                            oInnerNodeList = oNode.SelectNodes("struct/member/value/struct");
                            for (int innerNdeCnt = 0; innerNdeCnt < oInnerNodeList.Count; innerNdeCnt++)
                            {
                                oInnerNode = oInnerNodeList[innerNdeCnt];

                                if (oInnerNode.SelectSingleNode("member[name = \"conferenceGUID\"]/value/string") != null)
                                {
                                    cdrEvent.sConferenceGUID = oInnerNode.SelectSingleNode("member[name = \"conferenceGUID\"]/value/string").InnerText.ToString().Trim();
                                    cdrEvent.sMessage += " ConferenceGUID: " + cdrEvent.sConferenceGUID;
                                }
                                if (oInnerNode.SelectSingleNode("member[name = \"participantGUID\"]/value/string") != null)
                                {
                                    cdrEvent.sParticipantGUID = oInnerNode.SelectSingleNode("member[name = \"participantGUID\"]/value/string").InnerText.ToString().Trim();
                                    cdrEvent.sMessage += " ParticipantGUID: " + cdrEvent.sParticipantGUID;
                                }
                                if (oInnerNode.SelectSingleNode("member[name = \"name\"]/value/string") != null)
                                {
                                    cdrEvent.sName = oInnerNode.SelectSingleNode("member[name = \"name\"]/value/string").InnerText.ToString().Trim();
                                    cdrEvent.sMessage += " Name: " + cdrEvent.sName;
                                }
                                if (oInnerNode.SelectSingleNode("member[name = \"numericId\"]/value/string") != null)
                                {
                                    Int32.TryParse(oInnerNode.SelectSingleNode("member[name = \"numericId\"]/value/string").InnerText.ToString().Trim(), out cdrEvent.iNumericId);
                                    cdrEvent.sMessage += " NumericId: " + cdrEvent.iNumericId;
                                }
                                if (oInnerNode.SelectSingleNode("member[name = \"uris\"]/value/array/data/value/struct/member[name = \"uri\"]/value/string") != null)
                                {
                                    cdrEvent.sUri = oInnerNode.SelectSingleNode("member[name = \"uris\"]/value/array/data/value/struct/member[name = \"uri\"]/value/string").InnerText.ToString().Trim();
                                    cdrEvent.sMessage += " URI: " + cdrEvent.sName;
                                }
                                if (oInnerNode.SelectSingleNode("member[name = \"uris\"]/value/array/data/value/struct/member[name = \"pinProtected\"]/value/string") != null)
                                {
                                    cdrEvent.sPinProtected = oInnerNode.SelectSingleNode("member[name = \"uris\"]/value/array/data/value/struct/member[name = \"pinProtected\"]/value/string").InnerText.ToString().Trim();
                                    cdrEvent.sMessage += " PinProtected: " + cdrEvent.sPinProtected;
                                }
                                if (oInnerNode.SelectSingleNode("member[name = \"maxSimultaneousAudioOnlyParticipants\"]/value/int") != null)
                                {
                                    Int32.TryParse(oInnerNode.SelectSingleNode("member[name = \"maxSimultaneousAudioOnlyParticipants\"]/value/int").InnerText.ToString().Trim(), out cdrEvent.iMaxSimultaneousAudioOnlyParticipants);
                                    cdrEvent.sMessage += " MaxSimultaneousAudioOnlyParticipants: " + cdrEvent.iMaxSimultaneousAudioOnlyParticipants;
                                }
                                if (oInnerNode.SelectSingleNode("member[name = \"maxSimultaneousAudioVideoParticipants\"]/value/int") != null)
                                {
                                    Int32.TryParse(oInnerNode.SelectSingleNode("member[name = \"maxSimultaneousAudioVideoParticipants\"]/value/int").InnerText.ToString().Trim(), out cdrEvent.iMaxSimultaneousAudioVideoParticipants);
                                    cdrEvent.sMessage += " MaxSimultaneousAudioOnlyParticipants: " + cdrEvent.iMaxSimultaneousAudioOnlyParticipants;
                                }
                                if (oInnerNode.SelectSingleNode("member[name = \"totalAudioVideoParticipants\"]/value/int") != null)
                                {
                                    Int32.TryParse(oInnerNode.SelectSingleNode("member[name = \"totalAudioVideoParticipants\"]/value/int").InnerText.ToString().Trim(), out cdrEvent.iTotalAudioVideoParticipants);
                                    cdrEvent.sMessage += " TotalAudioVideoParticipants: " + cdrEvent.iTotalAudioVideoParticipants;
                                }
                                if (oInnerNode.SelectSingleNode("member[name = \"totalAudioOnlyParticipants\"]/value/int") != null)
                                {
                                    Int32.TryParse(oInnerNode.SelectSingleNode("member[name = \"totalAudioOnlyParticipants\"]/value/int").InnerText.ToString().Trim(), out cdrEvent.iTotalAudioOnlyParticipants);
                                    cdrEvent.sMessage += " TotalAudioOnlyParticipants: " + cdrEvent.iTotalAudioOnlyParticipants;
                                }
                                if (oInnerNode.SelectSingleNode("member[name = \"sessionDuration\"]/value/int") != null)
                                {
                                    Int32.TryParse(oInnerNode.SelectSingleNode("member[name = \"sessionDuration\"]/value/int").InnerText.ToString().Trim(), out cdrEvent.iSessionDuration);
                                    cdrEvent.sMessage += " Session Duration: " + cdrEvent.iSessionDuration;
                                }
                                if (oInnerNode.SelectSingleNode("member[name = \"callDirection\"]/value/string") != null)
                                {
                                    cdrEvent.sCallDirection = oInnerNode.SelectSingleNode("member[name = \"callDirection\"]/value/string").InnerText.ToString().Trim();
                                    cdrEvent.sMessage += " CallDirection: " + cdrEvent.sCallDirection;
                                }
                                if (oInnerNode.SelectSingleNode("member[name = \"callProtocol\"]/value/string") != null)
                                {
                                    cdrEvent.sCallProtocol = oInnerNode.SelectSingleNode("member[name = \"callProtocol\"]/value/string").InnerText.ToString().Trim();
                                    cdrEvent.sMessage += " CallProtocol: " + cdrEvent.sCallProtocol;
                                }
                                if (oInnerNode.SelectSingleNode("member[name = \"endpointIpAddress\"]/value/string") != null)
                                {
                                    cdrEvent.sEndpointIPAddress = oInnerNode.SelectSingleNode("member[name = \"endpointIpAddress\"]/value/string").InnerText.ToString().Trim();
                                    cdrEvent.sMessage += " EndpointIPAddress: " + cdrEvent.sEndpointIPAddress;
                                }
                                if (oInnerNode.SelectSingleNode("member[name = \"endpointDisplayName\"]/value/string") != null)
                                {
                                    cdrEvent.sEndpointDisplayName = oInnerNode.SelectSingleNode("member[name = \"endpointDisplayName\"]/value/string").InnerText.ToString().Trim();
                                    cdrEvent.sMessage += " Endpoint Display Name: " + cdrEvent.sEndpointDisplayName;
                                }
                                if (oInnerNode.SelectSingleNode("member[name = \"endpointUri\"]/value/string") != null)
                                {
                                    cdrEvent.sEndpointURI = oInnerNode.SelectSingleNode("member[name = \"endpointUri\"]/value/string").InnerText.ToString().Trim();
                                    cdrEvent.sMessage += " EndpointURI: " + cdrEvent.sEndpointURI;
                                }
                                if (oInnerNode.SelectSingleNode("member[name = \"endpointConfiguredName\"]/value/string") != null)
                                {
                                    cdrEvent.sEndpointConfiguredName = oInnerNode.SelectSingleNode("member[name = \"endpointConfiguredName\"]/value/string").InnerText.ToString().Trim();
                                    cdrEvent.sMessage += " Endpoint Configured Name: " + cdrEvent.sEndpointConfiguredName;
                                }
                                if (oInnerNode.SelectSingleNode("member[name = \"timeInConference\"]/value/int") != null)
                                {
                                    cdrEvent.sTimeInConference = oInnerNode.SelectSingleNode("member[name = \"timeInConference\"]/value/int").InnerText.ToString().Trim();
                                    cdrEvent.sMessage += " time In Conference: " + cdrEvent.sTimeInConference;
                                }

                                if (oInnerNode.SelectSingleNode("member[name = \"width\"]/value/int") != null)
                                {
                                    Int32.TryParse(oInnerNode.SelectSingleNode("member[name = \"width\"]/value/int").InnerText.ToString().Trim(), out cdrEvent.iWidth);
                                    cdrEvent.sMessage += " Width: " + cdrEvent.iWidth;
                                }
                                if (oInnerNode.SelectSingleNode("member[name = \"height\"]/value/int") != null)
                                {
                                    Int32.TryParse(oInnerNode.SelectSingleNode("member[name = \"height\"]/value/int").InnerText.ToString().Trim(), out cdrEvent.iHeight);
                                    cdrEvent.sMessage += " Height" + cdrEvent.iHeight;
                                }
                                if (oInnerNode.SelectSingleNode("member[name = \"maxBandwidth\"]/value/int") != null)
                                {
                                    Int32.TryParse(oInnerNode.SelectSingleNode("member[name = \"maxBandwidth\"]/value/int").InnerText.ToString().Trim(), out cdrEvent.iMaxBandwidth);
                                    cdrEvent.sMessage += " MaxBandwidth: " + cdrEvent.iMaxBandwidth;
                                }
                                if (oInnerNode.SelectSingleNode("member[name = \"bandwidth\"]/value/int") != null)
                                {
                                    Int32.TryParse(oInnerNode.SelectSingleNode("member[name = \"bandwidth\"]/value/int").InnerText.ToString().Trim(), out cdrEvent.iBandwidth);
                                    cdrEvent.sMessage += " Bandwidth: " + cdrEvent.iBandwidth;
                                }
                                if (oInnerNode.SelectSingleNode("member[name = \"packetsReceived\"]/value/int") != null)
                                {
                                    Int32.TryParse(oInnerNode.SelectSingleNode("member[name = \"maxBandwidth\"]/value/int").InnerText.ToString().Trim(), out cdrEvent.iPacketsReceived);
                                    cdrEvent.sMessage += " PacketsReceived: " + cdrEvent.iPacketsReceived;
                                }
                                if (oInnerNode.SelectSingleNode("member[name = \"packetsLost\"]/value/int") != null)
                                {
                                    Int32.TryParse(oInnerNode.SelectSingleNode("member[name = \"packetsLost\"]/value/int").InnerText.ToString().Trim(), out cdrEvent.iPacketsLost);
                                    cdrEvent.sMessage += " PacketsLost: " + cdrEvent.iPacketsLost;
                                }

                            }
                            if (listedConference == null)
                                listedConference = new Dictionary<string, NS_MESSENGER.Conference>();

                            foundConference = listedConference.Where(l => l.Key == cdrEvent.sConferenceGUID.Trim() || l.Key == cdrEvent.sParticipantGUID.Trim()).FirstOrDefault();
                            if (foundConference.Value != null)
                                cdrEvent.confDetails = foundConference.Value;
                            else
                            {
                                cdrEvent.confDetails = new NS_MESSENGER.Conference();
                                cdrEvent.confDetails.sGUID = cdrEvent.sConferenceGUID.Trim();
                                db.FetchConfforCallDetailRecord(ref cdrEvent.confDetails, cdrEvent.sParticipantGUID.Trim());
                                listedConference.Add(cdrEvent.sParticipantGUID.Trim() == "" ? cdrEvent.sConferenceGUID : cdrEvent.sParticipantGUID.Trim(), cdrEvent.confDetails);
                            }

                            if (cdrEventLists == null)
                                cdrEventLists = new List<NS_MESSENGER.CDREvent>();

                            cdrEventLists.Add(cdrEvent);
                        }
                    }
                }

                oNodeList = xd.SelectNodes("//param/value/struct/member");
                eventsRemainin = 0;
                for (ndeCnt = 0; ndeCnt < oNodeList.Count; ndeCnt++)
                {
                    oNode = oNodeList[ndeCnt];

                    if (oNode.SelectSingleNode("//member[name = \"nextIndex\"]/value/int") != null)
                        Int32.TryParse(oNode.SelectSingleNode("//member[name = \"nextIndex\"]/value/int").InnerText, out index);
                    logger.Trace("nextIndex: " + index);

                    if (oNode.SelectSingleNode("//member[name = \"eventsRemaining\"]/value/boolean") != null)
                        Int32.TryParse(oNode.SelectSingleNode("//member[name = \"eventsRemaining\"]/value/boolean").InnerText, out eventsRemainin);
                }

                if (eventsRemainin > 0)
                    goto GetCDR;

                if (cdrEventLists != null)
                {
                    if (!ProcessandInsertCDRforCisco8710(ref cdrEventLists, ref MCU))
                    {
                        logger.Exception(100, "Insert into CDR table Process is failed.");
                        return false;
                    }
                }
                else
                {
                    logger.Trace("No Conference to add in CDR Tables");
                }
            }
            catch (Exception ex)
            {
                errMsg = ex.Message;
                return false;
            }
            return true;
        }

        public bool ProcessandInsertCDRforCisco8710(ref List<NS_MESSENGER.CDREvent> cdrEventLists, ref NS_MESSENGER.MCU evntMCU)
        {
            bool ret = false;
            try
            {
                ret = db.InsertCDRinMyvrm(ref cdrEventLists, ref evntMCU);
                if (!ret)
                {
                    logger.Exception(100, "InsertCDRinMyvrm Method is failed.");
                    return false;
                }

                ret = db.InsertCDRinCisco8710(ref cdrEventLists, ref evntMCU);
                if (!ret)
                {
                    logger.Exception(100, "InsertCDRinCisco8710 Method is failed.");
                    return false;
                }
            }
            catch (Exception ex)
            {
                errMsg = ex.Message;
                return false;
            }
            return true;
        }
    }
}

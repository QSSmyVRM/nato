/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Usr_Roles_D ADD
	createType int NULL
GO
DECLARE @v sql_variant 
SET @v = N'1 - system, 2 - custom'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Usr_Roles_D', N'COLUMN', N'createType'
GO
ALTER TABLE dbo.Usr_Roles_D ADD CONSTRAINT
	DF_Usr_Roles_D_createType DEFAULT 2 FOR createType
GO
COMMIT

Update usr_roles_d set createType = 1 where roleName IN ('General User', 'Organization Administrator', 'Site Administrator', 'Catering Administrator', 'Inventory Administrator', 'Housekeeping Administrator', 'Express Profile', 'Express Profile Manage', 'Express Profile Advanced')

Update usr_roles_d set createType = 2 where  (createType <> 1 or createType is null)

Update usr_roles_d set locked = 1 where createType = 1

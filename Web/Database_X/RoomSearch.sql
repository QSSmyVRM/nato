/*
   Tuesday, December 22, 20098:27:57 AM
   User: 
   Server: YAMUNA\SQL2005
   Database: orgdbfull
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Usr_Templates_D
	DROP CONSTRAINT DF_Usr_Templates_D_ExpirationDate
GO
CREATE TABLE dbo.Tmp_Usr_Templates_D
	(
	id int NOT NULL IDENTITY (1, 1),
	name nvarchar(256) NULL,
	connectiontype int NULL,
	ipisdnaddress nvarchar(256) NULL,
	initialtime int NULL,
	timezone int NULL,
	videoProtocol int NULL,
	location nvarchar(150) NULL,
	languageId int NULL,
	lineRateId int NULL,
	bridgeId int NULL,
	deptId int NULL,
	groupId int NULL,
	ExpirationDate datetime NULL,
	emailNotification int NULL,
	outsideNetwork int NULL,
	role int NULL,
	addressBook int NULL,
	equipmentId int NULL,
	deleted int NULL,
	companyId int NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_Usr_Templates_D ADD CONSTRAINT
	DF_Usr_Templates_D_ExpirationDate DEFAULT (((1)/(1))/(1980)) FOR ExpirationDate
GO
SET IDENTITY_INSERT dbo.Tmp_Usr_Templates_D ON
GO
IF EXISTS(SELECT * FROM dbo.Usr_Templates_D)
	 EXEC('INSERT INTO dbo.Tmp_Usr_Templates_D (id, name, connectiontype, ipisdnaddress, initialtime, timezone, videoProtocol, location, languageId, lineRateId, bridgeId, deptId, groupId, ExpirationDate, emailNotification, outsideNetwork, role, addressBook, equipmentId, deleted, companyId)
		SELECT id, name, connectiontype, ipisdnaddress, initialtime, timezone, videoProtocol, location, languageId, lineRateId, bridgeId, deptId, groupId, ExpirationDate, emailNotification, outsideNetwork, role, addressBook, equipmentId, deleted, companyId FROM dbo.Usr_Templates_D WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_Usr_Templates_D OFF
GO
DROP TABLE dbo.Usr_Templates_D
GO
EXECUTE sp_rename N'dbo.Tmp_Usr_Templates_D', N'Usr_Templates_D', 'OBJECT' 
GO
ALTER TABLE dbo.Usr_Templates_D ADD CONSTRAINT
	PK_Usr_Templates_D PRIMARY KEY CLUSTERED 
	(
	id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT

/*
   Tuesday, December 22, 20098:28:45 AM
   User: 
   Server: YAMUNA\SQL2005
   Database: orgdbfull
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Usr_List_D
	DROP CONSTRAINT DF_Usr_List_D_Financial
GO
ALTER TABLE dbo.Usr_List_D
	DROP CONSTRAINT DF_Usr_List_D_Admin
GO
ALTER TABLE dbo.Usr_List_D
	DROP CONSTRAINT DF_Usr_List_D_TimeZone
GO
ALTER TABLE dbo.Usr_List_D
	DROP CONSTRAINT DF_Usr_List_D_Language
GO
ALTER TABLE dbo.Usr_List_D
	DROP CONSTRAINT DF_Usr_List_D_PreferedRoom
GO
ALTER TABLE dbo.Usr_List_D
	DROP CONSTRAINT DF_Usr_List_D_DoubleEmail
GO
ALTER TABLE dbo.Usr_List_D
	DROP CONSTRAINT DF_Usr_List_D_PreferedGroup
GO
ALTER TABLE dbo.Usr_List_D
	DROP CONSTRAINT DF_Usr_List_D_CCGroup
GO
ALTER TABLE dbo.Usr_List_D
	DROP CONSTRAINT DF_Usr_List_D_RoomID
GO
ALTER TABLE dbo.Usr_List_D
	DROP CONSTRAINT DF_Usr_List_D_PreferedOperator
GO
ALTER TABLE dbo.Usr_List_D
	DROP CONSTRAINT DF_Usr_List_D_lockCntTrns
GO
ALTER TABLE dbo.Usr_List_D
	DROP CONSTRAINT DF_Usr_List_D_DefLineRate
GO
ALTER TABLE dbo.Usr_List_D
	DROP CONSTRAINT DF_Usr_List_D_DefVideoProtocol
GO
ALTER TABLE dbo.Usr_List_D
	DROP CONSTRAINT DF_Usr_List_D_Deleted
GO
ALTER TABLE dbo.Usr_List_D
	DROP CONSTRAINT DF_Usr_List_D_EmailClient
GO
ALTER TABLE dbo.Usr_List_D
	DROP CONSTRAINT DF_Usr_List_D_newUser
GO
ALTER TABLE dbo.Usr_List_D
	DROP CONSTRAINT DF_Usr_List_D_PrefTZSID
GO
ALTER TABLE dbo.Usr_List_D
	DROP CONSTRAINT DF_Usr_List_D_DefaultEquipmentId
GO
ALTER TABLE dbo.Usr_List_D
	DROP CONSTRAINT DF_Usr_List_D_LockStatus
GO
ALTER TABLE dbo.Usr_List_D
	DROP CONSTRAINT DF_Usr_List_D_LastLogin
GO
ALTER TABLE dbo.Usr_List_D
	DROP CONSTRAINT DF_Usr_List_D_accountexpiry
GO
ALTER TABLE dbo.Usr_List_D
	DROP CONSTRAINT DF_Usr_List_D_approverCount
GO
ALTER TABLE dbo.Usr_List_D
	DROP CONSTRAINT DF_Usr_List_D_BridgeID
GO
ALTER TABLE dbo.Usr_List_D
	DROP CONSTRAINT DF_Usr_List_D_LevelID
GO
ALTER TABLE dbo.Usr_List_D
	DROP CONSTRAINT DF_Usr_List_D_endpointId
GO
ALTER TABLE dbo.Usr_List_D
	DROP CONSTRAINT DF_Usr_List_D_DateFormat
GO
ALTER TABLE dbo.Usr_List_D
	DROP CONSTRAINT DF_Usr_List_D_enableAV
GO
ALTER TABLE dbo.Usr_List_D
	DROP CONSTRAINT DF_Usr_List_D_timezonedisplay
GO
ALTER TABLE dbo.Usr_List_D
	DROP CONSTRAINT DF_Usr_List_D_timeformat
GO
ALTER TABLE dbo.Usr_List_D
	DROP CONSTRAINT DF_Usr_List_D_enableParticipants
GO
ALTER TABLE dbo.Usr_List_D
	DROP CONSTRAINT DF_Usr_List_D_enableExchange
GO
ALTER TABLE dbo.Usr_List_D
	DROP CONSTRAINT DF_Usr_List_D_enableDomino
GO
CREATE TABLE dbo.Tmp_Usr_List_D
	(
	Id int NOT NULL IDENTITY (1, 1),
	UserID int NOT NULL,
	FirstName nvarchar(256) NULL,
	LastName nvarchar(256) NULL,
	Email nvarchar(512) NULL,
	Financial int NULL,
	Admin smallint NULL,
	Login nvarchar(256) NULL,
	Password nvarchar(1000) NULL,
	Company nvarchar(512) NULL,
	TimeZone smallint NULL,
	Language smallint NULL,
	PreferedRoom varchar(150) NULL,
	AlternativeEmail nvarchar(512) NULL,
	DoubleEmail smallint NULL,
	PreferedGroup smallint NULL,
	CCGroup smallint NULL,
	Telephone nvarchar(50) NULL,
	RoomID int NULL,
	PreferedOperator int NULL,
	lockCntTrns int NULL,
	DefLineRate int NULL,
	DefVideoProtocol int NULL,
	Deleted int NOT NULL,
	DefAudio int NULL,
	DefVideoSession int NULL,
	MenuMask nvarchar(200) NULL,
	initialTime int NULL,
	EmailClient smallint NULL,
	newUser smallint NULL,
	PrefTZSID int NULL,
	IPISDNAddress nvarchar(256) NULL,
	DefaultEquipmentId smallint NOT NULL,
	connectionType smallint NULL,
	companyId int NULL,
	securityKey nvarchar(256) NULL,
	LockStatus smallint NOT NULL,
	LastLogin datetime NULL,
	outsidenetwork smallint NULL,
	emailmask bigint NULL,
	roleID int NULL,
	addresstype smallint NULL,
	accountexpiry datetime NULL,
	approverCount int NULL,
	BridgeID int NOT NULL,
	Title nvarchar(50) NULL,
	LevelID int NULL,
	preferredISDN nvarchar(50) NULL,
	portletId int NULL,
	searchId int NULL,
	endpointId int NOT NULL,
	DateFormat nvarchar(50) NULL,
	enableAV int NULL,
	timezonedisplay int NULL,
	timeformat int NULL,
	enableParticipants int NULL,
	TickerStatus int NULL,
	TickerSpeed int NULL,
	TickerPosition int NULL,
	TickerBackground nchar(10) NULL,
	TickerDisplay int NULL,
	RSSFeedLink nvarchar(50) NULL,
	TickerStatus1 int NULL,
	TickerSpeed1 int NULL,
	TickerPosition1 int NULL,
	TickerBackground1 nchar(10) NULL,
	TickerDisplay1 int NULL,
	RSSFeedLink1 nvarchar(50) NULL,
	enableExchange int NULL,
	enableDomino int NULL
	)  ON [PRIMARY]
GO
DECLARE @v sql_variant 
SET @v = N''
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_Usr_List_D', N'COLUMN', N'TimeZone'
GO
DECLARE @v sql_variant 
SET @v = N''
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_Usr_List_D', N'COLUMN', N'Language'
GO
DECLARE @v sql_variant 
SET @v = N''
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_Usr_List_D', N'COLUMN', N'PreferedRoom'
GO
DECLARE @v sql_variant 
SET @v = N''
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_Usr_List_D', N'COLUMN', N'DoubleEmail'
GO
ALTER TABLE dbo.Tmp_Usr_List_D ADD CONSTRAINT
	DF_Usr_List_D_Financial DEFAULT ((0)) FOR Financial
GO
ALTER TABLE dbo.Tmp_Usr_List_D ADD CONSTRAINT
	DF_Usr_List_D_Admin DEFAULT ((0)) FOR Admin
GO
ALTER TABLE dbo.Tmp_Usr_List_D ADD CONSTRAINT
	DF_Usr_List_D_TimeZone DEFAULT ((1)) FOR TimeZone
GO
ALTER TABLE dbo.Tmp_Usr_List_D ADD CONSTRAINT
	DF_Usr_List_D_Language DEFAULT ((1)) FOR Language
GO
ALTER TABLE dbo.Tmp_Usr_List_D ADD CONSTRAINT
	DF_Usr_List_D_PreferedRoom DEFAULT ((1)) FOR PreferedRoom
GO
ALTER TABLE dbo.Tmp_Usr_List_D ADD CONSTRAINT
	DF_Usr_List_D_DoubleEmail DEFAULT ((0)) FOR DoubleEmail
GO
ALTER TABLE dbo.Tmp_Usr_List_D ADD CONSTRAINT
	DF_Usr_List_D_PreferedGroup DEFAULT ((1)) FOR PreferedGroup
GO
ALTER TABLE dbo.Tmp_Usr_List_D ADD CONSTRAINT
	DF_Usr_List_D_CCGroup DEFAULT ((1)) FOR CCGroup
GO
ALTER TABLE dbo.Tmp_Usr_List_D ADD CONSTRAINT
	DF_Usr_List_D_RoomID DEFAULT ((1)) FOR RoomID
GO
ALTER TABLE dbo.Tmp_Usr_List_D ADD CONSTRAINT
	DF_Usr_List_D_PreferedOperator DEFAULT ((1)) FOR PreferedOperator
GO
ALTER TABLE dbo.Tmp_Usr_List_D ADD CONSTRAINT
	DF_Usr_List_D_lockCntTrns DEFAULT ((0)) FOR lockCntTrns
GO
ALTER TABLE dbo.Tmp_Usr_List_D ADD CONSTRAINT
	DF_Usr_List_D_DefLineRate DEFAULT ((2)) FOR DefLineRate
GO
ALTER TABLE dbo.Tmp_Usr_List_D ADD CONSTRAINT
	DF_Usr_List_D_DefVideoProtocol DEFAULT ((1)) FOR DefVideoProtocol
GO
ALTER TABLE dbo.Tmp_Usr_List_D ADD CONSTRAINT
	DF_Usr_List_D_Deleted DEFAULT ((0)) FOR Deleted
GO
ALTER TABLE dbo.Tmp_Usr_List_D ADD CONSTRAINT
	DF_Usr_List_D_EmailClient DEFAULT ((0)) FOR EmailClient
GO
ALTER TABLE dbo.Tmp_Usr_List_D ADD CONSTRAINT
	DF_Usr_List_D_newUser DEFAULT ((1)) FOR newUser
GO
ALTER TABLE dbo.Tmp_Usr_List_D ADD CONSTRAINT
	DF_Usr_List_D_PrefTZSID DEFAULT ((0)) FOR PrefTZSID
GO
ALTER TABLE dbo.Tmp_Usr_List_D ADD CONSTRAINT
	DF_Usr_List_D_DefaultEquipmentId DEFAULT ((1)) FOR DefaultEquipmentId
GO
ALTER TABLE dbo.Tmp_Usr_List_D ADD CONSTRAINT
	DF_Usr_List_D_LockStatus DEFAULT ((0)) FOR LockStatus
GO
ALTER TABLE dbo.Tmp_Usr_List_D ADD CONSTRAINT
	DF_Usr_List_D_LastLogin DEFAULT (getutcdate()) FOR LastLogin
GO
ALTER TABLE dbo.Tmp_Usr_List_D ADD CONSTRAINT
	DF_Usr_List_D_accountexpiry DEFAULT (getutcdate()) FOR accountexpiry
GO
ALTER TABLE dbo.Tmp_Usr_List_D ADD CONSTRAINT
	DF_Usr_List_D_approverCount DEFAULT ((0)) FOR approverCount
GO
ALTER TABLE dbo.Tmp_Usr_List_D ADD CONSTRAINT
	DF_Usr_List_D_BridgeID DEFAULT ((1)) FOR BridgeID
GO
ALTER TABLE dbo.Tmp_Usr_List_D ADD CONSTRAINT
	DF_Usr_List_D_LevelID DEFAULT ((0)) FOR LevelID
GO
ALTER TABLE dbo.Tmp_Usr_List_D ADD CONSTRAINT
	DF_Usr_List_D_endpointId DEFAULT ((0)) FOR endpointId
GO
ALTER TABLE dbo.Tmp_Usr_List_D ADD CONSTRAINT
	DF_Usr_List_D_DateFormat DEFAULT (N'MM/dd/yyyy') FOR DateFormat
GO
ALTER TABLE dbo.Tmp_Usr_List_D ADD CONSTRAINT
	DF_Usr_List_D_enableAV DEFAULT ((0)) FOR enableAV
GO
ALTER TABLE dbo.Tmp_Usr_List_D ADD CONSTRAINT
	DF_Usr_List_D_timezonedisplay DEFAULT ((0)) FOR timezonedisplay
GO
ALTER TABLE dbo.Tmp_Usr_List_D ADD CONSTRAINT
	DF_Usr_List_D_timeformat DEFAULT ((0)) FOR timeformat
GO
ALTER TABLE dbo.Tmp_Usr_List_D ADD CONSTRAINT
	DF_Usr_List_D_enableParticipants DEFAULT ((0)) FOR enableParticipants
GO
ALTER TABLE dbo.Tmp_Usr_List_D ADD CONSTRAINT
	DF_Usr_List_D_enableExchange DEFAULT ((0)) FOR enableExchange
GO
ALTER TABLE dbo.Tmp_Usr_List_D ADD CONSTRAINT
	DF_Usr_List_D_enableDomino DEFAULT ((0)) FOR enableDomino
GO
SET IDENTITY_INSERT dbo.Tmp_Usr_List_D ON
GO
IF EXISTS(SELECT * FROM dbo.Usr_List_D)
	 EXEC('INSERT INTO dbo.Tmp_Usr_List_D (Id, UserID, FirstName, LastName, Email, Financial, Admin, Login, Password, Company, TimeZone, Language, PreferedRoom, AlternativeEmail, DoubleEmail, PreferedGroup, CCGroup, Telephone, RoomID, PreferedOperator, lockCntTrns, DefLineRate, DefVideoProtocol, Deleted, DefAudio, DefVideoSession, MenuMask, initialTime, EmailClient, newUser, PrefTZSID, IPISDNAddress, DefaultEquipmentId, connectionType, companyId, securityKey, LockStatus, LastLogin, outsidenetwork, emailmask, roleID, addresstype, accountexpiry, approverCount, BridgeID, Title, LevelID, preferredISDN, portletId, searchId, endpointId, DateFormat, enableAV, timezonedisplay, timeformat, enableParticipants, TickerStatus, TickerSpeed, TickerPosition, TickerBackground, TickerDisplay, RSSFeedLink, TickerStatus1, TickerSpeed1, TickerPosition1, TickerBackground1, TickerDisplay1, RSSFeedLink1, enableExchange, enableDomino)
		SELECT Id, UserID, FirstName, LastName, Email, Financial, Admin, Login, Password, Company, TimeZone, Language, PreferedRoom, AlternativeEmail, DoubleEmail, PreferedGroup, CCGroup, Telephone, RoomID, PreferedOperator, lockCntTrns, DefLineRate, DefVideoProtocol, Deleted, DefAudio, DefVideoSession, MenuMask, initialTime, EmailClient, newUser, PrefTZSID, IPISDNAddress, DefaultEquipmentId, connectionType, companyId, securityKey, LockStatus, LastLogin, outsidenetwork, emailmask, roleID, addresstype, accountexpiry, approverCount, BridgeID, Title, LevelID, preferredISDN, portletId, searchId, endpointId, DateFormat, enableAV, timezonedisplay, timeformat, enableParticipants, TickerStatus, TickerSpeed, TickerPosition, TickerBackground, TickerDisplay, RSSFeedLink, TickerStatus1, TickerSpeed1, TickerPosition1, TickerBackground1, TickerDisplay1, RSSFeedLink1, enableExchange, enableDomino FROM dbo.Usr_List_D WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_Usr_List_D OFF
GO
DROP TABLE dbo.Usr_List_D
GO
EXECUTE sp_rename N'dbo.Tmp_Usr_List_D', N'Usr_List_D', 'OBJECT' 
GO
ALTER TABLE dbo.Usr_List_D ADD CONSTRAINT
	PK_Usr_List_D PRIMARY KEY CLUSTERED 
	(
	UserID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT

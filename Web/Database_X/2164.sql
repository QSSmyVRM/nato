update Loc_Tier3_D set disabled = 1 where Id = 0
update Loc_Tier2_D set disabled = 1 where L3LocationId = 0

INSERT INTO err_list_s  (ID,Message,Description,Level,Languageid,ErrorType)
VALUES (542,'Audio/Video conferences have been disabled by the administrator. Please Contact your VRM Adminstrator.',
'Audio/Video confs are disabled.','E',1,'U');

INSERT INTO err_list_s  (ID,Message,Description,Level,Languageid,ErrorType)
VALUES (543,'Audio Only conferences have been disabled by the administrator. Please Contact your VRM Adminstrator.',
'Audio Only confs are disabled.','E',1,'U');

INSERT INTO err_list_s  (ID,Message,Description,Level,Languageid,ErrorType)
VALUES (544,'Room conferences have been disabled by the administrator. Please Contact your VRM Adminstrator.',
'Room confs are disabled.','E',1,'U');

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Usr_Inactive_D ADD
	PluginConfirmations smallint NULL
GO
ALTER TABLE dbo.Usr_Inactive_D ADD CONSTRAINT
	DF_Usr_Inactive_D_PluginConfirmations DEFAULT 0 FOR PluginConfirmations
GO
COMMIT

Update Usr_Inactive_D  set PluginConfirmations  = 0

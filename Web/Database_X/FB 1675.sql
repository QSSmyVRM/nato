

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_Room_D ADD
	disabled smallint NULL
GO
ALTER TABLE dbo.Conf_Room_D ADD CONSTRAINT
	DF_Conf_Room_D_disabled DEFAULT 0 FOR disabled
GO
COMMIT

Update dbo.Conf_Room_D set disabled = 0

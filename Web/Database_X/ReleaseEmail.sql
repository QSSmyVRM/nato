/*
   Monday, March 14, 20117:45:01 PM
   User: myvrm
   Server: gowniyanvm
   Database: V2X
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Email_Queue_D ADD
	Release smallint NULL
GO
ALTER TABLE dbo.Email_Queue_D ADD CONSTRAINT
	DF_Email_Queue_D_Release DEFAULT 0 FOR Release
GO
COMMIT

Update Email_Queue_D set release = 0
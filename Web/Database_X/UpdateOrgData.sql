Insert into [Org_List_D]
([orgname],[address1],[address2],[city],[state],[country],[zipcode],[phone],[fax],[email],[website],[deleted],[disabled])
values
('Base Company','','','',34,225,'12345','','','basecompany@myvrm.com','www.myvrm.com',0,0)
Go

INSERT INTO [dbo].[Org_Settings_D]
(
[LastModified], [LastModifiedUser], [Logo], [CompanyTel], [CompanyEmail], 
[CompanyURL], [Connect2], [DialOut], [AccountingLogic], [BillPoint2Point], 
[AllLocation], [tzsystemid], [RealtimeStatus], [BillRealTime], 
[MultipleDepartments], [overAllocation], [Threshold], [autoApproveImmediate], 
[adminEmail1], [adminEmail2],[adminEmail3], [AutoAcceptModConf], [recurEnabled], 
[responsetime], [responsemessage],[SystemStartTime], [SystemEndTime], [Open24hrs], 
[Offdays], [ISDNLineCost], [ISDNPortCost], [IPLineCost], [IPPortCost], 
[ISDNTimeFrame], [dynamicinviteenabled], 
[doublebookingenabled], [IMEnabled], [IMRefreshConn], [IMMaxUnitConn], 
[IMMaxSysConn], [DefaultToPublic], [DefaultConferenceType], [EnableRoomConference],
[EnableAudioVideoConference], [EnableAudioOnlyConference], [DefaultCalendarToOfficeHours], 
[RoomTreeExpandLevel], EnableBufferZone,EnableCustomOption,RoomLimit,
MCULimit,UserLimit,OrgId ,[MaxNonVideoRooms]
,[MaxVideoRooms],[MaxEndpoints],[MaxCDRs],[EnableFacilities]
,[EnableCatering],[EnableHousekeeping],[EnableAPI],[Language]
,[ExchangeUserLimit],[DominoUserLimit]
)
VALUES
('20071010 16:21:56.420',11, N'ABC', NULL, N'vrmadmin@myVrm.com', 
N'', 1, 1, 0, 1, 0, 1, 1, 1, 0, 0, NULL, 
0, 0, 0, 0, 1, 1, 60, N'', '19000101 00:00:00.000', 
'19000101 23:59:00.000', 1, N'1,7', 2, 1, 33, 3, 
1, 1, 0, 0, 30, 3, 30, 0,7,1, 1, 1, 1, 3,1,1,1,1,1,11,0,1,1,0,0,0,0,0,0,0,0)
GO

/* Insert default data into diagnostics table for base company */
Insert into [dbo].[Org_Diagnostics_D]
(PurgeType,UpdatedDate,TimeofDay,DayofWeek,ServiceStatus,PurgeData,OrgId)
values
(0,getdate(),getdate(),5,0,1,11)
Go


update Sys_TechSupport_D set orgid = 11
Go
update Sys_Approver_D set orgid = 11
Go
update Conf_Conference_D set orgid = 11
Go
update Usr_SearchTemplates_D set orgid = 11
Go
update Usr_Templates_D set companyId = 11
Go
update Loc_Room_D set orgid = 11
Go
update usr_list_d set companyid = 11
Go
update usr_inactive_d set companyid = 11
Go
update MCU_List_D set orgid = 11
Go
update Dept_List_D set orgid = 11
Go
update Grp_Detail_D set orgid = 11
Go
update Loc_Tier3_D set orgid = 11
Go
update Loc_Tier2_D set orgid = 11
Go
update Tmp_list_d set orgid = 11
Go
Update Dept_CustomAttr_D set orgid=11
Go
update Inv_Category_D set orgid =11
GO
update Inv_Workorder_D set orgid =11
GO
update Acc_Balance_D set orgid =11
GO
update Inv_List_D set orgid =11
GO
update Inv_ItemCharge_D set orgid =11
GO
update Usr_Accord_D set orgid =11
GO
update Usr_GuestList_D set companyId =11
GO
Update Ept_List_D set orgid=11
GO
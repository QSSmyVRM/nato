/*
   Thursday, January 13, 20111:07:03 PM
   User: myvrm
   Server: gowniyanvm
   Database: V2X
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Loc_Room_D ADD
	isVIP smallint NULL
GO
ALTER TABLE dbo.Loc_Room_D ADD CONSTRAINT
	DF_Loc_Room_D_isVIP DEFAULT 0 FOR isVIP
GO
COMMIT

update Loc_Room_D set isVIP = 0

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE [dbo].[Gen_ServiceType_S](
	[ID] [int] NOT NULL,
	[ServiceType] [nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
) ON [PRIMARY]
GO
COMMIT

Insert into Gen_ServiceType_S (ID, ServiceType) values (1,'Gold')
Insert into Gen_ServiceType_S (ID, ServiceType) values (2,'Platinum')
Insert into Gen_ServiceType_S (ID, ServiceType) values (3,'Silver')



BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Loc_Room_D ADD
	ServiceType smallint 
GO
COMMIT


BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_Conference_D ADD
	ServiceType smallint 
GO
COMMIT

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
 EnableRoomServiceType smallint 
GO
COMMIT 


Update dbo.Conf_Conference_D SET ServiceType = -1

Update dbo.Loc_Room_D SET ServiceType = -1

update err_list_s set message = 'Room conflict, please modify Date / Time or Service Type.' where id = 241

update err_list_s set message = 'Room conflicts found in some recurring instances. Please modify Date / Time or Service Type.' where id = 510




-- Organization Module

INSERT INTO [dbo].[Err_List_S]
           ([ID]
           ,[Message]
           ,[Description]
           ,[Level])
     VALUES
           (423
           ,'Invalid Organization ID'
           ,'Organization ID Missing'
           ,'S')

--Image Module

INSERT INTO [dbo].[Err_List_S]
           ([ID]
           ,[Message]
           ,[Description]
           ,[Level])
     VALUES
           (424
           ,'Invalid Image or ID Missing'
           ,'Invalid Image or ID Missing'
           ,'S')


--FB 1603

INSERT INTO [dbo].[Err_List_S]
           ([ID]
           ,[Message]
           ,[Description]
           ,[Level])
     VALUES
           (425
           ,'Site is currently not activated.  Please contact your VRM administrator for assistance'
           ,'Site is currently not activated.  Please contact your VRM administrator for assistance'
           ,'S')


--FB 1604

INSERT INTO [dbo].[Err_List_S]
           ([ID]
           ,[Message]
           ,[Description]
           ,[Level])
     VALUES
           (426
           ,'The requested email address is already in use - Inactive state'
           ,'The requested email address is already in use - Inactive state'
           ,'E')

--FB 1617

INSERT INTO [dbo].[Err_List_S]
           ([ID]
           ,[Message]
           ,[Description]
           ,[Level])
     VALUES
           (427
           ,'Name already exists for tier1.'
           ,'Name already exists for tier1.'
           ,'E')

INSERT INTO [dbo].[Err_List_S]
           ([ID]
           ,[Message]
           ,[Description]
           ,[Level])
     VALUES
           (428
           ,'Name already exists for tier2.'
           ,'Name already exists for tier2.'
           ,'E')

INSERT INTO [dbo].[Err_List_S]
           ([ID]
           ,[Message]
           ,[Description]
           ,[Level])
     VALUES
           (429
           ,'Template license expired. Please contact your VRM Administrator.'
           ,'Template license expired. Please contact your VRM Administrator.'
           ,'S')

/* *** FB 1577 - User Friendly Error Message *** */

Update Err_List_S set [Message]='Delete error.' 
,[Description]='Delete error.' where ID=325
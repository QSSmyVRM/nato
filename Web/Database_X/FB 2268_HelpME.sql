/*        ************************** Help Me Icon  Start*****************************  */

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Usr_List_D ADD
	HelpReqEmailID nvarchar(512) NULL,
	HelpReqPhone nvarchar(50) NULL
	
GO
COMMIT


/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Usr_Inactive_D ADD
	HelpReqEmailID nvarchar(512) NULL,
	HelpReqPhone nvarchar(50) NULL
GO
COMMIT

--- Email Content 
INSERT INTO [dbo].[Email_Content_D]([EmailLangId],[EmailSubject],[EmailBody],[Placeholders],[EmailMode],[Emailtypeid],[EmailLanguage])VALUES(1,'Help Request','<p><span style="font-family: Arial; font-size: 11pt"><span style="color: #000080">{1}<br /></span></span></p><p><span style="font-family: Arial; font-size: 11pt"><span style="color: #000080">Hello,</span></span></p><p><span style="font-family: Arial; font-size: 11pt"><span style="color: #000080">The following user<strong> {2}</strong> requires help.</span></span></p><p style="text-align: justify; margin: 0in 0in 11pt" class="MsoNormal"><span style="text-decoration: ; text-decoration: underline;"><span style="line-height: 115%;font-family: Arial,sans-serif; color: #000080; font-size: 11pt">Tech Support:</span></p><p style="margin: 0in 0in 0pt" class="MsoNoSpacing"><span style="font-family: Arial,sans-serif; color: #000080; font-size: 11pt">ContacContact : {37}</span></p><p style="margin: 0in 0in 0pt" class="MsoNoSpacing"><span style="font-family: Arial,sans-serif; color: #000080; font-size: 11pt">Email : {38}</span></p><p style="margin: 0in 0in 0pt" class="MsoNoSpacing"><span style="font-family: Arial,sans-serif; color: #000080; font-size: 11pt">Phone : {39}</span></p><br /><br /><p class="MsoNormal" style="text-align: justify; margin: 0in 0in 11pt"><span style="line-height: 115%; font-family: Arial,sans-serif; color: #000080; font-size: 11pt">Googlemap Location:<br /><br /><img id="GoogleMap_img" src="{40}"/></td></tr></table></p>','1,2,37,38,39,40',0,31,'en')

INSERT INTO [dbo].[Email_Content_D]([EmailLangId],[EmailSubject],[EmailBody],[Placeholders],[EmailMode],[Emailtypeid],[EmailLanguage])VALUES(5,'Help Request','<p><span style="font-family: Arial; font-size: 11pt"><span style="color: #000080">{1}<br /></span></span></p><p><span style="font-family: Arial; font-size: 11pt"><span style="color: #000080">Hello,</span></span></p><p><span style="font-family: Arial; font-size: 11pt"><span style="color: #000080">The following user<strong> {2}</strong> requires help.</span></span></p><p style="text-align: justify; margin: 0in 0in 11pt" class="MsoNormal"><span style="text-decoration: ; text-decoration: underline;"><span style="line-height: 115%;font-family: Arial,sans-serif; color: #000080; font-size: 11pt">Tech Support:</span></p><p style="margin: 0in 0in 0pt" class="MsoNoSpacing"><span style="font-family: Arial,sans-serif; color: #000080; font-size: 11pt">ContacContact : {37}</span></p><p style="margin: 0in 0in 0pt" class="MsoNoSpacing"><span style="font-family: Arial,sans-serif; color: #000080; font-size: 11pt">Email : {38}</span></p><p style="margin: 0in 0in 0pt" class="MsoNoSpacing"><span style="font-family: Arial,sans-serif; color: #000080; font-size: 11pt">Phone : {39}</span></p><br /><br /><p class="MsoNormal" style="text-align: justify; margin: 0in 0in 11pt"><span style="line-height: 115%; font-family: Arial,sans-serif; color: #000080; font-size: 11pt">Googlemap Location:<br /><br /><img id="GoogleMap_img" src="{40}"/></td></tr></table></p>','1,2,37,38,39,40',0,31,'fr')

--Error Message

INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (605,'Please enter Phone Number','Please enter Phone Number','E',1,'U');

INSERT INTO Err_List_S (ID, [Message], Description, [Level], Languageid, ErrorType)
VALUES (606,'No Help Request email entry found.','No Help Request email entry found.','E',1,'U');

/*        ************************** Help Me Icon End *****************************  */



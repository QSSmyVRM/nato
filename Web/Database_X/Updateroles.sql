
update usr_roles_d set roleName='General User', roleMenuMask='6*60-4*15+8*0+3*0+2*0+8*0+2*0+2*0+2*0+1*0-6*63' where roleID=1
update usr_roles_d set roleName='Site Administrator', roleMenuMask='6*63-4*15+8*255+3*7+2*3+8*255+2*3+2*3+2*3+1*1-6*63' where roleID=3
update usr_roles_d set roleName='Catering Administrator', roleMenuMask='6*34-4*0+8*2+3*0+2*0+8*0+2*0+2*3+2*0+1*0-6*63' where roleID=4
update usr_roles_d set roleName='Inventory Administrator', roleMenuMask='6*34-4*15+8*4+3*0+2*0+8*0+2*3+2*0+2*0+1*0-6*63' where roleID=5
update usr_roles_d set roleName='Housekeeping Administrator', roleMenuMask='6*34-4*15+8*1+3*0+2*0+8*0+2*0+2*0+2*3+1*0-6*63' where roleID=6

update usr_roles_d set roleName='Organization Administrator',[level]=2, roleMenuMask='6*62-4*15+8*255+3*7+2*3+8*255+2*3+2*3+2*3+1*0-6*63' where roleID=2

update usr_list_d set usr_list_d.menumask = b.rolemenumask from usr_roles_d as b where usr_list_d.roleid  = b.roleid 

update usr_roles_d set level=0, roleMenuMask='6*60-4*15+8*0+3*0+2*0+8*0+2*0+2*0+2*0+1*0-6*63' where roleID > 6


update Usr_Roles_D set crossaccess=0
update Usr_Roles_D set crossaccess=1 where roleid=3
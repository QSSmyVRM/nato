
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT

BEGIN TRANSACTION
GO

ALTER TABLE dbo.Org_Settings_D ADD
	CustomAttributeLimit smallint NULL
GO
ALTER TABLE dbo.Org_Settings_D ADD CONSTRAINT
	DF_Org_Settings_D_CustomAttributeLimit DEFAULT 10 FOR CustomAttributeLimit
GO
COMMIT


/* Maximum limit is  10 */

Update Org_Settings_D set CustomAttributeLimit=10


/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Dept_CustomAttr_D ADD
	CreateType smallint NULL
GO
ALTER TABLE dbo.Dept_CustomAttr_D ADD CONSTRAINT
	DF_Dept_CustomAttr_D_CreateType DEFAULT 0 FOR CreateType
GO
COMMIT


/* CreateType .. System -1 User -0 */
Update Dept_CustomAttr_D set CreateType=0

/*  Express Interface Specific Queries */

/*  User Roles Menumask Updation */

--Make sure the ROLEIDs are correct for the corresponding ROLE.

update usr_roles_d set roleMenuMask='8*240-4*15+8*0+3*0+2*0+8*0+2*0+2*0+2*0+1*0-6*14' where roleID=1

update usr_roles_d set roleMenuMask='8*248-4*15+8*255+3*7+2*3+8*255+2*3+2*3+2*3+1*0-6*63' where roleID=2

update usr_roles_d set roleMenuMask='8*252-4*15+8*255+3*7+2*3+8*255+2*3+2*3+2*3+1*1-6*63' where roleID=3

update usr_roles_d set roleMenuMask='8*136-4*0+8*2+3*0+2*0+8*0+2*0+2*3+2*0+1*0-6*63' where roleID=4

update usr_roles_d set roleMenuMask='8*136-4*0+8*4+3*0+2*0+8*0+2*3+2*0+2*0+1*0-6*63' where roleID=5

update usr_roles_d set roleMenuMask='8*136-4*0+8*1+3*0+2*0+8*0+2*0+2*0+2*3+1*0-6*63' where roleID=6


update usr_list_d set usr_list_d.menumask = b.rolemenumask from usr_roles_d as b where usr_list_d.roleid  = b.roleid


--Excute the below only once to create Express Profile

Declare @roldid int

set @roldid = (select MAX(roleID)+ 1 from Usr_Roles_D)

insert into Usr_Roles_D (roleID,roleName,roleMenuMask,locked,level,crossaccess) values(
@roldid ,'Express Profile','8*3-4*0+8*0+3*0+2*0+8*0+2*0+2*0+2*0+1*0-6*2',1,0,0)


/* Express Interface - Custom Attributes Specific */

DECLARE @orgID INT

DECLARE cusAttrCursor CURSOR for
SELECT orgID FROM Org_List_D 

OPEN cusAttrCursor

FETCH NEXT FROM cusAttrCursor INTO @orgID
WHILE @@FETCH_STATUS = 0
BEGIN

Declare @id int

set @id = (select isnull(MAX(CustomAttributeId),0)+ 1 from Dept_CustomAttr_D)

INSERT INTO [Dept_CustomAttr_D]
([DeptID],[CustomAttributeId],[DisplayTitle],[Description],[Type],[Mandatory],[Deleted]
,[status],[IncludeInEmail],[orgId],[RoomApp],[McuApp],[SystemApp],[Scheduler],[Host]
,[Party] ,[McuAdmin],[RoomAdmin],[CreateType])
VALUES
(0,@id,'Work','Host',4,0,0,0,0,@orgID,0,0,0,0,0,0,0,0,1)

set @id = (select MAX(CustomAttributeId)+ 1 from Dept_CustomAttr_D)

INSERT INTO [Dept_CustomAttr_D]
([DeptID],[CustomAttributeId],[DisplayTitle],[Description],[Type],[Mandatory],[Deleted]
,[status],[IncludeInEmail],[orgId],[RoomApp],[McuApp],[SystemApp],[Scheduler],[Host]
,[Party] ,[McuAdmin],[RoomAdmin],[CreateType])
VALUES
(0,@id,'Cell','Host',4,0,0,0,0,@orgID,0,0,0,0,0,0,0,0,1)

set @id = (select MAX(CustomAttributeId)+ 1 from Dept_CustomAttr_D)

INSERT INTO [Dept_CustomAttr_D]
([DeptID],[CustomAttributeId],[DisplayTitle],[Description],[Type],[Mandatory],[Deleted]
,[status],[IncludeInEmail],[orgId],[RoomApp],[McuApp],[SystemApp],[Scheduler],[Host]
,[Party] ,[McuAdmin],[RoomAdmin],[CreateType])
VALUES
(0,@id,'Special Instructions','Special Instructions',10,0,0,0,1,@orgID,0,0,0,0,0,0,1,0,1)


/*
set @id = (select MAX(CustomAttributeId)+ 1 from Dept_CustomAttr_D)

INSERT INTO [Dept_CustomAttr_D]
([DeptID],[CustomAttributeId],[DisplayTitle],[Description],[Type],[Mandatory],[Deleted]
,[status],[IncludeInEmail],[orgId],[RoomApp],[McuApp],[SystemApp],[Scheduler],[Host]
,[Party] ,[McuAdmin],[RoomAdmin],[CreateType])
VALUES
(0,@id,'Session URL','Web Conference Instructions',7,0,0,0,0,@orgID,0,0,0,0,0,0,0,0,1)

set @id = (select MAX(CustomAttributeId)+ 1 from Dept_CustomAttr_D)

INSERT INTO [Dept_CustomAttr_D]
([DeptID],[CustomAttributeId],[DisplayTitle],[Description],[Type],[Mandatory],[Deleted]
,[status],[IncludeInEmail],[orgId],[RoomApp],[McuApp],[SystemApp],[Scheduler],[Host]
,[Party] ,[McuAdmin],[RoomAdmin],[CreateType])
VALUES
(0,@id,'Web Conference #','Web Conference Instructions',4,0,0,0,0,@orgID,0,0,0,0,0,0,0,0,1)

*/

FETCH NEXT
FROM cusAttrCursor INTO @orgID
END
CLOSE cusAttrCursor
DEALLOCATE cusAttrCursor
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[SelectCalendarDetails] 
(
	-- Add the parameters for the stored procedure here
	@timezone			int=null,
	@startTime			datetime=null,
	@endTime			datetime=null,
	@specificEntityCode int=null,
	@codeType			int=null,
	@tablename			varchar(200)=null
)

As
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if (@timezone is null)
	begin
		return
	end
	
	if (@codeType is null)
	begin
		return
	end

	/* Temp table for Calendar details */

	CREATE TABLE #TempCalendarDT(
	[Date] datetime,
	Start varchar(50),
	[End] varchar(50), 
	Duration int, 
	Room varchar(500),
	[Meeting Title] varchar(1000),
	[Last Name] varchar(100),
	First varchar(100),
	email varchar(200),
	Phone varchar(50),
	State varchar(100),
	City varchar(100),
	[Conf #] varchar(50), 
	[Meeting Type] varchar(100),
	Category varchar(50),
	Status varchar(50),
	Remarks varchar(500),
	conftype int,
	connectionType int,
	defVideoProtocol int,
	vidProtocol varchar(20),
	TypeFlag char(1),
	CustomAttributeId int, 
	SelectedOptionId int,
	addresstype int,
	[Entity Code] varchar(50),
	[Entity Name] varchar(200)
)

	declare @ecode as varchar(50)
	declare @optionid as int
	declare @calqry as varchar(4000)
	declare @where as varchar(2000)
	declare @cond as varchar(1000)
	declare @filter as varchar(1000)

	declare @seloptionid as int

	if(@codeType = 1) -- None (Need to identify code)
		begin
			set @seloptionid = ''
			set @ecode = 'NA'			
		end
	else if(@codeType = 2) -- Specific
		begin
			set @seloptionid = @specificEntityCode	
			SELECT @ecode = Caption FROM Dept_CustomAttr_Option_D where OptionId=@specificEntityCode and Caption is not null
		end
	
	set @filter = ''

	if (@startTime is not null And @endTime is not null)
		begin
			set @filter = @filter + ' AND c.confdate BETWEEN dbo.changeTOGMTtime('+ cast(@timezone as varchar(2)) +','''+ cast(@startTime as varchar(40)) +''') AND dbo.changeTOGMTtime('+ cast(@timezone as varchar(2)) +','''+ cast(@endTime as varchar(40)) +''')'
		end

	if (@tablename <> '' AND @tablename is not null)
		begin
			if exists (select * from dbo.sysobjects where [name]=@tablename and xtype='U')
			 begin
				set @filter = @filter +' AND cu.roomid IN (Select roomid from '+ @tablename +')'
			 end
		end

/* Details Part */
	set @calqry = 'Select a.*, isnull(d.[Entity Code],''NA'') as [Entity Code], isnull(d.[Entity Name],''NA'') as [Entity Name] From '
	set @calqry = @calqry + '(SELECT dbo.changeTime('+ cast(@timezone as varchar(2)) +',c.confdate) AS Date,'
	set @calqry = @calqry + 'substring(convert(char(30),dbo.changeTime('+ cast(@timezone as varchar(2)) +', c.confdate) - dateadd(dd,0, datediff(dd,0, dbo.changeTime('+ cast(@timezone as varchar(2)) +', c.confdate)))),13,8) as Start,'
	set @calqry = @calqry + 'substring(convert(char(30),dbo.changeTime('+ cast(@timezone as varchar(2)) +',dateadd(minute,c.duration, c.confdate)) - dateadd(dd,0, datediff(dd,0, dbo.changeTime('+ cast(@timezone as varchar(2)) +',dateadd(minute,c.duration, c.confdate))))),13,8) as [End],'
	set @calqry = @calqry + ' c.Duration, l.name AS Room, c.externalname AS [Meeting Title], u.lastname as [Last Name], '
	set @calqry = @calqry + ' u.firstname as First, u.email,'
	set @calqry = @calqry + ' Case when ((len(isnull(l.AssistantPhone,''''))>0) And (len(isnull(u.Telephone,''''))>0)) Then (l.AssistantPhone + '', '' + u.Telephone) ' 
	set @calqry = @calqry + ' when ((len(isnull(l.AssistantPhone,''''))>0) And (len(isnull(u.Telephone,''''))=0)) Then (l.AssistantPhone) '
	set @calqry = @calqry + ' when ((len(isnull(l.AssistantPhone,''''))=0) And (len(isnull(u.Telephone,''''))>0)) Then (u.Telephone) ' 
	set @calqry = @calqry + ' when ((len(isnull(l.AssistantPhone,''''))=0) And (len(isnull(u.Telephone,''''))=0)) Then ('''')'
	set @calqry = @calqry + ' End   AS Phone ' 
	set @calqry = @calqry + ' ,l3.name AS State, l2.name AS City, '
	set @calqry = @calqry + ' c.confnumname AS [Conf #],'
	set @calqry = @calqry + ' case c.conftype '
	set @calqry = @calqry + ' when 1 then ''Video'' '
	set @calqry = @calqry + ' when 2 then ''AudioVideo'' '
	set @calqry = @calqry + ' when 3 then ''Immediate'' '
	set @calqry = @calqry + ' when 4 then ''Point to Point'' '
	set @calqry = @calqry + ' when 5 then ''Template'' '
	set @calqry = @calqry + ' when 6 then ''AudioOnly'' '
	set @calqry = @calqry + ' when 7 then ''RoomOnly'' '
	set @calqry = @calqry + ' when 11 then ''Phantom'' '
	set @calqry = @calqry + ' end AS [Meeting Type],'

/*	set @calqry = @calqry + ' case cu.connectionType'
	set @calqry = @calqry + ' when 0 then ''Outbound'' '
	set @calqry = @calqry + ' else ''Inbound'' '
	set @calqry = @calqry + ' end AS Category,'
*/
	set @calqry = @calqry + ' case cu.connectionType'
	set @calqry = @calqry + ' when 0 then ''Dial-out from MCU'' '
	set @calqry = @calqry + ' when 1 then ''Dial-in to MCU'' '
	set @calqry = @calqry + ' when 3 then ''Direct to MCU'' '
	set @calqry = @calqry + ' end AS Category,'

	set @calqry = @calqry + ' case c.Status '
	set @calqry = @calqry + ' when 0 then ''Confirmed'' '
	set @calqry = @calqry + ' else ''Pending'''
	set @calqry = @calqry + ' end AS ''Status'', c.description  as Remarks'
	set @calqry = @calqry + ',c.conftype,cu.connectionType,cu.defVideoProtocol,'
	
	--set @calqry = @calqry + ' case cu.defVideoProtocol '
	
	set @calqry = @calqry + ' case cu.addresstype '
	set @calqry = @calqry + ' when 4 then ''ISDN'''
	set @calqry = @calqry + ' else ''IP'''
	set @calqry = @calqry + ' end AS vidProtocol, ''D'' as TypeFlag'
	set @calqry = @calqry + ' ,CustomAttributeId, SelectedOptionId, cu.addresstype'
/* Where Part */

	set @where = ' FROM '
	set @where = @where + ' conf_conference_d c,'
	set @where = @where + ' usr_list_d u, '
	set @where = @where + ' conf_room_d cu, '
	set @where = @where + ' loc_room_d l, '
	set @where = @where + ' loc_tier2_d l2,'
	set @where = @where + ' loc_tier3_d l3,'
	set @where = @where + ' conf_customattr_d ca'
	set @where = @where + ' WHERE (c.confid = cu.confid AND c.instanceid = cu.instanceid)'
	set @where = @where + ' AND (c.deleted = 0)'
	set @where = @where + ' AND c.owner = u.userid'
	set @where = @where + ' AND l.roomid = cu.roomid'
	set @where = @where + ' AND l2.id = l.l2locationid'
	set @where = @where + ' AND l3.id = l.l3locationid'
	set @where = @where + ' AND (c.confid = ca.confid AND c.instanceid = ca.instanceid)  and Customattributeid = 1'

	if(len(@filter) > 1)
		set @where = @where + @filter

	set @where = @where + ' )a'
	set @where = @where + ' left outer join '
	set @where = @where + ' (Select Caption AS [Entity Code], helpText as [Entity Name], '
	set @where = @where + ' OptionID, CustomAttributeId from Dept_CustomAttr_Option_D '
	set @where = @where + ' Where CustomAttributeId=1'
	Set @where = @where + ' And OptionType=6'
	Set @where = @where + ' And DeptId=0)D '
	--Set @where = @where + ' And OptionValue=0)D '
	set @where = @where + ' on a.CustomAttributeId = D.CustomAttributeId'
	set @where = @where + ' AND a.SelectedOptionId=D.OptionID'

	declare @cmdsql as varchar(8000)

	if(@codeType=0) -- All
		Begin
			set @seloptionid = ''
			Declare cursor1 Cursor for
				
				SELECT Distinct Caption AS EntityCode, OptionID as OptionID
				FROM Dept_CustomAttr_Option_D where Caption is not null

			Open cursor1

			Fetch Next from cursor1 into @ecode,@optionid

			While @@Fetch_Status = 0
			Begin
								
				set @cond = ''
				Set @cond = ' Where a.SelectedOptionId ='+ Cast(@optionid as varchar(10))
												
				Set @cmdsql = 'INSERT INTO #TempCalendarDT ' + @calqry + @where + @cond + ' ORDER BY dbo.changeTOGMTtime('+ cast(@timezone as varchar(2)) +',a.[Date]),[Meeting Title], Room'
				--print @cmdsql
				exec (@cmdsql)
				set @cond = ''
				set @cmdsql = ''

				Fetch Next from cursor1 into @ecode,@optionid
			End

			Close cursor1
			Deallocate cursor1

			/* Need to identify the entity codes */
			
			set @cond = ''
			Set @cond = ' Where a.SelectedOptionId ='''' OR a.SelectedOptionId = -1'
						
			Set @cmdsql = 'INSERT INTO #TempCalendarDT ' + @calqry + @where + @cond + ' ORDER BY dbo.changeTOGMTtime('+ cast(@timezone as varchar(2)) +',a.[Date]),[Meeting Title], Room'
			--print @cmdsql
			exec (@cmdsql)
			set @cond = ''
			set @cmdsql = ''

		End
	else 
		Begin
			
			set @cond = ''
			if @codetype = 1
				set @cond = ' Where [Entity code] is null or [Entity code] = ''-1'''
			else
				set @cond = ' Where a.SelectedOptionId ='+ cast(@seloptionid as varchar(10))
			
			Set @cmdsql = 'INSERT INTO #TempCalendarDT ' + @calqry + @where + @cond + ' ORDER BY dbo.changeTOGMTtime('+ cast(@timezone as varchar(2)) +',a.[Date]),[Meeting Title], Room'
			print @cmdsql
			exec (@cmdsql)
			set @cmdsql = ''			
			set @cond = ''
		End

/* ** Updates the duration as per requirement **

conftype = 11 phantom
conftype = 4  point-point
conftype = 7  room only

7. If a conference type is room only then the value posted in the duration field will always be equal to 0
*/
set @cmdsql = @cmdsql + 'Update #TempCalendarDT set Duration=0 where conftype in(7,11) and TypeFlag=''D'''
exec (@cmdsql)
set @cmdsql = ''

/*
8. Due to the method of connectivity by NGC, in a point-to-point defined conference there will be three rooms with the same meeting title, and the same date/time displayed, however,  one of the rooms is a phantom room and should be assigned a duration of 0.
   All point-to-point calls are to be billed as ISDN calls.

 PROGRAMMING NOTE: Any ISDN Room which doesn't belong to State = "ISDN Network" and City = "PacBell PRI's" should have duration as zero (0).
*/
set @cmdsql = @cmdsql + 'Update #TempCalendarDT set Duration=0 where (rtrim(State) <> ''ISDN Network'' AND rtrim(City) <> ''PacBell PRIs'' AND addresstype=4) and TypeFlag=''D'''
exec (@cmdsql)
set @cmdsql = ''

set @calqry = ''

if (@tablename <> '' AND @tablename is not null)
begin
	if exists (select * from dbo.sysobjects where [name]=@tablename and xtype='U')
	 begin
		set @cmdsql = ''
		set @cmdsql = 'drop table '+ @tablename
		exec(@cmdsql)
	 end
end
set @cmdsql = ''

/* Detail */
set @cmdsql = null

set @cmdsql = 'Select ltrim(rtrim(convert(char,[Date],101))) as [Date],ltrim(rtrim(Start)) as Start,ltrim(rtrim([End])) as [End],Duration,Room,[Meeting Title],'
set @cmdsql = @cmdsql + ' [Last Name],First,email,Phone,State,City,[Conf #],'
set @cmdsql = @cmdsql + ' [Entity Code],[Entity Name],[Meeting Type] as [Conference Type]'
set @cmdsql = @cmdsql + ' ,vidProtocol as Protocol,Category as [Connection Type],'
set @cmdsql = @cmdsql + ' Status,Remarks from #TempCalendarDT where TypeFlag=''D'';'

/* Consolidated Summary */

set @cmdsql = @cmdsql + ' Select isnull([Entity Code],''NA'') as Entity_Code, sum([Minutes]) as [Minutes],sum([Hours]) as [Hours],'
set @cmdsql = @cmdsql + ' sum([Connects]) as [Connects],'

set @cmdsql = @cmdsql + ' sum([VOIP Totals]) as [IP(Hours)], '
set @cmdsql = @cmdsql + ' case when( sum([Hours]) > 0 ) Then ((sum([VOIP Totals])/sum([Hours]))*100) '
set @cmdsql = @cmdsql + ' else 0 '
set @cmdsql = @cmdsql + ' End as [IP %],'

set @cmdsql = @cmdsql + ' sum([Inbound ISDN Totals]) as [ISDN Dial-in (Hours)],'

set @cmdsql = @cmdsql + ' sum([ISDN Outbound Totals]) as [ISDN Dial-out (Hours)],'
set @cmdsql = @cmdsql + ' case when( sum([Hours]) > 0 ) Then ((sum([ISDN Outbound Totals])/sum([Hours]))*100)'
set @cmdsql = @cmdsql + ' else 0 '
set @cmdsql = @cmdsql + ' End as [ISDN Dial-out %]'
--ISDN Outbound
set @cmdsql = @cmdsql + ' from '
set @cmdsql = @cmdsql + ' (Select isnull([Entity Code],''NA'') as [Entity Code],'
set @cmdsql = @cmdsql + ' isnull((sum(isnull(Duration,0))/60),0) as [ISDN Outbound Totals],'
set @cmdsql = @cmdsql + ' 0 as [Inbound ISDN Totals], 0 as [VOIP Totals],0 as Connects, 0 as Hours, 0 as Minutes'
set @cmdsql = @cmdsql + ' from #TempCalendarDT where (addresstype=4 AND connectionType=0) and  TypeFlag=''D'''
set @cmdsql = @cmdsql + ' group by [Entity Code] '

set @cmdsql = @cmdsql + ' Union '
set @cmdsql = @cmdsql + ' Select isnull([Entity Code],''NA'') as [Entity Code], '
set @cmdsql = @cmdsql + ' 0 as [ISDN Outbound Totals],'
set @cmdsql = @cmdsql + ' isnull((sum(isnull(Duration,0))/60),0) as [Inbound ISDN Totals], 0 as [VOIP Totals],0 as Connects, 0 as Hours, 0 as Minutes'
set @cmdsql = @cmdsql + ' from #TempCalendarDT where (addresstype=4 AND connectionType=1) and TypeFlag=''D'' group by [Entity Code]'

set @cmdsql = @cmdsql + ' Union'
set @cmdsql = @cmdsql + ' Select isnull([Entity Code],''NA'') as [Entity Code], '
set @cmdsql = @cmdsql + ' 0 as [ISDN Outbound Totals],'
set @cmdsql = @cmdsql + ' 0 as [Inbound ISDN Totals], isnull((sum(isnull(Duration,0))/60),0) as [VOIP Totals],0 as Connects, 0 as Hours, 0 as Minutes '
set @cmdsql = @cmdsql + ' from #TempCalendarDT where addresstype <> 4 and  TypeFlag=''D'' group by [Entity Code]'

set @cmdsql = @cmdsql + ' Union'
set @cmdsql = @cmdsql + ' Select isnull([Entity Code],''NA'') as [Entity Code], '
set @cmdsql = @cmdsql + ' 0 as [ISDN Outbound Totals],'
set @cmdsql = @cmdsql + ' 0 as [Inbound ISDN Totals], 0 as [VOIP Totals], count(*) as Connects, 0 as Hours, 0 as Minutes '
set @cmdsql = @cmdsql + ' from #TempCalendarDT  Where TypeFlag=''D'' group by [Entity Code]'

set @cmdsql = @cmdsql + ' Union'
set @cmdsql = @cmdsql + ' Select isnull([Entity Code],''NA'') as [Entity Code],'
set @cmdsql = @cmdsql + ' 0 as [ISDN Outbound Totals],'
set @cmdsql = @cmdsql + ' 0 as [Inbound ISDN Totals], 0 as [VOIP Totals],0 as Connects, isnull((sum(isnull(Duration,0))/60),0) as Hours, 0 as Minutes '
set @cmdsql = @cmdsql + ' from #TempCalendarDT Where TypeFlag=''D'' group by [Entity Code]'

set @cmdsql = @cmdsql + ' Union'
set @cmdsql = @cmdsql + ' Select isnull([Entity Code],''NA'') as [Entity Code],'
set @cmdsql = @cmdsql + ' 0 as [ISDN Outbound Totals],'
set @cmdsql = @cmdsql + ' 0 as [Inbound ISDN Totals], 0 as [VOIP Totals],0 as Connects, 0 as Hours, sum(isnull(Duration,0)) as [Minutes] '
set @cmdsql = @cmdsql + ' from #TempCalendarDT where TypeFlag=''D'' group by [Entity Code]) d group by [Entity Code];'

/* Entity codewise Summary */

set @cmdsql = @cmdsql + ' Select isnull([Entity Code],''NA'') as [Entity Code], sum([Minutes]) as [Minutes],sum([Hours]) as [Hours],'
set @cmdsql = @cmdsql + ' sum([Pt-Pt Meetings]) as [Point-to-Point Conferences],'
set @cmdsql = @cmdsql + ' sum([ISDN Outbound Totals]) as [ISDN Dial-out connections],'
set @cmdsql = @cmdsql + ' sum([Inbound ISDN Totals]) as [ISDN Dial-in connections],'
set @cmdsql = @cmdsql + ' sum([VOIP Totals]) as [IP Dial-out connections],'
set @cmdsql = @cmdsql + ' sum([Audio-Video Conferences]) as [Audio-Video Conferences],'
set @cmdsql = @cmdsql + ' sum([IP Dial-in connections]) as [IP Dial-in connections],'
set @cmdsql = @cmdsql + ' (sum([Pt-Pt Meetings])+ sum([Audio-Video Conferences])) as [Total Conferences]'
set @cmdsql = @cmdsql + ' from ('
--Pt-Pt Mins
set @cmdsql = @cmdsql + ' Select isnull([Entity Code],''NA'') as [Entity Code],'
set @cmdsql = @cmdsql + ' sum(isnull(Duration,0)) as [Pt-Pt Meetings],'
set @cmdsql = @cmdsql + ' 0 as [ISDN Outbound Totals],'
set @cmdsql = @cmdsql + ' 0 as [Inbound ISDN Totals], 0 as [VOIP Totals], 0 as Hours, 0 as Minutes'
set @cmdsql = @cmdsql + ' ,0 as [Audio-Video Conferences], 0 as [IP Dial-in connections]'
set @cmdsql = @cmdsql + ' from #TempCalendarDT where ConfType=4 and TypeFlag=''D'' group by [Entity Code]'

--ISDN Outbound Mins
set @cmdsql = @cmdsql + ' Union '
set @cmdsql = @cmdsql + ' Select isnull([Entity Code],''NA'') as [Entity Code], 0 as [Pt-Pt Meetings],'
set @cmdsql = @cmdsql + ' sum(isnull(Duration,0)) as [ISDN Outbound Totals],'
set @cmdsql = @cmdsql + ' 0 as [Inbound ISDN Totals], 0 as [VOIP Totals], 0 as Hours, 0 as Minutes'
set @cmdsql = @cmdsql + ' ,0 as [Audio-Video Conferences], 0 as [IP Dial-in connections]'
set @cmdsql = @cmdsql + ' from #TempCalendarDT where (addresstype=4 AND connectionType=0) and  TypeFlag=''D'' '
set @cmdsql = @cmdsql + ' group by [Entity Code] '

--ISDN Inbound Mins
set @cmdsql = @cmdsql + ' Union '
set @cmdsql = @cmdsql + ' Select isnull([Entity Code],''NA'') as [Entity Code], 0 as [Pt-Pt Meetings],'
set @cmdsql = @cmdsql + ' 0 as [ISDN Outbound Totals],'
set @cmdsql = @cmdsql + ' sum(isnull(Duration,0)) as [Inbound ISDN Totals], 0 as [VOIP Totals],0 as Hours, 0 as Minutes'
set @cmdsql = @cmdsql + ' ,0 as [Audio-Video Conferences], 0 as [IP Dial-in connections]'
set @cmdsql = @cmdsql + ' from #TempCalendarDT where (addresstype=4 AND connectionType=1) and  TypeFlag=''D'' group by [Entity Code]'

--IP Dial-Out Mins
set @cmdsql = @cmdsql + ' Union'
set @cmdsql = @cmdsql + ' Select isnull([Entity Code],''NA'') as [Entity Code], 0 as [Pt-Pt Meetings],'
set @cmdsql = @cmdsql + ' 0 as [ISDN Outbound Totals],'
set @cmdsql = @cmdsql + ' 0 as [Inbound ISDN Totals], sum(isnull(Duration,0)) as [VOIP Totals],0 as Hours, 0 as Minutes '
set @cmdsql = @cmdsql + ' ,0 as [Audio-Video Conferences], 0 as [IP Dial-in connections]'
set @cmdsql = @cmdsql + ' from #TempCalendarDT where (addresstype<>4 AND connectionType=0) and  TypeFlag=''D'' group by [Entity Code]'

--Hours
set @cmdsql = @cmdsql + ' Union'
set @cmdsql = @cmdsql + ' Select isnull([Entity Code],''NA'') as [Entity Code], 0 as [Pt-Pt Meetings],'
set @cmdsql = @cmdsql + ' 0 as [ISDN Outbound Totals],'
set @cmdsql = @cmdsql + ' 0 as [Inbound ISDN Totals], 0 as [VOIP Totals],isnull((sum(isnull(Duration,0))/60),0) as Hours, 0 as Minutes '
set @cmdsql = @cmdsql + ' ,0 as [Audio-Video Conferences], 0 as [IP Dial-in connections]'
set @cmdsql = @cmdsql + ' from #TempCalendarDT Where TypeFlag=''D'' group by [Entity Code]'

--Minutes
set @cmdsql = @cmdsql + ' Union'
set @cmdsql = @cmdsql + ' Select isnull([Entity Code],''NA'') as [Entity Code], 0 as [Pt-Pt Meetings],'
set @cmdsql = @cmdsql + ' 0 as [ISDN Outbound Totals],'
set @cmdsql = @cmdsql + ' 0 as [Inbound ISDN Totals], 0 as [VOIP Totals], 0 as Hours, sum(isnull(Duration,0)) as [Minutes] '
set @cmdsql = @cmdsql + ' ,0 as [Audio-Video Conferences], 0 as [IP Dial-in connections]'
set @cmdsql = @cmdsql + ' from #TempCalendarDT where TypeFlag=''D'' group by [Entity Code]'
--Audio-Video Mins
set @cmdsql = @cmdsql + ' Union'
set @cmdsql = @cmdsql + ' Select isnull([Entity Code],''NA'') as [Entity Code], 0 as [Pt-Pt Meetings],'
set @cmdsql = @cmdsql + ' 0 as [ISDN Outbound Totals],'
set @cmdsql = @cmdsql + ' 0 as [Inbound ISDN Totals], 0 as [VOIP Totals], 0 as Hours, 0 as Minutes'
set @cmdsql = @cmdsql + ' ,sum(isnull(Duration,0)) as [Audio-Video Conferences], 0 as [IP Dial-in connections]'
set @cmdsql = @cmdsql + ' from #TempCalendarDT where ConfType=2 and TypeFlag=''D'' group by [Entity Code]'

--IP Dial-in Mins
set @cmdsql = @cmdsql + ' Union'
set @cmdsql = @cmdsql + ' Select isnull([Entity Code],''NA'') as [Entity Code], 0 as [Pt-Pt Meetings],'
set @cmdsql = @cmdsql + ' 0 as [ISDN Outbound Totals],'
set @cmdsql = @cmdsql + ' 0 as [Inbound ISDN Totals], 0 as [VOIP Totals], 0 as Hours, 0 as Minutes'
set @cmdsql = @cmdsql + ' ,0 as [Audio-Video Conferences], sum(isnull(Duration,0)) as [IP Dial-in connections]'
set @cmdsql = @cmdsql + ' from #TempCalendarDT where (addresstype <> 4 AND connectionType=1) and TypeFlag=''D'' group by [Entity Code]'

set @cmdsql = @cmdsql + ') d group by [Entity Code];'

--ISDN Network - seperate tab
--All point-to-point calls are to be billed as ISDN calls.
-- Conftype = 4 (Point-Point)
set @cmdsql = @cmdsql + ' Select ltrim(rtrim(convert(char,[Date],101))) as [Date],ltrim(rtrim(Start)) as Start,ltrim(rtrim([End])) as [End],Duration,Room,[Meeting Title],'
set @cmdsql = @cmdsql + ' [Last Name],First,email,Phone,State,City,[Conf #],'
set @cmdsql = @cmdsql + ' [Entity Code],[Entity Name],[Meeting Type] as [Conference Type]'
set @cmdsql = @cmdsql + ' ,vidProtocol as Protocol,Category as [Connection Type],'
set @cmdsql = @cmdsql + ' Status,Remarks from #TempCalendarDT where ((addresstype=4 and TypeFlag=''D'''
set @cmdsql = @cmdsql + ' and State=''ISDN Network'' and City=''PacBell PRIs'') OR (Conftype=4));'


/* ISDN Summary */
declare @condisdn as varchar (1000)
declare @condisdn1 as varchar (1000)
declare @cmdsql2 as varchar(5000)

set @condisdn1 =  'and ((addresstype=4 and State=''ISDN Network'' and City=''PacBell PRIs''))'

set @cmdsql2 = ' Select ''ISDN'' as [Entity Code], sum([Minutes]) as [Minutes],sum([Hours]) as [Hours],'
set @cmdsql2 = @cmdsql2 + ' sum([Pt-Pt Meetings]) as [Point-to-Point Conferences],'
set @cmdsql2 = @cmdsql2 + ' sum([ISDN Outbound Totals]) as [ISDN Dial-out connections],'
set @cmdsql2 = @cmdsql2 + ' sum([Inbound ISDN Totals]) as [ISDN Dial-in connections],'
set @cmdsql2 = @cmdsql2 + ' sum([VOIP Totals]) as [IP Dial-out connections],'
set @cmdsql2 = @cmdsql2 + ' sum([Audio-Video Conferences]) as [Audio-Video Conferences],'
set @cmdsql2 = @cmdsql2 + ' sum([IP Dial-in connections]) as [IP Dial-in connections],'
set @cmdsql2 = @cmdsql2 + ' (sum([Pt-Pt Meetings])+ sum([Audio-Video Conferences])) as [Total Conferences]'

set @cmdsql2 = @cmdsql2 + ' from ('
--Pt-Pt Mins
set @cmdsql2 = @cmdsql2 + ' Select sum(isnull(Duration,0)) as [Pt-Pt Meetings],'
set @cmdsql2 = @cmdsql2 + ' 0 as [ISDN Outbound Totals],'
set @cmdsql2 = @cmdsql2 + ' 0 as [Inbound ISDN Totals], 0 as [VOIP Totals], 0 as Hours, 0 as Minutes'
set @cmdsql2 = @cmdsql2 + ' ,0 as [Audio-Video Conferences], 0 as [IP Dial-in connections]'
set @cmdsql2 = @cmdsql2 + ' from #TempCalendarDT where ConfType=4 and TypeFlag=''D'''
set @cmdsql2 = @cmdsql2 + @condisdn1

--ISDN Outbound Mins
set @cmdsql2 = @cmdsql2 + ' Union '
set @cmdsql2 = @cmdsql2 + ' Select 0 as [Pt-Pt Meetings],'
set @cmdsql2 = @cmdsql2 + ' sum(isnull(Duration,0)) as [ISDN Outbound Totals],'
set @cmdsql2 = @cmdsql2 + ' 0 as [Inbound ISDN Totals], 0 as [VOIP Totals], 0 as Hours, 0 as Minutes'
set @cmdsql2 = @cmdsql2 + ' ,0 as [Audio-Video Conferences], 0 as [IP Dial-in connections]'
set @cmdsql2 = @cmdsql2 + ' from #TempCalendarDT where connectionType=0 and  TypeFlag=''D'''
set @cmdsql2 = @cmdsql2 + @condisdn1

--ISDN Inbound Mins
set @cmdsql2 = @cmdsql2 + ' Union '
set @cmdsql2 = @cmdsql2 + ' Select 0 as [Pt-Pt Meetings],'
set @cmdsql2 = @cmdsql2 + ' 0 as [ISDN Outbound Totals],'
set @cmdsql2 = @cmdsql2 + ' sum(isnull(Duration,0)) as [Inbound ISDN Totals], 0 as [VOIP Totals],0 as Hours, 0 as Minutes'
set @cmdsql2 = @cmdsql2 + ' ,0 as [Audio-Video Conferences], 0 as [IP Dial-in connections]'
set @cmdsql2 = @cmdsql2 + ' from #TempCalendarDT where connectionType=1 and TypeFlag=''D'''
set @cmdsql2 = @cmdsql2 + @condisdn1

--Hours
set @cmdsql2 = @cmdsql2 + ' Union'
set @cmdsql2 = @cmdsql2 + ' Select 0 as [Pt-Pt Meetings],'
set @cmdsql2 = @cmdsql2 + ' 0 as [ISDN Outbound Totals],'
set @cmdsql2 = @cmdsql2 + ' 0 as [Inbound ISDN Totals], 0 as [VOIP Totals],isnull((sum(isnull(Duration,0))/60),0) as Hours, 0 as Minutes '
set @cmdsql2 = @cmdsql2 + ' ,0 as [Audio-Video Conferences], 0 as [IP Dial-in connections]'
set @cmdsql2 = @cmdsql2 + ' from #TempCalendarDT Where TypeFlag=''D'' '
set @cmdsql2 = @cmdsql2 + @condisdn1

--Minutes
set @cmdsql2 = @cmdsql2 + ' Union'
set @cmdsql2 = @cmdsql2 + ' Select 0 as [Pt-Pt Meetings],'
set @cmdsql2 = @cmdsql2 + ' 0 as [ISDN Outbound Totals],'
set @cmdsql2 = @cmdsql2 + ' 0 as [Inbound ISDN Totals], 0 as [VOIP Totals], 0 as Hours, sum(isnull(Duration,0)) as [Minutes] '
set @cmdsql2 = @cmdsql2 + ' ,0 as [Audio-Video Conferences], 0 as [IP Dial-in connections]'
set @cmdsql2 = @cmdsql2 + ' from #TempCalendarDT where TypeFlag=''D'' '
set @cmdsql2 = @cmdsql2 + @condisdn1
--Audio-Video Mins
set @cmdsql2 = @cmdsql2 + ' Union'
set @cmdsql2 = @cmdsql2 + ' Select 0 as [Pt-Pt Meetings],'
set @cmdsql2 = @cmdsql2 + ' 0 as [ISDN Outbound Totals],'
set @cmdsql2 = @cmdsql2 + ' 0 as [Inbound ISDN Totals], 0 as [VOIP Totals], 0 as Hours, 0 as Minutes'
set @cmdsql2 = @cmdsql2 + ' ,sum(isnull(Duration,0)) as [Audio-Video Conferences], 0 as [IP Dial-in connections]'
set @cmdsql2 = @cmdsql2 + ' from #TempCalendarDT where ConfType=2 and TypeFlag=''D'' '
set @cmdsql2 = @cmdsql2 + @condisdn1

--IP Dial-in Mins
set @cmdsql2 = @cmdsql2 + ')d;'

exec(@cmdsql + @cmdsql2)

--print (@cmdsql + @cmdsql2)

END



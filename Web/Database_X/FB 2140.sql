/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE Icons_Ref_S
	(
	IconID int NOT NULL IDENTITY (1, 1),
	IconUri nvarchar(MAX) NULL,
	Label nvarchar(300) NULL,
	IconTarget nvarchar(MAX) NULL
	) 
GO
ALTER TABLE Icons_Ref_S ADD CONSTRAINT
	PK_Icons_Ref_S PRIMARY KEY CLUSTERED 
	(
	IconID
	) 
COMMIT


/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE User_Lobby_D
	(
	UId int NOT NULL IDENTITY (1, 1),
	UserID int NOT NULL default 11,
	IconID int NOT NULL default 0,
	GridPosition int NOT NULL default 0,
	Label nvarchar(300) NULL
	)
COMMIT


insert into Icons_Ref_S values ('../en/img/calendar.jpg', 'Calendar', '../en/personalCalendar.aspx?v=1&r=1&hf=&m=&pub=&d=&comp=&f=v')
insert into Icons_Ref_S values ('../en/img/conference_new.jpg', 'New Conference', '../en/ConferenceSetup.aspx?t=n&op=1')
insert into Icons_Ref_S values ('../en/img/preference.jpg', 'My Preferences', '../en/ManageUserProfile.aspx')
insert into Icons_Ref_S values ('../en/img/report.jpg', 'Reports', '../en/GraphicalReport.aspx')
insert into Icons_Ref_S values ('../en/img/template.jpg', 'Manage Template', '../en/ManageTemplate.aspx')
insert into Icons_Ref_S values ('../en/img/group.jpg', 'Manage Groups', '../en/ManageGroup.aspx')
insert into Icons_Ref_S values ('../en/img/codec.jpg', 'Manage My Endpoints', '../en/EndpointList.aspx?t=')
insert into Icons_Ref_S values ('../en/img/diagnostic.jpg', 'My System Diagnostic', '../en/EventLog.aspx')
insert into Icons_Ref_S values ('../en/img/MCU.jpg', 'Manage My MCUs', '../en/ManageBridge.aspx')
insert into Icons_Ref_S values ('../en/img/rooms.jpg', 'Manage Rooms', '../en/manageroom.aspx')
insert into Icons_Ref_S values ('../en/img/location.jpg', 'Manage My Location Tree', '../en/ManageTiers.aspx')
insert into Icons_Ref_S values ('../en/img/activeUsers.jpg', 'Active Users', '../en/ManageUser.aspx?t=1')
insert into Icons_Ref_S values ('../en/img/bulkTool.jpg', 'Bulk Tool', '../en/allocation.aspx')
insert into Icons_Ref_S values ('../en/img/departments.jpg', 'Departments', '../en/ManageDepartment.aspx')
insert into Icons_Ref_S values ('../en/img/guests.jpg', 'Guests', '../en/ManageUser.aspx?t=2')
insert into Icons_Ref_S values ('../en/img/inactiveUsers.jpg', 'Inactive Users', '../en/ManageUser.aspx?t=3')
insert into Icons_Ref_S values ('../en/img/LDAP.jpg', 'LDAP Directory Import', '../en/LDAPImport.aspx')
insert into Icons_Ref_S values ('../en/img/userRoles.jpg', 'Roles', '../en/manageuserroles.aspx')
insert into Icons_Ref_S values ('../en/img/hasAdminTemplates.jpg', 'Templates', '../en/ManageUserTemplatesList.aspx')
insert into Icons_Ref_S values ('../en/img/options.jpg', 'My Organization Options', '../en/mainadministrator.aspx')
insert into Icons_Ref_S values ('../en/img/settings.jpg', 'My Organization Settings', '../en/OrganisationSettings.aspx')
insert into Icons_Ref_S values ('../en/img/inventory.jpg', 'Manage Inventory', '../en/InventoryManagement.aspx?t=1')
insert into Icons_Ref_S values ('../en/img/workorder2.jpg', 'Manage My Inventory Work Order', '../en/ConferenceOrders.aspx?t=1')
insert into Icons_Ref_S values ('../en/img/catering2.jpg', 'Manage Catering', '../en/InventoryManagement.aspx?t=2')
insert into Icons_Ref_S values ('../en/img/catering_order.jpg', 'Manage My Catering Work Order', '../en/ConferenceOrders.aspx?t=2')
insert into Icons_Ref_S values ('../en/img/house_keeping.jpg', 'Manage House Keeping', '../en/InventoryManagement.aspx?t=3')
insert into Icons_Ref_S values ('../en/img/workorder.jpg', 'Manage My House Keeping Work Order', '../en/ConferenceOrders.aspx?t=3')
insert into Icons_Ref_S values ('../en/img/system_setting.jpg', 'My Overall Site Settings', '../en/SuperAdministrator.aspx')
insert into Icons_Ref_S values ('../en/img/contact.jpg', 'Feedback', 'javascript: openFeedback()')
insert into Icons_Ref_S values ('../en/img/help.jpg', 'Help', 'javascript: openhelp()')
insert into Icons_Ref_S values ('../en/img/approval.jpg', 'Approval Conference', '../en/ConferenceList.aspx?t=6')
insert into Icons_Ref_S values ('../en/img/email_mgt.jpg', 'Managing Email Queue', '../en/ViewBlockedMails.aspx?tp=u')
insert into Icons_Ref_S values ('../en/img/lookandfeel.jpg', 'My Organization UI', '../en/UISettings.aspx')
insert into Icons_Ref_S values ('../en/img/audit.jpg', 'System Audit', '../en/SystemAudit.aspx')
insert into Icons_Ref_S values ('../en/img/dashboard.jpg', 'Dashboard', '../en/SettingSelect2.aspx')
insert into Icons_Ref_S values ('../en/img/summary.jpg', 'Search', '../en/SearchConferenceInputParameters.aspx')
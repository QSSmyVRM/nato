/* Gen_Language_S - Folder added */

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Gen_Language_S ADD
	languagefolder nvarchar(100) NULL
GO
COMMIT



update Gen_Language_S set languagefolder = 'en' where LanguageID = 1

update Gen_Language_S set languagefolder = 'en-uk' where LanguageID = 2

update Gen_Language_S set languagefolder = 'sp' where LanguageID = 3

update Gen_Language_S set languagefolder = 'ch' where LanguageID = 4

update Gen_Language_S set languagefolder = 'fr' where LanguageID = 5

---------------------------------------------------------------------------

/* Conference Edit Mode */
/* 1 - New 2-Edit */

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_Conference_D ADD
	confMode smallint NULL
GO
ALTER TABLE dbo.Conf_Conference_D ADD CONSTRAINT
	DF_Conf_Conference_D_confMode DEFAULT 1 FOR confMode
GO
COMMIT

/* Email Language in User Profile */

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Usr_List_D ADD
	EmailLangId int NULL
GO
COMMIT

update Usr_List_D set EmailLangId  = [Language]

/* Email Language in In-Active User */

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Usr_Inactive_D ADD
	EmailLangId int NULL
GO
COMMIT

update Usr_Inactive_D set EmailLangId  = [Language]

/* Email Language in Org_Settings_D */

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	EmailLangId int NULL
GO
ALTER TABLE dbo.Org_Settings_D ADD CONSTRAINT
	DF_Org_Settings_D_EmailLangId DEFAULT 1 FOR EmailLangId
GO
COMMIT

update Org_Settings_D set EmailLangId = Language

/* Email_Type_S */

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Email_Type_S ADD
	emailcategory smallint NULL
GO
ALTER TABLE dbo.Email_Type_S ADD CONSTRAINT
	DF_Email_Type_S_emailcategory DEFAULT 0 FOR emailcategory
GO
COMMIT


/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE [dbo].[Email_Content_D](
	[EmailContentId] [int] IDENTITY(1,1) NOT NULL,
	[EmailLangId] [smallint] NULL CONSTRAINT [DF_Email_Content_D_EmailLangId]  DEFAULT ((1)),
	[EmailSubject] [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[EmailBody] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Placeholders] [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[EmailMode] [smallint] NULL,
	[Emailtypeid] [int] NULL,
	[EmailLanguage] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_Email_Content_D_EmailLanguage]  DEFAULT (N'en')
) ON [PRIMARY]
GO
COMMIT

update Email_Content_D set EmailLanguage='en' where EmailLangId = 1


/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Email_PlaceHolder_S
	(
	PlaceHolderId smallint NULL,
	Description nvarchar(100) NULL
	)  ON [PRIMARY]
GO
COMMIT


--Update queries for FB 1830 - Email Language:
----------------------------------------------
update Org_Settings_D set Language = 1
update Org_Settings_D set EmailLangId = Language
update Usr_List_D set EmailLangId = Language


/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE [dbo].[Email_Language_D](
	[EmailLangId] [int] IDENTITY(101,1) NOT NULL,
	[EmailLanguage] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_Email_Language_D_EmailLanguage]  DEFAULT (N'en'),
	[LanguageId] [smallint] NULL CONSTRAINT [DF_Email_Language_D_LanguageId]  DEFAULT ((1)),
	[orgid] [int] NULL CONSTRAINT [DF_Email_Language_D_orgid]  DEFAULT ((11))
) ON [PRIMARY]
GO
COMMIT

Update Email_Language_D set orgid = 11

-------------------------------------------------------------

/* FB 1830 Email Edit */

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_User_D ADD
	NotifyOnEdit smallint NULL
GO
ALTER TABLE dbo.Conf_User_D ADD CONSTRAINT
	DF_Conf_User_D_NotifyOnEdit DEFAULT 1 FOR NotifyOnEdit
GO

COMMIT



update Conf_User_D  set NotifyOnEdit=1

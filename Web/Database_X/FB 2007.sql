/*
   30 March 201106:07:10 AM
   User: 
   Server: staginguk
   Database: myvrm
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Sys_Settings_D ADD
	LaunchBuffer int NULL
GO
ALTER TABLE dbo.Sys_Settings_D ADD CONSTRAINT
	DF_Sys_Settings_D_LaunchBuffer DEFAULT 5 FOR LaunchBuffer
GO
COMMIT

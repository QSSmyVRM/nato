/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Usr_Dept_D ADD
	ID int NOT NULL IDENTITY (1, 1)
COMMIT


/* QUERY FOR DELETE TEMPLATE COMMAND START*/
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Tmp_Room_D ADD
	ID int NOT NULL IDENTITY (1, 1)
COMMIT


/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Tmp_Participant_D ADD
	ID int NOT NULL IDENTITY (1, 1)
COMMIT
/* QUERY FOR DELETE TEMPLATE COMMAND END*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Acc_Balance_D ADD
	ID int NOT NULL IDENTITY (1, 1)
COMMIT


/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Acc_GroupList_D ADD
	ID int NOT NULL IDENTITY (1, 1)
COMMIT


/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Grp_Participant_D ADD
	ID int NOT NULL IDENTITY (1, 1)
GO
COMMIT


/* QUERY FOR  CounterInvite COMMAND START*/
INSERT INTO err_list_s  (ID,Message,Description,Level,Languageid,ErrorType)
VALUES (532,'User ID or Conference ID is invalid!',
'User ID or Conference ID is invalid!','S',1,'U');
/* QUERY FOR  CounterInvite COMMAND END*/


--FB 2027 - SetSuperAdmin start

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Ldap_ServerConfig_D ADD
	UID int NOT NULL CONSTRAINT DF_Ldap_ServerConfig_D_UID DEFAULT 1
GO
COMMIT

--FB 2027 - SetSuperAdmin end

/* QUERY FOR  SetBulkUserDepartment COMMAND START*/

INSERT INTO err_list_s  (ID,Message,Description,Level,Languageid,ErrorType)
VALUES (533,'No Department Found !',
'No Department Found !','S',1,'U');

/* QUERY FOR  SetBulkUserDepartment COMMAND END*/


/* QUERY FOR  SetBulkUserBridge COMMAND START*/

INSERT INTO err_list_s  (ID,Message,Description,Level,Languageid,ErrorType)
VALUES (531,'Bridge ID is Empty!',
'Bridge ID is Empty!','E',1,'U');

/* QUERY FOR  SetBulkUserBridge COMMAND END*/



/* QUERY FOR  SetTerminalControl COMMAND start*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_Monitor_D ADD
	Uid int NOT NULL IDENTITY (1, 1)
COMMIT



--Email_PlaceHolder
Insert into [Email_PlaceHolder_S] (PlaceHolderID, description) values (59,'IsdnThresholdCost')
Insert into [Email_PlaceHolder_S] (PlaceHolderID, description) values (60,'CurrentIsdnCost')

--Email_Type
Insert into [Email_Type_S] (emailtype, emailsubject, emailid, emailcategory) values ('MCU ISDN Threshold alert','',29,1)

--Email_Content
Insert into Email_Content_D (emailLangId,  emailSubject, emailBody, placeholders,	emailMode, emailtypeid, emailLanguage) values (1,'MCU ISDN Threshold alert!','<p>Hello,<br />The following MCU has exceeded the assigned threshold <br />limits for ISDN calls. <br />MCU Name : <b>{3}</b><br />ISDN Threshold cost : <b> $ {59}</b><br />Current ISDN cost : <b> $ {60}</b><br />Triggering Conference unique ID : <b>{5}</b><br />Please take appropriate action. You will continue to receive this alert <br />unless the threshold for this MCU is raised or threshold alerts are disabled <br />Thanks, <br /> VRM Admin<br /></p>','3,5,59,60',0,29,'en')

/* QUERY FOR  SetTerminalControl COMMAND END*/



/* QUERY FOR  ResponseInvite COMMAND START*/
INSERT INTO err_list_s  (ID,Message,Description,Level,Languageid,ErrorType)
VALUES (534,'You are not a particpant in this conference or the selected conference has passed or has been deleted.',
'You are not a particpant in this conference or the selected conference has passed or has been deleted.','S',1,'U');
/* QUERY FOR  ResponseInvite COMMAND END*/


/* QUERY FOR  GetTemplateList COMMAND START*/

Update Err_List_S set Message = 'User is linked with a Group/Template/Department. Cannot delete!', Description = 'User is linked with a Group/Template/Department. Cannot delete!' Where ID = 521

/* QUERY FOR  GetTemplateList COMMAND END*/


/* QUERY FOR  GetLogPreferences COMMAND start*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Err_LogPrefs_S ADD
	UID int NOT NULL IDENTITY (1, 1)
GO
COMMIT

/* QUERY FOR  GetLogPreferences COMMAND end*/

/* *** FB 2027 - SetConference start *** */

Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) values (536,'Please enter conference duration with minimum of 15 minutes.','Please enter conference duration with minimum of 15 minutes.','E',1,'U')

Insert into Err_List_S (ID,  Message, Description, Level, Languageid, Errortype) values (537,'Instance conflict in selected custom dates.','Instance conflict in selected custom dates.','E',1,'U')


/* *** FB 2027 - SetConference end *** */


/* *** FB 2027 - DeleteRoom Start *** */

INSERT INTO err_list_s  (ID,Message,Description,Level,Languageid,ErrorType)
VALUES (536,'Room in use ( cannot be deleted as its being used for conference or is set for future conference )!',
'Room in use ( cannot be deleted as its being used for conference or is set for future conference )!','E',1,'U');

INSERT INTO err_list_s  (ID,Message,Description,Level,Languageid,ErrorType)
VALUES (537,'Room in use ( cannot be deleted as its being used for conference template )!',
'Room in use ( cannot be deleted as its being used for conference template )!','E',1,'U');

INSERT INTO err_list_s  (ID,Message,Description,Level,Languageid,ErrorType)
VALUES (538,'Room in use ( cannot be deleted as its being used as a preferred room by a user )!',
'Room in use ( cannot be deleted as its being used as a preferred room by a user )!','E',1,'U');

INSERT INTO err_list_s  (ID,Message,Description,Level,Languageid,ErrorType)
VALUES (539,'Room in use ( cannot be deleted as its being used as preferred room in user template )!',
'Room in use ( cannot be deleted as its being used as preferred room in user template )!','E',1,'U');

INSERT INTO err_list_s  (ID,Message,Description,Level,Languageid,ErrorType)
VALUES (540,'Room in use ( cannot be deleted as its being used in the Workorder )!',
'Room in use ( cannot be deleted as its being used in the Workorder )!','E',1,'U');

INSERT INTO err_list_s  (ID,Message,Description,Level,Languageid,ErrorType)
VALUES (541,'Room in use ( cannot be deleted as its being used in search template )!',
'Room in use ( cannot be deleted as its being used in search template )!','E',1,'U');


/* *** FB 2027 - DeleteRoom end *** */
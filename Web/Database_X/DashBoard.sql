BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_Cascade_D ADD
	OnlineStatus int NULL,
	LastRunDateTime datetime NULL
GO
ALTER TABLE dbo.Conf_Cascade_D ADD CONSTRAINT
	DF_Conf_Cascade_D_OnlineStatus DEFAULT 0 FOR OnlineStatus
GO
ALTER TABLE dbo.Conf_Cascade_D ADD CONSTRAINT
	DF_Conf_Cascade_D_LastRunDateTime DEFAULT getutcdate() FOR LastRunDateTime
GO
ALTER TABLE dbo.Conf_Room_D ADD
	OnlineStatus int NULL,
	LastRunDateTime datetime NULL
GO
ALTER TABLE dbo.Conf_Room_D ADD CONSTRAINT
	DF_Conf_Room_D_OnlineStatus DEFAULT 0 FOR OnlineStatus
GO
ALTER TABLE dbo.Conf_Room_D ADD CONSTRAINT
	DF_Conf_Room_D_LastRunDateTime DEFAULT getutcdate() FOR LastRunDateTime
GO
ALTER TABLE dbo.Conf_User_D ADD
	OnlineStatus int NULL,
	LastRunDateTime datetime NULL
GO
ALTER TABLE dbo.Conf_User_D ADD CONSTRAINT
	DF_Conf_User_D_OnlineStatus DEFAULT 0 FOR OnlineStatus
GO
ALTER TABLE dbo.Conf_User_D ADD CONSTRAINT
	DF_Conf_User_D_LastRunDateTime DEFAULT getutcdate() FOR LastRunDateTime
GO
ALTER TABLE dbo.Conf_Conference_D ADD
	LastRunDateTime datetime NULL
GO
ALTER TABLE dbo.Conf_Conference_D ADD CONSTRAINT
	DF_Conf_Conference_D_LastRunDateTime DEFAULT getutcdate() FOR LastRunDateTime
GO
ALTER TABLE dbo.Sys_MailServer_D ADD
	LastRunDateTime datetime NULL
GO
ALTER TABLE dbo.Sys_MailServer_D ADD CONSTRAINT
	DF_Sys_MailServer_D_LastRunDateTime DEFAULT getutcdate() FOR LastRunDateTime
GO
COMMIT


update Conf_Cascade_D set LastRunDateTime = getdate() where LastRunDateTime is null

update Conf_Room_D set LastRunDateTime = getdate() where LastRunDateTime is null

update Conf_User_D set LastRunDateTime = getdate() where LastRunDateTime is null

update Conf_Conference_D set LastRunDateTime = getdate() where LastRunDateTime is null

update Sys_MailServer_D set LastRunDateTime = getdate() where LastRunDateTime is null

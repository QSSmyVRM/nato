/*
   Tuesday, March 29, 20114:44:30 AM
   User: myvrm
   Server: VENI\SQLEXPRESS
   Database: cqa1
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Mcu_List_D ADD
	ISDNAudioPrefix varchar(5) NULL,
	ISDNVideoPrefix varchar(5) NULL
GO
ALTER TABLE dbo.Mcu_List_D ADD CONSTRAINT
	DF_Mcu_List_D_ISDNAudioPrefix DEFAULT '' FOR ISDNAudioPrefix
GO
ALTER TABLE dbo.Mcu_List_D ADD CONSTRAINT
	DF_Mcu_List_D_ISDNVideoPrefix DEFAULT '' FOR ISDNVideoPrefix
GO
COMMIT

--FB 2181

/****** Object:  Table [dbo].[Conf_RecurInfo_D]    Script Date: 09/25/2011 19:15:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Conf_RecurInfoDefunt_D](
	[confid] [int] NOT NULL,
	[uId] [int] IDENTITY(1,1) NOT NULL,
	[confuId] [int] NOT NULL CONSTRAINT [DF_Conf_RecurInfoDefunt_D_confuId]  DEFAULT ((0)),
	[timezoneid] [int] NULL,
	[duration] [int] NULL,
	[recurType] [int] NULL,
	[subType] [int] NULL,
	[yearMonth] [int] NULL,
	[days] [nvarchar](256) NULL,
	[dayNo] [int] NULL,
	[gap] [int] NULL,
	[startTime] [datetime] NULL,
	[endTime] [datetime] NULL,
	[endType] [int] NULL,
	[occurrence] [int] NULL,
	[dirty] [smallint] NULL,
	[RecurringPattern] [nvarchar](4000) NULL
) ON [PRIMARY]

/*
   Monday, October 10, 201112:33:30 PM
   User: myvrm
   Server: gowniyanvm
   Database: Demo4
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_RecurInfoDefunt_D ADD
	SetupDuration int NULL,
	TeardownDuration int NULL
GO
COMMIT



/*
   Monday, March 14, 20119:42:38 AM
   User: myvrm
   Server: cqa1
   Database: myvrm
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Mcu_List_D ADD
	ISDNGateway varchar(50) NULL
GO
ALTER TABLE dbo.Mcu_List_D ADD CONSTRAINT
	DF_Mcu_List_D_ISDNGateway DEFAULT '' FOR ISDNGateway
GO
COMMIT

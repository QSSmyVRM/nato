-- FB 2158

INSERT INTO [dbo].[Email_Content_D]
           ([EmailLangId]
           ,[EmailSubject]
           ,[EmailBody]
           ,[Placeholders]
           ,[EmailMode]
           ,[Emailtypeid]
           ,[EmailLanguage])
     VALUES
           ( 1
           ,'Welcome to myVRM'
           ,'<p>{1}<br />Hello {2},<br /><br />Your myVRM account has been created.<br /><br /><b>Login Credential:</b><br />Login URL : {36}<br />Email ID : {41}<br />Password : {42}<br /></p>'
           ,'1,2,36,41,42'
           , 0
           ,30
           ,'en')

INSERT INTO [dbo].[Email_Content_D]
           ([EmailLangId]
           ,[EmailSubject]
           ,[EmailBody]
           ,[Placeholders]
           ,[EmailMode]
           ,[Emailtypeid]
           ,[EmailLanguage])
     VALUES
           ( 5
           ,'Welcome to myVRM'
           ,'<p>{1}<br />Hello {2},<br /><br />Your myVRM account has been created.<br /><br /><b>Login Credential:</b><br />Login URL : {36}<br />Email ID : {41}<br />Password : {42}<br /></p>'
           ,'1,2,36,41,42'
           , 0
           ,30
           ,'fr')

---------------------------------
--FB 2170

/*
   Monday, July 18, 20112:50:14 PM
   User: myvrm
   Server: sql2k5
   Database: Demo4
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	TelepresenceFilter smallint NULL
GO
ALTER TABLE dbo.Org_Settings_D ADD CONSTRAINT
	DF_Org_Settings_D_TelepresenceFilter DEFAULT 0 FOR TelepresenceFilter
GO
COMMIT

/*
   Monday, July 18, 20112:52:39 PM
   User: myvrm
   Server: sql2k5
   Database: Demo4
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Loc_Room_D ADD
	isTelepresence smallint NULL
GO
ALTER TABLE dbo.Loc_Room_D ADD CONSTRAINT
	DF_Loc_Room_D_isTelepresence DEFAULT 0 FOR isTelepresence
GO
COMMIT

update Org_Settings_D set TelepresenceFilter = isnull(TelepresenceFilter,0)

update loc_room_d set isTelepresence = isnull(isTelepresence,0)

----------------------------------
--FB 2141

/*
   Wednesday, June 29, 20113:18:04 PM
   User: myvrm
   Server: demo1
   Database: myvrm
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	PluginConfirmations smallint NULL
GO
ALTER TABLE dbo.Org_Settings_D ADD CONSTRAINT
	DF_Org_Settings_D_PluginConfirmations DEFAULT 0 FOR PluginConfirmations
GO
COMMIT

Update Org_Settings_D set PluginConfirmations  = 1

/*
   Wednesday, June 29, 20113:04:56 PM
   User: myvrm
   Server: demo1
   Database: myvrm
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Usr_List_D ADD
	PluginConfirmations smallint NULL
GO
ALTER TABLE dbo.Usr_List_D ADD CONSTRAINT
	DF_Usr_List_D_PluginConfirmations DEFAULT 0 FOR PluginConfirmations
GO
COMMIT

Update Usr_List_D  set PluginConfirmations  = 1
--------------------------------------------------------
--FB 2154
/*
   Wednesday, July 06, 20111:36:09 PM
   User: myvrm
   Server: sql2k5
   Database: Demo4
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	SendAttachmentsExternal smallint NULL
GO
ALTER TABLE dbo.Org_Settings_D ADD CONSTRAINT
	DF_Org_Settings_D_SendAttachmentsExternal DEFAULT 0 FOR SendAttachmentsExternal
GO
COMMIT


/****** Object:  Table [dbo].[Email_Domain_D]    Script Date: 07/06/2011 13:51:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Email_Domain_D](
	[DomainID] [int] IDENTITY(1,1) NOT NULL,
	[Companyname] [nvarchar](50) NULL,
	[Emaildomain] [nvarchar](50) NOT NULL,
	[Active] [smallint] NOT NULL CONSTRAINT [DF_Table_1_Active]  DEFAULT ((1)),
	[orgId] [smallint] NULL,
 CONSTRAINT [PK_Email_Domain_D] PRIMARY KEY CLUSTERED 
(
	[DomainID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

/*
   Tuesday, January 19, 201010:44:44 PM
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Ept_List_D ADD
	CalendarInvite int NULL
GO
COMMIT

/*
   Monday, January 01, 200712:28:48 AM
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Email_Queue_D ADD
	Iscalendar smallint NULL
GO
ALTER TABLE dbo.Email_Queue_D ADD CONSTRAINT
	DF_Email_Queue_D_Iscalendar DEFAULT 0 FOR Iscalendar
GO
COMMIT

Update email_queue_d set Iscalendar = 0

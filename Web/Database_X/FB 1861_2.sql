
/****** Object:  Table [dbo].[Holiday_Type_S]    Script Date: 01/14/2011 03:24:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Holiday_Type_S](
	[HolidayType] [int] NOT NULL,
	[HolidayDescription] [nvarchar](50) NOT NULL,
	[Color] [nvarchar](20) NOT NULL,
	[Priority] [int] NOT NULL,
	[OrgId] [int] NOT NULL
	
) ON [PRIMARY]

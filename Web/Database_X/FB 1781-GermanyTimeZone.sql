/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO

INSERT INTO [dbo].[Gen_TimeZone_S] ([TimeZoneID], [StandardName], [Offset], [TimeZone], [TimeZoneDiff], [Bias], [StandardBias], [DaylightBias], [swYear], [swMonth], [swDayOfWeek], [swDay], [swHour], [swMinute], [swSecond], [swMilliseconds], [dwYear], [dwMonth], [dwDayOfWeek], [dwDay], [dwHour], [dwMinute], [dwSecond], [dwMilliseconds], [systemID], [DST], [CurDSTstart], [CurDSTend], [CurDSTstart1], [CurDSTend1], [CurDSTstart2], [CurDSTend2], [CurDSTstart3], [CurDSTend3], [CurDSTstart4], [CurDSTend4], [CurDSTstart5], [CurDSTend5], [CurDSTstart6], [CurDSTend6], [CurDSTstart7], [CurDSTend7], [CurDSTstart8], [CurDSTend8], [CurDSTstart9], [CurDSTend9]) VALUES (75, N'W. Europe Standard Time', -1, N'Berlin', N'(GMT+01:00)', -60, 0, -60, 0, 10, 0, 5, 3, 0, 0, 0, 0, 3, 0, 5, 2, 0, 0, 0, 0, 1, '20040328 02:00:00.000', '20041031 03:00:00.000', '20050327 02:00:00.000', '20051030 03:00:00.000', '20060326 02:00:00.000', '20061029 03:00:00.000', '20070325 02:00:00.000', '20071028 03:00:00.000', '20080330 02:00:00.000', '20081026 03:00:00.000', '20090329 02:00:00.000', '20091025 03:00:00.000', '20100328 02:00:00.000', '20101031 03:00:00.000', '20110327 02:00:00.000', '20111030 03:00:00.000', '20120325 02:00:00.000', '20121028 03:00:00.000', '20130331 02:00:00.000', '20131027 03:00:00.000')

GO
COMMIT 



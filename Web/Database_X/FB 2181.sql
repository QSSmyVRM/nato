--FB 2181 Start

INSERT INTO err_list_s  (ID,Message,Description,Level,Languageid,ErrorType)
VALUES (546,'At least one item should have a requested quantity greater than 0',
'At least one item should have a requested quantity greater than 0','E',1,'U');

--FB 2068 Start

INSERT INTO err_list_s  (ID,Message,Description,Level,Languageid,ErrorType)
VALUES (535,'Conference are associated with inventory set in quantity greater than the given quantity!',
'Conference are associated with inventory set in quantity greater than the given quantity!','E',1,'U');

--FB 2068 End

--FB 2181 End
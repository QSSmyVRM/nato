/* **************** FB 1979 - Support for mobile devices in License. ************** */


/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION

GO
ALTER TABLE dbo.Usr_List_D ADD
	enableMobile int NULL
GO
ALTER TABLE dbo.Usr_List_D ADD CONSTRAINT
	DF_Usr_List_D_enableMobile DEFAULT 0 FOR enableMobile
GO
COMMIT

  /* Update existing data with default value 0 */

Update Usr_List_D Set enableMobile = 0 



/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Usr_Inactive_D ADD
	enableMobile int NULL
GO
ALTER TABLE dbo.Usr_Inactive_D ADD CONSTRAINT
	DF_Usr_Inactive_D_enableMobile DEFAULT 0 FOR enableMobile
GO
COMMIT

  /* Update existing data with default value 0 */

Update Usr_Inactive_D Set enableMobile = 0




/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	MobileUserLimit int NULL
GO
ALTER TABLE dbo.Org_Settings_D ADD CONSTRAINT
	DF_Org_Settings_D_MobileUserLimit DEFAULT 0 FOR MobileUserLimit
GO
COMMIT

/* Update existing data with default value 0 */

Update Org_Settings_D Set MobileUserLimit = 0 

/* ****** Error Messages for Mobile Users *********** */

Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) 
values (525,'User does not have Mobile license,','User does not have Mobile license.',
'E',1,'U')



Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) 
values (526,'Mobile user limit exceeds VRM license.','Mobile user limit exceeds VRM license.',
'E',1,'U')


update err_list_s set Message='User limit should be inclusive for domino,exchange and mobile users.',
 Description='User limit should be inclusive for domino,exchange and mobile users.' 
where ID=462


Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) 
values (527,'Please delete mobile users to reduce the count.As there are more active mobile users.','Please delete mobile users to reduce the count.As there are more active mobile users.',
'E',1,'U')


Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) 
values (528,'License Check FAILED: Please reduce the Mobile User Limit in any of the Organization.','License Check FAILED: Please reduce the Mobile User Limit in any of the Organization.',
'E',1,'U')
 



/* 
Sample License XML:
-------------------

<myVRMSiteLicense><IR>1-myVRMQA</IR><Site><ExpirationDate>11/30/2014</ExpirationDate><MaxOrganizations>5</MaxOrganizations><IsLDAP>1</IsLDAP><ServerActivation><ProcessorIDs><ID></ID><ID></ID></ProcessorIDs><MACAddresses><ID></ID><ID></ID></MACAddresses></ServerActivation></Site><Organizations><Rooms><MaxNonVideoRooms>150</MaxNonVideoRooms><MaxVideoRooms>400</MaxVideoRooms></Rooms><Hardware><MaxMCUs>25</MaxMCUs><MaxEndpoints>400</MaxEndpoints><MaxCDRs></MaxCDRs></Hardware><Users><MaxTotalUsers>200</MaxTotalUsers><MaxGuestsPerUser>10</MaxGuestsPerUser><MaxExchangeUsers>100</MaxExchangeUsers><MaxDominoUsers>100</MaxDominoUsers><MaxMobileUsers>10</MaxMobileUsers></Users><Modules><MaxFacilitiesModules>5</MaxFacilitiesModules><MaxCateringModules>5</MaxCateringModules><MaxHousekeepingModules>5</MaxHousekeepingModules><MaxAPIModules>1</MaxAPIModules><Languages><MaxSpanish></MaxSpanish><MaxMandarin></MaxMandarin><MaxHindi></MaxHindi><MaxFrench></MaxFrench></Languages></Modules></Organizations></myVRMSiteLicense>
*/
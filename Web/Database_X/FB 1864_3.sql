/*
   Thursday, January 13, 20113:43:21 PM
   User: myvrm
   Server: gowniyanvm
   Database: myvrm
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	isVIP smallint NULL
GO
ALTER TABLE dbo.Org_Settings_D ADD CONSTRAINT
	DF_Org_Settings_D_isVIP DEFAULT 0 FOR isVIP
GO
COMMIT

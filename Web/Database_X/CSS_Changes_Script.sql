
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO

ALTER TABLE [dbo].[Org_Settings_D] ADD[DefaultCssId] [int] NULL CONSTRAINT [DF_Org_Settings_D_DefaultCssId] DEFAULT ((0))

ALTER TABLE [dbo].[Org_Settings_D] ADD[MirrorCssId] [int] NULL CONSTRAINT [DF_Org_Settings_D_MirrorCssId] DEFAULT ((0))

ALTER TABLE [dbo].[Org_Settings_D] ADD[ArtifactsCssId] [int] NULL CONSTRAINT [DF_Org_Settings_D_ArtifactsCssId] DEFAULT ((0))

ALTER TABLE [dbo].[Org_Settings_D] ADD[TextchangeId] [int] NULL CONSTRAINT [DF_Org_Settings_D_TextchangeId] DEFAULT ((0))

GO
COMMIT



/*
   Friday, January 05, 200712:37:32 AM
   User: 
   Server: GANGES
   Database: OrgDB
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Sys_Settings_D ADD
	SiteLogoId int NULL,
	Companymessage varchar(50) NULL,
    	StdBannerId int NULL,
    	HighBannerId int NULL
GO
COMMIT

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE Dept_CustomAttr_D ADD
	 RoomApp int NULL,
     McuApp int NULL,
	 SystemApp int NULL,
     Scheduler int NULL,
	 Host int NULL,
	 Party int NULL,
     McuAdmin int NULL,
	 RoomAdmin int NULL
GO
COMMIT

GO
Update Dept_CustomAttr_D set RoomApp= 0,McuApp = 0,SystemApp = 0,Scheduler = 0,Host = 0,

  Party = 0,McuAdmin = 0,RoomAdmin = 0 where IncludeInEmail = 0

GO
Update Dept_CustomAttr_D set RoomApp= 0,McuApp = 0,SystemApp = 0,Scheduler = 0,Host = 0,

  Party = 1,McuAdmin = 1,RoomAdmin = 0 where IncludeInEmail = 1
                             
    


--Execute the below only once to create View-Only role

Declare @roldid int

set @roldid = (select MAX(roleID)+ 1 from Usr_Roles_D)

insert into Usr_Roles_D (roleID,roleName,roleMenuMask,locked,level,crossaccess,createType) values(
@roldid ,'View-Only','8*208-4*15+8*0+3*0+2*0+8*0+2*0+2*0+2*0+1*0-6*14',1,0,0,1)


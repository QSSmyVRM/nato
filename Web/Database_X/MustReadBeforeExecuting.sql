/*** Please cross check database before running below INSERT statements ***/


select * from [Loc_Tier2_D] where Id = 0

select * from [Loc_Tier3_D] where Id = 0

select * from [Loc_Room_D] where roomid = 11

/*************      If no results found, please run below INSERT statements otherwise dont execute   ***********/


/*
-- One Time Run Query

SET IDENTITY_INSERT [dbo].[Loc_Tier2_D] ON
INSERT INTO [dbo].[Loc_Tier2_D] ([Id], [Name], [Address], [L3LocationId], [Comment], [disabled]) VALUES (0, N'Phantom', NULL, 0, N'Phantom Room Tier', 1)
SET IDENTITY_INSERT [dbo].[Loc_Tier2_D] OFF


SET IDENTITY_INSERT [dbo].[Loc_Tier3_D] ON
INSERT INTO [dbo].[Loc_Tier3_D] ([Id], [Name], [Address], [disabled]) VALUES (0, N'Tier3', N'', 1)
SET IDENTITY_INSERT [dbo].[Loc_Tier3_D] OFF



SET IDENTITY_INSERT [dbo].[Loc_Room_D] ON
INSERT INTO [dbo].[Loc_Room_D] ([RoomID], [Name], [RoomBuilding], [RoomFloor], [RoomNumber], [RoomPhone], [Capacity], [Assistant], [AssistantPhone], [ProjectorAvailable], [MaxPhoneCall], [AdminID], [videoAvailable], [DefaultEquipmentid], [DynamicRoomLayout], [Caterer], [L2LocationId], [L3LocationId], [Disabled], [responsetime], [responsemessage], [roomimage], [setuptime], [teardowntime], [auxattachments], [endpointid], [costcenterid], [notifyemails]) VALUES (11, N'Phantom Room', N'None', N'None', N'None', N'None', N'0', N'11', N'', 0, 0, 11, 0, 0, 0, 0, 0, 0, 1, 0, N'0', N'0', 0, 0, N'0', 0, 0, N'0')
SET IDENTITY_INSERT [dbo].[Loc_Room_D] OFF


*/
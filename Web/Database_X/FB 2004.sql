/********* FB 2004  Starts ***********/
GO
CREATE TABLE [dbo].[Conf_CreateType_S]
(
[CreateID] [smallint] NOT NULL,
[VendorName] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Version] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)

GO
ALTER TABLE [dbo].[Conf_CreateType_S] ADD CONSTRAINT [PK_Conf_CreateType_S] PRIMARY KEY CLUSTERED  ([CreateID])


-- Add 7 rows to [dbo].[Conf_CreateType_S]
INSERT INTO [dbo].[Conf_CreateType_S] ([CreateID], [VendorName], [Version]) VALUES (0, N'VRM', N'')
INSERT INTO [dbo].[Conf_CreateType_S] ([CreateID], [VendorName], [Version]) VALUES (1, N'Outlook York', N'')
INSERT INTO [dbo].[Conf_CreateType_S] ([CreateID], [VendorName], [Version]) VALUES (2, N'Outlook Generic', N'')
INSERT INTO [dbo].[Conf_CreateType_S] ([CreateID], [VendorName], [Version]) VALUES (3, N'Lotus York', N'')
INSERT INTO [dbo].[Conf_CreateType_S] ([CreateID], [VendorName], [Version]) VALUES (4, N'Lotus Generic', N'')
INSERT INTO [dbo].[Conf_CreateType_S] ([CreateID], [VendorName], [Version]) VALUES (5, N'Ipad', N'')
INSERT INTO [dbo].[Conf_CreateType_S] ([CreateID], [VendorName], [Version]) VALUES (6, N'Other mobile devices', N'')


/********* FB 2004  Ends ***********/
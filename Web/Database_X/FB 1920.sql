/*
   Wednesday, February 16, 20116:09:09 AM
   User: myvrm
   Server: SERVER2004\SQL2005
   Database: TestDB
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Mcu_List_D ADD
	isPublic smallint NOT NULL CONSTRAINT DF_Mcu_List_D_Public DEFAULT 0
GO
COMMIT
select Has_Perms_By_Name(N'dbo.Mcu_List_D', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Mcu_List_D', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Mcu_List_D', 'Object', 'CONTROL') as Contr_Per 


--Error Message for changing Public as Private

Insert into Err_List_S (ID,  Message, Description, Level, Languageid,Errortype) values (516,'Error in Updating MCU as Private,MCU has been Used','Error in Updating MCU as Private,MCU has been Used','E',1,'U')




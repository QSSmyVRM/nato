/*
   Monday, January 17, 201111:54:55 AM
   User: myvrm
   Server: gowniyanvm
   Database: myvrm
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Usr_List_D ADD
	MailBlocked smallint NULL,
	MailBlockedDate datetime NULL
GO
ALTER TABLE dbo.Usr_List_D ADD CONSTRAINT
	DF_Usr_List_D_MailBlocked DEFAULT 0 FOR MailBlocked
GO
ALTER TABLE dbo.Usr_List_D ADD CONSTRAINT
	DF_Usr_List_D_MailBlockedDate DEFAULT (getutcdate()) FOR MailBlockedDate
GO
COMMIT

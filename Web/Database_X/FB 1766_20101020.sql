/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Mcu_List_D ADD
	EnableIVR int NULL,
	IVRServiceName nvarchar(250) NULL
GO


ALTER TABLE dbo.Mcu_List_D ADD CONSTRAINT
	DF_Mcu_List_D_EnableIVR DEFAULT 0 FOR EnableIVR 
GO

COMMIT

/* Query to update existing records */

update Mcu_List_D set EnableIVR = 0
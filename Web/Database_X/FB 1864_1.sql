/*
   Thursday, January 13, 201110:17:52 PM
   User: myvrm
   Server: gowniyanvm
   Database: V2X
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_Conference_D ADD
	isDedicatedEngineer smallint NULL,
	isLiveAssistant smallint NULL
GO
ALTER TABLE dbo.Conf_Conference_D ADD CONSTRAINT
	DF_Conf_Conference_D_isDedicatedEngineer DEFAULT 0 FOR isDedicatedEngineer
GO
ALTER TABLE dbo.Conf_Conference_D ADD CONSTRAINT
	DF_Conf_Conference_D_isLiveAssistant DEFAULT 0 FOR isLiveAssistant
GO
COMMIT

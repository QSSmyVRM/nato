INSERT INTO err_list_s  (ID,Message,Description,Level,Languageid,ErrorType)
VALUES (518,'User is linked with a conference either as Scheduler/Host/Participants .Cannot delete!',
'User is linked with a conference either as Scheduler/Host/Participants .Cannot delete!','E',1,'U');

INSERT INTO err_list_s  (ID,Message,Description,Level,Languageid,ErrorType)
VALUES (519,'Error in user operation,User is assigned as a System/MCU/Room/Department approver.Cannot delete!',
'Error in user operation,User is assigned as a System/MCU/Room/Department approver.Cannot delete!','E',1,'U');

INSERT INTO err_list_s  (ID,Message,Description,Level,Languageid,ErrorType)
VALUES (520,'Error in user operation,User is assigned as a System/MCU/Room/Workorder Incharge.Cannot delete!',
'Error in user operation,User is assigned as a System/MCU/Room/Workorder Incharge.Cannot delete!','E',1,'U');

INSERT INTO err_list_s  (ID,Message,Description,Level,Languageid,ErrorType)
VALUES (521,'User is linked with a Group/Department .Cannot delete!',
'User is linked with a Group/Department .Cannot delete!','E',1,'U');
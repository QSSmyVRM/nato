

--Excute the below only once to create Express Profile

Declare @roldid int

set @roldid = (select MAX(roleID)+ 1 from Usr_Roles_D)

insert into Usr_Roles_D (roleID,roleName,roleMenuMask,locked,level,crossaccess) values(
@roldid ,'Express Profile Manage','8*66-4*0+8*0+3*0+2*0+8*0+2*0+2*0+2*0+1*0-6*2',1,0,0)

set @roldid = (select MAX(roleID)+ 1 from Usr_Roles_D)

insert into Usr_Roles_D (roleID,roleName,roleMenuMask,locked,level,crossaccess) values(
@roldid ,'Express Profile Advanced','8*82-4*4+8*0+3*0+2*0+8*0+2*0+2*0+2*0+1*0-6*2',1,0,0)
/* Custom attribute Changes */
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Dept_CustomAttr_Option_D ADD
	HelpText nvarchar(4000) NULL
GO
COMMIT

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Dept_CustomAttr_D ADD
	IncludeInEmail smallint NULL
GO
COMMIT


BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_CustomAttr_D ADD
	[ConfAttrID] [int] IDENTITY(1,1) NOT NULL
GO
COMMIT


/* Update queries for Guest */

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Usr_GuestList_D ADD
	DateFormat nvarchar(50) NULL,
	enableAV int NULL,
	timezonedisplay int NULL,
	timeformat int NULL
GO
ALTER TABLE dbo.Usr_GuestList_D ADD CONSTRAINT
	DF_Usr_GuestList_D_DateFormat DEFAULT (N'MM/dd/yyyy') FOR DateFormat
GO
ALTER TABLE dbo.Usr_GuestList_D ADD CONSTRAINT
	DF_Usr_GuestList_D_enableAV DEFAULT ((0)) FOR enableAV
GO
ALTER TABLE dbo.Usr_GuestList_D ADD CONSTRAINT
	DF_Usr_GuestList_D_timezonedisplay DEFAULT ((1)) FOR timezonedisplay
GO
ALTER TABLE dbo.Usr_GuestList_D ADD CONSTRAINT
	DF_Usr_GuestList_D_timeformat DEFAULT ((1)) FOR timeformat
GO
COMMIT

Update Usr_GuestList_D set 
DateFormat = N'MM/dd/yyyy'
where (DateFormat is null or DateFormat = null)

Update Usr_GuestList_D set 
enableAV = 0
where (enableAV is null or enableAV = null)

Update Usr_GuestList_D set 
timezonedisplay = 0
where (timezonedisplay is null or timezonedisplay = null)

Update Usr_GuestList_D set 
timeformat = 0
where (timeformat is null or timeformat = null)

--

/* Update data for bufferzone */

/* Update statement for defaulting the confstarttime to setuptime */
/* for existing old data */

Update conf_conference_D set 
setupTime = confdate
where (setupTime is null or setuptime = null)

/* Update statement for defaulting the confend time to teardown time */
/* for existing old data */

Update conf_conference_D set 
teardownTime = Dateadd(minute,duration,confdate)
where (teardownTime is null or teardownTime = null)


--
/*
   Friday, October 09, 20099:12:01 PM
   User: 
   Server: revathivm\SQLEXPRESS
   Database: myVRM_090109
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Usr_List_D ADD
	TickerStatus int NULL,
	TickerSpeed int NULL,
	TickerPosition int NULL,
	TickerBackground nchar(10) NULL,
	TickerDisplay int NULL,
	RSSFeedLink nvarchar(50) NULL,
	TickerStatus1 int NULL,
	TickerSpeed1 int NULL,
	TickerPosition1 int NULL,
	TickerBackground1 nchar(10) NULL,
	TickerDisplay1 int NULL,
	RSSFeedLink1 nvarchar(50) NULL
GO
COMMIT


update usr_list_d set tickerstatus=1,tickerspeed=6,TickerPosition=0,
TickerBackground='#660066',TickerDisplay=0 

update usr_list_d set tickerstatus1=1,tickerspeed1=3,TickerPosition1=0,
TickerBackground1='#99ff99',TickerDisplay1=0 

--
/* Activation Module */

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Sys_Settings_D ADD
	IsDemo smallint NULL,
	ActivatedDate datetime NULL
GO
ALTER TABLE dbo.Sys_Settings_D ADD CONSTRAINT
	DF_Sys_Settings_D_IsDemo DEFAULT 0 FOR IsDemo
GO
COMMIT

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Sys_Settings_D ADD CONSTRAINT
	DF_Sys_Settings_D_ActivatedDate DEFAULT (01/01/1990) FOR ActivatedDate
GO
COMMIT

Update Sys_Settings_D set ActivatedDate = '01/01/1990'

Update Sys_Settings_D set IsDemo = '0'

--

/* Room Search Updates  */

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Loc_Room_D ADD
	HandiCappedAccess smallint NULL
GO
ALTER TABLE dbo.Loc_Room_D ADD
	Lastmodifieddate datetime NULL
GO
ALTER TABLE dbo.Loc_Room_D ADD CONSTRAINT
	DF_Loc_Room_D_HandiCappedAccess DEFAULT 0 FOR HandiCappedAccess
GO
ALTER TABLE dbo.Loc_Room_D ADD CONSTRAINT
	DF_Loc_Room_D_Lastmodifieddate DEFAULT getdate() FOR Lastmodifieddate
GO
COMMIT

Update loc_room_d set HandiCappedAccess = 0
Update loc_room_d set Lastmodifieddate = getdate()

--

/* License SQL Changes */

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Usr_List_D ADD
	enableExchange int NULL,
	enableDomino int NULL
GO
ALTER TABLE dbo.Usr_List_D ADD CONSTRAINT
	DF_Usr_List_D_enableExchange DEFAULT 0 FOR enableExchange
GO
ALTER TABLE dbo.Usr_List_D ADD CONSTRAINT
	DF_Usr_List_D_enableDomino DEFAULT 0 FOR enableDomino
GO
COMMIT

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Usr_Inactive_D ADD
	enableExchange int NULL,
	enableDomino int NULL
GO
ALTER TABLE dbo.Usr_Inactive_D ADD CONSTRAINT
	DF_Usr_Inactive_D_enableExchange DEFAULT 0 FOR enableExchange
GO
ALTER TABLE dbo.Usr_Inactive_D ADD CONSTRAINT
	DF_Usr_Inactive_D_enableDomino DEFAULT 0 FOR enableDomino
GO
COMMIT


/*Added for New License this is default values*/


/* V2x License Structure */

update sys_settings_d set license='KmzsDo+0yAjTApoYqR4/Egd2oqJZ7iUZgqwBLuJ2YCr1JXM2hSYxfUBxjl+Ct6IKgt4orJRTkvoTqTqS6f92Me6hejM0EatEOXV9ImWU5DnJnwU1BUjV0PU2mE+kSAlB1fZdEsMvuBh7bZkiXmM971jS9X/y6GttPJ+ZVZ9GzOQXzxeGRfbY4tUIOLzvvk80E/yL4SJkRpdv6PGqaF4w71C4uaXj8DIB/1T4hb9cK7fEGioBeOA6vz+Ii2vBPQM3hFyhF64Yht2qMNTypKGtI2jQ4o5wiLWhy+oXbZaDWM3r8atH4t1QNfywopu3ivczMpkRwVFDli1ZqCEkjng5saYNF4Fx/zxU5h1bmhXJWsjzto502eIasDh8EqfPic6lorVSui/aJTq67Gfe07D+1prtbPrC86d5cnvE5ltGJJ/iFXg3u3zKGAvDez4B14WMaK0GaTkE6j6cIo18wlcCE5MaO0Pi13c49FLArg8jT5AjfkHL33/jySVnJyYs0KDxVjNJzcDaFwekIzoj0dApm6TPAUxlTuM/snxTodoLWXWtZjL0QBfJas0GVBKBOiuniQ+CrAXp5HedJrzlNtuubvkAg4HA6yHrY4VQVsugQL4oZ4wttqT79fVx6kLn/nq/Br65SEykTcp9/JRjZRuv+J4Wz7JAqHbVYesdygbrFHaxAIL//c7FsDC9AQHSjDLCg/brQ2MQtV775eBnyI2Vbiw044EaRsrM5exOBPgJYb1EsXdx0ZpTV+Q/bVYkiK+lAWG5XdWRWN360iNdIdMfGVh7AGY4VsXJIVAGOA4++UecSKXVFebXje0ut6Wa2kmzHXEHfAeiPdZPw6wKMrpY8APayDnUeP49AwR2pYSTYchvMuRNeQQ/DhrRnmDtX0YjGVZAa4ABj0x2NGH0nQiIViahRm1ezoSTq7hiAGqUgOSmWocgliOZ/0xL9zV9S6ql3/FZ16ufl5jEsAJvc1Pia6cIAbqv2ImVgpUr3Sqq2dr2cBq25fQ0Wu4FWiIdZNq0KTMCltvPJUDWMmdgL5fGNwlmQA2K05pYE1YPWARnQzj5HL7p5kD77p0WPbiflNZVai516AI+kHEE3syVTzddMQ29UiflmnMgmXjny59mDGMNq7mICpy14MCqSIM5GGeAhehnWv/s4ofQoG+erbJPZ1wmENJzCukK0ENMhd4yO8iqlcjAfoLrOZQEMEZ5RjCF3RoN0QIxLUGgqdFcB8LvjO6RDRInSpESnctrY9Pp1PJiDq3yLKcysRUN9baDVVCcBCMtqbB/RGKOQF7sWrLvjGBhjJN1/HHlqkb39AcZuYMOXpHCIEd7APBHBdWOjbBApUD5JMi3a/ufdZdJ7Ro5IuFpK63Pf2c1L+/AgJC1dig=' where [Admin] = 11
--

/* Endpoint sql changes */




/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Ept_List_D ADD
	ExchangeID nvarchar(200) NULL
GO

ALTER TABLE dbo.Conf_User_D ADD
	ExchangeID nvarchar(200) NULL
GO

COMMIT


Insert into Gen_VideoEquipment_S values (27,'Cisco Telepresence System')

--

/* Image Project Related Script Changes - start */

/****** Object:  Table [dbo].[Img_List_D]    Script Date: 01/06/2010 04:22:22 ******/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO

CREATE TABLE [dbo].[Img_List_D](
	[ImageId] [int] IDENTITY(1,1) NOT NULL,
	[AttributeType] [smallint] NULL,
	[AttributeImage] [varbinary](max) NULL,
	[OrgId] [int] NULL,
 CONSTRAINT [PK_Img_List_D] PRIMARY KEY CLUSTERED 
(
	[ImageId] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
COMMIT


BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO

ALTER TABLE Org_Settings_D ADD
	LogoImageId int NULL,
	LobytopImageId int NULL,
	LobytopHighImageId int NULL,
	LogoImageName nvarchar(250) NULL,
	LobytopImageName nvarchar(250) NULL,
	LobytopHighImageName nvarchar(250) NULL
GO


ALTER TABLE Loc_Room_D ADD
	MapImage1Id int NULL,
	MapImage2Id int NULL,
	SecurityImage1Id int NULL,
	SecurityImage2Id int NULL,
	MiscImage1Id int NULL,
	MiscImage2Id int NULL,
	RoomImageId nvarchar(200) NULL

GO

ALTER TABLE Inv_ItemList_AV_D ADD
	ImageId int NULL
GO

ALTER TABLE Inv_ItemList_HK_D ADD
	ImageId int NULL
GO

ALTER TABLE Inv_List_D ADD
	ImageId int NULL
GO
ALTER TABLE Usr_Inactive_D ALTER COLUMN /* FB 1698*/
PreferedRoom varchar(150)
GO

COMMIT

/* Image Project Related Script Changes - end */


/*Account Spell Issue*/
update err_list_s set message ='Insufficient account balance to set up a conference. Please contact your VRM Administrator and supply this error code to obtain a usage account.' where id=231
Follow this order for executing sql statements:

1) MASTER_UI_SCHEMA.sql (Dont run this script for V 1.9x DB)

2) MASTER_UI_DATA.sql (Dont run this script for V 1.9x DB)

3) DroppedSQLtables.sql (Dont run this script for V 1.9x DB)

4) UpdateScriptforOrgModule.sql

5) UpdateOrgData.sql

6) NewEnhancementQueries.sql (Check and run each and every script till Ticker enhancements for availability in V 1.9x DB)

7) Dashboard.sql

8) RoomSearch.sql

9) Updateroles.sql

10) CSS_Changes_Script.sql

11) EmailLogo.sql

12) ICAL_Change.sql

13) ErrLogs.sql (before running this query please check Err_List_S table for the last updated error number)

14) APIPortScript.sql

15) DTMF_Change.sql

16) Gen_LineRate.sql

17) ADupdatescript.sql

18) audioaddon.sql

19) BlueStatus.sql

20) FB 1710.sql

21) FB 1721-Gen_Interface.sql

22) SetDefaultTemplateScript.sql

23) 1744-script.sql

24) FB 1767.sql

25) FB 1766_20101020.sql

26) FB 1781-GermanyTimeZone.sql

27) FB 1786 & FB 1710.sql

28)Express Interface Specific.sql

29)IcalQueries_FB1782.sql

30)FB 1830.sql

31)EmailData_Script.sql

32)TechPhone.sql

33)FB 1860_1.sql

34)FB 1860_2.sql

35)FB 1860_3.sql

36)FB 1860_4.sql

37)FB 1861_1.sql

38)FB 1861_2.sql

39)FB 1861_3.sql

40)FB 1864_1.sql

41)FB 1864_2.sql

42)FB 1864_3.sql

43)FB 1864_4.sql

44)FB 1865_1.sql

45)FB 1779 New Express Role.sql

46)FB 1901.sql

47)FB 1675.sql

48)err.sql

49)Err_List_Data.Sql

50)FB 1919.Sql

51)Conf_Reminders.sql

52)Org_Reminders.sql

53)FB 1939_UserRole.sql

54)MCUAlertContentUpdateQuery.sql

55)1907.sql

56)1921.sql

57)FB 1920.sql

58)FB1933.sql

59)MSE8000.sql

60)FB 1944.sql

61)FB 1899 & 1900.sql

62)ReleaseEmail.sql

63)FB 1522_View-Only Role.sql

64) ISDN_Gateway.sql

65) FB 1750_SelectCalendarDetails(Alter).sql   - (NGC Specfic)

66) FB 2007

67) ISDN_Prefix.sql

68) FB 1979.sql

69) FB 1970.sql

70)FB 2013.sql

71) 1497

72)FB 2016.sql

73) FB 2054.sql

74) FB 1907 & 2057 .sql

75) FB 2027.sql

76) FB1881-2.sql

77) FB 2140

78) MultilingualScripts.sql

79) FB 2141 Org_Settings_D_PluginConfirmations.sql

80) FB 2141 Usr_List_D_PluginConfirmations.sql

81) 2164.sql(Updated on_28july)

82) FB 2004.sql (V24_18July)

83) Consolidated.sql

84) FB 2189.sql (V24 _11 Aug)

85) FB 2164_EmptyDB.sql

86) FB 2075.sql

87) FB 2181.sql

88) FB 2227.sql (V24_19Sep)

89) FB 2045.sql (V24_20Sep)

90) FB 2219.sql (V24_26Sep)

91) FB 2045_1.sql (V24_26Sep)

92) FB 2052.sql(V24_4Oct)

93) FB 2218.sql

94) FB 2268_HelpME.sql

95) FB 2038.sql

96) FB 2051.sql (V25_24OCt)

97) FB 2269.sql

98) FB 2038_1.sql

99) FB 2283.sql

Note:
To create DB for V 2.x, please run all the scripts.
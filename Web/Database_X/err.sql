/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Err_List_S ADD
	UID int NOT NULL IDENTITY (1, 1),
	Languageid smallint NULL,
	ErrorType nchar(1) NULL
GO
ALTER TABLE dbo.Err_List_S ADD CONSTRAINT
	DF_Err_List_S_Languageid DEFAULT 1 FOR Languageid
GO
ALTER TABLE dbo.Err_List_S ADD CONSTRAINT
	DF_Err_List_S_ErrorType DEFAULT N'S' FOR ErrorType
GO
COMMIT


------------------------------------

update err_list_s set languageid=1


update err_list_s set errortype='S' where id in
(201,305,306,309,310,311,312,313,314,315,316,317,318,319,320,321,322,323,326,327,328,330,331,334,400,409,414,423,424)

update err_list_s set errortype='U' where id not in
(201,305,306,309,310,311,312,313,314,315,316,317,318,319,320,321,322,323,326,327,328,330,331,334,400,409,414,423,424)


delete from err_list_s where id in (409,301,401,239,236,421,405)
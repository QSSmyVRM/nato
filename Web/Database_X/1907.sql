/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Mcu_List_D ADD
	EnableRecording smallint NULL
GO
ALTER TABLE dbo.Mcu_List_D ADD CONSTRAINT
	DF_Mcu_List_D_EnableRecording DEFAULT 0 FOR EnableRecording
GO
COMMIT

/* Update existing data with default value 0 */
Update Mcu_List_D Set EnableRecording = 0
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Dept_CustomAttr_D ADD
	IncludeInCalendar smallint NULL
GO
ALTER TABLE dbo.Dept_CustomAttr_D ADD CONSTRAINT
	DF_Dept_CustomAttr_D_IncludeInCalendar DEFAULT 0 FOR IncludeInCalendar
COMMIT

update Dept_CustomAttr_D set IncludeInCalendar = 1 where createtype=1

update Dept_CustomAttr_D set IncludeInCalendar = 0 where createtype=0

/* ******************************************************* */
/*			FB 1744				   */
/* Participant media type issue. To make consistant across */
/* application, None is removed from conferencesetup       */
/* ******************************************************* */

/*
Table values before FB 1744
1 - None
2 - Audio Only
3 - Audio-Video
*/


update Gen_MediaType_S set MediaType ='Audio-only' where ID = 1

update Gen_MediaType_S set MediaType ='Audio,Video' where ID = 2

delete from Gen_MediaType_S where ID = 3

/*

Table values after FB 1744

1 - Audio Only
2 - Audio-Video
*/


/* Conf_User_D, Conf_Room_D, conf_cascade_D & Loc_Room_D */

-- External Participant Records with value 1 (None) will be treated as Audio itself

Update Conf_User_D set audioorvideo=1 where audioorvideo=2 -- Audio

Update Conf_User_D set audioorvideo=2 where audioorvideo=3 -- Video


-- Room Endpoints

Update Conf_Room_D set audioorvideo=2 where audioorvideo=3 -- Video


-- records with value 1 (None) will be treated as Audio itself

--Update conf_cascade_D set audioorvideo=1 where audioorvideo=2 -- Audio

--Update conf_cascade_D set audioorvideo=2 where audioorvideo=3 -- Video


-- Location Table

Update Loc_Room_D set videoavailable=2 where videoavailable=3 -- Video



/* Restrict USage - AV Common Settings */

-- Records with value None will be treated as Video

-- All records with 1 as value needs to be updated as 5

Update Conf_AdvAVParams_D set mediaID=5 where mediaID=1

Update Conf_AdvAVParams_D set mediaID=1 where mediaID=2 -- Audio 

Update Conf_AdvAVParams_D set mediaID=2 where mediaID=3 -- Video

Update Conf_AdvAVParams_D set mediaID=2 where mediaID=5 -- None is converted to Video


--Tmp_AdvAVParams_D

-- Records with value None will be treated as Video

-- All records with 1 as value needs to be updated as 5

Update Tmp_AdvAVParams_D set mediaID=5 where mediaID=1

Update Tmp_AdvAVParams_D set mediaID=1 where mediaID=2 -- Audio 

Update Tmp_AdvAVParams_D set mediaID=2 where mediaID=3 -- Video

Update Tmp_AdvAVParams_D set mediaID=2 where mediaID=5 -- None is converted to Video
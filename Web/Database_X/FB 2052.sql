/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_Holiday_Type_S
	(
	HolidayType int NOT NULL IDENTITY (1, 1),
	HolidayDescription nvarchar(50) NOT NULL,
	Color nvarchar(20) NOT NULL,
	Priority int NOT NULL,
	OrgId int NOT NULL
	)  ON [PRIMARY]
GO
SET IDENTITY_INSERT dbo.Tmp_Holiday_Type_S ON
GO
IF EXISTS(SELECT * FROM dbo.Holiday_Type_S)
	 EXEC('INSERT INTO dbo.Tmp_Holiday_Type_S (HolidayType, HolidayDescription, Color, Priority, OrgId)
		SELECT HolidayType, HolidayDescription, Color, Priority, OrgId FROM dbo.Holiday_Type_S WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_Holiday_Type_S OFF
GO
DROP TABLE dbo.Holiday_Type_S
GO
EXECUTE sp_rename N'dbo.Tmp_Holiday_Type_S', N'Holiday_Type_S', 'OBJECT' 
GO
COMMIT


/* To prevent any potential data loss issues, you should review this script 
in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	SpecialRecur smallint NULL
GO
ALTER TABLE dbo.Org_Settings_D ADD CONSTRAINT
	DF_Org_Settings_D_SpecialRecur DEFAULT 1 FOR SpecialRecur
GO
COMMIT

Update Org_Settings_D set SpecialRecur = 1

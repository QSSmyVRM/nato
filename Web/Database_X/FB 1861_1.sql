
/****** Object:  Table [dbo].[Holidays_details_D]    Script Date: 01/14/2011 03:23:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Holidays_details_D](
	[UID] [int] IDENTITY(1,1) NOT NULL,
	[OrgId] [int] NOT NULL,
	[EntityID] [int] NULL,
	[EnitityType] [int] NULL,
	[Date] [datetime] NOT NULL,
	[HolidayType] [int] NOT NULL,
 CONSTRAINT [PK_Holidays_details_D] PRIMARY KEY CLUSTERED 
(
	[UID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

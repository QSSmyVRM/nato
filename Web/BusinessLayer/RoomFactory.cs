using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using log4net;
using NHibernate;
using NHibernate.Criterion;
using myVRM.DataLayer;
using System.Data; //FB 2027

namespace myVRM.BusinessLayer
{
    /// <summary>
    /// Data Layer Logic for loading/saving Rooms data objects
    /// </summary>
    /// 
    public class RoomFactory
    {
        /// <summary>
        /// construct Room factory 
        /// </summary>
        private myVRMException myVRMEx; 
        private static log4net.ILog m_log;
        private string m_configPath;
        private const int defaultOrgId = 11;
        internal int organizationID = 0;
       
        private conferenceDAO m_confDAO;
        private orgDAO m_OrgDAO;
        private userDAO m_usrDAO;
        private deptDAO m_deptDAO;
        private GeneralDAO m_generalDAO;
        private IOrgSettingsDAO m_IOrgSettingsDAO;
        private IConferenceDAO m_IconfDAO;
        private IConfUserDAO m_IconfUser;
        private IConfRoomDAO m_IconfRoom;
        private IUserDao m_IUserDAO;
        private IConfAttrDAO m_IconfAttrDAO;
        private IDeptCustomAttrDao m_IDeptCustDAO;
        private ICustomLangDao m_ICustomAttrLangDAO;
        private IDeptCustomAttrOptionDao m_IDeptCustOptDAO;
        private IRoomDAO m_vrmRoomDAO;//FB 2027(GetOldRoom)
        private IDeptDao m_IdeptDAO;
        private ILocDeptDAO m_IlocDeptDAO;
        private ILocApprovDAO m_ILocApprovDAO;
        private IT3RoomDAO m_IT3DAO;
        private IT2RoomDAO m_IT2DAO;
        private LocationDAO m_locDAO;
        private myVRMSearch m_Search;
        private HardwareFactory m_HardwareFac;
        private UserFactory m_usrFactory;
        private IUserDao m_vrmUserDAO;//FB 2632
        private IStateDAO m_IStateDAO; //FB 2392
        private ICountryDAO m_ICountryDAO;
        private IESPublicRoomDAO m_IESPublicRoomDAO; //FB 2392-WhyGO
        private WorkOrderDAO m_woDAO;//Code added fro Room search
        private InvListDAO m_InvListDAO;//Code added fro Room search
        private InvCategoryDAO m_InvCategoryDAO;//Code added fro Room search
        private IEptDao m_vrmEpt;
        private hardwareDAO m_Hardware;

        List<vrmDeptCustomAttr> custAttrs = null;
        vrmFactory vrmFact = null;

        private IUserDeptDao m_IuserDeptDAO;
        internal OrgData orgInfo;
        private IRoomDAO m_IRoomDAO; //FB 2027
        private ns_SqlHelper.SqlHelper m_roomlayer = null; //FB 2027 DeleteRoom
        private imageFactory vrmImg = null; //FB 2136
        private ISecBadgeDao m_ISecBadgeDao; //FB 2136
        private secBadgeDao m_secBadgeDao; //FB 2136
        internal int PublicRoom = -1;
        private int m_iMaxRecords = 20;

        #region Constructor
        /// <summary>
        /// RoomFactory - Constructor
        /// </summary>
        /// <param name="obj"></param>
        public RoomFactory(ref vrmDataObject obj)
        {
            try
            {
                m_log = obj.log;
                m_configPath = obj.ConfigPath;

                m_confDAO = new conferenceDAO(m_configPath, m_log);
                m_OrgDAO = new orgDAO(m_configPath, m_log);
                m_usrDAO = new userDAO(m_configPath, m_log);
                m_deptDAO = new deptDAO(m_configPath, m_log);
                m_locDAO = new LocationDAO(obj.ConfigPath, obj.log);//FB 2027(GetOldRoom)
                m_generalDAO = new GeneralDAO(m_configPath, m_log);
                m_Hardware = new hardwareDAO(m_configPath, m_log);

                vrmFact = new vrmFactory(ref obj);
                m_Search = new myVRMSearch(obj);//FB 2027(GetOldRoom)
                m_HardwareFac = new HardwareFactory(ref obj);
                m_usrFactory = new UserFactory(ref obj);
                m_vrmRoomDAO = m_locDAO.GetRoomDAO();
                m_IdeptDAO = m_deptDAO.GetDeptDao();
                m_IlocDeptDAO = m_locDAO.GetLocDeptDAO();
                m_ILocApprovDAO = m_locDAO.GetLocApprovDAO();
                m_IT3DAO = m_locDAO.GetT3RoomDAO();
                m_IT2DAO = m_locDAO.GetT2RoomDAO();
                m_IStateDAO = m_generalDAO.GetStateDAO(); //FB 2392
                m_ICountryDAO = m_generalDAO.GetCountryDAO();
                m_IESPublicRoomDAO = m_locDAO.GetESPublicRoomDAO(); //FB 2392-WhyGO
                m_woDAO = new WorkOrderDAO(m_configPath, m_log);//Code added for room search
                m_InvListDAO = m_woDAO.GetInvListDAO();//Code added for room search
                m_InvCategoryDAO = m_woDAO.GetCategoryDAO();//Code added for room search
                m_vrmEpt = m_Hardware.GetEptDao();
                m_IOrgSettingsDAO = m_OrgDAO.GetOrgSettingsDao();
                m_IconfDAO = m_confDAO.GetConferenceDao();
                m_IconfRoom = m_confDAO.GetConfRoomDao();
                m_IconfUser = m_confDAO.GetConfUserDao();
                m_IUserDAO = m_usrDAO.GetUserDao();
                m_IconfAttrDAO = m_confDAO.GetConfAttrDao();
                m_IDeptCustDAO = m_deptDAO.GetDeptCustomAttrDao();
                m_ICustomAttrLangDAO = m_deptDAO.GetCustomLangDao();
                m_IDeptCustOptDAO = m_deptDAO.GetDeptCustomAttrOptionDao();

                m_IuserDeptDAO = m_deptDAO.GetUserDeptDao(); //FB 2027
                m_IRoomDAO = m_locDAO.GetRoomDAO();
                m_IT3DAO = m_locDAO.GetT3RoomDAO();
                m_IT2DAO = m_locDAO.GetT2RoomDAO();
                m_vrmUserDAO = m_usrDAO.GetUserDao();//FB 2632

                m_secBadgeDao = new secBadgeDao(m_configPath, m_log); //FB 2136
                m_ISecBadgeDao = m_secBadgeDao.GetSecImageDao();//FB 2136
                vrmImg = new imageFactory(ref obj); //FB 2136

            }
            catch(Exception ex)
            {
                m_log.Error("RoomFactory Constructor :" + ex.Message);
                throw ex;
            }
        }
        #endregion

        #region GetRoomMonthlyView
        /// <summary>
        /// GetRoomMonthlyView
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetRoomMonthlyView(ref vrmDataObject obj)
        {
            try
            {
                DateTime calDate = DateTime.Now;
                XmlDocument xd = new XmlDocument();

                xd.LoadXml(obj.inXml);
                XmlNode node;
                
                node = xd.SelectSingleNode("//calendarView/date");
                if (node != null)
                    DateTime.TryParse(node.InnerText.Trim(), out calDate);

                DateTime fromDate = new DateTime(calDate.Year, calDate.Month, 1, 0, 0, 0);
                DateTime NextMonth = calDate.AddMonths(1); //FB 2368
                DateTime toDate = new DateTime(calDate.Year, calDate.Month, calDate.AddMonths(1).AddDays(-NextMonth.Day).Day, 23, 59, 59); //FB 2368

                obj.outXml ="<monthlyView>" + GetCalendarView(obj, fromDate, toDate) + "</monthlyView>";
                return true;
            }
            catch (myVRMException myVRMEx)
            {
                m_log.Error("myVRMException GetRoomMonthlyView :" + myVRMEx.Message);
                obj.outXml = myVRMEx.FetchErrorMsg();
                return false;
            }
            catch(Exception ex)
            {
                m_log.Error("GetRoomMonthlyView :" + ex.Message);
                obj.outXml = "";
                return false;
            }
        }
        #endregion

        #region GetRoomWeeklyView
        /// <summary>
        /// GetRoomWeeklyView
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetRoomWeeklyView(ref vrmDataObject obj)
        {
            try
            {
                DateTime fromDate = DateTime.Now;
                DateTime toDate = DateTime.Now;
                DateTime calDate = DateTime.Now;
                XmlDocument xd = new XmlDocument();

                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//calendarView/date");
                if (node != null)
                    DateTime.TryParse(node.InnerText.Trim(), out calDate);

                int thisDay = (int)calDate.DayOfWeek;
                fromDate = calDate.AddDays(-thisDay);
                toDate = fromDate.AddDays(6);
                fromDate = new DateTime(fromDate.Year, fromDate.Month, fromDate.Day, 0, 0, 0);
                toDate = new DateTime(toDate.Year, toDate.Month, toDate.Day, 23, 59, 59);

                obj.outXml = "<weeklyView>" + GetCalendarView(obj, fromDate, toDate) + "</weeklyView>";
                return true;
            }
            catch (myVRMException myVRMEx)
            {
                m_log.Error("myVRMException GetRoomWeeklyView :" + myVRMEx.Message);
                obj.outXml = myVRMEx.FetchErrorMsg();
                return false;
            }
            catch (Exception ex)
            {
                m_log.Error("GetRoomWeeklyView :" + ex.Message);
                obj.outXml = "";
                return false;
            }
        }
        #endregion

        #region GetRoomDailyView
        /// <summary>
        /// GetRoomDailyView
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetRoomDailyView(ref vrmDataObject obj)
        {
            try
            {
                DateTime calDate = DateTime.Now;
                XmlDocument xd = new XmlDocument();

                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//calendarView/date");
                if (node != null)
                    DateTime.TryParse(node.InnerText.Trim(), out calDate);

                DateTime fromDate = new DateTime(calDate.Year, calDate.Month, calDate.Day, 0, 0, 0);
                DateTime toDate = new DateTime(calDate.Year, calDate.Month, calDate.Day, 23, 59, 59);

                obj.outXml = "<dailyView>" + GetCalendarView(obj, fromDate, toDate) + "</dailyView>";
                return true;
            }
            catch (myVRMException myVRMEx)
            {
                m_log.Error("myVRMException GetRoomDailyView :" + myVRMEx.Message);
                obj.outXml = myVRMEx.FetchErrorMsg();
                return false;
            }
            catch (Exception ex)
            {
                m_log.Error("GetRoomDailyView :" + ex.Message);
                obj.outXml = "";
                return false;
            }
        }
        #endregion

        #region GetCalendarView
        /// <summary>
        /// GetCalendarView
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <returns></returns>
        private String GetCalendarView(vrmDataObject obj, DateTime fromDate, DateTime toDate)
        {
            try
            {
                int userID = 11, RoomID = 0, utcEnabled = 0, Cfrist = 1;
                List<ICriterion> CritList = new List<ICriterion>();
                List<vrmConference> confs = new List<vrmConference>();
                timeZoneData tzData = new timeZoneData();
                StringBuilder outXML = new StringBuilder();
                DateTime calDate = DateTime.Now;
                XmlDocument xd = new XmlDocument();
                String temp = "";
                List<vrmConfRoom> confRooms = null;
                ICriterion criterium = null;

                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//calendarView/organizationID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out organizationID);

                if (organizationID < defaultOrgId)
                {
                    myVRMEx = new myVRMException(423);
                    throw myVRMEx;
                }

                node = xd.SelectSingleNode("//calendarView/utcEnabled");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out utcEnabled);

                node = xd.SelectSingleNode("//calendarView/userID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out userID);

                node = xd.SelectSingleNode("//calendarView/room");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out RoomID);

                vrmBaseUser vrmUser = m_IUserDAO.GetByUserId(userID);
                
                timeZone.changeToGMTTime(vrmUser.TimeZone, ref fromDate);
                timeZone.changeToGMTTime(vrmUser.TimeZone, ref toDate); //sysSettings.TimeZone

                fromDate = new DateTime(fromDate.Year, fromDate.Month, fromDate.Day, 0, 0, 0);
                toDate = new DateTime(toDate.Year, toDate.Month, toDate.Day, 23, 59, 59);

                criterium = (Expression.Or(
                             (Expression.And(Expression.Ge("confdate", fromDate), Expression.Le("confdate", toDate))),
                             (Expression.And(Expression.Gt("confEnd", fromDate), Expression.Le("confdate", toDate))))
                            );
                criterium = Expression.And(Expression.Eq("orgId", organizationID), criterium);

                node = xd.SelectSingleNode("//calendarView/isDeletedConf");
                if (node != null)
                    if (node.InnerText.Trim() == "0")
                        criterium = Expression.And(Expression.Eq("deleted", 0), criterium);

                vrmUser user = m_IUserDAO.GetByUserId(userID);
                if (RoomID != 0)
                {
                    CritList.Add(Expression.Eq("roomId", RoomID));
                    m_IconfRoom.addOrderBy(Order.Asc("StartDate"));

                    confRooms = m_IconfRoom.GetByCriteria(CritList);
                    for (int i = 0; i < confRooms.Count; i++)
                    {
                        CritList = new List<ICriterion>();
                        CritList.Add(criterium);
                        CritList.Add(Expression.Eq("confid", confRooms[i].confid));
                        CritList.Add(Expression.Eq("instanceid", confRooms[i].instanceid));
                        confs.AddRange(m_IconfDAO.GetByCriteria(CritList));
                    }
                }
                else
                {
                    CritList = new List<ICriterion>();
                    CritList.Add(criterium);
                    m_IconfDAO.addOrderBy(Order.Asc("confdate"));
                    confs.AddRange(m_IconfDAO.GetByCriteria(CritList));
                }
                OrgData org = m_IOrgSettingsDAO.GetByOrgId(organizationID);
                timeZone.GetTimeZone(user.TimeZone, ref tzData);

                
                outXML.Append("<timezoneName>" + tzData.TimeZone + "</timezoneName>");
                if (org != null)
                {
                    outXML.Append("<systemAvail>");
                    outXML.Append("<open24>" + org.Open24hrs + "</open24>");
                    outXML.Append("<startTime>");
                    outXML.Append("<startHour>" + org.SystemStartTime.ToString("hh") + "</startHour>");
                    outXML.Append("<startMin>" + org.SystemStartTime.ToString("mm") + "</startMin>");
                    outXML.Append("<startSet>" + org.SystemStartTime.ToString("tt") + "</startSet>");
                    outXML.Append("</startTime>");
                    outXML.Append("<endTime>");
                    outXML.Append("<endHour>" + org.SystemEndTime.ToString("hh") + "</endHour>");
                    outXML.Append("<endMin>" + org.SystemEndTime.ToString("mm") + "</endMin>");
                    outXML.Append("<endSet>" + org.SystemEndTime.ToString("tt") + "</endSet>");
                    outXML.Append("</endTime>");
                    outXML.Append("<dayClosed>" + org.Offdays + "</dayClosed>");
                    outXML.Append("</systemAvail>");
                }
                if (confs.Count <= 0)
                {
                    outXML.Append("<days>");
                    outXML.Append("<day>");
                    outXML.Append("<date>" + fromDate.ToString("MM/dd/yyyy") + "</date>");
                    outXML.Append("</day>");
                    outXML.Append("</days>");
                }
                else
                {
                    DateTime setUpTime = DateTime.Now;
                    DateTime tearDownTime = DateTime.Now;
                    DateTime confdate = DateTime.Now;
                    DateTime gmtConfDate = DateTime.Now;
                    for (int i = 0; i < confs.Count; i++)
                    {
                        if (RoomID != 0 || confs[i].isPublic == 1 || confs[i].owner == userID || user.Admin == 2 || user.Admin == 1)
                        {
                            int future = 0, ongoing = 0, ispublic = 0, isPending = 0, isAppr = 0, cnt = 0;
                            double duration = 0.0;

                            confdate = confs[i].confdate;
                            setUpTime = confs[i].SetupTime;
                            tearDownTime = confs[i].TearDownTime;
                            gmtConfDate = confs[i].confdate;

                            if (utcEnabled == 0)
                            {
                                timeZone.userPreferedTime(user.TimeZone, ref setUpTime);
                                timeZone.userPreferedTime(user.TimeZone, ref tearDownTime);
                                timeZone.userPreferedTime(user.TimeZone, ref confdate);
                            }

                            if (RoomID == 0)
                            {
                                confdate = setUpTime;
                                gmtConfDate = confs[i].SetupTime;
                                Int32.TryParse((tearDownTime - setUpTime).TotalMinutes.ToString(), out cnt);
                                confs[i].duration = cnt;
                            }

                            if (confs[i].status != 7 && confs[i].status != 9) //Completed
                            {
                                if (confs[i].status == 0)
                                    future = 1;

                                if (confs[i].status == 5)
                                    ongoing = 1;
                                else
                                {
                                    calDate = DateTime.Now;
                                    timeZone.changeToGMTTime(confs[i].timezone, ref calDate);
                                    if (calDate >= gmtConfDate && confs[i].status == 0)
                                    {
                                        future = 0;

                                        if (confs[i].TearDownTime < calDate)
                                            confs[i].status = 7;
                                        else
                                            ongoing = 1;
                                    }
                                }

                                if (confs[i].status == 0 && confs[i].isPublic == 1)
                                    ispublic = 1;

                                if (confs[i].status == 1)
                                {
                                    isPending = 1;
                                    isAppr = 1;
                                }
                            }

                            if (i == 0 || Cfrist == 1 || (confs[i - 1].confdate != confdate))
                            {
                                if (i != 0 && Cfrist != 1)
                                {
                                    outXML.Append("</conferences>");
                                    outXML.Append("</day>");
                                    outXML.Append("</days>");
                                }

                                outXML.Append("<days>");
                                outXML.Append("<day>");
                                outXML.Append("<date>" + confdate.ToString("MM/dd/yyyy") + "</date>");
                                outXML.Append("<conferences>");
                                Cfrist = 2;
                            }

                            outXML.Append("<conference>");
                            outXML.Append("<confID>" + confs[i].confid + "," + confs[i].instanceid + "</confID>");
                            outXML.Append("<uniqueID>" + confs[i].confnumname + "</uniqueID>");
                            outXML.Append("<confName>" + confs[i].externalname + "</confName>");
                            outXML.Append("<ConferenceType>" + confs[i].conftype + "</ConferenceType>");
                            outXML.Append("<deleted>" + confs[i].deleted + "</deleted>");
                            outXML.Append("<isVIP>" + confs[i].isVIP + "</isVIP>");
                            outXML.Append("<isVMR>" + confs[i].isVMR + "</isVMR>"); //FB 2448
                            outXML.Append("<confPassword>" + confs[i].password + "</confPassword>"); //FB 2622
                            outXML.Append("<icalID>" + confs[i].IcalID + "</icalID>");//FB 2149

                            calDate = confs[i].confEnd;
                            timeZone.userPreferedTime(user.TimeZone, ref calDate);
                            confs[i].confEnd = calDate;

                            if ((confs[i].duration >= 1440 || confs[i].confEnd.Day > confdate.Day) && confs[i].confEnd <= toDate && (toDate - fromDate).TotalMinutes < 1440)
                                Double.TryParse((confs[i].confEnd - fromDate).TotalMinutes.ToString(), out duration);
                            else
                                Double.TryParse(confs[i].duration.ToString(), out duration);

                            outXML.Append("<confDate>" + confdate.ToString("MM/dd/yyyy") + "</confDate>");

                            temp = confdate.ToString("hh:mm tt");
                            if (confdate < fromDate)
                                temp = "00:00 AM";

                            outXML.Append("<confTime>" + temp + "</confTime>");
                           
                            temp = setUpTime.ToString("hh:mm tt");
                            if (setUpTime < fromDate)
                                temp = "00:00 AM";

                            outXML.Append("<setupTime>" + temp + "</setupTime>");

                            temp = tearDownTime.ToString("hh:mm tt");
                            if (tearDownTime < fromDate)
                                temp = "00:00 AM";

                            outXML.Append("<teardownTime>" + temp + "</teardownTime>");

                            temp = "0";
                            for (cnt = 0; cnt < confs[i].ConfUser.Count; cnt++)
                                if (confs[i].ConfUser[cnt].userid == userID)
                                    temp = userID.ToString();
                            vrmUser vnocOperator = m_vrmUserDAO.GetByUserId(confs[i].confVNOC);//FB 2632

                            outXML.Append("<durationMin>" + duration.ToString() + "</durationMin>");
                            outXML.Append("<owner>" + confs[i].owner + "</owner>");
                            outXML.Append("<party>" + temp + "</party>");
                            outXML.Append("<mainLocation>");
                            for (int j = 0; j < confs[i].ConfRoom.Count; j++)
                            {
                                outXML.Append("<location>");
                                outXML.Append("<locationID>" + confs[i].ConfRoom[j].roomId + "</locationID>");
                                outXML.Append("<locationName>" + confs[i].ConfRoom[j].Room.Name + "</locationName>");
                                outXML.Append("</location>");
                            }
                            outXML.Append("</mainLocation>");
                            outXML.Append("<isImmediate>" + ongoing + "</isImmediate>");
                            outXML.Append("<isFuture>" + future + "</isFuture>");
                            outXML.Append("<isPublic>" + ispublic + "</isPublic>");
                            outXML.Append("<isPending>" + isPending + "</isPending>");
                            outXML.Append("<OnSiteAVSupport>" + confs[i].OnSiteAVSupport + "</OnSiteAVSupport>"); //FB 2632
                            outXML.Append("<MeetandGreet>" + confs[i].MeetandGreet + "</MeetandGreet>");
                            outXML.Append("<ConciergeMonitoring>" + confs[i].ConciergeMonitoring + "</ConciergeMonitoring>");
                            if(vnocOperator != null)
                                outXML.Append("<VNOCOperator>" + vnocOperator.FirstName + " " + vnocOperator.LastName + "</VNOCOperator>");
                            
                            outXML.Append("<isApproval>" + isAppr + "</isApproval>");
                            vrmFact.organizationID = organizationID; //FB 2045
                            vrmFact.FetchCustomAttrs(user, confs[i], ref outXML);
                            outXML.Append("</conference>");
                        }
                    }
                    if (Cfrist == 2)
                    {
                        outXML.Append("</conferences>");
                        outXML.Append("</day>");
                        outXML.Append("</days>");
                    }
                }

                return outXML.ToString();
            }
            catch (Exception ex)
            {
                m_log.Error("GetCalendarView" + ex.Message);
                return obj.outXml="";
                throw ex;
            }
        }
        #endregion

        //FB 2027(GetOldRoom) - Start
        #region GetOldRoom
        /// <summary>
        /// GetOldRoom (COM to .Net conversion)
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetOldRoom(ref vrmDataObject obj)
        {
            bool bRet = true;
            int userid = 0, roomid = 0;
            StringBuilder tempXML = new StringBuilder();
            StringBuilder outXml = new StringBuilder();
            List<ICriterion> criterionList = new List<ICriterion>();
            List<ICriterion> criterionList1 = new List<ICriterion>();
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node = null;
                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out userid);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                if (userid <= 0)
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/organizationID");
                string orgid = "";
                if (node != null)
                    orgid = node.InnerText.Trim();
                organizationID = defaultOrgId;

                //FB 2274 Starts
                node = xd.SelectSingleNode("//login/multisiloOrganizationID");
                string multisiloOrgID = "";
                int mutliOrgID = 0;
                if (node != null)
                    multisiloOrgID = node.InnerXml.Trim();
                Int32.TryParse(multisiloOrgID, out mutliOrgID);
                if (mutliOrgID > 11)
                    orgid = multisiloOrgID;
                //FB 2274 Ends

                Int32.TryParse(orgid, out organizationID);
                if (organizationID < defaultOrgId)
                {
                    myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/roomID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out roomid);

                outXml.Append("<room>");
                vrmRoom Loc = m_vrmRoomDAO.GetByRoomId(roomid);
                outXml.Append("<roomID>" + Loc.RoomID.ToString() + "</roomID>");
                outXml.Append("<roomName>" + Loc.Name + "</roomName>");
                Fetch32LvLocations(userid, roomid, 0, organizationID, obj, ref tempXML);
                outXml.Append(tempXML);
                outXml.Append("<floor>" + Loc.RoomFloor + "</floor>");
                outXml.Append("<number>" + Loc.RoomNumber + "</number>");
                outXml.Append("<phone>" + Loc.RoomPhone + "</phone>");
                outXml.Append("<capacity>" + Loc.Capacity + "</capacity>");
                
                outXml.Append("<assistant>");
                outXml.Append("<assistantID>" + Loc.assistant.ToString() + "</assistantID>");
                vrmUser userInfo = m_IUserDAO.GetByUserId(Loc.assistant);
                outXml.Append("<assistantName>" + userInfo.FirstName + " " + userInfo.LastName + "</assistantName>");
                outXml.Append("</assistant>");

                outXml.Append("<dynamicRoomLayout>" + Loc.DynamicRoomLayout + "</dynamicRoomLayout>");
                outXml.Append("<catererFacility>" + Loc.Caterer + "</catererFacility>");
                outXml.Append("<projector>" + Loc.ProjectorAvailable.ToString() + "</projector>");
                outXml.Append("<maxNumConcurrent>" + Loc.MaxPhoneCall.ToString() + "</maxNumConcurrent>");
                outXml.Append("<roomImage>" + Loc.RoomImage + "</roomImage>");

                outXml.Append("<mapFile></mapFile>");
                outXml.Append("<secPassFile></secPassFile>");
                outXml.Append("<miscAttachFile></miscAttachFile>");
                outXml.Append("<endpoint>" + Loc.endpointid.ToString() + "</endpoint>");
                outXml.Append("<video>" + Loc.VideoAvailable.ToString() + "</video>"); 
                outXml.Append("<setupTime>" + Loc.SetupTime.ToString() + "</setupTime>");
                outXml.Append("<teardownTime>" + Loc.TeardownTime.ToString() + "</teardownTime>");
                outXml.Append("<notificationEmails>" + Loc.notifyemails + "</notificationEmails>");
                outXml.Append("<IsVMR>" + Loc.IsVMR + "</IsVMR>"); //FB 2448
                
                outXml.Append("<audioProtocols>");
                List<vrmAudioAlg> audiolist = vrmGen.getAudioAlg();
                for (int i = 0; i < audiolist.Count; i++)
                {
                    outXml.Append("<audioProtocol>");
                    outXml.Append("<audioProtocolID>" + audiolist[i].Id.ToString() + "</audioProtocolID>");
                    outXml.Append("<audioProtocolName>" + audiolist[i].AudioType + "</audioProtocolName>");
                    outXml.Append("</audioProtocol>");
                }
                outXml.Append("</audioProtocols>");

                tempXML = new StringBuilder();
                m_Search.GetVideoSessionList(0, ref tempXML);
                outXml.Append(tempXML);
                outXml.Append("<videoEquipment>");
                outXml.Append("<default>" + Loc.DefaultEquipmentid.ToString() + "</default>");
                List<vrmVideoEquipment> videoList = vrmGen.getVideoEquipment();
                for (int k = 0; k < videoList.Count; k++)
                {
                    outXml.Append("<equipment>");
                    outXml.Append("<videoEquipmentID>" + videoList[k].Id.ToString() + "</videoEquipmentID>");
                    outXml.Append("<videoEquipmentName>" + videoList[k].VEName + "</videoEquipmentName>");
                    outXml.Append("</equipment>");
                }
                outXml.Append("</videoEquipment>");

                tempXML = new StringBuilder();
                FetchDepartmentsForRoom(userid, roomid, ref tempXML);
                m_HardwareFac.OldFetchBridgeList(organizationID, ref tempXML);
                GetRoomApprovers(roomid, ref tempXML);
                outXml.Append(tempXML);

                tempXML = new StringBuilder();
                m_usrFactory.FetchAddressTypeList(obj, ref tempXML);
                //outXml.Append(tempXML); //FB 2594-Fetching all endpoints-Not used.
                //m_HardwareFac.FetchEndpointList(organizationID, ref tempXML);
                //outXml.Append(tempXML);
                outXml.Append("</room>");

                obj.outXml = outXml.ToString();
            }
            catch (myVRMException ex)
            {
                m_log.Error("vrmException", ex);
                obj.outXml = ex.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception ex)
            {
                m_log.Error("sytemException", ex);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

        #region FetchDepartmentsForRoom
        /// <summary>
        /// FetchDepartmentsForRoom (COM to .Net conversion) 
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        public bool FetchDepartmentsForRoom(int userid, int roomid, ref StringBuilder outXml)
        {
            outXml = new StringBuilder();
            List<ICriterion> criterionList1 = new List<ICriterion>();
            List<ICriterion> criterionList2 = new List<ICriterion>();
            try
            {
                OrgData orgdata = m_IOrgSettingsDAO.GetByOrgId(organizationID);
                outXml.Append("<multiDepartment>" + orgdata.MultipleDepartments.ToString() + "</multiDepartment>");
                vrmUser userInfo = m_IUserDAO.GetByUserId(userid);

                outXml.Append("<departments>");
                criterionList1.Add(Expression.Eq("roomId", roomid));
                List<vrmLocDepartment> locdept = m_IlocDeptDAO.GetByCriteria(criterionList1);
                for (int i = 0; i < locdept.Count; i++)
                    outXml.Append("<selected>" + locdept[i].departmentId.ToString() + "</selected>");

                List<vrmDept> depInfo;
                if (userInfo.isSuperAdmin())
                {
                    depInfo = m_IdeptDAO.GetActive();
                    for (int j = 0; j < depInfo.Count; j++)
                    {
                        outXml.Append("<department>");
                        outXml.Append("<id>" + depInfo[j].departmentId.ToString() + "</id>");
                        outXml.Append("<name>" + depInfo[j].departmentName + "</name>");
                        outXml.Append("</department>");
                    }
                }
                else
                {
                    for (int i = 0; i < locdept.Count; i++)
                    {
                        criterionList2 = new List<ICriterion>();
                        criterionList2.Add(Expression.Eq("departmentId", locdept[i].departmentId));
                        criterionList2.Add(Expression.Eq("deleted", 0));
                        depInfo = m_IdeptDAO.GetByCriteria(criterionList2);
                        for (int j = 0; j < depInfo.Count; j++)
                        {
                            outXml.Append("<department>");
                            outXml.Append("<id>" + depInfo[j].departmentId.ToString() + "</id>");
                            outXml.Append("<name>" + depInfo[j].departmentName + "</name>");
                            outXml.Append("</department>");
                        }
                    }
                }
                outXml.Append("</departments>");
            }
            catch (Exception ex)
            {
                m_log.Error("FetchDepartmentsForRoom :" + ex.Message);
                throw ex;
            }
            return true;
        }
        #endregion

        #region GetRoomApprovers
        /// <summary>
        /// GetRoomApprovers (COM to .Net conversion) 
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        public bool GetRoomApprovers(int roomid, ref StringBuilder outXml)
        {
            outXml = new StringBuilder();
            List<ICriterion> criterionList = new List<ICriterion>();
            try
            {
                vrmRoom Loc = m_vrmRoomDAO.GetByRoomId(roomid);
                outXml.Append("<approvers>");
                outXml.Append("<responseMsg>" + Loc.ResponseMessage + "</responseMsg>");
                outXml.Append("<responseTime>" + Loc.ResponseTime.ToString() + "</responseTime>");
                criterionList.Add(Expression.Eq("roomid", roomid));
                IList<vrmLocApprover> locApprList = m_ILocApprovDAO.GetByCriteria(criterionList);
                for(int l=0; l < locApprList.Count; l++)
                {
                    vrmUser userInfo = m_IUserDAO.GetByUserId(locApprList[l].approverid);
                    outXml.Append("<approver>");
                    outXml.Append("<ID>" + locApprList[l].approverid.ToString() + "</ID>");
                    outXml.Append("<firstName>" + userInfo.FirstName + "</firstName>");
                    outXml.Append("<lastName>" + userInfo.LastName + "</lastName>");
                    outXml.Append("</approver>");
                }
                outXml.Append("</approvers>");
                return true;
            }
            catch (Exception ex)
            {
                m_log.Error("GetRoomApprovers :" + ex.Message);
                throw ex;
            }
        }
        #endregion

        #region Fetch32LvLocations
        /// <summary>
        /// Fetch32LvLocations (COM to .Net conversion) 
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        public bool Fetch32LvLocations(int userid, int roomid, int mode, int orgid, vrmDataObject obj, ref StringBuilder locout)
        {
            StringBuilder outXml = new StringBuilder();
            List<ICriterion> critert3 = new List<ICriterion>();
            String stmt = "", stmt1= "";
            outXml.Append("<locationList>");
            try
            {
                OrgData orgInfo = m_IOrgSettingsDAO.GetByOrgId(orgid);
                vrmUser user = m_IUserDAO.GetByUserId(userid);
                vrmRoom Loc = m_vrmRoomDAO.GetByRoomId(roomid);
                outXml.Append("<selected>");
                if (mode == 0)
                    outXml.Append("<level2ID>" + Loc.tier2.ID.ToString() + "</level2ID>");
                else if (mode == 1)
                    outXml.Append("<level2ID>" + roomid + "</level2ID>");
                outXml.Append("</selected>");

                outXml.Append("	<level3List>");
                if ((!user.isSuperAdmin()) && (orgInfo.MultipleDepartments == 1))
                {
                    stmt = "select distinct l3.ID, l3.Name from myVRM.DataLayer.vrmTier3 l3, myVRM.DataLayer.vrmRoom R ";
                    stmt += " where l3.disabled = 0 and l3.ID = R.L3LocationId  and l3.orgId = '" + orgid.ToString() + "'  and R.RoomID in (select crd.roomId from myVRM.DataLayer.vrmLocDepartment crd, myVRM.DataLayer.vrmUserDepartment UD ";
                    stmt += " where crd.departmentId = UD.departmentId and UD.userId = " + userid.ToString() + " )"; //FB 1597
                    stmt += " group by l3.ID, l3.Name";
                }
                else
                {
                    stmt = "select distinct l3.ID, l3.Name from myVRM.DataLayer.vrmTier3 l3 ";
                    stmt += " where l3.disabled = 0  and l3.orgId = '" + orgid.ToString() + "'"; //FB 1597
                }
                stmt += " order by l3.Name ";
                IList tier3 = m_IT3DAO.execQuery(stmt);
                if (tier3.Count > 0)
                {
                    foreach (object[] t3 in tier3)
                    {
                        outXml.Append("<level3>");
                        outXml.Append("<level3ID>" + Convert.ToString(t3[0]) + "</level3ID>");
                        outXml.Append("<level3Name>" + Convert.ToString(t3[1]) + "</level3Name>");
                        outXml.Append("<level2List>");
                        stmt1 = " select ID, Name, Address, Comment from myVRM.DataLayer.vrmTier2 ";
                        stmt1 += "where L3LocationId = " + Convert.ToString(t3[0]) + " and disabled = 0 order by upper(Name)";
                        IList tier2 = m_IT2DAO.execQuery(stmt1);
                        if (tier2.Count > 0)
                        {
                            foreach (object[] t2 in tier2)
                            {
                                outXml.Append("<level2>");
                                outXml.Append("<level2ID>" + Convert.ToString(t2[0]) + "</level2ID>");
                                outXml.Append("<level2Name>" + Convert.ToString(t2[1]) + "</level2Name>");
                                outXml.Append("<level2Address>" + Convert.ToString(t2[2]) + "</level2Address>");
                                outXml.Append("<level2Comment>" + Convert.ToString(t2[3]) + "</level2Comment>");
                                outXml.Append("</level2>");
                            }
                        }
                        outXml.Append("</level2List>");
                        outXml.Append("</level3>");
                    }
                }
                outXml.Append("</level3List>");
                outXml.Append("</locationList>");
                locout.Append(outXml.ToString());
                return true;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                return false;
            }
        }
        #endregion
        //FB 2027(GetOldRoom) - End
		//FB 2027
        #region Fetch3LvTempLocations
        /// <summary>
        /// Fetch3LvTempLocations
        /// </summary>
        /// <param name="lv3list"></param>
        /// <param name="userID"></param>
        public void Fetch3LvTempLocations(int userID, List<int> temprooms, ref StringBuilder outputxml)
        {
            try
            {
                bool isAppend = false;
                StringBuilder str = new StringBuilder();
                List<vrmTier3> tier3List = new List<vrmTier3>();
                m_Search.organizationID = organizationID;
                m_Search.GetRoomTree(userID, ref tier3List);

                outputxml.Append("<level3List>");
                for (int t3 = 0; t3 < tier3List.Count; t3++)
                {
                    isAppend = false;
                    str = new StringBuilder();
                    str.Append("<level3>");
                    str.Append("<level3ID>" + tier3List[t3].ID.ToString() + "</level3ID>");
                    str.Append("<level3Name>" + tier3List[t3].Name + "</level3Name>");
                    str.Append("<level2List>");
                    for (int t2 = 0; t2 < tier3List[t3].tier2.Count; t2++)
                    {
                        vrmTier2 T2 = (vrmTier2)tier3List[t3].tier2[t2];
                        str.Append("<level2>");
                        str.Append("<level2ID>" + T2.ID.ToString() + "</level2ID>");
                        str.Append("<level2Name>" + T2.Name + "</level2Name>");
                        str.Append("<level1List>");

                        for (int t1 = 0; t1 < T2.room.Count; t1++)
                        {
                            vrmRoom room = (vrmRoom)T2.room[t1];
                            if (!temprooms.Contains(room.roomId))
                                continue;

                            str.Append("<level1>");
                            str.Append("<level1ID>" + room.roomId.ToString() + "</level1ID>");
                            str.Append("<level1Name>" + room.Name + "</level1Name>");
                            str.Append("<capacity>" + room.Capacity.ToString() + "</capacity>");
                            str.Append("<projector>" + room.ProjectorAvailable.ToString() + "</projector>");
                            str.Append("<maxNumConcurrent>" + room.MaxPhoneCall.ToString() + "</maxNumConcurrent>");
                            str.Append("<videoAvailable>" + room.VideoAvailable.ToString() + "</videoAvailable>");
                            str.Append("</level1>");

                            isAppend = true;
                        }
                        str.Append("</level1List>");
                        str.Append("</level2>");
                    }
                    str.Append("</level2List>");
                    str.Append("</level3>");

                    if (isAppend)
                        outputxml.Append(str);
                }
                outputxml.Append("</level3List>");
            }
            catch (Exception ex)
            {
                m_log.Error("Fetch3LvLocations :" + ex.Message);
                throw ex;
            }
        }
        #endregion

        #region ManageConfRoom
        /// <summary>
        /// ManageConfRoom (COM to .Net conversion)
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool ManageConfRoom(ref vrmDataObject obj)
        {
            bool bRet = true;
            int userid = 0, mode = 0;
            long ttlRoom = 0, vRoom = 0, nvRoom = 0;
            StringBuilder outXml = new StringBuilder();
            List<ICriterion> criterionList = new List<ICriterion>();
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node = null;
                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out userid);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                if (userid <= 0)
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/organizationID");
                string orgid = "";
                if (node != null)
                    orgid = node.InnerText.Trim();
                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);
                if (organizationID < defaultOrgId)
                {
                    myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                vrmUser user = m_IUserDAO.GetByUserId(userid);
                if (user.isSuperAdmin())
                    mode = 1;
                else
                    mode = 0;

                OrgData orgdata = m_IOrgSettingsDAO.GetByOrgId(organizationID);
                outXml.Append("<manageConfRoom>");
                outXml.Append("<systemAvail>");
                outXml.Append("<open24>" + orgdata.Open24hrs + "</open24>");
                outXml.Append("<startTime>");
                outXml.Append("<startHour>" + orgdata.SystemStartTime.ToString("hh") + "</startHour>");
                outXml.Append("<startMin>" + orgdata.SystemStartTime.ToString("mm") + "</startMin>");
                outXml.Append("<startSet>" + orgdata.SystemStartTime.ToString("tt") + "</startSet>");
                outXml.Append("</startTime>");
                outXml.Append("<endTime>");
                outXml.Append("<endHour>" + orgdata.SystemEndTime.ToString("hh") + "</endHour>");
                outXml.Append("<endMin>" + orgdata.SystemEndTime.ToString("mm") + "</endMin>");
                outXml.Append("<endSet>" + orgdata.SystemEndTime.ToString("tt") + "</endSet>");
                outXml.Append("</endTime>");
                outXml.Append("<dayClosed>" + orgdata.Offdays + "</dayClosed>");
                outXml.Append("</systemAvail>");

                outXml.Append("<locationList>");
                outXml.Append("<selected></selected>");
                outXml.Append("<mode>" + mode + "</mode>");

                m_Search.organizationID = organizationID;
                m_Search.Fetch3LvLocations(ref outXml, userid); //Fetch Rooms
                outXml.Append("</locationList>");

                criterionList.Add(Expression.Not(Expression.Eq("Disabled", 1)));
                criterionList.Add(Expression.Eq("orgId", organizationID));
                ttlRoom = m_vrmRoomDAO.CountByCriteria(criterionList);
                criterionList.Add(Expression.Eq("VideoAvailable", 2));
                vRoom = m_vrmRoomDAO.CountByCriteria(criterionList);
                nvRoom = ttlRoom - vRoom;

                outXml.Append("<totalNumber>" + ttlRoom + "</totalNumber>");
                outXml.Append("<totalVideo>" + vRoom + "</totalVideo>");
                outXml.Append("<totalNonVideo>" + nvRoom + "</totalNonVideo>");
                long licRem = (sysSettings.RoomLimit - ttlRoom);
                outXml.Append("<licensesRemain>" + licRem + "</licensesRemain>");
                outXml.Append("</manageConfRoom>");
                obj.outXml = outXml.ToString();
            }
            catch (myVRMException ex)
            {
                m_log.Error("vrmException", ex);
                obj.outXml = ex.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception ex)
            {
                m_log.Error("sytemException", ex);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

        #region FetchLocations
        /// <summary>
        /// FetchLocations
        /// </summary>
        /// <param name="confid"></param>
        /// <param name="instanceid"></param>
        /// <param name="recurrentmode"></param>
        /// <param name="outputXML"></param>
        /// <returns></returns>
        public bool FetchLocations(int confid, int instanceid, int recurrentmode, ref StringBuilder outXML, ref vrmDataObject obj)
        {
            List<ICriterion> criterionList = new List<ICriterion>();
            try
            {
                criterionList.Add(Expression.Eq("confid", confid));
                criterionList.Add(Expression.Eq("instanceid", instanceid));
                List<vrmConfRoom> confroomids = m_IconfRoom.GetByCriteria(criterionList);

                outXML.Append("<mainLocation>");

                for (int i = 0; i < confroomids.Count; i++)
                {
                    vrmRoom roomids = m_vrmRoomDAO.GetByRoomId(confroomids[i].roomId);

                    outXML.Append("<location>");
                    outXML.Append("<locationID>" + roomids.roomId + "</locationID>");
                    outXML.Append("<locationName>" + roomids.Name + " (" + roomids.tier2.Name + ", " + roomids.tier2.tier3.Name + ")" + "</locationName>");
                    outXML.Append("<locationType>0</locationType>");
                    outXML.Append("<capacity>" + roomids.Capacity + "</capacity>");
                    outXML.Append("<project>" + roomids.ProjectorAvailable + "</project>");
                    outXML.Append("<maxNumConcurrent>" + roomids.MaxPhoneCall + "</maxNumConcurrent>");
                    //FB 2136 - start
                    if (roomids.SecurityImage1Id > 0)
                    {
                        vrmSecurityImage secImg = vrmImg.GetSecImage(roomids.SecurityImage1Id);
                        outXML.Append("<SecurityName>" + roomids.SecurityImage1.ToString() + "</SecurityName>");
                        outXML.Append("<SecurityImage>" + vrmImg.ConvertByteArrToBase64(secImg.BadgeImage).ToString() + "</SecurityImage>");
                        outXML.Append("<SecBadgeTextAxis>" + secImg.TextAxis.ToString() + "</SecBadgeTextAxis>");
                        outXML.Append("<SecBadgePhotoAxis>" + secImg.PhotoAxis.ToString() + "</SecBadgePhotoAxis>");
                        outXML.Append("<SecBadgeBarCodeAxis>" + secImg.BarCodeAxis.ToString() + "</SecBadgeBarCodeAxis>");
                    }
                    else
                    {
                        outXML.Append("<SecurityName></SecurityName>");
                        outXML.Append("<SecurityImage></SecurityImage>");
                        outXML.Append("<SecBadgeTextAxis></SecBadgeTextAxis>");
                        outXML.Append("<SecBadgePhotoAxis></SecBadgePhotoAxis>");
                        outXML.Append("<SecBadgeBarCodeAxis></SecBadgeBarCodeAxis>");
                    }
                    //FB 2136 Ends                    
                    outXML.Append("</location>");
                }
                outXML.Append("</mainLocation>");

            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                return false;
            }
            return true;
        }
        #endregion

		#region GetRoomArray

        public void GetRoomArray(Int32 userid, Int32 organizationID, ref ArrayList roomList, Boolean bHasDepartment, ref String rmStr, int mediatype, int serviceType, ArrayList selrooms) //FB 2089, FB 2219
        {
            Boolean isSuperAdmin = false;
            Boolean singleDeptMode = true;
            try
            {
                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                vrmUser usr = new vrmUser();
                usr = m_IUserDAO.GetByUserId(userid);

                if (orgInfo.MultipleDepartments == 1)
                {
                    singleDeptMode = false;

                    if (usr.isSuperAdmin())
                        isSuperAdmin = true;
                    else
                        isSuperAdmin = false;
                }

                String roomId = "";

                List<ICriterion> criterionList = new List<ICriterion>();

                if (!usr.isSuperAdmin() && orgInfo.MultipleDepartments == 1)
                {
                    ArrayList rmIDList = new ArrayList();
                    if (bHasDepartment)
                    {
                        rmIDList = new ArrayList();
                        List<vrmLocDepartment> locDeptList = m_Search.GetLocVsUsrDepartmentList(usr);

                        foreach (vrmLocDepartment locDept in locDeptList)
                            rmIDList.Add(locDept.roomId);

                        criterionList.Add(Expression.Eq("Disabled", 0));
                        criterionList.Add(Expression.In("roomId", rmIDList));
                    }
                    else
                    {

                        List<vrmLocDepartment> locDeptList = m_IlocDeptDAO.GetByCriteria(criterionList);
                        rmIDList = new ArrayList();
                        String deptid = "";
                        foreach (vrmLocDepartment vrmloc in locDeptList)
                            rmIDList.Add(vrmloc.roomId);

                        criterionList.Add(Expression.Eq("Disabled", 0));
                        criterionList.Add(Expression.Eq("orgId", organizationID));
                        criterionList.Add(Expression.Not(Expression.In("roomId", rmIDList)));
                    }
                }
                //Code moved for FB 2038 - Start
                //FB 2089 - IPAD requirement ... start
                if (mediatype >= 0)
                {
                    criterionList.Add(Expression.Eq("VideoAvailable", mediatype));
                }
                //FB 2089 - IPAD requirement ... end

                //FB 2219 start
                if (serviceType > 0)
                {
                    if (!selrooms[0].Equals(""))
                        criterionList.Add(Expression.Or(Expression.Eq("ServiceType", serviceType), Expression.In("roomId", selrooms)));
                    else
                        criterionList.Add(Expression.Lt("ServiceType", serviceType)); //FB 2411
                }
                //FB 2219 end
                //Code moved for FB 2038 - End

                //FB 2392
                if (PublicRoom > 0)
                    criterionList.Add(Expression.Eq("isPublic", 1));
                else if (PublicRoom < 0)
                    criterionList.Add(Expression.Eq("isPublic", 0));

                List<vrmRoom> roomChkList = m_IRoomDAO.GetByCriteria(criterionList);

                foreach (vrmRoom vrmrm in roomChkList)
                {
                    if (rmStr == "")
                        rmStr = vrmrm.roomId.ToString();
                    else
                        rmStr = rmStr + "," + vrmrm.roomId.ToString();

                    roomList.Add(vrmrm.roomId);
                }
            }
            catch (Exception ex)
            {
                m_log.Error("GetRoomArray: " + ex.Message);
            }
        }

        #endregion

        #region Fetch3LvAvailableLocations

        public void Fetch3LvAvailableLocations(Int32 organizationID, Int32 mode, String sqlStatement,
            Int32 userid, Boolean bHasDepartment, ref StringBuilder outxml, ref Int32 confType, int mediatype, int serviceType, String selectedloc)//FB 2170 //FB 2089, FB 2219
        {
            Boolean roomAvailable = false;
            try
            {
                outxml.Append("<locationList>");

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                if (orgInfo.MultipleDepartments == 0)
                    userid = 11;

                outxml.Append("<selected>");

                String stmt = "";

                stmt = " select cr.roomId from myVRM.DataLayer.vrmRoom cr, myVRM.DataLayer.vrmUser u where u.RoomID = cr.roomId";
                stmt += " and u.userid = " + userid.ToString() + " and cr.orgId = '" + organizationID.ToString() + "'"; //Organization Module

                IList rooms = m_IconfRoom.execQuery(stmt);
                if (rooms.Count > 0)
                {
                    //FB 2426 Start

                    //foreach (object[] obj in rooms)
                    //    outxml.Append("<level1ID>" + Convert.ToString(obj[0]) + "</level1ID>");

                    for (int j = 1; j < rooms.Count + 1; j++)
                    {
                        int apprid = 0;
                        Int32.TryParse(rooms[j - 1].ToString(), out apprid);
                        outxml.Append("<level1ID>" + apprid + "</level1ID>");

                    }

                    //FB 2426 End
                }
                outxml.Append("</selected>");

                outxml.Append("<level3List>");

                m_IT3DAO.addOrderBy(Order.Asc("Name"));
                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("orgId", organizationID));
                criterionList.Add(Expression.Eq("disabled", 0));
                List<vrmTier3> tier3List = m_IT3DAO.GetByCriteria(criterionList);
                m_IT3DAO.clearOrderBy();

                for (Int32 t1 = 0; t1 < tier3List.Count; t1++)
                {
                    bool bfirst;
                    bool bfinish;
                    bool bSecond;
                    bool bfinish2;

                    bfirst = true;
                    bfinish = false;

                    m_IT2DAO.addOrderBy(Order.Asc("Name"));
                    criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("L3LocationId", tier3List[t1].ID));
                    List<vrmTier2> tier2List = m_IT2DAO.GetByCriteria(criterionList);
                    m_IT2DAO.clearOrderBy();

                    for (Int32 t2 = 0; t2 < tier2List.Count; t2++)
                    {
                        String stmt2 = "";
                        if (mode == 1)
                        {
                            vrmUser usr = new vrmUser();
                            usr = m_IUserDAO.GetByUserId(userid);

                            if (!usr.isSuperAdmin())
                            {
                                stmt2 = " select roomId, Name, Capacity, ProjectorAvailable, MaxPhoneCall, VideoAvailable, isTelepresence, DedicatedVideo, isPublic ";//FB 2170 FB 2334 //FB 2392- WhyGO
                                stmt2 += " from myVRM.DataLayer.vrmRoom where L2LocationId = " + tier2List[t2].ID;
                                stmt2 += " AND Disabled = 0 ";

                                //FB 2089 - IPAD requirement ... start
                                if(mediatype >= 0)
                                    stmt2 += " AND VideoAvailable='"+ mediatype +"' ";
                                //FB 2089 - IPAD requirement ... end
                                //FB 2411 Start
                                string serivetypein = "0";
                                if (serviceType == 3)
                                    serivetypein = "1,2,3";
                                else if (serviceType == 2)
                                    serivetypein = "1,2";
                                else if (serviceType == 1)
                                    serivetypein = "1";


                                if (serviceType > 0)//FB 2219
                                {
                                    if (selectedloc.Trim() == "")
                                        stmt2 += " AND ServiceType in (" + serivetypein + ") ";
                                    else
                                        stmt2 += " AND (ServiceType in (" + serivetypein + ") OR roomId IN  (" + selectedloc.Trim() + "))";
                                }
                                //FB 2411 End

                                if (PublicRoom > 0)//FB 2392
                                    stmt2 += " AND isPublic = 1";
                                else if ( PublicRoom < 0 )
                                    stmt2 += " AND isPublic = 0";


                                if (sqlStatement != "")
                                    stmt2 += sqlStatement;
                                stmt2 += " and L3LocationId=" + tier3List[t1].ID;

                                if (bHasDepartment)
                                {
                                    stmt2 += " and roomId in (select crd.roomId from myVRM.DataLayer.vrmLocDepartment  crd, myVRM.DataLayer.vrmUserDepartment UD";
                                    stmt2 += " where crd.departmentId = UD.departmentId and UD.userId = " + userid + " )";
                                }
                                else
                                {
                                    stmt2 += " and orgId='" + organizationID + "' and ";
                                    stmt2 += " roomId not in (select roomId from myVRM.DataLayer.vrmLocDepartment) and roomId <> 11";
                                }
                                stmt2 += "	ORDER BY Name ";
                            }
                            else
                            {
                                stmt2 = " select roomId, Name, Capacity, ProjectorAvailable, MaxPhoneCall, VideoAvailable, isTelepresence, DedicatedVideo, isPublic ";//FB 2170 FB 2334 //FB 2392- WhyGO
                                stmt2 += " from myVRM.DataLayer.vrmRoom where L2LocationId = " + tier2List[t2].ID + " and L3LocationId=" + tier3List[t1].ID;
                                stmt2 += " AND Disabled = 0 ";

                                //FB 2089 - IPAD requirement ... start
                                if (mediatype >= 0)
                                    stmt2 += " AND VideoAvailable='" + mediatype + "' ";
                                //FB 2089 - IPAD requirement ... end
                                //FB 2411 Start
                                string serivetypein = "0";
                                if (serviceType == 3)
                                    serivetypein = "1,2,3";
                                else if (serviceType == 2)
                                    serivetypein = "1,2";
                                else if (serviceType == 1)
                                    serivetypein = "1";

                                if (serviceType > 0)//FB 2219
                                {
                                    if (selectedloc.Trim() == "")
                                        stmt2 += " AND ServiceType in (" + serivetypein + ") "; 
                                    else
                                        stmt2 += " AND (ServiceType in (" + serivetypein + ") OR roomId IN  (" + selectedloc.Trim() + "))";
                                }
                                //FB 2411 End

                                if (PublicRoom > 0)//FB 2392
                                    stmt2 += " AND isPublic = 1";
                                else if (PublicRoom < 0)
                                    stmt2 += " AND isPublic = 0";

                                if (sqlStatement != "")
                                    stmt2 += sqlStatement;
                                stmt2 += " ORDER BY Name";
                            }
                        }
                        else
                        {
                            stmt2 = "select roomId, Name, Capacity, ProjectorAvailable, MaxPhoneCall, VideoAvailable, isTelepresence, DedicatedVideo, isPublic ";//FB 2170 FB 2334 2392
                            stmt2 += "from myVRM.DataLayer.vrmRoom where L2LocationId = " + tier2List[t2].ID + " and L3LocationId=" + tier3List[t1].tier2;
                            stmt2 += " AND Disabled = 0 ";

                            //FB 2089 - IPAD requirement ... start
                            if (mediatype >= 0)
                                stmt2 += " AND VideoAvailable='" + mediatype + "' ";
                            //FB 2089 - IPAD requirement ... end

                            //FB 2411 Start
                            string serivetypein = "0";
                            if (serviceType == 3)
                                serivetypein = "1,2,3";
                            else if (serviceType == 2)
                                serivetypein = "1,2";
                            else if (serviceType == 1)
                                serivetypein = "1";

                            if (serviceType > 0)//FB 2219
                            {
                                if (selectedloc.Trim() == "")
                                    stmt2 += " AND ServiceType in (" + serivetypein + ") "; 
                                else
                                    stmt2 += " AND (ServiceType in (" + serivetypein + ") OR roomId IN  (" + selectedloc.Trim() + "))"; 
                            }
                            //FB 2411 End

                            if (PublicRoom > 0)//FB 2392
                                stmt2 += " AND isPublic = 1";
                            else if (PublicRoom < 0)
                                stmt2 += " AND isPublic = 0";

                            if (sqlStatement != "")
                                stmt2 += sqlStatement;
                        }

                        bSecond = true;
                        bfirst = true;
                        bfinish2 = false;
                        bfinish = false;

                        rooms = m_IconfRoom.execQuery(stmt2);
                        if (rooms.Count > 0)
                        {
                            foreach (object[] obj in rooms)
                            {
                                if (bfirst)
                                {
                                    outxml.Append("<level3>");
                                    outxml.Append(" <level3ID>" + tier3List[t1].ID + "</level3ID>");
                                    outxml.Append(" <level3Name>" + tier3List[t1].Name + "</level3Name>");
                                    outxml.Append(" <level2List>");

                                    bfirst = false;
                                    bfinish = true;
                                }
                                if (bSecond)
                                {
                                    outxml.Append(" <level2>");
                                    outxml.Append(" <level2ID>" + tier2List[t2].ID + "</level2ID>");
                                    outxml.Append(" <level2Name>" + tier2List[t2].Name + "</level2Name>");
                                    outxml.Append("<level1List>");

                                    bSecond = false;
                                    bfinish2 = true;
                                }
                                String istelepresence = ""; //FB 2170
                                String l1id = Convert.ToString(obj[0]);
                                String l1name = Convert.ToString(obj[1]);
                                String capacity = Convert.ToString(obj[2]);
                                String projector = Convert.ToString(obj[3]);
                                String maxNumConcurrent = Convert.ToString(obj[4]);
                                int iVideoAvailable = Convert.ToInt32(obj[5]);
                                int isPublic = Convert.ToInt32(obj[8]); //FB 2392- WhyGO
                                //FB 2170 Start
                                if (obj[6] == null)
                                    istelepresence = "0";
                                else
                                    istelepresence = obj[6].ToString();
                                //FB 2170 End
                                String Dedvideo = Convert.ToString(obj[7]); ;//FB 2334
                                roomAvailable = true;

                                if (orgInfo.TelepresenceFilter <= 0 && confType == vrmConfType.RooomOnly && istelepresence == "1")//FB 2170
                                    continue;
                                //FB 2334 Start
                                if (orgInfo.DedicatedVideo <= 0 && confType == vrmConfType.RooomOnly && Dedvideo == "1")
                                {
                                    if(l1id != selectedloc.Trim())
                                    {
                                        continue;
                                    }
                                }
                                //FB 2334 End
                                outxml.Append("<level1>");
                                outxml.Append("<level1ID>" + l1id + "</level1ID>");
                                outxml.Append("<level1Name>" + l1name + "</level1Name>");
                                outxml.Append("<capacity>" + capacity + "</capacity>");
                                outxml.Append("<projector>" + projector + "</projector>");
                                outxml.Append("<maxNumConcurrent>" + maxNumConcurrent + "</maxNumConcurrent>");

                                String videoAvailable = "";
                                if (iVideoAvailable == 2)
                                    videoAvailable = "1"; //video available
                                else
                                    videoAvailable = "0"; //no video available

                                outxml.Append("<videoAvailable>" + videoAvailable + "</videoAvailable>");
                                outxml.Append("<isPublic>" + isPublic + "</isPublic>");//FB 2392
                                //FB 2038 start
                                stmt2 = "select approverid from myVRM.DataLayer.vrmLocApprover where roomid = " + l1id;
                                IList appr = m_IconfRoom.execQuery(stmt2);
                                
                                if (appr.Count > 0)
                                    outxml.Append("<Approval>1</Approval>");
                                else
                                    outxml.Append("<Approval>0</Approval>");

                                outxml.Append("<Approvers>");
                                for (int j = 1; j < appr.Count + 1; j++)
                                {
                                    int apprid = 0;
                                    Int32.TryParse(appr[j - 1].ToString(), out apprid);

                                    vrmUser userInfo = m_IUserDAO.GetByUserId(apprid);
                                    outxml.Append("<Approver" + j + "ID>" + apprid + "</Approver" + j + "ID>");
                                    outxml.Append("<Approver" + j + "Name>" + userInfo.FirstName + " " + userInfo.LastName + "</Approver" + j + "Name>");
                                }
                                for (int j = appr.Count + 1; j <= 3; j++)
                                {
                                    outxml.Append("<Approver" + j + "ID></Approver" + j + "ID>");
                                    outxml.Append("<Approver" + j + "Name></Approver" + j + "Name>");
                                }
                                outxml.Append("</Approvers>");
                                //FB 2038 End
                                outxml.Append("</level1>");
                            }
                            if (bfinish2)
                            {
                                outxml.Append("</level1List>");
                                outxml.Append("</level2>");
                            }
                        }
                        if (bfinish)
                        {
                            outxml.Append("</level2List>");
                            outxml.Append("</level3>");
                        }
                    }
                }
                outxml.Append("</level3List>");
                outxml.Append("</locationList>");
            }
            catch (Exception ex)
            {
                m_log.Error("Fetch3LvAvailableLocations :" + ex.Message);
            }
        }

        #endregion

        #region Get Last Modified Rooms
        /// <summary>
        /// FB 2149
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetLastModifiedRoom(ref vrmDataObject obj)
        {
            IList rooms = null;
            String rmIDs = "";
            XmlDocument xd = null;
            XmlNode node = null;
            string userID = "";
            int userUID = 0;
            string orgid = "";
            string dateStr = "";
            DateTime modifiedDate = DateTime.Now;
            string timeZne = "";
            int timeZneId = -1;
            string stmt = "";
            vrmBaseUser vrmUser = null;
            string isUTC = "";
            StringBuilder str = null;
            try
            {
                xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                

                node = xd.SelectSingleNode("//login/userID");
                userID = node.InnerXml.Trim();

                if (userID == "")
                    userID = "11";

                Int32.TryParse(userID,out userUID);

                node = xd.SelectSingleNode("//login/organizationID"); 
                if (node != null)
                    orgid = node.InnerXml.Trim();

                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);

                node = xd.SelectSingleNode("//login/modifiedDate"); 
                if (node != null)
                    dateStr = node.InnerXml.Trim();
                if (dateStr != "")
                    DateTime.TryParse(dateStr, out modifiedDate);

                node = xd.SelectSingleNode("//login/utcEnabled");
                if (node != null)
                    isUTC = node.InnerXml.Trim();

                if (isUTC != "1")
                {


                    node = xd.SelectSingleNode("//login/timeZone");
                    if (node != null)
                        timeZne = node.InnerXml.Trim();

                    Int32.TryParse(timeZne, out timeZneId);

                    if (timeZneId <= 0)
                    {
                        vrmUser = m_IUserDAO.GetByUserId(userUID);
                        timeZneId = vrmUser.TimeZone;
                    }

                    try
                    {
                        timeZone.changeToGMTTime(timeZneId, ref modifiedDate);

                    }
                    catch (Exception lclEX)
                    {
                        modifiedDate = DateTime.MinValue;
                       
                    }
                }
                if (modifiedDate == DateTime.MinValue)
                {
                    modifiedDate = DateTime.Now;
                    timeZone.changeToGMTTime(sysSettings.TimeZone, ref modifiedDate);
                }


                stmt = " select roomId ";
                stmt += " from myVRM.DataLayer.vrmRoom where Lastmodifieddate >= '" +modifiedDate.ToString() + "' and orgId = "+ organizationID.ToString();
                

                rooms = m_IRoomDAO.execQuery(stmt);
                if (str == null)
                    str = new StringBuilder();
                str.Append("<level1>");
                for (int i = 0; i < rooms.Count; i++)
                    str.Append("<level1ID>" + rooms[i].ToString() + "</level1ID>");

                str.Append("</level1>");


                

                obj.outXml = "<login>"+ str.ToString() +"</login>";
                return true;
            }
            catch (Exception ex)
            {
                m_log.Error("sytemException in GetRoomProfile", ex);
                return false;
            }
        }
        #endregion

        //FB 2027 DeleteRoom,ActiveRoom - Start
        #region DeleteRoom
        /// <summary>
        /// DeleteRoom (COM to .Net conversion)
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool DeleteRoom(ref vrmDataObject obj)
        {
            bool bRet = true;
            int userid = 0, roomid = 0, errid = 0;
            String sAddon = "_Del_";
            StringBuilder outXml = new StringBuilder();
            List<ICriterion> criterionList = new List<ICriterion>();
            try
            {
                List<vrmRoom> locRoom = new List<vrmRoom>();
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node = null;
                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out userid);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                if (userid <= 0)
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/organizationID");
                string orgid = "";
                if (node != null)
                    orgid = node.InnerText.Trim();
                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);
                if (organizationID < defaultOrgId)
                {
                    myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/roomID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out roomid);

                if (CanDeleteRoom(roomid, ref errid))
                {
                    criterionList.Add(Expression.Eq("roomId", roomid));
                    locRoom = m_vrmRoomDAO.GetByCriteria(criterionList, true);
                    if (locRoom.Count > 0)
                    {
                        if (locRoom[0].OwnerID > 0)
                            locRoom[0].Name = locRoom[0].Name + sAddon + locRoom[0].roomId;

                        locRoom[0].Disabled = 1;
                        m_vrmRoomDAO.Update(locRoom[0]);
                    }
                }
                else
                {
                    myVRMEx = new myVRMException(errid);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
            }
            catch (myVRMException ex)
            {
                m_log.Error("vrmException", ex);
                obj.outXml = ex.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception ex)
            {
                m_log.Error("sytemException", ex);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }

        private bool CanDeleteRoom(int roomid, ref int errid)
        {
            try
            {
                string hql;
                DataSet ds = null;

                if (m_roomlayer == null)
                    m_roomlayer = new ns_SqlHelper.SqlHelper(m_configPath);

                hql = "select * from conf_conference_d b inner join conf_room_d a on a.confid = b.confid";
                hql += " and a.roomid=" + roomid + " and b.deleted =0 and ((getutcdate() between b.confdate and";
                hql += " dateAdd(minute,b.duration,b.confdate)) or b.confdate > getutcdate() )"; //FB 2494 [getdate() as getutcdate(), and b.status not in (7)] 

                if (hql != "")
                    ds = m_roomlayer.ExecuteDataSet(hql);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    errid = 550; //FB 2369
                    return false;
                }
                hql = " select * from tmp_list_d a inner join tmp_room_d b on a.tmpid=b.tmpid and b.roomid=" + roomid + " and a.deleted =0 ";
                if (hql != "")
                    ds = m_roomlayer.ExecuteDataSet(hql);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    errid = 545; //FB 2164
                    return false;
                }
                hql = " select * from usr_List_d where PreferedRoom like '" + roomid + "'";
                hql += "or PreferedRoom like '" + roomid + ",%'or PreferedRoom like '%," + roomid + ",%'or PreferedRoom like '%," + roomid + "'";
                if (hql != "")
                    ds = m_roomlayer.ExecuteDataSet(hql);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    errid = 538;
                    return false;
                }
                hql = " select * from usr_templates_D where deleted =0 and location= '" + roomid +"'"; //FB 2272
                if (hql != "")
                    ds = m_roomlayer.ExecuteDataSet(hql);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    errid = 539;
                    return false;
                }
                hql = " select * from Inv_Category_D a inner join inv_room_d b";
                hql += " on a.id=b.categoryid and b.locationid=" + roomid + " and a.deleted =0 ";
                if (hql != "")
                    ds = m_roomlayer.ExecuteDataSet(hql);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    errid = 540;
                    return false;
                }
                hql = "select * from usr_searchtemplates_d where template ";
                hql += " like '%<Selected>" + roomid + "</Selected>%' ";
                if (hql != "")
                    ds = m_roomlayer.ExecuteDataSet(hql);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    errid = 541;
                    return false;
                }
                return true;

            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
        }

        #endregion

        #region ActiveRoom
        /// <summary>
        /// ActiveRoom (COM to .Net conversion)
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool ActiveRoom(ref vrmDataObject obj)
        {
            bool bRet = true;
            int userid = 0, roomid = 0, errid = 0;
            StringBuilder outXml = new StringBuilder();
            List<ICriterion> criterionList = new List<ICriterion>();
            List<ICriterion> criterionList2 = new List<ICriterion>();
            List<ICriterion> criterionList3 = new List<ICriterion>();
            List<ICriterion> criterionList4 = new List<ICriterion>();//FB 2586
            List<ICriterion> Roomlist = new List<ICriterion>();
            try
            {
                List<vrmRoom> locRoom = new List<vrmRoom>();
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node = null;
                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out userid);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                if (userid <= 0)
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/organizationID");
                string orgid = "";
                if (node != null)
                    orgid = node.InnerText.Trim();
                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                if (organizationID < defaultOrgId)
                {
                    myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/roomID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out roomid);

                if (roomid != 0)
                {
                    criterionList.Add(Expression.Eq("Disabled", 0));
                    criterionList.Add(Expression.Eq("VideoAvailable", 2));
                    criterionList.Add(Expression.Eq("orgId", organizationID));
                    criterionList.Add(Expression.Eq("IsVMR", 0));//FB 2586
                    criterionList.Add(Expression.Eq("Extroom", 0));//FB 2586
                    criterionList.Add(Expression.Eq("isPublic", 0)); //FB 2594
                    List<vrmRoom> checkVRoomCount = m_IRoomDAO.GetByCriteria(criterionList);
                    int roomVCount = checkVRoomCount.Count;
                    criterionList2.Add(Expression.Eq("Disabled", 0));
                    criterionList2.Add(Expression.Lt("VideoAvailable", 2));
					criterionList2.Add(Expression.Eq("orgId", organizationID));
                    criterionList2.Add(Expression.Eq("IsVMR", 0));//FB 2586
                    criterionList2.Add(Expression.Eq("isPublic", 0)); //FB 2594
                    List<vrmRoom> checkNVRoomCount = m_IRoomDAO.GetByCriteria(criterionList2);
                    int roomNVCount = checkNVRoomCount.Count;
                    //FB 2586 Start
                    criterionList4.Add(Expression.Eq("Disabled", 0));
                    criterionList4.Add(Expression.Eq("VideoAvailable", 2));
                    criterionList4.Add(Expression.Eq("orgId", organizationID));
                    criterionList4.Add(Expression.Eq("IsVMR", 1));
                    List<vrmRoom> checkVMRRoomCount = m_IRoomDAO.GetByCriteria(criterionList4);
                    int roomVMRCount = checkVMRRoomCount.Count;
                    //FB 2586 End
                    int maxVRooms = orgInfo.MaxVideoRooms;
                    int maxNVRooms = orgInfo.MaxNonVideoRooms;
                    int maxVMRRms = orgInfo.MaxVMRRooms;//FB 2586
                    vrmRoom room = m_IRoomDAO.GetByRoomId(roomid);
                    int videoAvail = room.VideoAvailable;
                    int vmr = room.IsVMR;//FB 2586
                    int guestroom = room.Extroom;//FB 2586
                    roomVCount += 1;
                    roomNVCount += 1;
                    roomVMRCount += 1;//FB 2586 
                    int canActivate = 0;
                    if (videoAvail == 2 && vmr ==0) //FB 2586
                    {
                        if (roomVCount <= maxVRooms)
                        {
                            canActivate = 1;
                        }
                        else
                        {
                            myVRMEx = new myVRMException(455);
                            obj.outXml = myVRMEx.FetchErrorMsg();
                            return false;
                        }
                    }
                    //FB 2586 Start
                    else if (videoAvail == 2 && vmr == 1)
                    {
                        if (roomVMRCount <= maxVMRRms)
                        {
                            canActivate = 1;
                        }
                        else
                        {
                            myVRMEx = new myVRMException(655);
                            obj.outXml = myVRMEx.FetchErrorMsg();
                            return false;
                        }
                    }
                    //FB 2586 End
                    else
                    {
                        if (roomNVCount <= maxNVRooms)
                        {
                            canActivate = 1;
                        }
                        else
                        {
                            myVRMEx = new myVRMException(456);
                            obj.outXml = myVRMEx.FetchErrorMsg();
                            return false;
                        }
                    }
                    if (canActivate == 1)
                    {
                        Roomlist.Add(Expression.Eq("roomId", roomid));
                        List<vrmRoom> Actroom = m_IRoomDAO.GetByCriteria(Roomlist, true);
                        Actroom[0].disabled = 0;
                        m_IRoomDAO.Update(Actroom[0]);
                        criterionList3.Add(Expression.Eq("userid", Actroom[0].assistant));
                        List<vrmUser> usercnt = m_IUserDAO.GetByCriteria(criterionList3);
                        if (usercnt.Count == 0)
                        {
                            outXml.Append("<success><AssistantInchargeName>" + usercnt[0].FirstName + "</AssistantInchargeName>");
                            outXml.Append("<UserStatus>I</UserStatus></success>");
                        }
                    }
                    obj.outXml = outXml.ToString();
                }
                else
                {
                    myVRMEx = new myVRMException(252);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
            }
            catch (myVRMException ex)
            {
                m_log.Error("vrmException", ex);
                obj.outXml = ex.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception ex)
            {
                m_log.Error("sytemException", ex);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }

        #endregion
        //FB 2027 DeleteRoom,ActiveRoom - End
        #region GetServiceType
        /// <summary>
        /// GetServiceType - FB 2219
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetServiceType(ref vrmDataObject obj)
        {
            bool bRet = true;
            StringBuilder OutXml = new StringBuilder();
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//GetServiceType/UserID");
                string UserID = node.InnerXml.Trim();

                List<vrmServiceType> st_List = vrmGen.getServiceTypes();
                OutXml.Append("<GetServiceType>");
                foreach (vrmServiceType st in st_List)
                {
                    OutXml.Append("<ServiceType>");
                    OutXml.Append("<ID>" + st.Id.ToString() + "</ID>");
                    OutXml.Append("<Name>" + st.ServiceType + "</Name>");
                    OutXml.Append("</ServiceType>");
                }
                OutXml.Append("</GetServiceType>");
                obj.outXml = OutXml.ToString();
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

        //FB 2361
        #region GetRoomDetails 
        /// <summary>
        /// GetRoomDetails
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetRoomDetails(ref vrmDataObject obj)
        {
            bool bRet = true;
            StringBuilder OutXml = new StringBuilder();
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//GetRoomDetails/UserID");
                string UserID = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//GetRoomDetails/EndPointID");
                string eptID = node.InnerXml.Trim();

                List<ICriterion> criterionLst = new List<ICriterion>();
                if(eptID != "")
                    criterionLst.Add(Expression.Eq("endpointid", Convert.ToInt32(eptID)));

                List<vrmRoom> rmList = m_IRoomDAO.GetByCriteria(criterionLst);

                OutXml.Append("<GetRoomDetails>");
                foreach (vrmRoom rm in rmList)
                {
                    OutXml.Append("<Room>");
                    OutXml.Append("<ID>" + rm.roomId + "</ID>");
                    OutXml.Append("<Name>" + rm.Name + "</Name>");
                    OutXml.Append("</Room>");
                }
                OutXml.Append("</GetRoomDetails>");

                obj.outXml = OutXml.ToString();
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

        #region GetRoomQueue
        /// <summary>
        /// GetRoomQueue
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetRoomQueue(ref vrmDataObject obj)
        {
            bool bRet = true;
            StringBuilder OutXml = new StringBuilder();
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//GetRoomQueue/organizationID");
                string orgid = "";
                if (node != null)
                    orgid = node.InnerText.Trim();
                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                if (organizationID < defaultOrgId)
                {
                    myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                List<ICriterion> criterionLst = new List<ICriterion>();
                if (orgid != "")
                    criterionLst.Add(Expression.Eq("Disabled", 0));
                    criterionLst.Add(Expression.Eq("orgId", organizationID));
                    criterionLst.Add(Expression.Not(Expression.Eq("RoomQueue", ""))); 
                    

                List<vrmRoom> rmList = m_IRoomDAO.GetByCriteria(criterionLst);

                OutXml.Append("<GetRoomQueue>");
                OutXml.Append("<Roomemails>");
                foreach (vrmRoom rm in rmList)
                {
                    OutXml.Append(rm.RoomQueue +";");
                }
                OutXml.Append("</Roomemails>");
                OutXml.Append("</GetRoomQueue>");

                obj.outXml = OutXml.ToString();
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

        //FB 2426 Start

        #region SetGuesttoNormalRoom
        /// <summary>
        /// SetGuesttoNormalRoom
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SetGuesttoNormalRoom(ref vrmDataObject obj)
        {
            int GuestRoomID = 0;
            int userid = 0;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                List<ICriterion> criterionList = new List<ICriterion>();
                List<ICriterion> Licencechk = new List<ICriterion>();
                List<ICriterion> confroom = new List<ICriterion>();
                List<ICriterion> criterionLst = new List<ICriterion>();
                XmlNode node;
                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out userid);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                if (userid <= 0)
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/organizationID");
                string orgid = "";
                if (node != null)
                    orgid = node.InnerText.Trim();
                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                if (organizationID < defaultOrgId)
                {
                    myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/roomID");
                string roomID = node.InnerXml.Trim();
                Int32.TryParse(roomID, out GuestRoomID);

                criterionList.Add(Expression.Eq("roomId", GuestRoomID));
                criterionList.Add(Expression.Eq("Disabled", 0));
                criterionList.Add(Expression.Eq("Extroom", 1));
                criterionList.Add(Expression.Eq("isPublic", 0)); //FB 2594
                criterionList.Add(Expression.Eq("orgId", organizationID));
                List<vrmRoom> checkRoom = m_IRoomDAO.GetByCriteria(criterionList,true);
                if (checkRoom.Count > 0)
                {
                    criterionLst = new List<ICriterion>();
                    criterionLst.Add(Expression.Eq("Name", checkRoom[0].Name));
                    criterionLst.Add(Expression.Not(Expression.Eq("RoomID", GuestRoomID)));
                    criterionLst.Add(Expression.Eq("Extroom", 0));
                    List<vrmRoom> roomChkList = m_IRoomDAO.GetByCriteria(criterionLst);
                    if (roomChkList.Count > 0)
                    {
                        myVRMException myVRMEx = new myVRMException(256);
                        obj.outXml = myVRMEx.FetchErrorMsg();
                        return false;
                    }
                }

                Licencechk.Add(Expression.Eq("Disabled", 0));
                Licencechk.Add(Expression.Eq("VideoAvailable", 2));
                Licencechk.Add(Expression.Eq("Extroom", 0));
                Licencechk.Add(Expression.Eq("IsVMR", 0));//FB 2586
                Licencechk.Add(Expression.Eq("isPublic", 0)); //FB 2594
                Licencechk.Add(Expression.Eq("orgId", organizationID));//FB 2510
                List<vrmRoom> checkguestRoomCount = m_IRoomDAO.GetByCriteria(Licencechk);
                //FB 2586 START
                int maxVideosRooms = orgInfo.MaxVideoRooms;
                vrmRoom room = m_IRoomDAO.GetByRoomId(GuestRoomID);

                if (room.VideoAvailable == 2 && room.IsVMR == 0 && room.Extroom == 1)
                {
                    if (checkguestRoomCount.Count >= maxVideosRooms)
                    {
                        myVRMEx = new myVRMException(455);
                        obj.outXml = myVRMEx.FetchErrorMsg();
                        return false;
                    }
                }
                //FB 2586 END
                if (checkRoom.Count > 0)
                {
                    for (int i = 0; i < checkRoom.Count; i++)
                    {
                        checkRoom[i].Extroom = 0;
                        if (!m_HardwareFac.SetGuesttoNormalEndpoint(checkRoom[i].endpointid, ref obj))
                        {
                            return false;
                        }
                        else
                        {
                            m_IRoomDAO.Update(checkRoom[i]);
                            confroom.Add(Expression.Eq("roomId", GuestRoomID));
                            confroom.Add(Expression.Eq("Extroom", 1));
                            List<vrmConfRoom> confroomlist = m_IconfRoom.GetByCriteria(confroom);
                            if (confroomlist.Count > 0)
                            {
                                for (int r = 0; r < confroomlist.Count; r++)
                                {
                                    confroomlist[r].Extroom = 0;
                                    m_IconfRoom.Update(confroomlist[r]);
                                }
                            }
                        }
                    }
                }
                
                return true;
            }
            catch (Exception ex)
            {
                m_log.Error("sytemException in GetRoomProfile", ex);
                return false;
            }
        }
        #endregion

        #region DeleteGuestRoom
        /// <summary>
        /// DeleteGuestRoom
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool DeleteGuestRoom(ref vrmDataObject obj)
        {
            int GuestRoomID = 0;
            int userid = 0,errid =0;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                List<ICriterion> criterionList = new List<ICriterion>();
                XmlNode node;

                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out userid);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                if (userid <= 0)
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/organizationID");
                string orgid = "";
                if (node != null)
                    orgid = node.InnerText.Trim();
                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                if (organizationID < defaultOrgId)
                {
                    myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/roomID");
                string roomID = node.InnerXml.Trim();
                Int32.TryParse(roomID, out GuestRoomID);

                criterionList.Add(Expression.Eq("roomId", GuestRoomID));
                criterionList.Add(Expression.Eq("Disabled", 0));
                criterionList.Add(Expression.Eq("orgId", organizationID));
                criterionList.Add(Expression.Eq("Extroom", 1));
                List<vrmRoom> checkRoom = m_IRoomDAO.GetByCriteria(criterionList, true);
                if (checkRoom.Count > 0)
                {
                    for (int i = 0; i < checkRoom.Count; i++)
                    {
                        if (CanDeleteRoom(checkRoom[i].roomId, ref errid))
                        {
                            m_HardwareFac.DeleteGuestEndpoint(checkRoom[i].endpointid);
                            checkRoom[i].disabled = 1;
                            m_IRoomDAO.Update(checkRoom[i]);
                        }
                        else
                        {
                            myVRMEx = new myVRMException(errid);
                            obj.outXml = myVRMEx.FetchErrorMsg();
                            return false;
                        }
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                m_log.Error("sytemException in GetRoomProfile", ex);
                return false;
            }
        }
        #endregion

        //FB 2426 End

        //FB 2392 Starts FB 2532 

        

        #region FetchAvailableRooms

        public void FetchAvailableRooms(int outXMLType, Int32 organizationID, Int32 mode, string RoomNames,
            Int32 userid, Boolean bHasDepartment, ref StringBuilder outxml, ref Int32 confType, int mediatype,
            int serviceType, string selectedloc, int SearchBy, string SearchFor, int pageNo, int MaxRecords) //FB 2532 //FB 2558 WhyGo
        {
            StringBuilder publicFields = null;
            StringBuilder strOutXML = new StringBuilder();
            ESPublicRoom PublicRoomField = null;
            String stateName = "";
            string alphabet = "", a_Order = "Name";
            int sortBy = 3;
            ArrayList siteID = new ArrayList();
            int iPageNo = 0, numrows = 0, numrecs = 0, totpages = 0;
            Int32 u = 0;
            try
            {

                List<ICriterion> LocSiteList = new List<ICriterion>();
                List<ICriterion> LocStateList = new List<ICriterion>();

                
                List<ICriterion> criterionListorg = new List<ICriterion>();
                criterionListorg.Add(Expression.Eq("orgId", organizationID));

                alphabet = SearchFor;
                if (RoomNames.Trim() != "")
                    criterionListorg.Add(Expression.Sql("this_.Name Not in" + RoomNames));

                string serivetypein = "0";
                if (serviceType == 3)
                    serivetypein = "1,2,3";
                else if (serviceType == 2)
                    serivetypein = "1,2";
                else if (serviceType == 1)
                    serivetypein = "1";
                
                string stmtServiceType = "";

                if (serviceType > 0)
                {
                    if (selectedloc.Trim() == "")
                        stmtServiceType += " AND ServiceType in ('" + serivetypein + "') ";
                    else
                        stmtServiceType += " AND (ServiceType in ('" + serivetypein + "') OR roomId IN ('" + selectedloc.Trim() + "'))";

                    criterionListorg.Add(Expression.Sql(stmtServiceType));
                }

                if (PublicRoom > 0)
                    criterionListorg.Add(Expression.Eq("isPublic", 1));
                else if (PublicRoom < 0)
                    criterionListorg.Add(Expression.Eq("isPublic", 0));

                if (mediatype >= 0)
                {
                    criterionListorg.Add(Expression.Eq("VideoAvailable", mediatype));
                }

                criterionListorg.Add(Expression.Eq("Disabled", 0));
                if (selectedloc.Trim() != "")
                    criterionListorg.Add(Expression.Sql("RoomID Not in( " + selectedloc + " )"));

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                if (orgInfo.MultipleDepartments == 0)
                    userid = 11;

                #region OutXML
                strOutXML.Append("<GetConfAvailableRoom>");
				//FB 2558 WhyGo
                List<int> tier3IDs = null;
                String tier2IDs = "";

                string checkAlpha = alphabet.ToLower();
                int capacity = 0, strLength = 0;
                strLength = alphabet.Length;
                sortBy = SearchBy;
                switch (sortBy)
                {
                    case 1:
                        a_Order = "Capacity";
                        Int32.TryParse(checkAlpha, out capacity);
                        criterionListorg.Add(Expression.Ge("Capacity", capacity));
                        break;
                    case 2:
                        a_Order = "City";
                        if (strLength > 0)
                        {
                            criterionListorg.Add(Expression.Like("City", alphabet + "%"));
                        }
                        break;
                    case 4:
                        a_Order = "RoomPhone";
                        if (strLength > 0)
                        {
                            criterionListorg.Add(Expression.Like("RoomPhone", alphabet + "%"));
                        }
                        break;
                    case 5:
                        a_Order = "L3LocationId"; //CountryName //Tier2 //FB 2558 WhyGo - Starts
                        if (strLength > 0)
                        {
                            LocSiteList.Add(Expression.Like("Name", alphabet + "%"));
                        }
                        LocSiteList.Add(Expression.Eq("disabled", 0));
                        LocSiteList.Add(Expression.Eq("orgId", organizationID));
                        List<vrmTier2> tier2s = m_IT2DAO.GetByCriteria(LocSiteList);
                        tier3IDs = new List<int>();
                        
                        for (int j = 0; j < tier2s.Count; j++)
                        {
                            if (!tier3IDs.Contains(tier2s[j].L3LocationId))
                            {
                                tier3IDs.Add(tier2s[j].L3LocationId);
                                if (tier2IDs == "")
                                    tier2IDs = tier2s[j].ID.ToString();
                                else
                                    tier2IDs += "," + tier2s[j].ID.ToString();
                            }
                        }
                        criterionListorg.Add(Expression.In("L3LocationId", tier3IDs));
                        criterionListorg.Add(Expression.Sql("this_.L2LocationId in ( " + tier2IDs + ")"));//FB 2558 WhyGo - End
                        break;
                    case 6:
                        a_Order = "State";
                        LocStateList.Add(Expression.Or(Expression.Like("State", alphabet + "%"), Expression.Like("StateCode", alphabet + "%")));
                        List<vrmState> states = m_IStateDAO.GetByCriteria(LocStateList);
                        for (int i = 0; i < states.Count; i++)
                        {
                            siteID.Add(states[i].Id);
                        }

                        criterionListorg.Add(Expression.In("State", siteID));
                        break;
                    case 7:
                        a_Order = "Zipcode";
                        if (strLength > 0)
                        {
                            criterionListorg.Add(Expression.Like("Zipcode", alphabet + "%"));
                        }
                        break;
                    default:
                        a_Order = "Name";
                        if (strLength > 0)
                        {
                            criterionListorg.Add(Expression.Like("Name", alphabet + "%"));
                        }
                        break;
                }
				//FB 2558 WhyGo - End
                iPageNo = pageNo;

                if (iPageNo == 0)
                    iPageNo = 1;
                long ttlRecords = 0;
                IList roomList = new List<vrmRoom>();

                if (pageNo > 0)
                {
                    m_IRoomDAO.addProjection(Projections.RowCount());
                    IList list = m_IRoomDAO.GetObjectByCriteria(new List<ICriterion>());
                    Int32.TryParse((list[0].ToString()), out numrows);
                    list = m_IRoomDAO.GetObjectByCriteria(criterionListorg);

                    Int32.TryParse((list[0].ToString()), out numrecs);

                    m_IRoomDAO.clearProjection();

                    m_IRoomDAO.pageSize(MaxRecords);
                    m_IRoomDAO.pageNo(iPageNo);
                    ttlRecords =
                        m_IRoomDAO.CountByCriteria(criterionListorg);
                    m_IRoomDAO.addOrderBy(Order.Asc(a_Order));
                }
                roomList = m_IRoomDAO.GetByCriteria(criterionListorg);

                totpages = numrows / MaxRecords;
                if ((numrows % MaxRecords) > 0)
                    totpages++;

                int ttlPages = (int)(ttlRecords / MaxRecords);

                if (ttlRecords % MaxRecords > 0)
                    ttlPages++;


                publicFields = new StringBuilder();
                vrmUser userInfo = null;

                if (roomList != null || roomList.Count < 0)
                {
                    //Doubt
                    //FB 2594-Starts
                    Dictionary<int, ESPublicRoom> dictionary;
                    vrmRoom locRoom = null;

                    List<ESPublicRoom> bymyVRMRoomId = m_IESPublicRoomDAO.GetBymyVRMRoomId(roomList.Cast<vrmRoom>().Select<vrmRoom, int>(delegate(vrmRoom room)
                    {
                        return room.roomId;
                    }).ToList<int>());
                    if ((bymyVRMRoomId != null) && (bymyVRMRoomId.Count > 0))
                    {
                        dictionary = bymyVRMRoomId.ToDictionary<ESPublicRoom, int>(delegate(ESPublicRoom publicRoom)
                        {
                            return publicRoom.RoomID;
                        });
                    }
                    else
                    {
                        dictionary = new Dictionary<int, ESPublicRoom>();
                    }
                    //FB 2594-End

                    for (int count = 0; count < roomList.Count; count++)
                    {
                        locRoom = (vrmRoom)roomList[count];

                        /* FB 2558 - WhyGo
                        if (orgInfo.TelepresenceFilter <= 0 && confType == vrmConfType.RooomOnly && locRoom.isTelepresence.ToString() == "1")//FB 2170
                            continue;

                        if (orgInfo.DedicatedVideo <= 0 && confType == vrmConfType.RooomOnly && locRoom.DedicatedVideo.ToString() == "1")
                        {
                            if (locRoom.roomId.ToString() != selectedloc.Trim())
                            {
                                continue;
                            }
                        } Removed for whygo as there only public rooms
                        */
                        if (locRoom.Name != "Phantom Room")
                        {
                            strOutXML.Append("<Room>");
                            strOutXML.Append("<RoomID>" + locRoom.roomId.ToString() + "</RoomID>");

                            //PublicRoomField = m_IESPublicRoomDAO.GetBymyVRMRoomId(locRoom.roomId);
                            dictionary.TryGetValue(locRoom.roomId, out PublicRoomField);

                            strOutXML.Append("<LastModifiedDate>" + locRoom.Lastmodifieddate.ToString() + "</LastModifiedDate>"); //FB 2532

                            if (outXMLType == 1) //FB 2532 Starts
                            {
                                strOutXML.Append("<RoomName>" + locRoom.Name + "</RoomName>");
                                strOutXML.Append("<RoomPhoneNumber>" + locRoom.RoomPhone + "</RoomPhoneNumber>");
                                strOutXML.Append("<MaximumCapacity>" + locRoom.Capacity.ToString() + "</MaximumCapacity>");
                                if (PublicRoomField == null) //FB 2558 WhyGo
                                {
                                    strOutXML.Append("<AssistantInchargeID>" + locRoom.assistant.ToString() + "</AssistantInchargeID>");
                                    userInfo = m_IUserDAO.GetByUserId(locRoom.assistant);
                                    if (userInfo != null)
                                    {
                                        strOutXML.Append("<AssistantInchargeName>" + userInfo.FirstName + " " + userInfo.LastName + "</AssistantInchargeName>");
                                    }
                                    else
                                    {
                                        strOutXML.Append("<AssistantInchargeName></AssistantInchargeName>");
                                    }
                                }
                                if (locRoom.tier2 != null)
                                {
                                    if (locRoom.tier2.tier3 != null)
                                    {
                                        strOutXML.Append("<Tier1ID>" + locRoom.tier2.tier3.ID.ToString() + "</Tier1ID>");
                                        strOutXML.Append("<Tier1Name>" + locRoom.tier2.tier3.Name + "</Tier1Name>");
                                    }
                                    else
                                    {
                                        strOutXML.Append("<Tier1ID></Tier1ID>");
                                        strOutXML.Append("<Tier1Name></Tier1Name>");
                                    }
                                    strOutXML.Append("<Tier2ID>" + locRoom.tier2.ID.ToString() + "</Tier2ID>");
                                    strOutXML.Append("<Tier2Name>" + locRoom.tier2.Name + "</Tier2Name>");
                                }
                                else
                                {
                                    strOutXML.Append("<Tier1ID></Tier1ID>");
                                    strOutXML.Append("<Tier1Name></Tier1Name>");
                                    strOutXML.Append("<Tier2ID></Tier2ID>");
                                    strOutXML.Append("<Tier2Name></Tier2Name>");
                                }
                                if (PublicRoomField == null)//FB 2558 WhyGo
                                {
                                    strOutXML.Append("<Floor>" + locRoom.RoomFloor + "</Floor>");
                                    strOutXML.Append("<RoomNumber>" + locRoom.RoomNumber + "</RoomNumber>");
                                    strOutXML.Append("<StreetAddress1>" + locRoom.Address1 + "</StreetAddress1>");
                                    strOutXML.Append("<StreetAddress2>" + locRoom.Address2 + "</StreetAddress2>");
                                    strOutXML.Append("<Handicappedaccess>" + locRoom.HandiCappedAccess.ToString() + "</Handicappedaccess>");
                                    strOutXML.Append("<Disabled>" + locRoom.disabled.ToString() + "</Disabled>");
                                    strOutXML.Append("<isTelePresence>" + locRoom.isTelepresence.ToString() + "</isTelePresence>");//FB 2400
                                    strOutXML.Append("<State>" + locRoom.State.ToString() + "</State>");
                                    if (locRoom.State > 0)
                                    {
                                        vrmState objState = m_IStateDAO.GetById(locRoom.State);
                                        strOutXML.Append("<StateName>" + objState.StateCode.ToString() + "</StateName>");
                                    }
                                    else
                                    {
                                        stateName = "";
                                        if (PublicRoomField != null)
                                            stateName = PublicRoomField.StateName;

                                        strOutXML.Append("<StateName>" + stateName + "</StateName>");
                                    }
                                    strOutXML.Append("<Country>" + locRoom.Country.ToString() + "</Country>");
                                    if (locRoom.Country > 0)
                                    {
                                        vrmCountry objCountry = m_ICountryDAO.GetById(locRoom.Country);
                                        strOutXML.Append("<CountryName>" + objCountry.CountryName + "</CountryName>");
                                    }
                                    else
                                    {
                                        strOutXML.Append("<CountryName></CountryName>");
                                    }
                                    string stmt2 = "";
                                    stmt2 = "select approverid from myVRM.DataLayer.vrmLocApprover where roomid = " + locRoom.roomId.ToString();
                                    IList appr = m_IconfRoom.execQuery(stmt2);

                                    if (appr.Count > 0)
                                        strOutXML.Append("<Approval>1</Approval>");
                                    else
                                        strOutXML.Append("<Approval>0</Approval>");

                                    strOutXML.Append("<Approvers>");
                                    for (int j = 1; j < appr.Count + 1; j++)
                                    {
                                        int apprid = 0;
                                        Int32.TryParse(appr[j - 1].ToString(), out apprid);

                                        userInfo = m_IUserDAO.GetByUserId(apprid);
                                        strOutXML.Append("<Approver" + j + "ID>" + apprid + "</Approver" + j + "ID>");
                                        strOutXML.Append("<Approver" + j + "Name>" + userInfo.FirstName + " " + userInfo.LastName + "</Approver" + j + "Name>");
                                    }
                                    for (int j = appr.Count + 1; j <= 3; j++)
                                    {
                                        strOutXML.Append("<Approver" + j + "ID></Approver" + j + "ID>");
                                        strOutXML.Append("<Approver" + j + "Name></Approver" + j + "Name>");
                                    }
                                    strOutXML.Append("</Approvers>");
                                }

                                strOutXML.Append("<Video>" + locRoom.VideoAvailable.ToString() + "</Video>");
                                strOutXML.Append("<City>" + locRoom.City + "</City>");
                                strOutXML.Append("<ZipCode>" + locRoom.Zipcode + "</ZipCode>");
                                strOutXML.Append("<MapLink>" + locRoom.Maplink + "</MapLink>");
                                strOutXML.Append("<TimezoneID>" + locRoom.TimezoneID.ToString() + "</TimezoneID>");
                                if (locRoom.TimezoneID > 0)
                                {
                                    timeZoneData tz = new timeZoneData();
                                    timeZone.GetTimeZone(locRoom.TimezoneID, ref tz);
                                    strOutXML.Append("<TimezoneName>" + tz.TimeZone + "</TimezoneName>");
                                }
                                else
                                {
                                    strOutXML.Append("<TimezoneName></TimezoneName>");
                                }
                                strOutXML.Append("<Longitude>" + locRoom.Longitude + "</Longitude>");
                                strOutXML.Append("<Latitude>" + locRoom.Latitude + "</Latitude>");
                                List<vrmEndPoint> eptList = new List<vrmEndPoint>();
                                List<ICriterion> criterionListept = new List<ICriterion>();
                                criterionListept.Add(Expression.Eq("endpointid", locRoom.endpointid));
                                criterionListept.Add(Expression.Eq("deleted", 0));
                                eptList = m_vrmEpt.GetByCriteria(criterionListept);

                                strOutXML.Append("<EndpointID>" + locRoom.endpointid.ToString() + "</EndpointID>");

                                if (eptList.Count > 0)
                                {
                                    foreach (vrmEndPoint ept in eptList)
                                    {
                                        strOutXML.Append("<EndpointName>" + ept.name + "</EndpointName>");
                                        strOutXML.Append("<EndpointIP>" + ept.address + "</EndpointIP>");
                                        break;
                                    }
                                }
                                else
                                {
                                    strOutXML.Append("<EndpointName></EndpointName>");
                                    strOutXML.Append("<EndpointIP></EndpointIP>");
                                }
                                if (PublicRoomField == null) //FB 2558 WhyGo
                                {
                                    //Image Project codelines start...
                                    string roomImagesids = locRoom.RoomImageId;
                                    string roomImagesnames = locRoom.RoomImage;

                                    string imagename = "";

                                    string fileext = "";

                                    if (roomImagesids != null && roomImagesnames != null)
                                    {
                                        if (roomImagesids != "" && roomImagesnames != "")
                                        {
                                            string[] idArr = roomImagesids.Split(',');
                                            string[] nameArr = roomImagesnames.Split(',');

                                            if (idArr.Length > 0 && nameArr.Length > 0)
                                            {
                                                fileext = "jpg";

                                                imagename = nameArr[0].ToString();
                                                if (imagename != "")
                                                    fileext = imagename.Substring(imagename.LastIndexOf(".") + 1);
                                            }
                                        }
                                    }
                                    strOutXML.Append("<ImageName>" + imagename + "</ImageName>");
                                    strOutXML.Append("<Imagetype>" + fileext + "</Imagetype>");
                                    strOutXML.Append("<ServiceType>" + locRoom.ServiceType.ToString() + "</ServiceType>");
                                    strOutXML.Append("<DedicatedVideo>" + locRoom.DedicatedVideo + "</DedicatedVideo>");
                                    strOutXML.Append("<DedicatedCodec>" + locRoom.DedicatedCodec + "</DedicatedCodec>");
                                    strOutXML.Append("<Extroom>" + locRoom.Extroom + "</Extroom>");
                                    strOutXML.Append("<LoginUserId>" + locRoom.adminId + "</LoginUserId>");
                                    strOutXML.Append("<isVMR>" + locRoom.IsVMR + "</isVMR>");
                                    strOutXML.Append("<DefaultEquipmentID>" + locRoom.DefaultEquipmentid.ToString() + "</DefaultEquipmentID>");
                                }
                                strOutXML.Append("<isPublic>" + locRoom.isPublic + "</isPublic>");
                                strOutXML.Append("<PublicFields>");

                                int Speed = 384;
                                publicFields = new StringBuilder();

                                if (PublicRoomField != null)
                                {
                                    if (eptList.Count > 0)
                                        Speed = eptList[0].linerateid;

                                    publicFields.Append("<WhygoRoomID>" + PublicRoomField.WhygoRoomId + "</WhygoRoomID>");
                                    publicFields.Append("<Address>" + PublicRoomField.address + "</Address>");
                                    publicFields.Append("<State>" + PublicRoomField.StateName + "</State>");
                                    publicFields.Append("<Country>" + PublicRoomField.Country + "</Country>");
                                    publicFields.Append("<AUXEquipment>" + PublicRoomField.AUXEquipment + "</AUXEquipment>");
                                    publicFields.Append("<Speed>" + Speed + "</Speed>");
                                    publicFields.Append("<Type>" + PublicRoomField.Type + "</Type>");
                                    publicFields.Append("<Description>" + PublicRoomField.RoomDescription + "</Description>");
                                    publicFields.Append("<ExtraNotes>" + PublicRoomField.ExtraNotes + "</ExtraNotes>");
                                    publicFields.Append("<GeoCodeAddress>" + PublicRoomField.geoCodeAddress + "</GeoCodeAddress>");
                                    publicFields.Append("<IsAutomatic>" + PublicRoomField.isAutomatic + "</IsAutomatic>");
                                    publicFields.Append("<IsHDCapable>" + PublicRoomField.isHDCapable + "</IsHDCapable>");
                                    publicFields.Append("<IsIPCapable>" + PublicRoomField.isIPCapable + "</IsIPCapable>");
                                    publicFields.Append("<IsISDNCapable>" + PublicRoomField.isISDNCapable + "</IsISDNCapable>");
                                    publicFields.Append("<ImgLink>" + PublicRoomField.MapLink + "</ImgLink>");
                                    publicFields.Append("<IsTP>" + PublicRoomField.isTP + "</IsTP>");
                                    publicFields.Append("<isVCCapable>" + PublicRoomField.isVCCapable + "</isVCCapable>");
                                    publicFields.Append("<CurrencyType>" + PublicRoomField.CurrencyType + "</CurrencyType>");
                                    publicFields.Append("<IsEarlyHoursEnabled>" + PublicRoomField.IsEarlyHoursEnabled + "</IsEarlyHoursEnabled>");
                                    publicFields.Append("<EarlyHoursStart>" + PublicRoomField.EHStartTime + "</EarlyHoursStart>");
                                    publicFields.Append("<EarlyHoursEnd>" + PublicRoomField.EHEndTime + "</EarlyHoursEnd>");
                                    publicFields.Append("<EarlyHoursCost>" + PublicRoomField.EHCost + "</EarlyHoursCost>");
                                    publicFields.Append("<IsEarlyHoursFullyAuto>" + PublicRoomField.AHFullyAuto + "</IsEarlyHoursFullyAuto>"); //FB 2543
                                    publicFields.Append("<OpenHour>" + PublicRoomField.openHours + "</OpenHour>");
                                    publicFields.Append("<OfficeHoursStart>" + PublicRoomField.OHStartTime + "</OfficeHoursStart>");
                                    publicFields.Append("<OfficeHoursEnd>" + PublicRoomField.OHEndTime + "</OfficeHoursEnd>");
                                    publicFields.Append("<OfficeHoursCost>" + PublicRoomField.OHCost + "</OfficeHoursCost>");
                                    publicFields.Append("<IsAfterHourEnabled>" + PublicRoomField.IsAfterHourEnabled + "</IsAfterHourEnabled>");
                                    publicFields.Append("<AfterHoursStart>" + PublicRoomField.AHStartTime + "</AfterHoursStart>");
                                    publicFields.Append("<AfterHoursEnd>" + PublicRoomField.AHEndTime + "</AfterHoursEnd>");
                                    publicFields.Append("<AfterHoursCost>" + PublicRoomField.AHCost + "</AfterHoursCost>");
                                    publicFields.Append("<IsAfterHoursFullyAuto>" + PublicRoomField.AHFullyAuto + "</IsAfterHoursFullyAuto>"); //FB 2543
                                    publicFields.Append("<IsCrazyHoursSupported>" + PublicRoomField.isCrazyHoursSupported + "</IsCrazyHoursSupported>");
                                    publicFields.Append("<CrazyHoursStart>" + PublicRoomField.CHtartTime + "</CrazyHoursStart>");
                                    publicFields.Append("<CrazyHoursEnd>" + PublicRoomField.CHEndTime + "</CrazyHoursEnd>");
                                    publicFields.Append("<CrazyHoursCost>" + PublicRoomField.CHCost + "</CrazyHoursCost>");
                                    publicFields.Append("<IsCrazyHoursFullyAuto>" + PublicRoomField.CHFullyAuto + "</IsCrazyHoursFullyAuto>"); //FB 2543
                                    publicFields.Append("<Is24HoursEnabled>" + PublicRoomField.Is24HoursEnabled + "</Is24HoursEnabled>");
                                    publicFields.Append("<DefaultEquipment>" + PublicRoomField.DefaultEquipment + "</DefaultEquipment>");
                                    publicFields.Append("<PublicRoomLastModified>" + PublicRoomField.PublicRoomLastModified.ToString() + "</PublicRoomLastModified>");
                                }
                                strOutXML.Append(publicFields.ToString());
                                strOutXML.Append("</PublicFields>");
                            } //FB 2532 Ends
                            strOutXML.Append("</Room>");
                        }
                    }
                    if (pageNo > 0)
                    {
                        strOutXML.Append("<pageNo>" + pageNo.ToString() + "</pageNo>");
                        strOutXML.Append("<totalPages>" + ttlPages.ToString() + "</totalPages>");
                        strOutXML.Append("<searchBy>" + sortBy + "</searchBy>");
                        strOutXML.Append("<searchFor>" + alphabet + "</searchFor>");
                        strOutXML.Append("<totalNumber>" + numrecs + "</totalNumber>");
                    }
                }
                outxml.Append(strOutXML.ToString());

                outxml.Append("</GetConfAvailableRoom>");

                #endregion
               
            }
            catch (Exception ex)
            {
                m_log.Error("FetchAvailableRooms :" + ex.Message);
            }
        }

        #endregion

        //FB 2392 Ends
    }
}

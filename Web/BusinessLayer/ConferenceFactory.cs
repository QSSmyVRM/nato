using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using log4net;
using NHibernate.Criterion;
using myVRM.DataLayer;
using cryptography;
using System.Linq; //FB 2027 - Setconference start
using System.Reflection;
using ns_SqlHelper; //FB 2027 - Setconference end

namespace myVRM.BusinessLayer
{
    public enum Approver { ROOM_ENTITY = 1, MCU_ENTITY, DEPT_ENTITY, SYSTEM_ENTITY, APPROVED }; //FB 2027
    public enum eConfMode { CONF_NEW = 1, CONF_EDIT, CONF_DELETE, CONF_DENY, CONF_TERMINATE }; //FB 2027 SetConference

    #region Conference Class
    /// <summary>
    /// Data Layer Logic for loading/saving Reports(Templates/Schedule)
    /// </summary>
    public class Conference
    {
        #region Private Members

        private int m_videomode;
        private userDAO m_usrDAO;
        private conferenceDAO m_confDAO;
        //FB 2027 Start
        private ITempRoomDAO m_TempDAO;
        private ITempAdvAvParamsDAO m_TempAdvAvParamsDAO;
        private ITempUserDAO m_TempUserDAO;
        private ITempGroupDAO m_TempGroupDAO;
        private IConfMontiorDAO m_IconfMontDAO;
        //FB 2027 End
        private LocationDAO m_locDAO;
        private myVRMSearch m_Search;
        private vrmSystemFactory m_vrmSystemFactory;
        private hardwareDAO m_Hardware;
        private vrmFactory m_vrmFactor;
        private IUserDao m_vrmUserDAO;
        private IGuestUserDao m_vrmGuestDAO;
        private IConferenceDAO m_vrmConfDAO;
        private ITemplateDAO m_vrmTempDAO;
        private IRoomDAO m_vrmRoomDAO;
        private IMCUDao m_vrmMCU;
        private IEptDao m_vrmEpt;
        private IConfRoomDAO m_IconfRoom;
        private IConfUserDAO m_IconfUser;
        private IConfCascadeDAO m_IconfCascade;
        private IEmailDao m_emailDAO;
        private Hashtable m_ISDNusedNumber;
        private emailFactory m_emailFactory;
        UserFactory m_usrFactory;//FB 2027
        private OrganizationFactory m_OrgFactory; //FB 2027ResponseInvite
        private RoomFactory m_RoomFactory; //FB 2027ResponseInvite
        private UtilFactory m_UtilFactory; //FB 2236
        private HardwareFactory m_HardwareFactory;//FB 2426
        private IT2RoomDAO m_IT2DAO;//FB 2426
        private string m_configPath; 
        private ILog m_log;
        private int m_level;
        private Boolean m_isEdit;

        private List<int> pastInstances;    //FB 1158

        private orgDAO m_OrgDAO;    //Organization Module Fixes
        private IOrgSettingsDAO m_IOrgSettingsDAO;
        private ISysApproverDAO m_ISysApproverDAO;
        private OrgData orgInfo;
        //FB 2027 start
        private IConfRecurDAO m_ConfRecurDAO;
        private IConfAttachmentDAO m_ConfAttachDao;
        private IUserAccountDao m_userAccountDAO;
        private ILanguageDAO m_ILanguageDAO;
        private IEmailLanguageDao m_IEmailLanguageDAO;
		private GeneralDAO m_generalDAO;
        //FB 2027 end
        private const int defaultOrgId = 11;  //Default organization - Organization Module Fixes
        private int organizationID=0;
        private int multiDepts = 1;//FB 2027
        private IUserRolesDao m_IUserRolesDao; //FB 2027
        private IConfAdvAvParamsDAO m_confAdvAvParamsDAO;//FB 2027
        private deptDAO m_deptDAO;
        private IDeptDao m_IdeptDAO; //FB 2027
        private IUserDeptDao m_IuserDeptDAO; //FB 2027 - GetAvailableRoom
        private ISysTechDAO m_ISysTechDAO;//FB 2027ResponseInvite
		Hashtable HashTb = null; //FB 2027 - SetTerminalCtrl
        List<vrmMCU> Mcus = new List<vrmMCU>(); //FB 2027 - SetTerminalCtrl
		private myVRMException myVRMEx;
        //FB 2027 SetConference ... start
        private IConfAttrDAO m_IconfAttrDAO;
        private IConfMessageDao m_IConfMessageDao; //FB 2486
        private IMessageDao m_IMessageDao; //FB 2486
        private List<vrmConference> confLst;
        private bool newConf;
        private vrmUser confScheduler;
        private vrmUser confHost;
        private Config vrmConfig;
        private bool instanceEdit = false;
        private bool isRecurring = false;
        private SqlHelper sqlCon = null;
        List<string> fileList = null;
        vrmRecurInfoDefunct defunctRecurInfo = null;//FB 2218
        //FB 2027 SetConference ... end

        Boolean minimalWS = false;//FB 2342
        private IESPublicRoomDAO m_IESPublicRoomDAO; //FB 2392-WhyGO

        private IConfBridgeDAO m_IconfBridge; //FB 2566
        int MeetandGreetBuffer = 0;//FB 2609
        bool IsMeetandGreet = false;//FB 2609
        List<int> ConfRooms = null; //FB 2594
        private IConfSyncMCUadjustmentsDao m_IConfSyncMCUadjustmentsDao; //FB 2441
        #endregion
        //FB 2501 EM7 Starts
        #region EndpointStatus
        private enum EndpointStatus
        {
            Active = 1,
            InActive = 0
        }
        #endregion
        //FB 2501 EM7 Ends

        #region Constructor
        /// <summary>
        /// construct report factory with session reference
        /// </summary>
        public Conference(ref vrmDataObject obj)
        {
            try
            {
                m_configPath = obj.ConfigPath;
                m_log = obj.log;

                m_usrDAO = new userDAO(obj.ConfigPath, obj.log);
                m_confDAO = new conferenceDAO(obj.ConfigPath, obj.log);
                m_locDAO = new LocationDAO(obj.ConfigPath, obj.log);
                m_Hardware = new hardwareDAO(obj.ConfigPath, obj.log);
                m_emailFactory = new emailFactory(ref obj);
                m_usrFactory = new UserFactory(ref obj); //FB 2027    
                m_vrmFactor = new vrmFactory(ref obj);
				m_generalDAO = new GeneralDAO(m_configPath, m_log); //FB 2027
                m_OrgFactory = new OrganizationFactory(ref obj); //FB 2027ResponseInvite
                m_RoomFactory = new RoomFactory(ref obj); //FB 2027ResponseInvite
                m_UtilFactory = new UtilFactory(ref obj); //FB 2236
                m_HardwareFactory = new HardwareFactory(ref obj);//FB 2426
                m_IT2DAO = m_locDAO.GetT2RoomDAO();//FB 2426
                m_Search = new myVRMSearch(obj);
                m_vrmSystemFactory = new vrmSystemFactory(ref obj);

                m_vrmUserDAO = m_usrDAO.GetUserDao();
                m_vrmGuestDAO = m_usrDAO.GetGuestUserDao();
                m_vrmConfDAO = m_confDAO.GetConferenceDao();
                m_vrmTempDAO = m_confDAO.GetTemplateDao();
                m_emailDAO = m_confDAO.GetConfEmailDao();
                m_vrmRoomDAO = m_locDAO.GetRoomDAO();
                m_vrmMCU = m_Hardware.GetMCUDao();
                m_vrmEpt = m_Hardware.GetEptDao();

                m_IconfRoom = m_confDAO.GetConfRoomDao();
                m_IconfUser = m_confDAO.GetConfUserDao();
                m_IconfCascade = m_confDAO.GetConfCascadeDao();
                m_IconfBridge = new confBridgeDAO(obj.ConfigPath); //FB 2566

                m_confAdvAvParamsDAO = m_confDAO.GetConfAdvAvParamsDAO();//FB 2027
                m_OrgDAO = new orgDAO(m_configPath, m_log); //Organization Module Fixes
                m_IOrgSettingsDAO = m_OrgDAO.GetOrgSettingsDao();
                m_ISysApproverDAO = m_OrgDAO.GetSysApproverDao();
                //FB 2027 Start
                m_TempDAO = m_confDAO.GetTempRoomDAO();
                m_TempAdvAvParamsDAO = m_confDAO.GetTempAdvAvParamsDAO();
                m_TempUserDAO = m_confDAO.GetTempUserDAO();
                m_TempGroupDAO = m_confDAO.GetTempGroupDAO();
                m_ConfRecurDAO = m_confDAO.GetConfRecurDao();
	
                m_ConfAttachDao = m_confDAO.GetConfAttachmentDao();
                m_userAccountDAO = m_usrDAO.GetUserAccountDao();
                m_deptDAO = new deptDAO(obj.ConfigPath, obj.log);
                m_IdeptDAO = m_deptDAO.GetDeptDao();
				m_IconfMontDAO = m_confDAO.GetConfMontiorDAO(); //FB 2027 SetTerminalCtrl
				m_ILanguageDAO = m_generalDAO.GetLanguageDAO();
                m_IEmailLanguageDAO = m_confDAO.GetEmailLanguageDao();
                m_IUserRolesDao = m_usrDAO.GetUserRolesDao(); 
                m_IuserDeptDAO = m_deptDAO.GetUserDeptDao(); //FB 2027 - GetAvailableRoom
                m_ISysTechDAO = m_OrgDAO.GetSysTechDao();//FB 2027ResponseInvite
                //FB 2027 End
                m_log = obj.log;
                m_videomode = 1;
                m_level = 0;
                m_isEdit = false;
                //FB 2027 SetConference ... start
                m_IconfAttrDAO = m_confDAO.GetConfAttrDao();
                newConf = true;
                isRecurring = false;
                vrmConfig = new Config(m_configPath);
                vrmConfig.Initialize();
                //FB 2027 SetConference ... end
                m_IConfMessageDao = m_confDAO.GetConfMessageDao(); //FB 2486
                m_IMessageDao = m_Hardware.GetMessageDao(); //FB 2486
				m_IESPublicRoomDAO = m_locDAO.GetESPublicRoomDAO(); //FB 2392-WhyGO
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
        }
        #endregion

        //FB 2027 SetConference - start
        #region SetConference Related Methods

        #region SetConference
        /// <summary>
        /// mode 0, new conference setup, 2, modify the first recurrent conference.
        /// mode 1, modify all the instance for recurrent conference, or a common conference.
        /// mode 2, modify the first recurrent conference.
        /// mode 3, by add participant, when confid =="new", add a new conference, otherwise , modify.
        /// mode 4, modify all instance, but the confid like"23,2", so I change to 23, and let recurrentmode=0
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SetConference(ref vrmDataObject obj)
        {
            myVRMException myvrmEx = null;
            List<SplitRoom> splitRooms = null;
            List<vrmConfRoom> confRooms = null;
            List<vrmConfUser> confUser = null;
            List<vrmGuestUser> addedGuests = null;
            List<sRecurrDates> recurDates = null;
            vrmRecurInfo recurInfo = null;
            vrmConfAdvAvParams confAVParams = null;
            vrmConference vConf = null; ;
            List<vrmConfAttribute> confAttrs = null;
            List<vrmRoom> guestRooms = null;//FB 2426
            List<vrmEndPoint> guestEndpoints = null; //FB 2426
            Int32 totalInviteed = 0;
            Int32 totalEndpoints = 0;
            string conflictRooms = "";
            Hashtable PastInstanceLst = new Hashtable();
            string internalname = "";
            int pastCnt = 0;
            int iInstanceid = 0;
            int confNumName = 0;
            int confid = 0;
            string IcalID = "";
            String icalAttachment = "";

            //FB 2441
            List<vrmConfSyncMCUadjustments> lstSyncAdjstmnts = null;
            vrmConfSyncMCUadjustments vrmConfSyncMCUadjustment = null;
            List<vrmConfSyncMCUadjustments> removeMCUadjustment = null;
            try
            {
                if (!ParseConferenceXml(ref obj, ref confRooms, ref confUser, ref splitRooms, ref  addedGuests, ref totalInviteed, ref totalEndpoints,
                   ref conflictRooms, ref recurDates, ref recurInfo, ref confAVParams, ref confAttrs, ref vConf, ref icalAttachment, ref guestRooms, ref guestEndpoints)) //FB 2426
                    return false;

                if (!BuildConfList(ref vConf, ref recurDates, ref obj, ref totalEndpoints, ref confRooms, ref splitRooms))
                    return false;

                if (confLst == null)
                    throw new myVRMException("Error in binding conference list");

                if (confLst.Count <= 0)
                    throw new myVRMException("Error in binding conference list");

                if (newConf)
                {
                    #region New Conference (both normal & recurrence)

                    for (int ccnt = 0; ccnt < confLst.Count; ccnt++)
                    {
                        iInstanceid++;
                        internalname = confLst[ccnt].userid + confLst[ccnt].externalname + confLst[ccnt].confdate.ToShortDateString().Replace("/", ""); //;/,.- :
                        //Set Confumname
                        confNumName = GetConfNumname();

                        if (ccnt == 0)
                            confid = GetConfId(); //Set Confid

                        confLst[ccnt].sentSurvey = 0; //FB 2348
                        confLst[ccnt].confid = confid;
                        confLst[ccnt].confnumname = confNumName;
                        confLst[ccnt].instanceid = iInstanceid;
                        confLst[ccnt].internalname = internalname;
                        confLst[ccnt].IcalID = vConf.IcalID;//FB 2342 
                        confLst[ccnt].GUID = "";//FB 2501 Call Monitoring
                        //FB 2426 Start
                        int count = 0, loginuserid = 0, errNo=0;
                        if (guestRooms != null)
                        {
                            for (int ro = 0; ro < guestRooms.Count; ro++)
                            {
                                if (guestRooms[ro].roomId == 0)
                                    count += 1;
                            }

                            if (count > 0)
                            {
                                loginuserid = guestRooms[0].adminId;
                                if (!licensecheck(ref count, ref loginuserid, ref organizationID, ref errNo))
                                {
                                    myvrmEx = new myVRMException(errNo);
                                    obj.outXml = myvrmEx.FetchErrorMsg();
                                    return false;
                                }
                            }
                        }
                        //FB 2426 End
                        confLst[ccnt].NetworkSwitch = 0;//FB 2595
                        m_vrmConfDAO.Save(confLst[ccnt]);

                        vConf = confLst[ccnt];
                        if (isRecurring && ccnt == 0) //Insert recurinfo only once
                        {
                            recurInfo.confuId = confLst[0].confnumname;
                            if (!InsertRecurInfo(ref recurInfo))
                            {
                                myvrmEx = new myVRMException(551);
                                obj.outXml = myvrmEx.FetchErrorMsg();
                                return false;
                            }

                            if (defunctRecurInfo != null)//FB 2218
                            {
                                defunctRecurInfo.confuId = confLst[0].confnumname;
                                if (!InsertDefunctRecurInfo(ref defunctRecurInfo))
                                {
                                    myvrmEx = new myVRMException(551);
                                    obj.outXml = myvrmEx.FetchErrorMsg();
                                    return false;
                                }
                            }
                        }
                        if (!InsertAdvAVParams(ref confAVParams, ref vConf))
                        {
                            myvrmEx = new myVRMException(552);
                            obj.outXml = myvrmEx.FetchErrorMsg();
                            return false;
                        }
                        //FB 2426 Start
                        if (confRooms != null || guestRooms != null || guestEndpoints != null) 
                        {
                            if (!InsertLocations(ref confRooms, ref splitRooms, ref vConf, ref guestRooms, ref guestEndpoints, ref errNo))
                            {
                                myvrmEx = new myVRMException(errNo);
                                obj.outXml = myvrmEx.FetchErrorMsg();
                                return false;
                            }
                        }
                        //FB 2426 End
                        if (confUser != null || addedGuests != null)
                        {
                            if (!InsertParty(ref confUser, ref addedGuests, ref vConf))
                            {
                                myvrmEx = new myVRMException(553);
                                obj.outXml = myvrmEx.FetchErrorMsg();
                                return false;
                            }
                        }
                        if (fileList != null)
                        {
                            AttachmentSetup(ref vConf, ref icalAttachment);
                        }
                        if (confAttrs != null)
                        {
                            if (!InsertConfAttributes(ref confAttrs, ref vConf))
                            {
                                myvrmEx = new myVRMException(554);
                                obj.outXml = myvrmEx.FetchErrorMsg();
                                return false;
                            }
                        }
                    }
                    #endregion
                }
                else
                {
                    #region Conference Edit
                    DeleteConfEntities(ref pastCnt, ref PastInstanceLst, ref IcalID, ref lstSyncAdjstmnts);//FB 2441

                    if (isRecurring)
                    {
                        #region Recurrence Part
                        confid = 0;
                        iInstanceid = pastCnt;
                        for (int ccnt = 0; ccnt < confLst.Count; ccnt++)
                        {
                            iInstanceid++;
                            internalname = confLst[ccnt].userid + confLst[ccnt].externalname + confLst[ccnt].confdate.ToShortDateString().Replace("/", ""); //;/,.- :

                            //FB 1919 Start
                            confNumName = 0;
                            if (PastInstanceLst.Contains(iInstanceid))
                                confNumName = (int)PastInstanceLst[iInstanceid];

                            if (confNumName <= 0)
                            {
                                confNumName = GetConfNumname();
                            }

                            if (ccnt == 0)
                            {
                                if (pastCnt == 0) //Edit all future instances
                                {
                                    confid = GetConfId(); //Generate new confid
                                    recurInfo.confuId = confNumName;
                                }
                                else
                                {
                                    confid = confLst[0].confid; //retain existing confid
                                    recurInfo.confuId = confLst[0].confnumname;
                                }


                                UpdateRecurDefuntInfo(confLst[0].confid, confid, ref recurInfo);//FB 2218
                            }
                            confLst[ccnt].confid = confid;
                            confLst[ccnt].confnumname = confNumName;
                            confLst[ccnt].instanceid = iInstanceid;
                            confLst[ccnt].internalname = internalname;
                            confLst[ccnt].IcalID = IcalID;
                            //FB 2441 starts
                            if (lstSyncAdjstmnts != null && lstSyncAdjstmnts.Count > 0)
                            {
                                vrmConfSyncMCUadjustment = lstSyncAdjstmnts.Where(syncadj => syncadj.instanceID == iInstanceid).FirstOrDefault();
                                if(vrmConfSyncMCUadjustment != null)
                                {
                                    confLst[ccnt].ESId = vrmConfSyncMCUadjustment.esID;
                                    confLst[ccnt].Etag = vrmConfSyncMCUadjustment.Etag;
                                    confLst[ccnt].DialString = vrmConfSyncMCUadjustment.DialString;
                                    vrmConfSyncMCUadjustment.deleteDecision = 0;

                                }
                            }
                            //FB 2441 ends
                            //FB 2426 Start
                            int count = 0, loginuserid = 0, errNo = 0;
                            if (guestRooms != null)
                            {
                                for (int ro = 0; ro < guestRooms.Count; ro++)
                                {
                                    if (guestRooms[ro].roomId == 0)
                                        count += 1;
                                }

                                if (count > 0)
                                {
                                    loginuserid = guestRooms[0].adminId;
                                    if (!licensecheck(ref count, ref loginuserid, ref organizationID, ref errNo))
                                    {
                                        myvrmEx = new myVRMException(errNo);
                                        obj.outXml = myvrmEx.FetchErrorMsg();
                                        return false;
                                    }
                                }
                            }
                            //FB 2426 End
                            m_vrmConfDAO.Save(confLst[ccnt]);

                            vConf = confLst[ccnt];
                            if (!InsertAdvAVParams(ref confAVParams, ref vConf))
                            {
                                myvrmEx = new myVRMException(552);
                                obj.outXml = myvrmEx.FetchErrorMsg();
                                return false;
                            }
                            //FB 2426 Start
                            if (confRooms != null || guestRooms != null || guestEndpoints != null)
                            {
                                if (!InsertLocations(ref confRooms, ref splitRooms, ref vConf, ref guestRooms, ref guestEndpoints,ref errNo))
                                {
                                    myvrmEx = new myVRMException(errNo);
                                    obj.outXml = myvrmEx.FetchErrorMsg();
                                    return false;
                                }
                            }
                            //FB 2426 End
                            if (confUser != null || addedGuests != null)
                            {
                                if (!InsertParty(ref confUser, ref addedGuests, ref vConf))
                                {
                                    myvrmEx = new myVRMException(553);
                                    obj.outXml = myvrmEx.FetchErrorMsg();
                                    return false;
                                }
                            }
                            if (fileList != null)
                            {
                                AttachmentSetup(ref vConf, ref icalAttachment);
                            }
                            if (confAttrs != null)
                            {
                                if (!InsertConfAttributes(ref confAttrs, ref vConf))
                                {
                                    myvrmEx = new myVRMException(554);
                                    obj.outXml = myvrmEx.FetchErrorMsg();
                                    return false;
                                }
                            }
                        }
                        if (!InsertRecurInfo(ref recurInfo))
                        {
                            myvrmEx = new myVRMException(551);
                            obj.outXml = myvrmEx.FetchErrorMsg();
                            return false;
                        }
                        #endregion
                    }
                    else
                    {
                        #region Normal Conference

                        confLst[0].IcalID = IcalID;
                        m_vrmConfDAO.clearFetch();
                        //FB 2426 Start
                        int count = 0, loginuserid = 0, errNo = 0;
                        if (guestRooms != null)
                        {
                            for (int ro = 0; ro < guestRooms.Count; ro++)
                            {
                                if (guestRooms[ro].roomId == 0)
                                    count += 1;
                            }

                            if (count > 0)
                            {
                                loginuserid = guestRooms[0].adminId;
                                if (!licensecheck(ref count, ref loginuserid, ref organizationID, ref errNo))
                                {
                                    myvrmEx = new myVRMException(errNo);
                                    obj.outXml = myvrmEx.FetchErrorMsg();
                                    return false;
                                }
                            }
                        }
                        //FB 2426 End
                        m_vrmConfDAO.Update(confLst[0]);

                        vConf = confLst[0];
                        if (!InsertAdvAVParams(ref confAVParams, ref vConf))
                        {
                            myvrmEx = new myVRMException(552);
                            obj.outXml = myvrmEx.FetchErrorMsg();
                            return false;
                        }
                        //FB 2426 Start
                        if (confRooms != null || guestRooms != null || guestEndpoints != null) 
                        {
                            if (!InsertLocations(ref confRooms, ref splitRooms, ref vConf, ref guestRooms, ref guestEndpoints,ref errNo))
                            {
                                myvrmEx = new myVRMException(errNo);
                                obj.outXml = myvrmEx.FetchErrorMsg();
                                return false;
                            }
                        }
                        //FB 2426 End
                        if (confUser != null || addedGuests != null)
                        {
                            if (!InsertParty(ref confUser, ref addedGuests, ref vConf))
                            {
                                myvrmEx = new myVRMException(553);
                                obj.outXml = myvrmEx.FetchErrorMsg();
                                return false;
                            }
                        }
                        if (fileList != null)
                        {
                            AttachmentSetup(ref vConf, ref icalAttachment);
                        }
                        if (confAttrs != null)
                        {
                            if (!InsertConfAttributes(ref confAttrs, ref vConf))
                            {
                                myvrmEx = new myVRMException(554);
                                obj.outXml = myvrmEx.FetchErrorMsg();
                                return false;
                            }
                        }
                        #endregion
                    }
                    #endregion
                }

                
                if (minimalWS)//FB 2342
                {
                    XmlDocument xd = new XmlDocument();
                    XMLRefactoringForAVSettings(ref xd, confRooms,vConf,confRooms.Count);

                    obj.inXml = xd.InnerXml;

                    SetAdvancedAVSettings(ref obj);
                }

                //FB 2441 start
                if (lstSyncAdjstmnts != null && lstSyncAdjstmnts.Count > 0)
                {
                    if (lstSyncAdjstmnts.Where(syncadj => syncadj.deleteDecision == 1).ToList().Count > 0)
                    {
                        m_IConfSyncMCUadjustmentsDao = m_confDAO.GetConfSyncMCUadjustmentsDao();
                       removeMCUadjustment = lstSyncAdjstmnts.Where(syncadj => syncadj.deleteDecision == 1).ToList();

                        for (int i = 0; i < removeMCUadjustment.Count; i++)
                        {
                            vrmConfSyncMCUadjustment = removeMCUadjustment[i];
                            m_IConfSyncMCUadjustmentsDao.Save(vrmConfSyncMCUadjustment);
                        }

                        //m_IConfSyncMCUadjustmentsDao.SaveOrUpdateList(lstSyncAdjstmnts.Where(syncadj => syncadj.DeleteDecision == 1).ToList());
                        
                        m_IConfSyncMCUadjustmentsDao = null;
                    }
 
                }
                //FB 2441 end

                obj.outXml = SetConferenceOutXML(ref confUser,ref confRooms);//FB 2426

            }
            catch (myVRMException me)
            {
                m_log.Error("SetConference: ", me);
                obj.outXml = "";
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("SetConference: ", e);
                obj.outXml = "";
                return false;
            }
            return true;
        }
        #endregion
        //Code added for 2457 exchange round trip - Start
        private string GetRoomEmailID(string id)
        {
            vrmRoom room = new vrmRoom();
            int ID = 0;
            int.TryParse(id, out ID);
            room = m_vrmRoomDAO.GetByRoomId(ID);
            return room.RoomQueue;
        }
        //Code added for 2457 exchange round trip - end
        #region BuildConfList
        /// <summary>
        /// BuildConfList
        /// </summary>
        /// <param name="vConf"></param>
        /// <param name="recurDates"></param>
        /// <param name="obj"></param>
        /// <param name="totalEndpoints"></param>
        /// <param name="confRooms"></param>
        /// <param name="splitRooms"></param>
        /// <returns></returns>
        private bool BuildConfList(ref vrmConference vConf, ref List<sRecurrDates> recurDates, ref vrmDataObject obj, ref int totalEndpoints, ref List<vrmConfRoom> confRooms, ref List<SplitRoom> splitRooms)
        {
            vrmConference recurInsConf = null;
            int errNo = 0;
            myVRMException myvrmEx = null;
            System.Data.DataTable conflictDt = new System.Data.DataTable();
            string addErrMesg = "";
            bool isConflict = false;
            DateTime confdate = DateTime.MinValue, confenddate = DateTime.MinValue, setupDate = DateTime.MinValue, tearDate = DateTime.MinValue;
            string roomList = "";
            string isVMRoom = "";
            bool isVMRoomSet = false;
            int iVMRoom = 0;
            try
            {
                if (confRooms != null)
                {
                    //FB 2448 Starts
                    if (confRooms.Count > 0)
                    {
                        for (int r = 0; r < confRooms.Count; r++)
                        {
                            if (confRooms[r].Room.IsVMR == 1)
                            {
                                isVMRoomSet = true;
                                iVMRoom++;
                            }
                        }
                    }
                    //FB 2448 Ends

                    for (int r = 0; r < confRooms.Count; r++)
                    {
                        if (r == 0)
                            roomList = confRooms[r].roomId.ToString();
                        else
                            roomList += "," + confRooms[r].roomId.ToString();
                    }
                }
                //FB 2448 Starts
                if ((vConf.isVMR == 1 || vConf.conftype == 10) && orgInfo.EnableVMR == 2) //FB 2262 //FB 2599
                {
                    if (!isVMRoomSet)
                    {
                        myvrmEx = new myVRMException(620);
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }
                }
                if (iVMRoom > 1)
                {
                    myvrmEx = new myVRMException(621);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }
                   
                //FB 2448 Ends

                confLst = new List<vrmConference>();
                if (isRecurring)
                {
                    #region Recurrence
                    if (recurDates != null)
                    {
                        #region Conflict Table
                        if (!conflictDt.Columns.Contains("startDate")) conflictDt.Columns.Add("startDate");
                        if (!conflictDt.Columns.Contains("startHour")) conflictDt.Columns.Add("startHour");
                        if (!conflictDt.Columns.Contains("startMin")) conflictDt.Columns.Add("startMin");
                        if (!conflictDt.Columns.Contains("startSet")) conflictDt.Columns.Add("startSet");
                        if (!conflictDt.Columns.Contains("durationMin")) conflictDt.Columns.Add("durationMin");
                        if (!conflictDt.Columns.Contains("conflict")) conflictDt.Columns.Add("conflict");
                        if (!conflictDt.Columns.Contains("conflictRooms")) conflictDt.Columns.Add("conflictRooms");
                        #endregion

                        confdate = confenddate = setupDate = tearDate = DateTime.MinValue;
                        isConflict = false;
                        System.Data.DataRow dr = null;
                        for (int ccnt = 0; ccnt < recurDates.Count; ccnt++)
                        {
                            if (ccnt >= vrmConfig.ConfReccurance) //Recurring instance should not exceed maximum limit
                                break;

                            recurInsConf = null;
                            recurInsConf = new vrmConference();

                            if (!SetConfParams(ref vConf, ref recurInsConf))
                                throw new myVRMException("Error in setting conference parameters.");

                            dr = conflictDt.NewRow();
                            dr["startDate"] = recurDates[ccnt].RecDatetime.ToShortDateString();
                            dr["startHour"] = recurDates[ccnt].StartHour;
                            dr["startMin"] = recurDates[ccnt].StartMin;
                            dr["startSet"] = recurDates[ccnt].StartSet;
                            dr["durationMin"] = recurDates[ccnt].Duration;
                            dr["conflict"] = "0";
                            dr["conflictRooms"] = "";

                            recurInsConf.confdate = recurDates[ccnt].RecDatetime;
                            recurInsConf.conftime = recurDates[ccnt].RecDatetime;
                            recurInsConf.duration = recurDates[ccnt].Duration;
                            recurInsConf.timezone = recurDates[ccnt].Timezone;

                            confdate = recurDates[ccnt].RecDatetime;
                            setupDate = recurDates[ccnt].RecDatetime.Add(new TimeSpan(0, 0, recurDates[ccnt].SetDuration, 0));
                            confenddate = recurDates[ccnt].RecDatetime.Add(new TimeSpan(0, 0, recurDates[ccnt].Duration, 0));
                            tearDate = confenddate.Subtract(new TimeSpan(0, 0, recurDates[ccnt].TearDuration, 0));

                            recurInsConf.SetupTime = setupDate;
                            recurInsConf.TearDownTime = tearDate;

                            //Check for account balance
                            if (!CheckFinancial(ref totalEndpoints, ref recurInsConf))
                            {
                                myvrmEx = new myVRMException(231);
                                obj.outXml = myvrmEx.FetchErrorMsg();
                                return false;
                            }
                            //Check for past datetime
                            if (!CheckCurrentTime(recurInsConf.confdate, recurInsConf.timezone, ref errNo))
                            {
                                myvrmEx = new myVRMException(errNo);
                                obj.outXml = myvrmEx.FetchErrorMsg();
                                return false;

                            }
                            //Check for System open hours and days
                            string errMes = "";
                            if (!IsConferenceScheduleable(ref errMes, ref recurInsConf))
                            {
                                // Oops! We don't support conferences at this time.
                                isConflict = true; //System Open Hours Conflict <conflict>2</conflict>
                                dr["conflict"] = "2"; //Timezone conflict
                                errNo = 253;
                                if (errMes.Trim().Length > 1)
                                    addErrMesg = errMes;
                            }

                            //this is equivalent to converting to GMT while inserting as happening in COM
                            //Get all the date time to GMT this must be done after the time verification for schedulable and current time as they are done in conf time zone
                            timeZone.changeToGMTTime(recurInsConf.timezone, ref confdate);
                            timeZone.changeToGMTTime(recurInsConf.timezone, ref setupDate);
                            timeZone.changeToGMTTime(recurInsConf.timezone, ref confenddate);
                            timeZone.changeToGMTTime(recurInsConf.timezone, ref tearDate);

                            recurInsConf.SetupTime = setupDate;
                            recurInsConf.TearDownTime = tearDate;
                            recurInsConf.confdate = confdate;
                            recurInsConf.conftime = confdate;

                            recurInsConf.confEnd = confdate.Add(new TimeSpan(0, 0, recurInsConf.duration, 0));

                            //Check for room conflicts if double booking is disabled
                            string conflictRooms = "";
                            if (orgInfo.doublebookingenabled == 0)
                            {
                                if (CheckForConflicts(ref roomList, ref splitRooms, ref conflictRooms, ref recurInsConf))
                                {
                                    isConflict = true; //Room Conflict <conflict>1</conflict>
                                    dr["conflict"] = "1"; //Room conflict
                                    dr["conflictRooms"] = "(" + conflictRooms + ")";
                                    errNo = 510;
                                }
                            }
                            if (CheckForServiceTypeConflicts(ref roomList, ref splitRooms, ref conflictRooms, ref recurInsConf))//FB 2219
                            {
                                isConflict = true;
                                dr["conflict"] = "1";
                                dr["conflictRooms"] = "(" + conflictRooms + ")";
                                errNo = 510;
                            }
							//FB 2609
                            if (IsMeetandGreet)
                            {
                                if (CheckForMeetandGreetBuffer(recurInsConf.SetupTime, recurInsConf.timezone))
                                {
                                    myvrmEx = new myVRMException(633);
                                    obj.outXml = myvrmEx.FetchErrorMsg();
                                    return false;

                                }
                            }
                            confLst.Add(recurInsConf);
                            conflictDt.Rows.Add(dr);
                        }
                        if (isConflict)
                        {
                            myvrmEx = new myVRMException(errNo);
                            string errXml = "";
                            if (errNo == 253)
                                errXml = myvrmEx.FetchErrorMsg(addErrMesg);
                            else
                                errXml = myvrmEx.FetchErrorMsg();

                            string conflictXml = RecurDateList(ref conflictDt);

                            XmlDocument cf = new XmlDocument();
                            cf.LoadXml(errXml);
                            XmlNode nd = null;
                            nd = cf.SelectSingleNode("//error/level");
                            nd.InnerXml = "C";
                            nd = cf.SelectSingleNode("error");
                            nd.InnerXml = nd.InnerXml + conflictXml;

                            obj.outXml = cf.InnerXml;
                            return false;
                        }
                        if (confLst.Count < 2)
                        {
                            myvrmEx = new myVRMException(432);
                            obj.outXml = myvrmEx.FetchErrorMsg();
                            return false;
                        }
                    }
                    #endregion
                }
                else
                {
                    #region Immediate/ Future Conference

                    confdate = confenddate = setupDate = tearDate = DateTime.MinValue;

                    //this is equivalent to converting to GMT while inserting as happening in COM
                    if (vConf.immediate == 0)
                    {
                        confdate = vConf.confdate;
                        setupDate = vConf.SetupTime;
                        tearDate = vConf.TearDownTime;
                        timeZone.changeToGMTTime(vConf.timezone, ref confdate);//to be converted to GMT after checking the schedulable and current time as they are done in conf timezone and not in GMT
                        timeZone.changeToGMTTime(vConf.timezone, ref setupDate); //to be converted to GMT after checking the schedulable and current time as they are done in conf timezone and not in GMT
                        timeZone.changeToGMTTime(vConf.timezone, ref tearDate); //to be converted to GMT after checking the schedulable and current time as they are done in conf timezone and not in GMT
                        vConf.confdate = confdate;
                        vConf.conftime = confdate;
                        vConf.SetupTime = setupDate;
                        vConf.TearDownTime = tearDate;
                    }

                    if (!CheckFinancial(ref totalEndpoints, ref vConf))
                    {
                        myvrmEx = new myVRMException(231);
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }
                    if (instanceEdit)
                    {
                        if (!CheckInstanceDuplication(ref vConf))
                        {
                            myvrmEx = new myVRMException(537);
                            obj.outXml = myvrmEx.FetchErrorMsg();
                            return false;
                        }
                    }
                    string conflictRooms = "";
                    if (orgInfo.doublebookingenabled == 0)
                    {
                        if (CheckForConflicts(ref roomList, ref splitRooms, ref conflictRooms, ref vConf))
                        {
                            conflictRooms = "(" + conflictRooms + ")";
                            myvrmEx = new myVRMException(241);
                            obj.outXml = myvrmEx.FetchErrorMsg(conflictRooms);
                            return false;
                        }
                    }
                    if (CheckForServiceTypeConflicts(ref roomList, ref splitRooms, ref conflictRooms, ref vConf))//FB 2219
                    {
                        conflictRooms = "(" + conflictRooms + ")";
                        myvrmEx = new myVRMException(241);
                        obj.outXml = myvrmEx.FetchErrorMsg(conflictRooms);
                        return false;
                    }
					//FB 2609
                    if (IsMeetandGreet)
                    {
                        if (CheckForMeetandGreetBuffer(vConf.SetupTime, vConf.timezone))//FB 2609
                        {
                            myvrmEx = new myVRMException(633);
                            obj.outXml = myvrmEx.FetchErrorMsg();
                            return false;
                        }
                    }
                    confLst.Add(vConf);
                    #endregion
                }
            }
            catch (myVRMException me)
            {
                m_log.Error("SetConference-BuildConfList: ", me);
                obj.outXml = "";
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("SetConference-BuildConfList: ", e);
                obj.outXml = ""; //Error 200 will be thrown
                return false;
            }
            return true;
        }
        #endregion

        #region DeleteConfEntities
        /// <summary>
        /// DeleteConfEntities
        /// </summary>
        /// <param name="pastCnt"></param>
        /// <param name="PastInstanceLst"></param>
        /// <param name="IcalID"></param>
        /// <returns></returns>
        private bool DeleteConfEntities(ref int pastCnt, ref Hashtable PastInstanceLst, ref string IcalID, ref List<vrmConfSyncMCUadjustments> lstSyncAdjstmnts)//FB 2441
        {
            vrmConfSyncMCUadjustments  vrmConfSyncMCUadjustment = null;
            try
            {
                string stmt = "";
                sqlCon = new ns_SqlHelper.SqlHelper(m_configPath);
                if (isRecurring)
                {
                    #region Recurrence Part

                    string insid = "0";
                    try
                    {
                        sqlCon.OpenConnection();

                        //Fetch ICAL ID //1782
                        stmt = " select IcalID from Conf_Conference_D where confid='" + confLst[0].confid + "'";
                        System.Data.DataSet ds = sqlCon.ExecuteDataSet(stmt);
                        if (ds != null)
                        {
                            if (ds.Tables.Count > 0)
                            {
                                if (ds.Tables[0].Rows.Count > 0)
                                {
                                    IcalID = ds.Tables[0].Rows[0]["IcalID"].ToString();
                                }
                            }
                        }

                        stmt = "select count(*) as PastInstanceCnt from Conf_Conference_D "
                        + " where  confid='" + confLst[0].confid + "' AND ( datediff(minute,dbo.changeTOGMTtime((SELECT timezone FROM sys_settings_D),getdate()), "
                        + " confdate) < 5) ";

                        object retVal = sqlCon.ExecuteScalar(stmt);
                        if (retVal != null)
                        {
                            pastCnt = (int)retVal;
                            insid = retVal.ToString();
                        }

                        stmt = "select (ROW_NUMBER() OVER (ORDER BY confdate ASC) + " + insid + ") AS newInstanceid, confnumname, ESID,etag, dialString from " //FB 1919 - corrected //FB 2441
                        + " conf_conference_d where deleted = 0 and confid =" + confLst[0].confid + " and instanceid > " + insid + " order by confdate ";

                        ds = null;
                        ds = sqlCon.ExecuteDataSet(stmt);
                        if (ds != null)
                        {
                            if (ds.Tables.Count > 0)
                            {
                                if (ds.Tables[0].Rows.Count > 0)
                                {
                                    int confnumname = 0, newinstanceid = 0;
                                    for (int k = 0; k < ds.Tables[0].Rows.Count; k++)
                                    {
                                        confnumname = 0; newinstanceid = 0;
                                        int.TryParse(ds.Tables[0].Rows[k]["confnumname"].ToString(), out confnumname);
                                        int.TryParse(ds.Tables[0].Rows[k]["newInstanceid"].ToString(), out newinstanceid);
                                        
                                        if(ds.Tables[0].Rows[k]["ESID"]!= null && ds.Tables[0].Rows[k]["ESID"].ToString().Trim() != "")
                                        {
                                            if(lstSyncAdjstmnts == null)
                                                lstSyncAdjstmnts = new List<vrmConfSyncMCUadjustments>();

                                            vrmConfSyncMCUadjustment = new vrmConfSyncMCUadjustments();
                                            vrmConfSyncMCUadjustment.Confid = confLst[0].confid;
                                            vrmConfSyncMCUadjustment.instanceID = newinstanceid;
                                            vrmConfSyncMCUadjustment.Confnumname = confnumname;
                                            vrmConfSyncMCUadjustment.esID = ds.Tables[0].Rows[k]["ESID"].ToString().Trim();
                                            vrmConfSyncMCUadjustment.Etag = ds.Tables[0].Rows[k]["etag"].ToString().Trim();
                                            vrmConfSyncMCUadjustment.DialString = ds.Tables[0].Rows[k]["dialString"].ToString().Trim();
                                            vrmConfSyncMCUadjustment.deleteDecision = 1;

                                            lstSyncAdjstmnts.Add(vrmConfSyncMCUadjustment);
                                        }
                                        

                                        if (!PastInstanceLst.Contains(newinstanceid))
                                            PastInstanceLst.Add(newinstanceid, confnumname);
                                    }
                                }
                            }
                        }
                        ds = null;
                        sqlCon.OpenTransaction();

                        stmt = " Update Conf_Room_D  set disabled = 1 where confid ='" + confLst[0].confid + "' and instanceid > " + insid + ";"
                        + " Delete from Conf_Room_D where confid ='" + confLst[0].confid + "' and instanceid > " + insid + " and roomid in (select distinct roomid from conf_room_d where confid='" + confLst[0].confid + "');"
                        + " delete from  Conf_User_D where confid = '" + confLst[0].confid + "' and instanceid > " + insid + ";"
                        + " delete from Conf_Group_D where confid = '" + confLst[0].confid + "';"
                        + " delete from Conf_RecurInfo_D where confid ='" + confLst[0].confid + "';"
                        + " DELETE FROM Conf_AdvAVParams_D WHERE confid = '" + confLst[0].confid + "' and instanceid > " + insid + ";"
                        + " DELETE FROM Conf_Bridge_D WHERE confid = '" + confLst[0].confid + "' and instanceid > " + insid + ";"
                        + " DELETE FROM Conf_CustomAttr_D WHERE confid = '" + confLst[0].confid + "' and instanceid > " + insid + ";"
                        + " DELETE FROM Conf_Conference_D WHERE confid = '" + confLst[0].confid + "' and instanceid > " + insid + ";"
                        + " DELETE FROM Conf_RoomSplit_D WHERE confid = '" + confLst[0].confid + "' and instanceid > " + insid + ";"
                        + " DELETE FROM Conf_Attachments_D WHERE confid = '" + confLst[0].confid + "' and instanceid > " + insid + ";"
                        + " DELETE FROM Conf_Message_D WHERE confid = '" + confLst[0].confid + "' and instanceid > " + insid + ";"; //FB 2486

                        int strExec = sqlCon.ExecuteNonQuery(stmt);
                        sqlCon.CommitTransaction();
                        sqlCon.CloseConnection();
                        sqlCon = null;
                    }
                    catch (Exception)
                    {
                        sqlCon.RollBackTransaction();
                        sqlCon.CloseConnection();
                        sqlCon = null;
                    }
                    #endregion
                }
                else
                {
                    if (instanceEdit)
                    {
                        #region Instance Edit
                        try
                        {
                            sqlCon.OpenConnection();

                            //Fetch ICAL ID //1782
                            stmt = " select IcalID from Conf_Conference_D where confid='" + confLst[0].confid + "' and instanceid='" + confLst[0].instanceid + "'";
                            System.Data.DataSet ds = sqlCon.ExecuteDataSet(stmt);
                            if (ds != null)
                            {
                                if (ds.Tables.Count > 0)
                                {
                                    if (ds.Tables[0].Rows.Count > 0)
                                    {
                                        IcalID = ds.Tables[0].Rows[0]["IcalID"].ToString();
                                    }
                                }
                            }

                            // Before deleting the existing conference record,
                            // we have to know if modifying this instance
                            // causes the entire recurring conference to go into
                            // custom mode or not
                            int res = 1;
                            stmt = "DECLARE @cd AS DATETIME "
                                + "SELECT @cd = confdate FROM Conf_Conference_D WHERE confid ='" + confLst[0].confid + "'"
                                + " AND instanceid ='" + confLst[0].instanceid + "'"
                                + " IF ('" + confLst[0].confdate + "')"
                                + " = @cd BEGIN SELECT 1 AS res END ELSE BEGIN SELECT 0 AS res END";

                            object retVal = sqlCon.ExecuteScalar(stmt);
                            if (retVal != null)
                                res = (int)retVal;

                            // Before deleting the existing conference record,
                            // we have to know if modifying this instance
                            // causes the entire recurring conference to go into
                            // custom mode
                            // Irrespective of what kind of change is made to an instance...
                            // the dirty bit on the conference is set. This means that
                            // the recurring conference is no longer the same as the original
                            sqlCon.OpenTransaction();

                            stmt = " Update Conf_Room_D  set disabled = 1 where confid ='" + confLst[0].confid + "' and instanceid ='" + confLst[0].instanceid + "';" //check with gowniyan
                            + " Delete from Conf_Room_D where confid ='" + confLst[0].confid + "' and instanceid = " + confLst[0].instanceid + " and roomid in (select distinct roomid from conf_room_d where confid='" + confLst[0].confid + "' and instanceid ='" + confLst[0].instanceid + "');"
                            + " delete from Conf_User_D where confid = '" + confLst[0].confid + "' and instanceid ='" + confLst[0].instanceid + "';"
                            + " delete from Conf_Group_D where confid = '" + confLst[0].confid + "';"
                            + " DELETE FROM Conf_AdvAVParams_D WHERE confid = '" + confLst[0].confid + "' and instanceid ='" + confLst[0].instanceid + "';"
                            + " DELETE FROM Conf_Bridge_D WHERE confid = '" + confLst[0].confid + "' and instanceid ='" + confLst[0].instanceid + "';"
                            + " DELETE FROM Conf_CustomAttr_D WHERE confid = '" + confLst[0].confid + "' and instanceid ='" + confLst[0].instanceid + "';"
                            + " DELETE FROM Conf_RoomSplit_D WHERE confid = '" + confLst[0].confid + "' and instanceid ='" + confLst[0].instanceid + "';"
                            + " Update Conf_RecurInfo_D set dirty=1 WHERE confid ='" + confLst[0].confid + "';"
                            + " DELETE FROM Conf_Attachments_D WHERE confid = '" + confLst[0].confid + "' and instanceid =' " + confLst[0].instanceid + "';" //FB 2164
                            + " DELETE FROM Conf_Message_D WHERE confid = '" + confLst[0].confid + "' and instanceid > " + confLst[0].instanceid + ";"; //FB 2486

                            if (res == 0) //Change recurrence pattern as custom if date/time of this instance is changed
                                stmt += "Update Conf_RecurInfo_D set recurtype=5 WHERE confid ='" + confLst[0].confid + "';";

                            int strExec = sqlCon.ExecuteNonQuery(stmt);
                            sqlCon.CommitTransaction();
                            sqlCon.CloseConnection();
                            sqlCon = null;
                        }
                        catch (Exception e)
                        {
                            sqlCon.RollBackTransaction();
                            sqlCon.CloseConnection();
                            sqlCon = null;
                        }
                        #endregion
                    }
                    else
                    {
                        #region Normal Conference
                        try
                        {
                            sqlCon.OpenConnection();
                            //FB 2164 start
                            //Need to maintain IcalID even if the conference is single.
                            stmt = " select IcalID from Conf_Conference_D where confid='" + confLst[0].confid + "' and instanceid='" + confLst[0].instanceid + "'";
                            System.Data.DataSet ds = sqlCon.ExecuteDataSet(stmt);
                            if (ds != null)
                            {
                                if (ds.Tables.Count > 0)
                                {
                                    if (ds.Tables[0].Rows.Count > 0)
                                    {
                                        IcalID = ds.Tables[0].Rows[0]["IcalID"].ToString();
                                    }
                                }
                            }
                            //FB 2164 end
                            sqlCon.OpenTransaction();

                            stmt = " Delete from Conf_Room_D where confid ='" + confLst[0].confid + "';"
                            + " delete from  Conf_User_D where confid = '" + confLst[0].confid + "';"
                            + " delete from Conf_Group_D where confid = '" + confLst[0].confid + "';"
                            + " delete from Conf_RecurInfo_D where confid ='" + confLst[0].confid + "';"
                            + "DELETE FROM Conf_AdvAVParams_D WHERE confid = '" + confLst[0].confid + "';"
                            + "DELETE FROM Conf_Bridge_D WHERE confid = '" + confLst[0].confid + "';"
                            + "DELETE FROM Conf_CustomAttr_D WHERE confid = '" + confLst[0].confid + "';"
                            + " DELETE FROM Conf_Attachments_D WHERE confid = '" + confLst[0].confid + "';"
                            + " DELETE FROM Conf_Message_D WHERE confid = '" + confLst[0].confid + "'"; //FB 2486

                            //need to delete instances > 1 in case of recur conf into normal
                            int strExec = sqlCon.ExecuteNonQuery(stmt);
                            sqlCon.CommitTransaction();
                            sqlCon.CloseConnection();
                            sqlCon = null;
                        }
                        catch (Exception e)
                        {
                            sqlCon.RollBackTransaction();
                            sqlCon.CloseConnection();
                            sqlCon = null;
                        }
                        #endregion
                    }
                }
            }
            catch (Exception e)
            {
                throw new myVRMException("Error in SetConference - DeleteConfEntities", e);
            }
            return true;
        }
        #endregion

        #region GetConfNumname
        /// <summary>
        /// GetConfNumname
        /// </summary>
        /// <returns></returns>
        private int GetConfNumname()
        {
            int confNumName = 0;
            try
            {
                string cfst = "SELECT max(vc.confnumname) FROM myVRM.DataLayer.vrmConference vc";
                IList result = m_vrmConfDAO.execQuery(cfst);
				//Empty DB Issue FB 2164
                if (result[0] != null)
                    int.TryParse(result[0].ToString(), out confNumName);

                confNumName = confNumName + 1;
            }
            catch (Exception)
            {
                if (confNumName <= 0)
                    confNumName = 1;
            }
            return confNumName;
        }
        #endregion

        #region GetConfId
        /// <summary>
        /// GetConfId
        /// </summary>
        /// <returns></returns>
        private int GetConfId()
        {
            int confid = 0;
            try
            {
                string cfst = "SELECT max(vc.confid) FROM myVRM.DataLayer.vrmConference vc";
                IList result = m_vrmConfDAO.execQuery(cfst);
				//Empty DB Issue FB 2164
                if (result[0] != null)
                    int.TryParse(result[0].ToString(), out confid);
                confid = confid + 1;
            }
            catch (Exception)
            {
                if (confid <= 0)
                    confid = 1;
            }
            return confid;
        }
        #endregion

        #region ParseConferenceXml
        /// <summary>
        /// ParseConferenceXml
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="confRooms"></param>
        /// <param name="confUser"></param>
        /// <param name="splitRooms"></param>
        /// <param name="addedGuests"></param>
        /// <param name="totalInviteed"></param>
        /// <param name="totalEndpoints"></param>
        /// <param name="conflictRooms"></param>
        /// <param name="recurDates"></param>
        /// <param name="recurInfo"></param>
        /// <param name="confAVParams"></param>
        /// <param name="confAttrs"></param>
        /// <param name="vConf"></param>
        /// <param name="icalAttachment"></param>
        /// <returns></returns>
        private bool ParseConferenceXml(ref vrmDataObject obj, ref List<vrmConfRoom> confRooms, ref List<vrmConfUser> confUser
           , ref List<SplitRoom> splitRooms, ref  List<vrmGuestUser> addedGuests, ref int totalInviteed, ref int totalEndpoints
           , ref string conflictRooms, ref List<sRecurrDates> recurDates, ref vrmRecurInfo recurInfo, ref vrmConfAdvAvParams confAVParams
           , ref List<vrmConfAttribute> confAttrs, ref vrmConference vConf, ref String icalAttachment, ref List<vrmRoom> guestRooms, ref List<vrmEndPoint> guestEndpoints) //FB 2426
        {
            XmlDocument xd;
            XmlNode node;
            int errNo = 0;
            string addErrMesg = "";
            myVRMException myvrmEx = null;

            XmlNodeList roomNodes = null;
            XmlNodeList userNodes = null;
            XmlNodeList splitRoomNodes = null;
            XmlNodeList guestRoomNodes = null;//FB 2426
            Int32 confType = 0;
            Int32 isVMR = 0;//FB 2376
            try
            {
                if (DateTime.Now > sysSettings.ExpiryDate)
                {
                    myvrmEx = new myVRMException(250);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }

                if (obj.inXml == "")
                    throw new myVRMException("Invalid input XML.");

                xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                organizationID = 11; //default organization
                node = xd.SelectSingleNode("//conference/organizationID");
                if (node == null)
                    throw new myVRMException("Input xml error. Tag <organizationID> not present.");

                Int32.TryParse(node.InnerXml.Trim(), out organizationID);

                node = xd.SelectSingleNode("//conference/userEMail"); //FB 2342
                if (node != null)
                {
                    minimalWS = true;
                    if(!XMLRefactoring(ref xd))
                        throw new myVRMException("Issues with WS structure.");
                }

                //FB 2274 Starts
                node = xd.SelectSingleNode("//conference/multisiloOrganizationID");
                string multisiloOrgID = "";
                int mutliOrgID = 0;
                if (node != null)
                    multisiloOrgID = node.InnerXml.Trim();
                Int32.TryParse(multisiloOrgID, out mutliOrgID);
                if (mutliOrgID > 11)
                    organizationID = mutliOrgID;
                //FB 2274 Ends
                if (organizationID < 11)
                {
                    myvrmEx = new myVRMException(423);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }

                orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);
                if (orgInfo == null)
                {
                    myvrmEx = new myVRMException(423);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//conference/userID");
                if (node == null)
                    throw new myVRMException("UserID missing.Operation aborted or UserID not supplied.");

                int loginUserid = 0;
                Int32.TryParse(node.InnerXml.Trim(), out loginUserid);
                if (loginUserid <= 0)
                {
                    myvrmEx = new myVRMException(201);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }
                //FB 2544 - Requestor starts
                int requestorID = loginUserid;
                node = xd.SelectSingleNode("//conference/requestorID");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out requestorID);
                //FB 2544 - Requestor ends 
                node = xd.SelectSingleNode("//conference");
                if (node == null)
                    throw new myVRMException("Input xml error. Tag <conference> not present.");

                node = xd.SelectSingleNode("//conference/confInfo");
                if (node == null)
                    throw new myVRMException("Input xml error. Tag <confInfo> not present.");

                node = xd.SelectSingleNode("//conference/confInfo/confID");
                CConfID cID = new CConfID(node.InnerText.Trim());

                if (cID.ID == 0 && cID.instance == 0)
                    newConf = true;
                else
                    newConf = false;

                if (newConf)
                {
                    vConf = new vrmConference();
                    vConf.deleted = 0;
                    vConf.ConfDeptID = 0;
                    vConf.confid = cID.ID;
                    vConf.instanceid = cID.instance;
                    vConf.ConfMode = (int)eConfMode.CONF_NEW;
                    vConf.orgId = organizationID;
                }
                else //Edit Mode
                {
                    List<ICriterion> criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("confid", cID.ID));

                    if (cID.RecurMode == 0)
                        criterionList.Add(Expression.Eq("instanceid", cID.instance));

                    List<vrmConference> recConfs = m_vrmConfDAO.GetByCriteria(criterionList, true);

                    if (recConfs == null)
                        throw new myVRMException("Error in fetching recurring conference.");

                    if (recConfs.Count <= 0)
                        throw new myVRMException("Error in fetching recurring conference.");

                    vConf = recConfs[0];
                    vConf.ConfMode = (int)eConfMode.CONF_EDIT;
                }

                vConf.userid = requestorID; //FB 2544 - Requestor
                vConf.loginUser = loginUserid; //FB 2544 - Requestor
                node = xd.SelectSingleNode("//conference/confInfo/confName");
                if (node == null)
                    throw new myVRMException("Input xml error. Tag <confName> not present.");

                if (node.InnerText.Trim() == "")
                {
                    myvrmEx = new myVRMException(202);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }
                vConf.externalname = node.InnerText.Trim();

                node = xd.SelectSingleNode("//conference/confInfo/confHost");
                if (node == null)
                    throw new myVRMException("Input xml error. Tag <confHost> not present.");

                int confHostId = 0;
                int.TryParse(node.InnerText.Trim(), out confHostId);
                if (confHostId <= 0)
                    throw new myVRMException("Conference Host not found. Please contact Technical Support.");

                vConf.owner = confHostId;

                node = xd.SelectSingleNode("//conference/confInfo/confOrigin"); //conference origin - web/outlook/lotus
                int confOrigin = 0;
                if (node != null)
                    int.TryParse(node.InnerText, out confOrigin);

                if (confOrigin < 0) confOrigin = 0;

                vConf.ConfOrigin = confOrigin;

                node = xd.SelectSingleNode("//conference/confInfo/confPassword");
                if (node != null)
                    vConf.password = node.InnerText.Trim();

                //FB 2376
                node = xd.SelectSingleNode("//conference/confInfo/isVMR");
                if (node != null)
                    Int32.TryParse(node.InnerText, out isVMR);
                vConf.isVMR = isVMR;

                //FB 2342
                vConf.IcalID = "";
                node = xd.SelectSingleNode("//conference/confInfo/IcalID");
                if (node != null)
                    vConf.IcalID = node.InnerText;
                    
                //FB 2342

                node = xd.SelectSingleNode("//conference/confInfo/immediate");
                int immediate = 0;
                int.TryParse(node.InnerText, out immediate);
                if (immediate < 0)
                    immediate = 0;

                if (immediate == 1)
                {
                    vConf.immediate = 1;
                    vConf.status = 0;
                }
                else
                    vConf.immediate = 0;

                node = xd.SelectSingleNode("//conference/confInfo/recurring");
                int recurring = 0;
                int.TryParse(node.InnerText, out recurring);
                if (recurring < 0)
                    recurring = 0;
                if (newConf)
                {
                    instanceEdit = false;
                    vConf.recuring = recurring;
                    if (recurring == 1)
                        isRecurring = true;
                    else
                        isRecurring = false;
                }
                else
                {
                    if (recurring == 1) //Recurrence EditAll & Normal to Recurrence on edit
                    {
                        isRecurring = true;
                        instanceEdit = false;
                        vConf.recuring = 1;
                    }
                    else //Normal Edit or Recurrence instance edit
                    {
                        if (vConf.recuring == 1)
                        {
                            if (cID.RecurMode == 0) //Recurrence instance edit
                            {
                                instanceEdit = true;
                                isRecurring = false;
                            }
                        }
                        else //Normal Conference Edit
                        {
                            isRecurring = false;
                            instanceEdit = false;
                        }
                    }
                }

                string recurringText = "";
                node = xd.SelectSingleNode("//conference/confInfo/recurringText");
                if (node != null)
                    recurringText = node.InnerText.Trim();

                string timeset = "", timehour = "", timemin = "", confdate = "";

                //FB 2609 Start                
                node = xd.SelectSingleNode("//conference/confInfo/MeetandGreetBuffer");
		        if (node != null)
	                int.TryParse(node.InnerText, out MeetandGreetBuffer);
                //FB 2609 End


                if (recurring == 0)
                {
                    node = xd.SelectSingleNode("//conference/confInfo/startDate");
                    confdate = node.InnerText.Trim();

                    node = xd.SelectSingleNode("//conference/confInfo/startHour");
                    timehour = node.InnerText.Trim();
                    if (timehour == "")
                        timehour = "00";

                    node = xd.SelectSingleNode("//conference/confInfo/startMin");
                    timemin = node.InnerText.Trim();

                    if (timemin == "")
                        timemin = "00";

                    node = xd.SelectSingleNode("//conference/confInfo/startSet");
                    timeset = node.InnerText.Trim();
                    if (timeset == "")
                        timeset = "AM";

                    //conference time 
                    string conftime = timehour + ":" + timemin + ":45 " + timeset;

                    //combined time = date + time 
                    string confsttime = confdate + " " + conftime;
                    DateTime confdatetime;
                    DateTime.TryParse(confsttime, out confdatetime);

                    node = xd.SelectSingleNode("//conference/confInfo/timeZone");

                    int tzID = 26; //default EST
                    int.TryParse(node.InnerText, out tzID);
                    if (tzID <= 0)
                        tzID = sysSettings.TimeZone;
                    vConf.timezone = tzID;

                    vConf.confdate = confdatetime;
                    vConf.conftime = confdatetime;

                    node = xd.SelectSingleNode("//conference/confInfo/setupDateTime");
                    DateTime setupDate = DateTime.MinValue;
                    if (node != null)
                        DateTime.TryParse(node.InnerText, out setupDate);

                    if (setupDate <= DateTime.MinValue)
                        setupDate = confdatetime;

                    vConf.SetupTime = setupDate;

                    node = xd.SelectSingleNode("//conference/confInfo/teardownDateTime");
                    DateTime teardownDate = DateTime.MinValue;// NUll Check
                    if (node != null)// NUll Check
                        DateTime.TryParse(node.InnerText, out teardownDate);
                    vConf.TearDownTime = teardownDate;

                    string timeFormat = ""; //Need to verify
                    node = xd.SelectSingleNode("//conference/confInfo/timeFormat");
                    if (node != null)
                        timeFormat = node.InnerText.Trim();

                    node = xd.SelectSingleNode("//conference/confInfo/durationMin");
                    int durationMin = 0;
                    int.TryParse(node.InnerText, out durationMin);
                    if (durationMin < 15)
                    {
                        myvrmEx = new myVRMException(536);
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }
                    vConf.duration = durationMin;


                    if(teardownDate == DateTime.MinValue)//NUll check
                        vConf.TearDownTime = vConf.confdate.AddMinutes(vConf.duration);
                }
                DateTime sysdate = DateTime.Now;
                timeZone.changeToGMTTime(sysSettings.TimeZone, ref sysdate);
                vConf.settingtime = sysdate;
                vConf.LastRunDateTime = sysdate;

                node = xd.SelectSingleNode("//conference/confInfo/createBy");
                confType = 0;
                int.TryParse(node.InnerText, out confType);
                if (confType <= 0)
                    throw new myVRMException("Invalid conference type.");

                vConf.conftype = confType;

                //FB 2376  PIM Conf - VMR Type - start
                if (confType == 10)
                {
                    vConf.conftype = 2;
                    vConf.isVMR = 1;
                }
                //FB 2376  PIM Conf - VMR Type - end

                //FB 2544 starts
                int Startmode = 0;
                node = xd.SelectSingleNode("//conference/confInfo/StartMode");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out Startmode);
                vConf.StartMode = Startmode;
                //FB 2544 ends

                //FB 2595 Start
                int ConfSecured = 0;
                node = xd.SelectSingleNode("//conference/confInfo/Secured");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out ConfSecured);
                vConf.Secured = ConfSecured;
                //FB 2595 ends

                if (newConf)
                    vConf.CreateType = 0;
                else
                    vConf.CreateType = 1;

                //Allowed conference types in Org Options...
                if (!CheckForConftype(confType, ref errNo))
                {
                    myvrmEx = new myVRMException(errNo);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }

                confScheduler = null;
                confScheduler = m_vrmUserDAO.GetByUserId(loginUserid);
                if (confScheduler == null)
                {
                    myvrmEx = new myVRMException(201);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }

                confHost = null;
                if (loginUserid != confHostId)
                {
                    confHost = m_vrmUserDAO.GetByUserId(confHostId);
                    if (confHost == null)
                    {
                        myvrmEx = new myVRMException(201);
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }
                }
                else
                {
                    confHost = confScheduler;
                }

                if (vConf.immediate == 1)//Immediate conference
                {
                    //If Immediate get the User time zone
                    DateTime serverTime = DateTime.Now;
                    timeZone.changeToGMTTime(sysSettings.TimeZone, ref serverTime);
                    vConf.confdate = serverTime;
                    vConf.conftime = serverTime;
                    vConf.timezone = sysSettings.TimeZone; // FB 2545
                    vConf.SetupTime = serverTime;
                    vConf.confEnd = serverTime.Add(new TimeSpan(0, 0, vConf.duration, 0));
                    vConf.TearDownTime = vConf.confEnd;
                }
                // Abhi: 9/29/2004
                // At this point, we have determined the starttime and timezone of the 
                // conference. We need to prevent all conferences from being scheduled
                // when the system is off. i.e there is no support staff on certain off
                // days during certain off hours.
                //
                // as per P&G this can be overridden (default to time check)
                //
                node = xd.SelectSingleNode("//conference/confInfo/timeCheck");
                string timeCheck = "";
                if(node != null)
                    timeCheck = node.InnerText.Trim();
                if (timeCheck == "")
                    timeCheck = "0";

                if (recurring == 0)
                {
                    addErrMesg = "";
                    if (timeCheck == "0")
                    {
                        if (!IsConferenceScheduleable(ref addErrMesg, ref vConf))
                        {
                            // Oops! We don't support conferences at this time.
                            myvrmEx = new myVRMException(253);
                            obj.outXml = myvrmEx.FetchErrorMsg(addErrMesg);
                            return false;
                        }
                    }

                    if (vConf.immediate == 0) //Future Conference
                    {
                        if (!CheckCurrentTime(vConf.confdate, vConf.timezone, ref errNo))
                        {
                            myvrmEx = new myVRMException(errNo);
                            obj.outXml = myvrmEx.FetchErrorMsg();
                            return false;

                        }
                    }
                }

                node = xd.SelectSingleNode("//conference/confInfo/description");
                if (node != null)
                    vConf.description = m_UtilFactory.ReplaceInXMLSpecialCharacters(node.InnerText.Trim()); //FB 2236

                node = xd.SelectSingleNode("//conference/confInfo/publicConf");
                int publicConf = 0;
                int.TryParse(node.InnerText, out publicConf);
                if (publicConf < 0)
                    publicConf = 0;
                vConf.isPublic = publicConf;

                node = xd.SelectSingleNode("//conference/confInfo/dynamicInvite");
                int dynamicinvite = 0;
                int.TryParse(node.InnerText, out dynamicinvite);
                if (dynamicinvite < 0)
                    dynamicinvite = 0;
                vConf.dynamicinvite = dynamicinvite;

                node = xd.SelectSingleNode("//conference/confInfo/ServiceType");//FB 2219
                if (node != null)//FB 2219(Final)
                {
                    int stID = -1;
                    int.TryParse(node.InnerText, out stID);
                    vConf.ServiceType = stID;
                }
                //FB 2632 Starts
                int AVSupport = 0, MeetGrt = 0, CongMonitr = 0, DedicatedVNOC = 0;
                node = xd.SelectSingleNode("//conference/confInfo/ConciergeSupport/OnSiteAVSupport");//FB 2219
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out AVSupport);
                vConf.OnSiteAVSupport = AVSupport;

                node = xd.SelectSingleNode("//conference/confInfo/ConciergeSupport/MeetandGreet");//FB 2219
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out MeetGrt);
                vConf.MeetandGreet = MeetGrt;


                IsMeetandGreet = false;
                if (vConf.immediate == 0 || (vConf.recuring == 1 && vConf.instanceid < 2))
                {
                    if (MeetGrt == 1)
                        IsMeetandGreet = true;
                }

                node = xd.SelectSingleNode("//conference/confInfo/ConciergeSupport/ConciergeMonitoring");//FB 2219
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out CongMonitr);
                vConf.ConciergeMonitoring = CongMonitr;

                node = xd.SelectSingleNode("//conference/confInfo/ConciergeSupport/DedicatedVNOCOperator");//FB 2219
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out DedicatedVNOC);
                vConf.DedicatedVNOCOperator = DedicatedVNOC;

                
                //FB 2501 Starts
                int confVNOC = 0;
                node = xd.SelectSingleNode("//conference/confInfo/ConciergeSupport/VNOCOperatorID");
                if (node != null)
                    Int32.TryParse(node.InnerText.ToString(), out confVNOC);

                vConf.confVNOC = confVNOC;

                if (DedicatedVNOC > 0 && confVNOC < 11)
                {
                    myvrmEx = new myVRMException(634);
                    obj.outXml = myvrmEx.FetchErrorMsg(addErrMesg);
                    return false;
                }
                //FB 2501 End
                //FB 2632 Ends

                //FB 2539 Start
                if (!m_Search.CheckApproverRights(confVNOC.ToString()))
                {
                    myvrmEx = new myVRMException(630);
                    obj.outXml = myvrmEx.FetchErrorMsg(addErrMesg);
                    return false;
                }
                //FB 2539 End
                //FB 2501 End

                //FB 2377 - Starts
                //node = xd.SelectSingleNode("//conference/confInfo/ConceirgeSupport");//FB 2341
                //if (node != null)
                //    vConf.ConceirgeSupport = node.InnerText.Trim();
                //FB 2377 - End

                //Advanced AV Params
                XmlNode advParamNode = xd.SelectSingleNode("//conference/confInfo/advAVParam");
                LoadAdvAVParams(ref confAVParams, advParamNode, confOrigin);//FB 2429

                //FB 2376  PIM Conf - VMR Type - end
                if (confType == 10)
                {
                    confAVParams.externalBridge = confHost.ExternalVideoNumber;
                    confAVParams.internalBridge = confHost.InternalVideoNumber;
                }
                //FB 2376  PIM Conf - VMR Type - end

                roomNodes = xd.SelectNodes("//conference/confInfo/locationList/selected/level1ID");
                guestRoomNodes = xd.SelectNodes("//conference/confInfo/ConfGuestRooms/ConfGuestRoom");//FB 2426
                userNodes = xd.SelectNodes("//conference/confInfo/partys/party");//Null Cehck
                splitRoomNodes = xd.SelectNodes("//conference/confInfo/locationList/split/locations/location");
                XmlNodeList fileNodes = xd.SelectNodes("//conference/confInfo/fileUpload/file");

                fileList = new List<string>();
                for (Int32 f = 0; f < fileNodes.Count; f++)
                    fileList.Add(fileNodes[f].InnerText.Trim());

                XmlNode icalNode = xd.SelectSingleNode("//conference/confInfo/ICALAttachment");
                if(icalNode != null)//NUll Check
                    icalAttachment = icalNode.InnerText;

                XmlNodeList confAttrNodes = xd.SelectNodes("//conference/confInfo/CustomAttributesList/CustomAttribute");
                LoadConfAttributes(ref confAttrNodes, ref confAttrs);
                if (!LoadConferenceEndpoints(ref roomNodes, ref splitRoomNodes, ref userNodes, ref confRooms, ref splitRooms, ref confUser, ref addedGuests, ref totalInviteed, ref totalEndpoints, ref confType, ref errNo,ref guestRoomNodes, ref guestRooms, ref guestEndpoints)) //FB 2426
                {
                    myvrmEx = new myVRMException(errNo);
                    obj.outXml = myvrmEx.FetchErrorMsg(addErrMesg); //FB 2164
                    return false;
                }
                int connect2 = 0;
                //Connect2 = 0 - Regular conference , not point-to-point.
                //Connect2 = 1 - Point-2-Point conference , 2 party call   
                //Connect2 = 3 - Point-2-Point conference , 3 party call    ( New Addition)

                if (totalEndpoints == 2)
                    connect2 = 1;
                else if (totalEndpoints == 3)
                    connect2 = 3;
                else
                    connect2 = 0;

                vConf.connect2 = connect2;
                vConf.linerate = confAVParams.linerateID;
                vConf.videosession = confAVParams.videoSession;
                vConf.lecturemode = confAVParams.lectureMode;
                vConf.videolayout = confAVParams.videoLayoutID;
                vConf.continous = 0;
                vConf.transcoding = 0;
                vConf.videoprotocol = 0;

                if (isRecurring)
                {
                    recurInfo = new vrmRecurInfo();
                    recurInfo.RecurringPattern = recurringText;

                    if (!RecurringDates(ref xd, ref errNo, ref recurDates, ref recurInfo))
                    {
                        myvrmEx = new myVRMException(errNo);
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }

                    if (xd.SelectSingleNode("//conference/defunctPattern") != null)//FB 2218
                    {
                        defunctRecurInfo = new vrmRecurInfoDefunct();
                        if (!GetdefunctPatternInfo(ref xd, ref errNo, ref recurDates, ref defunctRecurInfo))
                        {
                            myvrmEx = new myVRMException(errNo);
                            obj.outXml = myvrmEx.FetchErrorMsg();
                            return false;
                        }
                    }
                }
            }
            catch (myVRMException me)
            {
                m_log.Error("SetConference-ParseConferenceXml: ", me);
                obj.outXml = "";
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("SetConference-ParseConferenceXml: ", e);
                obj.outXml = ""; //Error 200 will be thrown
                return false;
            }
            return true;
        }
        #endregion

        #region CheckForConftype
        /// <summary>
        /// CheckForConftype
        /// </summary>
        /// <param name="conftype"></param>
        /// <param name="errNo"></param>
        /// <returns></returns>
        private bool CheckForConftype(int conftype, ref int errNo)
        {
            try
            {
                switch (conftype)
                {
                    case 2:
                        {
                            if (orgInfo.EnableAudioVideoConference == 0)
                            {
                                errNo = 542; //FB 2164
                                return false;
                            }
                            break;
                        }
                    case 4:
                        {
                            if (orgInfo.Connect2 == 0) //PointtoPoint
                            {
                                errNo = 244;
                                return false;
                            }
                            break;
                        }
                    case 6:
                        {
                            if (orgInfo.EnableAudioOnlyConference == 0)
                            {
                                errNo = 543; //FB 2164
                                return false;
                            }
                            break;
                        }
                    case 7:
                        {
                            if (orgInfo.EnableRoomConference == 0)
                            {
                                errNo = 544; //FB 2164
                                return false;
                            }
                            break;
                        }
                }
            }
            catch (Exception e)
            {
                throw new myVRMException("Error in CheckForConftype.", e);
            }
            return true;
        }
        #endregion

        #region LoadConfAttributes
        /// <summary>
        /// LoadConfAttributes
        /// </summary>
        /// <param name="confAttrNodes"></param>
        /// <param name="confAttrs"></param>
        /// <returns></returns>
        private bool LoadConfAttributes(ref XmlNodeList confAttrNodes, ref List<vrmConfAttribute> confAttrs)
        {
            string SelOptValue = "";
            vrmConfAttribute confAtt = null;
            try
            {
                if (confAttrNodes != null)
                {
                    confAttrs = new List<vrmConfAttribute>();
                    int cusAttrId = 0, selOptID = 0;

                    for (int i = 0; i < confAttrNodes.Count; i++)
                    {
                        cusAttrId = 0;
                        selOptID = 0;

                        if (confAttrNodes[i].SelectSingleNode("CustomAttributeID") != null)
                            Int32.TryParse(confAttrNodes[i].SelectSingleNode("CustomAttributeID").InnerText.Trim(), out cusAttrId);

                        if (confAttrNodes[i].SelectSingleNode("OptionID") != null)
                            Int32.TryParse(confAttrNodes[i].SelectSingleNode("OptionID").InnerText.Trim(), out selOptID);

                        if (confAttrNodes[i].SelectSingleNode("OptionValue") != null)
                            SelOptValue = confAttrNodes[i].SelectSingleNode("OptionValue").InnerText.Trim();

                        if (selOptID != 0 && cusAttrId > 0)
                        {
                            confAtt = null;
                            confAtt = new vrmConfAttribute();
                            confAtt.CustomAttributeId = cusAttrId;
                            confAtt.SelectedOptionId = selOptID;
                            confAtt.SelectedValue = SelOptValue;
                            confAttrs.Add(confAtt);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return true;
        }
        #endregion

        #region InsertConfAttributes
        /// <summary>
        /// InsertConfAttributes
        /// </summary>
        /// <param name="confAttrs"></param>
        /// <param name="vConf"></param>
        /// <returns></returns>
        private bool InsertConfAttributes(ref List<vrmConfAttribute> confAttrs, ref vrmConference vConf)
        {
            try
            {
                if (confAttrs != null)
                {
                    vrmConfAttribute confAttr;
                    for (int i = 0; i < confAttrs.Count; i++)
                    {
                        confAttr = confAttrs[i];
                        confAttrs[i].ConfId = vConf.confid;
                        confAttrs[i].InstanceId = vConf.instanceid;
                        m_IconfAttrDAO.Save(confAttr);
                    }
                }
            }
            catch (myVRMException me)
            {
                throw me;
            }
            catch (Exception e)
            {
                throw e;
            }
            return true;
        }
        #endregion

        //FB 2486 Starts

        #region LoadConfMsgs
        /// <summary>
        /// LoadConfMsgs
        /// </summary>
        /// <param name="confMsgNodes"></param>
        /// <param name="confMsgs"></param>
        /// <returns></returns>
        private bool LoadConfMsgs(ref XmlNodeList confMsgNodes, ref List<vrmConfMessage> confMsgs)
        {
            vrmConfMessage confMsg = null;
            string msgduartion = "0", durationValue="0";
            try
            {
                if (confMsgNodes != null)
                {
                    confMsgs = new List<vrmConfMessage>();
                    int  duration = 0, controlID = 0;
                    string message = "";

                    for (int i = 0; i < confMsgNodes.Count; i++)
                    {
                        message = "";
                        duration = 0;
                        controlID = 0;

                        if (confMsgNodes[i].SelectSingleNode("controlID") != null)
                            Int32.TryParse(confMsgNodes[i].SelectSingleNode("controlID").InnerText.Trim(), out controlID);

                        if (confMsgNodes[i].SelectSingleNode("textMessage") != null)
                            message = confMsgNodes[i].SelectSingleNode("textMessage").InnerText.Trim();

                        if (confMsgNodes[i].SelectSingleNode("msgduartion") != null)
                            durationValue = confMsgNodes[i].SelectSingleNode("msgduartion").InnerText.Trim();
                        if (durationValue.Contains('M'))
                        {
                            msgduartion = durationValue.Split('M')[0];
                            Int32.TryParse(msgduartion, out duration);
                            duration = duration * 60;
                        }
                        else if (durationValue.Contains('s'))
                        {
                            msgduartion = durationValue.Split('s')[0];
                            Int32.TryParse(msgduartion, out duration);
                        }

                        if (controlID > 0)
                        {
                            if (duration != 0 && message != "")
                            {
                                confMsg = null;
                                confMsg = new vrmConfMessage();
                                confMsg.confMessage = message.ToString();
                                confMsg.duration = duration;
                                confMsg.controlID = controlID;
                                confMsg.durationID = durationValue;
                                confMsgs.Add(confMsg);
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return true;
        }
        #endregion

        #region InsertConfMsgs
        /// <summary>
        /// InsertConfMsgs
        /// </summary>
        /// <param name="confMsgs"></param>
        /// <param name="vConf"></param>
        /// <returns></returns>
        private bool InsertConfMsgs(ref  List<vrmConfMessage> confMsgs, ref vrmConference vConf)
        {
            try
            {
                if (vConf.conftype == 2 || vConf.conftype == 6)
                {
                    if (confMsgs != null)
                    {
                        vrmConfMessage confMsg;
                        for (int i = 0; i < confMsgs.Count; i++)
                        {
                            confMsg = confMsgs[i];
                            confMsgs[i].orgid = vConf.orgId;
                            confMsgs[i].confid = vConf.confid;
                            confMsgs[i].instanceid = vConf.instanceid;
                            m_IConfMessageDao.Save(confMsg);
                        }
                    }
                }
            }
            catch (myVRMException me)
            {
                throw me;
            }
            catch (Exception e)
            {
                throw e;
            }
            return true;
        }
        #endregion

        //FB  2486 Ends

        #region AttachmentSetup
        /// <summary>
        /// AttachmentSetup (COM to .Net conversion)
        /// </summary>
        /// <param name="vConf"></param>
        /// <param name="icalAttachment"></param>
        private void AttachmentSetup(ref vrmConference vConf, ref String icalAttachment)
        {
            vrmConfAttachments confAttach = new vrmConfAttachments();
            try
            {
                if (icalAttachment != "")
                {
                    confAttach = new vrmConfAttachments();
                    confAttach.confid = vConf.confid;
                    confAttach.instanceid = vConf.instanceid;
                    confAttach.attachment = "[ICAL]" + icalAttachment;
                    m_ConfAttachDao.Save(confAttach);
                }

                for (int i = 0; i < fileList.Count; i++)
                {
                    confAttach = new vrmConfAttachments();
                    confAttach.confid = vConf.confid;
                    confAttach.instanceid = vConf.instanceid;
                    confAttach.attachment = fileList[i];
                    m_ConfAttachDao.Save(confAttach);
                }
            }
            catch (Exception ex)
            {
                m_log.Error("AttachmentSetup:" + ex.Message);
                throw ex;
            }
        }
        #endregion

        #region IsConferenceScheduleable
        /// <summary>
        /// IsConferenceScheduleable
        /// </summary>
        /// <param name="addErrMesg"></param>
        /// <param name="vConf"></param>
        /// <returns></returns>
        private bool IsConferenceScheduleable(ref string addErrMesg, ref vrmConference vConf)
        {
            bool ret = true;
            try
            {
                addErrMesg = "";
                if (vConf == null)
                    return false;

                DateTime confStartDateTime = vConf.confdate;
                DateTime confEndDateTime;
                DateTime SystemStartTime = DateTime.Now;
                DateTime SystemEndTime = DateTime.Now;
                Int32 open24hrs = 1;
                String offDays = "";

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                if (orgInfo == null)
                    return false;

                SystemStartTime = orgInfo.SystemStartTime;
                SystemEndTime = orgInfo.SystemEndTime;
                open24hrs = orgInfo.Open24hrs;
                offDays = orgInfo.Offdays;

                timeZoneData tz = new timeZoneData();
                timeZone.GetTimeZone(sysSettings.TimeZone, ref tz);

                if (vConf.immediate == 1)
                    timeZone.userPreferedTime(sysSettings.TimeZone, ref confStartDateTime);
                else
                {
                    timeZone.changeToGMTTime(vConf.timezone, ref confStartDateTime);
                    timeZone.userPreferedTime(sysSettings.TimeZone, ref confStartDateTime);
                }

                confEndDateTime = confStartDateTime.Add(new TimeSpan(0, 0, vConf.duration, 0));

                int hourDuration = vConf.duration / 60;
                if ((vConf.duration % 60) > 0)
                    hourDuration++;

                //if (((confStartDateTime.TimeOfDay.TotalMinutes < SystemStartTime.TimeOfDay.TotalMinutes) || (confStartDateTime.TimeOfDay.TotalMinutes > SystemEndTime.TimeOfDay.TotalMinutes))
                //    || ((confEndDateTime.TimeOfDay.TotalMinutes < SystemStartTime.TimeOfDay.TotalMinutes) || (confEndDateTime.TimeOfDay.TotalMinutes > SystemEndTime.TimeOfDay.TotalMinutes)))
                //{
                //    // Oops! We don't support conferences at this time.
                //    // Throw an error 
                //}

                // you must compare times irrespective of the dates
                long a_sysStart = SystemStartTime.Hour * 60 + SystemStartTime.Minute;
                long a_sysEnd = SystemEndTime.Hour * 60 + SystemEndTime.Minute;
                long a_confStart = confStartDateTime.Hour * 60 + confStartDateTime.Minute;
                long a_confEnd = confEndDateTime.Hour * 60 + confEndDateTime.Minute;

                if (open24hrs == 0)
                {
                    if (hourDuration >= 24) //Oops! We don't support conferences at this time.
                        ret = false;

                    if (ret)
                    {
                        // Check the daily working hours
                        //	if((confStartDateTime >= SystemStartTime &&  confStartDateTime <= SystemEndTime)  && 
                        //				(confEndDateTime >= SystemStartTime &&  confEndDateTime <= SystemEndTime))
                        //		ret = true;
                        //	else
                        //		return false;

                        //Check for system working hours...
                        if ((a_confStart >= a_sysStart && a_confStart <= a_sysEnd) &&
                                    (a_confEnd >= a_sysStart && a_confEnd <= a_sysEnd))
                            ret = true;
                        else
                            ret = false; //Oops! We don't support conferences at this time.
                    }
                }

                if (ret) //Check for system working days...
                {
                    int start = (int)confStartDateTime.DayOfWeek + 1;
                    int end = (int)confEndDateTime.DayOfWeek + 1;

                    // could wrap around from Saturday to Sunday, Monday etc;
                    if (end < start)
                        end += start + (7 - start);

                    for (int i = start; i <= end; i++)
                    {
                        int k = i;
                        if (k > 7)
                            k -= 7;
                        if (offDays.Contains(k.ToString())) //Oops! We don't support conferences at this time.
                            ret = false;
                    }
                }
                if (!ret)
                {
                    addErrMesg = " Hours of operation: ";
                    if (open24hrs == 1)
                        addErrMesg += "Open 24 hours a day.";
                    else
                        addErrMesg += SystemStartTime.ToShortTimeString() + " To " + SystemEndTime.ToShortTimeString() + " (" + tz.TimeZone + ").";

                    if (offDays == null) offDays = "";

                    if (offDays.Length > 0)
                    {
                        addErrMesg += " Closed: ";

                        string[] offds = offDays.Split(',');

                        for (int i = 0; i < offds.Length; i++)
                        {
                            bool flag = false;
                            switch (offds[i])
                            {
                                case "1": addErrMesg += " Sunday"; flag = true; break;
                                case "2": addErrMesg += " Monday"; flag = true; break;
                                case "3": addErrMesg += " Tuesday"; flag = true; break;
                                case "4": addErrMesg += " Wednesday"; flag = true; break;
                                case "5": addErrMesg += " Thursday"; flag = true; break;
                                case "6": addErrMesg += " Friday"; flag = true; break;
                                case "7": addErrMesg += " Saturday"; flag = true; break;
                                default: break;
                            }
                            if (flag && (i < (offds.Length - 1)))
                                addErrMesg += ",";
                        }
                    }
                }
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                addErrMesg = "";
                return false;
            }
            return ret;
        }
        #endregion

        #region RecurringDates
        /// <summary>
        /// RecurringDates
        /// </summary>
        /// <param name="xd"></param>
        /// <param name="errNo"></param>
        /// <param name="recurDates"></param>
        /// <param name="recurInfo"></param>
        /// <returns></returns>
        private bool RecurringDates(ref XmlDocument xd, ref int errNo, ref List<sRecurrDates> recurDates, ref vrmRecurInfo recurInfo)
        {
            string conftime = "", confdatetime = "", startDate = "";
            XmlNode node = null;
            string timehour = "", timemin = "", timeset = "";
            int duration = 0, tearDur = 0, setupDur = 0, timezoneid = 0;
            int endType = 0;
            try
            {
                if (recurInfo == null)
                {
                    errNo = 200;
                    return false;
                }
                recurInfo.dirty = 0;

                node = xd.SelectSingleNode("//conference/confInfo/appointmentTime/customInstance");
                int custominstance = 0;
                if (node != null)
                    int.TryParse(node.InnerText, out custominstance);
                if (custominstance < 0)
                    custominstance = 0;

                if (custominstance == 0) //regular & custom pattern (not customized instances)
                {
                    node = xd.SelectSingleNode("//conference/confInfo/appointmentTime/timeZone");
                    int.TryParse(node.InnerText, out timezoneid);
                    if (timezoneid <= 0)
                        timezoneid = sysSettings.TimeZone;

                    node = xd.SelectSingleNode("//conference/confInfo/appointmentTime/startHour");
                    if (node != null)
                        timehour = node.InnerText.Trim();

                    node = xd.SelectSingleNode("//conference/confInfo/appointmentTime/startMin");
                    if (node != null)
                        timemin = node.InnerText.Trim();

                    node = xd.SelectSingleNode("//conference/confInfo/appointmentTime/startSet");
                    if (node != null)
                        timeset = node.InnerText.Trim();

                    node = xd.SelectSingleNode("//conference/confInfo/appointmentTime/durationMin");
                    if (node != null)
                        int.TryParse(node.InnerText.Trim(), out duration);
                    //FB 2398 Start
                    if (duration < 15 || duration > 1440)
                    {
                        errNo = 612;
                        return false;
                    }
                    //FB 2398 End
                    node = xd.SelectSingleNode("//conference/confInfo/appointmentTime/setupDuration");
                    if (node != null)
                        int.TryParse(node.InnerText.Trim(), out setupDur);

                    node = xd.SelectSingleNode("//conference/confInfo/appointmentTime/teardownDuration");
                    if (node != null)
                        int.TryParse(node.InnerText.Trim(), out tearDur);

                    // add extra 45 seconds to the "start conftime" to account for the various component time mismatch delays. 
                    conftime = timehour + ":" + timemin + ":45 " + timeset;

                    recurInfo.timezoneid = timezoneid;
                    recurInfo.duration = duration;
                }

                //get the recurringPattern from inputXML
                //get the recurType from inputXML;
                node = xd.SelectSingleNode("//conference/confInfo/recurrencePattern/recurType");
                int recurType = 0;
                int.TryParse(node.InnerText.Trim(), out recurType);
                if (recurType <= 0)
                    throw new myVRMException("Invalid recurring pattern (recurtype).");

                recurInfo.recurType = recurType;

                if (recurType < 5) //Regular Recurrence Pattern
                {
                    #region Regular Pattern

                    RecurrenceFactory recFactory = new RecurrenceFactory();
                    recFactory.maxRecurInstance = vrmConfig.ConfReccurance; //Default instance limit

                    // get the recurrenceRange from inputXML
                    node = xd.SelectSingleNode("//conference/confInfo/recurrencePattern/recurrenceRange/startDate");
                    startDate = node.InnerText.Trim();

                    //end type : 1= no end date ,2=end after <occurrence> times ,3=end by <endDate>
                    node = xd.SelectSingleNode("//conference/confInfo/recurrencePattern/recurrenceRange/endType");
                    int.TryParse(node.InnerText.Trim(), out endType);

                    node = xd.SelectSingleNode("//conference/confInfo/recurrencePattern/recurrenceRange/occurrence");
                    int occurrence = 0;
                    if (node != null)
                        int.TryParse(node.InnerText.Trim(), out occurrence);
                    if (occurrence <= 0) occurrence = 1;

                    //endType == 3
                    string endDate = "";
                    node = xd.SelectSingleNode("//conference/confInfo/recurrencePattern/recurrenceRange/endDate");
                    if (node != null)
                        endDate = node.InnerText.Trim(); // +" " + conftime;

                    confdatetime = startDate + " " + conftime;
                    DateTime startDatetime;
                    DateTime.TryParse(confdatetime, out startDatetime);

                    DateTime endDateTime = DateTime.MinValue;
                    DateTime.TryParse(endDate, out endDateTime); //recurrence series endate

                    recurInfo.startTime = startDatetime;
                    recurInfo.endTime = endDateTime; //recurrence series endate
                    recurInfo.endType = endType;
                    recurInfo.occurrence = occurrence;

                    //Recurrence details
                    //emailMessage += "Effective " + confdate + " until " + endDate;
                    //MaximumRecurIns

                    // AG 2.21.06
                    // in order to give asp the info it needs we must populate the recurinfo struct
                    // here. This is to take into consideration that it might be a custom instance 
                    // brought about by a conflict resolution.
                    //
                    string weekDay;

                    int dailyType, dayGap, weekGap,
                         monthDayNo, monthWeekDayNo, monthWeekDay, monthGap, monthlyType,
                         yearlyType, yearMonth, yearMonthDay, yearMonthWeekDayNo, yearMonthWeekDay;

                    switch (recurType)
                    {
                        case 1:    // recurring daily

                            node = xd.SelectSingleNode("//conference/confInfo/recurrencePattern/dailyType");
                            dailyType = 0;
                            int.TryParse(node.InnerText.Trim(), out dailyType);

                            if (dailyType < 0)
                                throw new myVRMException("Invalid recurring pattern (dailytype).");

                            node = xd.SelectSingleNode("//conference/confInfo/recurrencePattern/dayGap");
                            dayGap = 0;
                            int.TryParse(node.InnerText.Trim(), out dayGap);

                            recurInfo.subType = dailyType;
                            recurInfo.gap = dayGap;
                            break;

                        case 2:   // weekly

                            node = xd.SelectSingleNode("//conference/confInfo/recurrencePattern/weekGap");
                            weekGap = 0;
                            int.TryParse(node.InnerText.Trim(), out weekGap);

                            node = xd.SelectSingleNode("//conference/confInfo/recurrencePattern/weekDay");
                            weekDay = node.InnerText.Trim();

                            recurInfo.days = weekDay;
                            recurInfo.gap = weekGap;
                            break;

                        case 3: // monthly
                            {
                                node = xd.SelectSingleNode("//conference/confInfo/recurrencePattern/monthlyType");
                                monthlyType = 0;
                                int.TryParse(node.InnerText.Trim(), out monthlyType);

                                node = xd.SelectSingleNode("//conference/confInfo/recurrencePattern/monthGap");
                                monthGap = 0;
                                int.TryParse(node.InnerText.Trim(), out monthGap);

                                recurInfo.subType = monthlyType;
                                recurInfo.gap = monthGap;

                                if (monthlyType == 1)
                                {
                                    node = xd.SelectSingleNode("//conference/confInfo/recurrencePattern/monthDayNo");
                                    monthDayNo = 0;
                                    int.TryParse(node.InnerText.Trim(), out monthDayNo);
                                    recurInfo.dayno = monthDayNo;
                                }
                                else if (monthlyType == 2)
                                {
                                    node = xd.SelectSingleNode("//conference/confInfo/recurrencePattern/monthWeekDayNo");
                                    monthWeekDayNo = 0;
                                    int.TryParse(node.InnerText.Trim(), out monthWeekDayNo);

                                    node = xd.SelectSingleNode("//conference/confInfo/recurrencePattern/monthWeekDay");
                                    monthWeekDay = 0;
                                    int.TryParse(node.InnerText.Trim(), out monthWeekDay);

                                    recurInfo.dayno = monthWeekDayNo;
                                    recurInfo.days = monthWeekDay.ToString();
                                }
                                break;
                            }
                        case 4: //yearly
                            node = xd.SelectSingleNode("//conference/confInfo/recurrencePattern/yearlyType");
                            yearlyType = 0;
                            int.TryParse(node.InnerText.Trim(), out yearlyType);

                            node = xd.SelectSingleNode("//conference/confInfo/recurrencePattern/yearMonth");
                            yearMonth = 0;
                            int.TryParse(node.InnerText.Trim(), out yearMonth);

                            recurInfo.subType = yearlyType;
                            recurInfo.yearMonth = yearMonth;

                            if (yearlyType == 1)
                            {
                                node = xd.SelectSingleNode("//conference/confInfo/recurrencePattern/yearMonthDay");
                                yearMonthDay = 0;
                                int.TryParse(node.InnerText.Trim(), out yearMonthDay);
                                recurInfo.days = yearMonthDay.ToString();
                            }
                            else if (yearlyType == 2)
                            {
                                node = xd.SelectSingleNode("//conference/confInfo/recurrencePattern/yearMonthWeekDayNo");
                                yearMonthWeekDayNo = 0;
                                int.TryParse(node.InnerText.Trim(), out yearMonthWeekDayNo);

                                node = xd.SelectSingleNode("//conference/confInfo/recurrencePattern/yearMonthWeekDay");
                                yearMonthWeekDay = 0;
                                int.TryParse(node.InnerText.Trim(), out yearMonthWeekDay);

                                recurInfo.dayno = yearMonthWeekDayNo;
                                recurInfo.days = yearMonthWeekDay.ToString();
                            }
                            break;

                    }
                    //if(!CheckCurrentTime(confDatetime,timezoneid, ref errNo))
                    //    return false;

                    recFactory.recurInfo = recurInfo;
                    recFactory.startHour = timehour;
                    recFactory.startMin = timemin;
                    recFactory.startSet = timeset;
                    recFactory.setDuration = setupDur;
                    recFactory.tearDuration = tearDur;

                    switch (recurType)
                    {
                        case 1:    // recurring daily
                            {
                                recurDates = recFactory.GetDailyRecurDates();
                                break;
                            }
                        case 2:   // weekly
                            {
                                recurDates = recFactory.GetWeeklyRecurDates();
                                break;
                            }
                        case 3: // monthly
                            {
                                recurDates = recFactory.GetMonthlyRecurDates();
                                break;
                            }
                        case 4: //yearly
                            {
                                recurDates = recFactory.GetYearlyRecurDates();
                                break;
                            }
                    }
                    #endregion
                     
                    if (recurInfo.RecurringPattern.Trim() == "")//FB 2231
                        recurInfo.RecurringPattern = recFactory.RecurPatternTxt.Trim();

                }
                else //Custom Patterns
                {
                    #region Custom Pattern
                    // Custom (random) recurring pattern
                    // The recurrence patterns above (1 through 4) are MS Outlook
                    // patterns and use library routines for calculation of 
                    // these recurrence patterns.
                    // Custom recurrence patterns are supported in LotusNotes.
                    // Here, we write our own simple code to deal with this
                    // recurrence type
                    String customDates = "";//FB 2231
                    recurDates = new List<sRecurrDates>();
                    sRecurrDates recIns = new sRecurrDates();
                    XmlNode recurrencePattern;
                    int nstart = 0, err;
                    if (custominstance == 1)
                        recurrencePattern = xd.SelectSingleNode("//conference/confInfo/appointmentTime");
                    else
                        recurrencePattern = xd.SelectSingleNode("//conference/confInfo/recurrencePattern");

                    DateTime lastDate = DateTime.MinValue;
                    DateTime checkDate = DateTime.MinValue;
                    TimeSpan span;
                    int inscount = 0;
                    // this is not a custom instance therefor it is a recur type 5
                    if (custominstance == 1) //Customized instances
                    {
                        #region Customized Instances
                        node = recurrencePattern.SelectSingleNode("//instances/appointmentTime/timeZone");
                        if (node != null)
                            int.TryParse(node.InnerText, out timezoneid);
                        if (timezoneid <= 0)
                            timezoneid = sysSettings.TimeZone;

                        XmlNodeList insNodes = node.SelectNodes("//instances/instance");
                        if (insNodes != null)
                        {
                            for (int lp = 0; lp < insNodes.Count; lp++)
                            {
                                inscount++;

                                node = insNodes[lp].SelectSingleNode("startDate");
                                if (node != null)//FB 2231
                                {
                                    startDate = node.InnerText.Trim();
                                  
                                    customDates += startDate;
                                    if (insNodes.Count > 1 && lp != (insNodes.Count - 1))
                                        customDates += ",";
                                }

                                node = insNodes[lp].SelectSingleNode("startHour");
                                if (node != null)
                                    timehour = node.InnerText.Trim();
                                if (timehour == "") timehour = "00";

                                node = insNodes[lp].SelectSingleNode("startMin");
                                if (node != null)
                                    timemin = node.InnerText.Trim();
                                if (timemin == "") timemin = "00";

                                node = insNodes[lp].SelectSingleNode("startSet");
                                if (node != null)
                                    timeset = node.InnerText.Trim();
                                if (timeset == "") timeset = "AM";

                                node = insNodes[lp].SelectSingleNode("durationMin");
                                if (node != null)
                                    int.TryParse(node.InnerText.Trim(), out duration);
                                if (duration <= 0) duration = 60;

                                node = insNodes[lp].SelectSingleNode("setupDuration");
                                if (node != null)
                                    int.TryParse(node.InnerText.Trim(), out setupDur);

                                if (setupDur <= 0) setupDur = 0;

                                node = insNodes[lp].SelectSingleNode("teardownDuration");
                                if (node != null)
                                    int.TryParse(node.InnerText.Trim(), out tearDur);

                                if (tearDur <= 0) tearDur = 0;

                                // add extra 45 seconds to the "start conftime" to account for the various component time mismatch delays. 
                                conftime = timehour + ":" + timemin + ":45 " + timeset;
                                confdatetime = startDate + " " + conftime;
                                DateTime startDatetime;
                                DateTime.TryParse(confdatetime, out startDatetime);
                                span = new TimeSpan(0, 0, duration, 0);

                                //bool retVal = CheckCurrentTime(confDatetime,timezoneid, ref errNo);
                                if (lastDate > startDatetime)
                                {
                                    errNo = 537;
                                    return false;
                                }
                                lastDate = startDatetime.Add(span);

                                recIns = new sRecurrDates();
                                recIns.RecDatetime = startDatetime;
                                recIns.SetDuration = setupDur;
                                recIns.TearDuration = tearDur;
                                recIns.Duration = duration;
                                recIns.Timezone = timezoneid;
                                recIns.StartHour = timehour;
                                recIns.StartMin = timemin;
                                recIns.StartSet = timeset;

                                //Dont allow to create recurring instances more than the default limit mentioned in vrmConfig
                                if (inscount > vrmConfig.ConfReccurance)
                                    break;
                                recurDates.Add(recIns);
                            }
                        }
                        #endregion
                    }
                    else //custom pattern
                    {
                        #region Custom Pattern
                        XmlNodeList insNodes = node.SelectNodes("//recurrencePattern/startDates/startDate");
                        if (insNodes != null)
                        {
                            inscount = 0;
                            for (int lp = 0; lp < insNodes.Count; lp++)
                            {
                                inscount++;

                                if (insNodes[lp] != null)//FB 2231
                                {
                                    startDate = insNodes[lp].InnerText.Trim();
                                    
                                    customDates += startDate;
                                    if (insNodes.Count > 1 && lp != (insNodes.Count - 1))
                                        customDates += ",";
                                }

                                conftime = timehour + ":" + timemin + ":45 " + timeset;
                                confdatetime = startDate + " " + conftime;
                                DateTime startDatetime;
                                DateTime.TryParse(confdatetime, out startDatetime);
                                span = new TimeSpan(0, 0, duration, 0);

                                //bool retVal = CheckCurrentTime(confDatetime,timezoneid, ref errNo);
                                if (lastDate > startDatetime)
                                {
                                    errNo = 537;
                                    return false;
                                }
                                lastDate = startDatetime.Add(span);

                                recIns = new sRecurrDates();
                                recIns.RecDatetime = startDatetime;
                                recIns.SetDuration = setupDur;
                                recIns.TearDuration = tearDur;
                                recIns.Duration = duration;
                                recIns.Timezone = timezoneid;
                                recIns.StartHour = timehour;
                                recIns.StartMin = timemin;
                                recIns.StartSet = timeset;

                                //Dont allow to create recurring instances more than the default limit mentioned in vrmConfig
                                if (inscount > vrmConfig.ConfReccurance)
                                    break;
                                recurDates.Add(recIns);
                            }
                        }
                        #endregion
                    }

                    if (recurInfo.RecurringPattern.Trim() == "") //FB 2231
                        recurInfo.RecurringPattern = " Custom Date Selection: " + customDates;

                    #endregion
                }
            }
            catch (myVRMException me)
            {
                m_log.Error("MyvrmException-RecurringDates: ", me);
                errNo = 200;
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("SystemException-RecurringDates: ", e);
                errNo = 200;
                return false;
            }
            return true;
        }
        #endregion

        #region GetdefunctPatternInfo
        /// <summary>
        /// RecurringDates FB 2218
        /// </summary>
        /// <param name="xd"></param>
        /// <param name="errNo"></param>
        /// <param name="recurDates"></param>
        /// <param name="recurInfo"></param>
        /// <returns></returns>
        private bool GetdefunctPatternInfo(ref XmlDocument xd, ref int errNo, ref List<sRecurrDates> recurDates, ref vrmRecurInfoDefunct recurInfo)
        {
            string conftime = "", confdatetime = "", startDate = "";
            XmlNode node = null;
            string timehour = "", timemin = "", timeset = "";
            int duration = 0, tearDur = 0, setupDur = 0, timezoneid = 0;
            int endType = 0;
            try
            {
                if (recurInfo == null)
                {
                    errNo = 200;
                    return false;
                }
                recurInfo.dirty = 0;

                node = xd.SelectSingleNode("//conference/defunctPattern/appointmentTime/customInstance");
                int custominstance = 0;
                if (node != null)
                    int.TryParse(node.InnerText, out custominstance);
                if (custominstance < 0)
                    custominstance = 0;

                if (custominstance == 0) //regular & custom pattern (not customized instances)
                {
                    node = xd.SelectSingleNode("//conference/defunctPattern/appointmentTime/timeZone");
                    int.TryParse(node.InnerText, out timezoneid);
                    if (timezoneid <= 0)
                        timezoneid = sysSettings.TimeZone;

                    node = xd.SelectSingleNode("//conference/defunctPattern/appointmentTime/startHour");
                    if (node != null)
                        timehour = node.InnerText.Trim();

                    node = xd.SelectSingleNode("//conference/defunctPattern/appointmentTime/startMin");
                    if (node != null)
                        timemin = node.InnerText.Trim();

                    node = xd.SelectSingleNode("//conference/defunctPattern/appointmentTime/startSet");
                    if (node != null)
                        timeset = node.InnerText.Trim();

                    node = xd.SelectSingleNode("//conference/defunctPattern/appointmentTime/durationMin");
                    if (node != null)
                        int.TryParse(node.InnerText.Trim(), out duration);

                    node = xd.SelectSingleNode("//conference/defunctPattern/appointmentTime/setupDuration");
                    if (node != null)
                        int.TryParse(node.InnerText.Trim(), out setupDur);

                    node = xd.SelectSingleNode("//conference/defunctPattern/appointmentTime/teardownDuration");
                    if (node != null)
                        int.TryParse(node.InnerText.Trim(), out tearDur);

                    // add extra 45 seconds to the "start conftime" to account for the various component time mismatch delays. 
                    conftime = timehour + ":" + timemin + ":45 " + timeset;

                    recurInfo.timezoneid = timezoneid;
                    recurInfo.duration = duration;
                }

                //get the recurringPattern from inputXML
                //get the recurType from inputXML;
                node = xd.SelectSingleNode("//conference/defunctPattern/recurrencePattern/recurType");
                int recurType = 0;
                int.TryParse(node.InnerText.Trim(), out recurType);
                if (recurType <= 0)
                    throw new myVRMException("Invalid recurring pattern (recurtype).");

                recurInfo.recurType = recurType;

                if (recurType < 5) //Regular Recurrence Pattern
                {
                    #region Regular Pattern

                    RecurrenceFactory recFactory = new RecurrenceFactory();
                    recFactory.maxRecurInstance = vrmConfig.ConfReccurance; //Default instance limit

                    // get the recurrenceRange from inputXML
                    node = xd.SelectSingleNode("//conference/defunctPattern/recurrencePattern/recurrenceRange/startDate");
                    startDate = node.InnerText.Trim();

                    //end type : 1= no end date ,2=end after <occurrence> times ,3=end by <endDate>
                    node = xd.SelectSingleNode("//conference/defunctPattern/recurrencePattern/recurrenceRange/endType");
                    int.TryParse(node.InnerText.Trim(), out endType);

                    node = xd.SelectSingleNode("//conference/defunctPattern/recurrencePattern/recurrenceRange/occurrence");
                    int occurrence = 0;
                    if (node != null)
                        int.TryParse(node.InnerText.Trim(), out occurrence);
                    if (occurrence <= 0) occurrence = 1;

                    //endType == 3
                    string endDate = "";
                    node = xd.SelectSingleNode("//conference/defunctPattern/recurrencePattern/recurrenceRange/endDate");
                    if (node != null)
                        endDate = node.InnerText.Trim(); // +" " + conftime;

                    confdatetime = startDate + " " + conftime;
                    DateTime startDatetime;
                    DateTime.TryParse(confdatetime, out startDatetime);

                    DateTime endDateTime = DateTime.MinValue;
                    DateTime.TryParse(endDate, out endDateTime); //recurrence series endate

                    recurInfo.startTime = startDatetime;
                    recurInfo.endTime = endDateTime; //recurrence series endate
                    recurInfo.endType = endType;
                    recurInfo.occurrence = occurrence;

                    //Recurrence details
                    //emailMessage += "Effective " + confdate + " until " + endDate;
                    //MaximumRecurIns

                    // AG 2.21.06
                    // in order to give asp the info it needs we must populate the recurinfo struct
                    // here. This is to take into consideration that it might be a custom instance 
                    // brought about by a conflict resolution.
                    //
                    string weekDay;

                    int dailyType, dayGap, weekGap,
                         monthDayNo, monthWeekDayNo, monthWeekDay, monthGap, monthlyType,
                         yearlyType, yearMonth, yearMonthDay, yearMonthWeekDayNo, yearMonthWeekDay;

                    switch (recurType)
                    {
                        case 1:    // recurring daily

                            node = xd.SelectSingleNode("//conference/defunctPattern/recurrencePattern/dailyType");
                            dailyType = 0;
                            int.TryParse(node.InnerText.Trim(), out dailyType);

                            if (dailyType < 0)
                                throw new myVRMException("Invalid recurring pattern (dailytype).");

                            node = xd.SelectSingleNode("//conference/defunctPattern/recurrencePattern/dayGap");
                            dayGap = 0;
                            int.TryParse(node.InnerText.Trim(), out dayGap);

                            recurInfo.subType = dailyType;
                            recurInfo.gap = dayGap;
                            break;

                        case 2:   // weekly

                            node = xd.SelectSingleNode("//conference/defunctPattern/recurrencePattern/weekGap");
                            weekGap = 0;
                            int.TryParse(node.InnerText.Trim(), out weekGap);

                            node = xd.SelectSingleNode("//conference/defunctPattern/recurrencePattern/weekDay");
                            weekDay = node.InnerText.Trim();

                            recurInfo.days = weekDay;
                            recurInfo.gap = weekGap;
                            break;

                        case 3: // monthly
                            {
                                node = xd.SelectSingleNode("//conference/defunctPattern/recurrencePattern/monthlyType");
                                monthlyType = 0;
                                int.TryParse(node.InnerText.Trim(), out monthlyType);

                                node = xd.SelectSingleNode("//conference/defunctPattern/recurrencePattern/monthGap");
                                monthGap = 0;
                                int.TryParse(node.InnerText.Trim(), out monthGap);

                                recurInfo.subType = monthlyType;
                                recurInfo.gap = monthGap;

                                if (monthlyType == 1)
                                {
                                    node = xd.SelectSingleNode("//conference/defunctPattern/recurrencePattern/monthDayNo");
                                    monthDayNo = 0;
                                    int.TryParse(node.InnerText.Trim(), out monthDayNo);
                                    recurInfo.dayno = monthDayNo;
                                }
                                else if (monthlyType == 2)
                                {
                                    node = xd.SelectSingleNode("//conference/defunctPattern/recurrencePattern/monthWeekDayNo");
                                    monthWeekDayNo = 0;
                                    int.TryParse(node.InnerText.Trim(), out monthWeekDayNo);

                                    node = xd.SelectSingleNode("//conference/defunctPattern/recurrencePattern/monthWeekDay");
                                    monthWeekDay = 0;
                                    int.TryParse(node.InnerText.Trim(), out monthWeekDay);

                                    recurInfo.dayno = monthWeekDayNo;
                                    recurInfo.days = monthWeekDay.ToString();
                                }
                                break;
                            }
                        case 4: //yearly
                            node = xd.SelectSingleNode("//conference/defunctPattern/recurrencePattern/yearlyType");
                            yearlyType = 0;
                            int.TryParse(node.InnerText.Trim(), out yearlyType);

                            node = xd.SelectSingleNode("//conference/defunctPattern/recurrencePattern/yearMonth");
                            yearMonth = 0;
                            int.TryParse(node.InnerText.Trim(), out yearMonth);

                            recurInfo.subType = yearlyType;
                            recurInfo.yearMonth = yearMonth;

                            if (yearlyType == 1)
                            {
                                node = xd.SelectSingleNode("//conference/defunctPattern/recurrencePattern/yearMonthDay");
                                yearMonthDay = 0;
                                int.TryParse(node.InnerText.Trim(), out yearMonthDay);
                                recurInfo.days = yearMonthDay.ToString();
                            }
                            else if (yearlyType == 2)
                            {
                                node = xd.SelectSingleNode("//conference/defunctPattern/recurrencePattern/yearMonthWeekDayNo");
                                yearMonthWeekDayNo = 0;
                                int.TryParse(node.InnerText.Trim(), out yearMonthWeekDayNo);

                                node = xd.SelectSingleNode("//conference/defunctPattern/recurrencePattern/yearMonthWeekDay");
                                yearMonthWeekDay = 0;
                                int.TryParse(node.InnerText.Trim(), out yearMonthWeekDay);

                                recurInfo.dayno = yearMonthWeekDayNo;
                                recurInfo.days = yearMonthWeekDay.ToString();
                            }
                            break;

                    }


                    recurInfo.SetupDuration = setupDur;
                    recurInfo.TeardownDuration = tearDur;

                    //if(!CheckCurrentTime(confDatetime,timezoneid, ref errNo))
                    //    return false;


                    #endregion


                }
                else
                {
                    defunctRecurInfo = null;
                }
                
            }
            catch (myVRMException me)
            {
                m_log.Error("MyvrmException-RecurringDates: ", me);
                errNo = 200;
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("SystemException-RecurringDates: ", e);
                errNo = 200;
                return false;
            }
            return true;
        }
        #endregion

        #region CheckCurrentTime
        /// <summary>
        /// CheckCurrentTime
        /// check if the conference's endtime is prior to the currenttime and the 
        /// the conference's starting time is prior to currenttime for 5 minutes.
        /// if it is 5 or more minu
        /// </summary>
        /// <param name="cfdatetime"></param>
        /// <param name="timezoneid"></param>
        /// <param name="errNo"></param>
        /// <returns></returns>
        private bool CheckCurrentTime(DateTime cfdatetime, int timezoneid, ref int errNo)
        {
            try
            {
                DateTime nowdate = DateTime.Now;
                timeZone.changeToGMTTime(sysSettings.TimeZone, ref nowdate); //change system date to GMT
                timeZone.changeToGMTTime(timezoneid, ref cfdatetime); //change conf datetime to GMT

                // comparing the setting time with the current time.
                // if the confdatetime is prior the currenttime 5 minute or less, it is ok.
                TimeSpan span = cfdatetime.Subtract(nowdate);
                double minDiff = span.TotalMinutes;

                //FB 2398 Start
                if (minDiff < 5 && orgInfo.SetupTime > 0)
                {
                    errNo = 611;
                    return false;
                }
                //FB 2398 End
                if (minDiff < 5)
                {
                    errNo = 223;
                    return false;
                }
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
            return true;
        }
        #endregion

        #region LoadConferenceEndpoints
        /// <summary>
        /// Load conf room, parties and splitroom details.
        /// </summary>
        /// <param name="rooms"></param>
        /// <param name="roomsSplit"></param>
        /// <param name="users"></param>
        /// <param name="confRooms"></param>
        /// <param name="confSplitRooms"></param>
        /// <param name="confUsers"></param>
        /// <param name="addedGuests"></param>
        /// <param name="totalinvited"></param>
        /// <param name="totalEndpoints"></param>
        /// <param name="confType"></param>
        /// <param name="errNo"></param>
        /// <returns></returns>
        private bool LoadConferenceEndpoints(ref XmlNodeList rooms, ref XmlNodeList roomsSplit, ref XmlNodeList users, ref List<vrmConfRoom> confRooms, ref List<SplitRoom> confSplitRooms, ref List<vrmConfUser> confUsers, ref List<vrmGuestUser> addedGuests, ref Int32 totalinvited, ref Int32 totalEndpoints, ref Int32 confType, ref Int32 errNo, ref XmlNodeList guestRoomNodes, ref List<vrmRoom> guestRooms, ref List<vrmEndPoint> guestEndpoints) //FB 2426
        {
            vrmConfRoom room = null;
            SplitRoom sRoom = null;
            vrmConfUser user = null;
            vrmGuestUser guestUser = null;
            int roomid, splitroomid, userid, invite, notify, audorvid, notifyEdit = 0, survey,publicParty=0;//FB 2348 //FB 2550
            DateTime spliroomStart = DateTime.Now;
            double splitroomdur = 0;
            string fname, lname, email = "";
            List<ICriterion> criterionList = null;
            List<vrmUser> userList = null;
            List<vrmGuestUser> guestList = null;
            //FB 2426 Start
            XmlNodeList ProfileNodes = null;
            XmlNodeList RoomNodes = null;    
            vrmEndPoint guestEpt = null;
            vrmUser UserDetails = null;
            int pId = 0;
            vrmRoom guestRm = null;
            string GuestRoomName, ContactName, ContactEmail, ContactPhoneNo, RoomAddress = "";
            string City, State, ZipCode, loginuserId, profilename, Endpointname, RoomID = "", orgid = "";
            string multisiloOrgID = "";
            int Country, Tier1, Tier2, organizationID = 0;
            int mutliOrgID = 0;
            List<vrmTier2> t2RoomList = null;
            List<vrmEndPoint> guestEptcount = new List<vrmEndPoint>();
            //FB 2426 End
            try
            {
                #region Conf Rooms
                if (rooms != null)
                {
                    if (rooms.Count > 0)
                    {
                        for (int ccnt = 0; ccnt < rooms.Count; ccnt++)
                        {
                            roomid = 0;
                            if (rooms[ccnt].InnerText != "")
                            {
                                if (confRooms == null)
                                    confRooms = new List<vrmConfRoom>();

                                room = new vrmConfRoom();
                                Int32.TryParse(rooms[ccnt].InnerText, out roomid);

                                room.roomId = roomid;
                                if (room.roomId > 0)
                                {
                                    room.Room = m_vrmRoomDAO.GetByRoomId(room.roomId);
                                    confRooms.Add(room);

                                    if (confType != vrmConfType.RooomOnly && room.Room.IsVMR == 0) //FB 2448
                                    {
                                        if (room.Room.endpointid <= 0)
                                        {
                                            errNo = 262;
                                            return false;
                                        }
                                    }
                                    totalEndpoints++;
                                }
                            }
                        }
                    }
                }

                #endregion

                //FB 2426 start
                #region Guest Rooms & Endpoints
                RoomNodes = guestRoomNodes;
                if (RoomNodes != null)
                {
                    if (RoomNodes.Count > 0)
                    {
                        if (confType == 2 || confType == 6)
                        {
                            for (int j = 0; j < RoomNodes.Count; j++)
                            {
                                #region Endpoint

                                guestEpt = null;
                                guestRm = null;
                                if (RoomNodes[j].SelectSingleNode("GuestRoomID") != null)
                                {
                                    RoomID = RoomNodes[j].SelectSingleNode("GuestRoomID").InnerText;
                                }
                                ProfileNodes = RoomNodes[j].SelectNodes("Profiles/Profile");
                                guestEpt = new vrmEndPoint();
                                /*
                                 * first check for availability in guest endpoints by id (this is for handling edit mode)
                                 * if id does not exist have "-1" as endpoint id or original ept id
                                 * dont check for license just fill all properties here and increment totalept count for connect2
                                 */
                                if (ProfileNodes != null)
                                {
                                    if (ProfileNodes.Count > 0)
                                    {
                                        for (int i = 0; i < ProfileNodes.Count; i++)
                                        {

                                            if (ProfileNodes[i].SelectSingleNode("ProfileName").InnerText != "")
                                            {
                                                guestEpt = new vrmEndPoint();
                                                if (guestEndpoints == null)
                                                    guestEndpoints = new List<vrmEndPoint>();
                                                pId++;
                                                profilename = ProfileNodes[i].SelectSingleNode("ProfileName").InnerText;
                                                Endpointname = ProfileNodes[i].SelectSingleNode("EndpointName").InnerText;
                                                guestEpt.name = Endpointname;
                                                guestEpt.profileId = pId;
                                                guestEpt.profileName = ProfileNodes[i].SelectSingleNode("ProfileName").InnerText;
                                                guestEpt.deleted = 0;
                                                guestEpt.isDefault = Int32.Parse(ProfileNodes[i].SelectSingleNode("isDefault").InnerText);
                                                guestEpt.encrypted = 0;
                                                guestEpt.addresstype = Int32.Parse(ProfileNodes[i].SelectSingleNode("AddressType").InnerText);
                                                guestEpt.password = ProfileNodes[i].SelectSingleNode("Password").InnerText;
                                                guestEpt.address = ProfileNodes[i].SelectSingleNode("Address").InnerText;
                                                guestEpt.endptURL = "";
                                                guestEpt.outsidenetwork = 0;
                                                guestEpt.connectiontype = Int32.Parse(ProfileNodes[i].SelectSingleNode("ConnectionType").InnerText);
                                                guestEpt.videoequipmentid = 1;
                                                guestEpt.linerateid = Int32.Parse(ProfileNodes[i].SelectSingleNode("MaxLineRate").InnerText);
                                                guestEpt.bridgeid = -1;
                                                guestEpt.MCUAddress = "";
                                                guestEpt.MCUAddressType = -1;
                                                guestEpt.protocol = 1;
                                                guestEpt.TelnetAPI = 0;
                                                guestEpt.ExchangeID = "";
                                                guestEpt.CalendarInvite = 0;
                                                guestEpt.ConferenceCode = "";
                                                guestEpt.LeaderPin = "";
                                                organizationID = defaultOrgId;
                                                orgid = RoomNodes[j].SelectSingleNode("organizationID").InnerText;
                                                multisiloOrgID = RoomNodes[j].SelectSingleNode("multisiloOrganizationID").InnerText;
                                                if (multisiloOrgID != "")
                                                    Int32.TryParse(multisiloOrgID, out mutliOrgID);
                                                if (mutliOrgID > 11)
                                                    orgid = multisiloOrgID;

                                                Int32.TryParse(orgid, out organizationID);

                                                guestEpt.orgId = organizationID;
                                                guestEpt.ApiPortNo = 23;
                                                guestEpt.Extendpoint = 1;
                                                totalEndpoints++;
                                                if (RoomID == "new")
                                                {
                                                    if (guestEpt.endpointid <= 0)
                                                    {
                                                        criterionList = new List<ICriterion>();
                                                        criterionList.Add(Expression.Eq("name", Endpointname));
                                                        criterionList.Add(Expression.Eq("orgId", organizationID));
                                                        criterionList.Add(Expression.Eq("Extendpoint", 0));
                                                        guestEptcount = m_vrmEpt.GetByCriteria(criterionList);
                                                        if (guestEptcount.Count > 0)
                                                        {
                                                            errNo = 434;
                                                            return false;
                                                            //guestEpt.endpointid = guestEptcount[0].endpointid;
                                                        }
                                                        else
                                                        {
                                                            guestEpt.endpointid = -1;
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    guestRm = m_vrmRoomDAO.GetByRoomId(Int32.Parse(RoomID));
                                                    guestEpt.endpointid = guestRm.endpointid;
                                                    vrmEndPoint gustEp = m_vrmEpt.GetByEptId(guestEpt.endpointid);
                                                    if (gustEp != null)
                                                        guestEpt.uId = gustEp.uId;
                                                    else
                                                    {
                                                        errNo = 434; //error
                                                        return false;
                                                    }

                                                }
                                                guestEndpoints.Add(guestEpt); //add all fly endpoints to this list
                                            }
                                        }
                                    }
                                    pId = 0;
                                }

                                #endregion

                                /*
                                     * first check for availability in guest room by id (this is for handling edit mode)
                                     * if id does not exist have "-1" as room id or original fly room id
                                     * dont check for license just fill all properties here
                                    */

                                guestRm = new vrmRoom();
                                if (guestRooms == null)
                                    guestRooms = new List<vrmRoom>();

                                loginuserId = RoomNodes[j].SelectSingleNode("LoginUserID").InnerText;
                                RoomID = RoomNodes[j].SelectSingleNode("GuestRoomID").InnerText;
                                orgid = RoomNodes[j].SelectSingleNode("organizationID").InnerText;

                                multisiloOrgID = RoomNodes[j].SelectSingleNode("multisiloOrganizationID").InnerText;

                                if (multisiloOrgID != "")
                                    Int32.TryParse(multisiloOrgID, out mutliOrgID);
                                if (mutliOrgID > 11)
                                    orgid = multisiloOrgID;

                                Int32.TryParse(orgid, out organizationID);

                                GuestRoomName = RoomNodes[j].SelectSingleNode("GuestRoomName").InnerText;
                                ContactName = RoomNodes[j].SelectSingleNode("ContactName").InnerText;
                                ContactEmail = RoomNodes[j].SelectSingleNode("ContactEmail").InnerText;
                                ContactPhoneNo = RoomNodes[j].SelectSingleNode("ContactPhoneNo").InnerText;
                                RoomAddress = RoomNodes[j].SelectSingleNode("RoomAddress").InnerText;
                                City = RoomNodes[j].SelectSingleNode("City").InnerText;
                                State = RoomNodes[j].SelectSingleNode("State").InnerText;
                                ZipCode = RoomNodes[j].SelectSingleNode("ZipCode").InnerText;
                                Country = Int32.Parse(RoomNodes[j].SelectSingleNode("Country").InnerText);
                                Tier1 = Int32.Parse(RoomNodes[j].SelectSingleNode("Tier1").InnerText);
                                Tier2 = Int32.Parse(RoomNodes[j].SelectSingleNode("Tier2").InnerText);
                                UserDetails = m_vrmUserDAO.GetByUserEmail(ContactEmail.Trim());
                                guestRm.adminId = Int32.Parse(loginuserId);
                                guestRm.ResponseTime = 60;
                                guestRm.Name = GuestRoomName;
                                //guestRm.roomId = userroomID;
                                guestRm.orgId = organizationID;
                                guestRm.RoomPhone = ContactPhoneNo;
                                guestRm.HandiCappedAccess = 0;
                                guestRm.isVIP = 0;
                                guestRm.Capacity = 0;
                                guestRm.MaxPhoneCall = 0;
                                guestRm.SetupTime = 0;
                                guestRm.TeardownTime = 0;

                                //FB 2539 Start
                                if (UserDetails.Admin == vrmUserConstant.SUPER_ADMIN)
                                    guestRm.assistant = UserDetails.userid;
                                else
                                {
                                    errNo = 631;
                                    return false;
                                }
                                //FB 2539 End
                               
                                guestRm.notifyemails = "";
                                guestRm.Disabled = 0;
                                if (Tier1 == 0)
                                {
                                    guestRm.L3LocationId = 1;
                                }
                                else
                                {
                                    guestRm.L3LocationId = Tier1;
                                }

                                criterionList = new List<ICriterion>();
                                if (Tier2 == 0)
                                {
                                    criterionList.Add(Expression.Eq("ID", 1));
                                }
                                else
                                {
                                    criterionList.Add(Expression.Eq("ID", Tier2));
                                }
                                criterionList.Add(Expression.Eq("disabled", 0));
                                t2RoomList = m_IT2DAO.GetByCriteria(criterionList);
                                if (t2RoomList.Count > 0)
                                    guestRm.tier2 = (vrmTier2)t2RoomList[0];

                                guestRm.RoomFloor = "";
                                guestRm.RoomNumber = "";
                                guestRm.Address1 = RoomAddress;
                                guestRm.Address2 = "";
                                guestRm.City = City;
                                if (Country == 38)
                                    State = "55";
                                else if (Country == 137)
                                    State = "68";
                                else
                                    State = "34";
                                guestRm.State = Int32.Parse(State);
                                guestRm.Zipcode = ZipCode;
                                guestRm.Country = Country;
                                guestRm.Maplink = "";
                                guestRm.ParkingDirections = "";
                                guestRm.AdditionalComments = "";
                                guestRm.TimezoneID = UserDetails.TimeZone;
                                guestRm.CostCenterid = 0;
                                guestRm.Maplink = guestRm.ParkingDirections = guestRm.Longitude = "";
                                guestRm.AdditionalComments = guestRm.Latitude = "";
                                guestRm.MapImage1Id = guestRm.MapImage2Id = guestRm.SecurityImage1Id = 0;
                                guestRm.SecurityImage2Id = guestRm.MiscImage1Id = guestRm.MapImage2Id = 0;
                                guestRm.Caterer = "1";
                                guestRm.isTelepresence = 0;
                                guestRm.ServiceType = -1;
                                guestRm.DedicatedCodec = guestRm.DedicatedVideo = "0";
                                guestRm.RoomQueue = "";
                                guestRm.AVOnsiteSupportEmail = "";
                                guestRm.DynamicRoomLayout = "1";
                                guestRm.ProjectorAvailable = 1;
                                guestRm.VideoAvailable = 2;
                                guestRm.DefaultEquipmentid = -1;
                                guestRm.Custom1 = "";
                                guestRm.Custom2 = "";
                                guestRm.Custom3 = "";
                                guestRm.Custom4 = "";
                                guestRm.Custom5 = "";
                                guestRm.Custom6 = "";
                                guestRm.Custom7 = "";
                                guestRm.Custom8 = "";
                                guestRm.Custom9 = "";
                                guestRm.Custom10 = "";
                                DateTime modDate = DateTime.Now;
                                timeZone.changeToGMTTime(sysSettings.TimeZone, ref modDate);
                                guestRm.Lastmodifieddate = modDate;
                                guestRm.Extroom = 1;
                                if (RoomID == "new")
                                {
                                    if (guestRm.roomId <= 0)
                                    {
                                        criterionList = new List<ICriterion>();
                                        criterionList.Add(Expression.Eq("Name", GuestRoomName));
                                        criterionList.Add(Expression.Eq("orgId", organizationID));
                                        criterionList.Add(Expression.Eq("Extroom", 0));
                                        List<vrmRoom> roomChkList = m_vrmRoomDAO.GetByCriteria(criterionList);
                                        if (roomChkList.Count > 0)
                                        {
                                            errNo = 256;
                                            return false;
                                            //guestRm.roomId = roomChkList[0].roomId;
                                            //guestRm.endpointid = roomChkList[0].endpointid;
                                        }
                                        else
                                        {
                                            guestRm.roomId = 0;
                                        }
                                    }
                                }
                                else
                                {
                                    //guestRm = m_vrmRoomDAO.GetByRoomId(Int32.Parse(RoomID));
                                    //guestRm.endpointid = guestRm.endpointid;
                                    guestRm.roomId = Int32.Parse(RoomID);
                                }
                                guestRooms.Add(guestRm);
                            }
                        }
                        else
                        {
                            errNo = 633;
                            return false;
                        }
                    }
                }

                #endregion
                //FB 2426 end

                #region Conference Room Split

                if (roomsSplit != null)
                {
                    if (roomsSplit.Count > 0)
                    {
                        for (int ccnt = 0; ccnt < roomsSplit.Count; ccnt++)
                        {
                            splitroomid = 0; splitroomdur = 0; spliroomStart = DateTime.Now;
                            if (roomsSplit[ccnt].SelectSingleNode("level1ID").InnerText != "")
                            {
                                if (confSplitRooms == null)
                                    confSplitRooms = new List<SplitRoom>();

                                sRoom = new SplitRoom();
                                Int32.TryParse(roomsSplit[ccnt].SelectSingleNode("level1ID").InnerText, out splitroomid);

                                sRoom.RoomID = splitroomid;

                                if (sRoom.RoomID > 0)
                                {
                                    Double.TryParse(roomsSplit[ccnt].SelectSingleNode("duration").InnerText, out splitroomdur);
                                    sRoom.Duration = splitroomdur;
                                    DateTime.TryParse(roomsSplit[ccnt].SelectSingleNode("startTime").InnerText, out spliroomStart);
                                    sRoom.StartTime = spliroomStart;

                                    confSplitRooms.Add(sRoom);

                                    totalEndpoints++;
                                }
                            }
                        }
                    }
                }
                #endregion

                #region Conf Users
                if (users != null)
                {
                    if (users.Count > 0)
                    {
                        for (int ccnt = 0; ccnt < users.Count; ccnt++)
                        {
                            userid = 0; invite = 0; notify = 0; notifyEdit = 1; audorvid = 0; survey = 0; publicParty = 0;//FB 2348 //FB 2550

                            lname = ""; fname = ""; email = "";

                            if (users[ccnt].SelectSingleNode("partyID").InnerText != "")
                            {
                                if (confUsers == null)
                                    confUsers = new List<vrmConfUser>();

                                email = users[ccnt].SelectSingleNode("partyEmail").InnerText;
                                if (email.Trim().IndexOf("@") <= 0)// because emails from exchange some times be invalid
                                    continue;
                                Int32.TryParse(users[ccnt].SelectSingleNode("partyID").InnerText, out userid);
                                Int32.TryParse(users[ccnt].SelectSingleNode("partyInvite").InnerText, out invite);
                                Int32.TryParse(users[ccnt].SelectSingleNode("partyNotify").InnerText, out notify);
                                Int32.TryParse(users[ccnt].SelectSingleNode("partyAudVid").InnerText, out audorvid);
                                if(users[ccnt].SelectSingleNode("notifyOnEdit") != null)//NULL CHeck
                                    Int32.TryParse(users[ccnt].SelectSingleNode("notifyOnEdit").InnerText, out notifyEdit);
                                lname = users[ccnt].SelectSingleNode("partyLastName").InnerText;
                                fname = users[ccnt].SelectSingleNode("partyFirstName").InnerText;
                                if (users[ccnt].SelectSingleNode("survey") != null)//FB 2348
                                    Int32.TryParse(users[ccnt].SelectSingleNode("survey").InnerText, out survey);//FB 2348
                                
                                if (users[ccnt].SelectSingleNode("partyPublicVMR") != null)//FB 2550
                                    Int32.TryParse(users[ccnt].SelectSingleNode("partyPublicVMR").InnerText, out publicParty);

                                user = new vrmConfUser();

                                user.userid = userid;
                                user.invitee = invite;
                                user.NotifyOnEdit = notifyEdit;
                                user.partyNotify = notify;
                                user.audioOrVideo = audorvid;
                                user.Survey = survey;//FB 2348
                                //Needs to filled in properly after merging set advance AV settings
                                user.bridgeIPISDNAddress = "0";
                                user.bridgeid = 0;
                                user.bridgeAddressType = 0;
                                user.layout = 0;
                                user.defLineRate = 0;
                                user.TerminalType = vrmConfTerminalType.NormalUser;//FB 2501 Call Monitoring
                                user.PublicVMRParty = publicParty; //FB 2550

                                if (invite == 1)
                                    totalEndpoints++;

                                //if (user.userid <= 0) commented for FB 2501 Call Monitoring
                                {
                                    criterionList = new List<ICriterion>();
                                    criterionList.Add(Expression.Eq("Email", email));
                                    userList = m_vrmUserDAO.GetByCriteria(criterionList);

                                    if (userList.Count > 0)
                                        user.userid = userList[0].userid;
                                    else
                                    {
                                        guestList = m_vrmGuestDAO.GetByCriteria(criterionList);

                                        if (guestList.Count > 0)
                                        {
                                            user.userid = guestList[0].userid;
                                            user.TerminalType = vrmConfTerminalType.GuestUser;//FB 2501 Call Monitoring
                                        }
                                        else
                                        {
                                            //Guest cannot be saved here so it will be added to list saved after the conference creation is successfull. 
                                            //Then after saving the guest the conf user table will be updated with the user id for that email.
                                            guestUser = new vrmGuestUser();
                                            guestUser.Email = email;
                                            guestUser.FirstName = fname;
                                            guestUser.LastName = lname;

                                            if (addedGuests == null)
                                                addedGuests = new List<vrmGuestUser>();

                                            addedGuests.Add(guestUser);
                                            user.TerminalType = vrmConfTerminalType.GuestUser;//FB 2501 Call Monitoring
                                            user.userid = -1;
                                            user.endptURL = email;
                                        }
                                    }
                                }
                                totalinvited++;
                                confUsers.Add(user);
                            }
                        }
                    }
                }
                #endregion
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
            return true;
        }
        #endregion

        #region CheckForConflicts - Need to Delete after QA Clearance
        /// <summary>
        /// CheckForConflicts
        /// </summary>
        /// <param name="confRooms"></param>
        /// <param name="confSplitRooms"></param>
        /// <param name="conflictRooms"></param>
        /// <param name="vConf"></param>
        /// <returns></returns>
        private bool CheckForConflicts1(ref List<vrmConfRoom> confRooms, ref List<SplitRoom> confSplitRooms, ref String conflictRooms, ref vrmConference vConf)
        {
            vrmConfRoom room = null;
            List<ICriterion> criterionList = null;
            Double dur = Double.MinValue;
            DateTime confdate = DateTime.MinValue;
            DateTime endDate = DateTime.MinValue;
            List<vrmConfRoom> confRoomList = null;
            string sqlFilter = "";
            bool bret = false;
            try
            {
                if (confSplitRooms == null && confRooms != null)
                {
                    dur = vConf.duration;
                    //confdate = confdate.Subtract(new TimeSpan(0, 0, confdate.Second)).Add(new TimeSpan(0, 0, 45));

                    confdate = vConf.confdate;
                    //if (vConf.immediate == 1)
                    //{
                    //    timeZone.changeToGMTTime(sysSettings.TimeZone, ref confdate);
                    //}
                    endDate = confdate.AddMinutes(dur);
                    //endDate = endDate.AddSeconds(45);
                    //confdate = confdate.AddSeconds(45);

                    criterionList = new List<ICriterion>();
                    confRoomList = new List<vrmConfRoom>();

                    sqlFilter = "(StartDate < '" + endDate.ToString() + "' and StartDate >= '" + confdate.ToString() + "')or('" + confdate.ToString()
                        + "' >= StartDate and '" + confdate.ToString() + "' < dateadd(minute, duration,startdate))";
                    criterionList.Add(Expression.Sql(sqlFilter));
                    
                    confRoomList = m_IconfRoom.GetByCriteria(criterionList);
                    for (int ccnt = 0; ccnt < confRoomList.Count; ccnt++)
                    {
                        room = confRoomList[ccnt];
                        if (room.confid != vConf.confid)
                        {
                            room.Conf = m_vrmConfDAO.GetByConfId(room.confid, room.instanceid);
                            if (room.Conf.deleted == 0)
                            {
                                if (room.Conf.status != vrmConfStatus.Completed && room.Conf.status != vrmConfStatus.Deleted && room.Conf.status != vrmConfStatus.Terminated)
                                {
                                    /* LinQ
                                    var conflictroom = from innrRoom in confRooms
                                                       where innrRoom.roomId == room.roomId
                                                       select innrRoom;
                                    */
                                    if (confRooms != null)
                                    {
                                        foreach (vrmConfRoom cRoom in confRooms)
                                        {
                                            if (room.roomId == cRoom.roomId)
                                            {
                                                bret = true;
                                                if (conflictRooms == "")
                                                    conflictRooms = "'" + cRoom.Room.Name.Trim() + "'";
                                                else
                                                    conflictRooms += ", '" + cRoom.Room.Name.Trim() + "'";
                                            }

                                        }
                                    }
                                }
                            }
                        }
                    }

                }
                else if (confSplitRooms != null)
                {
                    if (confSplitRooms.Count > 0)
                    {
                        //Need to implement logic for splitroom
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return bret;
        }
        #endregion

        #region CheckForConflicts
        /// <summary>
        /// CheckForConflicts
        /// </summary>
        /// <param name="confRooms"></param>
        /// <param name="confSplitRooms"></param>
        /// <param name="conflictRooms"></param>
        /// <param name="vConf"></param>
        /// <returns></returns>
        private bool CheckForConflicts(ref string roomList, ref List<SplitRoom> confSplitRooms, ref String conflictRooms, ref vrmConference vConf)
        {
            string startTime = "";
            string endTime = "";
            bool conflict = false;
            try
            {
                if (roomList == null)
                    return false;
                if (roomList == "")
                    return false;

                startTime = vConf.confdate.ToString();
                endTime = vConf.confdate.AddMinutes(vConf.duration).ToString();

                string stmt = "";
                stmt = " select cf.roomId,cf.confid,cf.instanceid, l.Name from myVRM.DataLayer.vrmConfRoom cf, myVRM.DataLayer.vrmConference c, myVRM.DataLayer.vrmRoom l ";
                stmt += " where ( ('" + startTime + "' >= cf.StartDate and '" + startTime + "'  < dateadd(minute, cf.Duration,cf.StartDate)) ";
                stmt += " or (cf.StartDate >= '" + startTime + "' and cf.StartDate < '" + endTime + "') ) and c.confid = cf.confid and c.instanceid=cf.instanceid ";
                stmt += " and c.orgId ='"+ organizationID.ToString() +"' and c.deleted = 0  AND ( NOT (cf.confid = " + vConf.confid.ToString() + ")) and l.roomId = cf.roomId ";
                stmt += " and l.roomId in (" + roomList + ")";
                
                IList conferences = m_vrmConfDAO.execQuery(stmt);

                conflictRooms = "";
                string rmName = "";
                foreach (object[] obj in conferences)
                {
                    rmName = obj[3].ToString();

                    conflict = true;
                    if (conflictRooms == "")
                        conflictRooms = "'" + rmName.Trim() + "'";
                    else
                        conflictRooms += ", '" + rmName.Trim() + "'";
                }
                return conflict;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion

        #region CheckForServiceTypeConflicts
        /// <summary>
        /// CheckForServiceTypeConflicts //FB 2219
        /// </summary>
        /// <param name="confRooms"></param>
        /// <param name="confSplitRooms"></param>
        /// <param name="conflictRooms"></param>
        /// <param name="vConf"></param>
        /// <returns></returns>
        private bool CheckForServiceTypeConflicts(ref string roomList, ref List<SplitRoom> confSplitRooms, ref String conflictRooms, ref vrmConference vConf)
        {
            string serviceType = "";
            string stmt = "";
            string rmName = "";//FB 2334
            string rmdedvideo = "";//FB 2334
            bool conflict = false;
            try
            {
                if (roomList == null)
                    return false;
                if (roomList == "")
                    return false;

                serviceType = vConf.ServiceType.ToString();
                //FB 2411 Start
                string serivetypein = "0";
                if (serviceType == "3")
                    serivetypein = "1,2,3";
                else if (serviceType == "2")
                    serivetypein = "1,2";
                else if (serviceType == "1")
                    serivetypein = "1";
                if (serviceType != "-1" && serviceType != "0")
                {
                    stmt = " select l.roomId, l.Name, l.ServiceType from myVRM.DataLayer.vrmRoom l ";
                    stmt += " where ( l.ServiceType not in (" + serivetypein + ")) and l.roomId in (" + roomList + ")";
                    //FB 2411 End
                    IList rooms = m_vrmRoomDAO.execQuery(stmt);

                    conflictRooms = "";
                    rmName = "";
                    foreach (object[] obj in rooms)
                    {
                        rmName = obj[1].ToString();

                        conflict = true;
                        if (conflictRooms == "")
                            conflictRooms = "'" + rmName.Trim() + "'";
                        else
                            conflictRooms += ", '" + rmName.Trim() + "'";
                    }
                }
                //FB 2334 start
                if (vConf.conftype == 7)
                {
                    stmt = " select l.roomId, l.Name,l.DedicatedVideo from myVRM.DataLayer.vrmRoom l ";
                    stmt += " where l.roomId in (" + roomList + ")";

                    IList rooms = m_vrmRoomDAO.execQuery(stmt);

                    conflictRooms = "";
                    rmName = "";
                    rmdedvideo = "";
                    foreach (object[] obj in rooms)
                    {
                        rmName = obj[1].ToString();
                        rmdedvideo = obj[2].ToString();
                        if (rmdedvideo.Trim() == "1")
                        {
                            conflict = true;
                            if (conflictRooms == "")
                                conflictRooms = "'" + rmName.Trim() + "'";
                            else
                                conflictRooms += ", '" + rmName.Trim() + "'";
                        }
                    }
                }
                //FB 2334 End
                return conflict;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion

        #region CheckInstanceDuplication
        /// <summary>
        /// CheckInstanceDuplication
        /// </summary>
        /// <param name="vConf"></param>
        /// <returns></returns>
        private bool CheckInstanceDuplication(ref vrmConference vConf)
        {
            List<ICriterion> criterionList = null;
            Double dur = Double.MinValue;
            DateTime confdate = DateTime.MinValue;
            DateTime endDate = DateTime.MinValue;
            string sqlFilter = "";
            try
            {
                if (vConf != null)
                {
                    dur = vConf.duration;
                    confdate = vConf.confdate;
                    endDate = confdate.AddMinutes(dur);
                    endDate = endDate.AddSeconds(50);
                    confdate = confdate.AddSeconds(50);

                    criterionList = new List<ICriterion>();

                    sqlFilter = "((confdate < '" + endDate.ToString() + "' and confdate >= '" + confdate.ToString() + "')or('" + confdate.ToString()
                        + "' >= confdate and '" + confdate.ToString() + "' < dateadd(minute, duration,confdate))) and confid='" + vConf.confid + "'";
                    criterionList.Add(Expression.Sql(sqlFilter));

                    List<vrmConference> confInstances = m_vrmConfDAO.GetByCriteria(criterionList, true);
                    vrmConference cf;
                    for (int ccnt = 0; ccnt < confInstances.Count; ccnt++)
                    {
                        cf = confInstances[ccnt];
                        if (!((cf.confid == vConf.confid) && (cf.instanceid == vConf.instanceid)))
                            return false;
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return true;
        }
        #endregion

        #region CheckFinancial
        /// <summary>
        /// CheckFinancial
        /// </summary>
        /// <param name="totalEndpoints"></param>
        /// <param name="vConf"></param>
        /// <returns></returns>
        private bool CheckFinancial(ref Int32 totalEndpoints, ref vrmConference vConf)
        {
            vrmAccount account = null;
            int totalDuration = 0;
            bool bRet = true;
            try
            {
                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                if (orgInfo.overAllocation == 1)
                    return bRet;

                if (totalEndpoints > 0)
                {
                    account = m_userAccountDAO.GetByUserId(vConf.userid);
                    totalDuration = vConf.duration * totalEndpoints;

                    if (account.TimeRemaining < totalDuration)
                        bRet = false;
                }
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
            return bRet;
        }
        #endregion

        # region BillConference
        /// <summary>
        /// BillConference
        /// </summary>
        /// <param name="vConf"></param>
        private void BillConference(ref vrmConference vConf)
        {
            try
            {
                OrgData orgdata = m_IOrgSettingsDAO.GetByOrgId(organizationID);
                if (orgdata.AccountingLogic != 0)
                {
                    int bill = 0;
                    String query = "";
                    vrmAccount UserAccount = m_userAccountDAO.GetById(vConf.owner);

                    query = "SELECT CR.roomId FROM myVRM.DataLayer.vrmConfRoom CR WHERE CR.confid = " + vConf.confid.ToString()
                          + " AND CR.instanceid = " + vConf.instanceid.ToString();
                    IList confRoomresult = m_vrmConfDAO.execQuery(query);

                    if (orgdata.AccountingLogic == 1) // Charge Back to owner
                    {
                        query = "SELECT CU.userid FROM myVRM.DataLayer.vrmConfUser CU WHERE CU.confid = " + vConf.confid.ToString()
                              + " AND CU.instanceid = " + vConf.instanceid.ToString() + " AND CU.invitee = 1"; // Both users and guests
                        IList confUsrresult = m_vrmConfDAO.execQuery(query);

                        bill = (confUsrresult.Count + confRoomresult.Count) * vConf.duration;
                        UserAccount.TimeRemaining = UserAccount.TimeRemaining - bill;
                        m_userAccountDAO.Update(UserAccount);
                    }
                    else if (orgdata.AccountingLogic == 2) // Charge Back to individual
                    {
                        query = "SELECT CU.userid FROM myVRM.DataLayer.vrmConfUser CU, myVRM.DataLayer.vrmGuestUser U WHERE CU.confid = " + vConf.confid.ToString()
                              + " AND CU.instanceid = " + vConf.instanceid.ToString() + " AND CU.invitee = 1 AND U.userid = CU.userid";

                        IList result = m_vrmConfDAO.execQuery(query);
                        bill = (result.Count + confRoomresult.Count) * vConf.duration;
                        UserAccount.TimeRemaining = UserAccount.TimeRemaining - bill;
                        m_userAccountDAO.Update(UserAccount);

                        query = "SELECT CU.userid FROM myVRM.DataLayer.vrmConfUser CU, myVRM.DataLayer.vrmUser U WHERE CU.confid =" + vConf.confid.ToString()
                              + " and  CU.instanceid =" + vConf.instanceid.ToString() + " AND CU.invitee = 1 AND U.userid = CU.userid";
                        result = m_vrmConfDAO.execQuery(query);
                        for (int i = 0; i < result.Count; i++)
                        {
                            UserAccount = m_userAccountDAO.GetById((int)result[i]);
                            UserAccount.TimeRemaining = UserAccount.TimeRemaining - vConf.duration;
                            m_userAccountDAO.Update(UserAccount);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                m_log.Error("RefundConference" + ex.Message);
            }
        }
        #endregion

        #region Insert Conference Locations
        /// <summary>
        /// Insert Conference Locations
        /// </summary>
        private bool InsertLocations(ref List<vrmConfRoom> confRooms, ref List<SplitRoom> confSplitRooms, ref vrmConference vConf, ref List<vrmRoom> guestRooms, ref List<vrmEndPoint> guestEndpoints, ref int errNo) //FB 2426
        {
            bool bret = true;
            vrmEndPoint ept = null;
            vrmConfRoom confRoom = null;
            try
            {
                //FB 2426 Start
                if (guestRooms != null)
                {
                    if (!InsertGuestRoom(ref guestRooms, ref guestEndpoints, ref errNo, ref confRooms, ref vConf))
                    {
                        myVRMEx = new myVRMException(errNo);
                        m_log.Error("InsertGuestRoom", myVRMEx);
                        return false;
                    }
                }
                //FB 2426 End
                if (confRooms != null)
                {
                    for (int ccnt = 0; ccnt < confRooms.Count; ccnt++)
                    {
                        confRoom = null;
                        confRoom = confRooms[ccnt];

                        confRoom.confid = vConf.confid;
                        confRoom.instanceid = vConf.instanceid;
                        confRoom.confuId = vConf.confnumname;
                        confRoom.StartDate = vConf.confdate;
                        confRoom.Duration = vConf.duration;
                        confRoom.disabled = 0;
                        confRoom.LastRunDateTime = DateTime.UtcNow;
                        confRoom.TerminalType = vrmConfTerminalType.Room; //FB 2501 Call Monitoring
                        confRoom.layout = vConf.videolayout; //Tamil
                        confRoom.BridgeExtNo = ""; //FB 2610
                        if (vConf.conftype != vrmConfType.RooomOnly && confRoom.Room.endpointid > 0) //FB 2448
                        {
                            ept = m_vrmEpt.GetByEptId(confRoom.Room.endpointid);
                            //ept = m_vrmEpt.GetById(confRoom.Room.endpointid);
                            confRoom.bridgeIPISDNAddress = ept.MCUAddress;
                            confRoom.ipisdnaddress = ept.address;
                            confRoom.bridgeid = ept.bridgeid;
                            confRoom.bridgeAddressType = ept.MCUAddressType;
                            confRoom.audioorvideo = confRoom.Room.VideoAvailable;
                            confRoom.addressType = ept.addresstype;
                            confRoom.ApiPortNo = ept.ApiPortNo;
                            confRoom.connectionType = ept.connectiontype;
                            confRoom.defLineRate = ept.linerateid;
                            //FB 2610 Starts
                            if (confRoom.bridgeid >= 0)
                            {
                                vrmMCU confMCU = m_vrmMCU.GetById(confRoom.bridgeid);
                                confRoom.BridgeExtNo = confMCU.BridgeExtNo;
                            }
                            //FB 2610 Ends

                            confRoom.MultiCodecAddress = "";  //FB 2400 start
                            if(ept.isTelePresence == 1)
                                confRoom.MultiCodecAddress = ept.MultiCodecAddress;  //FB 2400 end

                            //FB 2595 Start
                            if (vConf.Secured == 1)
                                confRoom.Secured = 1;
                            else
                                confRoom.Secured = 0;
                            //FB 2595 End
                        }
                        m_IconfRoom.Save(confRoom);
                    }
                }
                if (confSplitRooms != null)
                {
                    if (confSplitRooms.Count > 0)
                    {
                        //Need to implement logic for splitroom
                    }
                }
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                bret = false;
            }
            return bret;
        }
        #endregion

        #region Insert Conference Participants
        /// <summary>
        /// Insert Conference Participants
        /// </summary>
        /// <param name="confUsers"></param>
        /// <param name="addedGuests"></param>
        /// <param name="vConf"></param>
        /// <returns></returns>
        private bool InsertParty(ref List<vrmConfUser> confUsers, ref List<vrmGuestUser> addedGuests, ref vrmConference vConf)
        {
            bool bret = true;
            vrmGuestUser gusr = null;
            int userid = 0;
            int ccnt = 0;
            try
            {
                if (confUsers != null)
                {
                    if (addedGuests != null)
                    {
                        for (ccnt = 0; ccnt < addedGuests.Count; ccnt++)
                        {
                            userid = 0;
                            gusr = addedGuests[ccnt];
                            gusr.Secured = vConf.Secured;//FB 2595
                            userid = GuestRegister(ref gusr);

                            if (userid <= 0)
                                return false;

                            var addedparties = from party in confUsers
                                               where party.endptURL == gusr.Email
                                               select party;

                            if (addedparties != null)
                            {
                                foreach (vrmConfUser addedparty in addedparties)
                                {
                                    addedparty.userid = userid;
                                    addedparty.TerminalType = vrmConfTerminalType.GuestUser;//FB 2501 Call Monitoring; //FB 2501 Call Monitoring
                                    //FB 2595 Start
                                    if (vConf.Secured == 1)
                                        addedparty.Secured = 1;
                                    else
                                        addedparty.Secured = 0;
                                    //FB 2595 End
                                }
                            }
                        }

                    }
                    vrmConfUser cfUser = null;
                    for (ccnt = 0; ccnt < confUsers.Count; ccnt++)
                    {
                        cfUser = null;
                        cfUser = confUsers[ccnt];

                        cfUser.confid = vConf.confid;
                        cfUser.instanceid = vConf.instanceid;
                        cfUser.confuId = vConf.confnumname;
                        if (vConf.immediate == 1 )
                            cfUser.status = 1; //Accepted for immediate conference
                        cfUser.layout = vConf.videolayout;//Tamil
                        //FB 2595 Start
                        if (vConf.Secured == 1)
                            cfUser.Secured = 1;
                        else
                            cfUser.Secured = 0;
                        //FB 2595 End
                        m_IconfUser.Save(cfUser);
                    }
                }
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                bret = false;
            }
            return bret;
        }
        #endregion

        #region Guest Register
        /// <summary>
        /// GuestRegister (COM to .NET Convertion)
        /// </summary>
        /// <param name="Gusr"></param>
        /// <returns></returns>
        public int GuestRegister(ref vrmGuestUser Gusr)
        {
            int gid = 0;
            int uid = 0;
            int userid = 0;
            cryptography.Crypto crypto = null;
            List<vrmGuestUser> Gusrlist = null;
            List<ICriterion> criterionList = null;
            try
            {
                criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("Email", Gusr.Email));
                Gusrlist = m_vrmGuestDAO.GetByCriteria(criterionList);
                for (int i = 0; i < Gusrlist.Count; i++)
                {
                    userid = Gusrlist[i].userid;
                    break;
                }

                if (userid <= 0)
                {

                    string uString = "SELECT MAX(userid) + 1 as userid FROM myVRM.DataLayer.vrmUser";
                    IList list = m_vrmUserDAO.execQuery(uString);
					//Empty DB Issue FB 2164
                    if (list[0] != null)
                        uid = Int32.Parse(list[0].ToString());

                    string qString = "SELECT MAX(userid) + 1 as userid FROM myVRM.DataLayer.vrmGuestUser";
                    IList glist = m_vrmGuestDAO.execQuery(qString);
					//Empty DB Issue FB 2164
                    if (glist[0] != null)
                        gid = Int32.Parse(glist[0].ToString());
                    
                    if (uid > gid)
                    {
                        userid = uid;
                    }
                    else
                    {
                        userid = gid;
                    }
                    Gusr.companyId = organizationID;
                    Gusr.userid = userid;
                    Gusr.TimeZone = confScheduler.TimeZone; //Guest TimeZone Issue
                    Gusr.emailmask = 1; //Guest email invitation issue
                    crypto = new cryptography.Crypto();
                    Gusr.Password = crypto.encrypt("pwd");
                    m_vrmGuestDAO.Save(Gusr);
                }
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                userid = 0;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                userid = 0;

            }
            return userid;
        }
        #endregion

        #region LoadAdvAVParams
        /// <summary>
        /// LoadAdvAVParams
        /// </summary>
        /// <param name="confAVParams"></param>
        /// <param name="advParamNode"></param>
        /// <returns></returns>
        private bool LoadAdvAVParams(ref vrmConfAdvAvParams confAVParams, XmlNode advParamNode, int ConfOrigin)//FB 2429
        {
            try
            {
                XmlNode node;
                confAVParams = new vrmConfAdvAvParams();
                if (advParamNode == null)
                    throw new myVRMException("AV advanced params are not available in inXML");

                //FB 2429 - Starts
                Int32 OrgLinerate = 0;
                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);
                OrgLinerate = orgInfo.DefLinerate;
                //FB 2429 - End

                node = advParamNode.SelectSingleNode("maxAudioPart");
                int maxAudioPart = 0;
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out maxAudioPart);

                if (maxAudioPart < 0) maxAudioPart = 0;

                node = advParamNode.SelectSingleNode("maxVideoPart");
                int maxVideoPart = 0;
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out maxVideoPart);

                if (maxVideoPart < 0) maxVideoPart = 0;

                node = advParamNode.SelectSingleNode("restrictProtocol");
                int restrictProtocol = 0;
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out restrictProtocol);

                if (restrictProtocol <= 0) restrictProtocol = 0;

                node = advParamNode.SelectSingleNode("restrictAV");
                int restrictAV = 0;
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out restrictAV);

                if (restrictAV <= 0) restrictAV = 1; //default audio

                node = advParamNode.SelectSingleNode("videoLayout");
                int videoLayout = 0;
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out videoLayout);

                if (videoLayout < 0) videoLayout = 1;

                node = advParamNode.SelectSingleNode("maxLineRateID");
                int maxLineRateID = 0;
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out maxLineRateID);

                if (maxLineRateID <= 0) maxLineRateID = 384; //default

                //FB 2429 - Starts
                if (ConfOrigin > 0)
                {
                    maxLineRateID = OrgLinerate;
                    if (maxLineRateID <= 0)
                        maxLineRateID = 384;
                }
                //FB 2429 - End

                node = advParamNode.SelectSingleNode("audioCodec");
                int audioCodec = 0;
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out audioCodec);

                if (audioCodec < 0) audioCodec = 0;

                node = advParamNode.SelectSingleNode("dualStream");
                int dualStream = 0;
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out dualStream);

                if (dualStream < 0) dualStream = 0;

                node = advParamNode.SelectSingleNode("confOnPort");
                int confOnPort = 0;
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out confOnPort);

                if (confOnPort < 0) confOnPort = 0;

                node = advParamNode.SelectSingleNode("encryption");
                int encryption = 0;
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out encryption);

                if (encryption < 0) encryption = 0;

                node = advParamNode.SelectSingleNode("lectureMode");
                int lectureMode = 0;
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out lectureMode);

                if (lectureMode < 0) lectureMode = 0;

                node = advParamNode.SelectSingleNode("VideoMode");
                int VideoMode = 0;
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out VideoMode);

                if (VideoMode <= 0) VideoMode = 3; //default

                node = advParamNode.SelectSingleNode("videoCodec");
                int videoCodec = 0;
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out videoCodec);

                if (videoCodec < 0) videoCodec = 0;

                node = advParamNode.SelectSingleNode("SingleDialin");
                int SingleDialin = 0;
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out SingleDialin);

                if (SingleDialin < 0) SingleDialin = 0;

                /** Fb 2376 **/
                node = advParamNode.SelectSingleNode("externalBridge");
                if (node != null)
                    confAVParams.externalBridge = advParamNode.SelectSingleNode("externalBridge").InnerText;

                node = advParamNode.SelectSingleNode("internalBridge");
                if (node != null)
                    confAVParams.internalBridge = advParamNode.SelectSingleNode("internalBridge").InnerText;

                /** Fb 2376 **/

                //FB 2501 Starts
                //int confVNOC = 0;
                //node = advParamNode.SelectSingleNode("VNOCOperator");
                //if (node != null)
                //    Int32.TryParse(advParamNode.SelectSingleNode("VNOCOperator").InnerText.ToString(), out confVNOC);

                //confAVParams.confVNOC = confVNOC;
                //FB 2501 Ends

                // FB 2501 FECC Starts
                node = advParamNode.SelectSingleNode("FECCMode");
                int FECCMode = 0;
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out FECCMode);

                if (FECCMode < 0) FECCMode = 0;
                // FB 2501 FECC Ends

                //FB 2441 Starts
                node = advParamNode.SelectSingleNode("PolycomSendMail");
                int PolycomSendMail = 0;
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out PolycomSendMail);
                node = advParamNode.SelectSingleNode("PolycomTemplate");
                if (node != null)
                    confAVParams.PolycomTemplate = advParamNode.SelectSingleNode("PolycomTemplate").InnerText;
                //FB 2441 Ends

                confAVParams.maxAudioParticipants = maxAudioPart;
                confAVParams.maxVideoParticipants = maxVideoPart;
                confAVParams.videoProtocolID = restrictProtocol;
                confAVParams.mediaID = restrictAV;
                confAVParams.videoLayoutID = videoLayout;
                confAVParams.linerateID = maxLineRateID;
                confAVParams.audioAlgorithmID = audioCodec;
                confAVParams.videoSession = videoCodec;
                confAVParams.dualStreamModeID = dualStream;
                confAVParams.conferenceOnPort = confOnPort;
                confAVParams.encryption = encryption;
                confAVParams.lectureMode = lectureMode;
                confAVParams.videoMode = VideoMode;
                confAVParams.singleDialin = SingleDialin;
                confAVParams.feccMode = FECCMode; // FB 2501 FECC
                confAVParams.PolycomSendEmail = PolycomSendMail; // FB 2441
            }
            catch (myVRMException me)
            {
                m_log.Error("myVRMException on LoadAdvAVParams", me);
                throw me;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException on LoadAdvAVParams", e);
                throw e;
            }
            return true;
        }
        #endregion

        #region InsertAdvAVParams
        /// <summary>
        /// InsertAdvAVParams
        /// </summary>
        /// <param name="confAVParams"></param>
        /// <param name="vConf"></param>
        /// <returns></returns>
        private bool InsertAdvAVParams(ref vrmConfAdvAvParams confAVParams, ref vrmConference vConf)
        {
            try
            {
                confAVParams.confid = vConf.confid;
                confAVParams.instanceid = vConf.instanceid;
                confAVParams.confuId = vConf.confnumname;
                //FB 2501 Call Monitoring Start
                confAVParams.Layout = 0;
                confAVParams.MuteTxaudio = 0;
                confAVParams.MuteRxaudio = 0;
                confAVParams.MuteTxvideo = 0;
                confAVParams.MuteRxvideo = 0;
                confAVParams.Recording = 0;
                confAVParams.ConfLockUnlock = 0;
                confAVParams.PacketLoss = "";
                confAVParams.Camera = 0;
                //FB 2501 Call Monitoring Ends
                m_confAdvAvParamsDAO.Save(confAVParams);
            }
            catch (Exception e)
            {
                throw e;
            }
            return true;
        }
        #endregion

        #region InsertRecurInfo
        /// <summary>
        /// InsertRecurInfo
        /// </summary>
        /// <param name="recurInfo"></param>
        /// <returns></returns>
        private bool InsertRecurInfo(ref vrmRecurInfo recurInfo)
        {
            try
            {
                if (recurInfo == null)
                    throw new myVRMException("Error in inserting recurinfo table.");

                recurInfo.confid = confLst[0].confid; //first instance information
                recurInfo.startTime = confLst[0].confdate;
                recurInfo.timezoneid = confLst[0].timezone;
                if (recurInfo.endType != 3)
                {
                    int totalIns = confLst.Count - 1;
                    recurInfo.endTime = confLst[totalIns].confdate;
                }
                m_ConfRecurDAO.Save(recurInfo);
            }
            catch (Exception e)
            {
                throw e;
            }
            return true;
        }
        #endregion

        #region InsertDefunctRecurInfo
        /// <summary>
        /// InsertRecurInfo FB 2218
        /// </summary>
        /// <param name="recurInfo"></param>
        /// <returns></returns>
        private bool InsertDefunctRecurInfo(ref vrmRecurInfoDefunct recurInfo)
        {
            try
            {
                if (recurInfo == null)
                    throw new myVRMException("Error in inserting recurinfo table.");

                recurInfo.confid = confLst[0].confid; //first instance information
                recurInfo.startTime = confLst[0].confdate;
                recurInfo.timezoneid = confLst[0].timezone;
                if (recurInfo.endType != 3)
                {
                    int totalIns = confLst.Count - 1;
                    recurInfo.endTime = confLst[totalIns].confdate;
                }
                m_confDAO.GetConfRecurDefunctDAO().Save(recurInfo);
            }
            catch (Exception e)
            {
                throw e;
            }
            return true;
        }

        private bool UpdateRecurDefuntInfo(int oldConfid, int newConfid,ref vrmRecurInfo recurinfo)
        {
            List<ICriterion> recurInfo = null;
            List<vrmRecurInfoDefunct> recurDefunctPattern = null;
            vrmRecurInfoDefunct newDefunctinfo = null;

            try
            {
               recurInfo = new List<ICriterion>();
               recurInfo.Add(Expression.Eq("confid", oldConfid));
               recurDefunctPattern = m_confDAO.GetConfRecurDefunctDAO().GetByCriteria(recurInfo, true);//Custom Instances

               if (recurDefunctPattern.Count > 0)
               {
                   newDefunctinfo = recurDefunctPattern[0];
                   if (newDefunctinfo != null)
                   {
                       if (recurinfo.recurType < 5)
                       {
                           m_confDAO.GetConfRecurDefunctDAO().Delete(newDefunctinfo);

                       }
                       else
                       {
                           newDefunctinfo.confid = newConfid;
                           m_confDAO.GetConfRecurDefunctDAO().Update(newDefunctinfo);
                       }
                   }
               }
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
                
        }

        #endregion

        #region Set Conf parameters
        /// <summary>
        /// SetConfparams (COM to .NET Convertion)
        /// </summary>
        /// <param name="vConf"></param>
        /// <param name="recurInsConf"></param>
        /// <returns></returns>
        private bool SetConfParams(ref vrmConference vConf, ref vrmConference recurInsConf)
        {
            try
            {
                recurInsConf.userid = vConf.userid;
                recurInsConf.owner = vConf.owner;
                recurInsConf.orgId = vConf.orgId;
                recurInsConf.deleted = 0;
                recurInsConf.status = 0;
                recurInsConf.ConfOrigin = vConf.ConfOrigin;
                recurInsConf.instanceid = vConf.instanceid;
                recurInsConf.externalname = vConf.externalname;
                recurInsConf.password = vConf.password;
                recurInsConf.isPublic = vConf.isPublic;
                recurInsConf.ServiceType = vConf.ServiceType;//FB 2219
                //recurInsConf.ConceirgeSupport = vConf.ConceirgeSupport;//FB 2341//FB 2377
                recurInsConf.continous = vConf.continous;
                recurInsConf.recuring = vConf.recuring;
                recurInsConf.description = vConf.description;
                recurInsConf.immediate = vConf.immediate;
                recurInsConf.internalname = vConf.internalname;
                recurInsConf.videosession = vConf.videosession;
                recurInsConf.connect2 = vConf.connect2;
                recurInsConf.totalpoints = vConf.totalpoints;
                recurInsConf.settingtime = vConf.settingtime;
                recurInsConf.videolayout = vConf.videolayout;
                recurInsConf.videoprotocol = vConf.videoprotocol;
                recurInsConf.manualvideolayout = vConf.manualvideolayout;
                recurInsConf.conftype = vConf.conftype;
                recurInsConf.lecturemode = vConf.lecturemode;
                recurInsConf.lecturer = vConf.lecturer;
                recurInsConf.dynamicinvite = vConf.dynamicinvite;
                recurInsConf.CreateType = vConf.CreateType;
                recurInsConf.ConfDeptID = vConf.ConfDeptID;
                recurInsConf.LastRunDateTime = vConf.LastRunDateTime;
                recurInsConf.isVMR = vConf.isVMR;//FB 2376
                recurInsConf.StartMode = vConf.StartMode;//FB 2501
                recurInsConf.loginUser = vConf.loginUser;// FB 2501
                recurInsConf.confVNOC = vConf.confVNOC;//FB 2501
                //FB 2632 Starts
                recurInsConf.OnSiteAVSupport = vConf.OnSiteAVSupport; 
                recurInsConf.MeetandGreet = vConf.MeetandGreet;
                recurInsConf.ConciergeMonitoring = vConf.ConciergeMonitoring;
                recurInsConf.DedicatedVNOCOperator = recurInsConf.DedicatedVNOCOperator;
                //FB 2632 Ends
                recurInsConf.Secured = vConf.Secured;//FB 2595
                recurInsConf.ConfMode = (int)eConfMode.CONF_NEW;
                if (!newConf)
                {
                    recurInsConf.confid = vConf.confid;
                    recurInsConf.instanceid = vConf.instanceid;
                    recurInsConf.confnumname = vConf.confnumname;
                    recurInsConf.ConfMode = (int)eConfMode.CONF_EDIT;
                }

            }
            catch (myVRMException e)
            {
                throw new myVRMException("myVRMException in SetConfParams", e);
            }
            catch (Exception e)
            {
                throw new myVRMException("sytemException in SetConfParams", e);
            }
            return true;
        }
        #endregion

        #region SetConferenceOutXML
        /// <summary>
        /// SetConferenceOutXML
        /// </summary>
        /// <param name="confUser"></param>
        /// <returns></returns>
        private string SetConferenceOutXML(ref List<vrmConfUser> confUser,ref List<vrmConfRoom> confRoom)//FB 2426
        {
            try
            {
                IGrpParticipantsDao m_IGrpPartyDao = m_usrDAO.GetGrpParticipantDAO();
                StringBuilder outputXML = new StringBuilder();
                StringBuilder PartyXML = null;
                String fname = "", lname = "", email = "", invitee = "", invited = "", cc = "";
                List<ICriterion> criter = null;
                List<int> grousID = null;
                vrmGuestUser Gust = null;
                vrmUser usr = null;

                outputXML.Append("<setConference>");
                outputXML.Append("<conferences>");
                outputXML.Append("<conference>");
                outputXML.Append("<confID>" + confLst[0].confid);

                if (!isRecurring)
                    outputXML.Append("," + confLst[0].instanceid);

                outputXML.Append("</confID>");

                if (confUser != null)
                {
                    for (int i = 0; i < confUser.Count; i++)
                    {
                        grousID = new List<int>();
                        criter = new List<ICriterion>();

                        usr = m_vrmUserDAO.GetByUserId(confUser[i].userid);

                        if (usr == null)
                        {
                            Gust = m_vrmGuestDAO.GetByUserId(confUser[i].userid);
                            fname = Gust.FirstName;
                            lname = Gust.LastName;
                            email = Gust.Email;
                        }
                        else
                        {
                            fname = usr.FirstName;
                            lname = usr.LastName;
                            email = usr.Email;
                        }
                        PartyXML = new StringBuilder();
                        PartyXML.Append("<party>");
                        PartyXML.Append("<partyID>" + confUser[i].userid + "</partyID>");
                        PartyXML.Append("<partyFirstName>" + fname + "</partyFirstName>");
                        PartyXML.Append("<partyLastName>" + lname + "</partyLastName>");
                        PartyXML.Append("<partyEmail>" + email + "</partyEmail>");

                        criter.Add(Expression.Eq("UserID", confUser[i].userid));
                        List<vrmGroupParticipant> partyGroups = m_IGrpPartyDao.GetByCriteria(criter);
                        for (int j = 0; j < partyGroups.Count; j++)
                        {
                            if (!grousID.Contains(partyGroups[j].GroupID))
                                grousID.Add(partyGroups[j].GroupID);
                        }
                        if (grousID.Count > 0)
                        {
                            criter = new List<ICriterion>();
                            criter.Add(Expression.In("GroupID", grousID));
                            m_usrFactory.FetchGroups(criter, ref PartyXML, 1, "inGroup");
                        }
                        PartyXML.Append("</party>");

                        if (confUser[i].invitee == 1)
                            invited += PartyXML.ToString();
                        else if (confUser[i].invitee == 2)
                            invitee += PartyXML.ToString();
                        else if (confUser[i].invitee == 0)
                            cc += PartyXML.ToString();
                    }

                    outputXML.Append("<invited>" + invited + "</invited>");//External Attendee
                    outputXML.Append("<invitee>" + invitee + "</invitee>"); //Room Attendee
                    outputXML.Append("<cc>" + cc + "</cc>");
                }
                else
                {
                    outputXML.Append("<invited></invited>");
                    outputXML.Append("<invitee></invitee>");
                    outputXML.Append("<cc></cc>");
                }
                //FB 2426 Start
                if (confRoom != null)
                {
                    outputXML.Append("<GuestRooms>");
                    for (int j = 0; j < confRoom.Count; j++)
                    {
                        if (confRoom[j].Extroom == 1)
                        {
                            outputXML.Append("<Endpoint>");
                            outputXML.Append("<Type>R</Type>");
                            outputXML.Append("<ID>"+confRoom[j].roomId.ToString()+"</ID>");
                            outputXML.Append("<UseDefault>1</UseDefault>");
                            outputXML.Append("<IsLecturer>0</IsLecturer>");
                            outputXML.Append("<EndpointID>"+confRoom[j].endpointId.ToString()+"</EndpointID>");
                            outputXML.Append("<ProfileID>"+confRoom[j].profileId.ToString()+"</ProfileID>");
                            outputXML.Append("<BridgeID>-1</BridgeID>");
                            outputXML.Append("<AddressType></AddressType>");
                            outputXML.Append("<Address></Address>");
                            outputXML.Append("<VideoEquipment></VideoEquipment>");
                            outputXML.Append("<connectionType></connectionType>");
                            outputXML.Append("<Bandwidth></Bandwidth>");
                            outputXML.Append("<IsOutside></IsOutside>");
                            outputXML.Append("<DefaultProtocol></DefaultProtocol>");
                            outputXML.Append(" <Connection>2</Connection>");
                            outputXML.Append(" <URL></URL>");
                            outputXML.Append(" <ExchangeID></ExchangeID>");
                            outputXML.Append(" <APIPortNo>23</APIPortNo>");
                            outputXML.Append(" <Connect2>-1</Connect2>");
                            outputXML.Append(" <OnFlyRoom>1</OnFlyRoom>");
                            outputXML.Append("</Endpoint>");
                        }
                    }
                    outputXML.Append("</GuestRooms>");
                }
                //FB 2426 End

                outputXML.Append("</conference>");
                outputXML.Append("</conferences>");
                outputXML.Append("</setConference>");
                return outputXML.ToString();
            }
            catch (Exception ex)
            {
                m_log.Error("SetConferenceOutXML", ex);
                return "";
            }
        }
        #endregion

        #region RecurDateList
        /// <summary>
        /// RecurDateList
        /// </summary>
        /// <param name="ConflictDt"></param>
        /// <returns></returns>
        private string RecurDateList(ref System.Data.DataTable ConflictDt)
        {
            StringBuilder RecOutXml = new StringBuilder();
            try
            {
                RecOutXml.Append("<dateList>");
                if (ConflictDt != null)
                {
                    for (int i = 0; i < ConflictDt.Rows.Count; i++)
                    {
                        RecOutXml.Append("<dateTime>");
                        RecOutXml.Append("<startDate>" + ConflictDt.Rows[i]["startDate"].ToString() + "</startDate>"); //timestamp.Format(_T("%m/%d/%Y"))
                        RecOutXml.Append("<startHour>" + ConflictDt.Rows[i]["startHour"].ToString() + "</startHour>");
                        RecOutXml.Append("<startMin>" + ConflictDt.Rows[i]["startMin"].ToString() + "</startMin>");
                        RecOutXml.Append("<startSet>" + ConflictDt.Rows[i]["startSet"].ToString() + "</startSet>");
                        RecOutXml.Append("<durationMin>" + ConflictDt.Rows[i]["durationMin"].ToString() + "</durationMin>");
                        RecOutXml.Append("<conflict>" + ConflictDt.Rows[i]["conflict"].ToString() + "</conflict>");
                        RecOutXml.Append("<conflictRooms>" + ConflictDt.Rows[i]["conflictRooms"].ToString() + "</conflictRooms>");
                        RecOutXml.Append("</dateTime>");
                    }
                }
                RecOutXml.Append("</dateList>");

                return RecOutXml.ToString();
            }
            catch (Exception ex)
            {
                m_log.Error("RecurDateList", ex);
                return "";
            }
        }
        #endregion

        #region GetRecurDateList
        /// <summary>
        /// GetRecurDateList
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetRecurDateList(ref vrmDataObject obj)
        {
            string roomList = "";
            vrmConference vConf = null;
            List<sRecurrDates> recurDates = new List<sRecurrDates>();
            try
            {
                if (obj.inXml == "")
                {
                    myVRMEx = new myVRMException("Invalid input XML.");
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                organizationID = 11; //default organization
                XmlNode node = xd.SelectSingleNode("//getRecurDateList/organizationID");
                if (node == null)
                    throw new myVRMException("Input xml error. Tag <organizationID> not present.");

                Int32.TryParse(node.InnerXml.Trim(), out organizationID);
                if (organizationID < 11)
                {
                    myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);
                if (orgInfo == null)
                {
                    myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//getRecurDateList/userID");
                if (node == null)
                    throw new myVRMException("UserID missing.Operation aborted or UserID not supplied.");

                int loginUserid = 0;
                Int32.TryParse(node.InnerXml.Trim(), out loginUserid);
                if (loginUserid <= 0)
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                node = xd.SelectSingleNode("//getRecurDateList/confID");
                CConfID cID = new CConfID(node.InnerText.Trim());

                if (cID.ID == 0 && cID.instance == 0)
                    newConf = true;
                else
                    newConf = false;
                
                if (newConf)
                {
                    vConf = new vrmConference();
                    vConf.deleted = 0;
                    vConf.ConfDeptID = 0;
                    vConf.confid = cID.ID;
                    vConf.instanceid = cID.instance;
                    vConf.ConfMode = (int)eConfMode.CONF_NEW;
                    vConf.orgId = organizationID;
                }
                else //Edit Mode
                {
                    List<ICriterion> criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("confid", cID.ID));

                    if (cID.RecurMode == 0)
                        criterionList.Add(Expression.Eq("instanceid", cID.instance));

                    List<vrmConference> recConfs = m_vrmConfDAO.GetByCriteria(criterionList, true);

                    if (recConfs == null)
                        throw new myVRMException("Error in fetching recurring conference.");

                    if (recConfs.Count <= 0)
                        throw new myVRMException("Error in fetching recurring conference.");

                    vConf = recConfs[0];
                    vConf.ConfMode = (int)eConfMode.CONF_EDIT;
                }
                vConf.userid = loginUserid;

                XmlNodeList rnodes = xd.SelectNodes("//getRecurDateList/rooms/roomID");
                int rid = 0;
                for (int k = 0; k < rnodes.Count; k++)
                {
                    rid = 0;
                    int.TryParse(rnodes[k].InnerText.Trim(), out rid);
                    if (rid > 0)
                    {
                        if (roomList == "")
                            roomList = rid.ToString();
                        else
                            roomList += "," + rid.ToString();
                    }
                }
                vrmRecurInfo recurInfo = new vrmRecurInfo();
                int errNo = 0;
                isRecurring = true;
                string inxml = "<conference><confInfo>" + xd.FirstChild.InnerXml + "</confInfo></conference>";
                xd = null;
                xd = new XmlDocument();
                xd.LoadXml(inxml);
                RecurringDates(ref xd, ref errNo, ref recurDates, ref recurInfo);
                BuildRecurInstance(ref vConf, ref recurDates, ref obj,ref roomList);
                
                return true;
            }
            catch (Exception ex)
            {
                m_log.Error("GetRecurDateListOutXML", ex);
                return false;
            }
        }
        #endregion

        #region BuildRecurInstance
        /// <summary>
        /// BuildRecurInstance
        /// </summary>
        /// <param name="vConf"></param>
        /// <param name="recurDates"></param>
        /// <param name="obj"></param>
        /// <param name="roomList"></param>
        /// <returns></returns>
        private bool BuildRecurInstance(ref vrmConference vConf, ref List<sRecurrDates> recurDates, ref vrmDataObject obj, ref string roomList)
        {
            StringBuilder RecOutXml = new StringBuilder();
            vrmConference recurInsConf = null;
            DateTime confdate = DateTime.MinValue, confenddate = DateTime.MinValue, setupDate = DateTime.MinValue, tearDate = DateTime.MinValue;
            string conflict = "0";
            List<SplitRoom> splitRooms = null;
            try
            {
                RecOutXml.Append("<dateList>");
                if (recurDates != null)
                {
                    confdate = confenddate = setupDate = tearDate = DateTime.MinValue;
                    for (int ccnt = 0; ccnt < recurDates.Count; ccnt++)
                    {
                        if (ccnt >= vrmConfig.ConfReccurance) //Recurring instance should not exceed maximum limit
                            break;

                        recurInsConf = null;
                        recurInsConf = new vrmConference();

                        SetConfParams(ref vConf, ref recurInsConf);

                        RecOutXml.Append("<dateTime>");
                        RecOutXml.Append("<startDate>" + recurDates[ccnt].RecDatetime.ToShortDateString() + "</startDate>"); //timestamp.Format(_T("%m/%d/%Y"))
                        RecOutXml.Append("<startHour>" + recurDates[ccnt].StartHour + "</startHour>");
                        RecOutXml.Append("<startMin>" + recurDates[ccnt].StartMin + "</startMin>");
                        RecOutXml.Append("<startSet>" + recurDates[ccnt].StartSet + "</startSet>");
                        RecOutXml.Append("<durationMin>" + recurDates[ccnt].Duration + "</durationMin>");
                        conflict = "0";

                        recurInsConf.confdate = recurDates[ccnt].RecDatetime;
                        recurInsConf.conftime = recurDates[ccnt].RecDatetime;
                        recurInsConf.duration = recurDates[ccnt].Duration;
                        recurInsConf.timezone = recurDates[ccnt].Timezone;

                        confdate = recurDates[ccnt].RecDatetime;
                        setupDate = recurDates[ccnt].RecDatetime.Add(new TimeSpan(0, 0, recurDates[ccnt].SetDuration, 0));
                        confenddate = recurDates[ccnt].RecDatetime.Add(new TimeSpan(0, 0, recurDates[ccnt].Duration, 0));
                        tearDate = confenddate.Subtract(new TimeSpan(0, 0, recurDates[ccnt].TearDuration, 0));

                        recurInsConf.SetupTime = setupDate;
                        recurInsConf.TearDownTime = tearDate;

                        //Check for System open hours and days
                        string errMes = "";
                        if (!IsConferenceScheduleable(ref errMes, ref recurInsConf))
                        {
                            // Oops! We don't support conferences at this time.
                            //System Open Hours Conflict <conflict>2</conflict>
                            conflict = "2"; //Timezone conflict
                        }

                        //this is equivalent to converting to GMT while inserting as happening in COM
                        //Get all the date time to GMT this must be done after the time verification for schedulable and current time as they are done in conf time zone
                        timeZone.changeToGMTTime(recurInsConf.timezone, ref confdate);
                        timeZone.changeToGMTTime(recurInsConf.timezone, ref setupDate);
                        timeZone.changeToGMTTime(recurInsConf.timezone, ref confenddate);
                        timeZone.changeToGMTTime(recurInsConf.timezone, ref tearDate);

                        recurInsConf.SetupTime = setupDate;
                        recurInsConf.TearDownTime = tearDate;
                        recurInsConf.confdate = confdate;
                        recurInsConf.conftime = confdate;

                        recurInsConf.confEnd = confdate.Add(new TimeSpan(0, 0, recurInsConf.duration, 0));

                        //Check for room conflicts if double booking is disabled
                        string conflictRooms = "";
                        if (orgInfo.doublebookingenabled == 0)
                        {
                            if (CheckForConflicts(ref roomList, ref splitRooms, ref conflictRooms, ref recurInsConf))
                            {
                                conflict = "1"; //Room Conflict <conflict>1</conflict>
                                conflictRooms = "(" + conflictRooms + ")";
                            }
                        }
                        RecOutXml.Append("<conflict>" + conflict + "</conflict>");
                        RecOutXml.Append("<conflictRooms>" + conflictRooms + "</conflictRooms>");
                        RecOutXml.Append("</dateTime>");
                    }
                }
                RecOutXml.Append("</dateList>");
                obj.outXml = RecOutXml.ToString();
            }
            catch (myVRMException me)
            {
                m_log.Error("SetConference-BuildConfList: ", me);
                obj.outXml = "";
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("SetConference-BuildConfList: ", e);
                obj.outXml = ""; //Error 200 will be thrown
                return false;
            }
            return true;
        }
        #endregion

        //FB 2426 Start

        #region InsertGuestRoom
        /// <summary>
        /// InsertGuestRoom
        /// </summary>
        /// <param name="confRoom"></param>
        /// <param name="addedGuestRooms"></param>
        /// <param name="vConf"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        private bool InsertGuestRoom(ref List<vrmRoom>guestRooms, ref List<vrmEndPoint> guestEndpoints, ref int errNo, ref List<vrmConfRoom> confRooms, ref vrmConference vConf)
        {
            bool bret = true;
            try
            {
                /* first create fly endpoints after checking license
                 * and fill ept id to guest rooms
                 * next create guest rooms
                 * Also create fly ept or fly room only if id is -1 otherwise update */

                vrmRoom userRoom = null;
                vrmEndPoint EndpointRoom = null;
                vrmConfRoom room = null;
                int roomid = 0;
                int eptid = 0;
                int loginuserid = 0;
                int count = 0;
                //int newRoom = 0;
                loginuserid = guestRooms[0].adminId;
                if (guestEndpoints != null)
                {
                    if (guestEndpoints.Count > 0)
                    {
                        //for (int ro = 0; ro < guestRooms.Count; ro++)
                        //{
                        //    if (guestRooms[ro].roomId == 0)
                        //    {
                        //        count += 1;
                        //    }
                        //}
                        //if (licensecheck(ref count, ref loginuserid, ref organizationID, ref errNo))
                        //{
                            for (int ccnt = 0; ccnt < guestEndpoints.Count; ccnt++)
                            {
                                eptid = 0;
                                roomid = 0;
                                if (ccnt > 0)
                                {
                                    if (guestEndpoints[ccnt - 1].name == guestEndpoints[ccnt].name)
                                        guestEndpoints[ccnt].endpointid = guestEndpoints[ccnt - 1].endpointid;
                                }
                                EndpointRoom = guestEndpoints[ccnt];
                                eptid = GuestEndpoint(ref vConf, ref EndpointRoom, ref loginuserid, ref count, ref errNo);
                                if (eptid <= 0 && errNo > 0)
                                {

                                    throw new Exception("Onfly Endpoint Not created");
                                    return false;
                                }

                                for (int r = 0; r < guestRooms.Count; r++)
                                {
                                    if (guestRooms[r].Name.Trim() == EndpointRoom.name.Trim())
                                    {
                                        userRoom = guestRooms[r];
                                        roomid = GuestRoom(ref vConf, ref eptid, ref userRoom, ref errNo);
                                        break;
                                    }
                                }
                                if (roomid <= 0)
                                {
                                    throw new Exception("Onfly Room Not Created");
                                    return false;
                                }

                                #region Adding Guest Rooms with normal rooms

                                if (confRooms == null)
                                    confRooms = new List<vrmConfRoom>();

                                room = new vrmConfRoom();
                                room.roomId = roomid;
                                if (room.roomId > 0 && EndpointRoom.isDefault == 1)
                                {
                                    bool isRoomExist = false;
                                    for (int i = 0; i < confRooms.Count; i++)
                                    {
                                        if (confRooms[i].roomId == room.roomId)
                                            isRoomExist = true;
                                    }

                                    if (!isRoomExist)
                                    {

                                        room.Room = m_vrmRoomDAO.GetByRoomId(room.roomId);
                                        room.endpointId = EndpointRoom.endpointid;
                                        room.profileId = EndpointRoom.profileId;
                                        room.connect2 = -1;
                                        room.Extroom = 1;

                                        confRooms.Add(room);

                                        if (vConf.conftype != vrmConfType.RooomOnly)
                                        {
                                            if (room.Room.endpointid <= 0)
                                            {
                                                errNo = 262;
                                                return false;
                                            }
                                        }
                                    }
                                }
                                #endregion
                            }
                        //}
                        //else
                        //{
                        //    throw new Exception("License Exceeds");
                        //    return false;
                        //}
                    }
                }
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                bret = false;
            }
            return bret;
        }
        #endregion

        #region GuestEndpoint
        /// <summary>
        /// GuestEndpoint
        /// </summary>
        /// <param name="Gept"></param>
        /// <returns></returns>
        public int GuestEndpoint(ref vrmConference vConf,ref vrmEndPoint Gept,ref int loginuserid,ref int count,ref int errNo)
        {
            int endpointid = 0;
            bool isEndpointExist = false;
            try
            {
                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(Gept.orgId);

                if (count <= orgInfo.GuestRoomPerUser)
                {

                    if (Gept.endpointid <= 0 && Gept.profileId == 1)
                    {
                        string uString = "SELECT MAX(endpointid) + 1 as endpointid FROM myVRM.DataLayer.vrmEndPoint";
                        IList list = m_vrmEpt.execQuery(uString);
                        if (list != null)
                        {
                            if (list[0] != null)
                            {
                                Int32.TryParse(list[0].ToString(), out endpointid);
                            }
                        }
                        Gept.endpointid = endpointid;
                        if (vConf.Secured == 1) //FB 2595
                            Gept.Secured = 1;
                    }
                    else
                    {

                        string usString = "SELECT count(*) FROM myVRM.DataLayer.vrmEndPoint where endpointId = " + Gept.endpointid.ToString() + " and profileId = " + Gept.profileId.ToString();
                        IList list1 = m_vrmEpt.execQuery(usString);
                        if (list1 != null)
                        {
                            if (list1.Count > 0)
                                if (list1[0].ToString() == "0")
                                {
                                    isEndpointExist = false;
                                }
                                else
                                {
                                    isEndpointExist = true;
                                }
                        }
                    }

                    if (isEndpointExist)
                        m_vrmEpt.Update(Gept);
                    else
                        m_vrmEpt.Save(Gept);

                    endpointid = Gept.endpointid;
                }
                else
                {
                    errNo = 616;
                    return 0;
                }
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                endpointid = 0;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                endpointid = 0;

            }
            return endpointid;
        }
        #endregion

        #region GuestRoom
        /// <summary>
        /// GuestRoom
        /// </summary>
        /// <param name="Gusr"></param>
        /// <returns></returns>
        public int GuestRoom(ref vrmConference vConf,ref int eID, ref vrmRoom guestRooms, ref int errNo)
        {
            int roomid = 0;
            try
            {
                if (guestRooms.roomId <= 0)
                {
                    if (eID > 0)
                    {
                        guestRooms.endpointid = eID;
                        m_vrmRoomDAO.Save(guestRooms);
                        roomid = guestRooms.roomId;
                    }
                }
                else
                {
                    //errNo = 256;
                    //return 0;
                    guestRooms.endpointid = eID;
                    m_vrmRoomDAO.Update(guestRooms);
                    roomid = guestRooms.roomId;
                }
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                roomid = 0;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                roomid = 0;

            }
            return roomid;
        }
        #endregion

        #region licensecheck
        /// <summary>
        /// licensecheck
        /// </summary>
        /// <param name="Gusr"></param>
        /// <returns></returns>
        public bool licensecheck(ref int count, ref int loginuserid, ref int orgid, ref int errNo)
        {
            int maximumvidLimit = 0;
            int maxorgguestLimit = 0;
            try
            {
                if (count > 0)
                {
                    string maxLimit = "SELECT count(*) FROM myVRM.DataLayer.vrmRoom vc where vc.Disabled = 0  and vc.VideoAvailable = 2 and vc.Disabled = 0 and vc.Extroom = 1 and  vc.orgId =" + orgInfo.OrgId.ToString() + " and AdminID = " + loginuserid.ToString();
                    IList vidresRec = m_vrmRoomDAO.execQuery(maxLimit);
                    if (vidresRec != null)
                    {
                        if (vidresRec.Count > 0)
                        {
                            if (vidresRec[0] != null)
                            {
                                if (vidresRec[0].ToString() != "")
                                    Int32.TryParse(vidresRec[0].ToString(), out maximumvidLimit);
                            }
                        }
                    }
                    if (orgInfo.GuestRoomPerUser < maximumvidLimit || count > (orgInfo.GuestRoomPerUser - maximumvidLimit))
                    {
                        errNo = 616;
                        return false;
                    }

                    string maxorgLimit = "SELECT count(*) FROM myVRM.DataLayer.vrmRoom vc where vc.Disabled = 0  and vc.VideoAvailable = 2 and vc.Disabled = 0 and vc.Extroom = 1 and  vc.orgId =" + orgInfo.OrgId.ToString();
                    IList orglimit = m_vrmRoomDAO.execQuery(maxorgLimit);
                    if (orglimit != null)
                    {
                        if (orglimit.Count > 0)
                        {
                            if (orglimit[0] != null)
                            {
                                if (orglimit[0].ToString() != "")
                                    Int32.TryParse(orglimit[0].ToString(), out maxorgguestLimit);
                            }
                        }
                    }
                    if (maxorgguestLimit >= orgInfo.GuestRoomLimit || count > (orgInfo.GuestRoomLimit - maxorgguestLimit))
                    {
                        errNo = 617;
                        return false;
                    }

                }
                else
                {
                    return true;
                }
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
            return true;
        }
        #endregion

        //FB 2426 End

        #endregion
        //FB 2027 SetConference - end

        //FB 2027 start GetOldConference
        #region GetOldConference
        /// <GetOldConference(notes)>
        /// selectType, ' 1/2, 1: conference , 2: template
        /// Notes:
        /// If confid = x ; It's either a single conference or a template 
        /// If confid = x,y : It's one instance (y) of a 
        /// recurring conference x
        /// </GetOldConference(notes)>
        public bool GetOldConference(ref vrmDataObject obj)
        {
            try
            {
                String recurEnabled = "1", selectType = "", confId = "", confstatus = "",internalVideonum="",externalVideonum="";//FB 2376
                StringBuilder outXml = new StringBuilder();
                StringBuilder tempXml = new StringBuilder();
                bool success = true;
                int userId = 11, timeDiff, mode = 1, utcEnabled = 0; //FB 2164
                vrmUser User, host,Assistant;//FB 2426
                vrmConference Conf = new vrmConference();
                vrmTemplate Template = new vrmTemplate();
                vrmEmailLanguage emailLang = new vrmEmailLanguage();
                List<ICriterion> ConfCritList = new List<ICriterion>();
                List<ICriterion> critList = new List<ICriterion>();
                vrmConf Conference;
                XmlDocument xd = new XmlDocument();
                DateTime confdate = DateTime.Now;
                DateTime today = DateTime.Now;
                timeZone.changeToGMTTime(sysSettings.TimeZone, ref today);
                StringBuilder confMsgList = new StringBuilder(); //FB 2486

                xd.LoadXml(obj.inXml);
                if (xd.SelectSingleNode("//login/userID") != null)
                    Int32.TryParse(xd.SelectSingleNode("//login/userID").InnerText.Trim(), out userId);

                if (xd.SelectSingleNode("//login/organizationID") != null)
                    Int32.TryParse(xd.SelectSingleNode("//login/organizationID").InnerText.Trim(), out organizationID);

                //FB 2274 Starts
                int mutliOrgID = 0;
                if (xd.SelectSingleNode("//login/multisiloOrganizationID") != null)
                    Int32.TryParse(xd.SelectSingleNode("//login/multisiloOrganizationID").InnerText.Trim(), out mutliOrgID);
                   
                if (mutliOrgID > 11)
                    organizationID = mutliOrgID;
                //FB 2274 Ends

                if (organizationID < defaultOrgId)
                    organizationID = defaultOrgId;

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                if (orgInfo != null)
                    recurEnabled = orgInfo.recurEnabled.ToString(); //Organization Module

                //FB 2558 WhyGo - Starts
                int iClient = 0;
                if (xd.SelectSingleNode("//Client") != null)
                {
                    if (xd.SelectSingleNode("//Client").InnerText != "")
                        Int32.TryParse(xd.SelectSingleNode("//Client").InnerXml.Trim(), out iClient);
                }
                //FB 2558 WhyGo - End

                if (xd.SelectSingleNode("//login/selectType") != null)
                    selectType = xd.SelectSingleNode("//login/selectType").InnerText.Trim();

                if (xd.SelectSingleNode("//login/selectID") != null)
                    confId = xd.SelectSingleNode("//login/selectID").InnerXml.Trim();

                if (xd.SelectSingleNode("//login/utcEnabled") != null)
                    Int32.TryParse(xd.SelectSingleNode("//login/utcEnabled").InnerText.Trim(), out utcEnabled); //FB 2164

                CConfID CConf = new CConfID(confId);
                Conf.confid = CConf.ID;
                Conf.instanceid = CConf.instance;
                User = m_vrmUserDAO.GetByUserId(userId);

                if (selectType.CompareTo("2") != 0)  //  selecttype = 2, template
                    selectType = "1";


                outXml.Append("<conference>");
                outXml.Append("<userInfo>");
                outXml.Append("<userEmail>" + User.Email + "</userEmail>");
                outXml.Append("<emailClient>" + User.EmailClient + "</emailClient>"); //' 0/1/2; 0: none, 1: Outlook, 2: Lotus
                outXml.Append("<recurEnabled>" + recurEnabled + "</recurEnabled>");
                outXml.Append("<Secured>" + User.Secured + "</Secured>");//FB 2595
                outXml.Append("</userInfo>");

                StringBuilder TempXml = new StringBuilder();
                if (!m_Search.GetDepartmentList(User, ref TempXml))
                {
                    m_log.Error("Error in Retrieving Department List");
                    obj.outXml = "";
                    return false;
                }
                outXml.Append(TempXml);
                outXml.Append("<confInfo>");

                if (selectType.CompareTo("1") == 0) //Conference Mode
                {
                    ConfCritList.Add(Expression.Eq("confid", Conf.confid));
                    if (CConf.RecurMode != 1)
                        ConfCritList.Add(Expression.Eq("instanceid", Conf.instanceid));

                    List<vrmConference> Conflist = m_vrmConfDAO.GetByCriteria(ConfCritList, true);
                    if (Conflist.Count > 0)
                        Conf = Conflist[0];
                    else
                    {
                        myVRMException vrmex = new myVRMException(242);
                        obj.outXml = vrmex.FetchErrorMsg();
                        return false;
                    }

                    confdate = Conf.confdate;
                    bool update = false;
                    DateTime endtime = DateTime.Now;
                    timeZone.changeToGMTTime(sysSettings.TimeZone, ref endtime);
                    timeZone.userPreferedTime(Conf.timezone, ref endtime);
                    endtime = endtime.AddMinutes(-Conf.duration);

                    if (utcEnabled == 0) //FB 2164
                        timeZone.userPreferedTime(Conf.timezone, ref confdate);

                    if (Conf.recuring == 1) //Update recurring instance status as past if any
                    {
                        Conf = null;
                        for (int i = 0; i < Conflist.Count; i++)
                        {
                            if ((Conflist[i].confEnd - today).TotalMinutes < 0 && Conflist[i].deleted == 0)
                            {
                                if (Conflist[i].status != 3 && Conflist[i].status != 6 && Conflist[i].status != 7)
                                {
                                    Conflist[i].status = 7;
                                    m_vrmConfDAO.Update(Conflist[i]);
                                }
                            }
                            else if (Conflist[i].deleted == 0)
                            {
                                if (Conf == null)
                                    Conf = Conflist[i];
                            }
                        }
                        if (Conf == null)
                            Conf = Conflist[0];
                    }
                    else
                    {
                        if (Conf.deleted == 1 && Conf.status != 9)
                        {
                            Conf.status = 9;
                            update = true;
                        }
                        else if (confdate < endtime)
                        {
                            if (Conf.status != 3 && Conf.status != 6 && Conf.status != 7)
                            {
                                Conf.status = 7;
                                update = true;
                            }
                            else
                            {
                                if (Conf.immediate == 1 && Conf.status != 7)
                                {
                                    Conf.status = 7;
                                    update = true;
                                }
                            }
                        }

                        if (update == true) //Update Single instance/normal conference status as past
                            m_vrmConfDAO.Update(Conf);
                    }

                    Conf = m_vrmConfDAO.GetByConfId(Conf.confid, Conf.instanceid);
                    Conference = Conf;

                    if (CConf.RecurMode == 1)
                        ConfCritList.Add(Expression.Eq("instanceid", Conf.instanceid));
                    else
                        Conf.recuring = 0; //IF open single instance of Rec

                }
                else
                {
                    Template = m_vrmTempDAO.GetById(Conf.confid);
                    Conference = Template;
                    Conf.timezone = User.TimeZone;
                    Conference.owner = Template.TMPOwner;
                    Conference.description = Template.confDescription;
                    if (Conf.orgId == 0) //FB 2376
                        Conf.orgId = Template.orgId;
                    if (Conf.lecturer == null)
                        Conf.lecturer = "";
                    Conf.StartMode = sysSettings.StartMode;//FB 2501
                }

                outXml.Append("<ConfOrgID>" + Conf.orgId + "</ConfOrgID>"); //FB 2274
                outXml.Append("<createBy>" + Conference.conftype + "</createBy>");
                outXml.Append ("<StartMode>"+Conf.StartMode+"</StartMode>"); //FB 2501
                outXml.Append("<deptID>" + Conf.ConfDeptID + "</deptID>");
                outXml.Append("<Secured>" + Conf.Secured+ "</Secured>"); //FB 2595
                outXml.Append("<levels></levels>"); //User'sLevels

                if (selectType.Trim() == "1")
                {
                    outXml.Append("<confID>" + Conf.confid.ToString());
                    if (CConf.RecurMode != 1)
                        outXml.Append("," + Conf.instanceid.ToString());

                    outXml.Append("</confID>");
                }
                else
                {
                    outXml.Append("<confID></confID>");
                    outXml.Append("<confType>" + Conference.conftype + "</confType>");
                }

                outXml.Append("<confName>" + Conference.externalname + "</confName>");
                if (selectType.Trim() == "1")
                    outXml.Append("<confOrigin>" + Conf.ConfOrigin + "</confOrigin>");
                else
                    outXml.Append("<confOrigin></confOrigin>");

                outXml.Append("<confPassword>" + Conference.password + "</confPassword>");
                outXml.Append("<bufferZone>");
                if (selectType.Trim() == "1")
                {
                    Int32.TryParse((Conf.SetupTime - Conf.confdate).TotalMinutes.ToString(), out timeDiff);
                    outXml.Append("<SetupDur>" + timeDiff + "</SetupDur>");
                    Int32.TryParse((Conference.duration - ((Conf.TearDownTime - Conf.confdate).TotalMinutes)).ToString(), out timeDiff);
                    outXml.Append("<TearDownDur>" + timeDiff + "</TearDownDur>");
                }
                else
                {
                    outXml.Append("<SetupDur></SetupDur>");
                    outXml.Append("<TearDownDur></TearDownDur>");
                }
                outXml.Append("</bufferZone>");

                host = m_vrmUserDAO.GetByUserId(Conference.owner);

                vrmUser vnocOperator = m_vrmUserDAO.GetByUserId(Conf.confVNOC); //FB 2501 VNOC

                vrmLanguage lang = m_ILanguageDAO.GetLanguageById(host.Language);
                if (host.EmailLangId > 100)
                    emailLang = m_IEmailLanguageDAO.GetById(host.EmailLangId);

                outXml.Append("<isVIP>" + Conf.isVIP + "</isVIP>");
                outXml.Append("<isVMR>" + Conf.isVMR + "</isVMR>");//FB 2376
                outXml.Append("<isDedicatedEngineer>" + Conf.isDedicatedEngineer + "</isDedicatedEngineer>");
                outXml.Append("<isLiveAssitant>" + Conf.isLiveAssistant + "</isLiveAssitant>");
                outXml.Append("<isReminder>" + Conf.isReminder + "</isReminder>");
                outXml.Append("<hostName>" + host.FirstName + " " + host.LastName + "</hostName>");
                outXml.Append("<hostId>" + host.userid + "</hostId>");
                outXml.Append("<hostEmail>" + host.Email + "</hostEmail>");
                outXml.Append("<hostLogin>" + host.Login + "</hostLogin>");
                outXml.Append("<hostTimeZone>" + host.TimeZone + "</hostTimeZone>");
                outXml.Append("<hostLang>" + lang.Name + "</hostLang>");
                outXml.Append("<hostEmailLang>" + emailLang.EmailLanguage + "</hostEmailLang>");
                outXml.Append("<hostMailBlocked>" + host.MailBlocked + "</hostMailBlocked>");
                outXml.Append("<hostWork>" + host.WorkPhone + "</hostWork>");
                outXml.Append("<hostCell>" + host.CellPhone + "</hostCell>");
                //FB 2632 Starts
                outXml.Append("<ConciergeSupport>");
                outXml.Append("<OnSiteAVSupport>"+Conf.OnSiteAVSupport+"</OnSiteAVSupport>");
                outXml.Append("<MeetandGreet>"+Conf.MeetandGreet+"</MeetandGreet>");
                outXml.Append("<ConciergeMonitoring>"+Conf.ConciergeMonitoring+"</ConciergeMonitoring>");
                outXml.Append("<DedicatedVNOCOperator>"+Conf.DedicatedVNOCOperator+"</DedicatedVNOCOperator>");
                //FB 2501 VNOC start
                if (vnocOperator != null)
                {
                    outXml.Append("<VNOCOperatorID>" + Conf.confVNOC + "</VNOCOperatorID>");
                    outXml.Append("<VNOCOperator>" + vnocOperator.FirstName + " " + vnocOperator.LastName + "</VNOCOperator>");
                }
                else
                {
                    outXml.Append("<VNOCOperatorID></VNOCOperatorID>");
                    outXml.Append("<VNOCOperator></VNOCOperator>");
                }
                //FB 2501 VNOC end
                outXml.Append("</ConciergeSupport>");
                //FB 2632 Ends
                

                if (selectType.Trim() == "1")
                {
                    host = m_vrmUserDAO.GetByUserId(Conf.userid);
                    outXml.Append("<lastModifiedById>" + Conf.userid + "</lastModifiedById>");
                    outXml.Append("<lastModifiedByName>" + host.FirstName + " " + host.LastName + "</lastModifiedByName>");
                    outXml.Append("<lastModifiedByEmail>" + host.Email + "</lastModifiedByEmail>");//FB 2501
                    outXml.Append("<immediate>" + Conf.immediate.ToString() + "</immediate>");
                }
                else
                {
                    outXml.Append("<lastModifiedById>" + Conference.owner + "</lastModifiedById>");
                    outXml.Append("<lastModifiedByName>" + host.FirstName + " " + host.LastName + "</lastModifiedByName>");
                    outXml.Append("<lastModifiedByEmail>" + host.Email + "</lastModifiedByEmail>");//FB 2501
                    outXml.Append("<immediate></immediate>");
                }

                if (selectType.Trim() == "1")
                {
                    outXml.Append("<recurring>" + Conf.recuring.ToString() + "</recurring>");
                    if (Conf.recuring == 1 && CConf.RecurMode == 1)
                        FetchRecurInfo(Conf, ref outXml, utcEnabled);// Whole recurring conference //FB 2164
                    else
                    {
                        // Single recurring instance or single conference
                        outXml.Append("<startDate>" + confdate.ToString("d") + "</startDate>");
                        outXml.Append("<startHour>" + confdate.ToString("hh") + "</startHour>");
                        outXml.Append("<startMin>" + confdate.ToString("mm") + "</startMin>");
                        outXml.Append("<startSet>" + confdate.ToString("tt") + "</startSet>");
                    }

                    FetchRecurInfoDefunct(Conf, ref outXml, utcEnabled); //FB 2218 - Instance details
                }
                outXml.Append("<timeZone>" + Conf.timezone + "</timeZone>");

                if (Conf.recuring != 1 || CConf.RecurMode != 1)
                    outXml.Append("<durationMin>" + Conference.duration + "</durationMin>");

                tempXml = new StringBuilder();
                OrganizationFactory m_OrgFactory = new OrganizationFactory(m_configPath, m_log);
                m_OrgFactory.organizationID = organizationID;

                if (m_OrgFactory.timeZonesToXML(ref tempXml))
                    outXml.Append(tempXml);
                else
                {
                    m_log.Error("Error in Retrieving interface List");
                    obj.outXml = "";
                    return false;
                }
                outXml.Append("<description>" + m_UtilFactory.ReplaceOutXMLSpecialCharacters(Conference.description) + "</description>"); //FB 2236

                ConfRooms = new List<int>(); //FB 2594
                vrmAdvAvParams AvParams = new vrmAdvAvParams(); 
                if (selectType.CompareTo("1") == 0)
                {

                    outXml.Append("<locationList>");
                    outXml.Append("<selected>");
                    for (int i = 0; i < Conf.ConfRoom.Count; i++)
                    {
                        if (Conf.ConfRoom[i].disabled == 0)
                        {
                            if (Conf.ConfRoom[i].Extroom == 0) //FB 2426
                            {
                                outXml.Append("<level1ID>" + Conf.ConfRoom[i].roomId + "</level1ID>");
                                
                                if (Conf.ConfRoom[i].roomId != null) //FB 2645
                                {
                                    if (!ConfRooms.Contains(Conf.ConfRoom[i].roomId)) //FB 2594
                                        ConfRooms.Add(Conf.ConfRoom[i].roomId);
                                }
                            }
                        }
                    }
                    outXml.Append("</selected>");

                    AvParams = Conf.ConfAdvAvParams;
                }
                else
                {
                    critList = new List<ICriterion>();
                    critList.Add(Expression.Eq("tmpId", Template.tmpId));

                    List<vrmTempRoom> TempRooms = m_TempDAO.GetByCriteria(critList);

                    outXml.Append("<locationList>");
                    outXml.Append("<selected>");

                    for (int i = 0; i < TempRooms.Count; i++)
                    {
                        outXml.Append("<level1ID>" + TempRooms[i].RoomID + "</level1ID>");

                        if (TempRooms[i].RoomID != null) //FB 2645
                        {
                            if (!ConfRooms.Contains(TempRooms[i].RoomID)) //FB 2594
                                ConfRooms.Add(TempRooms[i].RoomID);
                        }
                    }

                    outXml.Append("</selected>");

                    List<vrmTempAdvAvParams> tempAdvAv = m_TempAdvAvParamsDAO.GetByCriteria(critList);
                    if (tempAdvAv.Count > 0)
                        AvParams = (vrmAdvAvParams)tempAdvAv[0];
                }

                tempXml = new StringBuilder();
                m_Search.organizationID = Conf.orgId; //FB 2274

                if (ConfRooms.Count > 0) // FB 2594                  
                {
                    success = m_Search.GetConfRoomListOptimized(userId.ToString(), ConfRooms, ref tempXml);  //FB 2594 
                    if (!success)
                    {
                        m_log.Error("Error in Retrieving Room List");
                        obj.outXml = "";
                        return false;
                    }
                    else
                        outXml.Append(tempXml);
                }
                
                outXml.Append("</locationList>");
                //FB 2426 Start
                outXml.Append("<GuestLocationList>");
                outXml.Append("<ConfGuestRooms>");
                for (int i = 0; i < Conf.ConfRoom.Count; i++)
                {
                    if (Conf.ConfRoom[i].Extroom == 1 && Conf.ConfRoom[i].Room.disabled == 0)
                    {

                        outXml.Append("<ConfGuestRoom>");
                        outXml.Append("<LoginUserID>" + Conf.loginUser.ToString() + "</LoginUserID>"); //FB 2544
                        outXml.Append("<RoomID>" + Conf.ConfRoom[i].roomId.ToString() + "</RoomID>");
                        outXml.Append("<GuestRoomName>" + Conf.ConfRoom[i].Room.Name + "</GuestRoomName>");
                        Assistant = m_vrmUserDAO.GetByUserId(Conf.ConfRoom[i].Room.assistant);
                        outXml.Append("<ContactName>" + Assistant.FirstName + " " + Assistant.LastName + "</ContactName>");
                        outXml.Append("<ContactEmail>" + Assistant.Email + "</ContactEmail>");
                        outXml.Append("<ContactPhoneNo>" + Conf.ConfRoom[i].Room.RoomPhone.ToString() + "</ContactPhoneNo>");
                        outXml.Append("<RoomAddress>" + Conf.ConfRoom[i].Room.Address1 + "</RoomAddress>");
                        outXml.Append("<State>" + Conf.ConfRoom[i].Room.State.ToString() + "</State>");
                        outXml.Append("<City>" + Conf.ConfRoom[i].Room.City + "</City>");
                        outXml.Append("<ZipCode>" + Conf.ConfRoom[i].Room.Zipcode + "</ZipCode>");
                        outXml.Append("<Country>" + Conf.ConfRoom[i].Room.Country.ToString() + "</Country>");
                        outXml.Append("<Tier1Name>" + Conf.ConfRoom[i].Room.tier2.Name + "</Tier1Name>");
                        outXml.Append("<Tier2Name>" + Conf.ConfRoom[i].Room.tier2.tier3.Name + "</Tier2Name>");
                        outXml.Append("<Profiles>");
                        if (Conf.ConfRoom[i].endpointId > 0)
                        {
                            List<ICriterion> criterionList = new List<ICriterion>();
                            criterionList.Add(Expression.Eq("endpointid", Conf.ConfRoom[i].endpointId));
                            List<vrmEndPoint> guestEndpoints = m_vrmEpt.GetByCriteria(criterionList);

                            for (int e = 0; e < guestEndpoints.Count; e++)
                            {
                                outXml.Append("<Profile>");
                                outXml.Append("<ProfileName>" + guestEndpoints[e].profileName + "</ProfileName>");
                                outXml.Append("<Password>" + guestEndpoints[e].password + "</Password>");
                                outXml.Append("<confirmPassword>" + guestEndpoints[e].password + "</confirmPassword>");
                                outXml.Append("<isDefault>" + guestEndpoints[e].isDefault + "</isDefault>");

                                if (guestEndpoints[e].profileId == Conf.ConfRoom[i].profileId)
                                {
                                    outXml.Append("<AddressType>" + Conf.ConfRoom[i].addressType + "</AddressType>");
                                    outXml.Append("<Address>" + Conf.ConfRoom[i].ipisdnaddress + "</Address>");
                                    outXml.Append("<MaxLineRate>" + Conf.ConfRoom[i].defLineRate + "</MaxLineRate>");
                                    outXml.Append("<ConnectionType>" + Conf.ConfRoom[i].connectionType + "</ConnectionType>");
                                }
                                else
                                {
                                    outXml.Append("<AddressType>" + guestEndpoints[e].addresstype + "</AddressType>");
                                    outXml.Append("<Address>" + guestEndpoints[e].address + "</Address>");
                                    outXml.Append("<MaxLineRate>" + guestEndpoints[e].linerateid + "</MaxLineRate>");
                                    outXml.Append("<ConnectionType>" + guestEndpoints[e].connectiontype + "</ConnectionType>");
                                }
                                outXml.Append("</Profile>");
                            }
                        }
                        outXml.Append("</Profiles>");
                        outXml.Append("</ConfGuestRoom>");
                    }
                }
                outXml.Append("</ConfGuestRooms>");
                outXml.Append("</GuestLocationList>");
                //FB 2426 End
                //obj.outXml += common.FetchSplitLocations(confID,instanceid);

                tempXml = new StringBuilder();
                if (!m_Search.getAddressType(ref tempXml))
                {
                    m_log.Error("Error in Retrieving Address Type List");
                    obj.outXml = "";
                    return false;
                }
                if (!m_Search.GetAudioUsage(0, ref tempXml))
                {
                    m_log.Error("Error in Retrieving Media Type List");
                    obj.outXml = "";
                    return false;
                }
                if (selectType == "2") //FB 2376
                {
                    internalVideonum = host.InternalVideoNumber;
                    externalVideonum = host.ExternalVideoNumber;
                }
                else
                {
                    internalVideonum = AvParams.internalBridge;
                    externalVideonum = AvParams.externalBridge;
                }

                outXml.Append(tempXml);

                outXml.Append("<advAVParam>");
                outXml.Append("<maxAudioPart>" + AvParams.maxAudioParticipants + "</maxAudioPart>");
                outXml.Append("<maxVideoPart>" + AvParams.maxVideoParticipants + "</maxVideoPart>");
                outXml.Append("<restrictProtocol>" + AvParams.videoProtocolID + "</restrictProtocol>");
                outXml.Append("<restrictAV>" + AvParams.mediaID + "</restrictAV>");
                outXml.Append("<videoLayout>" + AvParams.videoLayoutID + "</videoLayout>");
                outXml.Append("<maxLineRateID>" + AvParams.linerateID + "</maxLineRateID>");
                outXml.Append("<audioCodec>" + AvParams.audioAlgorithmID + "</audioCodec>");
                outXml.Append("<videoCodec>" + AvParams.videoSession + "</videoCodec>");
                outXml.Append("<dualStream>" + AvParams.dualStreamModeID + "</dualStream>");
                outXml.Append("<confOnPort>" + AvParams.conferenceOnPort + "</confOnPort>");
                outXml.Append("<encryption>" + AvParams.encryption + "</encryption>");
                outXml.Append("<lectureMode>" + AvParams.lectureMode + "</lectureMode>");
                outXml.Append("<VideoMode>" + AvParams.videoMode + "</VideoMode>");
                outXml.Append("<SingleDialin>" + AvParams.singleDialin + "</SingleDialin>");
                outXml.Append("<internalBridge>" + internalVideonum + "</internalBridge>");//FB 2376
                outXml.Append("<externalBridge>" + externalVideonum + "</externalBridge>");//FB 2376
                //outXml.Append("<VNOCOperator>" + AvParams.confVNOC + "</VNOCOperator>");//FB 2501
                outXml.Append("<FECCMode>" + AvParams.feccMode + "</FECCMode>"); // FB 2501 FECC
                outXml.Append("<lockStatus>" + AvParams.ConfLockUnlock + "</lockStatus>"); // FB 2501 Dec7
                outXml.Append("<PolycomTemplate>" + AvParams.PolycomTemplate + "</PolycomTemplate>"); // FB 2441
                outXml.Append("<PolycomSendEmail>" + AvParams.PolycomSendEmail + "</PolycomSendEmail>"); // FB 2441
                outXml.Append("</advAVParam>");
                //outXml.Append("<ConceirgeSupport>" + Conf.ConceirgeSupport + "</ConceirgeSupport>");//FB 2341//FB 2377

                m_vrmFactor = new vrmFactory(ref obj);
                //m_vrmFactor.organizationID = organizationID; //FB 2045
                m_vrmFactor.organizationID = Conf.orgId; //FB 2274
                m_vrmFactor.FetchCustomAttrs(User, Conf, ref outXml);
                int servicetype = Conf.ServiceType;//FB 2219
                if (servicetype == 0)
                    servicetype = -1;

                outXml.Append("<publicConf>" + Conference.isPublic.ToString() + "</publicConf>");
                outXml.Append("<ServiceType>" + servicetype + "</ServiceType>");//FB 2219
                outXml.Append("<transcoding>" + Conf.transcoding.ToString() + "</transcoding>");
                outXml.Append("<continuous>" + Conf.continous.ToString() + "</continuous>");
                outXml.Append("<videoLayout>" + Conf.videolayout.ToString() + "</videoLayout>");
                outXml.Append("<videoSessionID>" + AvParams.videoSession.ToString() + "</videoSessionID>");
                outXml.Append("<manualVideoLayout>" + Conf.manualvideolayout.ToString() + "</manualVideoLayout>");
                outXml.Append("<lectureMode>" + Conference.lecturemode.ToString() + "</lectureMode>");

                if (selectType.CompareTo("1") == 0)
                {
                    DateTime fromDate = today;
                    DateTime toDate = today;
                    fromDate = fromDate.AddMinutes(5);
                    toDate = toDate.AddMinutes(Conf.duration);
                    confstatus = Conf.status.ToString();
                    if (Conf.confdate <= fromDate && today <= toDate && Conf.status == 0)
                        confstatus = vrmConfStatus.Ongoing.ToString(); // this confernece is ongoing immediate conference
                }

                outXml.Append("<Status>" + confstatus + "</Status>");
                outXml.Append("<lecturer>" + Conference.lecturer + "</lecturer>");
                outXml.Append("<lineRateID>" + AvParams.linerateID.ToString() + "</lineRateID>");
                outXml.Append("<audioAlgorithmID>" + AvParams.audioAlgorithmID.ToString() + "</audioAlgorithmID>");
                outXml.Append("<videoProtocolID>" + AvParams.videoProtocolID.ToString() + "</videoProtocolID>");

                tempXml = new StringBuilder();
                if (!m_Search.GetVideoSessionList(Conf.videosession, ref tempXml))
                {
                    m_log.Error("Error in Retrieving video session List");
                    obj.outXml = "";
                    return false;
                }
                if (!m_Search.GetLineRateList(Conf.linerate, ref tempXml))
                {
                    m_log.Error("Error in Retrieving line rate List");
                    obj.outXml = "";
                    return false;
                }
                if (!m_Search.GetVideoEquipment(0, ref tempXml))
                {
                    m_log.Error("Error in Retrieving video equipment List");
                    obj.outXml = "";
                    return false;
                }
                if (!m_Search.GetAudioList(Conf.audio, ref tempXml))
                {
                    m_log.Error("Error in Retrieving audio List");
                    obj.outXml = "";
                    return false;
                }
                if (!m_Search.ConfVideoProList(Conf.videoprotocol, ref tempXml))
                {
                    m_log.Error("Error in Retrieving interface List");
                    obj.outXml = "";
                    return false;
                }

                vrmUserRoles usrRole = m_IUserRolesDao.GetById(User.roleID); //crossaccess
                critList = new List<ICriterion>();
                if (mode == 0) //If User is General User
                {
                    critList.Add(Expression.Or(Expression.Eq("Private", 0),
                        Expression.Eq("Owner", User.userid)));
                }
                critList.Add(Expression.Eq("orgId", Conf.orgId));  //If User is Site Admin //FB 2274

                if (orgInfo.MultipleDepartments != 1)
                {
                    if (!User.isSuperAdmin())
                    {
                        critList.Add(Expression.Or(Expression.Eq("Private", 0), Expression.Eq("Owner", User.userid)));
                    }
                }
                else
                {
                    if (!(User.isSuperAdmin() && usrRole.crossaccess == 1))
                    {
                        if (User.isSuperAdmin() && usrRole.crossaccess == 0) //If User is OrgAdmin
                        {
                            ArrayList userArrLst = new ArrayList();
                            List<ICriterion> criterionList1 = new List<ICriterion>();
                            vrmUser superuser = m_vrmUserDAO.GetByUserId(11);
                            criterionList1.Add(Expression.Eq("Admin", 2));
                            criterionList1.Add(Expression.Eq("roleID", superuser.roleID));
                            List<vrmUser> userList = m_vrmUserDAO.GetByCriteria(criterionList1);
                            for (int i = 0; i < userList.Count; i++)
                            {
                                userArrLst.Add(userList[i].userid);
                            }
                            critList.Add(Expression.Or(Expression.Not(Expression.In("Owner", userArrLst)), Expression.Eq("Private", 0)));
                        }
                        else
                        {
                            critList.Add(Expression.Or(Expression.Eq("Private", 0), Expression.Eq("Owner", User.userid))); //If its Gereral Users
                        }
                    }
                }

                outXml.Append(tempXml);
                outXml.Append("<defaultAGroups></defaultAGroups>"); //confgroup and Tempgroup table is not using
                outXml.Append("<defaultCGroups></defaultCGroups>");
                outXml.Append("<groups>");
                m_usrFactory.FetchGroups(critList, ref outXml, 0, "group");
                outXml.Append("</groups>");

                if (selectType.CompareTo("1") == 0)
                {
                    m_Search.GetPartyList(Conf, ref outXml);
                    mode = 1;
                }
                else
                {
                    m_Search.GetPartyList(Template, ref outXml);
                    mode = 0;
                }

                if (Conf.confnumname > 0)
                    outXml.Append("<confUniqueID>" + Conf.confnumname.ToString() + "</confUniqueID>");
                else
                    outXml.Append("<confUniqueID></confUniqueID>");

                outXml.Append("<dynamicInvite>" + Conference.dynamicinvite + "</dynamicInvite>");

                ConfAttachments(ConfCritList, ref outXml, mode); //Files Upload

                //FB 2486 Starts
                List<ICriterion> criterionConf = new List<ICriterion>();
                if(mode ==1)
                {
                    criterionConf.Add(Expression.Eq("confid", Conf.confid));
                    if (Conf.recuring == 0)
                        criterionConf.Add(Expression.Eq("instanceid", Conf.instanceid));
                    
                    criterionConf.Add(Expression.Eq("orgid", Conf.orgId));
                    ConfMessages(criterionConf, ref outXml, mode);
                }
                else if (mode == 0)
                {
                    FetchConfMessages(null, ref confMsgList);
                    outXml.Append(confMsgList.ToString());
                }
                
                //FB 2486 Ends

                outXml.Append("</confInfo>");
                if (selectType.Trim() == "1")
                {
                    if (CConf.instance > 0 && Conf.recuring == 1)
                        outXml.Append("<isinstanceedit>1</isinstanceedit>");
                    else
                        outXml.Append("<isinstanceedit>0</isinstanceedit>");
                }
                outXml.Append("</conference>");
                obj.outXml = outXml.ToString();
            }
            catch (myVRMException e)
            {
                obj.outXml = "";
                m_log.Error("vrmException", e);
                return false;
            }
            catch (Exception e)
            {
                obj.outXml = "";
                m_log.Error("sytemException", e);
                return false;
            }
            return true;
        }
        #endregion

        #region FetchRecurInfo
        /// <summary>
        /// FetchRecurInfo
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="outputXML"></param>
        /// <returns></returns>
        public void FetchRecurInfo(vrmConference conf, ref StringBuilder outputXML, int utcEnabled) //FB 2164
        {
            List<vrmConference> confsList = new List<vrmConference>();
            DateTime startdate = DateTime.Now;
            DateTime today = DateTime.Now;
            StringBuilder instanceXML = new StringBuilder(); //FB 2218 - Instance details
            try
            {
                List<ICriterion> criterionlst = new List<ICriterion>();
                criterionlst.Add(Expression.Eq("confid", conf.confid));
                List<vrmRecurInfo> recurPattern = m_ConfRecurDAO.GetByCriteria(criterionlst, true);
                if (recurPattern != null)
                {
                    if (recurPattern.Count > 0)
                        conf.ConfRecurInfo = recurPattern[0];
                }
                vrmRecurInfo Ri = conf.ConfRecurInfo;
                if (Ri.recurType != 5) // Not custom type
                {
                    outputXML.Append("<appointmentTime>");
                    outputXML.Append("<timeZone>" + Ri.timezoneid.ToString() + "</timeZone>");
                    
                    startdate = Ri.startTime;
                    if (utcEnabled == 0) //FB 2164
                        timeZone.userPreferedTime(Ri.timezoneid, ref startdate);

                    outputXML.Append("<startHour>" + startdate.ToString("hh") + "</startHour>");
                    outputXML.Append("<startMin>" + startdate.ToString("mm") + "</startMin>");
                    outputXML.Append("<startSet>" + startdate.ToString("tt") + "</startSet>");
                    outputXML.Append("<durationMin>" + conf.duration.ToString() + "</durationMin>");
                    outputXML.Append("</appointmentTime>");
                }
                else
                {
                    outputXML.Append("<appointmentTime>");
                    outputXML.Append("<timeZone>" + conf.timezone.ToString() + "</timeZone>");

                    //FB 2164 start
                    startdate = conf.confdate;
                    if (utcEnabled == 0)
                        timeZone.userPreferedTime(conf.timezone, ref startdate);

                    outputXML.Append("<startHour>" + startdate.ToString("hh") + "</startHour>");
                    outputXML.Append("<startMin>" + startdate.ToString("mm") + "</startMin>");
                    outputXML.Append("<startSet>" + startdate.ToString("tt") + "</startSet>");
                    //FB 2164 End
                    outputXML.Append("<durationMin>" + conf.duration.ToString() + "</durationMin>");
                    outputXML.Append("</appointmentTime>");
                }
                outputXML.Append("<recurrencePattern>");
                outputXML.Append("<recurType>" + Ri.recurType.ToString()
                    + "</recurType>");  //' 1: Daily, 2: Weekly, 3: Monthly, 4: Yearly

                switch (Ri.recurType)
                {
                    case 1:
                        outputXML.Append("<dailyType>" + Ri.subType.ToString()
                            + "</dailyType>"); //		' 1: every ? day(s),	2: every weekday (have no <dayGap>)
                        outputXML.Append("<dayGap>" + Ri.gap.ToString()
                            + "</dayGap>");
                        break;
                    case 2:
                        outputXML.Append("<weekGap>" + Ri.gap.ToString()
                            + "</weekGap>");   //		' means "every 3 week(s)";
                        outputXML.Append("<weekDay>" + Ri.days.ToString()
                            + "</weekDay>");
                        break;
                    case 3:
                        outputXML.Append("<monthlyType>" + Ri.subType.ToString()
                            + "</monthlyType>");
                        switch (Ri.subType)
                        {
                            case 1:
                                outputXML.Append("<monthDayNo>" + Ri.dayno.ToString()
                                    + "</monthDayNo>");
                                outputXML.Append("<monthGap>" + Ri.gap.ToString()
                                    + "</monthGap>");
                                break;
                            case 2:
                                outputXML.Append("<monthWeekDayNo>" + Ri.dayno.ToString()
                                    + "</monthWeekDayNo>");  //	' 1: first, 2: second, 3: third, 4: forth, 5: last
                                outputXML.Append("<monthWeekDay>" + Ri.days.ToString()
                                    + "</monthWeekDay>");	//	' 1: day, 2: weekday, 3: weekend, 4: Su, 5: Mo, 6: Tu, 7: We, 8: Th, 9: Fr,  8: Sa
                                outputXML.Append("<monthGap>" + Ri.gap.ToString()
                                    + "</monthGap>");
                                break;
                        }
                        break;
                    case 4:

                        outputXML.Append("<yearlyType>" + Ri.subType.ToString()
                            + "</yearlyType>");  //' 1: expressed in day #, 2: expressed in weekday#
                        switch (Ri.subType)
                        {
                            case 1:
                                outputXML.Append("<yearMonth>" + Ri.yearMonth.ToString()
                                    + "</yearMonth>");  //		' 1: Jan, 2: Feb, ..., 12: Dec
                                outputXML.Append("<yearMonthDay>" + Ri.days.ToString()
                                    + "</yearMonthDay>");
                                break;
                            case 2:
                                outputXML.Append("<yearMonthWeekDayNo>" + Ri.dayno.ToString()
                                    + "</yearMonthWeekDayNo>"); //	' 1: first, 2: second, 3: third, 4: forth, 5: last
                                outputXML.Append("<yearMonthWeekDay>" + Ri.days.ToString()
                                    + "</yearMonthWeekDay>");  //' 1: day, 2: weekday, 3: weekend, 4: Su, 5: Mo, 6: Tu, 7: We, 8: Th, 9: Fr,  8: Sa
                                outputXML.Append("<yearMonth>" + Ri.yearMonth.ToString()
                                    + "</yearMonth>");
                                break;
                        }
                        break;

                    case 5:
                        // Custom mode. Fetch all instances of this conference
                        confsList = m_vrmConfDAO.GetByConfId(conf.confid);
                        outputXML.Append("<startDates>");
                        //FB 2218 - Instance details ... start
                        instanceXML.Append("<custominstances>");
                        instanceXML.Append("<timeZone>" + conf.timezone + "</timeZone>");
                        //FB 2218 - Instance details ... end
                        for (int i = 0; i < confsList.Count; i++)
                        {
                            if (confsList[i].deleted == 0)
                            {
                                DateTime dt1 = confsList[i].confdate;
                                if (utcEnabled == 0) //FB 2164 start
                                    timeZone.userPreferedTime(confsList[i].timezone, ref dt1);

                                outputXML.Append("<startDate>" + dt1.ToString("d") + "</startDate>");
                            }
                            //FB 2218 - Instance details ... start
                            DateTime dt = confsList[i].confdate;
                            if (utcEnabled == 0) //FB 2164 start
                                timeZone.userPreferedTime(confsList[i].timezone, ref dt);
                            instanceXML.Append("<instance>");
                            instanceXML.Append("<startDate>" + dt.ToString("d") + "</startDate>");
                            instanceXML.Append("<startHour>" + dt.ToString("hh") + "</startHour>");
                            instanceXML.Append("<startMin>" + dt.ToString("mm") + "</startMin>");
                            instanceXML.Append("<startSet>" + dt.ToString("tt") + "</startSet>");
                            instanceXML.Append("<durationMin>" + confsList[i].duration + "</durationMin>");
                            instanceXML.Append("<deleted>" + confsList[i].deleted + "</deleted>");
                            instanceXML.Append("</instance>");
                            //FB 2218 - Instance details ... end
                        }
                        outputXML.Append("</startDates>");
                        instanceXML.Append("</custominstances>"); //FB 2218 - Instance details
                        break;
                }
                outputXML.Append("</recurrencePattern>");


                //FB 2218 - Instance details ... start
                if (Ri.recurType == 5)
                {
                    outputXML.Append(instanceXML.ToString());
                }
                else //FB 2218 - Instance details ... end
                {
                    outputXML.Append("<recurrenceRange>");
                    outputXML.Append("<startDate>" + startdate.ToString("d") + "</startDate>");
                    outputXML.Append("<endType>" + Ri.endType.ToString() + "</endType>");		//' 1: no end date, 2: end after <occurrence> times, 3: end by <endDate>
                    switch (Ri.endType)
                    {
                        case 1:
                            break;
                        case 2:
                            outputXML.Append("<occurrence>" + Ri.occurrence.ToString() + "</occurrence>");
                            break;
                        case 3:
                            outputXML.Append("<endDate>" + Ri.endTime.ToString("d") + "</endDate>");
                            break;
                    }
                    outputXML.Append("</recurrenceRange>");
                }
            }
            catch (Exception e)
            {
                m_log.Error("FetchRecurInfo ( Error in Retriving Recur info ):", e);
                outputXML.Append("");
                throw e;
            }
        }
        #endregion

        //Command added FB 2218
 
        #region FetchRecurInfoDefunct
        /// <summary>
        /// FetchRecurInfoDefunct
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="outputXML"></param>
        /// <returns></returns>
        public void FetchRecurInfoDefunct(vrmConference conf, ref StringBuilder outputXML, int utcEnabled) //FB 2164
        {
            List<vrmConference> confsList = new List<vrmConference>();
            DateTime startdate = DateTime.Now;
            DateTime today = DateTime.Now;
            List<sRecurrDates> recurDates = new List<sRecurrDates>();
            try
            {
                List<ICriterion> criterionlst = new List<ICriterion>();
                criterionlst.Add(Expression.Eq("confid", conf.confid));
                List<vrmRecurInfoDefunct> recurDefunctPattern = m_confDAO.GetConfRecurDefunctDAO().GetByCriteria(criterionlst, true);//FB 2218
                if (recurDefunctPattern != null)
                {
                    if (recurDefunctPattern.Count > 0)
                    {
                        RecurrenceFactory recFactory = new RecurrenceFactory();
                        recFactory.maxRecurInstance = vrmConfig.ConfReccurance; //Default instance limit

                        conf.ConfRecurInfoDefunct = recurDefunctPattern[0];

                        vrmRecurInfoDefunct Ri = conf.ConfRecurInfoDefunct;
                        outputXML.Append("<defunctPattern>");
                        if (Ri.recurType < 5) //Regular pattern
                        {
                            outputXML.Append("<appointmentTime>");
                            outputXML.Append("<timeZone>" + Ri.timezoneid.ToString() + "</timeZone>");

                            startdate = Ri.startTime;
                            if (utcEnabled == 0) //FB 2164
                                timeZone.userPreferedTime(Ri.timezoneid, ref startdate);

                            outputXML.Append("<startHour>" + startdate.ToString("hh") + "</startHour>");
                            outputXML.Append("<startMin>" + startdate.ToString("mm") + "</startMin>");
                            outputXML.Append("<startSet>" + startdate.ToString("tt") + "</startSet>");
                            outputXML.Append("<durationMin>" + conf.duration.ToString() + "</durationMin>");
                            outputXML.Append("<setupDuration>"+ Ri.SetupDuration +"</setupDuration>");
                            outputXML.Append("<teardownDuration>"+ Ri.TeardownDuration+ "</teardownDuration>");
                            outputXML.Append("</appointmentTime>");
                        }
                        else
                        {
                            /*
                            outputXML.Append("<appointmentTime>");
                            outputXML.Append("<timeZone>" + conf.timezone.ToString() + "</timeZone>");

                            //FB 2164 start
                            startdate = conf.confdate;
                            if (utcEnabled == 0)
                                timeZone.userPreferedTime(conf.timezone, ref startdate);

                            outputXML.Append("<startHour>" + startdate.ToString("hh") + "</startHour>");
                            outputXML.Append("<startMin>" + startdate.ToString("mm") + "</startMin>");
                            outputXML.Append("<startSet>" + startdate.ToString("tt") + "</startSet>");
                            //FB 2164 End
                            outputXML.Append("<durationMin>" + conf.duration.ToString() + "</durationMin>");
                            outputXML.Append("<setupDuration>" + Ri.SetupDuration + "</setupDuration>");
                            outputXML.Append("<teardownDuration>" + Ri.TeardownDuration + "</teardownDuration>");
                            outputXML.Append("</appointmentTime>");
                           */
                        }
                        outputXML.Append("<recurrencePattern>");
                        outputXML.Append("<recurType>" + Ri.recurType.ToString()
                            + "</recurType>");  //' 1: Daily, 2: Weekly, 3: Monthly, 4: Yearly

                        vrmRecurInfo recurInfo = new vrmRecurInfo();

                        startdate = Ri.startTime;
                        if (utcEnabled == 0) //FB 2164
                            timeZone.userPreferedTime(Ri.timezoneid, ref startdate);

                        recurInfo.confid = Ri.confid;
                        recurInfo.confnumname = Ri.confnumname;
                        recurInfo.dayno = Ri.dayno;
                        recurInfo.days = Ri.days;
                        recurInfo.dirty = Ri.dirty;
                        recurInfo.duration = Ri.duration;
                        
                        recurInfo.startTime = startdate;
                        recurInfo.endTime = Ri.endTime;
                        recurInfo.endType = Ri.endType;
                        recurInfo.gap = Ri.gap;
                        recurInfo.occurrence = Ri.occurrence;
                        recurInfo.RecurringPattern = Ri.RecurringPattern;
                        recurInfo.recurType = Ri.recurType;
                        recurInfo.subType = Ri.subType;
                        recurInfo.timezoneid = Ri.timezoneid;
                        recurInfo.yearMonth = Ri.yearMonth;
                        recurInfo.uId = Ri.uId;

                        recFactory.recurInfo = recurInfo;
                        recFactory.startHour = startdate.ToString("hh");
                        recFactory.startMin = startdate.ToString("mm");
                        recFactory.startSet = startdate.ToString("tt");
                        recFactory.setDuration = Ri.SetupDuration;
                        recFactory.tearDuration = Ri.TeardownDuration;
                        

                        switch (Ri.recurType)
                        {
                            case 1:
                                outputXML.Append("<dailyType>" + Ri.subType.ToString()
                                    + "</dailyType>"); //		' 1: every ? day(s),	2: every weekday (have no <dayGap>)
                                outputXML.Append("<dayGap>" + Ri.gap.ToString()
                                    + "</dayGap>");

                                recurDates = recFactory.GetDailyRecurDates();
                                break;
                            case 2:
                                outputXML.Append("<weekGap>" + Ri.gap.ToString()
                                    + "</weekGap>");   //		' means "every 3 week(s)";
                                outputXML.Append("<weekDay>" + Ri.days.ToString()
                                    + "</weekDay>");

                                recurDates = recFactory.GetWeeklyRecurDates();
                                break;
                            case 3:
                                outputXML.Append("<monthlyType>" + Ri.subType.ToString()
                                    + "</monthlyType>");
                                switch (Ri.subType)
                                {
                                    case 1:
                                        outputXML.Append("<monthDayNo>" + Ri.dayno.ToString()
                                            + "</monthDayNo>");
                                        outputXML.Append("<monthGap>" + Ri.gap.ToString()
                                            + "</monthGap>");
                                        break;
                                    case 2:
                                        outputXML.Append("<monthWeekDayNo>" + Ri.dayno.ToString()
                                            + "</monthWeekDayNo>");  //	' 1: first, 2: second, 3: third, 4: forth, 5: last
                                        outputXML.Append("<monthWeekDay>" + Ri.days.ToString()
                                            + "</monthWeekDay>");	//	' 1: day, 2: weekday, 3: weekend, 4: Su, 5: Mo, 6: Tu, 7: We, 8: Th, 9: Fr,  8: Sa
                                        outputXML.Append("<monthGap>" + Ri.gap.ToString()
                                            + "</monthGap>");
                                        break;
                                }
                                recurDates = recFactory.GetMonthlyRecurDates();
                                break;
                            case 4:

                                outputXML.Append("<yearlyType>" + Ri.subType.ToString()
                                    + "</yearlyType>");  //' 1: expressed in day #, 2: expressed in weekday#
                                switch (Ri.subType)
                                {
                                    case 1:
                                        outputXML.Append("<yearMonth>" + Ri.yearMonth.ToString()
                                            + "</yearMonth>");  //		' 1: Jan, 2: Feb, ..., 12: Dec
                                        outputXML.Append("<yearMonthDay>" + Ri.days.ToString()
                                            + "</yearMonthDay>");
                                        break;
                                    case 2:
                                        outputXML.Append("<yearMonthWeekDayNo>" + Ri.dayno.ToString()
                                            + "</yearMonthWeekDayNo>"); //	' 1: first, 2: second, 3: third, 4: forth, 5: last
                                        outputXML.Append("<yearMonthWeekDay>" + Ri.days.ToString()
                                            + "</yearMonthWeekDay>");  //' 1: day, 2: weekday, 3: weekend, 4: Su, 5: Mo, 6: Tu, 7: We, 8: Th, 9: Fr,  8: Sa
                                        outputXML.Append("<yearMonth>" + Ri.yearMonth.ToString()
                                            + "</yearMonth>");
                                        break;
                                }
                                recurDates = recFactory.GetYearlyRecurDates();
                                break;

                            case 5:
                                 //Custom mode. Fetch all instances of this conference
                                /*
                                confsList = m_vrmConfDAO.GetByConfId(conf.confid);
                                outputXML.Append("<startDates>");
                                for (int i = 0; i < confsList.Count; i++)
                                {
                                    if (confsList[i].deleted == 0)
                                    {
                                        DateTime dt = confsList[i].confdate;
                                        if (utcEnabled == 0) //FB 2164 start
                                            timeZone.userPreferedTime(confsList[i].timezone, ref dt);

                                        outputXML.Append("<startDate>" + dt.ToString("d") + "</startDate>");
                                    }
                                }
                                outputXML.Append("</startDates>");
                                */
                                break;
                        }
                        outputXML.Append("</recurrencePattern>");

                        if (Ri.recurType < 5)
                        {
                            outputXML.Append("<recurrenceRange>");
                            outputXML.Append("<startDate>" + startdate.ToString("d") + "</startDate>");
                            outputXML.Append("<endType>" + Ri.endType.ToString() + "</endType>");		//' 1: no end date, 2: end after <occurrence> times, 3: end by <endDate>
                            switch (Ri.endType)
                            {
                                case 1:
                                    break;
                                case 2:
                                    outputXML.Append("<occurrence>" + Ri.occurrence.ToString() + "</occurrence>");
                                    break;
                                case 3:
                                    outputXML.Append("<endDate>" + Ri.endTime.ToString("d") + "</endDate>");
                                    break;
                            }
                            outputXML.Append("</recurrenceRange>");
                        }
                        if (recurDates != null)
                        {
                            StringBuilder oldInstanceXml = new StringBuilder();
                            oldInstanceXml.Append("<instanceDates>");
                            for (int ccnt = 0; ccnt < recurDates.Count; ccnt++)
                            {
                                oldInstanceXml.Append("<startDate>" + recurDates[ccnt].RecDatetime.ToShortDateString() + "</startDate>"); //timestamp.Format(_T("%m/%d/%Y"))
                            }
                            oldInstanceXml.Append("</instanceDates>");
                            outputXML.Append(oldInstanceXml);
                        }
                        outputXML.Append("</defunctPattern>");
                    }
                }
            }
            catch (Exception e)
            {
                m_log.Error("FetchRecurInfo ( Error in Retriving Recur info ):", e);
                outputXML.Append("");
                throw e;
            }
        }
        #endregion

        //FB 2027 End GetOldConference

        #region GetAdvancedAVSettings
        /// <summary>
        /// GetAdvancedAVSettings
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetAdvancedAVSettings(ref vrmDataObject obj)
        {
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;
                node = xd.SelectSingleNode("//GetAdvancedAVSettings/UserID");
                int userid = Int32.Parse(node.InnerXml.Trim());
                node = xd.SelectSingleNode("//GetAdvancedAVSettings/ConfID");
                string confid = node.InnerXml.Trim();

                
                CConfID Conf = new CConfID(confid);

                if (Conf.instance == 0)
                    Conf.instance = 1;

                //emailFactory em = new emailFactory(ref obj);
                //em.sendAcceptanceEmail(Conf.ID, Conf.instance, userid);
                //return true;
                obj.outXml += "<GetAdvancedAVSettings>";
                obj.outXml += "<UserID>" + userid + "</UserID>";
                obj.outXml += "<ConfID>" + confid + "</ConfID>";
                obj.outXml += "<Endpoints>";

                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("confid", Conf.ID));
                criterionList.Add(Expression.Eq("instanceid", Conf.instance));

                List<vrmConfRoom> rList = m_IconfRoom.GetByCriteria(criterionList);
                List<vrmConfUser> uList = m_IconfUser.GetByCriteria(criterionList);

                foreach (vrmConfRoom cr in rList)
                {
                    obj.outXml += EptRoomXml(cr);

                }
                foreach (vrmConfUser usr in uList)
                {
                    if (usr.invitee == 1)
                        obj.outXml += EptUserXml(usr);
                }
                obj.outXml += "</Endpoints>";
                obj.outXml += "</GetAdvancedAVSettings>";


                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = string.Empty;
                return false;
            }
        }
        #endregion

        #region SetAdvancedAVSettings
        /// <summary>
        /// SetAdvancedAVSettings
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SetAdvancedAVSettings(ref vrmDataObject obj)
        {
            myVRMException myvrmEx = null; //FB 2390
            bool Isreccuring = false;
            //FB 1864
            Int32 isVIP = 0;
            Int32 isDedicatedEngineer = 0;
            Int32 isLiveAssitant = 0;
            //FB 1864

            Int32 isReminder = 16;//FB 1926
			//FB 1937
            HardwareFactory HF = null;
            List<ICriterion> MCUcriterionList = null;
            List<vrmMCU> MCUList = null;
            List<vrmMCUResources> vMCUList = null;
            //FB 1937
            int editFromWeb = 0;//FB 2235
            List<vrmConfMessage> confMsgs = null; //FB 2486
            StringBuilder confMsgList = new StringBuilder(); //FB 2486
            vrmConference ConferenceMsg = null;
            List<int> ConfBridgeid = new List<int>();//FB 2566
            List<int> ConfE164Bridgeid = new List<int>();//FB 2636
            bool bE164 = false, bH323 = false;//FB 2636
            try
            {
                //1014 external attendee email support   
                //1036 auto detect MCU add support for Codian and Tanberg
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;
                XmlNodeList nodeList = null; //FB 2486
                //FB 2426 Starts
                nodeList = xd.SelectNodes("//SetAdvancedAVSettings/Endpoints/Endpoint/OnFlyRoom");
                if (nodeList.Count>0)
                {
                    int flyRoom = 0;
                    for (int i = 0; i < nodeList.Count; i++)
                    {
                        flyRoom = 0;
                        Int32.TryParse(nodeList[i].InnerText.Trim(), out flyRoom);
                        if (flyRoom == 1)
                            break;
                    }
                    if(flyRoom == 1)
                        SetFlyRoomBridge(ref xd);
                }
                //FB 2426 Ends
                node = xd.SelectSingleNode("//SetAdvancedAVSettings/UserID");
                int userid = Int32.Parse(node.InnerXml.Trim());
                node = xd.SelectSingleNode("//SetAdvancedAVSettings/ConfID");
                string confid = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//SetAdvancedAVSettings/organizationID"); //Organization Module Fixes
                string orgid = "";
                if(node != null)
                    orgid = node.InnerXml.Trim();
                organizationID = defaultOrgId;

                //FB 2274 Starts
                node = xd.SelectSingleNode("//SetAdvancedAVSettings/multisiloOrganizationID");
                string multisiloOrgID = "";
                int mutliOrgID = 0;
                if (node != null)
                    multisiloOrgID = node.InnerXml.Trim();
                Int32.TryParse(multisiloOrgID, out mutliOrgID);
                if (mutliOrgID > 11)
                    orgid = multisiloOrgID;
                //FB 2274 Ends
                Int32.TryParse(orgid, out organizationID);

                if (organizationID < 11)   
                    organizationID = defaultOrgId;

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                m_emailFactory.organizationID = organizationID;
                m_emailFactory.orgInfo = orgInfo;  //Organization Module

                node = xd.SelectSingleNode("//SetAdvancedAVSettings/AVParams/SingleDialin");
                string singleDialin = node.InnerXml.Trim();
                if (singleDialin.Length == 0)
                    singleDialin = "0";


                node = xd.SelectSingleNode("//SetAdvancedAVSettings/isVIP");   //FB 1864
                if (node != null)
                {
                    if (node.InnerText != "")
                        Int32.TryParse(node.InnerText, out isVIP);
                }

                node = xd.SelectSingleNode("//SetAdvancedAVSettings/editFromWeb");   //FB 2235
                if (node != null)
                {
                    if (node.InnerText != "")
                        Int32.TryParse(node.InnerText, out editFromWeb);
                }
                

                node = xd.SelectSingleNode("//SetAdvancedAVSettings/AVParams/isDedicatedEngineer");   //FB 1864
                if (node != null)
                {
                    if(node.InnerText != "")
                        Int32.TryParse(node.InnerText,out isDedicatedEngineer);
                }

                node = xd.SelectSingleNode("//SetAdvancedAVSettings/AVParams/isLiveAssitant");   //FB 1864
                if (node != null)
                {
                    if(node.InnerText != "")
                        Int32.TryParse(node.InnerText,out isLiveAssitant);
                }


                node = xd.SelectSingleNode("//SetAdvancedAVSettings/isReminder");   //FB 1926
                if (node != null)
                {
                    if (node.InnerText == "0")
                        Int32.TryParse(node.InnerText, out isReminder);
                }
                else  //FB 2499
                    isReminder = 0;

                //FB 2486 Starts
                nodeList = xd.SelectNodes("//SetAdvancedAVSettings/ConfMessageList");
                if (nodeList.Count == 0)
                {
                    FetchConfMessages(null, ref confMsgList);
                    xd.SelectSingleNode("//SetAdvancedAVSettings").InnerXml += confMsgList.ToString();
                }

                nodeList = xd.SelectNodes("//SetAdvancedAVSettings/ConfMessageList/ConfMessage");
                if (nodeList.Count > 0)
                    LoadConfMsgs(ref nodeList, ref confMsgs);

                //FB 2486 Ends

                CConfID Conf = new CConfID(confid);

                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Not(Expression.Eq("status", 7))); //FB 2027 SetConference
                criterionList.Add(Expression.Eq("confid", Conf.ID));
                
                if (Conf.instance > 0)
                    criterionList.Add(Expression.Eq("instanceid", Conf.instance));

                //FB 2075 Starts
                DateTime serverTime = DateTime.Now;
                timeZone.changeToGMTTime(sysSettings.TimeZone, ref serverTime);
                serverTime = serverTime.AddMinutes(-5); // Immd Conf Issue
                criterionList.Add(Expression.Ge("confdate", serverTime));
                //FB 2075 Ends
                List<vrmConference> conf = m_vrmConfDAO.GetByCriteria(criterionList, true);

                if (orgInfo.SendConfirmationEmail == 0) //FB 2470
                    m_emailFactory.sendICALforCISCOEndpoints(conf, "D", true, -1);// FB 1675
                
                List<vrmConfRoom> roomList = new List<vrmConfRoom>();
                List<vrmConfUser> userList = new List<vrmConfUser>();
                List<ICriterion> criterionListService;
               
                // cascading 
                Hashtable cascadeBridge = new Hashtable();
                List<vrmConfCascade> cascadeList = new List<vrmConfCascade>();

                XmlNodeList NodeList = xd.GetElementsByTagName("Endpoint");
                List<GetEndPointService> epServiceList = new List<GetEndPointService>();
                int bid = 0;
                bool b_vMCU = false;
                bool b_foundMCU = false;
                string eptURL = "";//During API Port...
                string exchangeID = ""; //Cisco Telepresence fix
                int ApiPortNo = 23; //API Port...   
                string Connect2 = "";//FB 2390
                
                //FB 2486  Starts
                List<ICriterion> criterionMsgMCu = new List<ICriterion>();
                criterionMsgMCu.Add(Expression.Eq("EnhancedMCU", 1)); //FB 2636
                criterionMsgMCu.Add(Expression.Eq("deleted", 0));
                //criterionMsgMCu.Add(Expression.Eq("BridgeTypeId", 8)); // MCU Type - Ploycom RMX
                criterionMsgMCu.Add(Expression.Or(Expression.Eq("orgId", organizationID), Expression.Eq("isPublic", 1)));
                List<vrmMCU> MCUMSGList = m_vrmMCU.GetByCriteria(criterionMsgMCu);

                //FB 2486 Ends

                // load eps with all information if -1 it is null (use default)
                vrmRoom locroom = null;//FB 2492
                foreach (XmlNode Node in NodeList)
                {
                    GetEndPointService eps = new GetEndPointService();

                    XmlElement element = (XmlElement)Node;
                    eps.sType = element.GetElementsByTagName("Type")[0].InnerText.ToUpper();

                    eps.id = Int32.Parse(element.GetElementsByTagName("ID")[0].InnerText);

                    eps.isDefault = Int32.Parse(element.GetElementsByTagName("UseDefault")[0].InnerText);

                    eps.singleDialin = Int32.Parse(singleDialin);

                    eps.endpointId = 0;
                    eps.profileId = 0;
                    if (element.GetElementsByTagName("EndpointID")[0].InnerText.Length > 0)
                        eps.endpointId = Int32.Parse(element.GetElementsByTagName("EndpointID")[0].InnerText);
                    if (element.GetElementsByTagName("ProfileID")[0].InnerText.Length > 0)
                        eps.profileId = Int32.Parse(element.GetElementsByTagName("ProfileID")[0].InnerText);
                    string BridgeID = element.GetElementsByTagName("BridgeID")[0].InnerText;
                    if (BridgeID.Length > 0)
                        bid = Int32.Parse(BridgeID);
                    else
                        bid = 0;
                    eps.bridgeid = bid;

                    //FB 2566 Start
                    if (bid > 0)
                    {
                        if (!ConfBridgeid.Contains(bid))
                            ConfBridgeid.Add(bid);
                    }
                    //FB 2566 End
                    //FB 2492 Start
                    if (eps.sType == "R")
                    {
                        if (eps.id > 0)
                        {
                            locroom = m_vrmRoomDAO.GetById(eps.id);
                            if (locroom != null)
                                if (locroom.IsVMR == 1)
                                    continue;
                        }
                    }
                    //FB 2492 End

                    // if any mcu is virtual the whole confernece is virtual
                    if (!b_vMCU && bid == -1)
                        b_vMCU = true;
                    string AddressType = element.GetElementsByTagName("AddressType")[0].InnerText;

                    //API Port Fix start
                    if (element.GetElementsByTagName("ExchangeID")[0] != null)
                        exchangeID = element.GetElementsByTagName("ExchangeID")[0].InnerText; //Cisco Telepresence fix
                    else
                        exchangeID = "";

                    eps.ExchangeID = exchangeID;

                    if (element.GetElementsByTagName("APIPortNo")[0] != null)
                    {
                        Int32.TryParse(element.GetElementsByTagName("APIPortNo")[0].InnerText.Trim(), out ApiPortNo);
                    }
                    if (ApiPortNo <= 0)
                        ApiPortNo = 23;
                    eps.EPTAPIPortNo = ApiPortNo;

                    if (element.GetElementsByTagName("URL")[0] != null)
                        eptURL = element.GetElementsByTagName("URL")[0].InnerText;//During API Port
                    else
                        eptURL = "";

                    eps.EPTURL = eptURL;
                    //API Port Fix end...

                    if (AddressType.Length > 0)
                        eps.addressType = Int32.Parse(AddressType);
                    else
                        eps.addressType = -1;

                    string address = element.GetElementsByTagName("Address")[0].InnerText;
                    if (address.Length == 0)
                        eps.address = string.Empty;
                    else
                        eps.address = address;

                    string Bandwidth = element.GetElementsByTagName("Bandwidth")[0].InnerText;
                    if (Bandwidth.Length > 0)
                        eps.defLineRate = Int32.Parse(Bandwidth);
                    else
                        eps.defLineRate = -1;
                    string IsOutside = element.GetElementsByTagName("IsOutside")[0].InnerText;
                    // video equipment is only user and no profile id
                    string VideoEquipment = element.GetElementsByTagName("VideoEquipment")[0].InnerText;
                    if (eps.sType == "U")
                    {
                        if (VideoEquipment.Length > 0)
                            eps.profileId = Int32.Parse(VideoEquipment);
                        else
                            eps.profileId = -1;
                        if (IsOutside.Length > 0)
                        {
                            if (Int32.Parse(IsOutside) == 1)
                                eps.outsideNetwork = eptDetails.isPublic;
                            else
                                eps.outsideNetwork = eptDetails.isPrivate;
                        }
                        else
                        {
                            eps.outsideNetwork = eptDetails.isPrivate;
                        }
                    }
          
                    
                    string connectionType = element.GetElementsByTagName("connectionType")[0].InnerText;
                    if (connectionType.Length > 0)
                        eps.connectionType = Int32.Parse(connectionType);
                    else
                        eps.connectionType = -1;
                    string DefaultProtocol = element.GetElementsByTagName("DefaultProtocol")[0].InnerText;
                    if (DefaultProtocol.Length > 0)
                        eps.defVideoProtocol = Int32.Parse(DefaultProtocol);
                    else
                        eps.defVideoProtocol = 1;
                    //Code Modified - FB 1422 -Start
                    //string Connect2 = element.GetElementsByTagName("Connection")[0].InnerText;
                    //eps.connect2 = eptDetails.isBoth;
                    //if (Connect2.Length > 0)
                    //    if (Int32.Parse(Connect2) == 2)
                    //        eps.connect2 = eptDetails.isAudio;
                    //    else
                    //        if (Int32.Parse(Connect2) == 3)
                    //            eps.connect2 = eptDetails.isVideo;
                    //FB 2390 Start
                    if (conf[0].conftype == vrmConfType.P2P)
                    {
                        if (NodeList.Count == 2)
                        {
                            if (orgInfo.EnableSmartP2P==1 && Connect2 == element.GetElementsByTagName("Connect2")[0].InnerText) //FB 2430
                                element.GetElementsByTagName("Connect2")[0].InnerText = Connect2.Trim() == "1" ? "0" : "1";

                            if (element.GetElementsByTagName("Connect2")[0].InnerText != "-1")
                            {
                                if (Connect2 != element.GetElementsByTagName("Connect2")[0].InnerText)
                                {
                                    Connect2 = element.GetElementsByTagName("Connect2")[0].InnerText;
                                }
                                else
                                {
                                    myvrmEx = new myVRMException(610);
                                    obj.outXml = myvrmEx.FetchErrorMsg();
                                    return false;
                                }
                            }

                            else
                            {
                                Connect2 = element.GetElementsByTagName("Connect2")[0].InnerText;
                            }
                        }
                        else
                        {
                            myvrmEx = new myVRMException(610);
                            obj.outXml = myvrmEx.FetchErrorMsg();
                            return false;
                        }
                    }
                    else
                    {
                        Connect2 = "-1";
                    }
                    //FB 2390 End
                    eps.connect2 = Int32.Parse(Connect2);
                    //Code Modified - FB 1422 -End

                    string isLecturer = element.GetElementsByTagName("IsLecturer")[0].InnerText;
                    eps.isLecturer = Int32.Parse(isLecturer);
                  
                    epServiceList.Add(eps);

                }
                foreach (vrmConference conference in conf)
                {
                    // FB 1675
                    List<ICriterion> disabledcriterionList = new List<ICriterion>();
                    disabledcriterionList.Add(Expression.Eq("confid", conference.confid));
                    disabledcriterionList.Add(Expression.Eq("instanceid", conference.instanceid));
                    disabledcriterionList.Add(Expression.Eq("disabled",1));

                    List<vrmConfRoom> disabledrList = m_IconfRoom.GetByCriteria(disabledcriterionList);

                    if (disabledrList.Count > 0)
                        m_IconfRoom.Delete(disabledrList[0]);

                    // FB 1675

                    m_ISDNusedNumber = new Hashtable();
                    // virtual mcu assignement
                    // load all mcu's (Ploycomm, Codian, Tanberg) 
                    // get ussage for all mcu's
                    // determine which mcu has the available resources.
                    // if no MCU has hresource assing "best fit" mcu
                    //
                    vrmConference baseConf;
                    if (b_vMCU)
                    {
                        // get all mcu's

                        //FB 1937
                        HF = new HardwareFactory(ref obj);

                        MCUcriterionList = new List<ICriterion>();
                        MCUcriterionList.Add(Expression.Eq("deleted", 0));
                        MCUcriterionList.Add(Expression.Or(Expression.Eq("orgId", organizationID),Expression.Eq("isPublic", 1))); //FB 2073
                        m_vrmMCU.addOrderBy(Order.Asc("BridgeID"));

                        MCUList = m_vrmMCU.GetByCriteria(MCUcriterionList);
                        m_vrmMCU.clearOrderBy();

                        vMCUList = new List<vrmMCUResources>();
                        foreach (vrmMCU mMCU in MCUList)
                        {
                            vrmMCUResources vRes = new vrmMCUResources(mMCU.BridgeID);
                            if (HF.loadVRMResourceData(mMCU, ref vRes))
                                vMCUList.Add(vRes);
                        }
                        //FB 1937

                        int isSwitched = 0;
                        int isEncryption = 0;

                        baseConf = m_vrmConfDAO.GetByConfId(Conf.ID, 1);                               
                        vrmConfAdvAvParams AvParams = baseConf.ConfAdvAvParams;
                        
                        if(AvParams.videoMode == 1)
                            isSwitched = 1;
                        if(AvParams.encryption == 1)
                            isEncryption = 1;
                       

                        
                        // get mcu usages
                        HF.GetMCUusage(conference.confdate, conference.duration, conference.confid,
                            conference.instanceid, ref vMCUList,true);


                        foreach (vrmMCUResources res in vMCUList)
                        {
                            if (b_foundMCU)
                                break;
                            foreach ( GetEndPointService eps in epServiceList)
                            {
                                int isVideo = 1;
                                int addressType = 0;
                                if (eps.sType == "R")
                                {
                                    MCUcriterionList = new List<ICriterion>();
                                    MCUcriterionList.Add(Expression.Eq("confid", conference.confid));
                                    MCUcriterionList.Add(Expression.Eq("instanceid", conference.instanceid));
                                    MCUcriterionList.Add(Expression.Eq("roomId", eps.id));
                                    List<vrmConfRoom> rList = m_IconfRoom.GetByCriteria(MCUcriterionList);

                                    if (rList[0].audioorvideo == 1)
                                        isVideo = 0;
                                    addressType = rList[0].addressType;
                                }
                                else 
                                {
                                    MCUcriterionList = new List<ICriterion>();
                                    MCUcriterionList.Add(Expression.Eq("confid", conference.confid));
                                    MCUcriterionList.Add(Expression.Eq("instanceid", conference.instanceid));
                                    MCUcriterionList.Add(Expression.Eq("userid", eps.id));
                                    List<vrmConfUser> uList = m_IconfUser.GetByCriteria(MCUcriterionList );
                                    
                                    if (uList[0].audioOrVideo == 1)
                                        isVideo = 0;
                                    addressType = uList[0].addressType;
                                }
                                HF.vMCUResourceLoad(addressType, isVideo, isSwitched, isEncryption, res, true);
                                
                            }
                            // check MCU if ok assign
                            if (vMCUcheck(res))
                            {
                                foreach (GetEndPointService eps in epServiceList)
                                    eps.bridgeid = res.bridgeId;
                                b_foundMCU = true;
                            }
                        }
                        if (!b_foundMCU)
                        {
                            int bestFitId = 0;
                            int oldWeight = -1;
                            foreach (vrmMCUResources vmR in vMCUList)
                            {
                                if (vmR.weight > oldWeight)
                                {
                                    bestFitId = vmR.bridgeId;
                                    oldWeight = vmR.weight;
                                    // don't use this algorytm, will use it in casaced (PHASE II)
                                    // for now pick the first bridge in the chain

                                    // lastley email bridge admin and conf host 
                                    //m_emailFactory.sendBridgeBookedEmail(baseConf.owner, baseConf.confid,baseConf.instanceid, vmR); Commented for FB 2472

                                    //FB 2344 - Starts
                                    //While checking Bestfit MCUs, if all MCUs are lack resources, then MCUAlert Mail will be sent to MCU Admin.
                                    //Same thing is doing below. So Commented for FB 2344.
                                     /*if (conf[0].conftype != vrmConfType.P2P) //FB 2350
                                     {
                                        //FB 2472 Start
                                        if (orgInfo.SendConfirmationEmail == 0)
                                        {
                                            if (orgInfo.MCUAlert != 0)
                                            {
                                                m_emailFactory.sendBridgeBookedEmail(baseConf, vmR); //FB 1830
                                            }
                                        }
                                        //FB 2472 End
                                     }*/
                                    //FB 2344 - End
                                    break;
                                }
                            }
                            foreach (GetEndPointService eps in epServiceList)
                                eps.bridgeid = bestFitId;
                        }
                    }

                  

                    foreach (GetEndPointService epsValue in epServiceList)
                    {
                        GetEndPointService eps = epsValue;
                        eps.confid = conference.confid;
                        eps.instanceid = conference.instanceid;

                        
                        string batype = string.Empty;
                        string servicename = string.Empty;
                        string bridgeipisdnaddress = string.Empty;


                        //
                        // conf user and conf room should be in one table as is demonstrated here! 
                        //

                        if (eps.sType == "R")
                        {

                            criterionList = new List<ICriterion>();
                            criterionList.Add(Expression.Eq("confid", conference.confid));
                            criterionList.Add(Expression.Eq("instanceid", conference.instanceid));
                            
                            // get the endpoint. If the above tags are blank use ep valuse instead. 
                            List<ICriterion> criterionListEP = new List<ICriterion>();
                            criterionListEP.Add(Expression.Eq("endpointid", eps.endpointId));
                            // if isDefault = 0 then get the profile by id
                            // if isDefault = 1 get the default profile FB:153
                            if(eps.isDefault == 0)
                                criterionListEP.Add(Expression.Eq("profileId", eps.profileId));
                            else
                                criterionListEP.Add(Expression.Eq("isDefault", 1));                          
                            criterionListEP.Add(Expression.Eq("deleted", 0));
                            List<vrmEndPoint> epList = m_vrmEpt.GetByCriteria(criterionListEP);

                            // it HAS to be here
                            if (epList.Count < 1)
                            {
                                //FB 1881 start
                               //obj.outXml = myVRMException.toXml("Error cannot retrieve endpoint " + eps.endpointId.ToString() + " / " + eps.profileId.ToString());
                                m_log.Error("Error cannot retrieve endpoint " + eps.endpointId.ToString() + " / " + eps.profileId.ToString());
                                obj.outXml = "";
                                //FB 1881 end
                                return false;
                            }

                            vrmEndPoint ep = epList[0];

                            // if aps did not supply the address use the one from the endpoint.
                            if (eps.address.Length == 0)
                                eps.address = ep.address;

                            eps.MCUAddress = string.Empty;
                            eps.MCUAddress = ep.MCUAddress;
                            eps.MCUAddressType = ep.MCUAddressType;

                            criterionList.Add(Expression.Eq("roomId", eps.id));
                            List<vrmConfRoom> rList = m_IconfRoom.GetByCriteria(criterionList);
                            if (rList.Count < 1)
                            {
                                //FB 1881 start
                                //obj.outXml = myVRMException.toXml("Error cannot retrieve room " + eps.id.ToString());
                                m_log.Error("Error cannot retrieve room " + eps.id.ToString());
                                obj.outXml = "";
                                //FB 1881 end
                                return false;
                            }
                            foreach (vrmConfRoom confRoom in rList)
                            {
                                

                                confRoom.endpointId = eps.endpointId;
                                confRoom.profileId = eps.profileId;
                                confRoom.ApiPortNo = eps.EPTAPIPortNo;//API Port No...
                                confRoom.endptURL = eps.EPTURL;//During API Port No...
                                if (eps.bridgeid >= 0)
                                    confRoom.bridgeid = eps.bridgeid;
                                else
                                    confRoom.bridgeid = ep.bridgeid;
                                if (eps.outsideNetwork < 0)
                                    eps.outsideNetwork = 0;

                                eps.id = confRoom.roomId;

                                if (confRoom.audioorvideo == 2) //FB 1744
                                    eps.usage = eptDetails.isVideo; // 1 is audio 2 is video
                                else
                                    eps.usage = eptDetails.isAudio;

                                if (ep.outsidenetwork == 0)
                                    eps.outsideNetwork = eptDetails.isPrivate;
                                else
                                    eps.outsideNetwork = eptDetails.isPublic;

                                criterionListService = new List<ICriterion>();

                                // address type is undefined use endp point.
                                if (eps.addressType == -1)
                                    eps.addressType = ep.addresstype;

                                // ip
                                bool valid = true;
                                if (eps.addressType != eptDetails.typeISDN)
                                {
                                    vrmMCU MCU = null;//FB 2594
                                    if (confRoom.bridgeid > 0)
                                        MCU = m_vrmMCU.GetById(confRoom.bridgeid);
                                    else if (m_vrmMCU.GetAll().Count > 0)
                                        MCU = m_vrmMCU.GetAll()[0];
                                    else
                                        continue;

                                    if (MCU.MCUType.bridgeInterfaceId == vrmMCU.BRIDGEID_TANBERG)
                                    {
                                        eps.mcuServiceName = MCU.BridgeName;
                                        eps.bridgeAddressType = eptDetails.typeIP;
                                        eps.bridgeIPISDNAddress = MCU.BridgeAddress;
                                    }                                                                        
                                    else
                                    {
                                        if (!GetEndPointService(ref eps))
                                        {
                                            m_log.Error("Error cannot retrieve IPServices, Bridgeid = " + confRoom.bridgeid.ToString());
                                            m_log.Error("for conference confid  = " + Conf.ID + "/" + Conf.instance);
                                            m_log.Error("Continuing ....");
                                            valid = false;
                                        }
                                   }
                                }
                                else // isdn
                                {
                                  
                                    if (!GetEndPointService(ref eps))
                                    {
                                        if (eps.addressType == eptDetails.typeMPI)
                                        {
                                            m_log.Error("Error cannot retrieve MPI Services, Bridgeid = " + confRoom.bridgeid.ToString());
                                            m_log.Error("for conference confid  = " + Conf.ID + "/" + Conf.instance);
                                            m_log.Error("Continuing ....");
                                            valid = false;
                                        }
                                        else
                                        {
                                            m_log.Error("Error cannot retrieve IPDNServices, Bridgeid = " + confRoom.bridgeid.ToString());
                                            m_log.Error("for conference confid  = " + Conf.ID + "/" + Conf.instance);
                                            m_log.Error("Continuing ....");
                                            valid = false;
                                        }
                                    }
                                }
                                if (valid)
                                {
                                    confRoom.ipisdnaddress = eps.address;
                                    confRoom.mcuServiceName = eps.mcuServiceName;
                                    confRoom.bridgeAddressType = eps.addressType;
                                    confRoom.bridgeIPISDNAddress = eps.bridgeIPISDNAddress;
                                    confRoom.prefix = eps.prefix;
                                    confRoom.defVideoProtocol = eps.defVideoProtocol;
                                }

                                // use END POINT address type here! 
                                confRoom.addressType = ep.addresstype;
                                //FB 2610 Starts
                                vrmMCU confMCU = m_vrmMCU.GetById(confRoom.bridgeid);
                                confRoom.BridgeExtNo = confMCU.BridgeExtNo;
                                //FB 2610 Ends
                                confRoom.MultiCodecAddress = ep.MultiCodecAddress;//FB 2602

                                if (eps.defLineRate >= 0)
                                    confRoom.defLineRate = eps.defLineRate;
                                else
                                    confRoom.defLineRate = ep.linerateid;

                                confRoom.outsideNetwork = ep.outsidenetwork;
                             
                                if (eps.connectionType >= 0)
                                    confRoom.connectionType = eps.connectionType;
                                else
                                    confRoom.connectionType = ep.connectiontype;

                                //Code Commented FB 1422
                                //if (eps.connect2 >= 0)
                                    confRoom.connect2 = eps.connect2;
                                
                                confRoom.isLecturer = eps.isLecturer;
                                //confRoom.connectStatus = 1; // FB 1262
                                confRoom.connectStatus = 0;
                                
                                //FB 2486,FB 2636 Starts
                                if (conference.isVMR == 0)
                                {
                                    for (int i = 0; i < MCUMSGList.Count; i++)
                                    {
                                        if (MCUMSGList[i].EnableMessage == 1)
                                        {
                                            if (MCUMSGList[i].BridgeID == confRoom.bridgeid)
                                                confRoom.isTextMsg = 1;
                                        }
                                        if ((MCUMSGList[i].E164Dialing == 1 || MCUMSGList[i].H323Dialing == 1) && orgInfo.EnableDialPlan == 1)
                                        {
                                            if (MCUMSGList[i].BridgeID == confRoom.bridgeid)
                                            {
                                                if (MCUMSGList[i].E164Dialing == 1)
                                                    bE164 = true;
                                                if (MCUMSGList[i].H323Dialing == 1)
                                                    bH323 = true;
                                                if (!ConfE164Bridgeid.Contains(MCUMSGList[i].BridgeID))
                                                    ConfE164Bridgeid.Add(MCUMSGList[i].BridgeID);
                                            }
                                        }
                                    }
                                }
                                //FB 2486,FB 2636 Ends
                                //FB 2501 Call monitoring Start
                                confRoom.GUID = "";
                                confRoom.MuteRxaudio = 0;
                                confRoom.MuteRxvideo = 0;
                                confRoom.MuteTxvideo = 0;
                                confRoom.Stream = "";
                                confRoom.Setfocus = 0;
                                confRoom.Message = 0;
                                confRoom.Camera = 0;
                                confRoom.Packetloss = 0;
                                confRoom.LockUnLock = 0;
                                confRoom.Record = 0;
                                //FB 2501 Call monitoring End
                                roomList.Add(confRoom);

                                if (!cascadeBridge.ContainsKey(confRoom.bridgeid))
                                    cascadeBridge.Add(confRoom.bridgeid, 0);

                            }

                        }
                        else
                        {
                            // right now asp only send external attendee's to this routing. 
                            // this logic will have to be modified to CHECK when it is merged with
                            // set conference.
                            criterionList = new List<ICriterion>();
                            criterionList.Add(Expression.Eq("confid", conference.confid));
                            criterionList.Add(Expression.Eq("instanceid", conference.instanceid));
                            criterionList.Add(Expression.Eq("userid", eps.id));
                            List<vrmConfUser> uList = m_IconfUser.GetByCriteria(criterionList);
                            if (uList.Count < 1)
                            {
                                //FB 1881 start
                                //obj.outXml = myVRMException.toXml("Error cannot retrieve user " + eps.id.ToString());
                                m_log.Error("Error cannot retrieve user " + eps.id.ToString());
                                obj.outXml = "";
                                //FB 1881 end
                                return false;
                            }
                            foreach (vrmConfUser confUser in uList)
                            {

                                if (confUser.invitee != 1) //FB 2599
                                    continue;

                                if (eps.bridgeid >= 0)
                                    confUser.bridgeid = eps.bridgeid;
                                confUser.addressType = eps.addressType;
                                confUser.ExchangeID = eps.ExchangeID; //Cisco Telepresence fix - API Port
                                confUser.ApiPortNo = eps.EPTAPIPortNo; //API Port...
                                confUser.endptURL = eps.EPTURL; //API Port...
                                if (eps.outsideNetwork < 0)
                                    eps.outsideNetwork = 0;

                                eps.id = confUser.userid;
                           
                                bool valid = true;
                     
                                // FB 601 do not invalidate if not EnpPointService found (could be external user)
                                if (eps.addressType != eptDetails.typeISDN)
                                {
                                    vrmMCU MCU = m_vrmMCU.GetById(confUser.bridgeid);
                                    if (MCU.MCUType.bridgeInterfaceId == vrmMCU.BRIDGEID_TANBERG)
                                    {
                                        eps.mcuServiceName = MCU.BridgeName;
                                        eps.bridgeAddressType = eptDetails.typeIP;
                                        eps.bridgeIPISDNAddress = MCU.BridgeAddress;
                                    }                                                                        
                                    else
                                    {
                                        if (!GetEndPointService(ref eps))
                                        {
                                            m_log.Error("WARNING cannot retrieve IPServices, Bridgeid = " + confUser.bridgeid.ToString());
                                            m_log.Error("for conference confid  = " + Conf.ID + "/" + Conf.instance);
                                            m_log.Error("Continuing ....");
                                            //valid = false;
                                        }
                                    }
                                }
                                else // isdn
                                {
                                 
                                    if (!GetEndPointService(ref eps))
                                    {
                                        if (eps.addressType == eptDetails.typeMPI)
                                        {
                                            m_log.Error("WARNING cannot retrieve MPI Services, Bridgeid = " + confUser.bridgeid.ToString());
                                            m_log.Error("for conference confid  = " + Conf.ID + "/" + Conf.instance);
                                            m_log.Error("Continuing ....");
                                        }
                                        else
                                        {
                                            m_log.Error("WARNING cannot retrieve ISDNServices, Bridgeid = " + confUser.bridgeid.ToString());
                                            m_log.Error("for conference confid  = " + Conf.ID + "/" + Conf.instance);
                                            m_log.Error("Continuing ....");
                                        }
                                        //valid = false;
                                    }
                                }
                                if (valid)
                                {
                                    if(confUser.responseDate < DateTime.Parse("01/01/1980 12:00:00"))
                                            confUser.responseDate = DateTime.Parse("01/01/1980 12:00:00");
                                    confUser.ipAddress = eps.address;
                                    confUser.ipisdnaddress = eps.address;
                                    confUser.bridgeAddressType = eps.addressType;
                                    confUser.mcuServiceName = eps.mcuServiceName;
                                    confUser.bridgeIPISDNAddress = eps.bridgeIPISDNAddress;
                                    confUser.prefix = eps.prefix;
                                    confUser.outsideNetwork = 0;
                                    if (eps.outsideNetwork == eptDetails.isPublic)
                                        confUser.outsideNetwork = 1;
                                }
                                if (eps.defLineRate >= 0)
                                    confUser.defLineRate = eps.defLineRate;
                                if (eps.defVideoProtocol >= 0)
                                    confUser.defVideoProtocol = eps.defVideoProtocol;
                                if (eps.connectionType >= 0)
                                    confUser.connectionType = eps.connectionType;
                                if (eps.profileId >= 0)
                                    confUser.videoEquipment = eps.profileId;
                                //Code Commented FB 1422
                                //if (eps.connect2 >= 0)
                                    confUser.connect2 = eps.connect2;
                                //auto set user to accept invitation 

                                //Code added - FB Issue 1501 Start
                                //confUser.status = vrmConfStatus.Pending;
                                confUser.status = 0; //Default to Undecided FB 2419
                                //FB 2490 start
                                vrmUser User = m_vrmUserDAO.GetByUserId(confUser.userid);
                                if (User != null)
                                    if (User.Audioaddon == "1")
                                        confUser.status = 1;
                                //FB 2490 end
                                if (conference != null)
                                {
                                    if (conference.immediate == 1)
                                        confUser.status = 1; //Default to Accepted
                                }
                                //Code added - FB Issue 1501 End

                                //confUser.connectStatus = 1; //FB 1262
                                confUser.connectStatus = 0;
                                confUser.isLecturer = eps.isLecturer;

                                //FB 2486,FB 2636 Starts

                                if (conference.isVMR == 0)
                                {
                                    for (int i = 0; i < MCUMSGList.Count; i++)
                                    {
                                        if (MCUMSGList[i].EnableMessage == 1)
                                        {
                                            if (MCUMSGList[i].BridgeID == confUser.bridgeid)
                                                confUser.isTextMsg = 1;
                                        }
                                        if ((MCUMSGList[i].E164Dialing == 1 || MCUMSGList[i].H323Dialing == 1) && orgInfo.EnableDialPlan == 1)
                                        {
                                            if (MCUMSGList[i].BridgeID == confUser.bridgeid)
                                            {
                                                if (MCUMSGList[i].E164Dialing == 1)
                                                    bE164 = true;
                                                if (MCUMSGList[i].H323Dialing == 1)
                                                    bH323 = true;
                                                if (!ConfE164Bridgeid.Contains(MCUMSGList[i].BridgeID))
                                                    ConfE164Bridgeid.Add(MCUMSGList[i].BridgeID);
                                            }
                                        }
                                    }
                                }
                                //FB 2486,FB 2636 Ends

                                //FB 2501 Call Monitoring Start

                                confUser.GUID = "";
                                confUser.MuteRxaudio = 0;
                                confUser.MuteRxvideo = 0;
                                confUser.MuteTxvideo = 0;
                                confUser.Stream = "";
                                confUser.Setfocus = 0;
                                confUser.Message = 0;
                                confUser.Camera = 0;
                                confUser.Packetloss = 0;
                                confUser.LockUnLock = 0;
                                confUser.Record = 0;
                                //FB 2501 Call Monitoring End

                                userList.Add(confUser);
                                if (confUser.bridgeid > 0 && (!cascadeBridge.ContainsKey(confUser.bridgeid)))
                                    cascadeBridge.Add(confUser.bridgeid, 0);
                            }
                        }
                    }
                }

                foreach (vrmConference createType in conf)
                {
                    if (createType.CreateType == 0)
                        m_isEdit = false;
                    else
                        m_isEdit = true;

                    createType.CreateType = 1;

                    //FB 1864
                    criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("confid", createType.confid));
                    criterionList.Add(Expression.Eq("instanceid", createType.instanceid));
                    List<vrmConfRoom> rList = m_IconfRoom.GetByCriteria(criterionList);
                    for (int rcnt = 0; rcnt < rList.Count;rcnt++ )
                    {
                        if (isVIP < 1)
                        {
                            if (rList[rcnt].Room.isVIP == 1)
                                isVIP = 1;
                        }
                    }
                    createType.isVIP = isVIP;
                    createType.isDedicatedEngineer = isDedicatedEngineer;
                    createType.isLiveAssistant = isLiveAssitant;
                    //FB 1864

                    //Fb 1926

                    if (isReminder < 15)
                        createType.isReminder = 0;
                    else
                    { //if (createType.isReminder < 15)
                            createType.isReminder = isReminder;
                    }

                    //Fb 1926
                    

                    //FB 2636 Start
                    if (ConfE164Bridgeid.Count > 0)
                    {
                        createType.H323Dialing = 0;
                        createType.E164Dialing = 0;

                        if (bH323)
                            createType.H323Dialing = 1;

                        if (bE164)
                            createType.E164Dialing = 1;
                    }
                    //FB 2636 End

                }              

                m_vrmConfDAO.SaveOrUpdateList(conf);

               

                // cascade setup here
                // later all of this will be persisted during the set conference transaction.
                // 
                // First remove all cascades for this conference
                // Then build the list

                // FB 423 Added support for TANBERG bridge 
                List<ICriterion> cascadeCriterion = new List<ICriterion>();
                cascadeCriterion.Add(Expression.Eq("confid", Conf.ID));
                cascadeList = m_IconfCascade.GetByCriteria(cascadeCriterion);
                foreach (vrmConfCascade delCascade in cascadeList)
                    m_IconfCascade.Delete(delCascade);
                cascadeList.Clear();

                IMCUDao m_ImcuDao;
                m_ImcuDao = m_Hardware.GetMCUDao();

                List<int> bridgeIn = new List<int>();
                IDictionaryEnumerator iEnum = cascadeBridge.GetEnumerator();
                while (iEnum.MoveNext())
                {
                    bridgeIn.Add((int)iEnum.Key);
                }

                if (bridgeIn.Count > 1)
                {
                    
                    criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("confid", Conf.ID));
                    
                    if (Conf.instance > 0)
                        criterionList.Add(Expression.Eq("instanceid", Conf.instance));
                    
                    // 1159 You must use a copy of this list and NOT close the session (becuase of Lazy Loading)
                    List<vrmConference> conf_list = new List<vrmConference>();
                    conf_list = m_vrmConfDAO.GetByCriteria(criterionList);

                    cascadeCriterion = new List<ICriterion>();
                    cascadeCriterion.Add(Expression.In("BridgeID", bridgeIn));
                    m_ImcuDao.addOrderBy(Order.Asc("ChainPosition"));                    
                    MCUList = m_ImcuDao.GetByCriteria(cascadeCriterion);
                    int masterId = 0;
                    string masterName = string.Empty;
                    string maddress = string.Empty;
                    string mService = string.Empty;
                    // setup the cascade links
                    // assign default values for master and slaves

                    int defvideoprotocol = 1; // Default IP
                    int mconnectiontype = dialingOption.dialIn; // Master dials in
                    int sconnectiontype = dialingOption.dialOut; // Slave dials out
                    int mAddressType = 0;

                    foreach (vrmConference confObj in conf_list)
                    {
                        int link = 0;
                        IMCUIPServicesDao IPDao = m_Hardware.GetMCUIPServicesDao();
                        foreach (vrmMCU mcu in MCUList)
                        {
                            List<vrmMCUIPServices> IPServiceList = new List<vrmMCUIPServices>();
                            if (link == 0)
                            {
                                masterId = mcu.BridgeID;
                                masterName = mcu.BridgeName;

                                cascadeCriterion = new List<ICriterion>();
                                cascadeCriterion.Add(Expression.Eq("bridgeId", mcu.BridgeID));
                                IPServiceList = IPDao.GetByCriteria(cascadeCriterion);

                                if (IPServiceList.Count > 0)
                                {
                                    maddress = IPServiceList[0].ipAddress;
                                    mService = IPServiceList[0].ServiceName;
                                    mAddressType = IPServiceList[0].addressType;
                                }
                                else
                                {
                                    if (mcu.BridgeTypeId == vrmMCU.BRIDGEID_TANBERG)
                                    {
                                        maddress = mcu.BridgeAddress;
                                        mService = mcu.BridgeName;
                                        mAddressType = eptDetails.typeIP;
                                    }
                                }
                                link++;
                            }
                            else
                            {
                                cascadeCriterion = new List<ICriterion>();
                                cascadeCriterion.Add(Expression.Eq("bridgeId", mcu.BridgeID));
                                IPServiceList = IPDao.GetByCriteria(cascadeCriterion);
                                string saddress = string.Empty;
                                string sService = string.Empty;
                                int sAddressType = 0;

                                if (IPServiceList.Count > 0)
                                {
                                    saddress = IPServiceList[0].ipAddress;
                                    sService = IPServiceList[0].ServiceName;
                                    sAddressType = IPServiceList[0].addressType;
                                }
                                else
                                {
                                    if (mcu.BridgeTypeId == vrmMCU.BRIDGEID_TANBERG)
                                    {
                                        saddress = mcu.BridgeAddress;
                                        sService = mcu.BridgeName;
                                        sAddressType = eptDetails.typeIP;
                                    }
                                }
                                // now add one like each for master and slave
                                // first master
                                vrmConfCascade cascade = new vrmConfCascade();
                                cascade.confid = confObj.confid;
                                cascade.instanceid = confObj.instanceid;
                                cascade.cascadeLinkId = link;
                                cascade.cascadelinkname = masterName + " - " + mcu.BridgeName;
                                cascade.masterOrSlave = 1;
                                cascade.defVideoProtocol = defvideoprotocol;
                                cascade.connectionType = mconnectiontype;
                                cascade.ipisdnAddress = saddress;
                                cascade.bridgeId = masterId;
                                cascade.bridgeipisdnAddress = maddress;
                                cascade.connectStatus = 0;
                                cascade.layout = confObj.ConfAdvAvParams.videoLayoutID;
                                cascade.audioOrVideo = confObj.ConfAdvAvParams.mediaID;
                                /* *** Code added for FB 1475 QA Bug -Start *** */
                                if (confObj.conftype == vrmConfType.AudioOnly)
                                    cascade.audioOrVideo = 1; //FB 1744
                                else if (confObj.conftype == vrmConfType.AudioVideo)
                                    cascade.audioOrVideo = 2; //FB 1744
                                /* *** Code added for FB 1475 QA Bug -End *** */

                                cascade.mcuservicename = mService;
                                cascade.addresstype = mAddressType;
                                cascade.bridgeAddressType = sAddressType;
                                cascade.TerminalType = 4; //FB 2560
                                cascadeList.Add(cascade);
                                link++;
                                // now slave 
                                cascade = new vrmConfCascade();
                                cascade.confid = confObj.confid;
                                cascade.instanceid = confObj.instanceid;
                                cascade.cascadeLinkId = link;
                                cascade.cascadelinkname = mcu.BridgeName + " - " + masterName;
                                cascade.masterOrSlave = 0;
                                cascade.defVideoProtocol = defvideoprotocol;
                                cascade.connectionType = sconnectiontype;
                                cascade.ipisdnAddress = maddress;
                                cascade.bridgeId = mcu.BridgeID;
                                cascade.bridgeipisdnAddress = saddress;
                                cascade.connectStatus = 0;
                                cascade.layout = confObj.ConfAdvAvParams.videoLayoutID;
                                cascade.audioOrVideo = confObj.ConfAdvAvParams.mediaID;

                                /* *** Code added for FB 1475 QA Bug -Start *** */
                                if (confObj.conftype == vrmConfType.AudioOnly)
                                    cascade.audioOrVideo = 1; //FB 1744
                                else if (confObj.conftype == vrmConfType.AudioVideo)
                                    cascade.audioOrVideo = 2; //FB 1744
                                /* *** Code added for FB 1475 QA Bug -End *** */


                                cascade.mcuservicename = sService;
                                cascade.addresstype = sAddressType;
                                cascade.bridgeAddressType = mAddressType;
                                cascade.TerminalType = 4; //FB 2501 Call Monitoring
                                cascadeList.Add(cascade);
                                link++;
                            }
                        }
                    }
                }
                // now update everything.

                //FB 2486 Starts
                for (int count = 0; count < conf.Count; count++)
                {
                    ConferenceMsg = conf[count];
                    for (int i = 0; i < roomList.Count; i++)
                    {
                        if (roomList[i].isTextMsg == 1)
                            conf[count].isTextMsg = 1;
                    }

                    if (conf[count].isTextMsg == 0)
                    {
                        for (int k = 0; k < userList.Count; k++)
                        {
                            if (userList[k].isTextMsg == 1)
                                conf[count].isTextMsg = 1;
                        }
                    }

                    if (conf[count].isTextMsg == 1)
                    {
                        if (confMsgs != null)
                        {
                            if (!InsertConfMsgs(ref confMsgs, ref ConferenceMsg))
                            {
                                myvrmEx = new myVRMException(555);
                                obj.outXml = myvrmEx.FetchErrorMsg();
                                return false;
                            }
                        }
                    }
                }

                //FB 2486 Ends
                m_IconfRoom.SaveOrUpdateList(roomList);
                m_IconfUser.SaveOrUpdateList(userList);

	       		if (conf[0].conftype != vrmConfType.P2P)      // FB 1436 - Cascade links not to be added for P2P conferences	
	              m_IconfCascade.SaveOrUpdateList(cascadeList);

                SetConfBridges(conf, ConfBridgeid, ConfE164Bridgeid); //FB 2566

                // if this is a NON Video conference
                // set approvers and go
                // if conferneceApproval returns true then approvals still pending       
                if (conf[0].recuring == 1)
                    Isreccuring = true;

                bool sendIcal = false;// SendIcal for Plugins

                if (conf[0].IcalID != "" && conf[0].ConfMode == 2)// SendIcal for Plugins
                    sendIcal = true;

                //FB 1937
                if (conf[0].conftype != vrmConfType.P2P && conf[0].conftype != vrmConfType.RooomOnly)//FB 2350
                {
                    //FB 2344 - Starts
					for (int i = 0; i < conf.Count; i++)
                    {
                        for (int brdgcnt = 0; brdgcnt < bridgeIn.Count; brdgcnt++)
                        {
                            HF = new HardwareFactory(ref obj);

                            MCUcriterionList = new List<ICriterion>();
                            MCUcriterionList.Add(Expression.Eq("deleted", 0));
                            MCUcriterionList.Add(Expression.Eq("BridgeID", bridgeIn[brdgcnt]));

                            MCUList = m_vrmMCU.GetByCriteria(MCUcriterionList);

                            vMCUList = new List<vrmMCUResources>();
                            foreach (vrmMCU mMCU in MCUList)
                            {
                                vrmMCUResources vRes = new vrmMCUResources(mMCU.BridgeID);
                                if (HF.loadVRMResourceData(mMCU, ref vRes))
                                    vMCUList.Add(vRes);
                            }

                            HF.GetCompleteMCUusage(conf[i].confdate, conf[i].duration, conf[i].confid, conf[i].instanceid, ref vMCUList);
                            if (orgInfo.SendConfirmationEmail == 0) //FB 2470 
                            {
                                //FB 2472 Start
                                if (orgInfo.MCUAlert != 0)
                                {
                                    //FB 1938
                                    if (vMCUList[0].bridgeType == MCUType.CODIAN)
                                    {
                                        if (vMCUList[0].AUDIO.available < 0 || vMCUList[0].VIDEO.available < 0)
                                            m_emailFactory.sendBridgeBookedEmail(conf[i], vMCUList[0]);
                                    }
                                    else if (vMCUList[0].VIDEO.available < 0)
                                        m_emailFactory.sendBridgeBookedEmail(conf[i], vMCUList[0]);
                                }
                                //FB 2472 End
                            }
							//FB 2344 - End
                        }
                    }
                }
                //FB 1937


                if (ConferenceApproval(userid, ref conf, Conf))  //Recurrence Fixes for approval process
                {
                        if (orgInfo.SendIcal > 0 || sendIcal)//FB 1782
                        {
                            if (!minimalWS) //FB 2342
                            {
                                if (editFromWeb == 1 && orgInfo.SendConfirmationEmail == 0)//FB 2235 //FB 2470
                                    m_emailFactory.sendICALforParticipants(conf, "C", false, sendIcal, Isreccuring);// SendIcal for Plugins
                            }
                        }

                        m_emailFactory.SendConfirmationEmails(conf);    //FB 1830

                        if (sendIcal && orgInfo.PluginConfirmations > 0 && editFromWeb == 1 && orgInfo.SendConfirmationEmail == 0)//FB 2235//FB 2141 //FB 2470
                            m_emailFactory.SendConfirmationEmailsforPlugin(conf);
                    /* Following codes commented for FB 1830
                    //if (conf[0].conftype == 7)
                    //{
                    //    // update status and send out emails
                    //    m_emailFactory.emailLocations(conf);
                    //    //m_emailFactory.emailParticipants(conf);
                    //}
                    //else
                    //{
                    //    m_emailFactory.emailLocations(conf);
                    //    m_emailFactory.sendICALforCISCOEndpoints(conf, "C");  //Cisco ICAL FB 1602
                    //    //m_emailFactory.emailParticipants(conf);
                    //    if (conf[0].mcuList.Count > 0)
                    //    {
                    //        m_emailFactory.sendMCUAdminEmail(conf[0]);
                    //    }
                    //}
                    */
                }
                else
                {
                    if (((orgInfo.SendApprovalIcal > 0 && orgInfo.SendIcal > 0) || sendIcal) && conf[0].ConfOrigin != 0)//FB 2056
                    {
                        if (!minimalWS) //FB 2342
                        {
                            if (editFromWeb == 1 && orgInfo.SendConfirmationEmail==1 )//FB 2235 //FB 2470
                                m_emailFactory.sendICALforParticipants(conf, "C", true, sendIcal, Isreccuring);// SendIcal for Plugins
                        }
                    }
                }
                if (!minimalWS) //FB 2342
                {
                    if (conf[0].conftype != 7 && orgInfo.SendConfirmationEmail ==1) //FB 1675 //FB 2470
                        m_emailFactory.sendICALforCISCOEndpoints(conf, "C", false, -1);  //Cisco ICAL FB 1602
                }
                
                m_vrmConfDAO.SaveOrUpdateList(conf);
                //FB 2441 Starts
                if (conf[0].status == 0 || conf[0].status==6)
                {
                    List<ICriterion> bridgecriterionList = new List<ICriterion>();
                    List<vrmConfBridge> bridge = new List<vrmConfBridge>();
                    bridgecriterionList = new List<ICriterion>();

                    bridgecriterionList.Add(Expression.Eq("ConfID", conf[0].confid));
                    if (Conf.RecurMode == 0)
                        bridgecriterionList.Add(Expression.Eq("InstanceID", conf[0].instanceid));

                    bridge = m_IconfBridge.GetByCriteria(bridgecriterionList);
                    if (bridge.Count > 0)
                    {
                        obj.outXml = "<conferences>";
                        obj.outXml += "<conference>";
	                    if (Conf.instance == 0 && conf[0].recuring == 1) //whole recurrence
	                        obj.outXml += "<confID>" + conf[0].confid + "</confID>";
	                    else //single recurring instance and normal conf
	                        obj.outXml += "<confID>" + conf[0].confid + "," + conf[0].instanceid + "</confID>";

                        obj.outXml += "<confbrdige>";
                        if (bridge[0].Synchronous == 1 && ((orgInfo.NetworkCallLaunch == 2 && conf[0].Secured == 0) || (orgInfo.NetworkCallLaunch == conf[0].Secured) || orgInfo.NetworkCallLaunch == 0))
                            obj.outXml += "<isSynchronous>1</isSynchronous>";
                        else
                            obj.outXml += "<isSynchronous>0</isSynchronous>";
                        obj.outXml += "</confbrdige>";
                    }
                    obj.outXml += "</conference>";
                    obj.outXml += "<sCommand>SetConferenceOnMcu</sCommand>";
                    obj.outXml += "</conferences>";
                }

                //FB 2441 Ends                
               return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
        }
        #endregion

        #region vMCUcheck
        /// <summary>
        /// vMCUcheck
        /// check if MCU has available resorces if not then assight a wieght based on how many 
        /// cards are needed (port # is not considered
        /// </summary>
        /// <param name="res"></param>
        /// <returns></returns>
        private bool vMCUcheck(vrmMCUResources res)
        {
            bool iRet = false;
            try
            {
                int weight = 0;
                if(res.hasIP && res.IP.available < 0)
                    weight += 1;
                if (res.hasAUDIO && res.AUDIO.available < 0)
                    weight += 1;
                if (res.hasVIDEO && res.VIDEO.available < 0)
                    weight += 1;
                if (res.hasISDN && res.ISDN.available < 0)
                    weight += 1;
                if (res.hasMUX && res.MUX.available < 0)
                    weight += 1;
                if (res.hasMPI && res.MPI.available < 0)
                    weight += 1;
                if (weight == 0)
                    return true;
                else
                    res.weight = weight;
                
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
            return iRet;
        }
        #endregion

        #region SearchConference
        /// <summary>
        /// SearchConference
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SearchConference(ref vrmDataObject obj)
        {
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;
                node = xd.SelectSingleNode("//login/userID/userID");
                int userid = Int32.Parse(node.InnerXml.Trim());
                node = xd.SelectSingleNode("//login/confSearch/confInfo/confDate/type");
                int type = Int32.Parse(node.InnerXml.Trim());
                node = xd.SelectSingleNode("//login/confSearch/confInfo/confName");
                string confName = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//login/confSearch/confInfo/confUniqueID");
                string confUniqueID = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//login/confSearch/confInfo/confHost");
                string confHost = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//login/confSearch/confInfo/pending");
                string pending = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//login/confSearch/confInfo/public");
                string isPublic = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//login/confSearch/confResource");
                string confResource = node.InnerXml.Trim();
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
        }
        #endregion

        #region GetConferenceEndpoint
        /// <summary>
        /// GetConferenceEndpoint
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetConferenceEndpoint(ref vrmDataObject obj)
        {
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;
                node = xd.SelectSingleNode("//GetConferenceEndpoint/UserID");
                int userid = Int32.Parse(node.InnerXml.Trim());
                node = xd.SelectSingleNode("//GetConferenceEndpoint/ConfID");
                string confid = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//GetConferenceEndpoint/Type");
                string type = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//GetConferenceEndpoint/EndpointID");
                int id = Int32.Parse(node.InnerXml.Trim());

                obj.outXml += "<GetConferenceEndpoint>";
                obj.outXml += "<ConfID>" + confid + "</ConfID>";

                CConfID Conf = new CConfID(confid);
                if (Conf.instance < 1)
                    Conf.instance = 1;

                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("confid", Conf.ID));
                criterionList.Add(Expression.Eq("instanceid", Conf.instance));


                if (type == "R")
                {
                    criterionList.Add(Expression.Eq("roomId", id));
                    List<vrmConfRoom> rList = m_IconfRoom.GetByCriteria(criterionList);
                    vrmConfRoom cr = rList[0];
                    obj.outXml += EptRoomXml(cr);
                }
                else if (type == "C")
                {
                    criterionList.Add(Expression.Eq("cascadeLinkId", id));
                    List<vrmConfCascade> cList = m_IconfCascade.GetByCriteria(criterionList);
                    vrmConfCascade cas = cList[0];
                    obj.outXml += EptCascadeXml(cas);
                }
                else
                {
                    criterionList.Add(Expression.Eq("userid", id));
                    List<vrmConfUser> uList = m_IconfUser.GetByCriteria(criterionList);
                    vrmConfUser usr = uList[0];
                    if (usr.invitee == 1)
                        obj.outXml += EptUserXml(usr);
                }
                obj.outXml += "</GetConferenceEndpoint>";

                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = string.Empty;
                return false;
            }
        }
        #endregion

        #region SetConferenceEndpoint
        /// <summary>
        /// SetConferenceEndpoint
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SetConferenceEndpoint(ref vrmDataObject obj)
        {
            try
            {
                int id = 0;

                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                //FB 1552 start
                int usrOrgid = 11;
                if (xd.SelectSingleNode("//SetConferenceEndpoint/organizationID") != null)
                    int.TryParse(xd.SelectSingleNode("//SetConferenceEndpoint/organizationID").InnerText.Trim(), out usrOrgid);
                //FB 1552 end
                node = xd.SelectSingleNode("//SetConferenceEndpoint/ConfID");
                string confid = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//SetConferenceEndpoint/Endpoint/EndpointID");
                string EndpointID = node.InnerXml.Trim();
                if (EndpointID.ToLower() != "new")
                    id = Int32.Parse(EndpointID);
                node = xd.SelectSingleNode("//SetConferenceEndpoint/Endpoint/EndpointName");
                string EndpointName = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//SetConferenceEndpoint/Endpoint/EndpointLastName");
                string EndpointLastName = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//SetConferenceEndpoint/Endpoint/EndpointEmail");
                string EndpointEmail = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//SetConferenceEndpoint/Endpoint/ProfileID");
                string ProfileID = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//SetConferenceEndpoint/Endpoint/Type");
                string type = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//SetConferenceEndpoint/Endpoint/EncryptionPreferred");
                string EncryptionPreferred = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//SetConferenceEndpoint/Endpoint/AddressType");
                string AddressType = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//SetConferenceEndpoint/Endpoint/Address");
                string Address = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//SetConferenceEndpoint/Endpoint/URL");
                string URL = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//SetConferenceEndpoint/Endpoint/IsOutside");
                string IsOutside = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//SetConferenceEndpoint/Endpoint/ConnectionType");
                string connectionType = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//SetConferenceEndpoint/Endpoint/VideoEquipment");
                string VideoEquipment = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//SetConferenceEndpoint/Endpoint/LineRate");
                string Bandwidth = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//SetConferenceEndpoint/Endpoint/Bridge");
                string BridgeID = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//SetConferenceEndpoint/Endpoint/Protocol");
                string Protocol = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//SetConferenceEndpoint/Endpoint/Connection");
                string Connection = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//SetConferenceEndpoint/Endpoint/BridgeAddress");
                string BridgeAddress = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//SetConferenceEndpoint/Endpoint/BridgeAddressType");
                string BridgeAddressType = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//SetConferenceEndpoint/Endpoint/ExchangeID"); //Cisco fix
                string exchangeID = node.InnerXml.Trim();
                //API Port starts..
                node = xd.SelectSingleNode("//SetConferenceEndpoint/Endpoint/ApiPortno");
                string ApiPortno = node.InnerXml.Trim();
                //API Port Ends..
                //<EndpointID></EndpointID>
                //FB 2441 - Starts
				int Mute = 0;
                node = xd.SelectSingleNode("//SetConferenceEndpoint/Endpoint/IsMute");
                int.TryParse(node.InnerText.Trim(), out Mute);
                //FB 2441 - End
                CConfID Conf = new CConfID(confid);
                if (Conf.instance < 1)
                    Conf.instance = 1;

                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("confid", Conf.ID));
                criterionList.Add(Expression.Eq("instanceid", Conf.instance));

                vrmConference bvconf = m_vrmConfDAO.GetByConfId(Conf.ID, Conf.instance); //FB Issue 1501

                // update user
                if (type == "U")
                {
                    // add as new guest
                    vrmConfUser confUser = new vrmConfUser();
                    vrmGuestUser guest = new vrmGuestUser();
                    if (id == 0)
                    {
                        confUser.invitee = 1; // set to external
                        id = m_usrDAO.GetID();

                        guest.userid = id;
                        guest.LastName = EndpointLastName;
                        guest.FirstName = EndpointName;
                        guest.Email = EndpointEmail;
                        guest.companyId = usrOrgid; //FB 1552
                        m_vrmGuestDAO.Save(guest);
                        confUser.userid = guest.userid;
                        confUser.TerminalType = 3; //FB 2501 Call Monitoring
                        confUser.Stream = "";// FB 2501 Dec5
                        confUser.layout = 1;// FB 2553
                    }
                    else
                    {
                        criterionList.Add(Expression.Eq("userid", id));
                        List<vrmConfUser> cuList = m_IconfUser.GetByCriteria(criterionList);
                        if (cuList.Count == 0)
                        {
                            //FB 1881 start
                            //obj.outXml = myVRMException.toXml("Error cannot retrieve ConfUser; ID:" + id.ToString());
                            m_log.Error("Error cannot retrieve ConfUser; ID:" + id.ToString());
                            obj.outXml = "";
                            //FB 1881 end
                            return false;
                        }
                        else
                            confUser = cuList[0];

                    }
                    confUser.confid = Conf.ID;
                    confUser.instanceid = Conf.instance;
                    confUser.addressType = Int32.Parse(AddressType);
                    confUser.ipisdnaddress = Address;
                    confUser.outsideNetwork = Int32.Parse(IsOutside);
                    confUser.mute = Mute;//FB 2441
                    confUser.connectionType = Int32.Parse(connectionType);
                    confUser.videoEquipment = Int32.Parse(VideoEquipment);
                    confUser.ExchangeID = exchangeID; //Cisco
                    confUser.defLineRate = Int32.Parse(Bandwidth);
                    confUser.bridgeid = Int32.Parse(BridgeID);
                    confUser.connect2 = Int32.Parse(Connection);
                    //Code added for FB Issue 1175 Start
                    //Code changed for FB 1744 - START
                    
                    //if (confUser.connect2 == 3)
                    //    confUser.audioOrVideo = 1;
                    //else
                    //    confUser.audioOrVideo = 0;
                    confUser.audioOrVideo = confUser.connect2;
                    
                    //Code changed for FB 1744 - END
                    //Code added for FB Issue 1175 End

                    //Code added - FB Issue 1501 Start
                    //confUser.status = vrmConfStatus.Pending; // auto accept
                    confUser.status = 0;
                    if (bvconf != null)
                    {
                        if (bvconf.immediate == 1)
                            confUser.status = 1;
                    }
                    //Code added - FB Issue 1501 End

                    confUser.bridgeAddressType = Int32.Parse(BridgeAddressType);
                    confUser.bridgeIPISDNAddress = BridgeAddress;
                    confUser.defVideoProtocol = Int32.Parse(Protocol);
                    confUser.connectStatus = 1;
                    confUser.ApiPortNo = int.Parse(ApiPortno);//API Port No...
                    confUser.endptURL = URL;//During API Port No...
                    GetEndPointService eps = new GetEndPointService();
                    eps.sType = "U";
                    eps.id = id;
                    eps.bridgeid = confUser.bridgeid;
                    eps.addressType = confUser.addressType;
                    eps.outsideNetwork = confUser.outsideNetwork;
                    if (eps.addressType != eptDetails.typeISDN)
                    {
                        vrmMCU MCU = m_vrmMCU.GetById(confUser.bridgeid);
                        if (MCU.MCUType.bridgeInterfaceId == vrmMCU.BRIDGEID_TANBERG)
                        {
                            eps.mcuServiceName = MCU.BridgeName;
                            eps.bridgeAddressType = eptDetails.typeIP;
                            eps.bridgeIPISDNAddress = MCU.BridgeAddress;
                        }
                        else
                        {
                            if (!GetEndPointService(ref eps))
                            {
                                m_log.Error("WARNING cannot retrieve IPServices, Bridgeid = " + confUser.bridgeid.ToString());
                                m_log.Error("for conference confid  = " + Conf.ID + "/" + Conf.instance);
                                m_log.Error("Continuing ....");
                                //valid = false;
                            }
                        }
                    }
                    else // isdn
                    {

                        if (!GetEndPointService(ref eps))
                        {
                            if (eps.addressType == eptDetails.typeMPI)
                            {
                                m_log.Error("WARNING cannot retrieve MPI Services, Bridgeid = " + confUser.bridgeid.ToString());
                                m_log.Error("for conference confid  = " + Conf.ID + "/" + Conf.instance);
                                m_log.Error("Continuing ....");
                            }
                            else
                            {
                                m_log.Error("WARNING cannot retrieve ISDNServices, Bridgeid = " + confUser.bridgeid.ToString());
                                m_log.Error("for conference confid  = " + Conf.ID + "/" + Conf.instance);
                                m_log.Error("Continuing ....");
                            }
                            //valid = false;
                        }
                    }
                    confUser.confuId = bvconf.confnumname;//FB 2501 Call Monitoring
                    m_IconfUser.SaveOrUpdate(confUser);
                }
                /* *** Code added for FB 1475 -Start *** */
                else if(type == "4") // update Cascade
                {
                    criterionList.Add(Expression.Eq("cascadeLinkId", id));
                    List<vrmConfCascade> cList = m_IconfCascade.GetByCriteria(criterionList);

                    if (cList.Count == 0)
                    {
                        //FB 1881 start
                        //obj.outXml = myVRMException.toXml("Error cannot retrieve Cascade End Point; ID:" + id.ToString());
                        m_log.Error("Error cannot retrieve Cascade End Point; ID:" + id.ToString());
                        obj.outXml = "";
                        //FB 1881 end
                        return false;

                    }
                    vrmConfCascade cas = cList[0];

                    cas.addresstype = Int32.Parse(AddressType);
                    cas.ipisdnAddress = Address;
                    cas.outsidenetwork = Int32.Parse(IsOutside);
                    cas.mute = Mute; //FB 2441
                    cas.connectionType = Int32.Parse(connectionType);
                    cas.deflinerate = Int32.Parse(Bandwidth);
                    cas.bridgeId = Int32.Parse(BridgeID);
                    cas.audioOrVideo = Int32.Parse(Connection);
                    cas.bridgeAddressType = Int32.Parse(BridgeAddressType);
                    cas.bridgeipisdnAddress = BridgeAddress;
                    cas.defVideoProtocol = Int32.Parse(Protocol);
                    cas.connectStatus = 1;
                    m_IconfCascade.SaveOrUpdate(cas);
                }
                /* *** Code added for FB 1475 -End *** */
                else // update room
                {
                    criterionList.Add(Expression.Eq("roomId", id));
                    List<vrmConfRoom> crList = m_IconfRoom.GetByCriteria(criterionList);
                    if (crList.Count == 0)
                    {
                        //FB 1881 start
                        //obj.outXml = myVRMException.toXml("Error cannot retrieve ConfRoom; ID:" + id.ToString());
                        m_log.Error("Error cannot retrieve ConfRoom; ID:" + id.ToString());
                        obj.outXml = "";
                        //FB 1881 end
                        return false;

                    }
                    vrmConfRoom confRoom = crList[0];

                    confRoom.addressType = Int32.Parse(AddressType);
                    confRoom.ipisdnaddress = Address;
                    confRoom.outsideNetwork = Int32.Parse(IsOutside);
                    confRoom.mute = Mute; //FB 2441
                    confRoom.connectionType = Int32.Parse(connectionType);
                    confRoom.defLineRate = Int32.Parse(Bandwidth);
                    confRoom.bridgeid = Int32.Parse(BridgeID);
                    //confRoom.connect2 = Int32.Parse(Connection);//Code changed for FB 1475
                    confRoom.audioorvideo = Int32.Parse(Connection);//Code changed for FB 1475
                    confRoom.bridgeAddressType = Int32.Parse(BridgeAddressType);
                    confRoom.bridgeIPISDNAddress = BridgeAddress;
                    confRoom.defVideoProtocol = Int32.Parse(Protocol);
                    confRoom.connectStatus = 1;
                    confRoom.ApiPortNo = int.Parse(ApiPortno);//API Port No...
                    confRoom.endptURL = URL;//During API Port No...
                    //FB 2610 Starts
                    vrmMCU confMCU = m_vrmMCU.GetById(confRoom.bridgeid);
                    confRoom.BridgeExtNo = confMCU.BridgeExtNo; 
                    //FB 2610 Ends
                    GetEndPointService eps = new GetEndPointService();
                    eps.sType = "R";
                    eps.id = id;
                    eps.bridgeid = confRoom.bridgeid;
                    eps.addressType = confRoom.addressType;
                    eps.outsideNetwork = confRoom.outsideNetwork;
                    if (eps.addressType != eptDetails.typeISDN)
                    {
                        vrmMCU MCU = m_vrmMCU.GetById(confRoom.bridgeid);
                        if (MCU.MCUType.bridgeInterfaceId == vrmMCU.BRIDGEID_TANBERG)
                        {
                            eps.mcuServiceName = MCU.BridgeName;
                            eps.bridgeAddressType = eptDetails.typeIP;
                            eps.bridgeIPISDNAddress = MCU.BridgeAddress;
                        }
                        else
                        {
                            if (!GetEndPointService(ref eps))
                            {
                                m_log.Error("Error cannot retrieve IPServices, Bridgeid = " + confRoom.bridgeid.ToString());
                                m_log.Error("for conference confid  = " + Conf.ID + "/" + Conf.instance);
                                m_log.Error("Continuing ....");
                            }
                        }
                    }
                    else // isdn
                    {

                        if (!GetEndPointService(ref eps))
                        {
                            if (eps.addressType == eptDetails.typeMPI)
                            {
                                m_log.Error("Error cannot retrieve MPI Services, Bridgeid = " + confRoom.bridgeid.ToString());
                                m_log.Error("for conference confid  = " + Conf.ID + "/" + Conf.instance);
                                m_log.Error("Continuing ....");
                            }
                            else
                            {
                                m_log.Error("Error cannot retrieve IPDNServices, Bridgeid = " + confRoom.bridgeid.ToString());
                                m_log.Error("for conference confid  = " + Conf.ID + "/" + Conf.instance);
                                m_log.Error("Continuing ....");
                            }
                        }
                    }
               
                    m_IconfRoom.SaveOrUpdate(confRoom);
                }
                obj.outXml += "<SetConferenceEndpoint>";
                obj.outXml += "<EndpointID>" + id.ToString() + "</EndpointID>";
                obj.outXml += "</SetConferenceEndpoint>";

                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);
                obj.outXml = ""; //FB 1881
                return false;
            }
        }
        #endregion

        #region EptRoomXml
        /// <summary>
        /// EptRoomXml
        /// simple fromatting....
        /// </summary>
        /// <param name="cr"></param>
        /// <returns></returns>
        private string EptRoomXml(vrmConfRoom cr)
        {
            // get the endpoint. If the above tags are blank use ep valuse instead. 
            List<ICriterion> criterionListEP = new List<ICriterion>();
            criterionListEP.Add(Expression.Eq("endpointid", cr.endpointId));
            criterionListEP.Add(Expression.Eq("profileId", cr.profileId));
            criterionListEP.Add(Expression.Eq("deleted", 0));
            List<vrmEndPoint> epList = m_vrmEpt.GetByCriteria(criterionListEP);

            // it HAS to be here
            if (epList.Count < 1)
            {
                //FB 1881 start
                //myVRMException e = new myVRMException("Error cannot retrieve endpoint " + cr.endpointId.ToString() + " / " + cr.profileId.ToString());
                m_log.Error("Error cannot retrieve endpoint " + cr.endpointId.ToString() + " / " + cr.profileId.ToString());
                myVRMException e = new myVRMException(200);
                //FB 1881 end
                throw e;
            }

            vrmEndPoint ep = epList[0];
            string outXml = string.Empty;
            //            vrmMCU mcu = m_vrmMCU.GetById(cr.bridgeid);

            outXml += "<Endpoint>";
            outXml += "<Type>R</Type>";
            outXml += "<ID>" + cr.roomId.ToString() + "</ID>";
            outXml += "<EndpointID>" + cr.endpointId.ToString() + "</EndpointID>";
            outXml += "<ProfileID>" + cr.profileId.ToString() + "</ProfileID>";
            outXml += "<EndpointName>" + ep.name + "</EndpointName>";
            outXml += "<Name>" + cr.Room.Name + "</Name>";
            outXml += "<BridgeID>" + cr.bridgeid.ToString() + "</BridgeID>";
            outXml += "<AddressType>" + cr.addressType.ToString() + "</AddressType>";
            outXml += "<Address>" + cr.ipisdnaddress + "</Address>";
            outXml += "<VideoEquipment>" + ep.videoequipmentid.ToString() + "</VideoEquipment>";
            outXml += "<connectionType>" + cr.connectionType.ToString() + "</connectionType>";
            outXml += "<Bandwidth>" + cr.defLineRate + "</Bandwidth>";
            outXml += "<IsOutside>" + cr.outsideNetwork.ToString() + "</IsOutside>";
            outXml += "<DefaultProtocol>" + cr.defVideoProtocol.ToString() + "</DefaultProtocol>";

            //Code Modified For FB 1422 -Start
            //outXml += "<Connection>" + cr.connect2.ToString() + "</Connection>";
            outXml += "<Connection>" + cr.audioorvideo.ToString() + "</Connection>";
            outXml += "<Connect2>" + cr.connect2.ToString() + "</Connect2>";
            //Code Modified For FB 1422 -End
            outXml += "<ApiPortno>" + ep.ApiPortNo.ToString() + "</ApiPortno>";//API Port...
            outXml += "<BridgeAddress>" + cr.bridgeIPISDNAddress + "</BridgeAddress>";
            outXml += "<BridgeAddressType>" + cr.bridgeAddressType.ToString() + "</BridgeAddressType>";
            outXml += "<BridgePrefix>" + cr.prefix + "</BridgePrefix>";
            outXml += "<URL>" + ep.endptURL + "</URL>";//During API Port
            outXml += "<ApiPortNo>" + ep.ApiPortNo.ToString() + "</ApiPortNo>";//API Port...
            outXml += "<IsLecturer>" + cr.isLecturer.ToString() + "</IsLecturer>";
            outXml += "<ExchangeID>"+ ep.ExchangeID +"</ExchangeID>"; //Cisco Telepresence fix
            outXml += "<EndPointStatus>" + cr.OnlineStatus + "</EndPointStatus>"; //FB 1650 Endpoint Status issue
            outXml += "<remoteEndpoint>" + cr.remoteEndPointIP + "</remoteEndpoint>";//Blue point status...
            //FB 2400 Start
            outXml += "<isTelePresence>" + ep.isTelePresence + "</isTelePresence>";
            outXml += "<MultiCodec>";

            if (cr.MultiCodecAddress != null)
            {
                String[] multiCodec = cr.MultiCodecAddress.Split('�');
                for (int i = 0; i < multiCodec.Length; i++)
                {
                    if (multiCodec[i].Trim() != "")
                        outXml += "<Address>" + multiCodec[i].Trim() + "</Address>";
                }
            }
            outXml += "</MultiCodec>";
            //FB 2400 End
            outXml += "<EptOnlineStatus>" + ((EndpointStatus)ep.EptOnlineStatus) + "</EptOnlineStatus>"; //FB 2501 EM7
            outXml += "<Secured>" + ep.Secured + "</Secured>"; //FB 2595
            outXml += "</Endpoint>";
            return outXml;
        }
        #endregion

        #region EptCascadeXml
        /// <summary>
        /// EptCascadeXml
        /// </summary>
        /// <param name="cas"></param>
        /// <returns></returns>
        private string EptCascadeXml(vrmConfCascade cas)
        {

            string outXml = string.Empty;
            outXml += "<Endpoint>";
            outXml += "<Type>C</Type>";
            outXml += "<ID>" + cas.uId.ToString() + "</ID>"; //FB 1650
            outXml += "<CascadeLinkId>" + cas.cascadeLinkId.ToString() + "</CascadeLinkId>"; //FB 1650
            //outXml += "<ID>" + cas.cascadeLinkId.ToString() + "</ID>";//Code changed for FB 1475
            outXml += "<EndpointID>" + cas.uId.ToString() + "</EndpointID>";//Code changed for FB 1475
            outXml += "<ProfileID></ProfileID>";
            outXml += "<EndpointName></EndpointName>";
            outXml += "<Name>" + cas.cascadelinkname + "</Name>";
            outXml += "<BridgeID>" + cas.bridgeId.ToString() + "</BridgeID>";
            outXml += "<AddressType>" + cas.addresstype.ToString() + "</AddressType>";
            outXml += "<Address>" + cas.ipisdnAddress + "</Address>";
            outXml += "<ApiPortno></ApiPortno>";//API Port... 
            //outXml += "<VideoEquipment>" + cas.audioOrVideo.ToString() + "</VideoEquipment>";//Code commented for FB 1475
            outXml += "<VideoEquipment></VideoEquipment>";//Code commented for FB 1475
            outXml += "<connectionType>" + cas.connectionType.ToString() + "</connectionType>";
            outXml += "<Bandwidth>" + cas.deflinerate.ToString() + "</Bandwidth>";
            outXml += "<IsOutside>" + cas.outsidenetwork.ToString() + "</IsOutside>";
            outXml += "<DefaultProtocol>" + cas.defVideoProtocol.ToString() + "</DefaultProtocol>";
            //outXml += "<Connection>" + cas.connectStatus.ToString() + "</Connection>";//Code commented for FB 1475
            outXml += "<Connection>" + cas.audioOrVideo.ToString() + "</Connection>";//Code added for FB 1475
            outXml += "<BridgeAddress>" + cas.bridgeipisdnAddress + "</BridgeAddress>";
            outXml += "<BridgeAddressType>" + cas.bridgeAddressType.ToString() + "</BridgeAddressType>";
            outXml += "<BridgePrefix>" + cas.prefix + "</BridgePrefix>";
            outXml += "<URL></URL>";//During API Port..
            outXml += "<EndPointStatus>"+ cas.OnlineStatus +"</EndPointStatus>"; //FB 1650 Endpoint Status issue
            outXml += "<remoteEndpoint>" + cas.remoteEndPointIP + "</remoteEndpoint>";//Blue point status...
            outXml += "<MultiCodec></MultiCodec>"; //FB 2400
            outXml += "<isTelePresence>0</isTelePresence>"; //FB 2400
            outXml += "<EptOnlineStatus>1</EptOnlineStatus>"; //FB 2501 EM7
            outXml += "</Endpoint>";
            return outXml;
        }
        #endregion

        #region EptUserXml
        /// <summary>
        /// EptUserXml
        /// </summary>
        /// <param name="usr"></param>
        /// <returns></returns>
        private string EptUserXml(vrmConfUser usr)
        {
            List<ICriterion> criterionList = new List<ICriterion>();
            criterionList.Add(Expression.Eq("userid", usr.userid));
            List<vrmUser> ul = m_vrmUserDAO.GetByCriteria(criterionList);

            string name = string.Empty;

            //Code added for FB iisue 1175 -- Start
            string eptEmail = string.Empty;
            //Code added for FB iisue 1175 -- End
            int eptId = 0;//Audio Add On...
            if (ul.Count == 0)
            {
                List<vrmGuestUser> gl = m_vrmGuestDAO.GetByCriteria(criterionList);
                vrmGuestUser guest = gl[0];
                //Code added for FB 1688 Starts
                //name = guest.LastName + "++" + guest.FirstName;  //FB 1640

                //FB 2528 - Starts
                //name = guest.FirstName + "++" + guest.LastName;
                name = guest.FirstName;
                if (guest.LastName != "")
                    name += "++" + guest.LastName;
                //FB 2528 - End

                //Code added for FB 1688 Ends
                //Code added for FB iisue 1175
                eptEmail = guest.Email;
                //Code added for FB iisue 1175
            }
            else
            {
                vrmUser theUser = ul[0];
                //Code added for FB 1688 Starts
                //name = theUser.LastName + "++" + theUser.FirstName;  //FB 1640

                //FB 2528 - Starts
                //name = theUser.FirstName + "++" + theUser.LastName;
                name = theUser.FirstName;
                if (theUser.LastName != "")
                    name += "++" + theUser.LastName;
                //FB 2528 - End

                eptId = theUser.endpointId;//Audio Add On..
                //Code added for FB 1688 Ends                
            }
            
            string outXml = string.Empty;
            outXml += "<Endpoint>";
            outXml += "<Type>U</Type>";
            outXml += "<ID>" + usr.userid.ToString() + "</ID>";
            outXml += "<EndpointID>" + eptId + "</EndpointID>";
            outXml += "<ProfileID></ProfileID>";
            outXml += "<EndpointName>" + name + "</EndpointName>";  //FB 1640
            outXml += "<Name>" + name + "</Name>";
            //Code added for FB iisue 1175 --Start
            outXml += "<EndPointEmail>" + eptEmail + "</EndPointEmail>";
            //Code added for FB iisue 1175 -- End
            outXml += "<BridgeID>" + usr.bridgeid.ToString() + "</BridgeID>";
            outXml += "<AddressType>" + usr.addressType.ToString() + "</AddressType>";
            outXml += "<MultiCodec></MultiCodec>"; //FB 2400
            outXml += "<Address>" + usr.ipisdnaddress.Trim() + "</Address>"; //FB 1734
            outXml += "<VideoEquipment>" + usr.videoEquipment.ToString() + "</VideoEquipment>";
            outXml += "<connectionType>" + usr.connectionType.ToString() + "</connectionType>";
            outXml += "<Bandwidth>" + usr.defLineRate.ToString() + "</Bandwidth>";
            outXml += "<IsOutside>" + usr.outsideNetwork.ToString() + "</IsOutside>";
            outXml += "<DefaultProtocol>" + usr.defVideoProtocol.ToString() + "</DefaultProtocol>";
            outXml += "<ApiPortno>" + usr.ApiPortNo.ToString() + "</ApiPortno>"; //API Port...
            //Code Modified For FB 1422 -Start
            //outXml += "<Connection>" + usr.connect2.ToString() + "</Connection>";
            outXml += "<Connection>" + usr.audioOrVideo.ToString() + "</Connection>";
            outXml += "<Connect2>" + usr.connect2.ToString() + "</Connect2>";
            //Code Modified For FB 1422 -End
            
            outXml += "<BridgeAddress>" + usr.bridgeIPISDNAddress + "</BridgeAddress>";
            outXml += "<BridgeAddressType>" + usr.bridgeAddressType.ToString() + "</BridgeAddressType>";
            outXml += "<BridgePrefix>" + usr.prefix + "</BridgePrefix>";
            outXml += "<IsLecturer>" + usr.isLecturer.ToString() + "</IsLecturer>";            
            outXml += "<URL>"+usr.endptURL+"</URL>";
            outXml += "<ExchangeID>" + usr.ExchangeID + "</ExchangeID>"; //Cisco Telepresence fix
            outXml += "<EndPointStatus>" + usr.OnlineStatus + "</EndPointStatus>"; //FB 1650 Endpoint Status issue
            outXml += "<remoteEndpoint>" + usr.remoteEndPointIP + "</remoteEndpoint>";//Blue point status...
            outXml += "<isTelePresence>0</isTelePresence>"; //FB 2400
            outXml += "<EptOnlineStatus>1</EptOnlineStatus>"; //FB 2501 EM7
            outXml += "</Endpoint>";
            return outXml;
        }
        #endregion

        #region GetEndPointService
        /// <summary>
        /// Summary Retrieve IP / ISDN services.
        /// </summary>
        private bool GetEndPointService(ref GetEndPointService eps)
        {
            if (eps.addressType == eptDetails.typeISDN)
                return GetISDNService(ref eps);
            else
                if (eps.addressType == eptDetails.typeMPI)
                    return GetMPIService(ref eps);
                else
                    return GetIPService(ref eps);
        }
        #endregion

        #region GetIPService
        /// <summary>
        /// GetIPService
        /// </summary>
        /// <param name="eps"></param>
        /// <returns></returns>
        private bool GetIPService(ref GetEndPointService eps)
        {
           
            //
            // logic for ip services(09/25/08)
            //
            //  see document networkusage.docx in vss\UI-BR\businessLayer\Doc
            //          
     
            try
            {
                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("bridgeId", eps.bridgeid));
                IMCUIPServicesDao IPDao = m_Hardware.GetMCUIPServicesDao();
                IList<vrmMCUIPServices> IPServiceList = IPDao.GetByCriteria(criterionList);

                if (IPServiceList.Count <= 0) //FB 1650 - Endpoint status issue
                    return false;

                vrmMCUIPServices theService = IPServiceList[0];
                bool found = false;
                // as the above document states;
                // if  a service has the same useage and network access we are done
                // if the usage is the same and the network access is both that is preverable to 
                // usage of both and network access of both. 
                // so if choice two (same usage and both network access) exists it is chosen until a better choice
                // (ie both match) or there are no more services. 
                // if neither is found then the last service that supplies both is chosen. 
                // NOTE as of now this only applies to POLYCOM (FB 1036)
                foreach(vrmMCUIPServices ipService in IPServiceList)
                {
                    if (eps.connect2 == ipService.usage && eps.outsideNetwork == ipService.networkAccess)
                    {
                        theService = ipService;
                        break;
                    }
                    else
                    {
                        if (ipService.usage == eps.connect2)
                        {
                            if (ipService.networkAccess == eptDetails.isBoth)
                            {
                                theService = ipService;
                                found = true;
                            }
                            else
                            {
                                if (ipService.networkAccess == eptDetails.isBoth &&
                                    ipService.usage == eptDetails.isBoth && !found)
                                {
                                    theService = ipService;
                                }
                            }
                        }
                        else
                        {
                            if (ipService.networkAccess == eps.outsideNetwork)
                            {
                                if (ipService.usage == eptDetails.isBoth)
                                {
                                    theService = ipService;
                                    found = true;
                                }
                                else
                                {
                                    if (ipService.networkAccess == eptDetails.isBoth &&
                                        ipService.usage == eptDetails.isBoth && !found)
                                    {
                                        theService = ipService;
                                    }
                                }
                            }

                            else
                            {
                                if (ipService.networkAccess == eptDetails.isBoth &&
                                   ipService.usage == eptDetails.isBoth && !found)
                                {
                                    theService = ipService;
                                }
                            }
                        }
                    }    
                }
                eps.bridgeIPISDNAddress = theService.ipAddress;
                eps.mcuServiceName = theService.ServiceName;
                return true;
                
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }

        }
        #endregion

        #region GetMPIService
        /// <summary>
        /// GetMPIService
        /// </summary>
        /// <param name="eps"></param>
        /// <returns></returns>
        /// MPI service (FB 427)
        private bool GetMPIService(ref GetEndPointService eps)
        {
            //
            // MPI service name must match address of endpoint
            //
            try
            {
                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("bridgeId", eps.bridgeid));
                IMCUMPIServicesDao IPDao = m_Hardware.GetMCUMPIServicesDao();
                IList<vrmMCUMPIServices> MPIServiceList = IPDao.GetByCriteria(criterionList);

                if (MPIServiceList.Count > 0)
                {
                    foreach(vrmMCUMPIServices mpi in MPIServiceList)
                    {
                        if(mpi.ipAddress.ToUpper() == eps.address.ToUpper())
                        {
                            eps.bridgeIPISDNAddress = mpi.ipAddress;
                            eps.mcuServiceName = mpi.ServiceName;
                            return true;
                        }
                    }
                }
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }

        }
        #endregion

        #region GetISDNService
        /// <summary>
        /// GetISDNService
        /// </summary>
        /// <param name="eps"></param>
        /// <returns></returns>
        private bool GetISDNService(ref GetEndPointService eps)
        {
            //
            // ISDN is not so simple.
            // AN isdn services collections is identified as bridgeid, networkaccess (public, private or both) and
            // usage (audio, video or both). There can be more than one service in the collection. 
            // After retrieving the collection you have to cycle through the ISDN numbers from lowest to highest 
            // (or visa,versa depending on the RangeSortOrder) until you find an available isdn number. 
            // ISDN number availability is determined by usage (ie find the first number not bieng used during that
            // day/time.
            //
            try
            {
                int networkAccess = eps.outsideNetwork;
                if (networkAccess == 0)
                    networkAccess = eptDetails.isPrivate;
                else
                    networkAccess = eptDetails.isPublic;
				//NG fixes
                m_log.Error("Network access is accordingly : " + networkAccess.ToString());
                m_log.Error("Network Usage is accordingly : " + eps.usage.ToString());

                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("BridgeId", eps.bridgeid));

                //NG fixes
                criterionList.Add(Expression.Or(Expression.Eq("networkAccess", eps.outsideNetwork),
                    Expression.Eq("networkAccess", eptDetails.isBoth)));
                criterionList.Add(Expression.Or(Expression.Eq("usage", eps.usage),
                    Expression.Eq("usage", eptDetails.isBoth)));

                IMCUISDNServicesDao ISDNDao = m_Hardware.GetMCUISDNServicesDao();

                // if you pass a did of -1 then no need to search for a number it was suppiled 
                IList<vrmMCUISDNServices> ISDNList = ISDNDao.GetByCriteria(criterionList);
                eps.bridgeIPISDNAddress = "0";

                // if no available ISDN number is found a did of 0 will be returned.
                // if singleDialin is on (1) then assingn one ISDN number to everone. 
                //
                if (eps.singleDialin == 1)
                {
                    // get the first value and return. 
                    foreach (DictionaryEntry de in m_ISDNusedNumber)
                    {
                        eps.bridgeIPISDNAddress = de.Key.ToString();
                        return true;
                    }
                }

                // check the static DID. If it is available then assign it
                // don't take it out of circulation....
                if (eps.MCUAddress != null && eps.MCUAddress.Length > 0)
                {
                    long DID = 0;
                    if (eps.MCUAddressType == 4)
                    {
                        DID = long.Parse(eps.MCUAddress);
                        if (!m_ISDNusedNumber.ContainsKey(DID))
                        {
                            foreach (vrmMCUISDNServices DIDService in ISDNList)
                            {
                                if (DID >= DIDService.startNumber && DID <= DIDService.endNumber)
                                {
                                    if (checkThisPhoneNumber(DID, eps.confid, eps.instanceid, eps.id, eps.sType))
                                    {
                                        eps.bridgeIPISDNAddress = DID.ToString();
                                        eps.mcuServiceName = DIDService.ServiceName;
                                        eps.prefix = DIDService.prefix;
                                        m_ISDNusedNumber.Add(DID, 0);
                                        return true;
                                    }
                                }
                            }
                        }
                    }
                }

                foreach (vrmMCUISDNServices ISDN in ISDNList)
                {
                    if (ISDN.RangeSortOrder == 1)
                    {
                        for (long i = ISDN.endNumber; i > ISDN.startNumber; i--)
                        {
                            // First check if this number has been used already.
                            if (!m_ISDNusedNumber.ContainsKey(i))
                            {
                                if (checkThisPhoneNumber(i, eps.confid, eps.instanceid, eps.id, eps.sType))
                                {
                                    eps.bridgeIPISDNAddress = i.ToString();
                                    eps.mcuServiceName = ISDN.ServiceName;
                                    eps.prefix = ISDN.prefix;
                                    m_ISDNusedNumber.Add(i, 0);
                                    return true;
                                }
                            }
                        }
                    }
                    else
                    {
                        for (long i = ISDN.startNumber; i < ISDN.endNumber; i++)
                        {
                            // First check if this number has been used already.

                            if (!m_ISDNusedNumber.ContainsKey(i))
                            {
                                if (checkThisPhoneNumber(i, eps.confid, eps.instanceid, eps.id, eps.sType))
                                {
                                    eps.bridgeIPISDNAddress = i.ToString();
                                    eps.mcuServiceName = ISDN.ServiceName;
                                    eps.prefix = ISDN.prefix;
                                    m_ISDNusedNumber.Add(i, 0);
                                    return true;
                                }
                            }
                        }
                    }
                    return true;
                }
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
        }
        #endregion

        //FB 2636 Start

        #region GetSIPService
        /// <summary>
        /// GetSIPService
        /// </summary>
        /// <param name="eps"></param>
        /// <returns></returns>
        private bool GetSIPService(ref int ConfBridgeid, vrmConference conference,ref int e164number)
        {
            List<ICriterion> criterionList = new List<ICriterion>();
            IList userdnumbers = null;
            IMCUE164ServicesDao E164Dao = null;
            IList<vrmMCUE164Services> E164List = null;
            IEnumerable<int> allList = null;
            int strt = 0, end = 0;
            try
            {

                criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("BridgeID", ConfBridgeid));
                E164Dao = m_Hardware.GetE164ServicesDao();
                E164List = E164Dao.GetByCriteria(criterionList);

                FetchUsedRange(ref userdnumbers, conference, ConfBridgeid);

                foreach (vrmMCUE164Services DIDService in E164List)
                {
                    if (!Int32.TryParse(DIDService.StartRange, out strt) || !Int32.TryParse(DIDService.EndRange, out end))
                        continue;

                    if (allList == null) 
                        allList = Enumerable.Range(strt, end - strt + 1);
                    else
                        allList = allList.Union(Enumerable.Range(strt, end - strt + 1));
                }
                e164number = allList.Except(userdnumbers.Cast<int>()).Min();

                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
        }
        #endregion

        #region FetchUsedRange
       /// <summary>
        /// FetchUsedRange
       /// </summary>
       /// <param name="i"></param>
       /// <param name="confid"></param>
       /// <param name="instanceid"></param>
       /// <param name="id"></param>
       /// <param name="sType"></param>
       /// <returns></returns>
        private bool FetchUsedRange(ref IList results,vrmConference conf,int BridgeID)
        {
            string status = "",hql = "";
            try
            {
                status = String.Format("{0,3:D},{1,3:D},{2,3:D},{3,3:D}",
                    vrmConfStatus.Scheduled,
                    vrmConfStatus.Pending,
                    vrmConfStatus.Ongoing,
                    vrmConfStatus.OnMCU);

                hql = "select a.E164Dialnumber from myVRM.DataLayer.vrmConference c, myVRM.DataLayer.vrmConfBridge a where c.deleted = 0 and ";
                hql += " c.isVMR = 0 and c.status in (" + status + ")";
                hql += " and c.confnumname = a.confuId and c.conftype = 2 "; //"and c.confdate<='" + conf.confEnd;
                
                hql += " AND (dateadd(minute, " + conf.duration.ToString() + ",'";
                hql += conf.confdate.ToString("d") + " " + conf.confdate.ToString("t") + "')) >= c.confdate ";
                hql += " AND '" + conf.confdate.ToString("d") + " " + conf.confdate.ToString("t") + "'";
                hql += " <= (dateadd(minute, c.duration, c.confdate )) and a.BridgeID=" + BridgeID;

                results = m_IconfBridge.execQuery(hql);

                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
        }
        #endregion

        //FB 2636 End

        #region ConfirmConferenceInvitation
        /// <summary>
        /// ConfirmConferenceInvitation
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool ConfirmConferenceInvitation(ref vrmDataObject obj)
        {
            vrmMCU vMCU = null;//FB 2141

            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;

                node = xd.SelectSingleNode("//ConfirmConferenceInvitation/UserID");
                int userid = Int32.Parse(node.InnerXml.Trim());
                //node = xd.SelectSingleNode("//ConfirmConferenceInvitation/confPassword");
                //string confPassword = node.InnerXml.Trim();

                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("userid", userid));
                List<vrmUser> ul = m_vrmUserDAO.GetByCriteria(criterionList);

                string firstname = string.Empty;
                string lastname = string.Empty;
                string emailaddres = string.Empty;

                if (ul.Count == 0)
                {
                    List<vrmGuestUser> gl = m_vrmGuestDAO.GetByCriteria(criterionList);
                    vrmGuestUser guest = gl[0];
                    firstname = guest.FirstName;
                    lastname = guest.LastName;
                    emailaddres = guest.Email;
                }
                else
                {
                    vrmUser theUser = ul[0];
                    firstname = theUser.FirstName;
                    lastname = theUser.LastName;
                    emailaddres = theUser.Email;
                }

                XmlNodeList NodeList = xd.GetElementsByTagName("Conference");

                //List<vrmEmail> confEmailList = new List<vrmEmail>(); //FB 1830
                List<vrmConfUser> confUserList = new List<vrmConfUser>();
                List<vrmConfUser> confEmailUserList = new List<vrmConfUser>();
                
                obj.outXml += "<success>1</success>";

                // load eps with all information if -1 it is null (use default)
                foreach (XmlNode Node in NodeList)
                {
                    XmlElement element = (XmlElement)Node;
                    string videoProtocol, IPISDNAddress, connectionType, outsidenetwork, addresstype;

                    IPISDNAddress = element.GetElementsByTagName("IPISDNAddress")[0].InnerText;
                    connectionType = element.GetElementsByTagName("ConnectionType")[0].InnerText;
                    if (connectionType.Length == 0) connectionType = "0";
                    outsidenetwork = element.GetElementsByTagName("IsOutsideNetwork")[0].InnerText;
                    if (outsidenetwork.Length == 0) outsidenetwork = "1";// default outside network
                    addresstype = element.GetElementsByTagName("AddressType")[0].InnerText;
                    if (addresstype.Length == 0) addresstype = "1";// default IP

                    string confid = element.GetElementsByTagName("ConfID")[0].InnerText.ToUpper();

                    //Conference Id
                    CConfID Conf = new CConfID(confid);

                    criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("confid", Conf.ID));
                    if (Conf.instance > 0)
                        criterionList.Add(Expression.Eq("instanceid", Conf.instance));

                    //
                    // weather single or recurring process the same way. 
                    //
                    List<vrmConference> confList = m_vrmConfDAO.GetByCriteria(criterionList);
                    vrmUser confHost = null;    //FB 1830
                    vrmConference conf = null; //FB 1830
                    int idx = 0;
                    for (int v = 0; v < confList.Count; v++ )
                    {
                        //FB 1830 - start
                        conf = confList[v];
                        if (confHost == null)
                            confHost = m_vrmUserDAO.GetByUserId(conf.owner);
                        //FB 1830 - end

                        //Fetch Invitee status
                        int invitee, roomId;
                        string decision = string.Empty;
                        string reason = string.Empty;

                        decision = element.GetElementsByTagName("Decision")[0].InnerText;
                        //
                        // asp should send "0" not empty (undecided)
                        //
                        if (decision.Length == 0)
                            decision = "0";

                        reason = element.GetElementsByTagName("Reason")[0].InnerText;

                        criterionList.Add(Expression.Eq("confid", conf.confid));
                        criterionList.Add(Expression.Eq("instanceid", conf.instanceid));
                        criterionList.Add(Expression.Eq("userid", userid));

                        List<vrmConfUser> uList = m_IconfUser.GetByCriteria(criterionList);

                        if (uList.Count < 1)
                        {
                            //FB 1881 start
                            //myVRMException e = new myVRMException("Error cannot retrieve ConfUser ID:" + userid.ToString());
                            //obj.outXml = e.FetchErrorMsg();
                            obj.outXml = "";
                            //FB 1881 end
                            m_log.Error("Error cannot retrieve ConfUser ID:" + userid.ToString());
                            return false;
                        }
                        vrmConfUser confUser = uList[0];

                        int iDescision = Int32.Parse(decision);
                        if (iDescision == vrmConfUserStatus.Accepted) // Accept confernece
                        {

                            string locationID = element.GetElementsByTagName("LocationID")[0].InnerText;
                            if (locationID.Length == 0) locationID = "0";
                            roomId = Int32.Parse(locationID);
                            if (roomId == -1)
                            {
                                invitee = vrmConfUserType.External;
                                roomId = 0;
                            }
                            else
                            {
                                invitee = confUser.invitee;
                            }
                            confUser.roomId = roomId;
                            if (roomId == 0)
                            {
                                videoProtocol = element.GetElementsByTagName("VideoProtocol")[0].InnerText;
                                if (videoProtocol == "IP")
                                    videoProtocol = "1";
                                else
                                    videoProtocol = "2";

                                confUser.invitee = invitee;
                                confUser.defVideoProtocol = Int32.Parse(videoProtocol);
                                confUser.ipisdnaddress = IPISDNAddress;
                                confUser.connectionType = Int32.Parse(connectionType);
                                confUser.outsideNetwork = Int32.Parse(outsidenetwork);
                                confUser.addressType = Int32.Parse(addresstype);
                            }
                            // Some changes have been made to the participant profiles
                            // We need to assign appropriate bridge addresses now
                            if (conf.conftype != vrmConfType.P2P) // not if p2p			
                            {
                                GetEndPointService eps = new GetEndPointService();                              
                                eps.sType = "U";
                                eps.id = userid;
                                eps.bridgeid = confUser.bridgeid;
                                eps.addressType = confUser.addressType;
                                eps.outsideNetwork = confUser.outsideNetwork;                                
                                if (GetEndPointService(ref eps))
                                {
                                    confUser.bridgeAddressType = eps.addressType;
                                    confUser.mcuServiceName = eps.mcuServiceName;
                                    confUser.bridgeIPISDNAddress = eps.bridgeIPISDNAddress;
                                    confUser.bridgeid = eps.bridgeid;
                                }
                                string bridgeName = string.Empty;
                                vrmMCU MCU = null;
                                if (confUser.bridgeid > 0) //FB 1167
                                {
                                    MCU = m_vrmMCU.GetById(confUser.bridgeid);
                                    bridgeName = MCU.BridgeName;
                                }
                                else
                                {
                                    //FB 2141
                                    if (loadMCUInfo(ref conf, ref vMCU) && vMCU != null)//Using short circuit AND
                                    {
                                        confUser.bridgeIPISDNAddress = vMCU.BridgeAddress;
                                        confUser.bridgeid = vMCU.BridgeID;

                                    }
                                    else
                                        confUser.bridgeid = -1;
                                }
                                obj.outXml = "<ConfirmConferenceInvitation>";
                                obj.outXml += "<Bridge>";
                                obj.outXml += "<ID>" + confUser.bridgeid.ToString() + "</ID>";
                                obj.outXml += "<Name>" + bridgeName + "</Name>";
                                obj.outXml += "<Address>" + confUser.bridgeIPISDNAddress + "</Address>";
                                obj.outXml += "<AddressType>" + confUser.bridgeAddressType.ToString() + "</AddressType>";
                                obj.outXml += "</Bridge>";
                                obj.outXml += "</ConfirmConferenceInvitation>";
                                idx++;
                            }

                            confUser.status = Int32.Parse(decision);
                            confUser.reason = reason;
                            confUserList.Add(confUser);

                            //FB 1830 - start
                            //confEmailUserList.Add(confUser);
                            m_emailFactory.sendAcceptanceEmail(ref confUser, ref conf, ref confHost);
                            //FB 1830 - end
                        }
                        else if (iDescision == vrmConfUserStatus.Reschedule) //Change Request
                        {
                            string newComment = element.GetElementsByTagName("Comment")[0].InnerText; // FB 1830
                            m_emailFactory.sendChangeRequest(conf, ref element, emailaddres); // FB 1830

                            /* FB 1830 - codes commented start */
                            //string newStartDate, newStartTime;
                            //string newTimezone, newDurationMin, newComment;
                            //// Get countered information for conference
                            //newStartDate = element.GetElementsByTagName("StartDate")[0].InnerText;
                            //newStartTime = element.GetElementsByTagName("StartTime")[0].InnerText;
                            //newTimezone = element.GetElementsByTagName("Timezone")[0].InnerText;
                            //newDurationMin = element.GetElementsByTagName("Duration")[0].InnerText;
                            //newComment = element.GetElementsByTagName("Comment")[0].InnerText;

                            //vrmEmail email = new vrmEmail();

                            //criterionList = new List<ICriterion>();
                            //criterionList.Add(Expression.Eq("userid", conf.owner));

                            //List<vrmUser> Owners = m_vrmUserDAO.GetByCriteria(criterionList);

                            //vrmUser confOwner;
                            //if (Owners.Count > 0)
                            //    confOwner = Owners[0];
                            //else
                            //    return false;

                            //string toname = confOwner.FirstName + " " + confOwner.LastName;

                            //string subject = "Reschedule request", body, greeting;

                            //timeZoneData tz = new timeZoneData();
                            //timeZone.GetTimeZone(conf.timezone, ref tz);
                            //greeting = "Hello " + toname + ",<BR>";
                            //greeting += "I am unable to attend the following conference and ";
                            //greeting += "hereby request you to reschedule the same.<BR>";
                            //body = "Conference Name : " + conf.externalname;
                            //body += "<BR> Conference Unique ID : " + conf.confnumname.ToString();
                            //body += "<BR> Requested Date and Time : ";
                            //body += newStartDate + " ";
                            //body += newStartTime;
                            //body += " " + tz.TimeZone;
                            //body += "<BR> Requested Duration (in minutes) : " + newDurationMin;
                            //body += "<BR> Comments: " + newComment;
                            //body += "<BR> <BR> ";

                            //body = greeting + body;

                            //email.UUID = m_usrDAO.getEmailUUID();
                            //email.emailFrom = emailaddres;
                            //email.emailTo = confOwner.Email;
                            //email.Subject = subject;
                            //email.Message = body;

                            //confEmailList.Add(email);
                            //// because we are assingning uid's we must save the email now. 
                            //// email list is implemented but not used for now. Also this should be
                            //// done by email factory! 
                            //m_emailDAO.Save(email);
                            /* FB 1830 - End */
                            confUser.status = Int32.Parse(decision);//FB 1466 - Start
                            confUser.reason = newComment;
                            confUserList.Add(confUser);//FB 1466 - End
                        }
                        else
                        {
                            confUser.status = Int32.Parse(decision);
                            confUser.reason = reason;
                            confUserList.Add(confUser);
                        }
                    }
                }
                m_IconfUser.SaveOrUpdateList(confUserList);//FB 1174
                
                //FB 1830 - Below code is commented for FB 1830
                //foreach (vrmConfUser eCu in confEmailUserList)
                //    m_emailFactory.sendAcceptanceEmail(eCu.confid, eCu.instanceid, eCu.userid);

            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);
                obj.outXml = ""; //FB 1881
                return false;
            }
            return true;
        }
        #endregion

        #region checkThisPhoneNumber
        /// <summary>
        /// checkThisPhoneNumber
        /// </summary>
        /// <param name="i"></param>
        /// <param name="confid"></param>
        /// <param name="instanceid"></param>
        /// <param name="id"></param>
        /// <param name="sType"></param>
        /// <returns></returns>
        private bool checkThisPhoneNumber(long i, int confid, int instanceid, int id, string sType)
        {
            // find this nubmer anywhere (user/room) that this conference is scheduled and it is not to be used.
            try
            {

                vrmConference Conf = m_vrmConfDAO.GetByConfId(confid, instanceid);

                string status = String.Format("{0,3:D},{1,3:D},{2,3:D},{3,3:D}",
                    vrmConfStatus.Scheduled,
                    vrmConfStatus.Pending,
                    vrmConfStatus.Ongoing,
                    vrmConfStatus.OnMCU);

                string hql;

                hql = "SELECT cu.bridgeIPISDNAddress, cu.confid,cu.instanceid, cu.userid FROM myVRM.DataLayer.vrmConfUser cu, ";
                hql += "myVRM.DataLayer.vrmConference c WHERE c.confid = cu.confid AND c.instanceid = cu.instanceid ";
                hql += " AND cu.bridgeIPISDNAddress = '" + i.ToString() + "' ";
                hql += " AND (dateadd(minute, " + Conf.duration.ToString() + ",'";
                hql += Conf.confdate.ToString("d") + " " + Conf.confdate.ToString("t") + "')) >= c.confdate ";
                hql += " AND '" + Conf.confdate.ToString("d") + " " + Conf.confdate.ToString("t") + "'";
                hql += " <= (dateadd(minute, c.duration, c.confdate ))";
                hql += "AND c.deleted = 0 AND c.status IN( " + status + ")";

                IList results = m_IconfRoom.execQuery(hql);
                // always return  false even if this is the same person (assign a new number ALWAYS)
                if (results.Count > 0)
                {
                    return false;
                }

                hql = "SELECT cr.bridgeIPISDNAddress, cr.confid, cr.instanceid, cr.roomId FROM myVRM.DataLayer.vrmConfRoom  cr, ";
                hql += "myVRM.DataLayer.vrmConference  c WHERE c.confid = cr.confid and c.instanceid = cr.instanceid ";
                hql += " AND  cr.bridgeIPISDNAddress = '" + i.ToString() + "' ";
                hql += " AND (dateadd(minute, " + Conf.duration.ToString() + ",'";
                hql += Conf.confdate.ToString("d") + " " + Conf.confdate.ToString("t") + "')) >= c.confdate ";
                hql += " AND '" + Conf.confdate.ToString("d") + " " + Conf.confdate.ToString("t") + "'";
                hql += " <= (dateadd(minute, c.duration, c.confdate ))";
                hql += " AND c.deleted = 0 AND c.status IN( " + status + ")";

                results = m_IconfUser.execQuery(hql);
                // always return  false even if this is the same person (assign a new number ALWAYS)
                if (results.Count > 0)
                {
                    return false;
                }

                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
        }
        #endregion

        #region SetTemplateOrder
        /// <summary>
        /// SetTemplateOrder
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SetTemplateOrder(ref vrmDataObject obj)
        {
            bool bRet = true;
            try
            {
                List<vrmTemplate> templList = new List<vrmTemplate>();

                XmlNodeList itemList;

                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                itemList = xd.GetElementsByTagName("Template");
                foreach (XmlNode innerNode in itemList)
                {
                    string templateId = innerNode.SelectSingleNode("TemplateID").InnerText;
                    string sortOrder = innerNode.SelectSingleNode("Order").InnerText;
                    int id = Int32.Parse(templateId);
                    vrmTemplate temp = m_vrmTempDAO.GetById(id);
                    temp.Orderid = Int32.Parse(sortOrder);
                    templList.Add(temp);
                }
                m_vrmTempDAO.SaveOrUpdateList(templList);
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                //obj.outXml = e.FetchErrorMsg();
                obj.outXml = ""; //FB 1881
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);
                obj.outXml = ""; //FB 1881
                bRet = false;
            }

            return bRet;
        }
        #endregion

        #region ConferenceApproval
        /// <summary>
        /// Summary description for CConfApproval
        /// ported over from .com this class will set up and maintain the approval chain. 
        /// There are 4 levels e.g (ROOM,MCU,DEPT,SYSTEM). For ROOM and MCU 1 entry in the 
        /// approval table per approver for the 1st entity it (RooomId/MCUId) for each instance of the conference.
        /// So; if ther are 10 rooms that need approval and 2 approvers there will be 2 entries with the first roomid for each.
        /// NOTE: this is not 100% correct (ie rooms can  be dropped or approvers can be changed) and should be redesigned in
        /// future.
        /// 
        /// DEPTARTMENT and SYSTEM approvals always have only one entry. 
        /// The active approval level is the only one in the approval table. When a level is approved it is removed and the
        /// next one added. 
        /// </summary>
        private bool ConferenceApproval(int userid, ref List<vrmConference> confList, CConfID confInsObj) //Recurrence Fixes for approval process
        {
                try
                {
                    bool bRet = false;
                    bool isFirstTime = true;    //FB 1158
                    vrmUser user = new vrmUser();
                    IUserDao IUser = m_usrDAO.GetUserDao();
                    user = IUser.GetByUserId(userid);
                    int iRecurring = 0;
                    List<vrmConfRoom> rList = new List<vrmConfRoom>();
                    List<vrmConfUser> uList = new List<vrmConfUser>();
                    List<vrmMCU> mcuList = new List<vrmMCU>();
                    List<ICriterion> criterionList;
                    m_level = 0;
                    Boolean isApprovalReq = false; //Code added for recurrance fixes
                    
                    int autoAcceptModConf = 0;  //Organization Module
                    if (orgInfo != null)
                        autoAcceptModConf = orgInfo.AutoAcceptModConf;

                        foreach (vrmConference conf in confList)
                    {   
                        // if createType is 0 this conference was created so it goes through the approval process
                        // otherwise it is modified and you must check the autoapproveImmediate flag in sys_settings_d
                        // if the flag is set on bypass aprrovals
                        if (isFirstTime)    //FB 1158
                        {
                            isFirstTime = false;    //FB 1158

                            criterionList = new List<ICriterion>();
                            criterionList.Add(Expression.Eq("confid", conf.confid));
                            criterionList.Add(Expression.Eq("instanceid", conf.instanceid));

                            rList = m_IconfRoom.GetByCriteria(criterionList);
                            uList = m_IconfUser.GetByCriteria(criterionList);
                            loadMCUInfo(ref mcuList, rList, uList);
                            confList[0].mcuList = mcuList;
                            m_emailFactory.confRoomList = rList; //FB 1830
                            m_emailFactory.confUserLst = uList;

                            if (conf.immediate == 1)
                                return true;

                            /* *** Recurrence Fixes for Approval Process (as per Bri Mail) - Start *** */
                            if (m_isEdit)
                            {
                                m_emailFactory.conf_isEdit = true; //FB 1749
                                if (conf.recuring == 1 && confInsObj.instance == 0)
                                {
                                    GetPastInstances(ref confList, ref user);   //FB 1158
                                    if (autoAcceptModConf == 1)
                                    {
                                        isApprovalReq = CheckConfApproval(conf.confid, rList, mcuList);
                                        if (!isApprovalReq)
                                            return true;
                                    }
                                    else   //FB 1158
                                    {
                                        ClearApprovalInfo(conf.confid); //FB 1158
                                    }
                                }
                                else
                                {
                                    if (autoAcceptModConf == 1 && conf.status != vrmConfStatus.Pending)//FB 885 & 1068 Used wrong properties 
                                        return true;

                                    /* *** Recurrence Fixes for Approval Process (as per Bri Mail) - Start *** */
                                    //FB 1158
                                    IConfApprovalDAO IApprover = m_confDAO.GetConfApprovalDao();
                                    
                                    List<ICriterion> criterionLst = new List<ICriterion>();
                                    List<vrmConfApproval> ConfApprovalLst;

                                    criterionLst.Add(Expression.Eq("confid", conf.confid));
                                    criterionLst.Add(Expression.Eq("instanceid", conf.instanceid));
                                    ConfApprovalLst = IApprover.GetByCriteria(criterionLst);

                                    foreach (vrmConfApproval vrmAppConf in ConfApprovalLst)
                                    {
                                        IApprover.clearFetch();
                                        IApprover.Delete(vrmAppConf);
                                    }
                                   
                                    /* *** Recurrence Fixes for Approval Process (as per Bri Mail) - End *** */
                                }
                            }
                            /* *** Recurrence Fixes for Approval Process (as per Bri Mail) - End *** */
                        }
                      
                        // this routine will increment and set the next level
                         // it returns true if it finds and sets a level 
                         // OR if it cannot find a level to set;
                         Hashtable approver;    //FB 1158

                         if (conf.recuring == 1) //FB 1158
                         {
                             if (IsPastInstance(conf, ref user))   //FB 1158 - this is to stop the iteration for the past confs
                                 continue;  //FB 1158
                         }

                         if (m_level == 0 && iRecurring == 0)
                             m_level++;

                         bool working = true;
                         while (m_level > 0 && working)
                         {
                             switch (m_level)
                             {
                                 case (int)LevelEntity.ROOM:
                                     approver = new Hashtable();    //FB 1158
                                     foreach (vrmConfRoom rm in rList)
                                     {
                                         // loop through each room and get EASH approver
                                         foreach (vrmLocApprover la in rm.Room.locationApprover)
                                         {
                                             string roomApp = la.approverid + "|" + la.roomid;  //FB 1158 - Approval Issues
                                             if (!approver.ContainsKey(roomApp))    //FB 1158 - Approval Issues
                                             {
                                                 if (la.approverid != conf.owner)//FB 1067
                                                 {
                                                     approver.Add(roomApp, la.roomid);  //FB 1158 - Approval Issues
                                                     SaveOrUpdateConfApproval(la.approverid, la.roomid, (int)LevelEntity.ROOM, conf.confid, conf.instanceid, 0);
                                                     if (iRecurring == 0)
                                                     {
                                                         user = m_vrmUserDAO.GetByUserId(la.approverid);
                                                         m_emailFactory.sendLevelEmail(conf, (int)LevelEntity.ROOM, user);
                                                     }
                                                     conf.status = vrmConfStatus.Pending;
                                                     working = false;
                                                 }
                                             }
                                         }
                                     }
                                     break;
                                 case (int)LevelEntity.MCU:
                                     approver = new Hashtable();    //FB 1158
                                     foreach (vrmMCU mcu in mcuList)
                                     {
                                         // loop through each room and get EASH approver
                                         foreach (vrmMCUApprover mAp in mcu.MCUApprover)
                                         {
                                             string mcuApp = mAp.approverid + "|" + mAp.mcuid;  //FB 1158 - Approval Issues
                                             if (!approver.ContainsKey(mcuApp))    //FB 1158 - Approval Issues
                                             {
                                                 approver.Add(mcuApp, mAp.mcuid);  //FB 1158 - Approval Issues

                                                 SaveOrUpdateConfApproval(mAp.approverid, mAp.mcuid, (int)LevelEntity.MCU, conf.confid, conf.instanceid, 0);

                                                 if (iRecurring == 0)
                                                 {
                                                     conf.mcuList = mcuList;
                                                     user = m_vrmUserDAO.GetByUserId(mAp.approverid);
                                                     m_emailFactory.sendLevelEmail(conf, (int)LevelEntity.MCU, user);
                                                     m_level = (int)LevelEntity.MCU;
                                                 }
                                                 conf.status = vrmConfStatus.Pending;
                                                 working = false;
                                             }
                                         }

                                     }
                                     break;
                                 case (int)LevelEntity.DEPT:    //Department Level is redundant
                                     if (conf.ConfDeptID != 0)
                                     {
                                         deptDAO m_deptDAO = new deptDAO(m_configPath, m_log);
                                         IDeptApproverDao m_IuserDeptDAO = m_deptDAO.GetDeptApproverDao();
                                         criterionList = new List<ICriterion>();
                                         criterionList.Add(Expression.Eq("departmentid", conf.ConfDeptID));
                                         List<vrmDeptApprover> deptList = m_IuserDeptDAO.GetByCriteria(criterionList);

                                         foreach (vrmDeptApprover deptApprov in deptList)
                                         {
                                             SaveOrUpdateConfApproval(deptApprov.approverid, conf.ConfDeptID, (int)LevelEntity.DEPT, conf.confid, conf.instanceid, 0);

                                             if (iRecurring == 0)
                                             {
                                                 user = m_vrmUserDAO.GetByUserId(deptApprov.approverid);
                                                 m_emailFactory.sendLevelEmail(conf, (int)LevelEntity.DEPT, user);
                                                 m_level = (int)LevelEntity.DEPT;
                                             }

                                             conf.status = vrmConfStatus.Pending;
                                             working = false;
                                         }
                                     }
                                     break;
                                 case (int)LevelEntity.SYSTEM:

                                     IList<sysApprover> sysApproverList = m_ISysApproverDAO.GetSysApproversByOrgId(organizationID);  //Organization Module

                                     foreach (sysApprover sysApp in sysApproverList)
                                     {
                                         conf.status = 1;
                                         working = false;
                                         //IApprover.Save(confApprove);
                                         SaveOrUpdateConfApproval(sysApp.approverid, 1, (int)LevelEntity.SYSTEM, conf.confid, conf.instanceid, 0);
                                         if (iRecurring == 0)
                                         {
                                             user = m_vrmUserDAO.GetByUserId(sysApp.approverid);
                                             m_emailFactory.sendLevelEmail(conf, (int)LevelEntity.SYSTEM, user);
                                             m_level = (int)LevelEntity.SYSTEM;
                                         }
                                         conf.status = vrmConfStatus.Pending;
                                         working = false;
                                     }
                                     break;
                                 // no more levels to set;
                                 default:
                                     conf.status = vrmConfStatus.Scheduled;
                                     if (iRecurring == 0)
                                     {
                                         m_level = 0;
                                         bRet = true;
                                     }
                                     working = false;
                                     break;
                             }
                             if(working)
                                m_level++;
                         }
                        iRecurring++;
                    
                   }
                   return bRet;
                }
                catch (myVRMException e)
                {
                    m_log.Error("vrmException", e);
                    return false;
                }
                catch (Exception e)
                {
                    m_log.Error("sytemException", e);
                    return false;
                }
            }
        #endregion

        #region loadMCUInfo
        /// <summary>
        /// load MCU Info
        /// </summary>
        /// <param name="mcuList"></param>
        /// <param name="confRoom"></param>
        /// <param name="confUser"></param>
        /// <returns></returns>
        public bool loadMCUInfo(ref List<vrmMCU> mcuList, List<vrmConfRoom> confRoom, List<vrmConfUser>confUser)
            {
                try
                {
                    mcuList = null;  //FB 1830 - start
                    mcuList = new List<vrmMCU>();
                    List<int> bId = new List<int>();
                    //Hashtable bId = new Hashtable();
                    foreach (vrmConfRoom cr in confRoom)
                    {
                        if (cr.bridgeid > 0 )
                        {
                            if (!bId.Contains(cr.bridgeid))
                                bId.Add(cr.bridgeid);
                        }
                    }
                    foreach (vrmConfUser cu in confUser)
                    {
                        if (cu.bridgeid > 0 && cu.invitee == 1) // external only
                        {
                            if (!bId.Contains(cu.bridgeid))
                                bId.Add(cu.bridgeid);
                        }
                    }
                    vrmMCU MCU = null;
                    for (int m = 0; m < bId.Count; m++)
                    {
                        MCU = m_vrmMCU.GetById(bId[m]);
                        mcuList.Add(MCU);
                    }
                    //IDictionaryEnumerator iEnum = bId.GetEnumerator();
                    //while (iEnum.MoveNext())
                    //{
                    //    int bridgeId = (int)iEnum.Value;
                    //    vrmMCU MCU = m_vrmMCU.GetById(bridgeId);
                    //    mcuList.Add(MCU);
                    //}
                    //FB 1830 - end
                    return true;
                }
                catch (myVRMException e)
                {
                    m_log.Error("vrmException", e);
                    return false;
                }
                catch (Exception e)
                {
                    m_log.Error("sytemException", e);
                    return false;
                }
            }
        #endregion

        #region SaveOrUpdateConfApproval
        /// <summary>
        /// SaveOrUpdateConfApproval
        /// </summary>
        /// <param name="approverID"></param>
        /// <param name="EntityID"></param>
        /// <param name="EntityType"></param>
        /// <param name="ConfID"></param>
        /// <param name="InstanceID"></param>
        /// <param name="Decision"></param>
        /// <returns></returns>
        public Boolean SaveOrUpdateConfApproval(Int32 approverID,Int32 EntityID,Int32 EntityType, Int32 ConfID, Int32 InstanceID, Int32 Decision)
        {
            vrmConfApproval confApprove;
            IConfApprovalDAO IApprover;
            List<ICriterion> criterionList;
            try
            {
                IApprover = m_confDAO.GetConfApprovalDao();
                criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("approverid", approverID));
                criterionList.Add(Expression.Eq("entityid", EntityID));
                criterionList.Add(Expression.Eq("entitytype", EntityType));
                criterionList.Add(Expression.Eq("confid", ConfID));
                criterionList.Add(Expression.Eq("instanceid", InstanceID));
                criterionList.Add(Expression.Eq("decision", Decision));
                List<vrmConfApproval> ConfApprList = IApprover.GetByCriteria(criterionList);
                if (ConfApprList.Count < 1)
                {
                    confApprove = new vrmConfApproval();
                    confApprove.approverid = approverID;
                    confApprove.entityid = EntityID;
                    confApprove.entitytype = EntityType;
                    confApprove.confid = ConfID;
                    confApprove.instanceid = InstanceID;
                    confApprove.decision = Decision;
                    IApprover.Save(confApprove);
                }
                return true; 
            }
            catch (Exception ex)
            {
                m_log.Error("SytemException in SaveOrUpdateConfApproval", ex);
               return false;
            }
        }
        #endregion

        /* *** Recurrence Fixes to check whether approval process required on modify start **** */
        #region Check whether approval process required on modify
        /// <summary>
        /// Check whether approval process required on modify
        /// </summary>
        /// <param name="ConfID"></param>
        /// <param name="rList"></param>
        /// <param name="mcuList"></param>
        /// <returns></returns>
        public Boolean CheckConfApproval(Int32 ConfID, List<vrmConfRoom> rList, List<vrmMCU> mcuList )
        {
            IConfApprovalDAO IApprover;
            List<ICriterion> criterionList;
            List<vrmConfApproval> ConfApprovalList;
            Boolean isApprovalProcessReq = false;
            
            try
            {
                IApprover = m_confDAO.GetConfApprovalDao();
                criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("confid", ConfID));
                criterionList.Add(Expression.Eq("decision",0));

                ConfApprovalList = IApprover.GetByCriteria(criterionList);

                if (ConfApprovalList.Count > 0)
                {
                    isApprovalProcessReq = true;
                }
                else
                {
                    foreach(vrmConfRoom rm in rList)
                    {
                        IApprover.clearFetch();
                        ConfApprovalList.Clear();
                        criterionList.Clear();
                        criterionList.Add(Expression.Eq("confid", ConfID));
                        criterionList.Add(Expression.Eq("entityid", rm.roomId));
                        criterionList.Add(Expression.Eq("entitytype",(int)LevelEntity.ROOM));
                        
                        ConfApprovalList = IApprover.GetByCriteria(criterionList);

                        if (ConfApprovalList.Count < 1)
                        {
                            isApprovalProcessReq = true;
                            break;
                        }
                    }

                    if (!isApprovalProcessReq)
                    {
                        foreach (vrmMCU mcu in mcuList)
                        {
                            IApprover.clearFetch();
                            ConfApprovalList.Clear();
                            criterionList.Clear();
                            criterionList.Add(Expression.Eq("confid", ConfID));
                            criterionList.Add(Expression.Eq("entityid", mcu.BridgeID));
                            criterionList.Add(Expression.Eq("entitytype", (int)LevelEntity.MCU));
                            ConfApprovalList = IApprover.GetByCriteria(criterionList);

                            if (ConfApprovalList.Count < 1)
                            {
                                isApprovalProcessReq = true;
                                break;
                            }
                        }
                    }
                 }

                 if (isApprovalProcessReq)
                 {
                     ConfApprovalList.Clear();
                     criterionList.Clear();

                     criterionList.Add(Expression.Eq("confid", ConfID));
                     
                     /* *** FB 1158 - Approval Issues ... start *** */
                     if (pastInstances != null)
                     {
                         if (pastInstances.Count > 0)
                             criterionList.Add(Expression.Not(Expression.In("instanceid", pastInstances)));
                     }
                     /* *** FB 1158 - Approval Issues ... end *** */

                     ConfApprovalList = IApprover.GetByCriteria(criterionList);

                     foreach (vrmConfApproval vrmAppConf in ConfApprovalList)
                     {
                         IApprover.clearFetch();
                         IApprover.Delete(vrmAppConf);
                     }
                 }
                return isApprovalProcessReq;
            }
            catch (Exception ex)
            {
                m_log.Error("SytemException in CheckConfApproval", ex);
                return false;
            }
        }

        #endregion
        /* *** Recurrence Fixes to check whether approval process required on modify end **** */

        /* *** FB 1158 - Approval Issues ... start *** */

        #region Get Past Instances
        /// <summary>
        /// Get Past Instances
        /// </summary>
        /// <param name="confList"></param>
        /// <param name="user"></param>
        private void GetPastInstances(ref List<vrmConference> confList, ref vrmUser user)
        {
            try
            {
                pastInstances = new List<int>();
                foreach (vrmConference conf in confList)
                {
                    if(IsPastInstance(conf, ref user))
                        pastInstances.Add(conf.instanceid);
                    
                }
            }
            catch (Exception ex)
            {
                m_log.Error("SytemException in GetPastInstances", ex);
            }
        }
        #endregion

        #region Check whether the conference is past or not
        /// <summary>
        /// Check whether the conference is past or not
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="user"></param>
        private bool IsPastInstance(vrmConference confObj, ref vrmUser user)
        {
            try
            {
                if (confObj.status == vrmConfStatus.Completed || confObj.status == vrmConfStatus.OnMCU || confObj.deleted == 1 || confObj.status == vrmConfStatus.Terminated)
                    return true;

                DateTime serverTime = DateTime.Now;
                timeZone.changeToGMTTime(sysSettings.TimeZone, ref serverTime);
                timeZone.userPreferedTime(user.TimeZone, ref serverTime);
                DateTime confDateTime = confObj.confdate;
                timeZone.userPreferedTime(user.TimeZone, ref confDateTime);

                if (confDateTime < serverTime)
                {
                    return true; 
                }
            }
            catch (Exception ex)
            {
                m_log.Error("SytemException in IsPastInstance", ex);
            }
            return false;
        }
        #endregion

        #region Clear the redundant approval details of the conference
        /// <summary>
        /// Clear the redundant approval details of the conference
        /// </summary>
        /// <param name="ConfID"></param>
        public void ClearApprovalInfo(Int32 ConfID)
        {
            IConfApprovalDAO IApprover;
            List<ICriterion> criterionList;
            List<vrmConfApproval> ConfApprovalList;
            try
            {
                IApprover = m_confDAO.GetConfApprovalDao();
                criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("confid", ConfID));

                if (pastInstances != null)
                {
                    if (pastInstances.Count > 0)
                        criterionList.Add(Expression.Not(Expression.In("instanceid", pastInstances)));
                }

                ConfApprovalList = IApprover.GetByCriteria(criterionList);

                if (ConfApprovalList.Count > 0)
                {
                    foreach (vrmConfApproval vrmAppConf in ConfApprovalList)
                    {
                        IApprover.clearFetch();
                        IApprover.Delete(vrmAppConf);
                    }
                }
            }
            catch (Exception ex)
            {
                m_log.Error("SytemException in ClearApprovalInfo", ex);
            }
        }

        #endregion

        /* *** FB 1158 - Approval Issues ... end *** */

        //Code added for Diagnostics Module - start

        #region Delete Past Conference - Event Log
        /// <summary>
        /// Delete uses conf id so org ID is not required.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>

        public bool DeletePastConference(ref vrmDataObject obj)
        {
            bool bRet = true;
            List<ICriterion> criterionList = new List<ICriterion>();
            String strSQL = "";
            Int32 strExec = -1;
            ns_SqlHelper.SqlHelper sqlCon = null;
            String path = "";
            System.Data.DataSet dSet = null;
            String confID = "";
            String instanceID = "";
            String confNumName = "";
            String purgeData = "";
            EvtLog e_log = new EvtLog();

            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//DeletePastConference/Path");
                path = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//DeletePastConference/PurgeData");
                purgeData = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//DeletePastConference/organizationID"); //Organization Module Fixes
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);

                sqlCon = new ns_SqlHelper.SqlHelper(path);

                sqlCon.OpenConnection();

                if (purgeData == "1")
                    strSQL = "select * from Conf_conference_d where confid <> 11 And (datediff(minute,dbo.changeTOGMTtime((SELECT timezone FROM sys_settings_D),getdate()), confdate) < 5) or status = 7 and orgId = '"+ organizationID.ToString() +"' order by Confid";
                else if (purgeData == "2")
                    strSQL = "select * from Conf_conference_d where confid <> 11 and orgId = '" + organizationID.ToString() + "' order by Confid";

                dSet = sqlCon.ExecuteDataSet(strSQL);

                sqlCon.OpenTransaction();
                if (purgeData == "1")
                    e_log.LogEvent("Purge Past Conferences Started Successfully to purge " + dSet.Tables[0].Rows.Count + " Conference(s).");
                else if (purgeData == "2")
                    e_log.LogEvent("Purge Conferences Started Successfully to purge " + dSet.Tables[0].Rows.Count + " Conference(s).");
                

                foreach (System.Data.DataRow row in dSet.Tables[0].Rows)
                {
                    confID = row["confid"].ToString();
                    instanceID = row["InstanceID"].ToString();
                    confNumName = row["confNumName"].ToString();

                    strSQL = "Delete from Conf_AdvAVParams_D  where confid=" + confID + " and InstanceID=" + instanceID;
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Conf_Alerts_D where confid=" + confID + " and InstanceID=" + instanceID;
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Conf_Approval_D where confid=" + confID + " and InstanceID=" + instanceID;
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Conf_Attachments_D where confid=" + confID + " and InstanceID=" + instanceID;
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Conf_Cascade_D where confid=" + confID + " and InstanceID=" + instanceID;
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Conf_FoodOrder_D where confid=" + confID + " and InstanceID=" + instanceID;
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Conf_Group_D where confid=" + confID;
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Conf_Monitor_D where confid=" + confID + " and InstanceID=" + instanceID;
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Conf_ResourceOrder_D where confid=" + confID + " and InstanceID=" + instanceID;
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Conf_Room_D where confid=" + confID + " and InstanceID=" + instanceID;
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Conf_RoomSplit_D where confid=" + confID + " and InstanceID=" + instanceID;
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Conf_User_D where confid=" + confID + " and InstanceID=" + instanceID;
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Inv_WorkItem_D where Workorderid in (select id from Inv_WorkOrder_D where confid =" + confID + " and InstanceID=" + instanceID +")";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Inv_WorkOrder_D where confid=" + confID + " and InstanceID=" + instanceID;
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Conf_conference_d where confid=" + confID + " and InstanceID=" + instanceID;
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    if (purgeData == "1")
                    {
                        strExec = 0;

                        strSQL = "select Isnull(min(ConfNumName),0) from Conf_conference_d where confid=" + confID;
                        strExec = Convert.ToInt32(sqlCon.ExecuteScalar(strSQL));

                        if (strExec > 0)
                        {
                            strSQL = "Update Conf_RecurInfo_D set ConfuID =" + strExec + " where confid=" + confID;
                            strExec = sqlCon.ExecuteNonQuery(strSQL);
                        }
                        else
                        {
                            strSQL = "Delete from Conf_RecurInfo_D where confid=" + confID + " and ConfUid=" + confNumName;
                            strExec = sqlCon.ExecuteNonQuery(strSQL);
                        }
                    }
                    else
                    {
                        strSQL = "Delete from Conf_RecurInfo_D where confid=" + confID + " and ConfUid=" + confNumName;
                        strExec = sqlCon.ExecuteNonQuery(strSQL);
                    }
                }

                sqlCon.CommitTransaction();
                
                sqlCon.CloseConnection();
                if (purgeData == "1")
                    e_log.LogEvent("Purge All Past Conferences Executed Successfully");
                else
                    e_log.LogEvent("Purge All Conferences Data Executed Successfully");

            }
            catch (myVRMException e)
            {
                sqlCon.RollBackTransaction();

                m_log.Error("vrmException", e);
                //obj.outXml = e.FetchErrorMsg();
                obj.outXml = ""; //FB 1881
                bRet = false;
            }
            catch (Exception e)
            {
                sqlCon.RollBackTransaction();

                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);
                obj.outXml = ""; //FB 1881
                bRet = false;
            }
            return bRet;
        }

        #endregion

        //Code added for Diagnostics Module - end

        //Code added for Cisco ICAL Fb 1602 - start

        #region DeleteCiscoICAL
        /// <summary>
        /// Delete ICAl on Delete conference
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>

        public bool DeleteCiscoICAL(ref vrmDataObject obj)
        {
            bool bRet = true;
            int eptID = -1;// FB 1675
            try
            {
                //Commented below codelines for FB 1915 starts
                ////FB 1830 start
                //List<vrmConfRoom> rList = new List<vrmConfRoom>();
                //List<vrmConfUser> uList = new List<vrmConfUser>();
                //List<vrmMCU> mcuList = new List<vrmMCU>();
                ////FB 1830 end
                //Commented below codelines for FB 1915 ends
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;
                node = xd.SelectSingleNode("//DeleteCiscoICAL/UserID");
                int userid = Int32.Parse(node.InnerXml.Trim());
                node = xd.SelectSingleNode("//DeleteCiscoICAL/ConfID");
                string confid = node.InnerXml.Trim();

                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);

				//FB 1675 start
                node = xd.SelectSingleNode("//DeleteCiscoICAL/endpointID");

                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out eptID);
				//FB 1675 end

                CConfID Conf = new CConfID(confid);

                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("confid", Conf.ID));

                if (Conf.instance > 0)
                    criterionList.Add(Expression.Eq("instanceid", Conf.instance));

                List<vrmConference> conf = m_vrmConfDAO.GetByCriteria(criterionList, true);

                if (orgInfo.SendConfirmationEmail == 0) //FB 2470
                    m_emailFactory.sendICALforCISCOEndpoints(conf, "D", false, eptID);// FB 1675
                
                //Commented below codelines for FB 1915 starts
                //if (eptID < 0)
                //{
                //    //FB 1830 start-Sending mail when conference is deleted
                //    rList = m_IconfRoom.GetByCriteria(criterionList);
                //    uList = m_IconfUser.GetByCriteria(criterionList);
                //    loadMCUInfo(ref mcuList, rList, uList);
                //    conf[0].mcuList = mcuList;
                //    m_emailFactory.confRoomList = rList; //FB 1830
                //    m_emailFactory.confUserLst = uList; //FB 1830

                //    if (pending == 1) //FB 1915
                //        conf[0].status = 1;
                //    m_emailFactory.SendConfirmationEmails(conf);

                //}//FB 1675

                //m_emailFactory.emailLocations(conf);
                //m_emailFactory.emailParticipants(conf);
                //if (conf[0].conftype != 7)
                //{
                //    if (conf[0].mcuList.Count > 0)
                //        m_emailFactory.sendMCUAdminEmail(conf[0]);
                //}
                //FB 1830 end
                //Commented below codelines for FB 1915 ends

            }
            catch (myVRMException e)
            {
               
                m_log.Error("vrmException", e);
                //obj.outXml = e.FetchErrorMsg();
                obj.outXml = ""; //FB 1881
                bRet = false;
            }
            catch (Exception e)
            {
              
                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);
                obj.outXml = ""; //FB 1881
                bRet = false;
            }
            return bRet;
        }

        #endregion

        #region CreateCiscoICALOnApproval
        /// <summary>
        /// On approval of the Conference send ICAL emails.
        /// This method needs to be merged with SetApproveConference while converting Com to .Net.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>

        public bool CreateCiscoICALOnApproval(ref vrmDataObject obj)
        {
            bool bRet = true;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;
                node = xd.SelectSingleNode("//approveConference/userID");
                int userid = Int32.Parse(node.InnerXml.Trim());
                XmlNodeList confs = xd.SelectNodes("//approveConference/conferences/conference");

                if (confs != null)
                {
                    foreach (XmlNode nd in confs)
                    {
                        node = nd.SelectSingleNode("confID");
                        string confid = node.InnerXml.Trim();

                        CConfID Conf = new CConfID(confid);

                        List<ICriterion> criterionList = new List<ICriterion>();
                        criterionList.Add(Expression.Eq("confid", Conf.ID));

                        if (Conf.instance > 0)
                            criterionList.Add(Expression.Eq("instanceid", Conf.instance));

                        List<vrmConference> conf = m_vrmConfDAO.GetByCriteria(criterionList, true);
                        
                            if (CheckConferenceforDenial(conf))//FB 1675
                            {
                                if (orgInfo.SendConfirmationEmail == 0) //FB 2470
                                    m_emailFactory.sendICALforCISCOEndpoints(conf, "D", false, -1);// FB 1675
                            }
                            else if (CheckConferenceApproval(conf))
                            {
                                if (orgInfo.SendConfirmationEmail == 0) //FB 2470
                                    m_emailFactory.sendICALforCISCOEndpoints(conf, "C", false, -1);// FB 1675
                            }
                    }
                }

            }
            catch (myVRMException e)
            {

                m_log.Error("vrmException", e);
                //obj.outXml = e.FetchErrorMsg();
                obj.outXml = ""; //FB 1881
                bRet = false;
            }
            catch (Exception e)
            {

                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);
                obj.outXml = ""; //FB 1881
                bRet = false;
            }
            return bRet;
        }

        #endregion

        #region CheckConferenceApproval
        /// <summary>
        /// Check whether the Conference having any approvals
        /// </summary>
        private bool CheckConferenceApproval(List<vrmConference> confList) //Recurrence Fixes for approval process
        {
            try
            {
                bool bRet = false;

                vrmConfApproval confApprove = new vrmConfApproval();

                IList<vrmConfApproval> confAppList = m_confDAO.GetPendingConfByInstanceID(confList[0].confid, confList[0].instanceid);

                if (confAppList.Count <= 0)
                    bRet = true;

                return bRet;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
        }
        #endregion

        //Code added for Cisco ICAL Fb 1602 - end

        //Code Added for Endpoint/MCU Status fix on Conference Console -- Start
        #region GetEndpointStatus
        /// <summary>
        /// GetEndpointStatus -Method to get the OnlineStatus for ConfID
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetEndpointStatus(ref vrmDataObject obj)
        {
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;

                node = xd.SelectSingleNode("//GetEndpointStatus/ConfID");
                string confid = node.InnerXml.Trim();

                CConfID Conf = new CConfID(confid);

                if (Conf.instance == 0)
                    Conf.instance = 1;

                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("confid", Conf.ID));
                criterionList.Add(Expression.Eq("instanceid", Conf.instance));

                List<vrmConfRoom> rList = m_IconfRoom.GetByCriteria(criterionList);

                obj.outXml += "<GetEndpointStatus>";
                obj.outXml += "<Endpoints>";
                foreach (vrmConfRoom cr in rList)
                {
                    obj.outXml += "<Endpoint>";
                    obj.outXml += "<Status>" + cr.OnlineStatus + "</Status>";
                    obj.outXml += "</Endpoint>";
                }
                obj.outXml += "</Endpoints>";
                obj.outXml += "</GetEndpointStatus>";

                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                //obj.outXml = string.Empty;
                obj.outXml = ""; //FB 1881
                return false;
            }
        }
        #endregion
        //End

        //FB 1772 - New Method added for Outlook Plug-in requirement
        #region DeleteRecurInstance
        /// <summary>
        /// Delete single instance of a recurring conference
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool DeleteRecurInstance(ref vrmDataObject obj)
        {
            Int32 confid = 0;
            int userid = 0;
            int orgid = 0;
            myVRMException myVRMEx = null;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;
                
                node = xd.SelectSingleNode("//login/userid");
                Int32.TryParse(node.InnerXml.Trim(), out userid);

                if (userid <= 0)
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/organizationID");
                Int32.TryParse(node.InnerXml.Trim(), out orgid);

                //if (orgid < 11) //Default orgid 11
                //{
                //    myVRMEx = new myVRMException(423);
                //    obj.outXml = myVRMEx.FetchErrorMsg();
                //    return false;
                //}
                
                node = xd.SelectSingleNode("//login/deleterecurinstance/confid");
                Int32.TryParse(node.InnerXml.Trim(), out confid);
                if (confid <= 11) //phantom conid 11
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/deleterecurinstance/reason");
                string reason = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//login/deleterecurinstance/confdate");
                string srtInstance = node.InnerXml.Trim();
                
                if (srtInstance == "")
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                DateTime insDate = DateTime.Now;
                DateTime.TryParse(srtInstance, out insDate);

                DateTime confFrom = new DateTime(insDate.Year, insDate.Month, insDate.Day, 0, 0, 0);
                DateTime confEnd = new DateTime(insDate.Year, insDate.Month, insDate.Day, 23, 59, 59);

                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("userid", userid));
                List<vrmUser> userList = m_vrmUserDAO.GetByCriteria(criterionList);
                vrmUser user = userList[0];

                timeZone.changeToGMTTime(user.TimeZone, ref confFrom);
                timeZone.changeToGMTTime(user.TimeZone, ref confEnd);

                criterionList = null;
                criterionList = new List<ICriterion>();

                ICriterion criterium = null;
                criterium = Expression.Le("confdate", confEnd);
                criterionList.Add(Expression.And(criterium, Expression.Ge("confEnd", confFrom)));
                criterionList.Add(Expression.Eq("confid", confid));
                
                List<vrmConference> confList = m_vrmConfDAO.GetByCriteria(criterionList, true);

                if (confList.Count <= 0)
                {
                    //obj.outXml = myVRMException.toXml("Invalid Opereation. Recurring instance does not exist.");
                    //FB 1881 start
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    //FB 1881 end
                    return false;
                }
                vrmConference conf = confList[0];

                obj.outXml = "<login>";
                obj.outXml += "<organizationID>"+ orgid +"</organizationID>";
                obj.outXml += "<userID>"+ userid +"</userID>";
                obj.outXml += "<delconference>";
                obj.outXml += "<conference>";
                obj.outXml += "<confID>" + conf.confid + "," + conf.instanceid + "</confID>";
                obj.outXml += "<reason>"+ reason +"</reason>";
                obj.outXml += "</conference>";
                obj.outXml += "</delconference>";
                obj.outXml += "</login>";
               
            }
            catch (myVRMException e)
            {

                m_log.Error("vrmException", e);
                //obj.outXml = myVRMException.toXml(e.Message);
                obj.outXml = ""; //FB 1881
                return false;
            }
            catch (Exception e)
            {

                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);
                obj.outXml = ""; //FB 1881
                return false;
            }
            return true;
        }

        #endregion

        #region DeleteConferenceByUID
        /// <summary>
        /// Delete single instance of a recurring conference
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>

        public bool DelRecurInstanceByUID(ref vrmDataObject obj)
        {
            Int32 confuid = 0;
            int userid = 0;
            int orgid = 0;
            myVRMException myVRMEx = null;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;

                node = xd.SelectSingleNode("//login/userid");
                Int32.TryParse(node.InnerXml.Trim(), out userid);

                if (userid <= 0)
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/organizationID");
                Int32.TryParse(node.InnerXml.Trim(), out orgid);

                //if (orgid < 11) //Default orgid 11
                //{
                //    myVRMEx = new myVRMException(423);
                //    obj.outXml = myVRMEx.FetchErrorMsg();
                //    return false;
                //}

                node = xd.SelectSingleNode("//login/delrecurinstancebyUID/confuniqueid");
                Int32.TryParse(node.InnerXml.Trim(), out confuid);
                if (confuid <= 0)
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }


                node = xd.SelectSingleNode("//login/delrecurinstancebyUID/reason");
                string reason = node.InnerXml.Trim();

                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("confnumname", confuid));
                List<vrmConference> confList = m_vrmConfDAO.GetByCriteria(criterionList, true);

                if (confList.Count <= 0)
                {
                    //obj.outXml = myVRMException.toXml("Invalid Opereation. Recurring instance does not exist.");
                    //FB 1881 start
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    //FB 1881 end
                    return false;
                }
                vrmConference conf = confList[0];
                
                obj.outXml = "<login>";
                obj.outXml += "<organizationID>" + orgid + "</organizationID>";
                obj.outXml += "<userID>" + userid + "</userID>";
                obj.outXml += "<delconference>";
                obj.outXml += "<conference>";
                obj.outXml += "<confID>" + conf.confid + "," + conf.instanceid + "</confID>";
                obj.outXml += "<reason>" + reason + "</reason>";
                obj.outXml += "</conference>";
                obj.outXml += "</delconference>";
                obj.outXml += "</login>";

            }
            catch (myVRMException e)
            {

                m_log.Error("vrmException", e);
                //obj.outXml = e.FetchErrorMsg();
                obj.outXml = ""; //FB 1881
                return false;
            }
            catch (Exception e)
            {

                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);
                obj.outXml = ""; //FB 1881
                return false;
            }
            return true;
        }

        #endregion

        #region EditRecurInstance
        /// <summary>
        /// Edit single instance of a recurring conference
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool EditRecurInstance(ref vrmDataObject obj)
        {
            Int32 confid = 0;
            int userid = 0;
            int orgid = 0;
            myVRMException myVRMEx = null;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;

                node = xd.SelectSingleNode("//editrecurinstance/conference/userID");
                Int32.TryParse(node.InnerXml.Trim(), out userid);

                if (userid <= 0)
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                //node = xd.SelectSingleNode("//editrecurinstance/organizationID");
                //Int32.TryParse(node.InnerXml.Trim(), out orgid);

                //if (orgid < 11) //Default orgid 11
                //{
                //    myVRMEx = new myVRMException(423);
                //    obj.outXml = myVRMEx.FetchErrorMsg();
                //    return false;
                //}

                node = xd.SelectSingleNode("//editrecurinstance/conference/confInfo/confID");
                CConfID Conf = new CConfID(node.InnerXml.Trim()); //FB 2287
                Int32.TryParse(Conf.ID.ToString(), out confid);
                if (confid <= 11) //phantom conid 11
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//editrecurinstance/confdate");
                string srtInstance = node.InnerXml.Trim();

                if (srtInstance == "")
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                DateTime insDate = DateTime.Now;
                DateTime.TryParse(srtInstance, out insDate);

                DateTime confFrom = new DateTime(insDate.Year, insDate.Month, insDate.Day, 0, 0, 0);
                DateTime confEnd = new DateTime(insDate.Year, insDate.Month, insDate.Day, 23, 59, 59);

                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("userid", userid));
                List<vrmUser> userList = m_vrmUserDAO.GetByCriteria(criterionList);
                vrmUser user = userList[0];

                timeZone.changeToGMTTime(user.TimeZone, ref confFrom);
                timeZone.changeToGMTTime(user.TimeZone, ref confEnd);

                criterionList = null;
                criterionList = new List<ICriterion>();

                ICriterion criterium = null;
                criterium = Expression.Le("confdate", confEnd);
                criterionList.Add(Expression.And(criterium, Expression.Ge("confEnd", confFrom)));
                criterionList.Add(Expression.Eq("confid", confid));

                List<vrmConference> confList = m_vrmConfDAO.GetByCriteria(criterionList, true);

                if (confList.Count <= 0)
                {
                    //obj.outXml = myVRMException.toXml("Invalid Opereation. Recurring instance does not exist.");
                    //FB 1881 start
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    //FB 1881 end
                    return false;
                }
                vrmConference conf = confList[0];

                node = xd.SelectSingleNode("//editrecurinstance/conference/confInfo/confID");
                node.InnerText = conf.confid + "," + conf.instanceid;

                node = xd.SelectSingleNode("//editrecurinstance/conference");
                obj.outXml = "<conference><organizationID>" + conf.orgId + "</organizationID>" + node.InnerXml + "</conference>";

            }
            catch (myVRMException e)
            {

                m_log.Error("vrmException", e);
                //obj.outXml = myVRMException.toXml(e.Message);
                obj.outXml = ""; //FB 1881
                return false;
            }
            catch (Exception e)
            {

                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);
                obj.outXml = ""; //FB 1881
                return false;
            }
            return true;
        }

        #endregion

        #region EditRecurInstanceByUID
        /// <summary>
        /// Edit single instance of a recurring conference
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>

        public bool EditRecurInstanceByUID(ref vrmDataObject obj)
        {
            Int32 confuid = 0;
            int userid = 0;
            int orgid = 0;
            myVRMException myVRMEx = null;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;

                node = xd.SelectSingleNode("//editrecurinstancebyUID/conference/userID");
                Int32.TryParse(node.InnerXml.Trim(), out userid);

                if (userid <= 0)
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                //node = xd.SelectSingleNode("//editrecurinstancebyUID/organizationID");
                //Int32.TryParse(node.InnerXml.Trim(), out orgid);

                //if (orgid < 11) //Default orgid 11
                //{
                //    myVRMEx = new myVRMException(423);
                //    obj.outXml = myVRMEx.FetchErrorMsg();
                //    return false;
                //}

                node = xd.SelectSingleNode("//editrecurinstancebyUID/confuniqueid");
                Int32.TryParse(node.InnerXml.Trim(), out confuid);
                if (confuid <= 0)
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("confnumname", confuid));
                List<vrmConference> confList = m_vrmConfDAO.GetByCriteria(criterionList, true);

                if (confList.Count <= 0)
                {
                    //obj.outXml = myVRMException.toXml("Invalid Opereation. Recurring instance does not exist.");
                    //FB 1881 start
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    //FB 1881 end
                    return false;
                }
                vrmConference conf = confList[0];

                node = xd.SelectSingleNode("//editrecurinstancebyUID/conference/confInfo/confID");
                node.InnerText = conf.confid + "," + conf.instanceid;

                node = xd.SelectSingleNode("//editrecurinstancebyUID/conference");
                obj.outXml = "<conference><organizationID>" + conf.orgId + "</organizationID>" + node.InnerXml + "</conference>";
            }
            catch (myVRMException e)
            {

                m_log.Error("vrmException", e);
                //obj.outXml = e.FetchErrorMsg();
                obj.outXml = ""; //FB 1881
                return false;
            }
            catch (Exception e)
            {

                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);
                obj.outXml = ""; //FB 1881
                return false;
            }
            return true;
        }

        #endregion
        //FB 1722 End

        #region UpdateICalID
        /// <summary>
        /// Update ICAl ID
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>

        public bool UpdateICalID(ref vrmDataObject obj)
        {
            bool bRet = true;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;
                node = xd.SelectSingleNode("//UpdateIcalID/UserID");
                int userid = Int32.Parse(node.InnerXml.Trim());
                node = xd.SelectSingleNode("//UpdateIcalID/ConfID");
                string confid = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//UpdateIcalID/IcalID");
                string icalID = "";
                if (node != null)
                    icalID = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//UpdateIcalID/organizationID");
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);

                if (organizationID < 11)
                    organizationID = defaultOrgId;

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                CConfID Conf = new CConfID(confid);

                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("confid", Conf.ID));

                if (Conf.instance > 0)
                    criterionList.Add(Expression.Eq("instanceid", Conf.instance));
                //else
                //    criterionList.Add(Expression.Eq("instanceid",1));


                List<vrmConference> conf = m_vrmConfDAO.GetByCriteria(criterionList, true);

                if (conf.Count > 0)
                {
                    for (int i = 0; i < conf.Count; i++)
                        conf[i].IcalID = icalID;

                }

                m_vrmConfDAO.SaveOrUpdateList(conf);

                bool Isreccuring = false;
                bool CheckConfApp = false; //FB 2141
                CheckConfApp = CheckConferenceApproval(conf);

                if (conf[0].recuring == 1)
                    Isreccuring = true;
                if (conf[0].IcalID != "" && orgInfo.SendApprovalIcal > 0)
                {

                    if ((!CheckConfApp) && conf[0].ConfOrigin != 0 && orgInfo.SendConfirmationEmail ==0 ) //FB 2056 //FB 2470
                        m_emailFactory.sendICALforParticipants(conf, "C", true, true, Isreccuring);
                }
                if (CheckConfApp) //FB 2141
                {
                    if (orgInfo.PluginConfirmations > 0 && orgInfo.SendConfirmationEmail ==0 ) //FB 2470
                        m_emailFactory.SendConfirmationEmailsforPlugin(conf);  //Invitation  Email for Plugin  FB 2141\
                }

                obj.outXml = "<success></success>";
            }
            catch (myVRMException e)
            {

                m_log.Error("vrmException", e);
                //obj.outXml = e.FetchErrorMsg();
                obj.outXml = ""; //FB 1881
                bRet = false;
            }
            catch (Exception e)
            {

                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);
                obj.outXml = ""; //FB 1881
                bRet = false;
            }
            return bRet;
        }

        #endregion  //FB 1782

        //FB 1782
        #region DeleteParticipantICAL
        /// <summary>
        /// Delete ICAl on Delete conference
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>

        public bool DeleteParticipantICAL(ref vrmDataObject obj)
        {
            bool bRet = true;
            bool Isreccuring = false;
            bool sendIcal = false;// SendIcal for Plugins
            try
            {
                //FB 1830 & FB 1915 starts
                List<vrmConfRoom> rList = new List<vrmConfRoom>();
                List<vrmConfUser> uList = new List<vrmConfUser>();
                List<vrmMCU> mcuList = new List<vrmMCU>();
                //FB 1830 & FB 1915 ends

                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;
                node = xd.SelectSingleNode("//DeleteParticipantICAL/UserID");
                int userid = Int32.Parse(node.InnerXml.Trim());
                node = xd.SelectSingleNode("//DeleteParticipantICAL/ConfID");
                string confid = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//DeleteParticipantICAL/organizationID");
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);

                if (organizationID < 11)
                    organizationID = defaultOrgId;

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                CConfID Conf = new CConfID(confid);

                List<ICriterion> criterionList = new List<ICriterion>();
				//FB 2075 Starts
                List<ICriterion> ConfcriterionList = new List<ICriterion>(); 
                List<ICriterion> RoomcriterionList = new List<ICriterion>(); 
                criterionList.Add(Expression.Eq("confid", Conf.ID));
                ConfcriterionList.Add(Expression.Eq("confid", Conf.ID)); 
                RoomcriterionList.Add(Expression.Eq("confid", Conf.ID));

                if (Conf.instance > 0)
                {
                    criterionList.Add(Expression.Eq("instanceid", Conf.instance));
                    ConfcriterionList.Add(Expression.Eq("instanceid", Conf.instance));
                    RoomcriterionList.Add(Expression.Eq("instanceid", Conf.instance));
                }
                else //FB 2495
                {
                    Isreccuring = true;
                    criterionList.Add(Expression.Eq("instanceid", 1));
                    ConfcriterionList.Add(Expression.Eq("instanceid", 1));
                    RoomcriterionList.Add(Expression.Eq("instanceid", 1));
                }

                DateTime serverTime = DateTime.Now;
                timeZone.changeToGMTTime(sysSettings.TimeZone, ref serverTime);
                serverTime = serverTime.AddMinutes(-5); // Immd Conf Issue
                ConfcriterionList.Add(Expression.Ge("confdate", serverTime));
                RoomcriterionList.Add(Expression.Ge("StartDate", serverTime));
               
                List<vrmConference> conf = m_vrmConfDAO.GetByCriteria(ConfcriterionList, true);
				//FB 2075 Ends
                if (conf[0].IcalID != "")// SendIcal for Plugins
                    sendIcal = true;

                // SendIcal for Plugins
                if (orgInfo.SendIcal > 0 || sendIcal)
                {
                    if (CheckConferenceApproval(conf))
                    {
                        if(orgInfo.SendConfirmationEmail ==0) //FB 2470
                            m_emailFactory.sendICALforParticipants(conf, "D", false, sendIcal, Isreccuring);
                    }
                    else if ((orgInfo.SendApprovalIcal > 0) && conf[0].ConfOrigin != 0) // FB 2056
                    {
                        if (orgInfo.SendConfirmationEmail == 0) //FB 2470
                            m_emailFactory.sendICALforParticipants(conf, "D", true, sendIcal, Isreccuring);
                    }

                }

                //FB 1915 starts
                //FB 1830 start-Sending mail when conference is deleted
                rList = m_IconfRoom.GetByCriteria(RoomcriterionList); //FB 2075
                uList = m_IconfUser.GetByCriteria(criterionList);
                loadMCUInfo(ref mcuList, rList, uList);
                conf[0].mcuList = mcuList;
                m_emailFactory.confRoomList = rList;
                m_emailFactory.confUserLst = uList;
                m_emailFactory.SendConfirmationEmails(conf);
                //FB 1915 ends
            }
            catch (myVRMException e)
            {

                m_log.Error("vrmException", e);
                //obj.outXml = e.FetchErrorMsg();
                obj.outXml = ""; //FB 1881
                bRet = false;
            }
            catch (Exception e)
            {

                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);
                obj.outXml = ""; //FB 1881
                bRet = false;
            }
            return bRet;
        }

        #endregion  //FB 1782

        #region CreateParticipantICALOnApproval
        /// <summary>
        /// On approval of the Conference send ICAL emails.
        /// This method needs to be merged with SetApproveConference while converting Com to .Net.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>

        public bool CreateParticipantICALOnApproval(ref vrmDataObject obj)
        {
            bool bRet = true;
            bool sendIcal = false;
            bool Isreccuring = false;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;
                node = xd.SelectSingleNode("//approveConference/userID");
                int userid = Int32.Parse(node.InnerXml.Trim());
                XmlNodeList confs = xd.SelectNodes("//approveConference/conferences/conference");


                node = xd.SelectSingleNode("//approveConference/organizationID");
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);

                if (organizationID < 11)
                    organizationID = defaultOrgId;

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                if (confs != null)
                {
                    foreach (XmlNode nd in confs)
                    {
                        node = nd.SelectSingleNode("confID");
                        string confid = node.InnerXml.Trim();


                        CConfID Conf = new CConfID(confid);

                        List<ICriterion> criterionList = new List<ICriterion>();
                        criterionList.Add(Expression.Eq("confid", Conf.ID));

                        if (Conf.instance > 0)
                            criterionList.Add(Expression.Eq("instanceid", Conf.instance));
                        else
                            Isreccuring = true;
                        //FB 2075 Starts
                        DateTime serverTime = DateTime.Now;
                        timeZone.changeToGMTTime(sysSettings.TimeZone, ref serverTime);
                        serverTime = serverTime.AddMinutes(-5); // Immd Conf Issue
                        criterionList.Add(Expression.Ge("confdate", serverTime));
                        //FB 2075 Ends
                        List<vrmConference> conf = m_vrmConfDAO.GetByCriteria(criterionList, true);

                        if (orgInfo.SendIcal > 0 || conf[0].IcalID != "")
                        {
                            if (CheckConferenceforDenial(conf))
                            {
                                if (conf[0].IcalID != "")
                                    sendIcal = true;

                                if (((conf[0].IcalID != "" || orgInfo.SendApprovalIcal > 0) && conf[0].ConfOrigin != 0) && orgInfo.SendConfirmationEmail ==0) //FB 2056 //FB 2470
                                    m_emailFactory.sendICALforParticipants(conf, "D", true, sendIcal, Isreccuring);
                            }
                            else if (CheckConferenceApproval(conf))
                            {
                                if (conf[0].IcalID != "" && orgInfo.SendApprovalIcal > 0)
                                    sendIcal = true;

                                if(orgInfo.SendConfirmationEmail ==0) //FB 2470
                                    m_emailFactory.sendICALforParticipants(conf, "C", false, sendIcal, Isreccuring);
                            }
                        }
                    }
                }

            }
            catch (myVRMException e)
            {

                m_log.Error("vrmException", e);
                //obj.outXml = e.FetchErrorMsg();
                obj.outXml = ""; //FB 1881
                bRet = false;
            }
            catch (Exception e)
            {

                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);
                obj.outXml = ""; //FB 1881
                bRet = false;
            }
            return bRet;
        }

        #endregion //FB 1782

        #region CheckConferenceforDenial
        /// <summary>
        /// Check whether the Conference having any approvals
        /// </summary>
        private bool CheckConferenceforDenial(List<vrmConference> confList) //Recurrence Fixes for approval process
        {
            try
            {
                bool bRet = false;
                if (confList.Count > 0)
                {

                    vrmConfApproval confApprove = new vrmConfApproval();

                    IList<vrmConfApproval> confAppList = m_confDAO.GetDeniedConfByInstanceID(confList[0].confid, confList[0].instanceid);

                    if (confAppList.Count > 0)
                        bRet = true;
                }

                return bRet;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
        }
        #endregion

        //FB 1782

        //Methods added for FB 1830 - start
        #region Check approval Entity
        /// <summary>
        /// Check Approval entity and send email
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>

        public bool CheckApprovalEntity(ref vrmDataObject obj)
        {
            bool bRet = true;
            List<vrmConfApproval> ConfApprovalList;
            IConfApprovalDAO IApprover;
            List<ICriterion> criterionList;
            List<ICriterion> ConfcriterionList;//FB 2075
            List<vrmConference> conf;
            vrmUser user = new vrmUser();
            try
            {
                List<vrmConfRoom> rList = new List<vrmConfRoom>();
                List<vrmConfUser> uList = new List<vrmConfUser>();
                List<vrmMCU> mcuList = new List<vrmMCU>();
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;
                XmlNodeList confs = xd.SelectNodes("//approveConference/conferences/conference");
                XmlNode nd;
                if (confs != null)
                {
                    for(int i=0;i< confs.Count;i++)
                    {
                        nd = confs[i];
                        node = nd.SelectSingleNode("confID");
                        string confid = node.InnerXml.Trim();
                        //FB 1830 Email - start
                        node = nd.SelectSingleNode("instanceType");
                        if(node != null)
                            m_emailFactory.instanceType = node.InnerXml.Trim();
                        //FB 1830 Email - end
                        node = nd.SelectSingleNode("partyInfo/response/level");
                        int level = Int32.Parse(node.InnerXml.Trim());
                        IApprover = m_confDAO.GetConfApprovalDao();
                        CConfID Conf = new CConfID(confid);
                        criterionList = new List<ICriterion>();
                        //FB 2075 Starts
                        ConfcriterionList = new List<ICriterion>();
                        criterionList.Add(Expression.Eq("confid", Conf.ID));
                        ConfcriterionList.Add(Expression.Eq("confid", Conf.ID));
                        if (Conf.instance > 0)
                        {
                            criterionList.Add(Expression.Eq("instanceid", Conf.instance));
                            ConfcriterionList.Add(Expression.Eq("instanceid", Conf.instance));
                        }
                        DateTime serverTime = DateTime.Now;
                        timeZone.changeToGMTTime(sysSettings.TimeZone, ref serverTime);
                        serverTime = serverTime.AddMinutes(-5); // Immd Conf Issue
                        ConfcriterionList.Add(Expression.Ge("confdate", serverTime));

                        conf = m_vrmConfDAO.GetByCriteria(ConfcriterionList, true);
                        if (conf.Count > 1)
                        {
                            conf.RemoveRange(1, conf.Count - 1);
                            if (criterionList.Count <= 1)
                                criterionList.Add(Expression.Eq("instanceid", conf[0].instanceid));
                        }
                        //FB 2075 Ends
                        rList = m_IconfRoom.GetByCriteria(criterionList);
                        uList = m_IconfUser.GetByCriteria(criterionList);
                        loadMCUInfo(ref mcuList, rList, uList);
                        conf[0].mcuList = mcuList;
                        m_emailFactory.confRoomList = rList; 
                        m_emailFactory.confUserLst = uList;
                        if (CheckConferenceApproval(conf) || CheckConferenceforDenial(conf))
                        {
                            //send conference invitations or cancellations mails
                            m_emailFactory.SendConfirmationEmails(conf); //FB 1830

                            if (conf[0].IcalID != "" && conf[0].ConfMode != 4)
                            {
                                //Invitation  Email for Plugin FB 2141
                                organizationID = conf[0].orgId;

                                if (organizationID < 11)
                                    organizationID = defaultOrgId;

                                if (orgInfo == null)
                                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                                if (orgInfo.PluginConfirmations > 0 && orgInfo.SendConfirmationEmail ==0)//FB 2470
                                    m_emailFactory.SendConfirmationEmailsforPlugin(conf);

                            }//Invitation  Email for Plugin FB 2141
                           
                            /* Following codes commented for FB 1830
                           //m_emailFactory.emailLocations(conf);
                           //m_emailFactory.emailParticipants(conf);
                           //if (conf[0].conftype != 7)
                           // {
                           //     if (conf[0].mcuList.Count > 0)
                           //         m_emailFactory.sendMCUAdminEmail(conf[0]);
                           // }
                           */
                            continue;
                        }
                        if (conf[0].deleted != 1)
                        {
                            criterionList.Add(Expression.Eq("decision", 0));
                            ConfApprovalList = IApprover.GetByCriteria(criterionList, true);
                            for (int app = 0; app < ConfApprovalList.Count; app++)
                            {
                                user = m_vrmUserDAO.GetByUserId(ConfApprovalList[app].approverid); //FB 1948
                                if (ConfApprovalList[app].entitytype != level)
                                {
                                    switch (ConfApprovalList[app].entitytype)
                                    {
                                        case (int)LevelEntity.ROOM:
                                        case (int)LevelEntity.MCU:
                                        case (int)LevelEntity.SYSTEM:
                                            m_emailFactory.sendLevelEmail(conf[0], ConfApprovalList[app].entitytype, user);
                                            break;
                                        // no more levels to set;
                                        default:
                                            // when conference is completely
                                            conf[0].status = vrmConfStatus.Scheduled;
                                            break;
                                    }
                                }
                            }
                        }
                    }
                }
                obj.outXml = "<success>1</success>";
            }

            catch (myVRMException e)
            {

                m_log.Error("vrmException", e);
                //obj.outXml = e.FetchErrorMsg();
                obj.outXml = ""; //FB 1881
                bRet = false;
            }
            catch (Exception e)
            {

                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);
                obj.outXml = ""; //FB 1881
                bRet = false;
            }
            return bRet;
        }

        #endregion

        #region SendPartyReminder
        /// <summary>
        /// SendPartyReminder
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SendPartyReminder(ref vrmDataObject obj)
        {
            bool bRet = true;
            int partyid = 0;
            int userid = 0;
            string confid = "";

            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;
                node = xd.SelectSingleNode("//login/UserID");
                userid = Int32.Parse(node.InnerXml.Trim());
                node = xd.SelectSingleNode("//login/ConfID");
                confid = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//login/PartyID");
                partyid = Int32.Parse(node.InnerXml.Trim());

                node = xd.SelectSingleNode("//login/organizationID");
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();

                CConfID Conf = new CConfID(confid);

                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("confid", Conf.ID));

                if (Conf.instance > 0)
                    criterionList.Add(Expression.Eq("instanceid", Conf.instance));
                //FB 2075 Starts
                DateTime serverTime = DateTime.Now;
                timeZone.changeToGMTTime(sysSettings.TimeZone, ref serverTime);
                serverTime = serverTime.AddMinutes(-5); // Immd Conf Issue
                criterionList.Add(Expression.Ge("confdate", serverTime));
                //FB 2075 Ends
                List<vrmConference> conf = m_vrmConfDAO.GetByCriteria(criterionList, true);

                m_emailFactory.SendReminderEmails(conf,partyid);

                obj.outXml = "<success></success>";
            }
            catch (myVRMException e)
            {

                m_log.Error("vrmException", e);
                //obj.outXml = e.FetchErrorMsg();
                obj.outXml = ""; //FB 1881
                bRet = false;
            }
            catch (Exception e)
            {

                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);
                obj.outXml = ""; //FB 1881
                bRet = false;
            }
            return bRet;
        }

        #endregion  //FB 1782
        //Methods added for FB 1830 - end

        //FB 2027 - Starts
        #region DeleteTerminal
        /// <summary>
        /// DeleteTerminal (COM to .Net conversion)
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool DeleteTerminal(ref vrmDataObject obj)
        {
            bool bRet = true;
            List<ICriterion> criterionList = new List<ICriterion>();

            //FB 2528 Starts
            List<ICriterion> criterionEPList = new List<ICriterion>(); 
            int cascadeLink = 0;
            string cascadeBridges = "";
            //FB 2528 Ends

            int userid = 0, endpointid = 0, type = 0;
            String confid = "";
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out userid);

                node = xd.SelectSingleNode("//login/confID");
                if (node != null)
                    confid=node.InnerText.Trim();

                node = xd.SelectSingleNode("//login/endpointID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out endpointid);

                node = xd.SelectSingleNode("//login/terminalType");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out type);

                List<vrmConfUser> confuser = new List<vrmConfUser>();
                List<vrmConfRoom> confroom = new List<vrmConfRoom>();
                List<vrmConfCascade> confcascade = new List<vrmConfCascade>();
                CConfID Conf = new CConfID(confid);

                criterionList.Add(Expression.Eq("confid", Conf.ID));
                if (Conf.instance > 0)
                    criterionList.Add(Expression.Eq("instanceid", Conf.instance));

                if ((type == 1) || (type == 3)) //User or Guest Endpoint
                {
                    criterionList.Add(Expression.Eq("userid", endpointid));
                    confuser = m_IconfUser.GetByCriteria(criterionList);
                    foreach (vrmConfUser userEpt in confuser)
                        m_IconfUser.Delete(userEpt);
                }
                else if (type == 2) //Room Endpoint
                {
                    criterionList.Add(Expression.Eq("roomId", endpointid));
                    confroom = m_IconfRoom.GetByCriteria(criterionList);
                    //FB 2528 - Starts
                    foreach (vrmConfRoom roomEpt in confroom)
                        m_IconfRoom.Delete(roomEpt); 
                    
                    /*foreach (vrmConfRoom roomEpt in confroom)
                    {
                        roomEpt.connectStatus = 3;
                        m_IconfRoom.SaveOrUpdate(roomEpt);
                    }*/
                    //FB 2528 - End
                }
                //else if (type == 4) //Cascade Endpoint //Commented for FB 2528 - Cascade Delete Issue
                //{
                //    criterionList.Add(Expression.Eq("uId", endpointid));
                //    confcascade = m_IconfCascade.GetByCriteria(criterionList);
                //    foreach (vrmConfCascade cascadeEpt in confcascade)
                //    {
                //        cascadeEpt.connectStatus = 3;
                //        m_IconfCascade.SaveOrUpdate(cascadeEpt);
                //    }
                //}
                //FB 2528 Starts
                criterionEPList.Add(Expression.Eq("confid", Conf.ID));
                if (Conf.instance > 0)
                    criterionEPList.Add(Expression.Eq("instanceid", Conf.instance));
                criterionEPList.Add(Expression.Not(Expression.Eq("connectStatus", 3)));
                confroom = m_IconfRoom.GetByCriteria(criterionEPList);
                List<vrmConfCascade> confCascade = m_IconfCascade.GetByCriteria(criterionEPList);

                criterionEPList.Add(Expression.Eq("invitee", 1));
                confuser = m_IconfUser.GetByCriteria(criterionEPList);

                cascadeLink = 0; cascadeBridges = "";
                for (int i = 0; i < confroom.Count; i++)
                {
                    if (!cascadeBridges.Contains(confroom[i].bridgeid.ToString()))
                    {
                        cascadeBridges += "," + confroom[i].bridgeid.ToString();
                        cascadeLink++;
                    }
                }

                for (int j = 0; j < confuser.Count; j++)
                {
                    if (!cascadeBridges.Contains(confuser[j].bridgeid.ToString()))
                    {
                        cascadeBridges += "," + confuser[j].bridgeid.ToString();
                        cascadeLink++;
                    }
                }

                if (cascadeLink < 2)
                {
                    for (int i = 0; i < confCascade.Count; i++)
                        m_IconfCascade.Delete(confCascade[i]);
                }
                //FB 2528 Ends
                   
                obj.outXml = "<success>1</success>";
                return true;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

        #region GetTemplateList
        /// <summary>
        /// GetTemplateList (COM to .Net conversion) 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetTemplateList(ref vrmDataObject obj)
        {
            bool bRet = true;
            bool singleDeptMode = true;
            List<ICriterion> criterionList = new List<ICriterion>();
            List<ICriterion> criterionList1 = new List<ICriterion>();
            myVRMException myVRMEx = new myVRMException();
            StringBuilder outXML = new StringBuilder();
            int userid = 0, sortby = 0;
            String orgid = "";
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out userid);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/organizationID");
                if (node != null)
                    orgid = node.InnerText.Trim();
                organizationID = defaultOrgId;

                //FB 2274 Starts
                node = xd.SelectSingleNode("//login/multisiloOrganizationID");
                string multisiloOrgID = "";
                int mutliOrgID = 0;
                if (node != null)
                    multisiloOrgID = node.InnerXml.Trim();
                Int32.TryParse(multisiloOrgID, out mutliOrgID);
                if (mutliOrgID > 11)
                    orgid = multisiloOrgID;
                //FB 2274 Ends
                Int32.TryParse(orgid, out organizationID);

                if (organizationID < defaultOrgId)
                {
                    myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/sortBy");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out sortby);

                if (userid <= 0 || sortby < 0)
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);
                if (orgInfo != null)
                    multiDepts = orgInfo.MultipleDepartments;

                vrmUser user = new vrmUser();
                if (multiDepts == 1)
                {
                    singleDeptMode = false;
                    user = m_vrmUserDAO.GetByUserId(userid);
                }

                user = m_vrmUserDAO.GetByUserId(userid);
                vrmUserRoles usrRole = m_IUserRolesDao.GetById(user.roleID); //crossaccess

                criterionList.Add(Expression.Eq("orgId", organizationID));//If User is SuperAdmin

                if (singleDeptMode)
                {
                    if (!user.isSuperAdmin())
                    {
                        criterionList.Add(Expression.Or(Expression.Eq("TMPOwner", userid), Expression.Eq("TMPPublic", 1)));
                    }
                }
                else
                {
                    if (!(user.isSuperAdmin() && usrRole.crossaccess == 1))
                    {
                        if (user.isSuperAdmin() && usrRole.crossaccess == 0) //If User is OrgAdmin
                        {
                            ArrayList userId = new ArrayList();
                            vrmUser superuser = m_vrmUserDAO.GetByUserId(11);
                            criterionList1.Add(Expression.Eq("Admin", 2));
                            criterionList1.Add(Expression.Eq("roleID", superuser.roleID));
                            List<vrmUser> userList = m_vrmUserDAO.GetByCriteria(criterionList1);
                            for (int i = 0; i < userList.Count; i++)
                            {
                                vrmUser users = userList[i];
                                userId.Add(users.userid);
                            }
                            criterionList.Add(Expression.Or(Expression.Not(Expression.In("TMPOwner", userId)), Expression.Eq("TMPPublic", 1)));
                        }
                        else
                        {
                            criterionList.Add(Expression.Or(Expression.Eq("TMPOwner", userid), Expression.Eq("TMPPublic", 1))); //If its Gereral Users
                        }
                    }
                }

                if (sortby == 1)
                    m_vrmTempDAO.addOrderBy(Order.Asc("TmpName"));
                else if (sortby == 2)
                    m_vrmTempDAO.addOrderBy(Order.Asc("TMPOwner"));
                else if (sortby == 3)
                    m_vrmTempDAO.addOrderBy(Order.Asc("TMPPublic"));
                else
                    m_vrmTempDAO.addOrderBy(Order.Asc("Orderid"));

                List<vrmTemplate> tempresult = m_vrmTempDAO.GetByCriteria(criterionList);

                outXML.Append("<templates>");
                outXML.Append("<sortBy>" + sortby + "</sortBy>");
                for (int j = 0; j < tempresult.Count; j++)
                {
                    vrmTemplate temp = tempresult[j];
                    vrmUser tempowner = m_vrmUserDAO.GetByUserId(temp.TMPOwner);
                    if (tempowner != null)
                    {
                        outXML.Append("<template>");
                        outXML.Append("<ID>" + temp.tmpId + "</ID>");
                        outXML.Append("<name>" + temp.TmpName + "</name>");
                        outXML.Append("<public>" + temp.TMPPublic + "</public>");
                        outXML.Append("<owner>");
                        outXML.Append("<ownerID>" + temp.TMPOwner + "</ownerID>");
                        outXML.Append("<firstName>" + tempowner.FirstName + "</firstName>");
                        outXML.Append("<lastName>" + tempowner.LastName + "</lastName>");
                        outXML.Append("</owner>");
                        outXML.Append("<description>" + temp.TMPDescription + "</description>");
                        outXML.Append("</template>");
                    }
                }
                outXML.Append("</templates>");

                obj.outXml = outXML.ToString();
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

        #region PartyInvitation
        /// <summary>
        /// PartyInvitation (COM to .Net conversion)  
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool PartyInvitation(ref vrmDataObject obj)
        {
            bool bRet = true;
            vrmConfUser confusr = new vrmConfUser();
            myVRMException myVRMEx = new myVRMException();
            List<ICriterion> criterionList = new List<ICriterion>();
            int userid = 0, confid = 0, instanceid = 0;
            int mode = 0; //mode = 1(AcceptInvite); mode = 2(RejectInvite);
            String confids = "";
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node = null;

                node = xd.SelectSingleNode("//party/userID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out userid);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//party/confID");
                if (node != null)
                    confids = node.InnerText.Trim();
                if (confids.Length <= 0)
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                CConfID ConfMode = new CConfID(confids);
                confid = ConfMode.ID;
                instanceid = ConfMode.instance;

                node = xd.SelectSingleNode("//party/mode");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out mode);//If mode is 1 - AcceptInvite; If mode is 2 - RejectInvite;

                criterionList.Add(Expression.Eq("userid", userid));
                criterionList.Add(Expression.Eq("confid", confid));
                if (instanceid > 0)
                    criterionList.Add(Expression.Eq("instanceid", instanceid));
                List<vrmConfUser> confuser1 = m_IconfUser.GetByCriteria(criterionList);
                for (int i = 0; i < confuser1.Count; i++)
                {
                    confusr = confuser1[i];
                    confusr.status = mode;
                    m_IconfUser.SaveOrUpdate(confusr);
                }
                obj.outXml = "<success>1</success>";
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

        # region TerminateConference
        /// <summary>
        /// TerminateConference (COM to .Net conversion)
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool TerminateConference(ref vrmDataObject obj)
        {
            bool bRet = true;
            string outXml = string.Empty;
            int userid = 0, confid = 0, instanceid = 0;
            myVRMException myVRMEx = new myVRMException();
            List<ICriterion> criterionList = new List<ICriterion>();
            String confids = "";
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node = null;
                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out userid);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                if (userid <= 0)
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                node = xd.SelectSingleNode("//login/organizationID");
                string orgid = "";
                if (node != null)
                    orgid = node.InnerText.Trim();
                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);
                if (organizationID < defaultOrgId)
                {
                    myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                node = xd.SelectSingleNode("//login/conferenceID");
                if (node != null)
                    confids = node.InnerText.Trim();
                if (confids.Length <= 0)
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                CConfID ConfMode = new CConfID(confids);
                confid = ConfMode.ID;
                instanceid = ConfMode.instance;
                if (instanceid == 0)
                    instanceid = 1;
                criterionList.Add(Expression.Eq("confid", confid));
                if (instanceid > 0)
                    criterionList.Add(Expression.Eq("instanceid", instanceid));
                List<vrmConference> confterm = m_vrmConfDAO.GetByCriteria(criterionList, true);
                for (int i = 0; i < confterm.Count; i++)
                {
                    confterm[i].deleted = 2;
                    confterm[i].status = 3;
                    m_vrmConfDAO.SaveOrUpdate(confterm[i]);
                }

                outXml = "<success>1</success>";
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

        #region DeleteTemplate
        /// <summary>
        /// DeleteBridge (COM to .NET Convertion)
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        public bool DeleteTemplate(ref vrmDataObject obj)
        {
            bool bRet = true;
            List<ICriterion> criterionList = new List<ICriterion>();
            List<ICriterion> criterionList2 = new List<ICriterion>();
            myVRMException myVRMEx = null;
            int userid = 0;
            int tempid = 0;

            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;
                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out userid);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                node = xd.SelectSingleNode("//login/templates/template/templateID");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out tempid);
                else
                {
                    myVRMEx = new myVRMException(200);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                if ((userid <= 0) || (tempid <= 0))
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                criterionList.Add(Expression.Eq("tmpId", tempid));
                List<vrmTemplate> tempList = m_vrmTempDAO.GetByCriteria(criterionList);
                foreach (vrmTemplate tmpList in tempList)
                    m_vrmTempDAO.Delete(tmpList);

                List<vrmTempRoom> tempRoom = m_TempDAO.GetByCriteria(criterionList);
                foreach (vrmTempRoom tmpRoomList in tempRoom)
                    m_TempDAO.Delete(tmpRoomList);

                List<vrmTempAdvAvParams> tempAdvAvParams = m_TempAdvAvParamsDAO.GetByCriteria(criterionList);
                foreach (vrmTempAdvAvParams tmpAdvAvList in tempAdvAvParams)
                    m_TempAdvAvParamsDAO.Delete(tmpAdvAvList);


                criterionList2.Add(Expression.Eq("TmpID", tempid));
                List<vrmTempUser> tempUser = m_TempUserDAO.GetByCriteria(criterionList2);
                foreach (vrmTempUser tmpUsrList in tempUser)
                    m_TempUserDAO.Delete(tmpUsrList);

                List<vrmTempGroup> tempGroup = m_TempGroupDAO.GetByCriteria(criterionList2);
                foreach (vrmTempGroup tmpGrpList in tempGroup)
                    m_TempGroupDAO.Delete(tmpGrpList);

                vrmUser usr = m_vrmUserDAO.GetByUserId(userid);
                usr.DefaultTemplate = 0;
                m_vrmUserDAO.Update(usr);

            }

            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

		#region CounterInvite
        /// <summary>
        /// CounterInvite(COM to .Net)
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool CounterInvite(ref vrmDataObject obj)
        {
            bool bRet = true;
            int userid = 0;
            string confid = "";
            int orgid = 0;
            int instanceid = 0;
            myVRMException myVRMEx = null;
            try
            {
                List<ICriterion> criterionList = new List<ICriterion>();
                List<ICriterion> criterionList1 = new List<ICriterion>();
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;
                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out userid);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                node = xd.SelectSingleNode("//login/organizationID");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out orgid);
                else
                {
                    myVRMEx = new myVRMException(200);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                node = xd.SelectSingleNode("//login/confID");
                if (node != null)
                    confid = node.InnerXml.Trim();
                if ((userid <= 0) || (orgid <= 0))
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                CConfID Cconf = new CConfID(confid);
                confid = Convert.ToString(Cconf.ID);
                instanceid = Cconf.instance;

                criterionList.Add(Expression.Eq("Deleted", 0));
                criterionList.Add(Expression.Eq("userid", userid));
                List<vrmUser> usrlist = m_vrmUserDAO.GetByCriteria(criterionList);
                List<vrmGuestUser> Gusrlist = m_vrmGuestDAO.GetByCriteria(criterionList);
                if (usrlist.Count > 0 || Gusrlist.Count > 0)
                {
                    criterionList1.Add(Expression.Eq("confid", Int32.Parse(confid)));
                    criterionList1.Add(Expression.Eq("userid", userid));
                    if (instanceid != 0)
                    {
                        criterionList1.Add(Expression.Eq("instanceid", instanceid));
                    }
                    List<vrmConfUser> cfusrlist = m_IconfUser.GetByCriteria(criterionList1);
                    if (cfusrlist.Count > 0)
                    {
                        obj.outXml = "<success>1</success>";
                        return true;
                    }
                }
                myVRMEx = new myVRMException(532);
                obj.outXml = myVRMEx.FetchErrorMsg();
                return false;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = "";
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }

        #endregion

        #region DisplayTerminal
        /// <summary>
        /// DisplayTerminal (COM to .Net conversion)
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool DisplayTerminal(ref vrmDataObject obj)
        {
            bool bRet = true;
            string outXml = string.Empty;
            int userid = 0, confid = 0, instanceid = 0;
            myVRMException myVRMEx = new myVRMException();
            List<ICriterion> criterionList = new List<ICriterion>();
            String confids = "", terminalType = "";
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node = null;
                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out userid);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                if (userid <= 0)
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                node = xd.SelectSingleNode("//login/organizationID");
                string orgid = "";
                if (node != null)
                    orgid = node.InnerText.Trim();
                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);
                if (organizationID < defaultOrgId)
                {
                    myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                node = xd.SelectSingleNode("//login/confID");
                if (node != null)
                    confids = node.InnerText.Trim();
                if (confids.Length <= 0)
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                CConfID ConfMode = new CConfID(confids);
                confid = ConfMode.ID;
                instanceid = ConfMode.instance;
                if (instanceid == 0)
                    instanceid = 1;

                int endpointID = 0, displayLayout = 0, displayLayoutAll = 0;//FB 2584
                node = xd.SelectSingleNode("//login/endpointID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out endpointID);
                node = xd.SelectSingleNode("//login/terminalType");
                if (node != null)
                    terminalType = node.InnerText.Trim();
                node = xd.SelectSingleNode("//login/displayLayout");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out displayLayout);

                node = xd.SelectSingleNode("//login/displayLayoutAll");//FB 2584
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out displayLayoutAll);

                criterionList.Add(Expression.Eq("confid", confid));
                if (instanceid > 0)
                    criterionList.Add(Expression.Eq("instanceid", instanceid));
                if (terminalType == "0" && endpointID == 0) //FB 2584
                {
                    List<vrmConfAdvAvParams> ConfAdvAvParams = m_confAdvAvParamsDAO.GetByCriteria(criterionList);
                    for (int i = 0; i < ConfAdvAvParams.Count; i++)
                        ConfAdvAvParams[i].videoLayoutID = displayLayout;
                    m_confAdvAvParamsDAO.SaveOrUpdateList(ConfAdvAvParams);
                }
                if (terminalType == "2")
                {
                    criterionList.Add(Expression.Eq("roomId", endpointID));
                    List<vrmConfRoom> confroom = m_IconfRoom.GetByCriteria(criterionList);
                    for (int i = 0; i < confroom.Count; i++)
                        confroom[i].layout = displayLayout;
                    m_IconfRoom.SaveOrUpdateList(confroom);
                }
                else if (terminalType == "4")
                {
                    criterionList.Add(Expression.Eq("uId", endpointID));
                    List<vrmConfCascade> confCascade = m_IconfCascade.GetByCriteria(criterionList);
                    for (int i = 0; i < confCascade.Count; i++)
                        confCascade[i].layout = displayLayout;
                    m_IconfCascade.SaveOrUpdateList(confCascade);
                }
                //FB 2584 Start
                else if (displayLayoutAll == 3)
                {
                    List<vrmConfRoom> confroom = m_IconfRoom.GetByCriteria(criterionList);
                    for (int i = 0; i < confroom.Count; i++)
                        confroom[i].layout = displayLayout;
                    m_IconfRoom.SaveOrUpdateList(confroom);

                    List<vrmConfCascade> confCascade = m_IconfCascade.GetByCriteria(criterionList);
                    for (int i = 0; i < confCascade.Count; i++)
                        confCascade[i].layout = displayLayout;
                    m_IconfCascade.SaveOrUpdateList(confCascade);

                    List<vrmConfUser> confuser = m_IconfUser.GetByCriteria(criterionList);
                    for (int i = 0; i < confuser.Count; i++)
                        confuser[i].layout = displayLayout;
                    m_IconfUser.SaveOrUpdateList(confuser);
                }
                //FB 2584 End
                else
                {
                    criterionList.Add(Expression.Eq("userid", endpointID));
                    List<vrmConfUser> confuser = m_IconfUser.GetByCriteria(criterionList);
                    for (int i = 0; i < confuser.Count; i++)
                        confuser[i].layout = displayLayout;
                    m_IconfUser.SaveOrUpdateList(confuser);
                }
                outXml = "<success>1</success>";
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

        #region MuteTerminal
        /// <summary>
        /// MuteTerminal (COM to .Net conversion)
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool MuteTerminal(ref vrmDataObject obj)
        {
            bool bRet = true;
            string outXml = string.Empty;
            int userid = 0, confid = 0, instanceid = 0;
            myVRMException myVRMEx = new myVRMException();
            List<ICriterion> criterionList = new List<ICriterion>();
            String confids = "", terminalType = "";
            int mutePartyALL = 1; //FB 2530
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node = null;
                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out userid);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                if (userid <= 0)
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                node = xd.SelectSingleNode("//login/organizationID");
                string orgid = "";
                if (node != null)
                    orgid = node.InnerText.Trim();
                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);
                if (organizationID < defaultOrgId)
                {
                    myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                node = xd.SelectSingleNode("//login/confID");
                if (node != null)
                    confids = node.InnerText.Trim();
                if (confids.Length <= 0)
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                CConfID ConfMode = new CConfID(confids);
                confid = ConfMode.ID;
                instanceid = ConfMode.instance;
                if (instanceid == 0)
                    instanceid = 1;

                int endpointID = 0, mute = 0;
                node = xd.SelectSingleNode("//login/endpointID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out endpointID);
                node = xd.SelectSingleNode("//login/terminalType");
                if (node != null)
                    terminalType = node.InnerText.Trim();
                node = xd.SelectSingleNode("//login/mute");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out mute);
                
                //FB 2530 Starts
                node = xd.SelectSingleNode("//login/muteAll");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out mutePartyALL);

                criterionList.Add(Expression.Eq("confid", confid));
                if (instanceid > 0)
                    criterionList.Add(Expression.Eq("instanceid", instanceid));

                List<vrmConfUser> confuser = new List<vrmConfUser>();
                List<vrmConfRoom> confroom = new List<vrmConfRoom>();
                List<vrmConfCascade> confCascade = new List<vrmConfCascade>();
                
                if (terminalType == "0")
                {
                    confroom = m_IconfRoom.GetByCriteria(criterionList);
                    for (int i = 0; i < confroom.Count; i++)
                        confroom[i].mute = mute;
                    m_IconfRoom.SaveOrUpdateList(confroom);
                    
                    confCascade = m_IconfCascade.GetByCriteria(criterionList);
                    for (int i = 0; i < confCascade.Count; i++)
                        confCascade[i].mute = mute;
                    m_IconfCascade.SaveOrUpdateList(confCascade);

                   confuser = m_IconfUser.GetByCriteria(criterionList);
                    for (int i = 0; i < confuser.Count; i++)
                        confuser[i].mute = mute;
                    m_IconfUser.SaveOrUpdateList(confuser);

                }
                else if (terminalType == "2")
                {
                    criterionList.Add(Expression.Eq("roomId", endpointID));
                    confroom = m_IconfRoom.GetByCriteria(criterionList);
                    for (int i = 0; i < confroom.Count; i++)
                        confroom[i].mute = mute;
                    m_IconfRoom.SaveOrUpdateList(confroom);
                }
                else if (terminalType == "4")
                {
                    criterionList.Add(Expression.Eq("uId", endpointID));
                    confCascade = m_IconfCascade.GetByCriteria(criterionList);
                    for (int i = 0; i < confCascade.Count; i++)
                        confCascade[i].mute = mute;
                    m_IconfCascade.SaveOrUpdateList(confCascade);
                }
                else if (terminalType == "3")
                {
                    criterionList.Add(Expression.Eq("userid", endpointID));
                    confuser = m_IconfUser.GetByCriteria(criterionList);
                    for (int i = 0; i < confuser.Count; i++)
                        confuser[i].mute = mute;
                    m_IconfUser.SaveOrUpdateList(confuser);
                }
                //FB 2530 Ends

                outXml = "<success>1</success>";
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

        #region DeleteConference
        /// <summary>
        /// DeleteConference
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool DeleteConference(ref vrmDataObject obj)
        {
            try
            {
                List<ICriterion> criterionList = new List<ICriterion>();
                List<String> instanceList = new List<String>();
                XmlDocument xd = new XmlDocument();
                StringBuilder outXML = new StringBuilder();
                myVRMException myVRMEx = null;
                String confid = "", reason = "", query = "";
                int userid = 11, confMode;
                XmlNode node;


                xd.LoadXml(obj.inXml);
                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    userid = Int32.Parse(node.InnerXml.Trim());

                node = xd.SelectSingleNode("//login/organizationID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out organizationID);

                if (organizationID < defaultOrgId)
                {
                    myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/delconference/conference/reason");
                if (node != null)
                    reason = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//login/delconference/conference/confID");
                if (node == null)
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                else
                    confid = node.InnerXml.Trim();

                CConfID Conf = new CConfID(confid);

                query = "SELECT confid,instanceid,PushedToExternal FROM myVRM.DataLayer.vrmConference WHERE DATEDIFF(MINUTE,dateadd(minute,duration, confdate)," // FB 2441
                      + " dbo.changeTOGMTtime(" + sysSettings.TimeZone + ", getdate())) < 0 AND Deleted = 0 AND (Status <= 1 OR Status = 6)"
                      + " AND confID = " + Conf.ID;
                if (Conf.RecurMode == 0)
                    query += "AND instanceid = " + Conf.instance;

                IList iListConf = m_vrmConfDAO.execQuery(query);
                if (iListConf.Count == 0)
                {
                    myVRMEx = new myVRMException(604); //can't Delete conference Error message changed for FB 2222  //FB 2380
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                else
                {
                    object[] confObj = null;
                    for (int i = 0; i < iListConf.Count; i++)
                    {
                        confObj = (object[])iListConf[i];
                        instanceList.Add(confObj[1].ToString());
                    }
                }

                criterionList.Add(Expression.Eq("confid", Conf.ID));
                List<vrmRecurInfo> recurPattern = m_ConfRecurDAO.GetByCriteria(criterionList, true);

                if (instanceList.Count > 0)
                    criterionList.Add(Expression.In("instanceid", instanceList));

                List<vrmConference> confs = m_vrmConfDAO.GetByCriteria(criterionList, true);
                for (int i = 0; i < confs.Count; i++)
                {
                    confMode = 3;
                    if (confs[i].status == 1)
                        confMode = 4;
                    confs[i].deleted = 1;
                    confs[i].status = vrmConfStatus.Deleted;
                    confs[i].ConfMode = confMode;
                    m_vrmConfDAO.SaveOrUpdate(confs[i]);
                    RefundConference(confs[i]);
                }
                if (Conf.RecurMode == 0)
                {
                    if (recurPattern.Count > 0)
                    {
                        recurPattern[0].dirty = 1;
                        recurPattern[0].recurType = 5;
                        m_ConfRecurDAO.SaveOrUpdate(recurPattern[0]);
                    }
                }

                List<vrmConfAttachments> attachments = m_ConfAttachDao.GetByCriteria(criterionList);

                outXML.Append("<conferences>");
                outXML.Append("<conference>");
                outXML.Append("<confID>" + Conf.ID);
                if (Conf.RecurMode == 0)
                    outXML.Append("," + Conf.instance);
                outXML.Append("</confID>");
                ConfAttachments(criterionList, ref outXML, 1); //FB 2027 - GetOldConference
                
                //FB 2486 Starts
                List<ICriterion> criterionConf = new List<ICriterion>();
                criterionConf.Add(Expression.Eq("confid", Conf.ID));
                if (Conf.RecurMode == 0)
                    criterionConf.Add(Expression.Eq("instanceid", Conf.instance));
                //criterionConf.Add(Expression.Eq("textType", "C"));

                ConfMessages(criterionConf, ref outXML, 1); 
                //FB 2486 Ends

                //FB 2441 Starts
                List<ICriterion> bridgecriterionList = new List<ICriterion>(); //FB 2441
                List<vrmConfBridge> bridge = new List<vrmConfBridge>();
                bridgecriterionList = new List<ICriterion>();

                bridgecriterionList.Add(Expression.Eq("ConfID", Conf.ID));
                if (Conf.RecurMode == 0)
                    bridgecriterionList.Add(Expression.Eq("InstanceID", Conf.instance));

                bridge = m_IconfBridge.GetByCriteria(bridgecriterionList);
                if (bridge.Count > 0)
                {
                    outXML.Append("<confbrdige>");
                    if (bridge[0].Synchronous == 1 && confs[0].PushedToExternal == 1)
                        outXML.Append("<isSynchronous>1</isSynchronous>");
                    else
                        outXML.Append("<isSynchronous>0</isSynchronous>");
                    outXML.Append("</confbrdige>");
                }
               //FB 2441 Ends

                outXML.Append("</conference>");
                outXML.Append("<sCommand>TerminateConference</sCommand>");
                outXML.Append("</conferences>");
                obj.outXml = outXML.ToString();
                return true;
            }
            catch (Exception ex)
            {
                m_log.Error("DeleteConference :" + ex.Message);
                obj.outXml = "";
                return false;
            }
        }
        #endregion

        # region RefundConference
        /// <summary>
        /// RefundConference
        /// </summary>
        /// <param name="conf"></param>
        private void RefundConference(vrmConference conf)
        {
            try
            {
                OrgData orgdata = m_IOrgSettingsDAO.GetByOrgId(organizationID);
                if (orgdata.AccountingLogic != 0)
                {
                    int bill = 0;
                    String query = "";
                    vrmAccount UserAccount = m_userAccountDAO.GetByUserId(conf.owner); //FB 2027

                    query = "SELECT CR.roomId FROM myVRM.DataLayer.vrmConfRoom CR WHERE CR.confid = " + conf.confid.ToString()
                          + " AND CR.instanceid = " + conf.instanceid.ToString();
                    IList confRoomresult = m_vrmConfDAO.execQuery(query);

                    if (orgdata.AccountingLogic == 1) // Charge Back to owner
                    {
                        query = "SELECT CU.userid FROM myVRM.DataLayer.vrmConfUser CU WHERE CU.confid = " + conf.confid.ToString()
                              + " AND CU.instanceid = " + conf.instanceid.ToString() + " AND CU.invitee = 1"; // Both users and guests
                        IList confUsrresult = m_vrmConfDAO.execQuery(query);

                        bill = (confUsrresult.Count + confRoomresult.Count) * conf.duration;
                        UserAccount.TimeRemaining += bill;
                        m_userAccountDAO.Update(UserAccount);
                    }
                    else if (orgdata.AccountingLogic == 2) // Charge Back to individual
                    {
                        query = "SELECT CU.userid FROM myVRM.DataLayer.vrmConfUser CU, myVRM.DataLayer.vrmGuestUser U WHERE CU.confid = " + conf.confid.ToString()
                              + " AND CU.instanceid = " + conf.instanceid.ToString() + " AND CU.invitee = 1 AND U.userid = CU.userid";

                        IList result = m_vrmConfDAO.execQuery(query);
                        bill = (result.Count + confRoomresult.Count) * conf.duration;
                        UserAccount.TimeRemaining += bill;
                        m_userAccountDAO.Update(UserAccount);

                        query = "SELECT CU.userid FROM myVRM.DataLayer.vrmConfUser CU, myVRM.DataLayer.vrmUser U WHERE CU.confid =" + conf.confid.ToString()
                              + " and  CU.instanceid =" + conf.instanceid.ToString() + " AND CU.invitee = 1 AND U.userid = CU.userid";
                        result = m_vrmConfDAO.execQuery(query);
                        for (int i = 0; i < result.Count; i++)
                        {
                            UserAccount = m_userAccountDAO.GetById((int)result[i]);
                            UserAccount.TimeRemaining += conf.duration;
                            m_userAccountDAO.Update(UserAccount);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                m_log.Error("RefundConference" + ex.Message);
            }
        }
        #endregion

        //FB 2027 start - GetOldConference
        #region ConfAttachments
        /// <summary>
        /// ConfAttachments
        /// </summary>
        /// <param name="criterionList"></param>
        /// <param name="outXML"></param>
        public void ConfAttachments(List<ICriterion> criterionList, ref StringBuilder outXML,int mode)
        {
            try
            {
                outXML.Append("<fileUpload>");
                if (mode == 1) //1- Conf Mode,0-TempMode
                {
                    List<vrmConfAttachments> attachments = m_ConfAttachDao.GetByCriteria(criterionList);
                    for (int lp = 0; lp < attachments.Count; lp++)
                    {
                        if (attachments[lp].attachment.Length <= 5 || attachments[lp].attachment.Substring(0, 6).ToString() != "[ICAL]")
                            outXML.Append("<file>" + attachments[lp].attachment + "</file>");
                    }
                }
                outXML.Append("</fileUpload>");
            }
            catch (Exception ex)
            {
                m_log.Error("ConfAttachments:" + ex.StackTrace);
                throw ex;
            }
        }
        #endregion
        //FB 2027 End - GetOldConference
        
        //FB 2486 Starts 
        #region ConfMessages
        /// <summary>
        /// ConfMessages
        /// </summary>
        /// <param name="criterionList"></param>
        /// <param name="outXML"></param>
        public void ConfMessages(List<ICriterion> criterionList, ref StringBuilder outXML, int mode)
        {
            try
            {
                outXML.Append("<ConfMessageList>");
                if (mode == 1)
                {
                    List<vrmConfMessage> confmsgList = m_IConfMessageDao.GetByCriteria(criterionList);
                    for (int lp = 0; lp < confmsgList.Count; lp++)
                    {
                        outXML.Append("<ConfMessage>");
                        outXML.Append("<controlID>" + confmsgList[lp].controlID.ToString() + "</controlID>");
                        outXML.Append("<textMessage>" + confmsgList[lp].confMessage.ToString() + "</textMessage>");
                        outXML.Append("<msgduartion>" + confmsgList[lp].durationID.ToString() + "</msgduartion>");
                        outXML.Append("</ConfMessage>");
                    }
                }
                outXML.Append("</ConfMessageList>");
            }
            catch (Exception ex)
            {
                m_log.Error("ConfMessages:" + ex.StackTrace);
            }
        }
        #endregion
        //FB 2486 Ends
        #region GetNewTemplate
        /// <summary>
        /// GetNewTemplate (COM to .Net conversion)
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetNewTemplate(ref vrmDataObject obj)
        {
            StringBuilder outXml = new StringBuilder();
            List<ICriterion> criterionList = new List<ICriterion>();
            try
            {
                String DeptID = "";
                StringBuilder tempXML = new StringBuilder(); //FB 2607 StringBuilder
                int userID = 11, mode = 1;
                ICriterion criterium = null;
                bool singleDeptMode = true;
                myVRMException myVRMEx = null;
                XmlDocument xd = new XmlDocument();
                DateTime currDate = DateTime.Now;

                xd.LoadXml(obj.inXml);
                XmlNode node;
                node = xd.SelectSingleNode("//login/organizationID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out organizationID);
                if (organizationID < defaultOrgId)
                {
                    myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out userID);
                if (userID < 11)
                    userID = 11;
                node = xd.SelectSingleNode("//login/deptID");
                if (node != null)
                    DeptID = node.InnerText.Trim();

                OrgData orgDt = m_IOrgSettingsDAO.GetByOrgId(organizationID);
                vrmUser user = m_vrmUserDAO.GetByUserId(userID);
                if (orgDt.recurEnabled == 0)
                    if (user.Admin == 2 || user.Admin == 1)
                        orgDt.recurEnabled = 1;

                outXml.Append("<template>");
                outXml.Append("<userInfo>");
                outXml.Append("<userFirstName>" + user.FirstName + "</userFirstName>");
                outXml.Append("<userLastName>" + user.LastName + "</userLastName>");
                outXml.Append("<emailClient>" + user.EmailClient + "</emailClient>");
                outXml.Append("</userInfo>");
                outXml.Append("<confInfo>");
                outXml.Append("<locationList>");
                outXml.Append("<selected></selected>");
                outXml.Append("<mode>0</mode>");
                m_Search.organizationID = organizationID;
                m_Search.Fetch3LvLocations(ref outXml, userID); //Fetch Rooms
                outXml.Append("</locationList>");
                outXml.Append("<lineRateID></lineRateID>");
                m_Search.GetVideoSessionList(0, ref tempXML);
                m_Search.GetLineRateList(0, ref tempXML);
                m_Search.GetVideoEquipment(0, ref tempXML);
                m_Search.GetAudioList(0, ref tempXML);
                m_Search.GetAudioUsage(0, ref tempXML);
                m_Search.GetVideoProto(0, ref tempXML);
                outXml.Append(tempXML);
                
                //Fetching Groups start
                FetchTempUserGroups(userID, organizationID, ref outXml, user);
                //Fetching Groups end

                tempXML = new StringBuilder();
                m_Search.getAddressType(ref tempXML);
                outXml.Append(tempXML);
                outXml.Append("</confInfo>");
                outXml.Append("</template>");
                obj.outXml = outXml.ToString();
                return true;
            }
            catch (Exception ex)
            {
                m_log.Error("GetNewConference" + ex.Message);
                return false;
            }
        }
        #endregion

        #region GetNewConference
        /// <summary>
        /// GetNewConference
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetNewConference(ref vrmDataObject obj)
        {
            StringBuilder outXml = new StringBuilder();
            List<ICriterion> criterionList = new List<ICriterion>();
            try
            {
                String DeptID = "";
                int userID = 11, mode = 1, utcEnabled = 0;//FB 2014
                ICriterion criterium = null;
                bool singleDeptMode = true;
                myVRMException myVRMEx = null;
                XmlDocument xd = new XmlDocument();
                DateTime currDate = DateTime.Now;
                
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//login/organizationID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out organizationID);

                if (organizationID < defaultOrgId)
                {
                    myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out userID);

                if (userID < 11)
                    userID = 11;

                node = xd.SelectSingleNode("//login/deptID");
                if (node != null)
                    DeptID  = node.InnerText.Trim();

                if (xd.SelectSingleNode("//login/utcEnabled") != null)//FB 2014
                    Int32.TryParse(xd.SelectSingleNode("//login/utcEnabled").InnerText.Trim(), out utcEnabled);

                OrgData orgDt = m_IOrgSettingsDAO.GetByOrgId(organizationID);
                vrmUser user = m_vrmUserDAO.GetByUserId(userID);

                //FB 2014 Start
                if (utcEnabled == 1)
                    currDate = DateTime.Now;
                else
                {
                    int tzone = user.TimeZone;
                    timeZone.changeToGMTTime(sysSettings.TimeZone, ref currDate);
                    timeZone.userPreferedTime(tzone, ref currDate);
                }
                //FB 2014 End
                
                if (orgDt.recurEnabled == 0)
                    if (user.Admin == 2 || user.Admin == 1)
                        orgDt.recurEnabled = 1;

                outXml.Append("<conference>");
	            
                outXml.Append("<userInfo>");
		        outXml.Append("<userEmail>"+ user.Email+"</userEmail>");
                outXml.Append("<emailClient>" + user.EmailClient + "</emailClient>");
                outXml.Append("<recurEnabled>" + orgDt.recurEnabled + "</recurEnabled>");
                outXml.Append("<internalBridge>" + user.InternalVideoNumber + "</internalBridge>"); //FB 2376
                outXml.Append("<externalBridge>" + user.ExternalVideoNumber + "</externalBridge>");//FB 2376
                outXml.Append("<Secured>" + user.Secured + "</Secured>");//FB 2595
	            outXml.Append("</userInfo>");
                outXml.Append("<departments></departments>");//Nodes not use
	            
                outXml.Append("<confInfo>");
                outXml.Append("<startDate>" + currDate.ToString("d") + "</startDate>");//FB 2014
                outXml.Append("<startHour>" + currDate.ToString("HH") + "</startHour>");//FB 2014
                outXml.Append("<startMin>" + currDate.ToString("mm") + "</startMin>");//FB 2014
                outXml.Append("<startSet>" + currDate.ToString("tt") + "</startSet>");
                outXml.Append("<deptID>" + DeptID + "</deptID>");
                outXml.Append("<timeZone>" + user.TimeZone + "</timeZone>");
                outXml.Append("<timezones></timezones>"); //Nodes not use start
                outXml.Append("<lineRateID>0</lineRateID>");
                outXml.Append("<videoSession></videoSession>");
                outXml.Append("<lineRate></lineRate>");
                outXml.Append("<videoEquipment></videoEquipment>");
                outXml.Append("<audioAlgorithm></audioAlgorithm>");
                outXml.Append("<audioUsage></audioUsage>");
                outXml.Append("<videoProtocol></videoProtocol>");
                outXml.Append("<addressType></addressType>");  //Nodes not use end

                outXml.Append("<locationList>");
			    outXml.Append("<selected></selected>");
			    outXml.Append("<mode>0</mode>");
                m_Search.organizationID = organizationID;
                //m_Search.Fetch3LvLocations(ref outXml,userID); //FB 2594 - Fetching all rooms but not used.
                outXml.Append("</locationList>");

                //Fetching Groups start
                criterium = Expression.And((Expression.Or(Expression.Eq("Private", 0), Expression.Eq("Owner", userID))),
                            Expression.Eq("orgId", organizationID));
                
                outXml.Append("<defaultAGroups>");
                if (user.PreferedGroup > 0)
                {
                    criterionList.Add(criterium);
                    criterionList.Add(Expression.Eq("GroupID", user.PreferedGroup));
                    m_usrFactory.FetchGroups(criterionList, ref outXml, 0 ,"");
                }
                outXml.Append("</defaultAGroups>");

                criterionList = new List<ICriterion>();
                criterionList.Add(criterium);
                criterionList.Add(Expression.Eq("GroupID", user.CCGroup));

                outXml.Append("<defaultCGroups>");
                m_usrFactory.FetchGroups(criterionList,ref outXml, 0, "");
                outXml.Append("</defaultCGroups>");

                if (outXml.ToString().IndexOf("<defaultAGroups><groupID>") > 0)
                    outXml.Append("<groupID>" + user.PreferedGroup + "</groupID>");
                else
                    outXml.Append("<groupID></groupID>");

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);
                if (orgInfo != null)
                    multiDepts = orgInfo.MultipleDepartments;

                vrmUser userdetails = new vrmUser();
                if (multiDepts == 1)
                {
                    singleDeptMode = false;
                    userdetails = m_vrmUserDAO.GetByUserId(userID);
                }

                userdetails = m_vrmUserDAO.GetByUserId(userID);
                vrmUserRoles usrRole = m_IUserRolesDao.GetById(userdetails.roleID); //crossaccess

                criterionList = new List<ICriterion>();
                if (mode == 0) //If User is General User
                {
                    criterionList.Add(Expression.Or(Expression.Eq("Private", 0),
                        Expression.Eq("Owner", userdetails.userid)));
                }
                criterionList.Add(Expression.Eq("orgId", organizationID));  //If User is Site Admin

                if (singleDeptMode)
                {
                    if (!userdetails.isSuperAdmin())
                    {
                        criterionList.Add(Expression.Or(Expression.Eq("Private", 0), Expression.Eq("Owner", userdetails.userid)));
                    }
                }
                else
                {
                    if (!(userdetails.isSuperAdmin() && usrRole.crossaccess == 1))
                    {
                        if (userdetails.isSuperAdmin() && usrRole.crossaccess == 0) //If User is OrgAdmin
                        {
                            ArrayList userId = new ArrayList();
                            List<ICriterion> criterionList1 = new List<ICriterion>();
                            vrmUser superuser = m_vrmUserDAO.GetByUserId(11);
                            criterionList1.Add(Expression.Eq("Admin", 2));
                            criterionList1.Add(Expression.Eq("roleID", superuser.roleID));
                            List<vrmUser> userList = m_vrmUserDAO.GetByCriteria(criterionList1);
                            for (int i = 0; i < userList.Count; i++)
                            {
                                userId.Add(userList[i].userid);
                            }
                            criterionList.Add(Expression.Or(Expression.Not(Expression.In("Owner", userId)), Expression.Eq("Private", 0)));
                        }
                        else
                        {
                            criterionList.Add(Expression.Or(Expression.Eq("Private", 0), Expression.Eq("Owner", userdetails.userid))); //If its Gereral Users
                        }
                    }
                }
                
                outXml.Append("<groups>");
                m_usrFactory.FetchGroups(criterionList, ref outXml, 0, "group");
                outXml.Append("</groups>");
                //Fetching Groups end

                vrmConference conf = new vrmConference();
                m_vrmFactor.organizationID = organizationID; //FB 2045
                m_vrmFactor.FetchCustomAttrs(user, conf, ref outXml); //FB 2045

                FetchConfMessages(conf, ref outXml); //FB 2486

                outXml.Append("</confInfo>");
                outXml.Append("</conference>");

                obj.outXml = outXml.ToString();
                return true;
            }
            catch (Exception ex)
            {
                m_log.Error("GetNewConference" + ex.Message);
                return false;
            }
        }
        #endregion

        #region GetApprovalStatus
        /// <summary>
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetApprovalStatus(ref vrmDataObject obj)
        {
            StringBuilder outXML = new StringBuilder();
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;

                node = xd.SelectSingleNode("//login/confID");
                String confid = node.InnerXml.Trim();

                CConfID Conf = new CConfID(confid);
                if (Conf.instance == 0)
                    Conf.instance = 1;

                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("confid", Conf.ID));
                criterionList.Add(Expression.Eq("instanceid", Conf.instance));

                IList<vrmConference> confList = m_vrmConfDAO.GetByCriteria(criterionList);

                outXML.Append("<confApprovals>");
                outXML.Append(GetEntityApprovalStatus(confList, (Int32)Approver.ROOM_ENTITY));
                outXML.Append(GetEntityApprovalStatus(confList, (Int32)Approver.MCU_ENTITY));
                outXML.Append(GetEntityApprovalStatus(confList, (Int32)Approver.DEPT_ENTITY));
                outXML.Append(GetEntityApprovalStatus(confList, (Int32)Approver.SYSTEM_ENTITY));
                outXML.Append("</confApprovals>");

                obj.outXml = outXML.ToString();

                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                return false;
            }
        }

        #endregion

        #region GetEntityApprovalStatus
        /// <summary>
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        private StringBuilder GetEntityApprovalStatus(IList<vrmConference> confList, Int32 level)
        {
            StringBuilder outXML = new StringBuilder();

            try
            {
                Int32 confid = confList[0].confid;
                Int32 instanceid = confList[0].instanceid;
                Int32 confowner = confList[0].owner;//FB 1067
                #region ROOM_ENTITY
                if (level == (Int32)Approver.ROOM_ENTITY)
                {
                    outXML.Append("<room>");
                    foreach (vrmConfRoom rm in confList[0].ConfRoom)
                    {
                        IList urList = GetConferenceApproverDetails(confid, instanceid, level, rm.Room.roomId, 11);

                        if (urList.Count > 0)
                        {
                            Int32 usrID = 0;

                            outXML.Append("<response>");

                            vrmUser usr = (vrmUser)urList[0];
                            usrID = usr.userid;

                            List<ICriterion> criterionList = new List<ICriterion>();
                            criterionList.Add(Expression.Eq("approverid", usrID));
                            criterionList.Add(Expression.Eq("confid", confid));
                            criterionList.Add(Expression.Eq("instanceid", instanceid));

                            IConfApprovalDAO IApprover = m_confDAO.GetConfApprovalDao();
                            IList apprList = IApprover.GetByCriteria(criterionList);

                            foreach (vrmConfApproval confApp in apprList)
                            {
                                outXML.Append("<roomName>" + rm.Room.Name + "</roomName>");
                                outXML.Append("<approverName>" + usr.FirstName + " " + usr.LastName + "</approverName>");
                                outXML.Append("<decision>" + confApp.decision + "</decision>");
                                outXML.Append("<message>" + confApp.responsemessage + "</message>");
                                outXML.Append("<responseTime>" + confApp.responsetimestamp + "</responseTime>");
                            }
                            outXML.Append("</response>");
                        }
                        else
                        {
                            IList<vrmLocApprover> locApprList = rm.Room.locationApprover;
                            String appName = "";
                            foreach (vrmLocApprover locAppr in locApprList)
                            {
                                vrmUser usr = new vrmUser();
                                usr = m_vrmUserDAO.GetByUserId(locAppr.approverid);
                                if (locAppr.approverid != confowner)//FB 1067
                                {
                                    if (appName == "")
                                        appName = usr.FirstName + " " + usr.LastName;
                                    else
                                        appName = appName + ", " + usr.FirstName + " " + usr.LastName;
                                }
                            }

                            if (appName != "")
                            {
                                outXML.Append("<response>");
                                outXML.Append("<roomName>" + rm.Room.Name + "</roomName>");
                                outXML.Append("<approverName>" + appName + "</approverName>");
                                outXML.Append("<decision>0</decision>");
                                outXML.Append("<message>pending decision at ROOM level</message>");
                                outXML.Append("<responseTime></responseTime>");
                                outXML.Append("</response>");
                            }
                        }
                    }

                    outXML.Append("</room>");
                }

                #endregion

                #region MCU_ENTITY
                else if (level == (Int32)Approver.MCU_ENTITY)
                {
                    String confType = confList[0].conftype.ToString();
                    Int32 mcuOrgID = confList[0].orgId;

                    IList<vrmMCU> mcuList = new List<vrmMCU>();
                    vrmMCU mcu = null;
                    foreach (vrmConfUser confUser in confList[0].ConfUser)
                    {
                        if (confUser.bridgeid > 0)
                        {
                            mcu = new vrmMCU();
                            mcu = m_vrmMCU.GetById(confUser.bridgeid);

                            Boolean isExists = false;
                            foreach (vrmMCU m in mcuList)
                            {
                                if (m.BridgeID == mcu.BridgeID)
                                {
                                    isExists = true;
                                    break;
                                }
                            }

                            if (!isExists)
                                mcuList.Add(mcu);
                        }
                    }

                    foreach (vrmConfRoom confRm in confList[0].ConfRoom)
                    {
                        if (confRm.bridgeid > 0)
                        {
                            mcu = new vrmMCU();
                            mcu = m_vrmMCU.GetById(confRm.bridgeid);
                            Boolean isExists = false;
                            foreach (vrmMCU m in mcuList)
                            {
                                if (m.BridgeID == mcu.BridgeID)
                                {
                                    isExists = true;
                                    break;
                                }
                            }

                            if (!isExists)
                                mcuList.Add(mcu);
                        }
                    }

                    outXML.Append("<MCU>");
                    if (confType == "2" || confType == "1" || confType == "4" || confType == "6") //Approval status
                    {
                        for (Int32 i = 0; i < mcuList.Count; i++)
                        {
                            vrmMCU lst = mcuList[i];
                            IList urList = GetConferenceApproverDetails(confid, instanceid, level, lst.BridgeID, 11);

                            if (urList.Count > 0)
                            {
                                outXML.Append("<response>");
                                Int32 usrID = 0;

                                String bdgName = lst.BridgeName;
                                if (lst.isPublic == 1 && mcuOrgID != 11)
                                    bdgName = lst.BridgeName + "(*)";

                                vrmUser usr = (vrmUser)urList[0];
                                usrID = usr.userid;

                                List<ICriterion> criterionList = new List<ICriterion>();
                                criterionList.Add(Expression.Eq("approverid", usrID));
                                criterionList.Add(Expression.Eq("confid", confid));
                                criterionList.Add(Expression.Eq("instanceid", instanceid));

                                IConfApprovalDAO IApprover = m_confDAO.GetConfApprovalDao();
                                IList apprList = IApprover.GetByCriteria(criterionList);

                                foreach (vrmConfApproval confApp in apprList)
                                {
                                    outXML.Append("<MCUName>" + bdgName + "</MCUName>");
                                    outXML.Append("<approverName>" + usr.FirstName + " " + usr.LastName + "</approverName>");
                                    outXML.Append("<decision>" + confApp.decision + "</decision>");
                                    outXML.Append("<message>" + confApp.responsemessage + "</message>");
                                    outXML.Append("<responseTime>" + confApp.responsetimestamp + "</responseTime>");
                                }
                                outXML.Append("</response>");
                            }
                            else
                            {
                                String appName = "";
                                foreach (vrmMCUApprover mcuApprover in lst.MCUApprover)
                                {
                                    vrmUser usr = new vrmUser();
                                    usr = m_vrmUserDAO.GetByUserId(mcuApprover.approverid);

                                    if (appName == "")
                                        appName = usr.FirstName + " " + usr.LastName;
                                    else
                                        appName = appName + ", " + usr.FirstName + " " + usr.LastName;
                                }

                                String bdgName = lst.BridgeName;
                                if (lst.isPublic == 1 && mcuOrgID != 11)
                                    bdgName = lst.BridgeName + "(*)";

                                if (appName != "")
                                {
                                    outXML.Append("<response>");
                                    outXML.Append("<MCUName>" + bdgName + "</MCUName>");
                                    outXML.Append("<approverName>" + appName + "</approverName>");
                                    outXML.Append("<decision>0</decision>");
                                    outXML.Append("<message>pending decision at MCU level</message>");
                                    outXML.Append("<responseTime></responseTime>");
                                    outXML.Append("</response>");
                                }
                            }
                        }
                    }

                    outXML.Append("</MCU>");
                }
                #endregion

                #region DEPT_ENTITY
                else if (level == (Int32)Approver.DEPT_ENTITY)
                {
                    DetachedCriteria query = DetachedCriteria.For(typeof(vrmConference), "conf");
                    query.SetProjection(Projections.Property("confnumname"));
                    query.Add(Expression.Eq("confid", confid));
                    query.Add(Expression.Eq("instanceid", instanceid));
                    query.Add(Property.ForName("conf.ConfDeptID")
                                      .EqProperty("dept.departmentId"));

                    DetachedCriteria depart = DetachedCriteria.For(typeof(vrmDept), "dept");
                    depart.Add(Subqueries.Exists((query)));

                    IList dpList = m_IdeptDAO.Search(depart);

                    outXML.Append("<department>");
                    Int32 deptID = 0;
                    String name = "";
                    foreach (vrmDept dp in dpList)
                    {
                        deptID = dp.departmentId;
                        name = dp.departmentName;
                    }
                    if (deptID != 0)
                    {
                        IList urList = GetConferenceApproverDetails(confid, instanceid, level, 0, 11);

                        outXML.Append("<response>");
                        if (urList.Count > 0)
                        {
                            Int32 usrID = 0;
                            vrmUser usr = (vrmUser)urList[0];
                            usrID = usr.userid;

                            List<ICriterion> criterionList = new List<ICriterion>();
                            criterionList.Add(Expression.Eq("approverid", usrID));
                            criterionList.Add(Expression.Eq("confid", confid));
                            criterionList.Add(Expression.Eq("instanceid", instanceid));

                            IConfApprovalDAO IApprover = m_confDAO.GetConfApprovalDao();
                            IList apprList = IApprover.GetByCriteria(criterionList);

                            foreach (vrmConfApproval confApp in apprList)
                            {
                                outXML.Append("<departmentName>" + name + "</departmentName>");
                                outXML.Append("<approverName>" + usr.FirstName + " " + usr.LastName + "</approverName>");
                                outXML.Append("<decision>" + confApp.decision + "</decision>");
                                outXML.Append("<message>" + confApp.responsemessage + "</message>");
                                outXML.Append("<responseTime>" + confApp.responsetimestamp + "</responseTime>");
                            }
                        }
                        else
                        {
                            DetachedCriteria queryA = DetachedCriteria.For(typeof(vrmDeptApprover), "DeptApproval");
                            queryA.SetProjection(Projections.Property("id"));
                            queryA.Add(Expression.Eq("departmentid", deptID));
                            queryA.Add(Property.ForName("user.userid").EqProperty("DeptApproval.approverid"));

                            DetachedCriteria usrCrite = DetachedCriteria.For(typeof(vrmUser), "user");
                            usrCrite.Add(Subqueries.Exists((queryA)));

                            urList = m_vrmUserDAO.Search(usrCrite);
                            String appName = "";
                            foreach (vrmUser usr in urList)
                            {
                                if (appName == "")
                                    appName = usr.FirstName + " " + usr.LastName;
                                else
                                    appName = appName + ", " + usr.FirstName + " " + usr.LastName;
                            }

                            if (appName != "")
                            {
                                outXML.Append("<departmentName>" + name + "</departmentName>");
                                outXML.Append("<approverName>" + appName + "</approverName>");
                                outXML.Append("<decision>0</decision>");
                                outXML.Append("<message>pending decision at Department level</message>");
                                outXML.Append("<responseTime></responseTime>");
                            }
                        }

                        outXML.Append("</response>");
                    }
                    outXML.Append("</department>");
                }

                #endregion

                #region SYSTEM_ENTITY
                else if (level == (Int32)Approver.SYSTEM_ENTITY)
                {
                    Int32 OrgID = confList[0].orgId;
                    IList urList = GetConferenceApproverDetails(confid, instanceid, level, 0, OrgID);

                    outXML.Append("<system>");
                    if (urList.Count > 0)
                    {
                        outXML.Append("<response>");
                        Int32 usrID = 0;

                        vrmUser usr = (vrmUser)urList[0];
                        usrID = usr.userid;

                        List<ICriterion> criterionList = new List<ICriterion>();
                        criterionList.Add(Expression.Eq("approverid", usrID));
                        criterionList.Add(Expression.Eq("confid", confid));
                        criterionList.Add(Expression.Eq("instanceid", instanceid));

                        IConfApprovalDAO IApprover = m_confDAO.GetConfApprovalDao();
                        IList apprList = IApprover.GetByCriteria(criterionList);

                        foreach (vrmConfApproval confApp in apprList)
                        {
                            outXML.Append("<approverName>" + usr.FirstName + " " + usr.LastName + "</approverName>");
                            outXML.Append("<decision>" + confApp.decision + "</decision>");
                            outXML.Append("<message>" + confApp.responsemessage + "</message>");
                            outXML.Append("<responseTime>" + confApp.responsetimestamp + "</responseTime>");
                        }
                        outXML.Append("</response>");
                    }
                    else
                    {
                        IList<sysApprover> sysApproverList = m_ISysApproverDAO.GetSysApproversByOrgId(OrgID);  //Organization Module
                        String appName = "";

                        foreach (sysApprover sysApp in sysApproverList)
                        {
                            vrmUser usr = new vrmUser();
                            usr = m_vrmUserDAO.GetByUserId(sysApp.approverid);

                            if (appName == "")
                                appName = usr.FirstName + " " + usr.LastName;
                            else
                                appName = appName + ", " + usr.FirstName + " " + usr.LastName;
                        }
                        if (appName != "")
                        {
                            outXML.Append("<response>");
                            outXML.Append("<approverName>" + appName + "</approverName>");
                            outXML.Append("<decision>0</decision>");
                            outXML.Append("<message>pending decision at SYSTEM level</message>");
                            outXML.Append("<responseTime></responseTime>");
                            outXML.Append("</response>");
                        }
                    }
                    outXML.Append("</system>");
                }
                #endregion

                return outXML;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return null;
            }
        }

        #endregion

        #region GetConferenceApproverDetails

        private IList GetConferenceApproverDetails(Int32 confid, Int32 instanceid, Int32 level, Int32 entityID, Int32 OrgID)
        {
            try
            {

                DetachedCriteria queryA = DetachedCriteria.For(typeof(vrmConfApproval), "confApproval");
                queryA.SetProjection(Projections.Property("uId"));
                queryA.Add(Expression.Eq("confid", confid));
                queryA.Add(Expression.Eq("instanceid", instanceid));
                queryA.Add(Expression.Eq("entitytype", level));
                if (level == (Int32)Approver.ROOM_ENTITY || level == (Int32)Approver.MCU_ENTITY)
                    queryA.Add(Expression.Eq("entityid", entityID));

                if (level == (Int32)Approver.SYSTEM_ENTITY)
                    queryA.Add(Expression.Eq("user.companyId", OrgID));

                queryA.Add(Expression.Not(Expression.Eq("decision", 0)));
                queryA.Add(Property.ForName("user.userid").EqProperty("confApproval.approverid"));

                DetachedCriteria usrCrite = DetachedCriteria.For(typeof(vrmUser), "user");
                usrCrite.Add(Subqueries.Exists((queryA)));

                IList urList = m_vrmUserDAO.Search(usrCrite);

                return urList;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return null;
            }
        }

        #endregion

        //FB 2027(GetTerminalControl)
        #region GetTerminalControl
        /// <summary>
        /// GetTerminalControl (COM to .Net conversion)
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetTerminalControl(ref vrmDataObject obj)
        {
            bool bRet = true;
            int userid = 0, type = 0, confid = 0, instanceid = 0, utcEnabled = 0;//FB 2014
            String confids = "", name = "";
            StringBuilder tempXML = new StringBuilder();
            ArrayList ar = new ArrayList();
            StringBuilder outXml = new StringBuilder();
            myVRMException myVRMEx = new myVRMException();
            List<ICriterion> criterionList = new List<ICriterion>();
            List<ICriterion> criterionList1 = new List<ICriterion>();
            vrmConference Conf = new vrmConference();
            vrmMCU MCU = new vrmMCU();//FB 2441
            int isDMA = 0;//FB 2441
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node = null;
                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out userid);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                if (userid <= 0)
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                node = xd.SelectSingleNode("//login/confID");
                if (node != null)
                    confids = node.InnerText.Trim();
                if (confids.Length <= 0)
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                if (xd.SelectSingleNode("//login/utcEnabled") != null)//FB 2014
                    Int32.TryParse(xd.SelectSingleNode("//login/utcEnabled").InnerText.Trim(), out utcEnabled);

                CConfID CConf = new CConfID(confids);
                Conf.confid = CConf.ID;
                Conf.instanceid = CConf.instance;
                if (Conf.instanceid == 0)
                    Conf.instanceid = 1;
                Conf = m_vrmConfDAO.GetByConfId(Conf.confid, Conf.instanceid);
                outXml.Append("<terminalControl>");
                outXml.Append("<confInfo>");
                outXml.Append("<confID>");
                outXml.Append(Conf.confid.ToString() + "," + Conf.instanceid.ToString());
                outXml.Append("</confID>");
                outXml.Append("<confName>" + Conf.externalname + "</confName>");
                outXml.Append("<confUniqueID>" + Conf.confnumname + "</confUniqueID>");
                vrmUser Owner = m_vrmUserDAO.GetByUserId(Conf.owner);
                outXml.Append("<owner>" + Owner.FirstName + "" + Owner.LastName + "</owner>");
                outXml.Append("<ownerEmail>" + Owner.Email + "</ownerEmail>");
                Conf.description = m_UtilFactory.ReplaceOutXMLSpecialCharacters(Conf.description); //FB 2236
                outXml.Append("<description>" + Conf.description + "</description>");
                //FB 2014 - Start
                DateTime confDateTime = Conf.confdate;
                if (utcEnabled == 0)
                    timeZone.userPreferedTime(Conf.timezone, ref confDateTime);
                outXml.Append("<startDate>" + confDateTime.ToString("d") + "</startDate>");
                outXml.Append("<startHour>" + confDateTime.ToString("hh") + "</startHour>");
                outXml.Append("<startMin>" + confDateTime.ToString("mm") + "</startMin>");
                outXml.Append("<startSet>" + confDateTime.ToString("tt") + "</startSet>");
                //FB 2014 - End
                timeZoneData tz = new timeZoneData();
                timeZone.GetTimeZone(Conf.timezone, ref tz);
                outXml.Append("<timeZoneName>" + tz.TimeZone + "</timeZoneName>");
                outXml.Append("<durationMin>" + Conf.duration + "</durationMin>");
                outXml.Append("<status>" + Conf.status + "</status>");
                outXml.Append("<displayLayout>" + Conf.videolayout + "</displayLayout>");
                m_Search.GetAudioUsage(0, ref tempXML);
                outXml.Append(tempXML);
                outXml.Append("<terminals>");

                //FB 2560 Codes Modified ... start
                vrmUser actUsr = null;
                vrmGuestUser GstUsr = null;
                vrmConfUser ur = null;

                for (int i = 0; i < Conf.ConfUser.Count; i++)
                {
                    ur = null;
                    actUsr = null;
                    GstUsr = null;

                    ur = Conf.ConfUser[i];
                    if (ur.invitee == 1 && ur.connectStatus != 3)
                    {
                        if (ur.TerminalType == vrmConfTerminalType.NormalUser)
                        {
                            actUsr = m_vrmUserDAO.GetByUserId(ur.userid);
                            name = actUsr.FirstName + " " + actUsr.LastName;
                        }
                        else
                        {
                            GstUsr = m_vrmGuestDAO.GetByUserId(ur.userid);
                            name = GstUsr.FirstName + " " + GstUsr.LastName;
                        }
                        outXml.Append("<terminal>");
                        outXml.Append("<endpointID>" + ur.userid + "</endpointID>");
                        outXml.Append("<type>" + ur.TerminalType + "</type>");
                        outXml.Append("<name>" + name + "</name>");
                        outXml.Append("<mute>" + ur.mute + "</mute>");
                        outXml.Append("<displayLayout>" + ur.layout + "</displayLayout>");
                        outXml.Append("<addressType>" + ur.addressType + "</addressType>");
                        outXml.Append("<address>" + ur.ipisdnaddress + "</address>");
                        outXml.Append("<LastRunDate>" + ur.LastRunDateTime.ToString("MM/dd/yyyy hh:mm:ss") + "</LastRunDate>");
                        outXml.Append("<status>" + ur.OnlineStatus + "</status>");
                        outXml.Append("<isCalendarInvite>0</isCalendarInvite>");
                        outXml.Append("<rmEndPointId></rmEndPointId>");
						//FB 2441 Starts
                    	isDMA = 1;
                        if (ur.bridgeid != 0)
                   		{
                            MCU = m_vrmMCU.GetById(ur.bridgeid);
                       		 if (MCU.MCUType.id == 13 && MCU.DMAMonitorMCU == 0)
                           		 isDMA = 0;
						}
                        outXml.Append("<BridgeTypeId>" + MCU.MCUType.id + "</BridgeTypeId>");
                        outXml.Append("<isMonitorDMA>" + isDMA + "</isMonitorDMA>");
                        //FB 2441 Ends
                        outXml.Append("</terminal>");
                    }
                }

                vrmConfRoom rm = null;
                vrmRoom room = null;
                vrmEndPoint ep = null;
                for (int i = 0; i < Conf.ConfRoom.Count; i++)
                {
                    rm = null;
                    room = null;
                    ep = null;

                    rm = Conf.ConfRoom[i];
                    room = m_vrmRoomDAO.GetByRoomId(rm.roomId);
                    ep = m_vrmEpt.GetByEptId(rm.endpointId);//FB 2027
                    if (rm.connectStatus != 3)
                    {
                        outXml.Append("<terminal>");
                        outXml.Append("<endpointID>" + room.roomId + "</endpointID>");
                        outXml.Append("<type>2</type>"); // 1 = user , 2 = room, 3 = guest , 4 = cascade
                        outXml.Append("<name>" + room.Name + "</name>");
                        outXml.Append("<mute>" + rm.mute + "</mute>");
                        outXml.Append("<displayLayout>" + rm.layout + "</displayLayout>");
                        outXml.Append("<addressType>" + rm.addressType + "</addressType>");
                        outXml.Append("<address>" + rm.ipisdnaddress + "</address>");
                        outXml.Append("<LastRunDate>" + rm.LastRunDateTime.ToString("MM/dd/yyyy hh:mm:ss") + "</LastRunDate>");
                        outXml.Append("<status>" + rm.OnlineStatus + "</status>");
                        outXml.Append("<isCalendarInvite>" + ep.CalendarInvite + "</isCalendarInvite>");
                        outXml.Append("<rmEndPointId>" + rm.endpointId + "</rmEndPointId>");
						//FB 2441 Starts
                   		isDMA = 1;
                        if (rm.bridgeid != 0)
                   		{
                            MCU = m_vrmMCU.GetById(rm.bridgeid);
                      	    if (MCU.MCUType.id == 13 && MCU.DMAMonitorMCU == 0)
                       		     isDMA = 0;
                    	}
                        outXml.Append("<BridgeTypeId>" + MCU.MCUType.id + "</BridgeTypeId>");
                        outXml.Append("<isMonitorDMA>" + isDMA + "</isMonitorDMA>");
                    	//FB 2441 Ends
                        outXml.Append("</terminal>");
                    }
                }
                //Cascade endpoints
                criterionList1.Add(Expression.Eq("confid", Conf.confid));
                if (Conf.instanceid > 0)
                    criterionList1.Add(Expression.Eq("instanceid", Conf.instanceid));
                criterionList1.Add(Expression.Not(Expression.Eq("connectStatus", 3)));
                //FB 2560 Codes Modified ... end

                List<vrmConfCascade> confCascade = m_IconfCascade.GetByCriteria(criterionList1);
                for (int i = 0; i < confCascade.Count; i++)
                {
                    outXml.Append("<terminal>");
                    outXml.Append("<endpointID>" + confCascade[i].cascadeLinkId + "</endpointID>");
                    outXml.Append("<type>4</type>");
                    outXml.Append("<name>" + confCascade[i].cascadelinkname + "</name>");
                    outXml.Append("<mute>" + confCascade[i].mute + "</mute>");
                    outXml.Append("<displayLayout>" + confCascade[i].layout + "</displayLayout>");
                    outXml.Append("<addressType>" + confCascade[i].connectionType + "</addressType>");
                    outXml.Append("<address>" + confCascade[i].ipisdnAddress + "</address>");
                    outXml.Append("<LastRunDate>" + confCascade[i].LastRunDateTime.ToString("MM/dd/yyyy hh:mm:ss") + "</LastRunDate>");
                    outXml.Append("<status>" + confCascade[i].OnlineStatus + "</status>");
                    outXml.Append("<isCalendarInvite>0</isCalendarInvite>");
                    outXml.Append("<rmEndPointId></rmEndPointId>");
                    outXml.Append("</terminal>");
                }
                outXml.Append("</terminals>");
                outXml.Append("</confInfo>");
                outXml.Append("</terminalControl>");
                obj.outXml = outXml.ToString();
                bRet = true;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

        #region FetchMostConfs
        /// <summary>
        /// FetchMostConfs
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public StringBuilder FetchMostConfs(Int32 organizationID, Int32 userID, ref StringBuilder outXml)
        {
            myVRMException myVRMEx = null;
            List<ICriterion> criterionList = new List<ICriterion>();
            try
            {
                vrmUser usr = new vrmUser();
                usr = m_vrmUserDAO.GetByUserId(userID);
                outXml.Append("<newUser>" + usr.newUser + "</newUser>");
                outXml.Append("<organizationID>" + organizationID + "</organizationID>");
                // The hasRoom field in the outputXML shows a message on the ASP page saying
                // "No rooms are defined... etc" if 0. This was an old concept and is no longer
                // relevant. All users (SuperAdmin/Admin/User) can see all rooms (unless in 
                // multiple department mode, which is taken care of by itself). So we basically
                // hard-code hasRoom to 1 to avoid any confusion.
                outXml.Append("<hasRoom>1</hasRoom>");
                
                outXml.Append("<futureMostUsedConf/>"); //Used Usr_TemplatePrefs_D table to get details, but we didnt use this table anywhere.
                
                outXml.Append("<futureMostRecentConf>");

                SubFetchMostConfs("FMRC", userID, organizationID, ref outXml);
                
                outXml.Append("</futureMostRecentConf>");

                outXml.Append("<roomMostUsedConf/>");//Used Usr_TemplatePrefs_D table to get details, but we didnt use this table anywhere.

                outXml.Append("<roomMostRecentConf>");

                SubFetchMostConfs("RMRC", userID, organizationID, ref outXml);
                
                outXml.Append("</roomMostRecentConf>");
                
                outXml.Append("<immediateMostUsedConf/>");//Used Usr_TemplatePrefs_D table to get details, but we didnt use this table anywhere.
                
                outXml.Append("<immediateMostRecentConf>");

                SubFetchMostConfs("IMRC", userID, organizationID, ref outXml);

                outXml.Append("</immediateMostRecentConf>");

                outXml.Append("<audioMostUsedConf/>");//Used Usr_TemplatePrefs_D table to get details, but we didnt use this table anywhere.

                outXml.Append("<audioMostRecentConf>");

                SubFetchMostConfs("AMRC", userID, organizationID, ref outXml);
                
                outXml.Append("</audioMostRecentConf>");
                                
                return outXml;
            }
            catch (Exception ex)
            {
                m_log.Error("FetchMostConfs" + ex.Message);
                return null;
            }
        }

        private void SubFetchMostConfs(String type, Int32 userID, Int32 organizationID, ref StringBuilder outXml)
        {
            try
            {
                m_vrmConfDAO = m_confDAO.GetConferenceDao();
                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("owner", userID));

                if (type == "FMRC")
                    criterionList.Add(Expression.Eq("conftype", 1));
                else if (type == "AMRC")
                    criterionList.Add(Expression.Eq("conftype", 6));

                if (type == "IMRC")
                    criterionList.Add(Expression.Eq("immediate", 1));

                criterionList.Add(Expression.Eq("instanceid", 1));

                if (type == "RMRC")
                    criterionList.Add(Expression.Not(Expression.Eq("deleted", 1)));
                else
                    criterionList.Add(Expression.Eq("deleted", 0));     

                criterionList.Add(Expression.Eq("orgId", organizationID));
                m_vrmConfDAO.addOrderBy(Order.Desc("settingtime"));

                List<vrmConference> confList = new List<vrmConference>();
                confList = m_vrmConfDAO.GetByCriteria(criterionList);

                if (confList.Count > 0)
                {
                    Int32 n = 5;

                    if (confList.Count < 5)
                        n = confList.Count;

                    for (Int32 i = 0; i < n; i++)
                    {
                        outXml.Append("<conf>");
                        if (type == "RMRC")
                        {
                            if (confList[i].recuring > 0)
                                outXml.Append("<confID>" + confList[i].confid + "</confID>");
                            else
                                outXml.Append("<confID>" + confList[i].confid + ",1</confID>");
                        }
                        else
                            outXml.Append("<confID>" + confList[i].confid + "</confID>");

                        outXml.Append("<confName>" + confList[i].externalname + "</confName>");
                        outXml.Append("</conf>");
                    }
                }
            }
            catch (Exception ex)
            {
                m_log.Error("SubFetchMostConfs" + ex.Message);
            }
        }

        #endregion

		#region GetOldTemplate
        /// <summary>
        /// GetOldTemplate (COM to .Net conversion)
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetOldTemplate(ref vrmDataObject obj)
        {
            bool bRet = true;
            StringBuilder outXml = new StringBuilder();
            int userid = 0, templateID = 0;
            myVRMException myVRMEx = new myVRMException();
            List<ICriterion> criterionList = new List<ICriterion>();            
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node = null;
                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out userid);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                if (userid <= 0)
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                node = xd.SelectSingleNode("//login/organizationID");
                string orgid = "";
                if (node != null)
                    orgid = node.InnerText.Trim();
                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);
                if (organizationID < defaultOrgId)
                {
                    myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                node = xd.SelectSingleNode("//login/templateID");
                if (node != null)
                    templateID = Int32.Parse(node.InnerText.Trim());


                outXml.Append("<template>");

                vrmUser usr = new vrmUser();
                usr = m_vrmUserDAO.GetByUserId(userid);

                outXml.Append("<userInfo>");
                outXml.Append("<userFirstName>" + usr.FirstName + "</userFirstName>");
                outXml.Append("<userLastName>" + usr.LastName + "</userLastName>");
                outXml.Append("<emailClient>" + usr.EmailClient.ToString() + "</emailClient>");
                if (usr.DefaultTemplate != 0 && usr.DefaultTemplate == templateID)
                    outXml.Append("<setDefault>True</setDefault>");
                else
                    outXml.Append("<setDefault>False</setDefault>");
                outXml.Append("</userInfo>");

                vrmTemplate vrmtemp = new vrmTemplate();
                vrmtemp = m_vrmTempDAO.GetById(templateID);

                outXml.Append(" <templateInfo>");
                outXml.Append(" <ID>" + vrmtemp.tmpId.ToString() + "</ID>");
                outXml.Append("<name>" + vrmtemp.TmpName + "</name>");
                outXml.Append("<public>" + vrmtemp.TMPPublic.ToString() + "</public>");

                vrmUser tmpusr = new vrmUser();
                tmpusr = m_vrmUserDAO.GetByUserId(vrmtemp.TMPOwner);
                outXml.Append(" <owner>");
                outXml.Append(" <firstName>" + tmpusr.FirstName + "</firstName>");
                outXml.Append(" <lastName>" + tmpusr.LastName + "</lastName>");
                outXml.Append("</owner>");

                outXml.Append(" <description>" + vrmtemp.TMPDescription + "</description>");
                outXml.Append(" </templateInfo>");
                outXml.Append(" <confInfo>");
                outXml.Append("<confName>" + vrmtemp.externalname + "</confName>");
                outXml.Append("<confPassword>" + vrmtemp.password + "</confPassword>");
                outXml.Append("<durationMin>" + vrmtemp.duration + "</durationMin>");
                outXml.Append("<description>" + m_UtilFactory.ReplaceOutXMLSpecialCharacters(vrmtemp.confDescription) + "</description>");//FB 2236
                outXml.Append("<confType>" + vrmtemp.conftype + "</confType>");

                vrmTempAdvAvParams vrmTmpAdvAv = new vrmTempAdvAvParams();
                vrmTmpAdvAv = m_TempAdvAvParamsDAO.GetById(templateID);

                outXml.Append("<advAVParam>");
                outXml.Append("<maxAudioPart>" + vrmTmpAdvAv.maxAudioParticipants.ToString() + "</maxAudioPart>");
                outXml.Append("<maxVideoPart>" + vrmTmpAdvAv.maxVideoParticipants.ToString() + "</maxVideoPart>");
                outXml.Append("<restrictProtocol>" + vrmTmpAdvAv.videoProtocolID.ToString() + "</restrictProtocol>");
                outXml.Append("<restrictAV>" + vrmTmpAdvAv.mediaID.ToString() + "</restrictAV>");
                outXml.Append("<videoLayout>" + vrmTmpAdvAv.videoLayoutID.ToString() + "</videoLayout>");
                outXml.Append("<maxLineRateID>" + vrmTmpAdvAv.linerateID.ToString() + "</maxLineRateID>");
                outXml.Append("<audioCodec>" + vrmTmpAdvAv.audioAlgorithmID.ToString() + "</audioCodec>");
                outXml.Append("<videoCodec>" + vrmTmpAdvAv.videoSession.ToString() + "</videoCodec>");
                outXml.Append("<dualStream>" + vrmTmpAdvAv.dualStreamModeID.ToString() + "</dualStream>");
                outXml.Append("<confOnPort>" + vrmTmpAdvAv.conferenceOnPort.ToString() + "</confOnPort>");
                outXml.Append("<encryption>" + vrmTmpAdvAv.encryption.ToString() + "</encryption>");
                outXml.Append("<lectureMode>" + vrmTmpAdvAv.lectureMode.ToString() + "</lectureMode>");
                outXml.Append("<VideoMode>" + vrmTmpAdvAv.videoMode.ToString() + "</VideoMode>");
                outXml.Append("<SingleDialin>" + vrmTmpAdvAv.singleDialin.ToString() + "</SingleDialin>");
                outXml.Append("</advAVParam>");

                outXml.Append("<locationList>");
                outXml.Append("<selected>");
                List<int> roomids = new List<int>();
                List<ICriterion> criterionList2 = new List<ICriterion>();
                criterionList2.Add(Expression.Eq("tmpId", templateID));
                List<vrmTempRoom> room = m_TempDAO.GetByCriteria(criterionList2);
                for (int i = 0; i < room.Count; i++)
                {
                    outXml.Append("<level1ID>" + room[i].RoomID + "</level1ID>");
                    roomids.Add(room[i].RoomID);
                }
                outXml.Append("</selected>");
                
                m_RoomFactory.organizationID = organizationID;
                m_RoomFactory.Fetch3LvTempLocations(userid, roomids, ref outXml);
                outXml.Append("</locationList>");

                outXml.Append("<publicConf>" + vrmtemp.isPublic.ToString() + "</publicConf>");
                outXml.Append("<continuous>" + vrmtemp.continous.ToString() + "</continuous>");
                outXml.Append("<videoLayout>" + vrmtemp.videolayout.ToString() + "</videoLayout>");
                outXml.Append("<videoSessionID>" + vrmtemp.videosession.ToString() + "</videoSessionID>");
                outXml.Append("<manualVideoLayout>" + vrmtemp.manualvideolayout.ToString() + "</manualVideoLayout>");
                outXml.Append("<lectureMode>" + vrmtemp.lecturemode.ToString() + "</lectureMode>");
                outXml.Append("<lecturer>" + vrmtemp.lecturer + "</lecturer>");
                outXml.Append("<dynamicInvite>" + vrmtemp.dynamicinvite.ToString() + "</dynamicInvite>");
                outXml.Append("<lineRateID>" + vrmtemp.linerate.ToString() + "</lineRateID>");

                outXml.Append("<audioAlgorithmID>" + vrmTmpAdvAv.audioAlgorithmID.ToString() + "</audioAlgorithmID>");
                outXml.Append("<videoProtocolID>" + vrmTmpAdvAv.videoProtocolID.ToString() + "</videoProtocolID>");

                StringBuilder outstrXML = new StringBuilder();
                if (!m_Search.GetVideoSessionList(vrmtemp.videosession, ref outstrXML))
                {
                    m_log.Error("Error in Retrieving video session List");
                    obj.outXml = "";
                    return false;
                }
                else
                    outXml.Append(outstrXML);

                outstrXML = new StringBuilder();
                if (!m_Search.GetLineRateList(vrmtemp.linerate, ref outstrXML))
                {
                     m_log.Error("Error in Retrieving line rate List");
                     obj.outXml = "";
                    return false;
                }
                 else
                    outXml.Append(outstrXML);

                outstrXML = new StringBuilder();
                if (!m_Search.GetVideoEquipment(0, ref outstrXML))
                {
                    m_log.Error("Error in Retrieving video equipment List");
                    obj.outXml = "";
                    return false;
                }
                else
                    outXml.Append(outstrXML);

                outstrXML = new StringBuilder();
                if (!m_Search.GetAudioList(vrmtemp.audio, ref outstrXML))
                {
                    m_log.Error("Error in Retrieving audio List");
                    obj.outXml = "";
                    return false;
                }
                else
                    outXml.Append(outstrXML);

                outstrXML = new StringBuilder();
                if (!m_Search.GetVideoProto(vrmtemp.videoprotocol, ref outstrXML))
                {
                    m_log.Error("Error in Retrieving interface List");
                    obj.outXml = "";
                    return false;
                }
                else
                    outXml.Append(outstrXML);

                outstrXML = new StringBuilder();
                if (!m_Search.GetAudioUsage(vrmtemp.videoprotocol, ref outstrXML))
                {
                    m_log.Error("Error in Retrieving Audio Usage List");
                    obj.outXml = "";
                    return false;
                }
                else
                    outXml.Append(outstrXML);

                FetchTempUserGroups(userid, organizationID,ref outXml, usr);

                m_Search.GetPartyList(vrmtemp, ref outXml);

                outstrXML = new StringBuilder();
                if (!m_Search.getAddressType(ref outstrXML))
                {
                    m_log.Error("Error in Retrieving Address Type List");
                    obj.outXml = "";
                    return false;
                }
                else
                    outXml.Append(outstrXML);

                outXml.Append(" </confInfo>");
                outXml.Append("</template>");

                obj.outXml = outXml.ToString();

            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

        #region FetchTempUserGroups 

        private void FetchTempUserGroups(Int32 userid, Int32 organizationID, ref StringBuilder outXml,vrmUser usr)
        {
            ICriterion criterium = null;
            bool singleDeptMode = true;
            List<ICriterion> criterionList = new List<ICriterion>();
            int mode = 1;
            try
            {
                criterium = Expression.And((Expression.Or(Expression.Eq("Private", 0), Expression.Eq("Owner", userid))),
                          Expression.Eq("orgId", organizationID));
                outXml.Append("<defaultAGroups>");
                if (usr.PreferedGroup > 0)
                {
                    criterionList.Add(criterium);
                    criterionList.Add(Expression.Eq("GroupID", usr.PreferedGroup));
                    m_usrFactory.FetchGroups(criterionList, ref outXml, 0, "");
                }
                outXml.Append("</defaultAGroups>");

                criterionList = new List<ICriterion>();
                criterionList.Add(criterium);
                criterionList.Add(Expression.Eq("GroupID", usr.CCGroup));
                outXml.Append("<defaultCGroups>");
                m_usrFactory.FetchGroups(criterionList, ref outXml, 0, "");
                outXml.Append("</defaultCGroups>");

                if (outXml.ToString().IndexOf("<defaultAGroups><groupID>") > 0)
                    outXml.Append("<groupID>" + usr.PreferedGroup + "</groupID>");
                else
                    outXml.Append("<groupID></groupID>");

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                if (orgInfo != null)
                    multiDepts = orgInfo.MultipleDepartments;

                vrmUser userdetails = new vrmUser();
                if (multiDepts == 1)
                {
                    singleDeptMode = false;
                    userdetails = m_vrmUserDAO.GetByUserId(userid);
                }

                userdetails = m_vrmUserDAO.GetByUserId(userid);
                vrmUserRoles usrRole = m_IUserRolesDao.GetById(userdetails.roleID); //crossaccess

                criterionList = new List<ICriterion>();
                if (mode == 0) //If User is General User
                {
                    criterionList.Add(Expression.Or(Expression.Eq("Private", 0),
                        Expression.Eq("Owner", userdetails.userid)));
                }
                criterionList.Add(Expression.Eq("orgId", organizationID));  //If User is Site Admin

                if (singleDeptMode)
                {
                    if (!userdetails.isSuperAdmin())
                        criterionList.Add(Expression.Or(Expression.Eq("Private", 0), Expression.Eq("Owner", userdetails.userid)));
                }
                else
                {
                    if (!(userdetails.isSuperAdmin() && usrRole.crossaccess == 1))
                    {
                        if (userdetails.isSuperAdmin() && usrRole.crossaccess == 0) //If User is OrgAdmin
                        {
                            ArrayList userId = new ArrayList();
                            List<ICriterion> criterionList1 = new List<ICriterion>();
                            vrmUser superuser = m_vrmUserDAO.GetByUserId(11);
                            criterionList1.Add(Expression.Eq("Admin", 2));
                            criterionList1.Add(Expression.Eq("roleID", superuser.roleID));
                            List<vrmUser> userList = m_vrmUserDAO.GetByCriteria(criterionList1);
                            for (int i = 0; i < userList.Count; i++)
                                userId.Add(userList[i].userid);

                            criterionList.Add(Expression.Or(Expression.Not(Expression.In("Owner", userId)), Expression.Eq("Private", 0)));
                        }
                        else
                            criterionList.Add(Expression.Or(Expression.Eq("Private", 0), Expression.Eq("Owner", userdetails.userid))); //If its Gereral Users
                    }
                }

                outXml.Append("<groups>");
                m_usrFactory.FetchGroups(criterionList, ref outXml, 0, "group");
                outXml.Append("</groups>");

            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
            }
        }

        #endregion

		#region GetConfGMTInfo
        /// <summary>
        /// GetConfGMTInfo - COM to .Net Conversion
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetConfGMTInfo(ref vrmDataObject obj)
        {
            StringBuilder outXML1 = new StringBuilder();
            myVRMException myVRMEx = null;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                String confid = "";
                XmlNode node;
                string outDSTstart = "";
                string outDSTend = "";
                int BASE_DATE = 2004;
                int userID= 0;

                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out userID);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                if ((userID <= 0))
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/confID");
                if(node != null)
                    confid = node.InnerXml.Trim();

                CConfID Conf = new CConfID(confid);
                if (Conf.instance == 0)
                    Conf.instance = 1;

                List<ICriterion> CriterionUser = new List<ICriterion>();
                CriterionUser.Add(Expression.Eq("userid", userID));
                List<vrmUser> Userl = m_vrmUserDAO.GetByCriteria(CriterionUser);

                vrmUser UserH = Userl[0];
                String dtfrmt = "MM/dd/yyyy";
                String tmefrmt = "hh:mm tt";
                dtfrmt = UserH.DateFormat.Trim();
                if (UserH.TimeFormat.Trim() == "0") 
                    tmefrmt = "HH:mm";

                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("confid", Conf.ID));
                criterionList.Add(Expression.Eq("instanceid",Conf.instance));
                List<vrmConference> confList = m_vrmConfDAO.GetByCriteria(criterionList);

                vrmConference conf = confList[0];

                timeZoneData tz = new timeZoneData();
                timeZone.GetTimeZone(conf.timezone, ref tz);
                if (tz != null && conf != null)
                {
                    DateTime StartDate = conf.confdate;
                    DateTime Current = DateTime.Now;
                    int iTimeZone = conf.timezone;
                    double duration = 0.0;
                    Double.TryParse(conf.duration.ToString(), out duration);
                    DateTime EndDate = StartDate.AddMinutes(duration);

                    DateTime DSTstart = tz.CurDSTstart;
                    DateTime DSTend = tz.CurDSTend;
                    string timeZoneName = tz.StandardName;
                    int timeZoneID = tz.TimeZoneID;

                    int iDateDiff, iCurrentYear;
                    int iDST = tz.DST;
                    if (iDST == 1)
                    {
                        iCurrentYear = StartDate.Year;
                        iDateDiff = iCurrentYear - BASE_DATE;

                        switch (iDateDiff.ToString())
                        {
                            case "0":
                                DSTstart = tz.CurDSTstart;
                                DSTend = tz.CurDSTend;
                                break;
                            case "1":
                                DSTstart = tz.CurDSTstart1;
                                DSTend = tz.CurDSTend1;
                                break;
                            case "2":
                                DSTstart = tz.CurDSTstart2;
                                DSTend = tz.CurDSTend2;
                                break;
                            case "3":
                                DSTstart = tz.CurDSTstart3;
                                DSTend = tz.CurDSTend3;
                                break;
                            case "4":
                                DSTstart = tz.CurDSTstart4;
                                DSTend = tz.CurDSTend4;
                                break;
                            case "5":
                                DSTstart = tz.CurDSTstart5;
                                DSTend = tz.CurDSTend5;
                                break;
                            case "6":
                                DSTstart = tz.CurDSTstart6;
                                DSTend = tz.CurDSTend6;
                                break;
                            case "7":
                                DSTstart = tz.CurDSTstart7;
                                DSTend = tz.CurDSTend7;
                                break;
                            case "8":
                                DSTstart = tz.CurDSTstart8;
                                DSTend = tz.CurDSTend8;
                                break;
                            case "9":
                                DSTstart = tz.CurDSTstart9;
                                DSTend = tz.CurDSTend9;
                                break;
                            default:
                                break;
                        }
                        outDSTstart = DSTstart.ToString("MM/dd/yyyy hh:mm:ss");
                        outDSTend = DSTend.ToString("MM/dd/yyyy hh:mm:ss");
                    }
                    outXML1.Append("<confGMT>");
                    outXML1.Append("<confID>" + confid + "</confID>");
                    outXML1.Append("<start>" + StartDate.ToString("MM/dd/yyy " + tmefrmt) + "</start>");
                    outXML1.Append("<end>" + EndDate.ToString("MM/dd/yyy " + tmefrmt) + "</end>");
                    outXML1.Append("<current>" + Current.ToString("MM/dd/yyyy hh:mm:ss") + "</current>");
                    outXML1.Append("<timezoneName>" + timeZoneName + "</timezoneName>");
                    outXML1.Append("<timezoneID>" + timeZoneID.ToString() + "</timezoneID>"); //FB 1676
                    outXML1.Append("<daylightSaving>");
                    outXML1.Append("<start>" + outDSTstart + "</start>");
                    outXML1.Append("<end>" + outDSTend + "</end>");
                    outXML1.Append("</daylightSaving>");
                    outXML1.Append("</confGMT>");

                    obj.outXml = outXML1.ToString();
                }
                else
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                return false;
            }
        }
        #endregion

        #region AssignBridgeAddressToPartys
        /// <summary>
        /// AssignBridgeAddressToPartys 
        /// </summary>
        /// <param name="confid"></param>
        /// <param name="instanceid"></param>
        /// <param name="recurrentmode"></param>
        protected void AssignBridgeAddressToPartys(int confid, int instanceid, int recurrentmode)
        {
            string outXml = "";
            try
            {
                if (instanceid.ToString().Length == 0)
                    instanceid = 0;
                // We have to assign bridge IP/ISDN address for each of the participants
                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("confid", confid));
                criterionList.Add(Expression.Eq("instanceid", instanceid));
                criterionList.Add(Expression.Eq("invitee", 1));// only invited participants
                List<vrmConfUser> ConfUList = m_IconfUser.GetByCriteria(criterionList);

                if (ConfUList.Count >= 0)
                {
                    for (int i = 0; i < ConfUList.Count; i++)
                    {
                        vrmConfUser ConfUser = ConfUList[i];
                        int userid = ConfUser.userid;
                        AssignBridgeAddressToSingleParty(confid, instanceid, userid, recurrentmode);
                    }
                }
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                outXml = "";
            }
        }
        #endregion

        #region AssignBridgeAddressToSingleParty
        /// <summary>
        /// AssignBridgeAddressToSingleParty
        /// </summary>
        /// <param name="confid"></param>
        /// <param name="instanceid"></param>
        /// <param name="userid"></param>
        /// <param name="recurrentmode"></param>
        protected void AssignBridgeAddressToSingleParty(int confid, int instanceid, int userid, int recurrentmode)
        {
            string outXml = "";
            try
            {

                if (instanceid.ToString().Length == 0)
                    instanceid = 0;
                // We have to assign bridge IP/ISDN address for each of the participants
                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("confid", confid));
                criterionList.Add(Expression.Eq("instanceid", instanceid));
                criterionList.Add(Expression.Eq("invitee", 1));// only invited participants
                criterionList.Add(Expression.Eq("userid", userid));// make sure this participant is invited
                List<vrmConfUser> ConfUList = m_IconfUser.GetByCriteria(criterionList);

                vrmConfUser ConfU = ConfUList[0];
                if (ConfU != null)
                {
                    string vidpro = ConfU.defVideoProtocol.ToString();
                    string connectiontype = ConfU.connectionType.ToString();
                    int outsidenetwork = ConfU.outsideNetwork;
                    if (outsidenetwork.ToString().Length == 0)
                        outsidenetwork = 0; // By default inside network
                    string bridgeaddress = "", servicename = "";
                    int bridgeid = 0, batype = 0;
                    List<ICriterion> criterionListU1 = new List<ICriterion>();
                    criterionListU1.Add(Expression.Eq("userid", userid));
                    List<vrmUser> user = m_vrmUserDAO.GetByCriteria(criterionListU1);
                    if (user != null && user.Count != 0)
                        bridgeid = user[0].BridgeID;
                    if (bridgeid.ToString() == "" || bridgeid == 0)
                    {
                        vrmGuestUser guest = m_vrmGuestDAO.GetByUserId(userid);
                        bridgeid = guest.BridgeID;
                    }
                    // The bridge id is the host bridge ID, otherwise
                    // If the party is a VRM user, we have the bridge
                    // If the party is a Guest, we assign the bridge with 
                    // lowest position in chain
                    if (bridgeid == 0)
                    {
                        m_vrmMCU.clearOrderBy();
                        List<ICriterion> criterionLst = new List<ICriterion>();
                        criterionLst.Add(Expression.Eq("deleted", 0));
                        List<vrmMCU> MCUList = m_vrmMCU.GetByCriteria(criterionLst);

                        m_vrmMCU.addOrderBy(Order.Asc("ChainPosition"));
                        vrmMCU mcuL = null;
                        for (int i = 0; i <= MCUList.Count; i++)
                        {
                            mcuL = MCUList[0];
                        }
                        bridgeid = mcuL.BridgeID;
                    }
                    if (vidpro == "1") //IP
                    {
                        if (bridgeid.ToString().Length != 0)
                        {
                            List<ICriterion> criterionList1 = new List<ICriterion>();
                            criterionList1.Add(Expression.Eq("bridgeId", bridgeid));

                            IMCUIPServicesDao IPDao = m_Hardware.GetMCUIPServicesDao();
                            List<vrmMCUIPServices> IPServiceList = IPDao.GetByCriteria(criterionList1);

                            if (IPServiceList != null && IPServiceList.Count != 0)
                            {
                                vrmMCUIPServices ipServices = IPServiceList[0];
                                if (ipServices != null)
                                {
                                    batype = ipServices.addressType;
                                    bridgeaddress = ipServices.ipAddress.ToString();
                                    outsidenetwork = ipServices.networkAccess;
                                    if (outsidenetwork.ToString().Length == 0)
                                        outsidenetwork = 1;
                                    servicename = ipServices.ServiceName.ToString();
                                }
                            }
                        }

                    }
                    else if (vidpro == "2") //ISDN
                    {
                        batype = 4;
                        List<ICriterion> criterionList3 = new List<ICriterion>();
                        criterionList3.Add(Expression.Eq("BridgeId", bridgeid));

                        IMCUISDNServicesDao ISDNDao = m_Hardware.GetMCUISDNServicesDao();
                        List<vrmMCUISDNServices> ISDNList = ISDNDao.GetByCriteria(criterionList3);
                        vrmMCUISDNServices isdnServices = ISDNList[0];
                        if (isdnServices != null)
                        {
                            string prefix = "", serviceid = "";
                            long start = 0, end = 0, last = 0;

                            prefix = isdnServices.prefix.ToString();
                            start = isdnServices.startNumber;
                            end = isdnServices.endNumber;
                            serviceid = isdnServices.SortID.ToString();
                            outsidenetwork = isdnServices.networkAccess;
                            if (outsidenetwork.ToString().Length == 0)
                                outsidenetwork = 1;
                            servicename = isdnServices.ServiceName;

                            last++;
                            if (last < start || last > end)
                                last = start;

                            bridgeaddress = last.ToString();
                        }
                    }
                    if (batype.ToString().Length == 0)
                        batype = 0; //Unknown
                    if (bridgeid.ToString().Length == 0)
                        bridgeid = 0;

                    List<ICriterion> criteriList = new List<ICriterion>();
                    criteriList.Add(Expression.Eq("confid", confid));
                    criteriList.Add(Expression.Eq("userid", userid));
                    if (recurrentmode == 1)
                        criteriList.Add(Expression.Eq("instanceid", instanceid));
                    List<vrmConfUser> ConfuList = m_IconfUser.GetByCriteria(criteriList);
                    vrmConfUser CONFU = ConfuList[0];

                    CONFU.bridgeid = bridgeid;
                    CONFU.bridgeAddressType = batype;
                    CONFU.bridgeIPISDNAddress = bridgeaddress;
                    CONFU.connectStatus = 0;
                    CONFU.outsideNetwork = outsidenetwork;
                    CONFU.mcuServiceName = servicename;
                    m_IconfUser.Update(CONFU);

                }

            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                outXml = "";
            }
        }
        #endregion

        #region CascadeSetup
        /// <summary>
        /// CascadeSetup
        /// </summary>
        /// <param name="confid"></param>
        /// <param name="instanceid"></param>
        /// <param name="recurrentmode"></param>
        protected void CascadeSetup(int confid, int instanceid, int recurrentmode)
        {
            int mediaID = 0, layout = 0;
            ArrayList slaves = new ArrayList();
            try
            {
                if (instanceid.ToString().Length == 0)
                    instanceid = 1;
                // First determine all the bridges being used
                List<ICriterion> CriterionL = new List<ICriterion>();
                CriterionL.Add(Expression.Eq("confid", confid));
                List<vrmConfAdvAvParams> ConfAdvAVP = m_confAdvAvParamsDAO.GetByCriteria(CriterionL);

                vrmConfAdvAvParams ConfAdv = ConfAdvAVP[0];
                if (ConfAdv != null)
                {
                    mediaID = ConfAdv.mediaID;
                    layout = ConfAdv.videoLayoutID;
                }
                if (mediaID == 0 || mediaID.ToString().Length == 0)
                    mediaID = 1;

                if (layout.ToString().Length == 0)
                    layout = 0;

                List<ICriterion> CriterionMCUList = new List<ICriterion>();
                CriterionMCUList.Add(Expression.Eq("confid", confid));
                CriterionMCUList.Add(Expression.Eq("instanceid", instanceid));
                List<vrmConfRoom> ConfROOM = m_IconfRoom.GetByCriteria(CriterionMCUList);
                CriterionMCUList.Add(Expression.Eq("invitee", 1));
                List<vrmConfUser> ConFUser = m_IconfUser.GetByCriteria(CriterionMCUList);
                List<int> Brdgeid = new List<int>();
                for (int i = 0; i < ConfROOM.Count; i++)
                {
                    if (!Brdgeid.Contains(ConfROOM[i].bridgeid))
                        Brdgeid.Add(ConfROOM[i].bridgeid);
                }
                for (int j = 0; j < ConFUser.Count; j++)
                {
                    if (!Brdgeid.Contains(ConFUser[j].bridgeid))
                        Brdgeid.Add(ConFUser[j].bridgeid);
                }
                List<ICriterion> CriteriaMCU = new List<ICriterion>();
                CriteriaMCU.Add(Expression.In("BridgeID", Brdgeid));
                List<vrmMCU> bridges = m_vrmMCU.GetByCriteria(CriteriaMCU);
                m_vrmMCU.addOrderBy(Order.Asc("ChainPosition"));
                vrmMCU MCUListID = null;
                string bridgelist = "", id = "";
                int master = 0;
                for (int k = 0; k < bridges.Count; k++)
                {
                    MCUListID = bridges[k];
                    id = MCUListID.BridgeID.ToString();
                    if (master.ToString().Length == 0)// first record
                        master = MCUListID.BridgeID;
                    else
                        bridgelist += MCUListID.BridgeID.ToString();
                }
                slaves.Add(MCUListID.BridgeID.ToString());
                if (bridgelist.Length != 0) // Found slaves. Have to cascade the call
                {
                    // Now we have the master and the slave bridges
                    // We have to connect them now
                    // First delete any existing cascade connections
                    List<ICriterion> CritriaCascade = new List<ICriterion>();
                    CritriaCascade.Add(Expression.Eq("confid", confid));
                    if (recurrentmode != 0)
                        CritriaCascade.Add(Expression.Eq("instanceid", instanceid));
                    List<vrmConfCascade> cascade = m_IconfCascade.GetByCriteria(CritriaCascade);
                    // setup the cascade links
                    // assign default values for master and slaves
                    int defvideoprotocol = 1; // Default IP
                    int mconnectiontype = 1; // Master dials in
                    string mname = "", maddress = "";
                    int sconnectiontype = 2; // Slave dials out
                    string sname = "", saddress = "";

                    // Get master bridge information
                    List<ICriterion> MasterMCUList = new List<ICriterion>();
                    MasterMCUList.Add(Expression.Eq("bridgeID", master));
                    List<vrmMCU> MasterMCU = m_vrmMCU.GetByCriteria(MasterMCUList);

                    List<ICriterion> CriteriaMCU1 = new List<ICriterion>();
                    CriteriaMCU1.Add(Expression.Eq("bridgeID", master));

                    IMCUIPServicesDao IPDao = m_Hardware.GetMCUIPServicesDao();
                    IList<vrmMCUIPServices> MCUIPSer = IPDao.GetByCriteria(CriteriaMCU1);

                    if (MasterMCU[0] != null && MCUIPSer[0] != null)
                    {
                        mname = MasterMCU[0].BridgeName;
                        maddress = MCUIPSer[0].ipAddress;
                    }
                    for (int i = 0; i < slaves.Count; i++)
                    {
                        string Slave = slaves[i].ToString();
                        int slave = 0;
                        Int32.TryParse(Slave, out slave);
                        // Get other bridge specific information
                        List<ICriterion> SlaveMCULIst = new List<ICriterion>();
                        SlaveMCULIst.Add(Expression.Eq("bridgeID", slave));
                        List<vrmMCU> SlaveMCU = m_vrmMCU.GetByCriteria(SlaveMCULIst);

                        List<ICriterion> CriteriaMCU2 = new List<ICriterion>();
                        CriteriaMCU2.Add(Expression.Eq("bridgeID", slave));

                        IMCUIPServicesDao IPDao1 = m_Hardware.GetMCUIPServicesDao();
                        IList<vrmMCUIPServices> SlaveMCUIPSer = IPDao1.GetByCriteria(CriteriaMCU2);
                        if (SlaveMCU[0] != null && SlaveMCUIPSer[0] != null)
                        {
                            sname = SlaveMCU[0].BridgeName;
                            saddress = SlaveMCUIPSer[0].ipAddress;
                        }
                        // Now we setup the links
                        // 1. On the master side

                        vrmConfCascade MasterSide = new vrmConfCascade();
                        MasterSide.confid = confid;
                        MasterSide.instanceid = instanceid;
                        MasterSide.cascadeLinkId = (i + 1);  // Int
                        MasterSide.cascadelinkname = mname + "-" + sname;
                        MasterSide.masterOrSlave = 1;
                        MasterSide.defVideoProtocol = defvideoprotocol;
                        MasterSide.connectionType = mconnectiontype;
                        MasterSide.ipisdnAddress = saddress;
                        MasterSide.bridgeId = master;
                        MasterSide.bridgeipisdnAddress = maddress;
                        MasterSide.connectStatus = 0;
                        MasterSide.layout = layout;
                        MasterSide.audioOrVideo = mediaID;

                        m_IconfCascade.Save(MasterSide);

                        // 2. On the slave side
                        vrmConfCascade SlaveSide = new vrmConfCascade();
                        SlaveSide.confid = confid;
                        SlaveSide.instanceid = instanceid;
                        SlaveSide.cascadeLinkId = (i + 1);
                        SlaveSide.cascadelinkname = sname + "-" + mname;
                        SlaveSide.masterOrSlave = 0;
                        SlaveSide.defVideoProtocol = defvideoprotocol;
                        SlaveSide.connectionType = sconnectiontype;
                        SlaveSide.ipisdnAddress = maddress;
                        SlaveSide.bridgeId = slave;
                        SlaveSide.bridgeipisdnAddress = saddress;
                        SlaveSide.connectStatus = 0;
                        SlaveSide.layout = layout;
                        SlaveSide.audioOrVideo = mediaID;

                        m_IconfCascade.Save(SlaveSide);
                    }
                }
                // There's only one bridge scheduled for this conference
                // No slaves! Therefore No cascade! Hurray!
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
            }
        }
        #endregion

        #region SetDynamicUser
        /// <summary>
        /// SetDynamicUser - COM to .Net Conversion
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SetDynamicUser(ref vrmDataObject obj)
        {
            StringBuilder outXML1 = new StringBuilder();
            myVRMException myVRMEx = null;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                string confid = "",ipisdnaddress = ""; //FB 2550
                int partyNotify = 0, survey = 0, isInstanceEdit = 1, PublicVMRParty =0;//FB 2348 FB 2550
                XmlNode node;
                int userID = 0;
                int OUTSIDE = 0, BRIDGEID = 0, VIDEOPROTOCOL = 0,roomid =0,conntype=0; //FB 2550

                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out userID);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                if ((userID <= 0))
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/organizationID");
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                if (orgInfo != null)
                    multiDepts = orgInfo.MultipleDepartments;

                node = xd.SelectSingleNode("//login/confID");
                if (node != null)
                    confid = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//login/partyNotify");
                if (node != null)
                {
                    Int32.TryParse(node.InnerXml.Trim(), out partyNotify);
                    if (partyNotify.ToString().Length == 0)
                        partyNotify = 0;
                }

                node = xd.SelectSingleNode("//login/survey");//FB 2348
                if (node != null)
                {
                    Int32.TryParse(node.InnerXml.Trim(), out survey);
                    if (survey.ToString().Length == 0)
                        survey = 0;
                }

                int invitee = 0;
                node = xd.SelectSingleNode("//login/partyInvite");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out invitee);

                CConfID Conf = new CConfID(confid);
                if (Conf.instance == 0)//FB 2550
                {
                    isInstanceEdit = 0;
                    Conf.instance = 1;
                }

                List<ICriterion> criterionListNum = new List<ICriterion>();
                criterionListNum.Add(Expression.Eq("confid", Conf.ID));
                List<vrmConference> confUserList = m_vrmConfDAO.GetByCriteria(criterionListNum);
                //FB 2550 Starts
                criterionListNum.Add(Expression.Eq("instanceid", Conf.instance));
                List<vrmConference> conf = m_vrmConfDAO.GetByCriteria(criterionListNum);
                if ((conf[0].isVMR == 1) && (conf[0].dynamicinvite == 1))
                    invitee = 4;

                if (invitee == 1) // invited
                {
                    string  outside = "", bridgeId = "0";
                    string videoprotocol = "";
                    
                    node = xd.SelectSingleNode("//login/videoProtocol");
                    if (node != null)
                    {
                        videoprotocol = node.InnerXml.Trim();
                        if (videoprotocol == "IP")
                            videoprotocol = "1";
                        else if (videoprotocol == "ISDN")
                            videoprotocol = "2";
                    }

                    node = xd.SelectSingleNode("//login/IPISDNAddress");
                    if (node != null)
                        ipisdnaddress = node.InnerXml.Trim();
                    node = xd.SelectSingleNode("//login/connectionType");
                    if (node != null)
                        Int32.TryParse(node.InnerXml.Trim(), out conntype);
                    node = xd.SelectSingleNode("//login/isOutside");
                    if (node != null)
                    {
                        outside = node.InnerXml.Trim();
                        if (outside.Length == 0)
                            outside = "0";
                    }
                   
                    // Determine the bridgeid we're going to use for this user
                    // For now, always pick the first active bridge
                    List<ICriterion> criterionLst = new List<ICriterion>();
                    criterionLst.Add(Expression.Eq("orgId", organizationID));
                    criterionLst.Add(Expression.Eq("deleted", 0));
                    criterionLst.Add(Expression.Eq("Status", 1));
                    List<vrmMCU> MCUList1 = m_vrmMCU.GetByCriteria(criterionLst);
                    if (MCUList1.Count != 0)
                    {
                        vrmMCU mcuL = MCUList1[0];
                        bridgeId = mcuL.BridgeID.ToString();
                    }
                    
                    Int32.TryParse(outside, out OUTSIDE);
                    Int32.TryParse(bridgeId, out BRIDGEID);
                    Int32.TryParse(videoprotocol, out VIDEOPROTOCOL);

                }
                else if (invitee == 2) // invitee - Room Attendee
                {
                    node = xd.SelectSingleNode("//login/locationID");
                    if (node != null)
                        Int32.TryParse(node.InnerXml.Trim(), out roomid);

                } 
                else if (invitee == 4) // VMR Attendee //FB 2550 Starts 
                {
                    node = xd.SelectSingleNode("//login/locationID");
                    if (node != null)
                        Int32.TryParse(node.InnerXml.Trim(), out roomid);

                    PublicVMRParty = 1;

                } //FB 2550 Ends

                //FB 2250 - Recurrance Issue - Starts
                // First delete the record if the user already is part of  the conference.
                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("confid", Conf.ID));
                criterionList.Add(Expression.Eq("instanceid", Conf.instance));
                criterionList.Add(Expression.Eq("userid", userID));
                List<vrmConfUser> ConfUList = m_IconfUser.GetByCriteria(criterionList);

                if (ConfUList.Count != 0 && ConfUList != null)
                {
                    vrmConfUser ConfUser = ConfUList[0];
                    m_IconfUser.Delete(ConfUser);
                }
                List<ICriterion> PublicVMRcriterionList = null; //FB 2550
                int remVMRParty = 0;
                
                vrmConfUser DynamicUser = new vrmConfUser();
                if (isInstanceEdit > 0)
                {
                    DynamicUser.confid = Conf.ID;
                    DynamicUser.instanceid = Conf.instance;
                    DynamicUser.confuId = conf[0].confnumname;
                    DynamicUser.userid = userID;
                    DynamicUser.invitee = invitee;
                    DynamicUser.defVideoProtocol = VIDEOPROTOCOL;
                    DynamicUser.partyNotify = partyNotify;
                    DynamicUser.connectionType = conntype;
                    DynamicUser.ipisdnaddress = ipisdnaddress;
                    DynamicUser.outsideNetwork = OUTSIDE;
                    DynamicUser.bridgeid = BRIDGEID;
                    DynamicUser.status = 1;
					DynamicUser.Survey = survey;//FB 2348
                    DynamicUser.defLineRate = conf[0].linerate;
                    DynamicUser.partyNotify = partyNotify;
                    DynamicUser.addressType = VIDEOPROTOCOL;
                    DynamicUser.audioOrVideo = 2;
                    DynamicUser.roomId = roomid;
                    DynamicUser.PublicVMRParty = PublicVMRParty;
                    m_IconfUser.Save(DynamicUser);
                }
                else
                {
                    for (int i = 0; i < confUserList.Count; i++)
                    {
                        //FB 2550 Starts
                        ConfUList = new List<vrmConfUser>();
                        PublicVMRcriterionList = new List<ICriterion>();
                        PublicVMRcriterionList.Add(Expression.Eq("confid", confUserList[i].confid));
                        PublicVMRcriterionList.Add(Expression.Eq("instanceid", confUserList[i].instanceid));
                        PublicVMRcriterionList.Add(Expression.Eq("PublicVMRParty", 1));
                        ConfUList = m_IconfUser.GetByCriteria(PublicVMRcriterionList);
                        remVMRParty = 0;
                        remVMRParty = orgInfo.MaxPublicVMRParty - ConfUList.Count;
                        if (remVMRParty <= 0)
                            continue;
                        //FB 2550 End

                        DynamicUser.confid = confUserList[i].confid;
                        DynamicUser.instanceid = confUserList[i].instanceid;
                        DynamicUser.confuId = confUserList[i].confnumname;
                        DynamicUser.userid = userID;
                        DynamicUser.invitee = invitee;
                        DynamicUser.defVideoProtocol = VIDEOPROTOCOL;
                        DynamicUser.partyNotify = partyNotify;
                        DynamicUser.connectionType = conntype;
                        DynamicUser.ipisdnaddress = ipisdnaddress;
                        DynamicUser.outsideNetwork = OUTSIDE;
                        DynamicUser.bridgeid = BRIDGEID;
                        DynamicUser.status = 1;
                        DynamicUser.defLineRate = confUserList[i].linerate;
                        DynamicUser.partyNotify = partyNotify;
                        DynamicUser.addressType = VIDEOPROTOCOL;
                        DynamicUser.audioOrVideo = 2;
                        DynamicUser.roomId = roomid;
                        DynamicUser.PublicVMRParty = PublicVMRParty;
                        m_IconfUser.Save(DynamicUser);
                    }
                }
                //FB 2250 - Recurrance Issue - Ends
                AssignBridgeAddressToPartys(Conf.ID, Conf.instance, Conf.RecurMode);
                CascadeSetup(Conf.ID, Conf.instance, Conf.RecurMode);

                List<ICriterion> GUser = new List<ICriterion>();
                GUser.Add(Expression.Eq("userid", userID));
                List<vrmGuestUser> GuestUser = m_vrmGuestDAO.GetByCriteria(GUser);
                string firstname = "", lastname = "", emailclient = "";
                if (GuestUser.Count != 0)
                {
                    firstname = GuestUser[0].FirstName;
                    lastname = GuestUser[0].LastName;
                    emailclient = GuestUser[0].EmailClient.ToString();
                }

                outXML1.Append("<confirmedConferences>");
                outXML1.Append("<user>");
                outXML1.Append("<firstName>" + firstname + "</firstName>");
                outXML1.Append("<lastName>" + lastname + "</lastName>");
                outXML1.Append("<emailClient>" + emailclient + "</emailClient>");
                outXML1.Append("</user>");

                outXML1.Append("<conferences>");
                outXML1.Append("<conference>");
                if (!FetchUserConfInfo(userID, Conf.ID, Conf.instance, 2, ref outXML1, ref obj, ""))//FB 2154
                    return false;
                if (!FetchUserPartyInfo(userID, Conf.ID, Conf.instance, ref outXML1))
                    return false;

                outXML1.Append("</conference>");
                outXML1.Append("</conferences>");
                outXML1.Append("</confirmedConferences>");

                if (orgInfo.SendConfirmationEmail == 0) //FB 2550
                    m_emailFactory.SendPublicParty(conf, userID); 

                obj.outXml = outXML1.ToString();

                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                return false;
            }
        }
        #endregion

        #region FetchUserConfInfo
        /// <summary>
        /// FetchUserConfInfo (COM to .NET Convertion)
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="confID"></param>
        /// <param name="instanceID"></param>
        /// <param name="mode"></param>
        /// <param name="outXML"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool FetchUserConfInfo(int userID, int confID, int instanceID, int mode, ref StringBuilder outXML, ref vrmDataObject obj, string userEmail)//FB 2154
        {
            vrmConference vrmConfrence = null;
            List<ICriterion> criterionList = new List<ICriterion>();
            List<ICriterion> criterionList1 = new List<ICriterion>();
            myVRMException myVRMEx = new myVRMException();
            long numusers = 0;
            DateTime confdate = DateTime.MinValue;
            DateTime Utcmin = DateTime.MinValue;

            try
            {
                //Utcnow = DateTime.UtcNow; //EmptyDb Issue - Starts
                //Utcmin = Utcnow.AddMinutes(5);

                Utcmin = DateTime.Now.AddMinutes(5);
                timeZone.changeToGMTTime(sysSettings.TimeZone, ref Utcmin); //EmptyDb Issue - End

                DetachedCriteria query = DetachedCriteria.For(typeof(vrmConfUser), "confUser");
                query.SetProjection(Projections.Property("uId"));
                query.Add(Expression.Eq("conf.confid", confID));
                if (instanceID > 0)
                    query.Add(Expression.Eq("conf.instanceid", instanceID));
                if (mode == 2)
                    query.Add(Expression.Ge("conf.confEnd", Utcmin));//FB 1094
                query.Add(Expression.Eq("confUser.userid", userID));
                query.Add(Property.ForName("confUser.confid").EqProperty("conf.confid"));
                query.Add(Property.ForName("confUser.instanceid").EqProperty("conf.instanceid"));
                DetachedCriteria conf = DetachedCriteria.For(typeof(vrmConference), "conf");
                conf.Add(Subqueries.Exists((query)));

                IList confDetails = m_vrmConfDAO.Search(conf);

                if (confDetails.Count <= 0)
                {
                    myVRMEx = new myVRMException(534);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                if (confDetails.Count > 0)
                {
                    vrmConference Confrence = (vrmConference)confDetails[0];

                    if (Confrence.deleted == 1)
                    {
                        myVRMEx = new myVRMException(243);
                        obj.outXml = myVRMEx.FetchErrorMsg();
                        return false;
                    }
                    if (Confrence.status == 1)
                    {
                        myVRMEx = new myVRMException(255);
                        obj.outXml = myVRMEx.FetchErrorMsg();
                        return false;
                    }

                    vrmConfrence = m_vrmConfDAO.GetByConfId(confID, Confrence.instanceid);
                    confdate = vrmConfrence.confdate;
                    timeZone.GMTToUserPreferedTime(vrmConfrence.timezone, ref confdate);

                    outXML.Append("<confInfo>");
                    outXML.Append("<createBy>" + Confrence.conftype + "</createBy>");
                    outXML.Append("<confID>" + Confrence.confid.ToString());
                    if (Confrence.instanceid > 0 && Confrence.recuring == 1)
                        outXML.Append("," + Confrence.instanceid.ToString());
                    outXML.Append("</confID>");
                    outXML.Append("<confUniqueID>" + Confrence.confnumname + "</confUniqueID>");
                    //FB 2136 Start
                    vrmUser user = m_vrmUserDAO.GetByUserId(Confrence.owner);
                    outXML.Append("<confHost>" + user.FirstName +" "+ user.LastName + "</confHost>");
                    //FB 2136 End
                    outXML.Append("<confName>" + Confrence.externalname + "</confName>");
                    outXML.Append("<confPassword>" + Confrence.password + "</confPassword>");
                    outXML.Append("<startDate>" + confdate.ToString("d") + "</startDate>");
                    outXML.Append("<startHour>" + confdate.ToString("HH") + "</startHour>");
                    outXML.Append("<startMin>" + confdate.ToString("mm") + "</startMin>");
                    outXML.Append("<startSet>" + confdate.ToString("tt") + "</startSet>");

                    timeZoneData timezone = new timeZoneData();
                    timeZone.GetTimeZone(Confrence.timezone, ref timezone);
                    string ConfZoneName = timezone.TimeZone;

                    outXML.Append("<timeZoneName>" + ConfZoneName + "</timeZoneName>");
                    outXML.Append("<durationMin>" + Confrence.duration.ToString() + "</durationMin>");
                    outXML.Append("<description>" +m_UtilFactory.ReplaceOutXMLSpecialCharacters(Confrence.description) + "</description>");//FB 2236T

                    criterionList1.Add(Expression.Eq("confid", confID));
                    numusers = m_IconfUser.CountByCriteria(criterionList1);
                    outXML.Append("<numberParticipants>" + numusers + "</numberParticipants>");
                    m_RoomFactory.FetchLocations(confID, Confrence.instanceid, Confrence.recuring, ref outXML, ref obj);
                    GetAttachments(confID, Confrence.instanceid, ref outXML, userEmail, mode);//FB 2154
                    outXML.Append("</confInfo>");
                }
            }
            catch (myVRMException e)
            {
                m_log.Error("FetchUserConfInfo", e);
                obj.outXml = e.FetchErrorMsg();
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("FetchUserConfInfo", e);
                obj.outXml = "";
                return false;
            }
            return true;

        }
        #endregion

        #region FetchUserPartyInfo
        /// <summary>
        /// FetchUserPartyInfo
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="confID"></param>
        /// <param name="instanceID"></param>
        /// <param name="outXML"></param>
        /// <returns></returns>
        public bool FetchUserPartyInfo(int userID, int confID, int instanceID, ref StringBuilder outXML)
        {
            List<ICriterion> criterionList = new List<ICriterion>();
            String VideoProtocol = "";
            try
            {
                criterionList.Add(Expression.Eq("confid", confID));
                criterionList.Add(Expression.Eq("userid", userID));
                if (instanceID > 0)
                    criterionList.Add(Expression.Eq("instanceid", instanceID));
                List<vrmConfUser> confUser = m_IconfUser.GetByCriteria(criterionList);
                for (int i = 0; i < confUser.Count; i++)
                {
                    outXML.Append("<partyInfo>");
                    outXML.Append("<partyInvite>" + confUser[i].invitee + "</partyInvite>");
                    outXML.Append("<response>");
                    outXML.Append("<decision>" + confUser[i].status + "</decision>");
                    outXML.Append("<reason>" + confUser[i].reason + "</reason>");
                    outXML.Append("<connect>");
                    if (confUser[i].invitee == 2)
                    {
                        if (confUser[i].roomId > 0)
                        {
                            outXML.Append("<locationID>" + confUser[i].roomId + "</locationID>");
                            outXML.Append("<mapFile></mapFile>");
                            outXML.Append("<secPassFile></secPassFile>");
                            outXML.Append("<miscAttachFile></miscAttachFile>");
                        }
                        else
                            outXML.Append("<locationID></locationID>");
                    }
                    else
                        outXML.Append("<locationID></locationID>");

                    if (confUser[i].defVideoProtocol < 0 || confUser[i].defVideoProtocol == 1)
                        VideoProtocol = "IP";
                    if (confUser[i].defVideoProtocol == 2)
                        VideoProtocol = "ISDN";

                    outXML.Append("<videoProtocol>" + VideoProtocol.Trim() + "</videoProtocol>");
                    outXML.Append("<IPISDNAddress>" + confUser[i].ipisdnaddress + "</IPISDNAddress>");
                    if (confUser[i].invitee == 3)
                        confUser[i].connectionType = 1; //Set as "Dial-in to MCU" for PC Attendee //FB 2347R
                    outXML.Append("<connectionType>" + confUser[i].connectionType + "</connectionType>");
                    outXML.Append("<MCUIPISDN>" + confUser[i].bridgeIPISDNAddress + "</MCUIPISDN>");
                    outXML.Append("<isOutside>" + confUser[i].outsideNetwork + "</isOutside>");
                    outXML.Append("<addressType>" + confUser[i].addressType + "</addressType>");
                    outXML.Append("</connect>");
                    outXML.Append("</response>");
                    outXML.Append("</partyInfo>");
                }
            }
            catch (myVRMException e)
            {
                m_log.Error("FetchUserPartyInfo", e);
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("FetchUserPartyInfo", e);
                return false;
            }
            return true;
        }

        #endregion

        #region GetAttachments
        /// <summary>
        /// GetAttachments
        /// </summary>
        /// <param name="confID"></param>
        /// <param name="instanceid"></param>
        /// <param name="outXML"></param>
        /// <returns></returns>
        public bool GetAttachments(int confID, int instanceid, ref StringBuilder outXML, string userEmail, int mode)//FB 2154
        {
            List<ICriterion> criterionList = new List<ICriterion>();
            try
            {
                outXML.Append("<fileUpload>");

                //FB 2154
                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                //FB 2436 - Start               
                 if (mode == 1 || (orgInfo.SendAttachmentsExternal <= 0 && CheckEmailDomain(userEmail)) || orgInfo.SendAttachmentsExternal == 1)
                 {
                        //FB 2154      
                        IConfAttachmentDAO confAttach = m_confDAO.GetConfAttachmentDao();
                        criterionList.Add(Expression.Eq("confid", confID));
                        criterionList.Add(Expression.Eq("instanceid", instanceid));
                        List<vrmConfAttachments> attachments = confAttach.GetByCriteria(criterionList);

                        for (int i = 0; i < attachments.Count; i++)
                        {
                            if (attachments[i].attachment != "")
                                if (attachments[i].attachment.Length <= 5 || attachments[i].attachment.Substring(0, 6) != "[ICAL]")
                                    outXML.Append("<file>" + attachments[i].attachment + "</file>");

                        }                    
                 }
                 //FB 2436 - End
                outXML.Append("</fileUpload>");
            }
            catch (myVRMException e)
            {
                m_log.Error("GetAttachments", e);
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("GetAttachments", e);
                return false;
            }
            return true;
        }
        #endregion

        //FB 2027 SetTerminalControl Start
        #region SetTerminalControl
        /// <summary>
        /// SetTerminalControl
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SetTerminalControl(ref vrmDataObject obj)
        {
            try
            {
                StringBuilder outxml = new StringBuilder();
                XmlDocument xd = new XmlDocument();
                myVRMException myVRMEx = null;
                List<ICriterion> critlst = new List<ICriterion>();
                String confID = "";
                int userid = 0, retry = 0, ExtTime = 0;

                xd.LoadXml(obj.inXml);

                if (xd.SelectSingleNode("//login/organizationID") != null)
                    Int32.TryParse(xd.SelectSingleNode("//login/organizationID").InnerText.Trim(), out organizationID);

                if (organizationID < defaultOrgId)
                {
                    myVRMEx = new myVRMException(423);
                    throw myVRMEx;
                }

                if (xd.SelectSingleNode("//login/userID") != null)
                    Int32.TryParse(xd.SelectSingleNode("//login/userID").InnerText.Trim(), out userid);

                if (userid < 11)
                    userid = 11;

                if (xd.SelectSingleNode("//login/confInfo/confID") != null)
                    confID = xd.SelectSingleNode("//login/confInfo/confID").InnerText.Trim();

                CConfID CConf = new CConfID(confID);

                if (xd.SelectSingleNode("//login/confInfo/retry") != null)
                    Int32.TryParse(xd.SelectSingleNode("//login/confInfo/retry").InnerText.Trim(), out retry);

                if (xd.SelectSingleNode("//login/confInfo/extendEndTime") != null)
                    Int32.TryParse(xd.SelectSingleNode("//login/confInfo/extendEndTime").InnerText.Trim(), out ExtTime);


                critlst.Add(Expression.Eq("confid", CConf.ID));
                if (CConf.RecurMode != 1)
                    critlst.Add(Expression.Eq("instanceid", CConf.instance));

                List<vrmConference> Confs = m_vrmConfDAO.GetByCriteria(critlst, true);
                if (Confs.Count < 0)
                {
                    myVRMEx = new myVRMException(532);
                    throw myVRMEx;
                }

                if (retry == 1)
                {
                    Confs[0].status = 0;

                    List<vrmConfRoom> confRooms = m_IconfRoom.GetByCriteria(critlst, true);
                    for (int i = 0; i < confRooms.Count; i++)
                        confRooms[i].connectStatus = 0;

                    if (confRooms.Count > 0)
                        m_IconfRoom.SaveOrUpdateList(confRooms);

                    List<vrmConfUser> confUsers = m_IconfUser.GetByCriteria(critlst, true);
                    for (int i = 0; i < confUsers.Count; i++)
                        confUsers[i].connectStatus = 0;

                    if (confUsers.Count > 0)
                        m_IconfUser.SaveOrUpdateList(confUsers);

                    List<vrmConfCascade> confCascs = m_IconfCascade.GetByCriteria(critlst, true);
                    for (int i = 0; i < confCascs.Count; i++)
                        confCascs[i].connectStatus = 0;

                    if (confCascs.Count > 0)
                        m_IconfCascade.SaveOrUpdateList(confCascs);
                }

                Confs[0].duration = Confs[0].duration + ExtTime;
                m_vrmConfDAO.Update(Confs[0]);
                CheckISDNThreshold(Confs[0]);
                obj.outXml = "<success>1</success>";
                return true;
            }
            catch (myVRMException myVRMEx)
            {
                obj.outXml = myVRMEx.FetchErrorMsg();
                return false;
            }
            catch (Exception ex)
            {
                m_log.Error("SetTerminalControl", ex);
                obj.outXml = "";
                return false;
            }
        }
        #endregion

        #region CheckISDNThreshold
        /// <summary>
        /// CheckISDNThreshold
        /// </summary>
        /// <param name="baseconf"></param>
        private void CheckISDNThreshold(vrmConference baseconf)
        {
            try
            {
                List<ICriterion> critlst = new List<ICriterion>();
                List<vrmConference> Conflist = null;
                myVRMException myVRMEx = null;
                Hashtable currentisdncost = new Hashtable();
                DateTime from = DateTime.Now;
                DateTime to = DateTime.Now;
                Double currIsdnCost = 0.0, isdnthresholdCost = 0.0;

                if (organizationID < defaultOrgId)
                {
                    myVRMEx = new myVRMException(423);
                    throw myVRMEx;
                }

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                if (orgInfo.ISDNTimeFrame == 1)
                {
                    from = new DateTime(from.Year, from.Month, 1, 0, 0, 0);
                    if (from.Month == 12)
                        to = new DateTime(from.Year + 1, 1, 1, 0, 0, 0);
                    else
                        to = new DateTime(from.Year, from.Month + 1, 1, 0, 0, 0);
                }
                else if (orgInfo.ISDNTimeFrame == 2)
                {
                    from = new DateTime(from.Year, 1, 1, 0, 0, 0);
                    to = new DateTime(from.Year + 1, 1, 1, 0, 0, 0);
                }

                critlst.Add(Expression.Ge("confdate", from));
                critlst.Add(Expression.Lt("confdate", to));
                critlst.Add(Expression.Eq("deleted", 0));
                critlst.Add(Expression.Eq("orgId", organizationID));
                Conflist = m_vrmConfDAO.GetByCriteria(critlst);

                for (int i = 0; i < Conflist.Count; i++)
                    ConferenceISDNCost(Conflist[i], ref currentisdncost);

                m_emailFactory.organizationID = organizationID;
                for (int i = 0; i < Mcus.Count; i++)
                {
                    if (currentisdncost.Contains(Mcus[i].BridgeID) && currentisdncost[Mcus[i].BridgeID].ToString().Trim() != "0")
                    {
                        Double.TryParse(currentisdncost[Mcus[i].BridgeID].ToString(), out currIsdnCost);
                        isdnthresholdCost = (Mcus[i].isdnmaxcost * Mcus[i].isdnthreshold) / (100.00);

                        if (orgInfo.SendConfirmationEmail == 0)//FB 2470
                        {
                            if (currIsdnCost > isdnthresholdCost) //Send Threshold Alert Mail
                                m_emailFactory.SendThresholdEmail(Mcus[i], baseconf, currIsdnCost, isdnthresholdCost);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                m_log.Error("CheckISDNThreshold", ex);
            }
        }
        #endregion

        #region  ConferenceISDNCost
        /// <summary>
        ///  ConferenceISDNCost - conference costs
        /// </summary>
        /// <param name="bridgeid"></param>
        /// <param name="confid"></param>
        /// <param name="currentisdncost"></param>
        private void ConferenceISDNCost(vrmConference conf, ref Hashtable bridgesISDNcost)
        {
            try
            {
                List<ICriterion> critlst = new List<ICriterion>();
                Double usrPortCharge = 0.0, RoomPortCharge = 0.0, usrLineCharge = 0.0, roomLineCharge = 0.0;
                int channel = 64, m = 0, quo = 1, epType = 1, eptDuration = 0;
                double bridgeCost = 0.0;

                if (HashTb == null)
                {
                    HashTb = new Hashtable();
                    critlst.Add(Expression.Eq("Status", 1));
                    critlst.Add(Expression.Eq("VirtualBridge", 0));
                    critlst.Add(Expression.Eq("isdnthresholdalert", 1));
                    critlst.Add(Expression.Eq("deleted", 0));
                    critlst.Add(Expression.Or(Expression.Eq("orgId", organizationID), Expression.Eq("isPublic", 1)));
                    Mcus = m_vrmMCU.GetByCriteria(critlst);
                    for (int i = 0; i < Mcus.Count; i++)
                    {
                        if (!HashTb.Contains(Mcus[i].BridgeID))
                        {
                            HashTb.Add(Mcus[i].BridgeID, i);
                            bridgesISDNcost.Add(Mcus[i].BridgeID, 0);
                        }
                    }
                }

                if (Mcus.Count > 0)
                {
                    for (int j = 0; j < conf.ConfUser.Count; j++)
                    {
                        if (conf.ConfUser[j].invitee == 1 && conf.ConfUser[j].defVideoProtocol == 2 && HashTb.ContainsKey(conf.ConfUser[j].bridgeid))
                        {
                            Int32.TryParse(HashTb[conf.ConfUser[j].bridgeid].ToString(), out m);
                            usrPortCharge += Mcus[m].isdnportrate * conf.duration;

                            if (conf.ConfUser[j].connectionType == 2) //Dial out
                            {
                                quo = (conf.ConfUser[j].defLineRate / channel);

                                if ((conf.ConfUser[j].defLineRate % channel) > 0)
                                    quo++;

                                epType = 3;
                                vrmUser usr = m_vrmUserDAO.GetByUserId(conf.ConfUser[j].userid);
                                if (usr != null)
                                    epType = 1;

                                critlst = new List<ICriterion>();
                                critlst.Add(Expression.Eq("confid", conf.confid));
                                critlst.Add(Expression.Eq("instanceid", conf.instanceid));
                                critlst.Add(Expression.Eq("endpointid", conf.ConfUser[j].userid));
                                critlst.Add(Expression.Eq("endpointtype", epType));

                                ComputeEndpointDuration(critlst, ref eptDuration);

                                usrLineCharge = eptDuration * Mcus[m].isdnlinerate * quo;
                            }

                            Double.TryParse(bridgesISDNcost[conf.ConfUser[j].bridgeid].ToString(), out bridgeCost);

                            bridgesISDNcost[conf.ConfUser[j].bridgeid] = bridgeCost + usrLineCharge + usrPortCharge;
                        }
                    }

                    for (int j = 0; j < conf.ConfRoom.Count; j++)
                    {
                        if (conf.ConfRoom[j].defVideoProtocol == 2 && HashTb.ContainsKey(conf.ConfRoom[j].bridgeid))
                        {
                            Int32.TryParse(HashTb[conf.ConfRoom[j].bridgeid].ToString(), out m);
                            RoomPortCharge += Mcus[m].isdnportrate * conf.duration;

                            if (conf.ConfRoom[j].connectionType == 2) //Dial out
                            {
                                quo = (conf.ConfRoom[j].defLineRate / channel);
                                if ((conf.ConfRoom[j].defLineRate % channel) > 0)
                                    quo++;

                                critlst = new List<ICriterion>();
                                critlst.Add(Expression.Eq("confid", conf.confid));
                                critlst.Add(Expression.Eq("instanceid", conf.instanceid));
                                critlst.Add(Expression.Eq("endpointid", conf.ConfRoom[j].roomId));
                                critlst.Add(Expression.Eq("endpointtype", 2));

                                ComputeEndpointDuration(critlst, ref eptDuration);

                                roomLineCharge = eptDuration * Mcus[m].isdnlinerate * quo;
                            }

                            Double.TryParse(bridgesISDNcost[conf.ConfUser[j].bridgeid].ToString(), out bridgeCost);

                            bridgesISDNcost[conf.ConfUser[j].bridgeid] = bridgeCost + usrLineCharge + usrPortCharge;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                m_log.Error("ConferenceISDNCost", ex);
            }

        }
        #endregion

        #region ComputeEndpointDuration
        /// <summary>
        /// ComputeEndpointDuration
        /// </summary>
        /// <param name="critlist"></param>
        /// <param name="tot"></param>
        private void ComputeEndpointDuration(List<ICriterion> critlist, ref int tot)
        {
            try
            {
                int seconds = 0;
                DateTime prev = new DateTime();
                m_IconfMontDAO.clearOrderBy();
                m_IconfMontDAO.addOrderBy(Order.Asc("timestamp"));
                List<vrmConfMonitor> confMonts = m_IconfMontDAO.GetByCriteria(critlist);

                tot = 0;
                for (int i = 0; i < confMonts.Count; i++)
                {
                    if (i == 0)
                        prev = confMonts[i].timestamp;
                    else
                        prev = confMonts[i - 1].timestamp;

                    if (confMonts[i].status == 1)
                    {
                        Int32.TryParse((confMonts[i].timestamp - prev).TotalSeconds.ToString(), out seconds);
                        tot += seconds;
                    }
                }

            }
            catch (Exception ex)
            {
                m_log.Error("ComputeEndpointDuration", ex);

            }
        }

        #endregion
        //FB 2027 SetTerminalControl End
	
		#region GetAvailableRoom

        public bool GetAvailableRoom(ref vrmDataObject obj)
        {
            StringBuilder outXml = new StringBuilder();
            Int32 multiDepts = 0;
            Int32 confid = 0;
            Int32 instanceid = 0;
            Int32 confType = 0;//FB 2170
            Int32 iPublic = 0;//FB 2392
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;
                node = xd.SelectSingleNode("//conferenceTime/userID");
                Int32 userid = Int32.Parse(node.InnerXml.Trim());

                node = xd.SelectSingleNode("//conferenceTime/confID");
                String cid = node.InnerXml.Trim();

                if (cid.ToLower() == "new")
                {
                    confid = 0;
                    instanceid = 0;
                }
                else
                {
                    CConfID cconf = new CConfID(cid);
                    confid = cconf.ID;
                    instanceid = cconf.instance;
                }

                node = xd.SelectSingleNode("//conferenceTime/organizationID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out organizationID);
                
                //FB 2274 Starts
                node = xd.SelectSingleNode("//conferenceTime/multisiloOrganizationID");
                string multisiloOrgID = "";
                int mutliOrgID = 0;
                if (node != null)
                    multisiloOrgID = node.InnerXml.Trim();
                Int32.TryParse(multisiloOrgID, out mutliOrgID);
                if (mutliOrgID > 11)
                    organizationID = mutliOrgID;
                //FB 2274 Ends

                if (organizationID < defaultOrgId)
                {
                    myVRMEx = new myVRMException(423);
                    throw myVRMEx;
                }
                //FB 2170 start
                node = xd.SelectSingleNode("//conferenceTime/confType");
                if (node != null)
                    if (!Int32.TryParse(node.InnerText.Trim(), out confType))
                        confType = 0;
                //FB 2170 end
                node = xd.SelectSingleNode("//conferenceTime/startDate");
                String confdate = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//conferenceTime/startHour");
                String startTime = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//conferenceTime/startMin");
                startTime += ":" + node.InnerXml.Trim();

                node = xd.SelectSingleNode("//conferenceTime/startSet");
                startTime += ":45 " + node.InnerXml.Trim();

                node = xd.SelectSingleNode("//conferenceTime/timeZone");
                Int32 tzone = 0;
                if (node != null)
                    tzone = (node.InnerXml.Trim().Length > 0) ? Int32.Parse(node.InnerXml.Trim()) : sysSettings.TimeZone;

                node = xd.SelectSingleNode("//conferenceTime/durationMin");
                String duration = "00";
                if (node != null)
                    duration = (node.InnerXml.Trim() == "") ? "00" : node.InnerXml.Trim();

                node = xd.SelectSingleNode("//conferenceTime/immediate");
                String immediate = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//conferenceTime/recurring");
                String recurring = node.InnerXml.Trim();

                //FB 2089 start
                //Media type => none:0,audio:1,video:2, all types:-1
                int mediaType = -1;
                node = xd.SelectSingleNode("//conferenceTime/mediaType");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out mediaType);
                //if (mediaType == 0)
                //    mediaType = -1;
                //FB 2089 end

                int serviceType = -1;//FB 2219
                node = xd.SelectSingleNode("//conferenceTime/ServiceType");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out serviceType);

                node = xd.SelectSingleNode("//conferenceTime/Public");//FB 2392
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out iPublic);

                //FB 2448 Starts
                int RoomVMR = 0;//FB 2219
                node = xd.SelectSingleNode("//conferenceTime/isRoomVMR");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out RoomVMR);
                //FB 2448 Ends

                DateTime confdatetime = Convert.ToDateTime(confdate + " " + startTime);

                node = xd.SelectSingleNode("//conferenceTime/selected");//FB 2219
                String selectedloc = "";
                ArrayList selrooms = new ArrayList();
                if (node != null)
                {
                    selectedloc = node.InnerXml.Trim();
                    selrooms = new ArrayList(selectedloc.Split(','));
                }

                //timeZone.changeToGMTTime(tzone, ref confdatetime);

                Boolean bHasDepartment = false;

                List<vrmUserDepartment> deptList = new List<vrmUserDepartment>();
                List<ICriterion> userCriterionList = new List<ICriterion>();
                userCriterionList.Add(Expression.Eq("userId", userid));

                deptList = m_IuserDeptDAO.GetByCriteria(userCriterionList);

                if (deptList.Count > 0)
                    bHasDepartment = true;

                ArrayList roomList = new ArrayList();
                String rmStr = "";
                m_RoomFactory.PublicRoom = iPublic;//FB 2392
                m_RoomFactory.GetRoomArray(userid, organizationID, ref roomList, bHasDepartment, ref rmStr, mediaType, serviceType, selrooms);//FB 2089, FB 2219

                String roomConflicts = CheckConfRoom(confid, instanceid, immediate, rmStr, confdatetime.ToString(), tzone.ToString(), duration);

                if (orgInfo != null)
                    multiDepts = orgInfo.MultipleDepartments;

                if (roomList.Count == 0)
                {
                    String outputXML = roomConflicts;
                    obj.outXml = outputXML;
                }
                String stmt = "";
                if (roomConflicts != "")
                {
                    stmt = "and Name NOT IN " + roomConflicts;
                }

                m_RoomFactory.Fetch3LvAvailableLocations(organizationID, 1, stmt, userid, bHasDepartment, ref outXml, ref confType, mediaType, serviceType, selectedloc);//FB 2170 //FB 2089, FB 2219

                obj.outXml = outXml.ToString();

                return true;
            }
            catch (myVRMException myVRMEx)
            {
                m_log.Error("myVRMException GetAvailableRoom :" + myVRMEx.Message);
                obj.outXml = myVRMEx.FetchErrorMsg();
                return false;
            }
            catch (Exception ex)
            {
                m_log.Error("GetAvailableRoom :" + ex.Message);
                obj.outXml = "";
                return false;
            }
        }

        #region GetRoombyMediaService
        /// <summary>
        /// This is created for PIM request
        /// Get all rooms that match the selection criterion but dont check for avaliability FB 2038
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetRoombyMediaService(ref vrmDataObject obj)
        {
            StringBuilder outXml = new StringBuilder();
            Int32 multiDepts = 0;
            Int32 confid = 0;
            Int32 instanceid = 0;
            Int32 confType = 0;//FB 2170
            Int32 iPublic = 0;//FB 2392
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;
                node = xd.SelectSingleNode("//conferenceTime/userID");
                Int32 userid = Int32.Parse(node.InnerXml.Trim());

                node = xd.SelectSingleNode("//conferenceTime/confID");
                String cid = node.InnerXml.Trim();

                if (cid.ToLower() == "new")
                {
                    confid = 0;
                    instanceid = 0;
                }
                else
                {
                    CConfID cconf = new CConfID(cid);
                    confid = cconf.ID;
                    instanceid = cconf.instance;
                }

                node = xd.SelectSingleNode("//conferenceTime/organizationID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out organizationID);

                if (organizationID < defaultOrgId)
                {
                    myVRMEx = new myVRMException(423);
                    throw myVRMEx;
                }
                //FB 2170 start
                node = xd.SelectSingleNode("//conferenceTime/confType");
                if (node != null)
                    if (!Int32.TryParse(node.InnerText.Trim(), out confType))
                        confType = 0;
                //FB 2170 end
                node = xd.SelectSingleNode("//conferenceTime/startDate");
                String confdate = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//conferenceTime/startHour");
                String startTime = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//conferenceTime/startMin");
                startTime += ":" + node.InnerXml.Trim();

                node = xd.SelectSingleNode("//conferenceTime/startSet");
                startTime += ":45 " + node.InnerXml.Trim();

                node = xd.SelectSingleNode("//conferenceTime/timeZone");
                Int32 tzone = 0;
                if (node != null)
                    tzone = (node.InnerXml.Trim().Length > 0) ? Int32.Parse(node.InnerXml.Trim()) : sysSettings.TimeZone;

                node = xd.SelectSingleNode("//conferenceTime/durationMin");
                String duration = "00";
                if (node != null)
                    duration = (node.InnerXml.Trim() == "") ? "00" : node.InnerXml.Trim();

                node = xd.SelectSingleNode("//conferenceTime/immediate");
                String immediate = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//conferenceTime/recurring");
                String recurring = node.InnerXml.Trim();

                //FB 2089 start
                //Media type => none:0,audio:1,video:2, all types:-1
                int mediaType = -1;
                node = xd.SelectSingleNode("//conferenceTime/mediaType");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out mediaType);
                //if (mediaType == 0)
                //    mediaType = -1;
                //FB 2089 end

                int serviceType = -1;//FB 2219
                node = xd.SelectSingleNode("//conferenceTime/ServiceType");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out serviceType);
				
				node = xd.SelectSingleNode("//conferenceTime/Public");//FB 2392
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out iPublic);


                //FB 2448 Starts
                int VMRRoom = 0;
                node = xd.SelectSingleNode("//conferenceTime/isRoomVMR");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out VMRRoom);
                //FB 2448 Ends

                DateTime confdatetime = Convert.ToDateTime(confdate + " " + startTime);

                node = xd.SelectSingleNode("//conferenceTime/selected");//FB 2219
                String selectedloc = "";
                ArrayList selrooms = new ArrayList();
                if (node != null)
                {
                    selectedloc = node.InnerXml.Trim();
                    selrooms = new ArrayList(selectedloc.Split(','));
                }

                //timeZone.changeToGMTTime(tzone, ref confdatetime);

                Boolean bHasDepartment = false;

                List<vrmUserDepartment> deptList = new List<vrmUserDepartment>();
                List<ICriterion> userCriterionList = new List<ICriterion>();
                userCriterionList.Add(Expression.Eq("userId", userid));

                deptList = m_IuserDeptDAO.GetByCriteria(userCriterionList);

                if (deptList.Count > 0)
                    bHasDepartment = true;

                ArrayList roomList = new ArrayList();
                String rmStr = "";
                m_RoomFactory.PublicRoom = iPublic;//FB 2392
                m_RoomFactory.GetRoomArray(userid, organizationID, ref roomList, bHasDepartment, ref rmStr, mediaType, serviceType, selrooms);//FB 2089, FB 2219

                
                m_RoomFactory.Fetch3LvAvailableLocations(organizationID, 1, "", userid, bHasDepartment, ref outXml, ref confType, mediaType, serviceType, selectedloc);//FB 2170 //FB 2089, FB 2219

                obj.outXml = outXml.ToString();

                return true;
            }
            catch (myVRMException myVRMEx)
            {
                m_log.Error("myVRMException GetAvailableRoom :" + myVRMEx.Message);
                obj.outXml = myVRMEx.FetchErrorMsg();
                return false;
            }
            catch (Exception ex)
            {
                m_log.Error("GetAvailableRoom :" + ex.Message);
                obj.outXml = "";
                return false;
            }
        }
        #endregion

        #endregion

        #region CheckConfRoom

        public String CheckConfRoom(Int32 confid, Int32 instanceid, String immediate, String roomList, String confdate, String timezoneid, String duration)
        {
            String startTime = "";
            String endTime = "";
            String outputXML = "";

            try
            {
                if (immediate == "1")
                    startTime = " dbo.changeTOGMTtime(" + timezoneid + ",getdate()) "; //FB 2023
                    //startTime = " dateadd(minute," + timezoneid + ",getdate()) ";
                else
                    startTime = " dbo.changeTOGMTtime(" + timezoneid + ",'" + confdate + "') ";

                endTime = " dateadd(minute," + duration + "," + startTime + ") ";

                String stmt = "";
                stmt = " select cf.roomId,cf.confid,cf.instanceid, l.Name from myVRM.DataLayer.vrmConfRoom cf, myVRM.DataLayer.vrmConference c, myVRM.DataLayer.vrmRoom l ";
                stmt += " where ( (" + startTime + " >= cf.StartDate and " + startTime + "  < dateadd(minute, cf.Duration,cf.StartDate)) ";
                stmt += " or (cf.StartDate >= " + startTime + "  and cf.StartDate < " + endTime + ") ) and c.confid = cf.confid and c.instanceid=cf.instanceid ";
                stmt += " and c.deleted = 0  AND ( NOT (cf.confid = "+ confid.ToString() +")) and l.roomId = cf.roomId ";
                if (roomList.Trim() != "") //FB 2038
                    stmt += " and l.roomId in (" + roomList + ")";

                IList conferences = m_vrmConfDAO.execQuery(stmt);

                String conflictRooms = "(";
                int first = 1;
                bool conflict = false;

                foreach (object[] obj in conferences)
                {

                    int roomid = Int32.Parse(obj[0].ToString());
                    int rmconfid = Int32.Parse(obj[1].ToString());
                    int rminstanceid = Int32.Parse(obj[2].ToString());
                    String rmName = obj[3].ToString();

                    //if (checkForSplit(roomid, confdate, rmconfid.ToString(), rminstanceid.ToString(), timezoneid, duration)) //For Split Concept
                    //{
                    if (first == 0)
                        conflictRooms += ", ";
                    if (first == 1)
                        first = 0;

                    conflictRooms += "'" + rmName + "'";
                    conflict = true;
                    // }
                }

                conflictRooms += ")";
                if (conflict == true)
                    outputXML = conflictRooms;

                return outputXML;
            }
            catch (Exception ex)
            {
                m_log.Error("CheckConfRoom: " + ex.Message);
                return "";
            }
        }

        #endregion
		//FB 2558 WhyGo - Starts
        #region CheckConfRoomforOutLook

        public String CheckConfRoomforOutLook(Int32 confid, Int32 instanceid, String immediate, String confdate, 
                String timezoneid, String duration, Int32 userid, Int32 organizationID, Boolean bHasDepartment,
                int mediatype, int serviceType, ArrayList selrooms, int isPublic)
        {
            String startTime = "";
            String endTime = "";
            String outputXML = "";
            
            String stmt = "";

            try
            {
                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                vrmUser usr = new vrmUser();
                usr = m_vrmUserDAO.GetByUserId(userid);

                if (immediate == "1")
                    startTime = " dbo.changeTOGMTtime(" + timezoneid + ",getdate()) "; //FB 2023
                    //startTime = " dateadd(minute," + timezoneid + ",getdate()) ";
                else
                    startTime = " dbo.changeTOGMTtime(" + timezoneid + ",'" + confdate + "') ";

                endTime = " dateadd(minute," + duration + "," + startTime + ") ";

                stmt = " select cf.roomId,cf.confid,cf.instanceid, l.Name from myVRM.DataLayer.vrmConfRoom cf, myVRM.DataLayer.vrmConference c, myVRM.DataLayer.vrmRoom l ";
                stmt += " where ( (" + startTime + " >= cf.StartDate and " + startTime + "  < dateadd(minute, cf.Duration,cf.StartDate)) ";
                stmt += " or (cf.StartDate >= " + startTime + "  and cf.StartDate < " + endTime + ") ) and c.confid = cf.confid and c.instanceid=cf.instanceid ";
                stmt += " and c.deleted = 0  AND ( NOT (cf.confid = "+ confid.ToString() +")) and l.roomId = cf.roomId ";
                //if (roomList.Trim() != "") //FB 2038
                // stmt += " and l.roomId in (" + roomList + ")";

                if (!usr.isSuperAdmin() && orgInfo.MultipleDepartments == 1) //Doubt - Do we need to test Department based rooms for OutLook?
                {
                    if (bHasDepartment)
                    {
                        stmt += " and l.roomId in (select crd.roomId from myVRM.DataLayer.vrmLocDepartment  crd, myVRM.DataLayer.vrmUserDepartment UD";
                        stmt += " where crd.departmentId = UD.departmentId and UD.userId = " + userid + " )";
                    }
                    else
                    {
                        stmt += " and l.orgId='" + organizationID + "' and ";
                        stmt += " l.roomId not in (select roomId from myVRM.DataLayer.vrmLocDepartment) and roomId <> 11";
                    }

                }
                if (mediatype >= 0)
                    stmt += " and l.VideoAvailable='" + mediatype + "'";

                if (serviceType > 0)
                {
                    if (!selrooms[0].Equals(""))
                        stmt += " and (l.ServiceType = '" + serviceType + "' or l.roomId in ('" + selrooms + "'))";
                    else
                        stmt += " and l.ServiceType < '" + serviceType + "' ";
                }

                stmt += " and l.isPublic = '" + isPublic + "'";
                stmt += " and l.Disabled = 0 ";

                IList conferences = m_vrmConfDAO.execQuery(stmt);

                String conflictRooms = "(";
                int first = 1;
                bool conflict = false;

                foreach (object[] obj in conferences)
                {

                    int roomid = Int32.Parse(obj[0].ToString());
                    int rmconfid = Int32.Parse(obj[1].ToString());
                    int rminstanceid = Int32.Parse(obj[2].ToString());
                    String rmName = obj[3].ToString();

                    //if (checkForSplit(roomid, confdate, rmconfid.ToString(), rminstanceid.ToString(), timezoneid, duration)) //For Split Concept
                    //{
                        if (first == 0)
                            conflictRooms += ", ";
                        if (first == 1)
                            first = 0;

                        conflictRooms += "'" + rmName + "'";
                        conflict = true;
                   // }
                }

                conflictRooms += ")";
                if (conflict == true)
                    outputXML = conflictRooms;

                return outputXML;
            }
            catch (Exception ex)
            {
                m_log.Error("CheckConfRoomforOutLook: " + ex.Message);
                return "";
            }
        }

        #endregion
		//FB 2558 WhyGo - End
        #region checkForSplit

        public Boolean checkForSplit(Int32 roomid, String confdate, String rmconfid, String rminstanceid, String timezoneid, String duration)
        {
            String chkTime = "";
            String startTime = "";
            String endTime = "";
            String stmt = "";
            try
            {
                chkTime = " dbo.changeTOGMTtime(" + timezoneid + ",'" + confdate + "') ";

                stmt = "SELECT startTime, dbo.changeTOGMTtime(" + timezoneid + ",'" + confdate + "') as  checkTime, ";
                stmt += " dateadd(minute, duration, startTime) as endtime, ";
                stmt += " dateadd(minute," + duration + ", " + chkTime + ") as totalTime ";
                stmt += " FROM myVRM.DataLayer.vrmRoomSplit WHERE roomId = " + roomid;
                stmt += " AND(confid = " + rmconfid + " AND instanceid = " + rminstanceid + ")";
                                                
                IList rooms = m_IconfRoom.execQuery(stmt);

                if (rooms.Count > 0)
                {
                    foreach (object[] obj in rooms)
                    {
                        DateTime checkTime = Convert.ToDateTime(obj[1]);
                        DateTime sTime = Convert.ToDateTime(obj[0]);
                        DateTime eTime = Convert.ToDateTime(obj[2]);
                        DateTime totalTime = Convert.ToDateTime(obj[3]);

                        if ((checkTime >= sTime && checkTime <= eTime) || (checkTime < sTime && totalTime > sTime))
                            return true;
                        else
                            return false;
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                m_log.Error("CheckConfRoom: " + ex.Message);
                return false;
            }
        }
        #endregion

        //FB 2027 - ResponseInvite & GetInstances - Starts
        #region ResponseInvite
        /// <summary>
        /// ResponseInvite (COM to .NET Convertion)
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool ResponseInvite(ref vrmDataObject obj)
        {
            bool bRet = true;
            StringBuilder outXML = new StringBuilder();
            myVRMException myVRMEx = new myVRMException();
            List<ICriterion> criterionList = new List<ICriterion>();
            vrmUtility util = new vrmUtility();
            vrmUser users = new vrmUser();
            vrmGuestUser gustusers = new vrmGuestUser();
            vrmConference Confrence = new vrmConference();
            cryptography.Crypto crypto = new Crypto();
            String encrptuserid = "", encrptconfid = "", orgid = "", encrypt = "", userEmail = "";//FB 2154
            int UserID = 0, confid = 0, instanceid = 0;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node = null;

                node = xd.SelectSingleNode("//login/encrypted");
                if (node != null)
                    encrypt = node.InnerText.Trim();

                node = xd.SelectSingleNode("//login/organizationID");
                if (node != null)
                    orgid = node.InnerXml.Trim();
                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);
                if (organizationID < defaultOrgId)
                {
                    myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    encrptuserid = node.InnerText.Trim();
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/confID");
                if (node != null)
                    encrptconfid = node.InnerText.Trim();
                if (encrptconfid.Length <= 0 || encrptuserid.Length <= 0)
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                if (encrypt == "")
                    encrypt = "1";
                if (encrypt != "1")
                {
                    util.simpleDecrypt(ref encrptuserid);
                    util.simpleDecrypt(ref encrptconfid);
                }

                Int32.TryParse(encrptuserid, out UserID);
                CConfID ConfMode = new CConfID(encrptconfid);
                confid = ConfMode.ID;
                instanceid = ConfMode.instance;

                users = m_vrmUserDAO.GetByUserId(UserID);

                outXML.Append("<responseInvite>");
                outXML.Append("<userID>" + UserID + "</userID>");
                outXML.Append("<globalInfo>");
                outXML.Append("<userName>");
                if (users != null)
                {
                    userEmail = users.Email;//FB 2154
                    outXML.Append("<firstName>" + users.FirstName + "</firstName>");
                    outXML.Append("<lastName>" + users.LastName + "</lastName>");
                    outXML.Append("<emailClient>" + users.EmailClient + "</emailClient>");
                }
                else
                {
                    gustusers = m_vrmGuestDAO.GetByUserId(UserID);
                    userEmail = gustusers.Email;//FB 2154
                    outXML.Append("<firstName>" + gustusers.FirstName + "</firstName>");
                    outXML.Append("<lastName>" + gustusers.LastName + "</lastName>");
                    outXML.Append("<emailClient>" + gustusers.EmailClient + "</emailClient>");
                }
                outXML.Append("</userName>");
                String lot = "";
                m_usrFactory.FetchLotusInfo(UserID, obj, ref lot); //Lotus Info
                outXML.Append(lot);
                sysTechData sysTechDta;
                sysTechDta = m_ISysTechDAO.GetTechByOrgId(organizationID);  // Fetch Tech support details
                outXML.Append("<contactDetails>");
                outXML.Append("<name>" + sysTechDta.name + "</name>");
                outXML.Append("<email>" + sysTechDta.email + "</email>");
                outXML.Append("<phone>" + sysTechDta.phone + "</phone>");
                outXML.Append("<additionInfo>" + sysTechDta.info + "</additionInfo>");
                outXML.Append("<feedback>" + sysTechDta.feedback + "</feedback>");
                outXML.Append("</contactDetails>");

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);
                outXML.Append("<dialoutEnabled>" + orgInfo.DialOut + "</dialoutEnabled>");
                outXML.Append("<doubleBookingEnabled>" + orgInfo.doublebookingenabled + "</doubleBookingEnabled>");
                outXML.Append("<EnableSecurityBadge>" + orgInfo.EnableSecurityBadge + "</EnableSecurityBadge>");//FB 2136

                outXML.Append("</globalInfo>");
                outXML.Append("<timezones></timezones>");
                outXML.Append("<conferences>");
                outXML.Append("<conference>");
                int LOGIN_CONFERENCE = 1;
                int EMAIL_RESPONSE = 2;
                if (!FetchUserConfInfo(UserID, confid, instanceid, EMAIL_RESPONSE, ref outXML, ref obj, userEmail))//FB 2154
                    return false;
                if (!FetchUserPartyInfo(UserID, confid, instanceid, ref outXML))
                    return false;

                outXML.Append("</conference>");
                outXML.Append("</conferences>");
                outXML.Append("</responseInvite>");

                obj.outXml = outXML.ToString();
            }
            catch (myVRMException e)
            {
                m_log.Error("ResponseInvite", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("ResponseInvite", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

        #region GetInstances
        /// <summary>
        /// GetInstances (COM to .NET Convertion)
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetInstances(ref vrmDataObject obj)
        {
            bool bRet = true;
            StringBuilder outXML = new StringBuilder();
            myVRMException myVRMEx = new myVRMException();
            List<ICriterion> criterionList = new List<ICriterion>();
            vrmUtility util = new vrmUtility();
            cryptography.Crypto crypto = new Crypto();
            vrmConference ConfDetails = new vrmConference();
            DateTime UtcNow = DateTime.MinValue;
            String encrptconfid = "", encrypt = "";
            int UserID = 0, confid = 0, instanceid = 0;

            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node = null;

                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out UserID);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/conferenceID");
                if (node != null)
                    encrptconfid = node.InnerText.Trim();
                if (encrptconfid.Length <= 0 || UserID <= 0)
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/encrypted");
                if (node != null)
                    encrypt = node.InnerText.Trim();

                if (encrypt == "")
                    encrypt = "1";

                if (encrypt != "1")
                {
                    util.simpleDecrypt(ref encrptconfid);
                }

                CConfID ConfMode = new CConfID(encrptconfid);
                confid = ConfMode.ID;
                instanceid = ConfMode.instance;

                vrmUser users = m_vrmUserDAO.GetByUserId(UserID);
                ConfDetails = m_vrmConfDAO.GetByConfId(confid, instanceid);

                outXML.Append("<getInstance>");
                outXML.Append("<confInfo>");
                outXML.Append("<confID>" + confid + "</confID>");
                outXML.Append("<confName>" + ConfDetails.externalname + "</confName>");
                outXML.Append("</confInfo>");
                outXML.Append("<instances>");

                //UtcNow = DateTime.UtcNow; //EmptyDb Issue - Starts

                UtcNow = DateTime.Now.AddMinutes(5);
                timeZone.changeToGMTTime(sysSettings.TimeZone, ref UtcNow); //EmptyDb Issue - End

                criterionList.Add(Expression.Eq("confid", confid));
                criterionList.Add(Expression.Eq("deleted", 0));
                criterionList.Add(Expression.Ge("confdate", UtcNow));
                if (ConfMode.RecurMode == 0 && instanceid > 0)
                    criterionList.Add(Expression.Eq("instanceid", instanceid));

                List<vrmConference> conf = m_vrmConfDAO.GetByCriteria(criterionList);

                for (int i = 0; i < conf.Count; i++)
                {
                    outXML.Append("<instance>");
                    GetInstanceInfo(UserID, confid, conf[i].instanceid, ref outXML, ref obj);
                    if (!FetchUserPartyInfo(UserID, confid, conf[i].instanceid, ref outXML))
                        return false;
                    outXML.Append("</instance>");
                }
                outXML.Append("</instances>");
                outXML.Append("</getInstance>");

                obj.outXml = outXML.ToString();
            }
            catch (myVRMException e)
            {
                m_log.Error("GetInstances", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("GetInstances", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

        #region GetInstanceInfo
        /// <summary>
        /// GetInstanceInfo (COM to .NET Convertion)
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="confID"></param>
        /// <param name="instanceID"></param>
        /// <param name="outputXML"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetInstanceInfo(int userID, int confID, int instanceID, ref StringBuilder outXML, ref vrmDataObject obj)
        {
            DateTime confdate = DateTime.MinValue;
            vrmConference vrmConf = null;

            try
            {
                DetachedCriteria query = DetachedCriteria.For(typeof(timeZoneData), "timeZone");
                query.SetProjection(Projections.Property("TimeZoneID"));
                query.Add(Expression.Eq("conf.confid", confID));
                query.Add(Expression.Eq("conf.instanceid", instanceID));
                query.Add(Property.ForName("conf.timezone").EqProperty("timeZone.TimeZoneID"));
                DetachedCriteria conf = DetachedCriteria.For(typeof(vrmConference), "conf");
                conf.Add(Subqueries.Exists((query)));

                IList confDetails = m_vrmConfDAO.Search(conf);
                foreach (vrmConference confrnce in confDetails)
                {
                    vrmConf = m_vrmConfDAO.GetByConfId(confID, confrnce.instanceid);
                    confdate = vrmConf.confdate;
                    timeZone.GMTToUserPreferedTime(vrmConf.timezone, ref confdate);

                    timeZoneData timezone = new timeZoneData();
                    timeZone.GetTimeZone(confrnce.timezone, ref timezone);
                    string ConfZoneName = timezone.StandardName;

                    outXML.Append("<instanceInfo>");
                    outXML.Append("<seriesID>" + confrnce.instanceid + "</seriesID>");
                    outXML.Append("<uniqueID>" + confrnce.confnumname + "</uniqueID>");
                    outXML.Append("<startDate>" + confdate.ToString("d") + "</startDate>");
                    outXML.Append("<startHour>" + confdate.ToString("HH") + "</startHour>");
                    outXML.Append("<startMin>" + confdate.ToString("mm") + "</startMin>");
                    outXML.Append("<startSet>" + confdate.ToString("tt") + "</startSet>");
                    outXML.Append("<timeZoneName>" + ConfZoneName + "</timeZoneName>");
                    outXML.Append("<durationMin>" + confrnce.duration + "</durationMin>");
                    m_RoomFactory.FetchLocations(confID, confrnce.instanceid, confrnce.recuring, ref outXML, ref obj);
                    outXML.Append("<pending>" + confrnce.status + "</pending>");
                    outXML.Append("<public>" + confrnce.isPublic + "</public>");
                    outXML.Append("<dynamicInvite>" + confrnce.dynamicinvite + "</dynamicInvite>");
                    outXML.Append("<recurring>" + confrnce.recuring + "</recurring>");
                    //GetConfFoodInfo - Not used
                    //GetConfResourceInfo - Not used
                    outXML.Append("<confFood></confFood>");
                    outXML.Append("<confResource></confResource>");
                    outXML.Append("</instanceInfo>");
                }
            }
            catch (myVRMException e)
            {
                m_log.Error("GetInstanceInfo", e);
                obj.outXml = e.FetchErrorMsg();
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("GetInstanceInfo", e);
                obj.outXml = "";
                return false;
            }
            return true;

        }
        #endregion
        //FB 2027 - ResponseInvite & GetInstances - End

        #region GetTemplate
        /// <summary>
        /// GetTemplate (COM to .Net conversion)
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetTemplate(ref vrmDataObject obj)
        {
            bool bRet = true;
            List<ICriterion> criterionList = new List<ICriterion>();
            StringBuilder outXML = new StringBuilder();
            myVRMException myVRMEx = new myVRMException();
            int loginId = 0, templateID = 0;
            String recurEnabled = "1";

            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node = null;

                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out loginId);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/organizationID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out organizationID);
                if (organizationID < defaultOrgId)
                {
                    myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/templateID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out templateID);
                else
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                if (templateID <= 0)
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                outXML.Append("<conference>");
                outXML.Append("<userInfo>");
                vrmUser loginusr = m_vrmUserDAO.GetByUserId(loginId);
                outXML.Append("<userEmail>" + loginusr.Email + "</userEmail>");
                outXML.Append("<emailClient>" + loginusr.EmailClient + "</emailClient>");
                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);
                if (orgInfo != null)
                    recurEnabled = orgInfo.recurEnabled.ToString(); //Organization Module
                outXML.Append("<recurEnabled>" + recurEnabled + "</recurEnabled>");
                outXML.Append("</userInfo>");

                vrmTemplate vrmtemp = m_vrmTempDAO.GetById(templateID);
                outXML.Append("<templateInfo>");
                outXML.Append("<templateID>" + vrmtemp.tmpId + "</templateID>");
                outXML.Append("<templates>");
                outXML.Append("<template>");
                outXML.Append("<ID>" + vrmtemp.tmpId + "</ID>");
                outXML.Append("<name>" + vrmtemp.TmpName + "</name>");
                outXML.Append("<public>" + vrmtemp.TMPPublic + "</public>");

                vrmUser tempowner = m_vrmUserDAO.GetByUserId(vrmtemp.TMPOwner);
                outXML.Append("<owner>");
                outXML.Append("<firstName>" + tempowner.FirstName + "</firstName>");
                outXML.Append("<lastName>" + tempowner.LastName + "</lastName>");
                outXML.Append("</owner>");
                outXML.Append("<description>" + vrmtemp.TMPDescription + "</description>");
                outXML.Append("</template>");
                outXML.Append("</templates>");
                outXML.Append("</templateInfo>");

                outXML.Append("<confInfo>");
                outXML.Append("<createBy></createBy>");
                outXML.Append("<confID></confID>");
                outXML.Append("<confName>" + vrmtemp.externalname + "</confName>");
                outXML.Append("<confPassword>" + vrmtemp.password + "</confPassword>");
                outXML.Append("<durationMin>" + vrmtemp.duration + "</durationMin>");
                outXML.Append("<timeZone>" + loginusr.TimeZone + "</timeZone>");
                outXML.Append("<timezones></timezones>"); //Nodes - not used
                outXML.Append("<description>" + m_UtilFactory.ReplaceOutXMLSpecialCharacters(vrmtemp.confDescription) + "</description>"); //G3

                outXML.Append("<locationList>");
                outXML.Append("<selected>");
                List<int> roomids = new List<int>();
                List<ICriterion> criterionList2 = new List<ICriterion>();
                criterionList2.Add(Expression.Eq("tmpId", templateID));
                List<vrmTempRoom> room = m_TempDAO.GetByCriteria(criterionList2);
                for (int i = 0; i < room.Count; i++)
                {
                    outXML.Append("<level1ID>" + room[i].RoomID + "</level1ID>");
                    roomids.Add(room[i].RoomID);
                }
                outXML.Append("</selected>");
                m_RoomFactory.organizationID = organizationID;
                m_RoomFactory.Fetch3LvTempLocations(loginId, roomids, ref outXML);
                outXML.Append("</locationList>");
                outXML.Append("<publicConf>" + vrmtemp.isPublic + "</publicConf>");
                outXML.Append("<continuous>" + vrmtemp.continous + "</continuous>");
                outXML.Append("<videoLayout>" + vrmtemp.videolayout + "</videoLayout>");
                outXML.Append("<videoSessionID>" + vrmtemp.videosession + "</videoSessionID>");
                outXML.Append("<manualVideoLayout>" + vrmtemp.manualvideolayout + "</manualVideoLayout>");
                outXML.Append("<lectureMode>" + vrmtemp.lecturemode + "</lectureMode>");
                outXML.Append("<lecturer>" + vrmtemp.lecturer + "</lecturer>");
                outXML.Append("<dynamicInvite>" + vrmtemp.dynamicinvite + "</dynamicInvite>");

                vrmTempAdvAvParams TempAdvAvParams = m_TempAdvAvParamsDAO.GetById(templateID);
                outXML.Append("<lineRateID>" + TempAdvAvParams.linerateID + "</lineRateID>");
                outXML.Append("<audioAlgorithmID>" + TempAdvAvParams.audioAlgorithmID + "</audioAlgorithmID>");
                outXML.Append("<videoProtocolID>" + TempAdvAvParams.videoProtocolID + "</videoProtocolID>");

                outXML.Append("<advAVParam></advAVParam>"); //Nodes - not used - Start
                outXML.Append("<videoSession></videoSession>");
                outXML.Append("<lineRate></lineRate>");
                outXML.Append("<videoEquipment></videoEquipment>");
                outXML.Append("<audioAlgorithm></audioAlgorithm>");
                outXML.Append("<audioUsage></audioUsage>");
                outXML.Append("<videoProtocol></videoProtocol>");
                outXML.Append("<defaultAGroups/>");
                outXML.Append("<defaultCGroups/>");
                outXML.Append("<groups/>"); //Nodes - not used - End

                m_Search.GetPartyList(vrmtemp, ref outXML);
                outXML.Append("</confInfo>");
                outXML.Append("</conference>");

                obj.outXml = outXML.ToString();

            }
            catch (myVRMException e)
            {
                m_log.Error("GetTemplate", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("GetTemplate", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion 

        //FB 2027 GetApproveCoference Start

        #region GetApproveConference
        /// <summary>
        /// GetApproveConference
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>

        public bool GetApproveConference(ref vrmDataObject obj)
        {
            bool bRet = true;
            XmlNode node = null;
            string appr = "";
            StringBuilder outxml = new StringBuilder();
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);


                node = xd.SelectSingleNode("//login/userID");
                int userid = Int32.Parse(node.InnerXml.Trim());


                node = xd.SelectSingleNode("//login/organizationID");
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);

                if (organizationID < 11)
                    organizationID = defaultOrgId;

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                outxml.Append("<approveConference>");
                if (GetUserConfInfos(userid, organizationID, obj, ref appr))
                    outxml.Append(appr);
                outxml.Append("</approveConference>");
                obj.outXml = outxml.ToString();
            }
            catch (myVRMException e)
            {

                m_log.Error("vrmException", e);
                obj.outXml = "";
                bRet = false;
            }
            catch (Exception e)
            {

                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }

        #endregion

        #region GetUserConfInfos
        /// <summary>
        /// GetUserConfInfos
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>

        public bool GetUserConfInfos(int userid, int orgid, vrmDataObject obj, ref string appr)
        {
            bool bRet = true;
            StringBuilder outxml = new StringBuilder();
            int Total = 0;
            List<ICriterion> criterionList;
            List<vrmConfApproval> ConfApprovalList;
            IConfApprovalDAO IApprover;
            try
            {
                outxml.Append("<conferences>");
                vrmUser usr = m_vrmUserDAO.GetByUserId(userid);

                DateTime timenow = DateTime.Now.AddMinutes(5);
                timeZone.changeToGMTTime(sysSettings.TimeZone, ref timenow);

                criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Ge("confdate", timenow));
                criterionList.Add(Expression.Eq("deleted", 0));
                criterionList.Add(Expression.Eq("orgId", orgid));
                m_vrmConfDAO.addOrderBy(Order.Asc("confid"));
                List<vrmConference> conf = m_vrmConfDAO.GetByCriteria(criterionList);
                m_vrmConfDAO.clearOrderBy();

                Int32 cnt = 0;
                IApprover = m_confDAO.GetConfApprovalDao();
                Int32 confid = 0;
                for (Int32 c = 0; c < conf.Count; c++)
                {
                    if (confid == conf[c].confid)
                        continue;

                    criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("confid", conf[c].confid));
                    criterionList.Add(Expression.Eq("instanceid", conf[c].instanceid));
                    criterionList.Add(Expression.Eq("decision", 0));
                    if (!usr.isSuperAdmin())
                        criterionList.Add(Expression.Eq("approverid", userid));

                    ConfApprovalList = IApprover.GetByCriteria(criterionList);

                    if (ConfApprovalList != null && ConfApprovalList.Count > 0)
                        cnt = cnt + 1;

                    if (ConfApprovalList.Count == 0)
                        confid = 0;
                    else
                        confid = conf[c].confid;
                }

                Total = cnt;
                outxml.Append("<Approval>" + Total + "</Approval>");
                outxml.Append("</conferences>");
                obj.outXml = outxml.ToString();
                appr = outxml.ToString();
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = "";
                bRet = false;
            }
            catch (Exception e)
            {

                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }

        #endregion

        //FB 2027 GetApproveCoference End

        //FB 2027 SetApproveConference Start
        #region SetApproveConference
        /// <summary>
        /// SetApproveConference
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SetApproveConference(ref vrmDataObject obj)
        {
            bool bRet = true;
            int level = 0;
            int entityID = 0;
            int decision = 0;
            String messg = "";
            String instanceType = "";
            List<vrmConference> conf = null;
            List<vrmConfRoom> rList = null;
            List<vrmMCU> mcuList = null;
            List<ICriterion> criterionList = null;
            List<ICriterion> resCriterionList = null;
            XmlNode node = null;
            XmlNodeList confs = null;
            CConfID Conf = null;
            XmlNodeList entities = null;
            string confid = "";
            string createtype = "";
            List<vrmConfUser> uList = null;
            Boolean sendIcal = false;
            bool Isreccuring = true;
            XmlNode nd = null;
            StringBuilder outXML = new StringBuilder(); // FB 2441
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);


                node = xd.SelectSingleNode("//approveConference/userID");
                int userid = Int32.Parse(node.InnerXml.Trim());


                node = xd.SelectSingleNode("//approveConference/organizationID");
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);

                if (organizationID < 11)
                    organizationID = defaultOrgId;

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                confs = xd.SelectNodes("//approveConference/conferences/conference");

                if (confs != null)
                {
                   outXML.Append("<conferences>");
                    for (int confcnt = 0; confcnt < confs.Count; confcnt++)
                    {
                        nd = confs[confcnt];

                        entityID = 1;
                        decision = 0;

                        node = nd.SelectSingleNode("confID");
                        confid = node.InnerXml.Trim();

                        node = nd.SelectSingleNode("instanceType");
                        if (node != null)
                            instanceType = node.InnerXml.Trim();

                        Conf = new CConfID(confid);

                        criterionList = new List<ICriterion>();
                        resCriterionList = new List<ICriterion>();
                        criterionList.Add(Expression.Eq("confid", Conf.ID));
                        resCriterionList.Add(Expression.Eq("confid", Conf.ID));

                        if (Conf.instance > 0)
                        {
                            criterionList.Add(Expression.Eq("instanceid", Conf.instance));
                            resCriterionList.Add(Expression.Eq("instanceid", Conf.instance));
                            Isreccuring = false;
                        }
                        else
                            resCriterionList.Add(Expression.Eq("instanceid", 1));


                        node = nd.SelectSingleNode("partyInfo/response/level");

                        if (node != null)
                        {
                            if (node.InnerText != "")
                                level = Int32.Parse(node.InnerText);
                        }


                        conf = m_vrmConfDAO.GetByCriteria(criterionList, true);
                        rList = m_IconfRoom.GetByCriteria(resCriterionList, true);
                        uList = m_IconfUser.GetByCriteria(resCriterionList, true);
                        loadMCUInfo(ref mcuList, rList, uList);
                        conf[0].mcuList = mcuList;
                        m_emailFactory.confRoomList = rList;
                        m_emailFactory.confUserLst = uList;

                        switch (level)
                        {
                            case (int)LevelEntity.ROOM:
                                entities = nd.SelectNodes("partyInfo/response/rooms/room");
                                m_level = (int)LevelEntity.MCU;
                                if (conf[0].conftype == vrmConfType.RooomOnly)
                                    m_level = (int)LevelEntity.SYSTEM;
                                break;
                            case (int)LevelEntity.MCU:
                                entities = nd.SelectNodes("partyInfo/response/MCUs/MCU");
                                m_level = (int)LevelEntity.SYSTEM;
                                break;
                            case (int)LevelEntity.SYSTEM:
                                entities = nd.SelectNodes("partyInfo/response/systems/system");
                                m_level = (int)LevelEntity.APPROVED;
                                break;

                        }

                        if (entities != null)
                        {

                            for (int ety = 0; ety < entities.Count; ety++)
                            {
                                if (entities[ety].SelectSingleNode("decision") != null)
                                    if (entities[ety].SelectSingleNode("decision").InnerText != "")
                                        decision = Int32.Parse(entities[ety].SelectSingleNode("decision").InnerText);

                                if (entities[ety].SelectSingleNode("ID") != null)
                                    if (entities[ety].SelectSingleNode("ID").InnerText != "")
                                        entityID = Int32.Parse(entities[ety].SelectSingleNode("ID").InnerText);

                                if (entities[ety].SelectSingleNode("message") != null)
                                    if (entities[ety].SelectSingleNode("message").InnerText != "")
                                        messg = entities[ety].SelectSingleNode("message").InnerText;

                                if (decision > 0)
                                {
                                    SaveOrUpdateConfApproval(userid, entityID, level, Conf.ID, Conf.instance, decision, messg);

                                    if (decision == 2)
                                    {
                                        if (instanceType == "I")
                                        {
                                            // Need to set custom pattern
                                        }

                                        for (int denyconf = 0; denyconf < conf.Count; denyconf++)
                                        {
                                            conf[denyconf].deleted = 1;
                                            conf[denyconf].ConfMode = 4;
                                        }
                                    }
                                    else if (m_confDAO.IsLevelApproved(Conf.ID, Conf.instance, level))
                                        SetLevelApproval(ref conf, ref Conf, ref rList, ref uList);

                                }

                            }
                        }

                        m_vrmConfDAO.SaveOrUpdateList(conf);

                        createtype = "";
                        if (CheckConferenceforDenial(conf))
                            createtype = "D";
                        else if (CheckConferenceApproval(conf))
                            createtype = "C";

                        if (createtype != "")
                        {
                            if (conf[0].IcalID != "" && conf[0].ConfMode != 4)
                            {//Invitation  Email for Plugin FB 2141

                                organizationID = conf[0].orgId;

                                if (organizationID < 11)
                                    organizationID = defaultOrgId;

                                if (orgInfo == null)
                                    orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                                if (orgInfo.PluginConfirmations > 0 && orgInfo.SendConfirmationEmail ==1)//FB 2470
                                    m_emailFactory.SendConfirmationEmailsforPlugin(conf);

                            }//Invitation  Email for Plugin FB 2141
                            
                            m_emailFactory.SendConfirmationEmails(conf);

                            if (conf[0].conftype != vrmConfType.RooomOnly  && orgInfo.SendConfirmationEmail ==1 )//FB 2470
                               m_emailFactory.sendICALforCISCOEndpoints(conf, createtype, false, -1);


                            if (orgInfo.SendIcal > 0 || conf[0].IcalID != "")
                            {
                                if (createtype == "D")
                                {
                                    if (conf[0].IcalID != "")
                                        sendIcal = true;
                                    if ((conf[0].IcalID != "" || orgInfo.SendApprovalIcal > 0) && conf[0].ConfOrigin !=0 && orgInfo.SendConfirmationEmail ==1) //FB 2056 //FB 2470
                                        m_emailFactory.sendICALforParticipants(conf, createtype, true, sendIcal, Isreccuring);
                                }
                                else if (createtype == "C")
                                {
                                    if (conf[0].IcalID != "" && orgInfo.SendApprovalIcal > 0)
                                        sendIcal = true;
                                    if (orgInfo.SendConfirmationEmail == 0) //FB 2470
                                        m_emailFactory.sendICALforParticipants(conf, createtype, false, sendIcal, Isreccuring);

                                    //FB 2441 Starts
                                    List<ICriterion> bridgecriterionList = new List<ICriterion>();
                                    List<vrmConfBridge> bridge = new List<vrmConfBridge>();
                                    bridgecriterionList = new List<ICriterion>();

                                    bridgecriterionList.Add(Expression.Eq("ConfID", Conf.ID));
                                    if (Conf.RecurMode == 0)
                                        bridgecriterionList.Add(Expression.Eq("InstanceID", Conf.instance));

                                    bridge = m_IconfBridge.GetByCriteria(bridgecriterionList);
                                    if (bridge.Count > 0)
                                    {
                                        if (bridge[0].Synchronous == 1)
                                        {
                                            outXML.Append("<conference>");
                                            if (Conf.instance == 0 && conf[0].recuring == 1) //whole recurrence
                                                outXML.Append("<confID>" + Conf.ID + "</confID>");
                                            else
                                                outXML.Append("<confID>" + Conf.ID + "," + Conf.instance + "</confID>");
                                            outXML.Append("<confbrdige>");
                                            if ((orgInfo.NetworkCallLaunch == 2 && conf[0].Secured == 0) || (orgInfo.NetworkCallLaunch == conf[0].Secured) || orgInfo.NetworkCallLaunch == 0)
                                                outXML.Append("<isSynchronous>1</isSynchronous>");
                                            else
                                                outXML.Append("<isSynchronous>0</isSynchronous>");
                                            outXML.Append("</confbrdige>");
                                            outXML.Append("</conference>");
                                        }
                                    }
                                    //FB 2441 Ends
                                }
                            }
                        }
                    }
                    outXML.Append("<sCommand>SetConferenceOnMcu</sCommand>");
                    outXML.Append("</conferences>");
                    obj.outXml = outXML.ToString();
                }

            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = "";
                bRet = false;
            }
            catch (Exception e)
            {

                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

        #region SetLevelApproval
        /// <summary>
        /// SetLevelApproval
        /// </summary>
        /// <param name="confList"></param>
        /// <param name="confInsObj"></param>
        /// <param name="rList"></param>
        /// <param name="uList"></param>
        /// <returns></returns>
        private bool SetLevelApproval(ref List<vrmConference> confList, ref CConfID confInsObj, ref List<vrmConfRoom> rList, ref List<vrmConfUser> uList)
        {
            bool bRet = false;
            List<vrmMCU> mcuList = null;
            Hashtable approver = null;
            int iRecurring = 0;
            vrmUser user = null;
            vrmConference conf = null;

            try
            {


                for (int appconf = 0; appconf < confList.Count; appconf++)
                {
                    conf = confList[appconf];


                    if (conf.recuring == 1)
                    {
                        if (IsPastInstance(conf))
                            continue;
                    }

                    bool working = true;

                    while (working)
                    {

                        switch (m_level)
                        {
                            case (int)LevelEntity.ROOM:
                                approver = new Hashtable();
                                foreach (vrmConfRoom rm in rList)
                                {
                                    foreach (vrmLocApprover la in rm.Room.locationApprover)
                                    {
                                        string roomApp = la.approverid + "|" + la.roomid;
                                        if (!approver.ContainsKey(roomApp))
                                        {
                                            if (la.approverid != conf.owner)//FB 1067
                                            {
                                                approver.Add(roomApp, la.roomid);
                                                SaveOrUpdateConfApproval(la.approverid, la.roomid, (int)LevelEntity.ROOM, conf.confid, conf.instanceid, 0);
                                                if (iRecurring == 0)
                                                {
                                                    user = m_vrmUserDAO.GetByUserId(la.approverid);
                                                    m_emailFactory.sendLevelEmail(conf, (int)LevelEntity.ROOM, user);
                                                    m_level = (int)LevelEntity.ROOM;
                                                }
                                                conf.status = vrmConfStatus.Pending;
                                                working = false;
                                            }
                                        }
                                    }
                                }
                                break;
                            case (int)LevelEntity.MCU:
                                loadMCUInfo(ref mcuList, rList, uList);
                                confList[0].mcuList = mcuList;
                                approver = new Hashtable();
                                foreach (vrmMCU mcu in mcuList)
                                {

                                    foreach (vrmMCUApprover mAp in mcu.MCUApprover)
                                    {
                                        string mcuApp = mAp.approverid + "|" + mAp.mcuid;
                                        if (!approver.ContainsKey(mcuApp))
                                        {
                                            approver.Add(mcuApp, mAp.mcuid);

                                            SaveOrUpdateConfApproval(mAp.approverid, mAp.mcuid, (int)LevelEntity.MCU, conf.confid, conf.instanceid, 0);

                                            if (iRecurring == 0)
                                            {
                                                conf.mcuList = mcuList;
                                                user = m_vrmUserDAO.GetByUserId(mAp.approverid);
                                                m_emailFactory.sendLevelEmail(conf, (int)LevelEntity.MCU, user);
                                                m_level = (int)LevelEntity.MCU;
                                            }
                                            conf.status = vrmConfStatus.Pending;
                                            working = false;
                                        }
                                    }
                                }
                                break;
                            case (int)LevelEntity.DEPT:
                                break;
                            case (int)LevelEntity.SYSTEM:

                                IList<sysApprover> sysApproverList = m_ISysApproverDAO.GetSysApproversByOrgId(conf.orgId); //FB 2164
                                foreach (sysApprover sysApp in sysApproverList)
                                {
                                    conf.status = 1;
                                    SaveOrUpdateConfApproval(sysApp.approverid, 1, (int)LevelEntity.SYSTEM, conf.confid, conf.instanceid, 0);
                                    if (iRecurring == 0)
                                    {
                                        user = m_vrmUserDAO.GetByUserId(sysApp.approverid);
                                        m_emailFactory.sendLevelEmail(conf, (int)LevelEntity.SYSTEM, user);
                                        m_level = (int)LevelEntity.SYSTEM;
                                    }
                                    conf.status = vrmConfStatus.Pending;
                                    working = false;
                                }
                                break;
                            default:
                                conf.status = vrmConfStatus.Scheduled;
                                if (iRecurring == 0)
                                {
                                    m_level = 0;
                                    bRet = true;
                                }
                                working = false;
                                break;
                        }
                        if (working)
                            m_level++;
                    }

                    iRecurring++;

                }
                return bRet;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
        }
        #endregion

        #region SaveOrUpdateConfApproval
        /// <summary>
        /// SaveOrUpdateConfApproval
        /// </summary>
        /// <param name="approverID"></param>
        /// <param name="EntityID"></param>
        /// <param name="EntityType"></param>
        /// <param name="ConfID"></param>
        /// <param name="InstanceID"></param>
        /// <param name="Decision"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public Boolean SaveOrUpdateConfApproval(Int32 approverID, Int32 EntityID, Int32 EntityType, Int32 ConfID, Int32 InstanceID, Int32 Decision, String message)
        {
            vrmConfApproval confApproveobj;
            IConfApprovalDAO IApprover;
            List<ICriterion> criterionList;
            try
            {
                IApprover = m_confDAO.GetConfApprovalDao();
                criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("entityid", EntityID));
                criterionList.Add(Expression.Eq("entitytype", EntityType));
                criterionList.Add(Expression.Eq("confid", ConfID));
                if (InstanceID > 0)
                    criterionList.Add(Expression.Eq("instanceid", InstanceID));
                List<vrmConfApproval> ConfApprList = IApprover.GetByCriteria(criterionList);

                if (ConfApprList.Count < 1)
                {
                    confApproveobj = new vrmConfApproval();
                    ConfApprList.Add(confApproveobj);
                }


                foreach (vrmConfApproval confApprove in ConfApprList)
                {

                    confApprove.approverid = approverID;
                    confApprove.entityid = EntityID;
                    confApprove.entitytype = EntityType;
                    confApprove.confid = ConfID;
                    if (InstanceID > 0)
                        confApprove.instanceid = InstanceID;
                    confApprove.decision = Decision;
                    confApprove.responsemessage = message;
                    confApprove.responsetimestamp = DateTime.Now;
                }
                IApprover.SaveOrUpdateList(ConfApprList);

                return true;
            }
            catch (Exception ex)
            {
                m_log.Error("SytemException in SaveOrUpdateConfApproval", ex);
                return false;
            }
        }
        #endregion

        #region IsPastInstance
        /// <summary>
        /// IsPastInstance
        /// </summary>
        /// <param name="confObj"></param>
        /// <returns></returns>
        private bool IsPastInstance(vrmConference confObj)
        {
            try
            {
                if (confObj.status == vrmConfStatus.Completed || confObj.status == vrmConfStatus.OnMCU || confObj.deleted == 1 || confObj.status == vrmConfStatus.Terminated)
                    return true;

                DateTime serverTime = DateTime.Now;
                timeZone.changeToGMTTime(sysSettings.TimeZone, ref serverTime);
                DateTime confDateTime = confObj.confdate;

                if (confDateTime < serverTime)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                m_log.Error("SytemException in IsPastInstance", ex);
            }
            return false;
        }
        #endregion
        //FB 2027 SetApproveConference End

        //FB 2027 - SetTemplate - Starts
        #region SetTemplate
        /// <summary>
        /// SetTemplate (COM to .Net conversion)
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SetTemplate(ref vrmDataObject obj)
        {
            bool bRet = true;
            myVRMException myVRMEx = new myVRMException();
            vrmTemplate tmpdetails = new vrmTemplate();
            vrmTempAdvAvParams TempParams = new vrmTempAdvAvParams();
            List<ICriterion> criterionList = new List<ICriterion>();
            StringBuilder outXML = new StringBuilder();
            int loginid = 0, tmpId = 0, tmpnewid = 0, tmppublic = 0, confpublic = 0, duration = 0;
            String tmpid = "", tmpname = "", tmpdesc = "", confname = "";
            String confpassword = "", confdesc = "", tmpsetdefault = "";
            int lineRateID = 0, audioAlgorithmID = 0, videoProtocolD = 0, dyninvite = 0;
            int videoLayoutID = 0, dualSteamModeID = 0, conferenceOnPort = 0;
            int encryption = 0, maxAudioParticipants = 0, maxVideoParticipants = 0, mediaID = 0;
            int videoSession = 0, lectureMode = 0, confTempType = 0;
            try
            {
                XmlDataDocument xd = new XmlDataDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node = null;

                node = xd.SelectSingleNode("//template/userInfo/userID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out loginid);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//template/organizationID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out organizationID);
                if (organizationID < defaultOrgId)
                {
                    myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//template/templateInfo/ID");
                if (node != null)
                    tmpid = node.InnerText.Trim();
                if (tmpid != "new")
                {
                    Int32.TryParse(tmpid, out tmpId);
                    tmpdetails = m_vrmTempDAO.GetById(tmpId);
                }
                if (tmpid.Length <= 0)
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//template/templateInfo/name");
                if (node != null)
                    tmpname = node.InnerText.Trim();
                if (tmpname.Length <= 0)
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                tmpdetails.TmpName = tmpname;

                node = xd.SelectSingleNode("//template/templateInfo/public");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out tmppublic);
                tmpdetails.TMPPublic = tmppublic;

                node = xd.SelectSingleNode("//template/templateInfo/setDefault");
                if (node != null)
                    tmpsetdefault = node.InnerText.Trim();

                node = xd.SelectSingleNode("//template/templateInfo/description");
                if (node != null)
                    tmpdesc = node.InnerText.Trim();
                tmpdetails.TMPDescription = tmpdesc;

                node = xd.SelectSingleNode("//template/confInfo/confName");
                if (node != null)
                    confname = node.InnerText.Trim();
                tmpdetails.externalname = confname;

                node = xd.SelectSingleNode("//template/confInfo/confPassword");
                if (node != null)
                    confpassword = node.InnerText.Trim();
                tmpdetails.password = confpassword;

                node = xd.SelectSingleNode("//template/confInfo/durationMin");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out duration);
                tmpdetails.duration = duration;

                node = xd.SelectSingleNode("//template/confInfo/description");
                if (node != null)
                    confdesc = m_UtilFactory.ReplaceInXMLSpecialCharacters(node.InnerText.Trim());//FB 2236
                tmpdetails.confDescription = confdesc;

                node = xd.SelectSingleNode("//template/confInfo/publicConf");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out confpublic);
                tmpdetails.isPublic = confpublic;

                node = xd.SelectSingleNode("//template/confInfo/confType");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out confTempType);
                tmpdetails.conftype = confTempType;

                XmlNodeList AvList = xd.SelectNodes("//template/confInfo/advAVParam");
                if (AvList != null)
                {
                    foreach (XmlNode AvNode in AvList)
                    {
                        node = AvNode.SelectSingleNode("maxAudioPart");
                        Int32.TryParse(node.InnerXml.Trim(), out maxAudioParticipants);
                        node = AvNode.SelectSingleNode("maxVideoPart");
                        Int32.TryParse(node.InnerXml.Trim(), out maxVideoParticipants);
                        node = AvNode.SelectSingleNode("restrictProtocol");
                        Int32.TryParse(node.InnerXml.Trim(), out videoProtocolD);
                        node = AvNode.SelectSingleNode("restrictAV");
                        Int32.TryParse(node.InnerXml.Trim(), out mediaID);
                        node = AvNode.SelectSingleNode("videoLayout");
                        Int32.TryParse(node.InnerXml.Trim(), out videoLayoutID);
                        if (videoLayoutID == 0)
                            videoLayoutID = 1;
                        node = AvNode.SelectSingleNode("maxLineRateID");
                        Int32.TryParse(node.InnerXml.Trim(), out lineRateID);
                        node = AvNode.SelectSingleNode("audioCodec");
                        Int32.TryParse(node.InnerXml.Trim(), out audioAlgorithmID);
                        node = AvNode.SelectSingleNode("videoCodec");
                        Int32.TryParse(node.InnerXml.Trim(), out videoSession);
                        node = AvNode.SelectSingleNode("dualStream");
                        Int32.TryParse(node.InnerXml.Trim(), out dualSteamModeID);
                        node = AvNode.SelectSingleNode("confOnPort");
                        Int32.TryParse(node.InnerXml.Trim(), out conferenceOnPort);
                        node = AvNode.SelectSingleNode("encryption");
                        Int32.TryParse(node.InnerXml.Trim(), out encryption);
                        node = AvNode.SelectSingleNode("lectureMode");
                        Int32.TryParse(node.InnerXml.Trim(), out lectureMode);
                    }
                }
                node = xd.SelectSingleNode("//template/confInfo/dynamicInvite");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out dyninvite);
                tmpdetails.dynamicinvite = dyninvite;

                criterionList.Add(Expression.Eq("orgId", organizationID));
                criterionList.Add(Expression.Eq("TmpName", tmpname.ToUpper()));
                if (tmpid == "new")
                {
                    IList<vrmTemplate> TempIds = m_vrmTempDAO.GetByCriteria(criterionList);
                    if (TempIds.Count > 0)
                    {
                        myVRMEx = new myVRMException(222); //Duplicate Template Name.
                        obj.outXml = myVRMEx.FetchErrorMsg();
                        return false;
                    }

                    string uString = "SELECT MAX(tmpId + 1) as userid FROM myVRM.DataLayer.vrmTemplate";
                    IList list = m_vrmTempDAO.execQuery(uString);
                    
                    if (list[0] != null) //SetTemplate issue
                        tmpnewid = Int32.Parse(list[0].ToString());
                    else
                        tmpnewid = 1;

                    tmpdetails.tmpId = tmpnewid;
                    tmpdetails.TMPOwner = loginid; //newTemplate - LoginId
                    tmpdetails.partynotify = 0;
                    tmpdetails.deleted = 0;
                    tmpdetails.deletereason = "";
                    tmpdetails.lecturer = "";
                    tmpdetails.description = "";
                    tmpdetails.lecturemode = lectureMode;
                    tmpdetails.orgId = organizationID;
                    m_vrmTempDAO.Save(tmpdetails);
                }
                else if (tmpid != "new")
                {
                    if (tmpid.ToLower() != "new")
                    {
                        criterionList.Add(Expression.Not(Expression.Eq("tmpId", tmpId)));
                    }
                    IList<vrmTemplate> TempIds = m_vrmTempDAO.GetByCriteria(criterionList);
                    if (TempIds.Count > 0)
                    {
                        myVRMEx = new myVRMException(222); //Duplicate Template Name.
                        obj.outXml = myVRMEx.FetchErrorMsg();
                        return false;
                    }

                    m_vrmTempDAO.Update(tmpdetails);
                    List<ICriterion> criterionList6 = new List<ICriterion>();
                    criterionList6.Add(Expression.Eq("tmpId", tmpId));
                    List<vrmTempAdvAvParams> tempAdvAv = m_TempAdvAvParamsDAO.GetByCriteria(criterionList6);
                    for (int i = 0; i < tempAdvAv.Count; i++)
                    {
                        m_TempAdvAvParamsDAO.Delete(tempAdvAv[i]);
                    }
                }

                if (tmpid != "new")
                    Int32.TryParse(tmpid, out tmpId);
                else
                    tmpId = tmpnewid;

                vrmUser usrdetails = m_vrmUserDAO.GetByUserId(loginid);

                if (tmpsetdefault == "true")
                {
                    usrdetails.DefaultTemplate = tmpId;

                    m_vrmUserDAO.Update(usrdetails);
                }
                else
                {
                    usrdetails.DefaultTemplate = 0;
                    m_vrmUserDAO.Update(usrdetails);
                }

                TempParams.tmpId = tmpId;
                TempParams.linerateID = lineRateID;
                TempParams.audioAlgorithmID = audioAlgorithmID;
                TempParams.videoProtocolID = videoProtocolD;
                TempParams.videoLayoutID = videoLayoutID;
                TempParams.dualStreamModeID = dualSteamModeID;
                TempParams.conferenceOnPort = conferenceOnPort;
                TempParams.encryption = encryption;
                TempParams.maxAudioParticipants = maxAudioParticipants;
                TempParams.maxVideoParticipants = maxVideoParticipants;
                TempParams.mediaID = mediaID;
                TempParams.videoSession = videoSession;
                TempParams.lectureMode = lectureMode;
                m_TempAdvAvParamsDAO.Save(TempParams);

                //InsertLocations
                criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("tmpId", tmpId));
                List<vrmTempRoom> tmpRoomList = m_TempDAO.GetByCriteria(criterionList);
                for (int j = 0; j < tmpRoomList.Count; j++)
                    m_TempDAO.Delete(tmpRoomList[j]);

                int roomid = 0;
                XmlNodeList rooms = null;
                rooms = xd.SelectNodes("//template/confInfo/locationList/selected/level1ID");

                if (rooms != null)
                {
                    for (int i = 0; i < rooms.Count; i++)
                    {
                        if (rooms[i].InnerText != "")
                        {
                            Int32.TryParse(rooms[i].InnerText, out roomid);
                            vrmRoom tmpRoom = new vrmRoom();
                            vrmTempRoom tmpRoomids = new vrmTempRoom();

                            tmpRoom = m_vrmRoomDAO.GetByRoomId(roomid);
                            tmpRoomids.tmpId = tmpId;
                            tmpRoomids.RoomID = tmpRoom.roomId;
                            tmpRoomids.DefLineRate = tmpdetails.linerate;
                            tmpRoomids.DefVideoProtocol = tmpdetails.videoprotocol;
                            tmpRoomids.Duration = tmpdetails.duration;

                            m_TempDAO.Save(tmpRoomids);
                        }
                    }
                }

                //InsertTempPartys
                criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("TmpID", tmpId));
                List<vrmTempUser> tempUserList = m_TempUserDAO.GetByCriteria(criterionList);
                for (int i = 0; i < tempUserList.Count; i++)
                    m_TempUserDAO.Delete(tempUserList[i]); //First Delete All Partys

                XmlNodeList users = null;
                users = xd.SelectNodes("//template/confInfo/partys/party");
                InsertTempPartys(usrdetails, tmpId, users, ref outXML);

                //TemplateInfo
                String temphql = "";
                temphql = " Select t.tmpId,t.TmpName,t.TMPPublic, t.TMPOwner, u.FirstName,u.LastName,t.TMPDescription, "
                + " t.externalname, t.password, t.duration, t.confDescription,t.isPublic,t.conftype from myVRM.DataLayer.vrmTemplate t, myVRM.DataLayer.vrmUser u  "
                + " where t.tmpId = " + tmpId + " and u.userid = t.TMPOwner and (t.TMPPublic = " + tmppublic + " or t.TMPOwner= " + tmpId + ")";

                IList tmpresult = m_vrmTempDAO.execQuery(temphql);

                object[] tmpInfo = (object[])tmpresult[0];

                outXML.Append("<template>");
                outXML.Append("<userInfo>");
                outXML.Append("<userName>" + usrdetails.FirstName + "</userName>"); //FB 2164
                outXML.Append("</userInfo>");
                outXML.Append("<templateInfo>");
                outXML.Append("<ID>" + tmpInfo[0] + "</ID>");
                outXML.Append("<name>" + tmpInfo[1] + "</name>");
                outXML.Append("<public>" + tmpInfo[2] + "</public>");
                outXML.Append("<owner>");
                outXML.Append("<firstName>" + tmpInfo[4] + "</firstName>");
                outXML.Append("<lastName>" + tmpInfo[5] + "</lastName>");
                outXML.Append("</owner>");
                outXML.Append("<description>" + tmpInfo[6] + "</description>");
                outXML.Append("</templateInfo>");

                outXML.Append("<confInfo>");
                outXML.Append("<confName>" + tmpInfo[7] + "</confName>");
                outXML.Append("<confPassword>" + tmpInfo[8] + "</confPassword>");
                outXML.Append("<durationMin>" + tmpInfo[9] + "</durationMin>");
                outXML.Append("<description>" + m_UtilFactory.ReplaceOutXMLSpecialCharacters(tmpInfo[10].ToString()) + "</description>"); //FB 2236

                //FetchTempLocations
                String hql = "";
                hql = "Select c.roomId,c.Name,c.Capacity,c.ProjectorAvailable,c.MaxPhoneCall,c.VideoAvailable from myVRM.DataLayer.vrmRoom c,  myVRM.DataLayer.vrmTempRoom tr "
                + "where c.roomId = tr.RoomID and tr.tmpId = " + tmpId;

                outXML.Append("<mainLocation>");
                IList tmpRooms = m_vrmRoomDAO.execQuery(hql);
                for (int i = 0; i < tmpRooms.Count; i++)
                {
                    object[] obj3 = (object[])tmpRooms[i];
                    outXML.Append("<location>");
                    outXML.Append("<locationName>" + obj3[1] + "</locationName>");
                    outXML.Append("</location>");
                }
                outXML.Append("</mainLocation>");

                outXML.Append("<publicConf>" + tmpInfo[11] + "</publicConf>");
                outXML.Append("<confType>" + tmpInfo[12] + "</confType>");

                List<ICriterion> criterionList1 = new List<ICriterion>();
                criterionList1.Add(Expression.Eq("TmpID", tmpId));
                criterionList1.Add(Expression.Or(Expression.Eq("invitee", 1), Expression.Eq("invitee", 1)));
                long numberParticipants = m_TempUserDAO.CountByCriteria(criterionList1);
                outXML.Append("<numberParticipants>" + numberParticipants + "</numberParticipants>");

                String usrtable = "";
                //Invited
                usrtable = "U";
                int invitemode = 1;
                outXML.Append("<invited>");
                FetchTempPartys(tmpId, invitemode, usrtable, ref outXML);
                usrtable = "G";
                FetchTempPartys(tmpId, invitemode, usrtable, ref outXML);
                outXML.Append("</invited>");

                //Invitee
                invitemode = 2;
                usrtable = "U";
                outXML.Append("<invitee>");
                FetchTempPartys(tmpId, invitemode, usrtable, ref outXML);
                usrtable = "G";
                FetchTempPartys(tmpId, invitemode, usrtable, ref outXML);
                outXML.Append("</invitee>");

                //PC Invitee//FB 2347
                invitemode = 3;
                usrtable = "U";
                outXML.Append("<pcinvitee>");
                FetchTempPartys(tmpId, invitemode, usrtable, ref outXML);
                usrtable = "G";
                FetchTempPartys(tmpId, invitemode, usrtable, ref outXML);
                outXML.Append("</pcinvitee>");

                //CC
                invitemode = 0;
                usrtable = "U";
                outXML.Append("<cc>");
                FetchTempPartys(tmpId, invitemode, usrtable, ref outXML);
                usrtable = "G";
                FetchTempPartys(tmpId, invitemode, usrtable, ref outXML);
                outXML.Append("</cc>");

                outXML.Append("</confInfo>");
                outXML.Append("</template>");

                obj.outXml = outXML.ToString();

            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

        #region InsertTempPartys
        /// <summary>
        /// InsertTempPartys
        /// </summary>
        /// <param name="usrdetails"></param>
        /// <param name="tmpId"></param>
        /// <param name="users"></param>
        /// <param name="outXMl"></param>
        /// <returns></returns>
        public bool InsertTempPartys(vrmUser usrdetails, int tmpId, XmlNodeList users, ref StringBuilder outXMl)
        {
            int UserID = 0;
            int invite, notify, audorvid = 0, survey;//FB 2348
            int pProtocol, pConnectionType, pAtype, pOutside = 0;
            String pAddress = "";
            string userid, fname, lname, email = "";
            try
            {
                if (users != null)
                {
                    for (int ccnt = 0; ccnt < users.Count; ccnt++)
                    {
                        invite = 0; notify = 0; audorvid = 0; survey = 0;//FB 2348
                        pProtocol = 0; pConnectionType = 0; pAtype = 0; pOutside = 0;
                        userid = ""; lname = ""; fname = ""; email = ""; pAddress = "";

                        if (users[ccnt].SelectSingleNode("partyID").InnerText != "")
                        {
                            userid = users[ccnt].SelectSingleNode("partyID").InnerText;
                            if (userid != "new")
                                Int32.TryParse(userid, out UserID);
                            Int32.TryParse(users[ccnt].SelectSingleNode("partyInvite").InnerText, out invite);
                            Int32.TryParse(users[ccnt].SelectSingleNode("partyNotify").InnerText, out notify);
                            Int32.TryParse(users[ccnt].SelectSingleNode("partyAudVid").InnerText, out audorvid);
                            lname = users[ccnt].SelectSingleNode("partyLastName").InnerText;
                            fname = users[ccnt].SelectSingleNode("partyFirstName").InnerText;
                            email = users[ccnt].SelectSingleNode("partyEmail").InnerText;
                            if (users[ccnt].SelectSingleNode("Survey") != null)//FB 2348
                                Int32.TryParse(users[ccnt].SelectSingleNode("Survey").InnerText, out survey);//FB 2348
                            if (userid == "new" && users[ccnt].SelectSingleNode("partyProtocol") != null)
                            {
                                Int32.TryParse(users[ccnt].SelectSingleNode("partyProtocol").InnerText, out pProtocol);
                                Int32.TryParse(users[ccnt].SelectSingleNode("partyConnectionType").InnerText, out pConnectionType);
                                pAddress = users[ccnt].SelectSingleNode("partyAddress").InnerText.Trim();
                                Int32.TryParse(users[ccnt].SelectSingleNode("partyAddressType").InnerText, out pAtype);
                                Int32.TryParse(users[ccnt].SelectSingleNode("partyIsOutside").InnerText, out pOutside);
                            }
                            if (userid == "new")
                            {
                                //FetchMaxUsers 
                                int newPartyID = 0;
                                string uString = "SELECT MAX(userid + 1) as userid FROM myVRM.DataLayer.vrmUser";
                                IList list = m_vrmUserDAO.execQuery(uString);
								//Empty DB Issue FB 2164
                                int uid = 0;
                                if (list[0] != null)
                                    uid = Int32.Parse(list[0].ToString());

                                string qString = "SELECT MAX(userid + 1) as userid FROM myVRM.DataLayer.vrmGuestUser";
                                IList glist = m_vrmGuestDAO.execQuery(qString);
								//Empty DB Issue FB 2164
                                int gid = 0;
                                if (glist[0] != null)
                                    gid = Int32.Parse(glist[0].ToString());
                                
                                if (uid > gid)
                                    newPartyID = uid;
                                else
                                    newPartyID = gid;
                                String login = "";
                                m_usrFactory.checkUser(newPartyID, login, email, organizationID);

                                //MAX(endpointId)
                                int maxEptid = 0;
                                string Eptid = "SELECT MAX(endpointid + 1) AS endpointid FROM myVRM.DataLayer.vrmEndPoint";
                                IList list1 = m_vrmEpt.execQuery(Eptid);
								//Empty DB Issue FB 2164
                                if (list1[0] != null)
                                    maxEptid = Int32.Parse(list1[0].ToString());
                                else
                                    maxEptid = 1;

                                String pwd = "";
                                if (email.Length > 0)
                                {
                                    cryptography.Crypto crypto = new cryptography.Crypto();
                                    pwd = crypto.encrypt(email);
                                }

                                timeZoneData timezone = new timeZoneData();
                                timeZone.GetTimeZone(usrdetails.TimeZone, ref timezone);
                                int defTimeZone = timezone.TimeZoneID;

                                int roleid = 1;
                                vrmUserRoles genralusr = m_IUserRolesDao.GetById(roleid); //GeneralUser Role

                                vrmGuestUser newGuest = new vrmGuestUser();
                                newGuest.userid = newPartyID;
                                newGuest.FirstName = fname;
                                newGuest.LastName = lname;
                                newGuest.Email = email;
                                newGuest.TimeZone = defTimeZone;
                                newGuest.DefLineRate = usrdetails.DefLineRate;
                                newGuest.DefVideoProtocol = pProtocol;
                                newGuest.Admin = 0;
                                newGuest.Password = pwd;
                                newGuest.MenuMask = genralusr.roleMenuMask;
                                newGuest.IPISDNAddress = pAddress;
                                newGuest.DefaultEquipmentId = 0;
                                newGuest.connectionType = pConnectionType;
                                newGuest.outsidenetwork = pOutside;
                                newGuest.companyId = organizationID;
                                m_vrmGuestDAO.Save(newGuest);

                                vrmTempUser tmpnewusr = new vrmTempUser();
                                tmpnewusr.TmpID = tmpId;
                                tmpnewusr.userid = newPartyID;
                                tmpnewusr.invitee = invite;
                                tmpnewusr.status = 0;
                                tmpnewusr.partyNotify = notify;
                                tmpnewusr.DefLineRate = usrdetails.DefLineRate;
                                tmpnewusr.defVideoProtocol = pProtocol;
                                tmpnewusr.audioOrVideo = audorvid;
                                tmpnewusr.Survey = survey;//FB 2348
                                m_TempUserDAO.Save(tmpnewusr);

                                vrmEndPoint usrEndpoint = new vrmEndPoint();
                                usrEndpoint.endpointid = maxEptid;
                                usrEndpoint.profileId = 0;
                                usrEndpoint.MCUAddressType = 0;
                                usrEndpoint.name = lname;
                                usrEndpoint.protocol = 0;
                                usrEndpoint.connectiontype = pConnectionType;
                                usrEndpoint.addresstype = pAtype;
                                usrEndpoint.address = pAddress;
                                usrEndpoint.outsidenetwork = pOutside;
                                usrEndpoint.videoequipmentid = 0;
                                usrEndpoint.linerateid = usrdetails.DefLineRate;
                                usrEndpoint.bridgeid = 0;
                                usrEndpoint.orgId = organizationID;
                                m_vrmEpt.Save(usrEndpoint);
                            }
                            else
                            {
                                vrmTempUser tmpnewusr = new vrmTempUser();
                                tmpnewusr.TmpID = tmpId;
                                tmpnewusr.userid = UserID;
                                tmpnewusr.invitee = invite;
                                tmpnewusr.status = 0;
                                tmpnewusr.partyNotify = notify;
                                vrmUser partyDetails = new vrmUser();
                                partyDetails = m_vrmUserDAO.GetByUserId(UserID);
                                if (partyDetails == null)
                                {
                                    vrmGuestUser GustDtls = m_vrmGuestDAO.GetByUserId(UserID);
                                    tmpnewusr.DefLineRate = GustDtls.DefLineRate;
                                    tmpnewusr.defVideoProtocol = GustDtls.DefVideoProtocol;
                                }
                                else
                                {
                                    tmpnewusr.DefLineRate = partyDetails.DefLineRate;
                                    tmpnewusr.defVideoProtocol = partyDetails.DefVideoProtocol;
                                }
                                tmpnewusr.audioOrVideo = audorvid;
                                tmpnewusr.Survey = survey; //FB 2348
                                m_TempUserDAO.Save(tmpnewusr);
                            }
                        }
                    }
                }
            }
            catch (myVRMException e)
            {
                m_log.Error("InsertTempPartys", e);
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("InsertTempPartys", e);
                return false;
            }
            return true;
        }
        #endregion

        #region FetchTempPartys
        /// <summary>
        /// FetchTempPartys (COM to .Net conversion)
        /// </summary>
        /// <param name="tempid"></param>
        /// <param name="invitemode"></param>
        /// <param name="usrtable"></param>
        /// <param name="outputxml"></param>
        /// <returns></returns>
        public bool FetchTempPartys(int tempid, int invitemode, String usrtable, ref StringBuilder outputxml)
        {
            try
            {
                String partyhql = "";

                IList partys = null;
                if (usrtable == "U")
                {
                    partyhql = "Select U.userid,U.FirstName,U.LastName,U.Email from myVRM.DataLayer.vrmTempUser C,"
                         + " myVRM.DataLayer.vrmUser U where C.userid = U.userid and C.invitee = " + invitemode + " and C.TmpID= " + tempid;
                    partys = m_vrmUserDAO.execQuery(partyhql);

                }
                else if (usrtable == "G")
                {
                    partyhql = "Select U.userid,U.FirstName,U.LastName,U.Email from myVRM.DataLayer.vrmTempUser C,"
                        + "myVRM.DataLayer.vrmGuestUser U where C.userid = U.userid and C.invitee = " + invitemode + " and C.TmpID= " + tempid;

                    partys = m_vrmGuestDAO.execQuery(partyhql);
                }
                for (int i = 0; i < partys.Count; i++)
                {
                    object[] obj = (object[])partys[i];
                    outputxml.Append("<party>");
                    outputxml.Append("<partyFirstName>" + obj[1] + "</partyFirstName>");
                    outputxml.Append("<partyLastName>" + obj[2] + "</partyLastName>");
                    outputxml.Append("<partyEmail>" + obj[3] + "</partyEmail>");
                    outputxml.Append("</party>");
                }
            }
            catch (myVRMException e)
            {
                m_log.Error("FetchTempPartys", e);
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("FetchTempPartys", e);
                return false;
            }
            return true;
        }
        #endregion
        //FB 2027 - SetTemplate - End

        //FB 2027 - End
        //FB 2141
        #region loadMCUInfo
        /// <summary>
        /// load MCU Info
        /// </summary>
        /// <param name="mcuList"></param>
        /// <param name="confRoom"></param>
        /// <param name="confUser"></param>
        /// <returns></returns>
        public bool loadMCUInfo(ref vrmConference conf, ref vrmMCU MCU)
        {
            List<ICriterion> criterionList = null;
            List<vrmConfRoom> rList = null;
            List<vrmConfUser> uList = null;
            int bId = -1;

            try
            {

                criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("confid", conf.confid));
                criterionList.Add(Expression.Eq("instanceid", conf.instanceid));

                rList = m_IconfRoom.GetByCriteria(criterionList);

                foreach (vrmConfRoom cr in rList)
                {
                    if (cr.bridgeid > 0)
                    {
                        bId = cr.bridgeid;
                        break;

                    }
                }
                if (bId <= 0)
                {
                    uList = m_IconfUser.GetByCriteria(criterionList);
                    foreach (vrmConfUser cu in uList)
                    {
                        if (cu.bridgeid > 0 && cu.invitee == 1) // external only
                        {
                            bId = cu.bridgeid;
                            break;
                        }
                    }
                }

                if (bId > 0)
                    MCU = m_vrmMCU.GetById(bId);

                return true;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
        }
        #endregion

        //FB 2154 start
        #region CheckEmailDomain
        /// <summary>
        /// CheckEmailDomain
        /// </summary>
        /// <param name="emailAddress"></param>
        /// <returns></returns>
        private Boolean CheckEmailDomain(String emailAddress)
        {
            List<vrmEmailDomain> emailDomains = null;
            string usrDomain = "";
            bool inDomain = false;
            IEmailDomainDAO m_IEmailDomainDAO; //FB 2154
            try
            {
                if (emailAddress != "")
                {
                    m_IEmailDomainDAO = m_OrgDAO.GetEmailDomainDAO();
                    emailDomains = m_IEmailDomainDAO.GetActiveEmailDomainbyOrgId(organizationID);

                    if (emailAddress.Split('@').Length > 1)
                    {
                        usrDomain = emailAddress.Split('@')[1];
                        if (usrDomain != "")
                        {
                            var eDomains = from orgDomain in emailDomains
                                           where orgDomain.Emaildomain.ToUpper().Trim() == usrDomain.ToUpper().Trim()
                                           select orgDomain;

                            foreach (vrmEmailDomain domain in eDomains)
                                inDomain = true;

                        }
                    }
                }
            }
            catch (Exception spEX1)
            {
                m_log.Error(spEX1);
                inDomain = false;
            }
            return inDomain;
        }
        #endregion
        //FB 2154 end

        //FB 2342 - Start
        #region GetRoomIDFromRoomQueue

        private void GetRoomIDFromRoomQueue(ref XmlNodeList rooms, ref int validRoomcnt)
        {
            vrmRoom locroom = null;
            

            try
            {
                if (rooms != null)
                {
                    if (rooms.Count > 0)
                    {
                        for (int c = 0; c < rooms.Count; c++)
                        {
                            if (rooms[c].InnerText != "")
                            {
                                locroom = m_vrmRoomDAO.GetByRoomQueue(rooms[c].InnerText);
                                if (locroom != null)
                                {
                                    rooms[c].InnerText = locroom.roomId.ToString();
                                    validRoomcnt = validRoomcnt + 1;
                                }

                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                
                m_log.Error("Trace " + ex.StackTrace.ToString() + " Exception" + ex.Message);
            }
            
        }

        #endregion
        //FB 2342 - End
        //Exchange web services time zone starts
        #region GetTimezoneFromRoomQueue
        private void GetTimezoneFromRoomQueue(ref XmlNode timeZone)
        {
            vrmRoom locroom = null;
            vrmUser user = null;
            try
            {
                if (timeZone != null)
                {
                    
                    locroom = m_vrmRoomDAO.GetByRoomQueue(timeZone.InnerText);
                    timeZone.InnerText = locroom.TimezoneID.ToString();
                }
            }
            catch (Exception ex)
            {

                m_log.Error("Trace " + ex.StackTrace.ToString() + " Exception" + ex.Message);
            }
        }
        //Exchange web services time zone ends
        #endregion

        //FB 2392-Whygo Starts
        #region GetRoomIDFromWhyGoRoomId

        private void GetRoomIDFromWhyGoRoomId(ref XmlNodeList rooms, ref int validRoomcnt)
        {
            ESPublicRoom ESRoom = null;
            try
            {
                if (rooms != null)
                {
                    if (rooms.Count > 0)
                    {
                        for (int c = 0; c < rooms.Count; c++)
                        {
                            if (rooms[c].InnerText != "")
                            {
                                ESRoom = m_IESPublicRoomDAO.GetByWhygoRoomId(Int32.Parse(rooms[c].InnerText));
                                if (ESRoom != null)
                                {
                                    rooms[c].InnerText = ESRoom.RoomID.ToString();
                                    validRoomcnt = validRoomcnt + 1;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                m_log.Error("Trace " + ex.StackTrace.ToString() + " Exception" + ex.Message);
            }
        }
        #endregion

        //FB 2392-Whygo End

        //FB 2342
        #region XMLRefactoring
        /// <summary>
        /// TO refactor the XMl and not disturb setconference method. This is used to refactor minimalistic WS calls
        /// </summary>
        private Boolean XMLRefactoring(ref XmlDocument xd)
        {
            XmlNode node = null;
            Boolean bRet = true;
            String qualifiedName = "";
            String qualifiedValue = "";
            vrmUser cUser = null;
            XmlNode insertBefore = null;
            XmlNodeList roomNodes = null;
            String insertNode = "";
            Boolean bAudVid = false;
            XmlNode timezone = null;
            int validRoomcnt = 0;
            try
            {
                node = xd.SelectSingleNode("//conference/userEMail");
                string orgid = xd.SelectSingleNode("//conference/organizationID").InnerText.Trim();
                if (!Int32.TryParse(orgid, out organizationID))
                    organizationID = 11;

                if (node != null)
                {
                    if (node.InnerText.Trim().IndexOf("@") <= 0)
                    {
                        myVRMEx = new myVRMException(498);
                        throw myVRMEx;
                    }

                    int userid = 0;
                    qualifiedName = "userID";
                    cUser = m_vrmUserDAO.GetByUserEmail(node.InnerText.Trim());
                    if (cUser == null)
                    {
                        m_usrFactory.SetDynamicHost(node.InnerText.Trim(),ref orgid, ref userid);
                        node.InnerXml = userid.ToString();
                    }
                    else
                    {
                        node.InnerXml = cUser.userid.ToString();
                    }
                    RenameNode(ref node, qualifiedName);
                }

                node = xd.SelectSingleNode("//conference/confInfo/confHostEmail");
                if (node != null)
                {
                    int userid = 0;
                    qualifiedName = "confHost";
                    cUser = m_vrmUserDAO.GetByUserEmail(node.InnerText.Trim());
                    if (cUser == null)
                    {
                        m_usrFactory.SetDynamicHost(node.InnerText.Trim(), ref orgid, ref userid);
                        node.InnerXml = userid.ToString();
                    }
                    else
                    {
                        node.InnerXml = cUser.userid.ToString();
                    }
                    RenameNode(ref node, qualifiedName);
                }
                //Exchange web services time zone starts
                timezone = xd.SelectSingleNode("//conference/confInfo/timeZone");
                if (timezone != null)
                {
                    GetTimezoneFromRoomQueue(ref timezone);
                }
                //Exchange web services time zone ends
                roomNodes = xd.SelectNodes("//conference/confInfo/locationList/selected/level1ID");
                if (roomNodes != null)
                {
                    if (roomNodes.Count <= 0)
                    {
                        qualifiedName = "level1ID";
                        roomNodes = xd.SelectNodes("//conference/confInfo/locationList/selected/roomQueue");

                        if (roomNodes != null && roomNodes.Count > 0)
                        {
                            GetRoomIDFromRoomQueue(ref roomNodes, ref validRoomcnt);
                        }
                        else//FB 2392-WhyGo
                        {
                            roomNodes = xd.SelectNodes("//conference/confInfo/locationList/selected/WhyGoRoomId");
                            if (roomNodes != null)
                            {
                                GetRoomIDFromWhyGoRoomId(ref roomNodes, ref validRoomcnt);
                            }
                        }

                        if (roomNodes != null)
                        {
                            GetRoomIDFromRoomQueue(ref roomNodes, ref validRoomcnt);
                            for (int ndeCnt = 0; ndeCnt < roomNodes.Count; ndeCnt++)
                            {
                                node = roomNodes[ndeCnt];
                                RenameNode(ref node, qualifiedName);
                            }

                            if (validRoomcnt > 1)
                                bAudVid = true;
                            else if(validRoomcnt <= 0)
                            {
                                throw new Exception("Empty rooms. Conference has no rooms.");
                            }
                               

                        }


                    }
                }

                //Returning conferences with no rooms
                if (validRoomcnt <= 0)
                {
                    throw new Exception("Empty rooms. Conference has no rooms.");
                }

                /*** Inserting  mandatory nodes  **/
                insertNode = "//conference/confInfo";
                insertBefore = xd.SelectSingleNode("//conference/confInfo/startDate");               
                    
                node = xd.SelectSingleNode("//conference/confInfo/immediate");
                if (node == null)
                {
                    qualifiedName = "immediate";
                    qualifiedValue = "0";
                    InsertNode(ref xd, qualifiedName, qualifiedValue, insertBefore, insertNode);
                }

                node = xd.SelectSingleNode("//conference/confInfo/recurring");
                if (node == null)
                {
                    qualifiedName = "recurring";
                    qualifiedValue = "0";
                    InsertNode(ref xd, qualifiedName, qualifiedValue, insertBefore, insertNode);
                }
                
                node = xd.SelectSingleNode("//conference/confInfo/recurringText");
                if (node == null)
                {
                    qualifiedName = "recurringText";
                    qualifiedValue = "";
                    InsertNode(ref xd, qualifiedName, qualifiedValue, insertBefore, insertNode);
                }

                node = xd.SelectSingleNode("//conference/confInfo/publicConf");
                if (node == null)
                {
                    qualifiedName = "publicConf";
                    qualifiedValue = "0";
                    InsertNode(ref xd, qualifiedName, qualifiedValue, insertBefore, insertNode);
                }

                node = xd.SelectSingleNode("//conference/confInfo/dynamicInvite");
                if (node == null)
                {
                    qualifiedName = "dynamicInvite";
                    qualifiedValue = "0";
                    InsertNode(ref xd, qualifiedName, qualifiedValue, insertBefore, insertNode);
                }

                node = xd.SelectSingleNode("//conference/confInfo/ServiceType");
                if (node == null)
                {
                    qualifiedName = "ServiceType";
                    qualifiedValue = "0";
                    InsertNode(ref xd, qualifiedName, qualifiedValue, insertBefore, insertNode);
                }

                //Advanced AV Params
                insertBefore = xd.SelectSingleNode("//conference/confInfo/party"); 
                node = xd.SelectSingleNode("//conference/confInfo/advAVParam");
                if (node == null)
                {
                    qualifiedName = "advAVParam";
                    qualifiedValue = "<maxAudioPart>1</maxAudioPart><maxVideoPart>1</maxVideoPart><restrictProtocol>4</restrictProtocol><restrictAV>2</restrictAV>";
                    qualifiedValue += "<videoLayout>01</videoLayout><maxLineRateID>1024</maxLineRateID><audioCodec>0</audioCodec><videoCodec>0</videoCodec>";  
                    qualifiedValue += "<dualStream>0</dualStream><confOnPort>0</confOnPort><encryption>0</encryption><lectureMode>0</lectureMode>";
                    qualifiedValue += "<VideoMode>3</VideoMode><SingleDialin>0</SingleDialin>";
                    InsertNode(ref xd, qualifiedName, qualifiedValue, insertBefore, insertNode);
                }

                /*** Inserting non mandatory nodes no null check  **/
                node = xd.SelectSingleNode("//conference/confInfo/confPassword");
                if (node == null)
                {
                    qualifiedName = "confPassword";
                    qualifiedValue = "";
                    InsertNode(ref xd, qualifiedName, qualifiedValue, insertBefore, insertNode);
                }

                node = xd.SelectSingleNode("//conference/confInfo/fileUpload");
                if (node == null)
                {
                    qualifiedName = "fileUpload";
                    qualifiedValue = "<file></file><file></file><file></file>";
                    InsertNode(ref xd, qualifiedName, qualifiedValue, insertBefore, insertNode);
                }

                node = xd.SelectSingleNode("//conference/confInfo/createBy");
                //Exchange connector point-to-point fix starts
                if (node != null)
                {  
                    node.InnerText = "7";

                    if(orgInfo == null)
                        orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                    if (bAudVid)
                        node.InnerText = "2";
                    if (validRoomcnt == 2 && orgInfo.Connect2 == 1)
                        node.InnerText = "4";
                 //Exchange connector point-to-point fix ends
                }
            }
            catch (Exception ex)
            {
                m_log.Error("Trace " + ex.StackTrace.ToString() + " Exception" + ex.Message);
            }
            return bRet;
        }

        private Boolean XMLRefactoringForAVSettings(ref XmlDocument xd, List<vrmConfRoom> confRooms, vrmConference vConf, int roomCnt)
        {
            XmlNode node = null;
            Boolean bRet = true;
            String qualifiedName = "";
            String qualifiedValue = "";
            XmlNode insertBefore = null;
            String insertNode = "";
            try
            {

                String inXML = "<SetAdvancedAVSettings><organizationID>11</organizationID><editFromWeb>1</editFromWeb><language>en</language><isVIP>0</isVIP><isReminder>0</isReminder>";
                inXML += "<UserID>" + vConf.userid + "</UserID>";
                inXML += "<ConfID>" + vConf.confid + "</ConfID>";
                inXML += "<AVParams><isDedicatedEngineer>0</isDedicatedEngineer><isLiveAssitant>0</isLiveAssitant><SingleDialin>0</SingleDialin></AVParams></SetAdvancedAVSettings>";

                xd.LoadXml(inXML);
                if (roomCnt > 1)
                {
                    insertNode = "//SetAdvancedAVSettings";
                    insertBefore = xd.SelectSingleNode("//SetAdvancedAVSettings/AVParams");
                    node = xd.SelectSingleNode("//SetAdvancedAVSettings/Endpoints");
                    if (node == null)
                    {
                        vrmConfRoom confRoom = null;
                        vrmEndPoint ept = null;
                        if (confRooms != null)
                        {
                            for (int ccnt = 0; ccnt < confRooms.Count; ccnt++)
                            {
                                confRoom = null;
                                confRoom = confRooms[ccnt];
                                qualifiedValue = "";
                                if (ccnt == 0)
                                    qualifiedName = "Endpoints";
                                else
                                {
                                    qualifiedName = "Endpoint";
                                    insertNode = "//SetAdvancedAVSettings/Endpoints";
                                    insertBefore = xd.SelectSingleNode("//SetAdvancedAVSettings/Endpoints/Endpoint");
                                }

                                if (ccnt == 0)
                                    qualifiedValue = "<Endpoint>";
                                
                                qualifiedValue += "<Type>R</Type>";
                                qualifiedValue += "<ID>" + confRoom.roomId + "</ID><UseDefault>1</UseDefault><IsLecturer>0</IsLecturer>";
                                qualifiedValue += "<EndpointID>" + confRoom.Room.endpointid + "</EndpointID>";
                                ept = m_vrmEpt.GetByEptId(confRoom.Room.endpointid);
                                qualifiedValue += "<ProfileID>" + ept.profileId + "</ProfileID><BridgeID>" + ept.bridgeid + "</BridgeID>";
                                qualifiedValue += "<AddressType></AddressType><Address></Address><VideoEquipment></VideoEquipment><connectionType></connectionType>";
                                qualifiedValue += "<Bandwidth></Bandwidth><IsOutside></IsOutside><DefaultProtocol></DefaultProtocol><Connection>2</Connection><URL></URL>";
                                qualifiedValue += "<ExchangeID></ExchangeID><APIPortNo>" + ept.ApiPortNo + "</APIPortNo>";
                                //Exchange connector point-to-point fix start
                                if (confRooms.Count == 2 && ccnt == 0 && orgInfo.Connect2 == 1)
                                    qualifiedValue += "<Connect2>1</Connect2>";
                                else if (confRooms.Count == 2 && ccnt == 1 && orgInfo.Connect2 == 1)
                                    qualifiedValue += "<Connect2>0</Connect2>";
                                else
                                    qualifiedValue += "<Connect2>-1</Connect2>";
                                //Exchange connector point-to-point fix ends

                                if (ccnt == 0)
                                    qualifiedValue += "</Endpoint>";

                                InsertNode(ref xd, qualifiedName, qualifiedValue, insertBefore, insertNode);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                m_log.Error("Trace " + ex.StackTrace.ToString() + " Exception" + ex.Message);
            }
            return bRet;
        }


        private void RenameNode(ref XmlNode node, string qualifiedName)
        {
            try
            {
                if (node.NodeType == XmlNodeType.Element)
                {
                    XmlElement oldElement = (XmlElement)node;
                    XmlElement newElement =
                    node.OwnerDocument.CreateElement(qualifiedName);
                    newElement.InnerText = oldElement.InnerText;
                    if (oldElement.ParentNode != null)
                    {
                        oldElement.ParentNode.ReplaceChild(newElement, oldElement);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
 
            }
        }

        private void InsertNode(ref XmlDocument xd, string qualifiedName, string value, XmlNode insertBefore, String insertNode)
        {
            XmlNode node = null;
            try
            {
                node = xd.CreateElement(qualifiedName);
                node.InnerXml = value;
                xd.SelectSingleNode(insertNode).InsertBefore(node, insertBefore);

            }
            catch (Exception ex)
            {
                throw ex;

            }
        }

        #endregion


        //FB 2342

        //FB 2448 Start

        #region GetConfMCUDetails
        /// <summary>
        /// GetConfMCUDetails
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetConfMCUDetails(ref vrmDataObject obj)
        {
            List<ICriterion> criterionList = null;
            List<vrmConference> cList = null;
            List<vrmConfRoom> rList = null;
            List<vrmConfUser> uList = null;
            int confid = 0; int instanceid = 0;
            DateTime databaseservertime = DateTime.Now;
            DateTime MCUCurrenttime = DateTime.Now;
            try
            {
                StringBuilder OutXml = new StringBuilder();
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                 sqlCon = new ns_SqlHelper.SqlHelper(m_configPath);
                sqlCon.OpenConnection();
                sqlCon.OpenTransaction();
                XmlNode node;
                node = xd.SelectSingleNode("//GetConfMCUDetails/UserID");
                Int32 userid = Int32.Parse(node.InnerXml.Trim());

                node = xd.SelectSingleNode("//GetConfMCUDetails/organizationID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out organizationID);

                node = xd.SelectSingleNode("//GetConfMCUDetails/multisiloOrganizationID");
                string multisiloOrgID = "";
                int mutliOrgID = 0;
                if (node != null)
                    multisiloOrgID = node.InnerXml.Trim();
                Int32.TryParse(multisiloOrgID, out mutliOrgID);
                if (mutliOrgID > 11)
                    organizationID = mutliOrgID;
                
                if (organizationID < defaultOrgId)
                {
                    myVRMEx = new myVRMException(423);
                    throw myVRMEx;
                }

                node = xd.SelectSingleNode("//GetConfMCUDetails/ConfID");
                String cid = node.InnerXml.Trim();

                if (cid.ToLower() == "new")
                {
                    confid = 0;
                    instanceid = 0;
                }
                else
                {
                    CConfID cconf = new CConfID(cid);
                    confid = cconf.ID;
                    instanceid = cconf.instance;
                }             
                criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("confid", confid));
                criterionList.Add(Expression.Eq("instanceid", instanceid));
                cList = m_vrmConfDAO.GetByCriteria(criterionList, true);
                rList = m_IconfRoom.GetByCriteria(criterionList, true);
                uList = m_IconfUser.GetByCriteria(criterionList,true);
                OutXml.Append("<GetConfMCUDetails>");
                if (cList.Count > 0)
                {
                    OutXml.Append("<confinfo>");
                    OutXml.Append("<ConfID>" + cList[0].confid.ToString() + "</ConfID>");
                    OutXml.Append("<confName>" + cList[0].externalname + "</confName>");
                    OutXml.Append("<ConfStartDatetime>" + cList[0].confdate.ToString() + "</ConfStartDatetime>");
                    OutXml.Append("<Duration>" + cList[0].duration.ToString() + "</Duration>");
                    OutXml.Append("</confinfo>");
                    OutXml.Append("<Bridgelist>");
                    if (rList.Count > 0)
                    {
                        for (int i = 0; i < rList.Count; i++)
                        {
                            OutXml.Append("<Bridge>");
                            OutXml.Append("<BridgeID>" + rList[i].bridgeid.ToString() + "</BridgeID>");
                            vrmMCU MCUDetails = m_vrmMCU.GetById(rList[i].bridgeid);
                            OutXml.Append("<BridgeName>" + MCUDetails.BridgeName + "</BridgeName>");
                            
                            timeZoneData tz = new timeZoneData();
                            timeZone.GetTimeZone(MCUDetails.timezone, ref tz);
                            OutXml.Append("<BrigeTimezone>" + tz.TimeZone + "</BrigeTimezone>");

                            if (MCUDetails.CurrentDateTime != DateTime.MinValue)
                            {
                                MCUCurrenttime = MCUDetails.CurrentDateTime;
                                timeZone.userPreferedTime(MCUDetails.timezone, ref MCUCurrenttime);
                                OutXml.Append("<BrigeDateTime>" + MCUCurrenttime + "</BrigeDateTime>");
                            }
                            else
                            {
                                OutXml.Append("<BrigeDateTime></BrigeDateTime>");
                            }

                            OutXml.Append("</Bridge>");
                        }                     
                    }
                    if (uList.Count > 0)
                    {
                        for (int i = 0; i < uList.Count; i++)
                        {
                            OutXml.Append("<Bridge>");
                            OutXml.Append("<BridgeID>" + uList[i].bridgeid.ToString() + "</BridgeID>");
                            vrmMCU MCUDetails = m_vrmMCU.GetById(uList[i].bridgeid);
                            OutXml.Append("<BridgeName>" + MCUDetails.BridgeName + "</BridgeName>");
                            if (MCUDetails.CurrentDateTime != null)
                            {
                                MCUCurrenttime = MCUDetails.CurrentDateTime;
                                timeZone.userPreferedTime(MCUDetails.timezone, ref MCUCurrenttime);
                                OutXml.Append("<BrigeDateTime>" + MCUCurrenttime + "</BrigeDateTime>");
                            }
                            else
                            {
                                OutXml.Append("<BrigeDateTime></BrigeDateTime>");
                            }
                            OutXml.Append("</Bridge>");
                        }    
                    }
                    OutXml.Append("</Bridgelist>");                 
                }

                DateTime currentDatetime = DateTime.Now;
                //timeZone.changeToGMTTime(sysSettings.TimeZone,ref currentDatetime);
                string cfst = "SELECT GETDATE() As DatabaseCurrenttime";
                System.Data.DataSet ds = sqlCon.ExecuteDataSet(cfst);
                if (ds != null)
                {
                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            string DBcurrentDatetime = ds.Tables[0].Rows[0]["DatabaseCurrenttime"].ToString();
                            DateTime.TryParse(DBcurrentDatetime, out databaseservertime);
                        }
                    }
                }
                OutXml.Append("<WebserverDateTime>" + currentDatetime + "</WebserverDateTime>");
                OutXml.Append("<DatabaseServerDateTime>" + databaseservertime + "</DatabaseServerDateTime>");
                OutXml.Append("</GetConfMCUDetails>");
                sqlCon.CommitTransaction();
                sqlCon.CloseConnection();
                sqlCon = null;
                obj.outXml = OutXml.ToString();
                return true;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
        }
        #endregion

        //FB 2448 End

        //FB 2486 Starts
        #region GetConfMsg
        /// <summary>
        /// GetConfMsg
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetConfMsg(ref vrmDataObject obj)
        {

            bool bRet = true;
            try
            {
                List<ICriterion> criterionMsgList = new List<ICriterion>();
                List<vrmMessage> confTxtmsg = new List<vrmMessage>();

                XmlDocument xd = new XmlDocument();
                XmlNode node;
                xd.LoadXml(obj.inXml);

                node = xd.SelectSingleNode("//GetConfMsg/organizationID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out organizationID);

                int LangID =1;
                node = xd.SelectSingleNode("//GetConfMsg/language");
                if (node != null)
                    Int32.TryParse(xd.SelectSingleNode("//GetConfMsg/language").InnerText.Trim(), out LangID);

                vrmLanguage Lang = m_ILanguageDAO.GetLanguageById(LangID);

                node = xd.SelectSingleNode("//GetConfMsg/multisiloOrganizationID");
                string multisiloOrgID = "";
                int mutliOrgID = 0;
                if (node != null)
                    multisiloOrgID = node.InnerXml.Trim();
                Int32.TryParse(multisiloOrgID, out mutliOrgID);
                if (mutliOrgID > 11)
                    organizationID = mutliOrgID;

                if (organizationID < defaultOrgId)
                {
                    myVRMEx = new myVRMException(423);
                    throw myVRMEx;
                }

                criterionMsgList.Add(Expression.Eq("orgId", organizationID));
                criterionMsgList.Add(Expression.Eq("Languageid", Lang.Id));
                confTxtmsg = m_IMessageDao.GetByCriteria(criterionMsgList);

                if (confTxtmsg.Count == 0)
                    return false;

                string tempXML = string.Empty;
                obj.outXml = "<GetConfMsgList>";
                
                for (int i = 0; i < confTxtmsg.Count; i++)
                {

                    tempXML += "<GetConfMsg>";
                    tempXML += "<ConfMsg>"+ confTxtmsg[i].Message.ToString().Trim()+"</ConfMsg>";
                    tempXML += "<ConfMsgID>"+confTxtmsg[i].msgId.ToString()+"</ConfMsgID>";
                    tempXML += "<MsgType>"+ confTxtmsg[i].Type.ToString()+"</MsgType>";
                    tempXML += "</GetConfMsg>";
                }
               
                obj.outXml += tempXML;

                obj.outXml  += "</GetConfMsgList>";

                return true;
            }
            catch (myVRMException e)
            {
                m_log.Error("GetConfMsg", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }

            return bRet;
        }
        #endregion

        #region FetchConfMessages
        /// <summary>
        /// FetchConfMessages
        /// </summary>
        /// <param name="ConfMsg"></param>
        public void FetchConfMessages(vrmConference conf,ref StringBuilder ConfMsg)
        {
            ArrayList selOptIDs = new ArrayList();
            string txtMsgList = "";
            String[] TxtMsgID;
            try
            {
                orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);
                vrmMessage Msg = null;
                int msgID = 0;

                txtMsgList = orgInfo.DefaultTxtMsg;

               
                ConfMsg.Append("<ConfMessageList>");
                String[] TxtMsg = txtMsgList.Split(';');
                if (TxtMsg.Length > 0)
                {
                    for (int i = 0; i < TxtMsg.Length-1; i++)
                    {
                        TxtMsgID = TxtMsg[i].Split('#');

                        Int32.TryParse(TxtMsgID[1].ToString(), out msgID);
                        Msg = m_IMessageDao.GetByMsgId(msgID);

                        ConfMsg.Append("<ConfMessage>");

                        ConfMsg.Append("<controlID>" + TxtMsgID[0].Trim() + "</controlID>");
                        ConfMsg.Append("<textMessage>" + Msg.Message.Trim() + "</textMessage>");
                        ConfMsg.Append("<msgduartion>" + TxtMsgID[2].Trim() + "</msgduartion>");
                        ConfMsg.Append("</ConfMessage>");
                    }
                }
                ConfMsg.Append("</ConfMessageList>");
            }
            catch (Exception ex)
            {
                m_log.Error("FetchConfMessages" + ex.Message);
            }
        }
        #endregion
        //FB 2486 Ends

        //FB 2426 Starts
        # region SetFlyRoomBridge
        /// <summary>
        /// SetFlyRoomBridge
        /// </summary>
        /// <param name="xd"></param>
        private void SetFlyRoomBridge(ref XmlDocument xd)
        {
            try
            {   
                int bridgeID= -1, flyRoomBridge=-1;

                XmlNode node = null;
                XmlNodeList EPNodes=null;

                EPNodes = xd.SelectNodes("//SetAdvancedAVSettings/Endpoints/Endpoint/BridgeID");

                if (EPNodes != null)
                {
                    if (EPNodes.Count > 0)
                    {
                        for (int i=0; i<EPNodes.Count; i++)
                        {
                            bridgeID = -1;
                            Int32.TryParse(EPNodes[i].InnerText, out bridgeID);
                            flyRoomBridge = -1;
                            if (bridgeID > 0)
                            {
                                flyRoomBridge = bridgeID;
                                break;
                            }
                        }
                    }
                }
                if (xd.SelectSingleNode("//SetAdvancedAVSettings/Endpoints/Endpoint") != null)
                {
                    EPNodes = xd.SelectNodes("//SetAdvancedAVSettings/Endpoints/Endpoint");
                    for (int count = 0; count<EPNodes.Count; count++)
                    {
                        node = EPNodes[count].SelectSingleNode("OnFlyRoom");
                        if (node != null)
                        {
                            if (node.InnerText.Trim() == "1")
                                EPNodes[count].SelectSingleNode("BridgeID").InnerText = flyRoomBridge.ToString();
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                m_log.Error("SetFlyRoomBridge" + ex.Message);
            }
        }
        #endregion
        //FB 2426 Ends

        // FB 2501 callMonitor start

        #region LockTerminal
        /// <summary>
        /// LockTerminal 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool LockTerminal(ref vrmDataObject obj)
        {
            bool bRet = true;
            string outXml = string.Empty;
            int userid = 0, confid = 0, instanceid = 0, lockorunlock = 0;
            myVRMException myVRMEx = new myVRMException();
            List<ICriterion> criterionList = new List<ICriterion>();
            String confids = "";
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node = null;
                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out userid);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                if (userid <= 0)
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                node = xd.SelectSingleNode("//login/organizationID");
                string orgid = "";
                if (node != null)
                    orgid = node.InnerText.Trim();
                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);
                if (organizationID < defaultOrgId)
                {
                    myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                node = xd.SelectSingleNode("//login/confID");
                if (node != null)
                    confids = node.InnerText.Trim();
                if (confids.Length <= 0)
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                CConfID ConfMode = new CConfID(confids);
                confid = ConfMode.ID;
                instanceid = ConfMode.instance;
                if (instanceid == 0)
                    instanceid = 1;

                node = xd.SelectSingleNode("//login/lockorunlock");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out lockorunlock);

                criterionList.Add(Expression.Eq("confid", confid));
                if (instanceid > 0)
                    criterionList.Add(Expression.Eq("instanceid", instanceid));

                List<vrmConfAdvAvParams> confAdv = new List<vrmConfAdvAvParams>();

                confAdv = m_confAdvAvParamsDAO.GetByCriteria(criterionList);
                for (int i = 0; i < confAdv.Count; i++)
                    confAdv[i].ConfLockUnlock = lockorunlock;
                m_confAdvAvParamsDAO.SaveOrUpdateList(confAdv);

                outXml = "<success>1</success>";
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

        #region MonitorMuteTerminal
        /// <summary>
        /// MonitorMuteTerminal 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool MonitorMuteTerminal(ref vrmDataObject obj)
        {
            bool bRet = true;
            string outXml = string.Empty;
            int userid = 0, confid = 0, instanceid = 0;
            string MuteIdentificationId = "";
            myVRMException myVRMEx = new myVRMException();
            List<ICriterion> criterionList = new List<ICriterion>();
            String confids = "", terminalType = "";
            int mutePartyALL = 1; 
            bool updateAll = false;
            bool roomUpdate = false;
            bool userUpdate = false;
            bool cascadeUpdate = false;

            vrmConfRoom nRoom = null;
            vrmConfCascade nCascade = null;
            vrmConfUser nUser = null;
            vrmConfAdvAvParams nAdvAvParam = null;

            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node = null;
                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out userid);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                if (userid <= 0)
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                node = xd.SelectSingleNode("//login/organizationID");
                string orgid = "";
                if (node != null)
                    orgid = node.InnerText.Trim();
                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);
                if (organizationID < defaultOrgId)
                {
                    myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                node = xd.SelectSingleNode("//login/confID");
                if (node != null)
                    confids = node.InnerText.Trim();
                if (confids.Length <= 0)
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                CConfID ConfMode = new CConfID(confids);
                confid = ConfMode.ID;
                instanceid = ConfMode.instance;
                if (instanceid == 0)
                    instanceid = 1;

                int endpointID = 0, mute = 0;
                node = xd.SelectSingleNode("//login/endpointID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out endpointID);
                
                node = xd.SelectSingleNode("//login/terminalType");
                if (node != null)
                    terminalType = node.InnerText.Trim();
                if (terminalType == "0")
                    updateAll = true;
                else if (terminalType == "2")
                {
                    roomUpdate = true;
                    criterionList.Add(Expression.Eq("roomId", endpointID));
                }
                else if (terminalType == "4")
                {
                    cascadeUpdate = true;
                    criterionList.Add(Expression.Eq("uId", endpointID));
                }
                else if (terminalType == "3")
                {
                    userUpdate = true;
                    criterionList.Add(Expression.Eq("userid", endpointID));
                }
                node = xd.SelectSingleNode("//login/mute");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out mute);

                node = xd.SelectSingleNode("//login/muteAll");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out mutePartyALL);

                node = xd.SelectSingleNode("//login/MuteIdentificationId");
                if (node != null)
                    MuteIdentificationId = node.InnerText.Trim();

                criterionList.Add(Expression.Eq("confid", confid));
                if (instanceid > 0)
                    criterionList.Add(Expression.Eq("instanceid", instanceid));


                List<vrmConfUser> confuser = new List<vrmConfUser>();
                List<vrmConfRoom> confroom = new List<vrmConfRoom>();
                List<vrmConfCascade> confCascade = new List<vrmConfCascade>();
                List<vrmConfAdvAvParams> confAdv = new List<vrmConfAdvAvParams>();

                if (updateAll == true)
                {
                    confAdv = m_confAdvAvParamsDAO.GetByCriteria(criterionList);
                    for (int i = 0; i < confAdv.Count; i++)
                    {
                        nAdvAvParam = confAdv[i];
                        methodMute(ref nRoom, ref nUser, ref nCascade,ref nAdvAvParam, MuteIdentificationId, mute);
                        m_confAdvAvParamsDAO.SaveOrUpdate(nAdvAvParam);
                    }
                }
                if (updateAll == true || roomUpdate == true)
                {
                    confroom = m_IconfRoom.GetByCriteria(criterionList);
                    nCascade = null;
                    nUser = null;
                    for (int i = 0; i < confroom.Count; i++)
                    {
                        nRoom = confroom[i];
                        methodMute(ref nRoom, ref nUser, ref nCascade,ref nAdvAvParam, MuteIdentificationId, mute);
                        m_IconfRoom.SaveOrUpdate(nRoom);
                    }   
                }

                if (updateAll == true || cascadeUpdate == true)
                {
                    confCascade = m_IconfCascade.GetByCriteria(criterionList);
                    nRoom = null;
                    nUser = null;
                    for (int i = 0; i < confCascade.Count; i++)
                    {
                        nCascade = confCascade[i];
                        methodMute(ref nRoom, ref nUser, ref nCascade,ref nAdvAvParam, MuteIdentificationId, mute);
                        m_IconfCascade.SaveOrUpdate(nCascade);
                    }
                }

                if (updateAll == true || userUpdate == true)
                {
                    confuser = m_IconfUser.GetByCriteria(criterionList);
                    nRoom = null;
                    nCascade = null;
                    for (int i = 0; i < confuser.Count; i++)
                    {
                        nUser = confuser[i];
                        methodMute(ref nRoom, ref nUser, ref nCascade, ref nAdvAvParam, MuteIdentificationId, mute);
                        m_IconfUser.SaveOrUpdate(nUser);
                    }
                }

                outXml = "<success>1</success>";
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

        #region methodMute
        private void methodMute(ref vrmConfRoom room, ref  vrmConfUser user, ref  vrmConfCascade cascade, ref vrmConfAdvAvParams AdvAvParam, string MuteIdentificationId, int mute)
        {
            if (MuteIdentificationId == "audioTx")
            {
                if (room != null)
                    room.mute = mute;
                else if (user != null)
                    user.mute = mute;
                else if (cascade != null)
                    cascade.mute = mute;
                else if (AdvAvParam != null)
                    AdvAvParam.MuteTxaudio = mute;
            }
            else if (MuteIdentificationId == "audioRx")
            {
                if (room != null)
                    room.MuteRxaudio = mute;
                else if (user != null)
                    user.MuteRxaudio = mute;
                else if (cascade != null)
                    cascade.mute = mute;
                else if (AdvAvParam != null)
                    AdvAvParam.MuteRxaudio = mute;
            }
            else if (MuteIdentificationId == "videoTx")
            {
                if (room != null)
                    room.MuteTxvideo = mute;
                else if (user != null)
                    user.MuteTxvideo = mute;
                else if (cascade != null)
                    cascade.mute = mute;
                else if (AdvAvParam != null)
                    AdvAvParam.MuteTxvideo = mute;
            }
            else if (MuteIdentificationId == "videoRx")
            {
                if (room != null)
                    room.MuteRxvideo = mute;
                else if (user != null)
                    user.MuteRxvideo = mute;
                else if (cascade != null)
                    cascade.mute = mute;
                else if (AdvAvParam != null)
                    AdvAvParam.MuteRxvideo = mute;
            }

        }
        #endregion

        #region PacketLossTerminal
        /// <summary>
        /// PacketLossTerminal 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetPacketLossTerminal(ref vrmDataObject obj)
        {
            bool bRet = true;
            StringBuilder outXml = new StringBuilder();
            int userid = 0, confid = 0, instanceid = 0;
            myVRMException myVRMEx = new myVRMException();
            List<ICriterion> criterionList = new List<ICriterion>();
            String confids = "", terminalType = "";
            bool roomUpdate = false;
            bool userUpdate = false;
            bool cascadeUpdate = false;
            String AudioPacketReceived = "", AudioPacketError = "", AudioPacketMissing = "", VideoPacketError = "", VideoPacketReceived = "", VideoPacketMissing = "";
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node = null;
                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out userid);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                if (userid <= 0)
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                node = xd.SelectSingleNode("//login/organizationID");
                string orgid = "";
                if (node != null)
                    orgid = node.InnerText.Trim();
                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);
                if (organizationID < defaultOrgId)
                {
                    myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                node = xd.SelectSingleNode("//login/confID");
                if (node != null)
                    confids = node.InnerText.Trim();
                if (confids.Length <= 0)
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                CConfID ConfMode = new CConfID(confids);
                confid = ConfMode.ID;
                instanceid = ConfMode.instance;
                if (instanceid == 0)
                    instanceid = 1;

                int endpointID = 0;
                node = xd.SelectSingleNode("//login/endpointID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out endpointID);
                node = xd.SelectSingleNode("//login/terminalType");
                if (node != null)
                    terminalType = node.InnerText.Trim();

                if (terminalType == "2")
                    roomUpdate = true;
                else if (terminalType == "4")
                    cascadeUpdate = true;
                else if (terminalType == "3")
                    userUpdate = true;

                criterionList.Add(Expression.Eq("confid", confid));
                if (instanceid > 0)
                    criterionList.Add(Expression.Eq("instanceid", instanceid));


                List<vrmConfUser> confuser = new List<vrmConfUser>();
                List<vrmConfRoom> confroom = new List<vrmConfRoom>();
                List<vrmConfCascade> confCascade = new List<vrmConfCascade>();


                if (roomUpdate == true)
                {
                    criterionList.Add(Expression.Eq("roomId", endpointID));
                    confroom = m_IconfRoom.GetByCriteria(criterionList, true);
                    for (int i = 0; i < confroom.Count; i++)
                    {
                        AudioPacketReceived = confroom[i].RxAudioPacketsReceived;
                        AudioPacketError = confroom[i].RxAudioPacketErrors;
                        AudioPacketMissing = confroom[i].RxAudioPacketsMissing;
                        VideoPacketError = confroom[i].RxVideoPacketErrors;
                        VideoPacketReceived = confroom[i].RxVideoPacketsReceived;
                        VideoPacketMissing = confroom[i].RxVideoPacketsMissing;
                    }
                }

                if (cascadeUpdate == true)
                {
                    criterionList.Add(Expression.Eq("uId", endpointID));
                    confCascade = m_IconfCascade.GetByCriteria(criterionList, true);
                    for (int i = 0; i < confCascade.Count; i++)
                    {
                        AudioPacketReceived = confCascade[i].RxAudioPacketsReceived;
                        AudioPacketError = confCascade[i].RxAudioPacketErrors;
                        AudioPacketMissing = confCascade[i].RxAudioPacketsMissing;
                        VideoPacketError = confCascade[i].RxVideoPacketErrors;
                        VideoPacketReceived = confCascade[i].RxVideoPacketsReceived;
                        VideoPacketMissing = confCascade[i].RxVideoPacketsMissing;
                    }
                }

                if (userUpdate == true)
                {
                    criterionList.Add(Expression.Eq("userid", endpointID));
                    confuser = m_IconfUser.GetByCriteria(criterionList, true);
                    for (int i = 0; i < confuser.Count; i++)
                    {
                        AudioPacketReceived = confuser[i].RxAudioPacketsReceived;
                        AudioPacketError = confuser[i].RxAudioPacketErrors;
                        AudioPacketMissing = confuser[i].RxAudioPacketsMissing;
                        VideoPacketError = confuser[i].RxVideoPacketErrors;
                        VideoPacketReceived = confuser[i].RxVideoPacketsReceived;
                        VideoPacketMissing = confuser[i].RxVideoPacketsMissing;
                    }
                }
                outXml.Append("<PacketLossTerminal>");
                outXml.Append("<ConfID>" + confid + "</ConfID>");
                outXml.Append("<instanceid>" + instanceid + "</instanceid>");
                outXml.Append("<endpointID>" + endpointID + "</endpointID>");
                outXml.Append("<terminalType>" + terminalType + "</terminalType>");
                outXml.Append("<AudioPacketReceived>" + AudioPacketReceived + "</AudioPacketReceived>");
                outXml.Append("<AudioPacketError>" + AudioPacketError + "</AudioPacketError>");
                outXml.Append("<AudioPacketMissing>" + AudioPacketMissing + "</AudioPacketMissing>");
                outXml.Append("<VideoPacketError>" + VideoPacketError + "</VideoPacketError>");
                outXml.Append("<VideoPacketReceived>" + VideoPacketReceived + "</VideoPacketReceived>");
                outXml.Append("<VideoPacketMissing>" + VideoPacketMissing + "</VideoPacketMissing>");
                outXml.Append("</PacketLossTerminal>");
                obj.outXml = outXml.ToString();
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

        // FB 2501 callMonitor Ends

        // FB 2501 p2p callMonitor start

        #region SetP2PCallerEndpoint
        /// <summary>
        /// SetP2PCallerEndpoint 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SetP2PCallerEndpoint(ref vrmDataObject obj)
        {
            bool bRet = true;
            string outXml = string.Empty;
            int userid = 0, confid = 0, instanceid = 0, endpointID = 0, terminalType = 0, NewendpointID = 0, NewterminalType = 0, errNo = 0;
            myVRMException myVRMEx = new myVRMException();
            List<ICriterion> criterionList = new List<ICriterion>();
            List<ICriterion> eptcriterionList = new List<ICriterion>();
            String confids = "";
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node = null;
                node = xd.SelectSingleNode("//SetP2PCallerEndpoint/UserID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out userid);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                if (userid <= 0)
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                node = xd.SelectSingleNode("//SetP2PCallerEndpoint/organizationID");
                string orgid = "";
                if (node != null)
                    orgid = node.InnerText.Trim();
                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);
                if (organizationID < defaultOrgId)
                {
                    myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                node = xd.SelectSingleNode("//SetP2PCallerEndpoint/ConfID");
                if (node != null)
                    confids = node.InnerText.Trim();
                if (confids.Length <= 0)
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                CConfID ConfMode = new CConfID(confids);
                confid = ConfMode.ID;
                instanceid = ConfMode.instance;
                if (instanceid == 0)
                    instanceid = 1;

                node = xd.SelectSingleNode("//SetP2PCallerEndpoint/OldEndPoints/endpointID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out endpointID);

                node = xd.SelectSingleNode("//SetP2PCallerEndpoint/OldEndPoints/terminalType");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out terminalType);

                node = xd.SelectSingleNode("//SetP2PCallerEndpoint/NewEndPoints/RoomID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out NewendpointID);

                node = xd.SelectSingleNode("//SetP2PCallerEndpoint/NewEndPoints/TerminalType");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out NewterminalType);

                List<vrmConfUser> ConfUser = new List<vrmConfUser>();
                List<vrmConfRoom> ConfRoom = new List<vrmConfRoom>();
                List<vrmConference> Conference = new List<vrmConference>();
                List<vrmEndPoint> eptlist = new List<vrmEndPoint>();
                vrmConfRoom vrmConfRoom = null;
                vrmConference vConf = null;

                criterionList.Add(Expression.Eq("confid", confid));
                if (instanceid > 0)
                    criterionList.Add(Expression.Eq("instanceid", instanceid));

                #region Load Conference

                Conference = m_vrmConfDAO.GetByCriteria(criterionList, true);
                for (int i = 0; i < Conference.Count; i++)
                {
                    vConf = Conference[i];
                }

                #endregion

                #region Delete Endpoint

                criterionList.Add(Expression.Eq("connect2", 1));

                if (terminalType == vrmConfTerminalType.Room)
                {
                    criterionList.Add(Expression.Eq("roomId", endpointID));
                    ConfRoom = m_IconfRoom.GetByCriteria(criterionList, true);
                    for (int i = 0; i < ConfRoom.Count; i++)
                    {
                        m_IconfRoom.Delete(ConfRoom[i]);
                    }
                }
                else
                {
                    criterionList.Add(Expression.Eq("userid", endpointID));
                    ConfUser = m_IconfUser.GetByCriteria(criterionList, true);
                    for (int i = 0; i < ConfUser.Count; i++)
                    {
                        m_IconfUser.Delete(ConfUser[i]);
                    }
                }

                #endregion

                #region Insert Room

                if (NewendpointID > 0)
                {
                    if (ConfRoom != null)
                        ConfRoom = new List<vrmConfRoom>();

                    vrmConfRoom = new vrmConfRoom();

                    vrmConfRoom.roomId = NewendpointID;
                    if (vrmConfRoom.roomId > 0)
                    {
                        vrmConfRoom.Room = m_vrmRoomDAO.GetByRoomId(vrmConfRoom.roomId);
                        ConfRoom.Add(vrmConfRoom);

                        if (vrmConfRoom.Room.endpointid <= 0)
                        {
                            myVRMEx = new myVRMException(262);
                            obj.outXml = myVRMEx.FetchErrorMsg();
                            return false;
                        }
                    }
                }
                
                for (int ccnt = 0; ccnt < ConfRoom.Count; ccnt++)
                {
                    vrmConfRoom = null;
                    vrmConfRoom = ConfRoom[ccnt];

                    vrmConfRoom.confid = vConf.confid;
                    vrmConfRoom.instanceid = vConf.instanceid;
                    vrmConfRoom.confuId = vConf.confnumname;
                    vrmConfRoom.StartDate = vConf.confdate;
                    vrmConfRoom.Duration = vConf.duration;
                    vrmConfRoom.disabled = 0;
                    vrmConfRoom.LastRunDateTime = DateTime.UtcNow;
                    vrmConfRoom.TerminalType = vrmConfTerminalType.Room;

                    if (vrmConfRoom.Room.endpointid > 0)
                    {
                        eptcriterionList.Add(Expression.Eq("endpointid", vrmConfRoom.Room.endpointid));
                        eptcriterionList.Add(Expression.Eq("isDefault", 1));
                        eptlist = m_vrmEpt.GetByCriteria(eptcriterionList, true);

                        for (int j = 0; j < eptlist.Count; j++)
                        {
                            vrmEndPoint ept = null;
                            ept = eptlist[j];
                            vrmConfRoom.endpointId = vrmConfRoom.Room.endpointid;
                            vrmConfRoom.profileId = ept.profileId;
                            vrmConfRoom.bridgeIPISDNAddress = ept.MCUAddress;
                            vrmConfRoom.ipisdnaddress = ept.address;
                            vrmConfRoom.bridgeid = ept.bridgeid;
                            vrmConfRoom.bridgeAddressType = ept.MCUAddressType;
                            vrmConfRoom.audioorvideo = vrmConfRoom.Room.VideoAvailable;
                            vrmConfRoom.addressType = ept.addresstype;
                            vrmConfRoom.ApiPortNo = ept.ApiPortNo;
                            vrmConfRoom.connectionType = ept.connectiontype;
                            vrmConfRoom.defLineRate = ept.linerateid;
                            vrmConfRoom.connect2 = 1;
                            vrmConfRoom.MultiCodecAddress = "";
                            vrmConfRoom.BridgeExtNo = ""; //FB 2610

                            if (ept.isTelePresence == 1)
                                vrmConfRoom.MultiCodecAddress = ept.MultiCodecAddress;

                            if (ept.bridgeid > 0)
                            {
                                vrmMCU MCU = m_vrmMCU.GetById(ept.bridgeid);
                                vrmConfRoom.bridgeIPISDNAddress = MCU.BridgeAddress;
                            }
                        }
                    }

                    m_IconfRoom.Save(vrmConfRoom);
                }
                #endregion

                outXml = "<success>1</success>";
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

        #region SetP2PCallerLinerate
        /// <summary>
        /// SetP2PCallerEndpoint 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SetP2PCallerLinerate(ref vrmDataObject obj)
        {
            bool bRet = true;
            string outXml = string.Empty;
            int userid = 0, confid = 0, instanceid = 0, endpointID = 0, terminalType = 0, Linerate = 0;
            myVRMException myVRMEx = new myVRMException();
            List<ICriterion> criterionList = new List<ICriterion>();
            List<ICriterion> eptcriterionList = new List<ICriterion>();
            String confids = "";
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node = null;
                node = xd.SelectSingleNode("//SetP2PCallerLinerate/UserID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out userid);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                if (userid <= 0)
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                node = xd.SelectSingleNode("//SetP2PCallerLinerate/organizationID");
                string orgid = "";
                if (node != null)
                    orgid = node.InnerText.Trim();
                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);
                if (organizationID < defaultOrgId)
                {
                    myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                node = xd.SelectSingleNode("//SetP2PCallerLinerate/ConfID");
                if (node != null)
                    confids = node.InnerText.Trim();
                if (confids.Length <= 0)
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                CConfID ConfMode = new CConfID(confids);
                confid = ConfMode.ID;
                instanceid = ConfMode.instance;
                if (instanceid == 0)
                    instanceid = 1;

                node = xd.SelectSingleNode("//SetP2PCallerLinerate/endpointID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out endpointID);

                node = xd.SelectSingleNode("//SetP2PCallerLinerate/terminalType");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out terminalType);

                node = xd.SelectSingleNode("//SetP2PCallerLinerate/Linerate");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out Linerate);

                List<vrmConfUser> ConfUser = new List<vrmConfUser>();
                List<vrmConfRoom> ConfRoom = new List<vrmConfRoom>();
               
                criterionList.Add(Expression.Eq("confid", confid));
                if (instanceid > 0)
                    criterionList.Add(Expression.Eq("instanceid", instanceid));

                criterionList.Add(Expression.Eq("connect2", 1));

                if (terminalType == vrmConfTerminalType.Room)
                {
                    criterionList.Add(Expression.Eq("roomId", endpointID));
                    ConfRoom = m_IconfRoom.GetByCriteria(criterionList, true);
                    for (int i = 0; i < ConfRoom.Count; i++)
                    {
                        ConfRoom[i].defLineRate = Linerate;
                        m_IconfRoom.SaveOrUpdate(ConfRoom[i]);
                    }
                }
                else
                {
                    criterionList.Add(Expression.Eq("userid", endpointID));
                    ConfUser = m_IconfUser.GetByCriteria(criterionList, true);
                    for (int i = 0; i < ConfUser.Count; i++)
                    {
                        ConfUser[i].defLineRate = Linerate;
                        m_IconfUser.SaveOrUpdate(ConfUser[i]);
                    }
                }

                outXml = "<success>1</success>";
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

        // FB 2501 p2p callMonitor End

        //FB 2566
        #region SetConferenceBridges
        /// <summary>
        /// SetConfBridges
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="bridgeid"></param>
        protected void SetConfBridges(List<vrmConference> conf, List<int> bridgeid, List<int> ConfE164Bridgeid)
        {
            List<vrmConfBridge> vConfbridgeList = new List<vrmConfBridge>();
            List<ICriterion> CriterionMCUList = new List<ICriterion>();
            List<ICriterion> CriteriaMCU = new List<ICriterion>();
            vrmConfBridge vConfbridge = null;
            vrmMCU MCUListID = null;
            int e164number = 0,confBridge=0;
            try
            {
                for (int i = 0; i < conf.Count; i++)
                {
                    CriteriaMCU = new List<ICriterion>();
                    CriteriaMCU.Add(Expression.In("BridgeID", bridgeid));
                    List<vrmMCU> bridges = m_vrmMCU.GetByCriteria(CriteriaMCU, true);
                    
                    for (int k = 0; k < bridges.Count; k++)
                    {
                        e164number = 0;
                        vConfbridge = new vrmConfBridge();
                        MCUListID = new vrmMCU();
                        MCUListID = bridges[k];
                        CriterionMCUList = new List<ICriterion>();
                        CriterionMCUList.Add(Expression.Eq("confid", conf[i].confid));
                        CriterionMCUList.Add(Expression.Eq("instanceid", conf[i].instanceid));
                        CriterionMCUList.Add(Expression.Eq("bridgeid", MCUListID.BridgeID));
                        List<vrmConfRoom> ConfROOM = m_IconfRoom.GetByCriteria(CriterionMCUList, true);
                        CriterionMCUList.Add(Expression.Eq("invitee", 1));
                        List<vrmConfUser> ConFUser = m_IconfUser.GetByCriteria(CriterionMCUList, true);
                    
                        vConfbridge.ConfID = conf[i].confid;
                        vConfbridge.InstanceID = conf[i].instanceid;
                        vConfbridge.confuId = conf[i].confnumname;
                        vConfbridge.BridgeID = MCUListID.BridgeID;
                        vConfbridge.BridgeName = MCUListID.BridgeName;
                        vConfbridge.BridgeTypeid = MCUListID.MCUType.bridgeInterfaceId;
                        vConfbridge.BridgeIPISDNAddress = MCUListID.BridgeAddress;
                        vConfbridge.TotalPortsUsed = ConfROOM.Count + ConFUser.Count;
                        vConfbridge.BridgeExtNo = MCUListID.BridgeExtNo; //FB 2610
                        
                        confBridge = MCUListID.BridgeID;
                        if ((MCUListID.E164Dialing == 1 || MCUListID.H323Dialing == 1) && orgInfo.EnableDialPlan == 1)
                        {
                            GetSIPService(ref confBridge, conf[i], ref e164number);

                        }
                        vConfbridge.E164Dialnumber = e164number;
                        vConfbridge.Synchronous = MCUListID.Synchronous; // FB 2441
                        m_IconfBridge.Save(vConfbridge);
                    }
                    
                }
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
            }
        }
        #endregion


        //FB 2609
        #region CheckForMeetandGreetBuffer
        /// <summary>
        /// CheckForMeetandGreetBuffer 
        /// </summary>
        /// <param name="confRooms"></param>
        /// <param name="confSplitRooms"></param>
        /// <param name="conflictRooms"></param>
        /// <param name="vConf"></param>
        /// <returns></returns>
        private bool CheckForMeetandGreetBuffer(DateTime cfdatetime, int timezoneid)
        {
            double MeetandGreetSchedulehours = 0;
            
            try
            {
                DateTime currenttime = DateTime.Now;
                timeZone.changeToGMTTime(sysSettings.TimeZone, ref currenttime);
                TimeSpan span = cfdatetime.Subtract(currenttime);
                MeetandGreetSchedulehours = span.TotalHours;
                if (!(MeetandGreetSchedulehours >= MeetandGreetBuffer))
                    return true;
                else
                    return false;
             }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion


	 //FB 2392 Starts
        #region GetConfAvailableRoom
        /// <summary>
        /// GetConfAvailableRoom - Command for Outlook Plugin for returning  Available Rooms (both Private and Public Rooms)with paging -Similar to GetAvailableRoom
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetConfAvailableRoom(ref vrmDataObject obj)
        {
            StringBuilder outXml = new StringBuilder();
            Int32 multiDepts = 0;
            Int32 confid = 0;
            Int32 instanceid = 0;
            Int32 confType = 0;
            Int32 iPublic = 0;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;
                node = xd.SelectSingleNode("//GetConfAvailableRoom/userID");
                Int32 userid = Int32.Parse(node.InnerXml.Trim());

                node = xd.SelectSingleNode("//GetConfAvailableRoom/confID");
                String cid = node.InnerXml.Trim();

                if (cid.ToLower() == "new")
                {
                    confid = 0;
                    instanceid = 0;
                }
                else
                {
                    CConfID cconf = new CConfID(cid);
                    confid = cconf.ID;
                    instanceid = cconf.instance;
                }

                node = xd.SelectSingleNode("//GetConfAvailableRoom/organizationID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out organizationID);

                node = xd.SelectSingleNode("//GetConfAvailableRoom/multisiloOrganizationID");
                string multisiloOrgID = "";
                int mutliOrgID = 0;
                if (node != null)
                    multisiloOrgID = node.InnerXml.Trim();
                Int32.TryParse(multisiloOrgID, out mutliOrgID);
                if (mutliOrgID > 11)
                    organizationID = mutliOrgID;

                if (organizationID < defaultOrgId)
                {
                    myVRMEx = new myVRMException(423);
                    throw myVRMEx;
                }
                node = xd.SelectSingleNode("//GetConfAvailableRoom/confType");
                if (node != null)
                    if (!Int32.TryParse(node.InnerText.Trim(), out confType))
                        confType = 0;
                node = xd.SelectSingleNode("//GetConfAvailableRoom/startDate");
                String confdate = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//GetConfAvailableRoom/startHour");
                String startTime = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//GetConfAvailableRoom/startMin");
                startTime += ":" + node.InnerXml.Trim();

                node = xd.SelectSingleNode("//GetConfAvailableRoom/startSet");
                startTime += ":45 " + node.InnerXml.Trim();

                node = xd.SelectSingleNode("//GetConfAvailableRoom/timeZone");
                Int32 tzone = 0;
                if (node != null)
                    tzone = (node.InnerXml.Trim().Length > 0) ? Int32.Parse(node.InnerXml.Trim()) : sysSettings.TimeZone;

                node = xd.SelectSingleNode("//GetConfAvailableRoom/dirationMin");//Spell Mistake in OutLook //FB 2558 WhyGo
                String duration = "00";
                if (node != null)
                    duration = (node.InnerXml.Trim() == "") ? "00" : node.InnerXml.Trim();

                node = xd.SelectSingleNode("//GetConfAvailableRoom/immediate");
                String immediate = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//GetConfAvailableRoom/recurring");
                String recurring = node.InnerXml.Trim();

                //FB 2089 start
                //Media type => none:0,audio:1,video:2, all types:-1
                int mediaType = -1;
                node = xd.SelectSingleNode("//GetConfAvailableRoom/mediaType");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out mediaType);
                //FB 2089 end

                int serviceType = -1;
                node = xd.SelectSingleNode("//GetConfAvailableRoom/ServiceType");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out serviceType);

                node = xd.SelectSingleNode("//GetConfAvailableRoom/Public");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out iPublic);
                //FB 2558 WhyGo - Starts
				int isPublic = 0;
                if (iPublic > 0)
                    isPublic = 1;
                else
                    isPublic = 0;
				//FB 2558 WhyGo - End
                DateTime confdatetime = Convert.ToDateTime(confdate + " " + startTime);

                string alphabet = "";
                int pageNo = 0, sortBy = 3;

                node = xd.SelectSingleNode("//GetConfAvailableRoom/searchBy");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim().ToString(),out sortBy);

                node = xd.SelectSingleNode("//GetConfAvailableRoom/searchFor");
                if (node != null)
                   alphabet = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//GetConfAvailableRoom/pageNo");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out pageNo);
				//FB 2558 WhyGo - Starts
                int MaxRecords = 20;
                node = xd.SelectSingleNode("//GetConfAvailableRoom/MaxRecords");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim().ToString(), out MaxRecords);
				//FB 2558 WhyGo - End

                node = xd.SelectSingleNode("//GetConfAvailableRoom/selected");
                String selectedloc = "";
                ArrayList selrooms = new ArrayList();
                if (node != null)
                {
                    selectedloc = node.InnerXml.Trim();
                    selrooms = new ArrayList(selectedloc.Split(','));
                }

                //FB 2532 Starts
                int outXMLType = 1;
                // 1- Returns all data; 2 - returns Room ID and timestamp
                node = xd.SelectSingleNode("//GetConfAvailableRoom/outXMLType");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out outXMLType);
                if (outXMLType == 0)
                    outXMLType = 1; // Default Value 
                //FB 2532 Ends

                //timeZone.changeToGMTTime(tzone, ref confdatetime);

                Boolean bHasDepartment = false;

                List<vrmUserDepartment> deptList = new List<vrmUserDepartment>();
                List<ICriterion> userCriterionList = new List<ICriterion>();
                userCriterionList.Add(Expression.Eq("userId", userid));

                deptList = m_IuserDeptDAO.GetByCriteria(userCriterionList);

                if (deptList.Count > 0)
                    bHasDepartment = true;

                ArrayList roomList = new ArrayList();
                m_RoomFactory.PublicRoom = iPublic;
				//FB 2558 WhyGo - Starts                
                //m_RoomFactory.GetRoomArray(userid, organizationID, ref roomList, bHasDepartment, ref rmStr, mediaType, serviceType, selrooms);
                //String roomConflicts = CheckConfRoom(confid, instanceid, immediate, rmStr, confdatetime.ToString(), tzone.ToString(), duration);

                String roomConflicts = CheckConfRoomforOutLook(confid, instanceid, immediate, confdatetime.ToString(), tzone.ToString(), duration,
                    userid, organizationID, bHasDepartment, mediaType, serviceType, selrooms, isPublic);

                if (roomConflicts.Length != 0)
                {
                    obj.outXml = roomConflicts;
                }
				//FB 2558 WhyGo - End
                String stmt = "";
                ArrayList RoomNames = new ArrayList();
                if (roomConflicts != "")
                {
                    stmt = roomConflicts;
                    RoomNames = new ArrayList(roomConflicts.Split(','));
                }

                m_RoomFactory.FetchAvailableRooms(outXMLType, organizationID, 1, stmt, userid, bHasDepartment, ref outXml, ref confType, mediaType, serviceType, selectedloc, sortBy, alphabet, pageNo, MaxRecords); //FB 2532 //FB 2558 WhyGo

                obj.outXml = outXml.ToString();

                return true;
            }
            catch (myVRMException myVRMEx)
            {
                m_log.Error("myVRMException GetConfAvailableRoom :" + myVRMEx.Message);
                obj.outXml = myVRMEx.FetchErrorMsg();
                return false;
            }
            catch (Exception ex)
            {
                m_log.Error("GetConfAvailableRoom :" + ex.Message);
                obj.outXml = "";
                return false;
            }
        }

        #endregion

        //FB 2392 Ends

        //FB 2441 Starts
        #region ConferenceRecording
        /// <summary>
        /// LockTerminal 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool ConferenceRecording(ref vrmDataObject obj)
        {
            bool bRet = true;
            string outXml = string.Empty;
            int userid = 0, confid = 0, instanceid = 0, Record = 0;
            myVRMException myVRMEx = new myVRMException();
            List<ICriterion> criterionList = new List<ICriterion>();
            String confids = "";
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node = null;
                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out userid);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                if (userid <= 0)
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                node = xd.SelectSingleNode("//login/organizationID");
                string orgid = "";
                if (node != null)
                    orgid = node.InnerText.Trim();
                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);
                if (organizationID < defaultOrgId)
                {
                    myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                node = xd.SelectSingleNode("//login/confID");
                if (node != null)
                    confids = node.InnerText.Trim();
                if (confids.Length <= 0)
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                CConfID ConfMode = new CConfID(confids);
                confid = ConfMode.ID;
                instanceid = ConfMode.instance;
                if (instanceid == 0)
                    instanceid = 1;

                node = xd.SelectSingleNode("//login/confrecord");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out Record);

                criterionList.Add(Expression.Eq("confid", confid));
                if (instanceid > 0)
                    criterionList.Add(Expression.Eq("instanceid", instanceid));

                List<vrmConfAdvAvParams> confAdv = new List<vrmConfAdvAvParams>();

                confAdv = m_confAdvAvParamsDAO.GetByCriteria(criterionList);
                for (int i = 0; i < confAdv.Count; i++)
                    confAdv[i].Recording = Record;
                m_confAdvAvParamsDAO.SaveOrUpdateList(confAdv);

                outXml = "<success>1</success>";
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion
        //FB 2441 Ends

        #region MuteUnMuteParticipants
        /// <summary>
        /// MuteUnMuteParticipants
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool MuteUnMuteParticipants(ref vrmDataObject obj)
        {
            bool bRet = true;
            string outXml = string.Empty;
            int userid = 0, confid = 0, instanceid = 0;
            int endpointID = 0, mute = 0, terminalType = 0;
            myVRMException myVRMEx = new myVRMException();
            List<ICriterion> criterionList = new List<ICriterion>();
            XmlNodeList nodes = null;
            String confids = "";
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node = null;
                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out userid);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                if (userid <= 0)
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                node = xd.SelectSingleNode("//login/organizationID");
                string orgid = "";
                if (node != null)
                    orgid = node.InnerText.Trim();
                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);
                if (organizationID < defaultOrgId)
                {
                    myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                node = xd.SelectSingleNode("//login/confID");
                if (node != null)
                    confids = node.InnerText.Trim();
                if (confids.Length <= 0)
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                CConfID ConfMode = new CConfID(confids);
                confid = ConfMode.ID;
                instanceid = ConfMode.instance;
                if (instanceid == 0)
                    instanceid = 1;

                node = xd.SelectSingleNode("//login/mode");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out mute); //0-UnMuteAllParties 1-MuteAllPartiesExcept

                criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("confid", confid));
                if (instanceid > 0)
                    criterionList.Add(Expression.Eq("instanceid", instanceid));

                List<vrmConfRoom> confroom = m_IconfRoom.GetByCriteria(criterionList);
                for (int i = 0; i < confroom.Count; i++)
                    confroom[i].mute = mute;
                if (confroom.Count > 0)
                    m_IconfRoom.SaveOrUpdateList(confroom);

                List<vrmConfCascade> confCascade = m_IconfCascade.GetByCriteria(criterionList);
                for (int i = 0; i < confCascade.Count; i++)
                    confCascade[i].mute = mute;
                if (confCascade.Count > 0)
                    m_IconfCascade.SaveOrUpdateList(confCascade);

                List<vrmConfUser> confuser = m_IconfUser.GetByCriteria(criterionList);
                for (int i = 0; i < confuser.Count; i++)
                    confuser[i].mute = mute;
                if (confuser.Count > 0)
                    m_IconfUser.SaveOrUpdateList(confuser);

                nodes = xd.SelectNodes("//login/endpoints/endpoint");
                if (nodes != null && nodes.Count > 0)
                {
                    mute = 0;
                    for (int j = 0; j < nodes.Count; j++)
                    {
                        criterionList = new List<ICriterion>();

                        criterionList.Add(Expression.Eq("confid", confid));
                        if (instanceid > 0)
                            criterionList.Add(Expression.Eq("instanceid", instanceid));

                        node = nodes[j].SelectSingleNode("endpointId");
                        if (node != null)
                            Int32.TryParse(node.InnerText.Trim(), out endpointID);

                        node = nodes[j].SelectSingleNode("terminalType");
                        if (node != null)
                            Int32.TryParse(node.InnerText.Trim(), out terminalType);

                        if (terminalType == 2)
                        {
                            criterionList.Add(Expression.Eq("roomId", endpointID));
                            confroom = m_IconfRoom.GetByCriteria(criterionList);
                            for (int i = 0; i < confroom.Count; i++)
                                confroom[i].mute = mute;
                            if (confroom.Count > 0)
                                m_IconfRoom.SaveOrUpdateList(confroom);
                        }
                        else if (terminalType == 4)
                        {
                            criterionList.Add(Expression.Eq("uId", endpointID));
                            confCascade = m_IconfCascade.GetByCriteria(criterionList);
                            for (int i = 0; i < confCascade.Count; i++)
                                confCascade[i].mute = mute;
                            if (confCascade.Count > 0)
                                m_IconfCascade.SaveOrUpdateList(confCascade);
                        }
                        else
                        {
                            criterionList.Add(Expression.Eq("userid", endpointID));
                            confuser = m_IconfUser.GetByCriteria(criterionList);
                            for (int i = 0; i < confuser.Count; i++)
                                confuser[i].mute = mute;
                            if (confuser.Count > 0)
                                m_IconfUser.SaveOrUpdateList(confuser);
                        }
                    }
                }
                outXml = "<success>1</success>";
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

    }
    #endregion

    #region CConfID
    /// <summary>
    /// Summary description for CConfID messanger object.
    /// </summary>
    /// 
    public class CConfID
    {
        private int m_confID;
        private int m_instanceID,m_RecurMode; //FB 2027
        private string m_sConfID;

        #region Constructors
        public CConfID(string confInst)
        {
            if (confInst.ToLower().CompareTo("new") == 0)
                confInst = "0";

            m_sConfID = confInst;
            int i = confInst.IndexOf(",");
            if (i >= 0)
            {
                m_confID = Int32.Parse(m_sConfID.Substring(0, i));
                m_instanceID = Int32.Parse(m_sConfID.Substring(i + 1, m_sConfID.Length - (i + 1)));
                //FB 2027 start
                m_RecurMode = 0;
                if(m_instanceID == 0)
                    m_RecurMode = 2;
                //FB 2027 end
            }
            else
            {
                m_confID = Int32.Parse(m_sConfID);
                m_instanceID = 0;
                m_RecurMode = 1; //FB 2027
            }
        }

        public CConfID(int confID, int instanceID)
        {
            m_sConfID = confID.ToString();
            if (instanceID > 0)
                m_sConfID += "," + instanceID.ToString();
            m_confID = confID;
            m_instanceID = instanceID;
        }
        #endregion

        public override string ToString() { return m_sConfID; }


        public int ID
        {
            get { return m_confID; }
            set { m_confID = value; }
        }
        public int instance
        {
            get { return m_instanceID; }
            set { m_instanceID = value; }
        }
        public int RecurMode //FB 2027
        {
            get { return m_RecurMode; }
            set { m_RecurMode = value; }
        }

    }
    #endregion

    //FB 2027 - SetConference - start
    #region SplitRoom
    /// <summary>
    /// SplitRoom
    /// </summary>
    public class SplitRoom
    {

        public Double Duration { get; set; }
        public DateTime StartTime { get; set; }
        public int RoomID { get; set; }
    }
    #endregion
    //FB 2027 - SetConference - end
}

